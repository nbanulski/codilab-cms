<?php

function dd($value)
{
    var_dump($value);
    exit;
}

chdir(__DIR__);

include __DIR__.'/init_autoloader.php';

Zend\Loader\AutoloaderFactory::factory(array(
    'Zend\Loader\StandardAutoloader' => array(
        'namespaces' => array(
            'SiteTest' => __DIR__ . '/module/Site/tests/SiteTest'
        )
    )
));

Zend\Mvc\Application::init(include __DIR__.'/config/application.config.php');
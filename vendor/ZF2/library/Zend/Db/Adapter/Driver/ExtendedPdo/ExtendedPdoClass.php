<?php
namespace Zend\Db\Adapter\Driver\ExtendedPdo;

class ExtendedPdoClass extends \PDO
{
    // Database drivers that support SAVEPOINTs.
    protected static $savepointTransactions = array('pgsql', 'mysql');
    protected static $currentTransactionLevel = 0;
    protected $inTransaction = false;

    protected function nestable()
    {
        return
            in_array(
                $this->getAttribute(\PDO::ATTR_DRIVER_NAME),
                self::$savepointTransactions
            );
    }

    public function beginTransaction()
    {
        if (!$this->nestable() || self::$currentTransactionLevel == 0)
        {
            //echo 'Starting transaction ' . "\n";
            parent::beginTransaction();
        }
        else
        {
            //echo 'Starting transaction ' . self::$currentTransactionLevel . "\n";
            $this->exec("SAVEPOINT LEVEL" . self::$currentTransactionLevel);
        }

        self::$currentTransactionLevel++;
        $this->inTransaction = true;
    }

    public function commit()
    {
        self::$currentTransactionLevel--;

        if (!$this->nestable() || self::$currentTransactionLevel == 0)
        {
            //echo 'Ending transaction. (commit)' . "\n";
            parent::commit();

            $this->inTransaction = false;
        }
        else
        {
            //echo 'Ending transaction ' . self::$currentTransactionLevel . ' (commit)' . "\n";
            $this->exec("RELEASE SAVEPOINT LEVEL" . self::$currentTransactionLevel);
        }
    }

    public function rollback()
    {
        self::$currentTransactionLevel--;

        if (!$this->nestable() || self::$currentTransactionLevel == 0)
        {
            //echo 'Ending transaction. (rollback)' . "\n";
            parent::rollBack();

            $this->inTransaction = false;
        }
        else
        {
            //echo 'Ending transaction ' . self::$currentTransactionLevel . ' (rollback)' . "\n";
            $this->exec("ROLLBACK TO SAVEPOINT LEVEL" . self::$currentTransactionLevel);
        }
    }

    public function inTransaction()
    {
        return $this->inTransaction;
    }
}
#!/bin/bash

database_array=()

echo "### --- Databases available to install:"
cd databases
index=0;
for i in $(ls -d *); do
	database_name=${i%%/};
	database_array[index]=$database_name;
	echo "[$index] ${database_name}";
	index=$[index+1];
done
max_index=$index
cd ..

echo -n "Enter the number of database which you want to import: "
read db_index
if [ -n "$db_index" ]
then
	if [ "$db_index" -gt "-1" ] && [ "$db_index" -le "$max_index" ]; then
		DB_NAME=${database_array[db_index]}
	fi

	export DB_USER=""
	export DB_PASSWORD=""
	source installation-scripts/ask-for-database-credentials.bash

	echo "Installing $DB_NAME database. Please be patient. It may take about 2 minutes on slow HDDs."
	cd databases/$DB_NAME/
	echo -e "\nDropping and re-creating $DB_NAME database:"
	bash drop-and-create-database.bash
	bash import-everything.bash
	cd ../../
else
	echo -e "\e[01;31mYou did not typed the name of the database. Aborting database installation.\e[00m"
fi

echo "### --- Done."

#!/bin/bash

INFO_ABOUT_OS="$(uname -a)"
ON_WINDOWS=0

if [[ $INFO_ABOUT_OS == *MINGW* ]]
then
    ON_WINDOWS=1
fi



if [[ $ON_WINDOWS == 0 ]]; then
    echo "| --- Setting 0777 permissions to config/autoload/global.php file."
    if [[ $UID != 0 ]]; then
        SUDO_EXISTS=$(sudo -h | wc -l)
        if [ ${SUDO_EXISTS} -gt 10 ]
        then
            sudo chmod 0777 ../config/autoload/global.php
        else
            echo -e "\e[01;31mNot enough permissions. Please start the script as root or sudo!\e[00m"
        fi
    else
        chmod 0777 ../config/autoload/global.php
    fi



    echo "| --- Setting 0777 permissions to data directory recursively."
    if [[ $UID != 0 ]]; then
        SUDO_EXISTS=$(sudo -h | wc -l)
        if [ ${SUDO_EXISTS} -gt 10 ]
        then
            sudo chmod -R 0777 ../data/
        else
            echo -e "\e[01;31mNot enough permissions. Please start the script as root or sudo!\e[00m"
        fi
    else
        chmod -R 0777 ../data/
    fi



    echo "| --- Setting 0777 permissions to public/ckeditor-files directory recursively."
    if [[ $UID != 0 ]]; then
        SUDO_EXISTS=$(sudo -h | wc -l)
        if [ ${SUDO_EXISTS} -gt 10 ]
        then
            sudo chmod -R 0777 ../public/ckeditor-files
        else
            echo -e "\e[01;31mNot enough permissions. Please start the script as root or sudo!\e[00m"
        fi
    else
        chmod -R 0777 ../public/ckeditor-files
    fi



    echo "| --- Setting 0777 permissions to public/upload directory recursively."
    if [[ $UID != 0 ]]; then
        SUDO_EXISTS=$(sudo -h | wc -l)
        if [ ${SUDO_EXISTS} -gt 10 ]
        then
            sudo chmod -R 0777 ../public/upload
        else
            echo -e "\e[01;31mNot enough permissions. Please start the script as root or sudo!\e[00m"
        fi
    else
        chmod -R 0777 ../public/upload
    fi
fi
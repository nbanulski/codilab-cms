#!/bin/bash

# if [ -n "${DB_USER+1}" ]; then # Another way to test if variable exist.
if [ -z "${DB_USER}" ]
then
	echo -n "Enter username of your database (default root): "
	read DB_USER
	if [ ! -n "$DB_USER" ]
	then
		DB_USER="root"
	fi

	echo -n "Enter password for this user (or leave empty if no password): "
	read DB_PASSWORD

	#echo -n "Enter the new database name (default ${DB_NAME}): "
	#read DB_NAME
fi

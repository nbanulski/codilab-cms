#!/bin/bash

bash ../../installation-scripts/ask-for-database-credentials.bash
mysql -u $DB_USER --password=$DB_PASSWORD < drop-and-create-database.sql

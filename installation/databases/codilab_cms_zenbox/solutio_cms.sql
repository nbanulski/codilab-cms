-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas generowania: 18 Sty 2016, 10:30
-- Wersja serwera: 5.5.44-MariaDB-cll-lve
-- Wersja PHP: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `solutio_cms`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `attachments`
--

CREATE TABLE `attachments` (
  `attachment_id` mediumint(8) UNSIGNED NOT NULL,
  `parent_attachment_id` mediumint(8) UNSIGNED DEFAULT NULL COMMENT 'Parent from which this attachment was created.',
  `is_published` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `file_size` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `page_block_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `hash_for_file_name` char(40) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `extension` varchar(5) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `mime_type` varchar(64) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `module_name` varchar(32) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `block_name` varchar(64) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `image_crop_rect_coords` varchar(32) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `original_file_name` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '' COMMENT 'File name from user file system - before upload.',
  `file_path` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '' COMMENT 'Full file path (with file name and extension) relative to attachments directory.'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `attachments`
--

INSERT INTO `attachments` (`attachment_id`, `parent_attachment_id`, `is_published`, `file_size`, `page_block_id`, `last_update_author_id`, `last_update_date`, `creation_date`, `hash_for_file_name`, `extension`, `mime_type`, `module_name`, `block_name`, `image_crop_rect_coords`, `original_file_name`, `file_path`) VALUES
(1, NULL, 1, 9596, 138, 3, '2014-02-19 13:20:28', '2014-02-19 13:20:28', '712623ffd540e3ae8b27f5ce936a9d2be7666c84', 'jpg', 'image/jpeg', 'site', 'events', '', 'Dolacz-do-nas!-Rekrutacja-na-studia-w-AFIB-Vistula_medium.jpg', 'data/attachments/module/site/block/events/138/712623ffd540e3ae8b27f5ce936a9d2be7666c84.jpg'),
(2, 1, 1, 9596, 138, NULL, '2014-02-19 13:22:40', '2014-02-19 13:20:28', '4e2f4f69792e7b4fb735e33a147a9ff189beb628', 'jpg', 'image/jpeg', 'site', 'events', '0,0,200,139.1304347826087', 'Dolacz-do-nas!-Rekrutacja-na-studia-w-AFIB-Vistula_medium.jpg', 'data/attachments//module/site/block/events/138/4e2f4f69792e7b4fb735e33a147a9ff189beb628.jpg'),
(3, NULL, 1, 9596, 138, 3, '2014-02-19 13:23:04', '2014-02-19 13:23:04', '63aa577fc01a56f9f81d2cf4fe40356b14240cdb', 'jpg', 'image/jpeg', 'site', 'events', '', 'Dolacz-do-nas!-Rekrutacja-na-studia-w-AFIB-Vistula_medium.jpg', 'data/attachments/module/site/block/events/138/63aa577fc01a56f9f81d2cf4fe40356b14240cdb.jpg'),
(4, 3, 1, 9596, 138, NULL, '2014-02-20 20:46:04', '2014-02-19 13:23:04', 'ccbbf2736fbf81d7510db52a6d2fb33928567cf7', 'jpg', 'image/jpeg', 'site', 'events', '0,0,200,139.1304347826087', 'Dolacz-do-nas!-Rekrutacja-na-studia-w-AFIB-Vistula_medium.jpg', 'data/attachments//module/site/block/events/138/ccbbf2736fbf81d7510db52a6d2fb33928567cf7.jpg'),
(5, NULL, 1, 44500, 138, 3, '2014-02-19 16:03:49', '2014-02-19 16:03:49', 'e6cc170630840c8ec526c350bd3fcedcb2ad16e1', 'jpg', 'image/jpeg', 'site', 'events', '', 'Pixmac000037703985 (Small).jpg', 'data/attachments/module/site/block/events/138/e6cc170630840c8ec526c350bd3fcedcb2ad16e1.jpg'),
(6, 5, 1, 44500, 138, NULL, '2014-02-19 16:04:00', '2014-02-19 16:03:49', 'b36547deaea3c925ed93d9cb11db70c0b4b5b511', 'jpg', 'image/jpeg', 'site', 'events', '0,0,613.8125,427', 'Pixmac000037703985 (Small).jpg', 'data/attachments//module/site/block/events/138/b36547deaea3c925ed93d9cb11db70c0b4b5b511.jpg'),
(7, NULL, 1, 1017634, 138, 3, '2014-02-19 17:31:56', '2014-02-19 17:31:56', '9404cdffcbb0d3a058b3bf1bd3a67a3f101b6eeb', 'jpg', 'image/jpeg', 'site', 'events', '', 'biznes foto.jpg', 'data/attachments/module/site/block/events/138/9404cdffcbb0d3a058b3bf1bd3a67a3f101b6eeb.jpg'),
(8, 7, 1, 1017634, 138, NULL, '2014-02-19 17:32:22', '2014-02-19 17:31:56', '1c6d218fb0e7ca97ef1a0843dc446a879f97f4de', 'jpg', 'image/jpeg', 'site', 'events', '0,0,2700,1878.2608695652175', 'biznes foto.jpg', 'data/attachments//module/site/block/events/138/1c6d218fb0e7ca97ef1a0843dc446a879f97f4de.jpg'),
(9, NULL, 1, 9596, 138, 3, '2014-02-19 17:33:20', '2014-02-19 17:33:20', '3931aec5e3cf961c2edfa365805a295ab29fcb1c', 'jpg', 'image/jpeg', 'site', 'events', '', 'Dolacz-do-nas!-Rekrutacja-na-studia-w-AFIB-Vistula_medium.jpg', 'data/attachments/module/site/block/events/138/3931aec5e3cf961c2edfa365805a295ab29fcb1c.jpg'),
(10, 9, 1, 9596, 138, NULL, '2014-02-19 17:34:08', '2014-02-19 17:33:20', '082b78adef0533f6ce41cbec250bc63167f04bb9', 'jpg', 'image/jpeg', 'site', 'events', '0,56,200,195.1304347826087', 'Dolacz-do-nas!-Rekrutacja-na-studia-w-AFIB-Vistula_medium.jpg', 'data/attachments//module/site/block/events/138/082b78adef0533f6ce41cbec250bc63167f04bb9.jpg'),
(11, NULL, 1, 1017634, 138, 3, '2014-02-19 19:15:58', '2014-02-19 19:15:58', '028f8651c92458a63e1a3fcd9e47494ee37d6901', 'jpg', 'image/jpeg', 'site', 'events', '', 'biznes foto.jpg', 'data/attachments/module/site/block/events/138/028f8651c92458a63e1a3fcd9e47494ee37d6901.jpg'),
(12, 11, 1, 1017634, 138, NULL, '2014-02-19 19:16:05', '2014-02-19 19:15:58', '404a6a198ec7cdcc42867ddbbccbcd81d80865f1', 'jpg', 'image/jpeg', 'site', 'events', '0,0,2700,1878.2608695652175', 'biznes foto.jpg', 'data/attachments//module/site/block/events/138/404a6a198ec7cdcc42867ddbbccbcd81d80865f1.jpg'),
(13, NULL, 1, 1411493, 138, 3, '2014-02-20 20:44:51', '2014-02-20 20:44:51', '11852598bc895f3627848905e3f9a76530d5fd92', 'jpg', 'image/jpeg', 'site', 'news', '', 'Fotolia_52895963_M.jpg', 'data/attachments/module/site/block/news/138/11852598bc895f3627848905e3f9a76530d5fd92.jpg'),
(14, NULL, 1, 1411493, 138, 3, '2014-02-20 20:45:46', '2014-02-20 20:45:46', '6d06342741c4f3a5c35069943f86c0d27e9ac4df', 'jpg', 'image/jpeg', 'site', 'news', '', 'Fotolia_52895963_M.jpg', 'data/attachments/module/site/block/news/138/6d06342741c4f3a5c35069943f86c0d27e9ac4df.jpg'),
(15, NULL, 1, 570509, 138, 3, '2014-02-20 20:46:22', '2014-02-20 20:46:22', '6e33f9faf135d059804f00d5f2c089eb2bc633d8', 'jpg', 'image/jpeg', 'site', 'news', '', 'azjata.jpg', 'data/attachments/module/site/block/news/138/6e33f9faf135d059804f00d5f2c089eb2bc633d8.jpg'),
(16, 15, 1, 570509, 138, NULL, '2014-02-20 20:46:45', '2014-02-20 20:46:22', 'f1cea66e16f2bb26ed764a16ac4d3bdfc043592c', 'jpg', 'image/jpeg', 'site', 'news', '0,0,941,654.6086956521739', 'azjata.jpg', 'data/attachments//module/site/block/news/138/f1cea66e16f2bb26ed764a16ac4d3bdfc043592c.jpg'),
(17, NULL, 1, 1062890, 138, 3, '2014-02-20 20:47:01', '2014-02-20 20:47:01', '69e9c496b60e8acb588ee40c584781844f9c8ada', 'jpg', 'image/jpeg', 'site', 'news', '', 'bandowe.jpg', 'data/attachments/module/site/block/news/138/69e9c496b60e8acb588ee40c584781844f9c8ada.jpg'),
(18, 17, 1, 1062890, 138, NULL, '2014-02-20 20:47:09', '2014-02-20 20:47:01', '5dce1900991d7e9e256af9c5fb98a96c7241c41f', 'jpg', 'image/jpeg', 'site', 'news', '', 'bandowe.jpg', 'data/attachments//module/site/block/news/138/5dce1900991d7e9e256af9c5fb98a96c7241c41f.jpg'),
(19, NULL, 1, 60096, 138, 3, '2014-02-20 20:47:49', '2014-02-20 20:47:49', '1a03e574bee1dc4e4df1d9b0da8f0f473873f58f', 'jpg', 'image/jpeg', 'site', 'news', '', 'Fotolia_32576060_XS.jpg', 'data/attachments/module/site/block/news/138/1a03e574bee1dc4e4df1d9b0da8f0f473873f58f.jpg'),
(20, NULL, 1, 275121, 138, 3, '2014-02-20 20:49:08', '2014-02-20 20:49:08', 'c2254b5eb39aa52082ad4a0ace31d29cbda0a20a', 'jpg', 'image/jpeg', 'site', 'news', '', 'Fotolia_44983429_M.jpg', 'data/attachments/module/site/block/news/138/c2254b5eb39aa52082ad4a0ace31d29cbda0a20a.jpg'),
(21, 20, 1, 275121, 138, NULL, '2014-02-20 20:49:29', '2014-02-20 20:49:08', 'e259d875f3907139b6b8c877e48361422a4cb116', 'jpg', 'image/jpeg', 'site', 'news', '0,0,1571,1092.8695652173913', 'Fotolia_44983429_M.jpg', 'data/attachments//module/site/block/news/138/e259d875f3907139b6b8c877e48361422a4cb116.jpg'),
(22, NULL, 1, 1825089, 138, 3, '2014-02-20 20:50:30', '2014-02-20 20:50:30', 'bfddc6f24f0ad3a5fdd96304a257c55e1a9f8d08', 'jpg', 'image/jpeg', 'site', 'news', '', 'Fotolia_41002280_X.jpg', 'data/attachments/module/site/block/news/138/bfddc6f24f0ad3a5fdd96304a257c55e1a9f8d08.jpg'),
(23, NULL, 1, 186734, 1024, 1, '2014-03-06 13:29:46', '2014-03-06 13:29:46', 'b3dbff2411a0cef0d6552e358972390aa20c7592', 'png', 'image/png', 'site', 'banner-small', '', 'cms_bug_blocks.png', 'data/attachments/module/site/block/banner-small/1024/b3dbff2411a0cef0d6552e358972390aa20c7592.png'),
(24, NULL, 1, 22819, 138, 4, '2014-03-06 15:01:19', '2014-03-06 15:01:19', '088c4d0903cc11d2e8a46c1bb8d14616e29794a7', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'u371.jpg', 'data/attachments/module/site/block/news-list/138/088c4d0903cc11d2e8a46c1bb8d14616e29794a7.jpg'),
(25, 24, 1, 72906, 138, NULL, '2014-03-13 17:08:48', '2014-03-06 15:01:19', 'cb128b64c15470ea2ffc3e07ad8e2680d2e77eb9', 'jpg', 'image/jpeg', 'site', 'news-list', '8,9,403.3125,284', 'u371.jpg', 'data/attachments//module/site/block/news-list/138/cb128b64c15470ea2ffc3e07ad8e2680d2e77eb9.jpg'),
(26, 19, 1, 60161, 138, NULL, '2014-03-13 17:11:27', '2014-02-20 20:47:49', '8d4db82d8a00654207184bdd8a014fbc37b63698', 'jpg', 'image/jpeg', 'site', 'news', '0,0,385,267.82608695652175', 'Fotolia_32576060_XS.jpg', 'data/attachments//module/site/block/news/138/8d4db82d8a00654207184bdd8a014fbc37b63698.jpg'),
(27, 13, 1, 1411493, 138, NULL, '2014-03-07 08:44:11', '2014-02-20 20:44:51', 'bdc27a9044d80532d477d25183699203ca908cc3', 'jpg', 'image/jpeg', 'site', 'news', '0,0,1617.1875,1125', 'Fotolia_52895963_M.jpg', 'data/attachments//module/site/block/news/138/bdc27a9044d80532d477d25183699203ca908cc3.jpg'),
(28, 22, 1, 1825089, 138, NULL, '2014-03-07 08:44:39', '2014-02-20 20:50:30', '931ddb129429e55f2f8dce9b1ee2079e2016a0ab', 'jpg', 'image/jpeg', 'site', 'news', '0,0,4071,2832', 'Fotolia_41002280_X.jpg', 'data/attachments//module/site/block/news/138/931ddb129429e55f2f8dce9b1ee2079e2016a0ab.jpg'),
(29, NULL, 1, 22819, 1267, 4, '2014-03-07 09:48:19', '2014-03-07 09:48:19', 'a934969d259c75db0f945596ed1836db61adfc93', 'jpg', 'image/jpeg', 'site', 'banner-small', '', 'u371.jpg', 'data/attachments/module/site/block/banner-small/1267/a934969d259c75db0f945596ed1836db61adfc93.jpg'),
(30, NULL, 1, 1017634, 1267, 4, '2014-03-07 09:48:54', '2014-03-07 09:48:54', 'fc14dc5ab03732cfe4d8551f19946803cb442d04', 'jpg', 'image/jpeg', 'site', 'banner-small', '', 'biznes foto.jpg', 'data/attachments/module/site/block/banner-small/1267/fc14dc5ab03732cfe4d8551f19946803cb442d04.jpg'),
(31, NULL, 1, 229702, 1267, 4, '2014-03-07 09:49:58', '2014-03-07 09:49:58', 'f51329217868ee2983cf4f03d0aa130b0f266176', 'png', 'image/png', 'site', 'banner-small', '', 'aktualnosci-lwow_2_dni-170-210-440-250.png', 'data/attachments/module/site/block/banner-small/1267/f51329217868ee2983cf4f03d0aa130b0f266176.png'),
(32, NULL, 1, 221971, 1344, 1, '2014-03-07 11:24:09', '2014-03-07 11:24:09', '6cc1f5f9bf3e16e6e218ac5eb87ab40bc489acd1', 'jpg', 'image/jpeg', 'site', 'banner-small', '', '2.jpg', 'data/attachments/module/site/block/banner-small/1344/6cc1f5f9bf3e16e6e218ac5eb87ab40bc489acd1.jpg'),
(33, NULL, 1, 308555, 1344, 1, '2014-03-07 11:25:32', '2014-03-07 11:25:32', 'd900dc71c1ff5e114d71e48a338082c1b8a7a331', 'jpg', 'image/jpeg', 'site', 'banner-small', '', 'ChromaStock_1642292.jpg', 'data/attachments/module/site/block/banner-small/1344/d900dc71c1ff5e114d71e48a338082c1b8a7a331.jpg'),
(34, NULL, 1, 12731, 1363, 1, '2014-03-07 13:05:10', '2014-03-07 13:05:10', 'cd7ad90caeec08b0ab16f9cf143063484ac2d7aa', 'jpg', 'image/jpeg', 'site', 'banner-small', '', 'eliasz.jpg', 'data/attachments/module/site/block/banner-small/1363/cd7ad90caeec08b0ab16f9cf143063484ac2d7aa.jpg'),
(35, 34, 1, 12731, 1363, NULL, '2014-03-10 14:14:10', '2014-03-07 13:05:10', '5933c341394fcffe034417ac6ca3a98de2f8dc55', 'jpg', 'image/jpeg', 'site', 'banner-small', '0,24,250,109.71428571428572', 'eliasz.jpg', 'data/attachments//module/site/block/banner-small/1363/5933c341394fcffe034417ac6ca3a98de2f8dc55.jpg'),
(36, NULL, 1, 40918, 1363, 1, '2014-03-10 14:11:06', '2014-03-10 14:11:06', 'fc52d176393f60e5c5fb1f605300fa981ebfc6b6', 'png', 'image/png', 'site', 'banner-small', '', 'keep-calm-and-davay-da-svidaniya.png', 'data/attachments/module/site/block/banner-small/1363/fc52d176393f60e5c5fb1f605300fa981ebfc6b6.png'),
(37, NULL, 1, 40918, 1363, 1, '2014-03-10 14:16:01', '2014-03-10 14:16:01', '48157b2c2038725b6c885bf547a689d6511784a4', 'png', 'image/png', 'site', 'banner-small', '', 'keep-calm-and-davay-da-svidaniya.png', 'data/attachments/module/site/block/banner-small/1363/48157b2c2038725b6c885bf547a689d6511784a4.png'),
(38, NULL, 1, 49274, 1389, 1, '2014-03-11 14:29:51', '2014-03-11 14:29:51', '0d323118c70415d0abe4faa124d5125d15c34120', 'jpg', 'image/jpeg', 'site', 'banner-small', '0,0,875,300', 'PR7_7870_stitch (Kopiowanie).jpg', 'data/attachments/module/site/block/banner-small/1389/0d323118c70415d0abe4faa124d5125d15c34120.jpg'),
(39, NULL, 1, 7191215, 1442, 1, '2014-03-12 16:47:45', '2014-03-12 16:47:44', 'f9182a509a88eccf5392c45be5fab81f34e61e0e', 'jpg', 'image/jpeg', 'site', 'banner-small', '0,0,875,300', 'ChromaStock_28074901.jpg', 'data/attachments/module/site/block/banner-small/1442/f9182a509a88eccf5392c45be5fab81f34e61e0e.jpg'),
(40, NULL, 1, 7191215, 1442, 1, '2014-03-12 16:49:26', '2014-03-12 16:49:24', '15df5ae32d90fa4ab5b122c3b6ac70969755458c', 'jpg', 'image/jpeg', 'site', 'banner-small', '0,0,875,300', 'ChromaStock_28074901.jpg', 'data/attachments/module/site/block/banner-small/1442/15df5ae32d90fa4ab5b122c3b6ac70969755458c.jpg'),
(41, NULL, 1, 7191215, 1442, 1, '2014-03-13 10:02:55', '2014-03-13 10:02:54', '0f253f94e18954facffd8b4f3b5d20c50a97a28d', 'jpg', 'image/jpeg', 'site', 'banner-small', '0,0,875,300', 'ChromaStock_28074901.jpg', 'data/attachments/module/site/block/banner-small/1442/0f253f94e18954facffd8b4f3b5d20c50a97a28d.jpg'),
(42, 41, 1, 7191215, 1442, NULL, '2014-03-13 10:04:04', '2014-03-13 10:02:54', '83609c75d9ad786d0678c18f782400e4fbb44993', 'jpg', 'image/jpeg', 'site', 'banner-small', '0,0,875,300', 'ChromaStock_28074901.jpg', 'data/attachments//module/site/block/banner-small/1442/83609c75d9ad786d0678c18f782400e4fbb44993.jpg'),
(43, NULL, 1, 7191215, 1442, 1, '2014-03-13 10:05:33', '2014-03-13 10:05:33', 'fef7f04d983de320740a7a20e78797058d40c483', 'jpg', 'image/jpeg', 'site', 'banner-small', '0,0,875,300', 'ChromaStock_28074901.jpg', 'data/attachments/module/site/block/banner-small/1442/fef7f04d983de320740a7a20e78797058d40c483.jpg'),
(44, 43, 1, 7191215, 1442, NULL, '2014-03-13 10:06:01', '2014-03-13 10:05:33', 'e935c96dd34432d9e38aeae6f8641e356c8b9b7f', 'jpg', 'image/jpeg', 'site', 'banner-small', '0,0,875,300', 'ChromaStock_28074901.jpg', 'data/attachments//module/site/block/banner-small/1442/e935c96dd34432d9e38aeae6f8641e356c8b9b7f.jpg'),
(45, NULL, 1, 345688, NULL, 4, '2014-03-13 13:08:22', '2014-03-13 13:08:22', '3f0ecc9718f29491195db4a695670d7c7eac2349', 'png', 'image/png', 'site', 'event-list', '', 'image-8000x8000.png', 'data/attachments/module/site/block/event-list/1444/3f0ecc9718f29491195db4a695670d7c7eac2349.png'),
(46, NULL, 1, 7191215, NULL, 4, '2014-03-13 13:11:05', '2014-03-13 13:11:05', '08d771e3f0f4b3141cab3086055ea4ab925da47f', 'jpg', 'image/jpeg', 'site', 'event-list', '', 'ChromaStock_28074901.jpg', 'data/attachments/module/site/block/event-list/1444/08d771e3f0f4b3141cab3086055ea4ab925da47f.jpg'),
(47, NULL, 1, 5828188, NULL, 4, '2014-03-13 13:17:18', '2014-03-13 13:17:18', '3f756c6019e46df9a7aa254fd281bab4baa745bb', 'jpg', 'image/jpeg', 'site', 'event-list', '', 'Indigo_22.JPG', 'data/attachments/module/site/block/event-list/1444/3f756c6019e46df9a7aa254fd281bab4baa745bb.jpg'),
(48, NULL, 1, 7191215, NULL, 4, '2014-03-13 13:24:34', '2014-03-13 13:24:34', '7887e3e544d655c4f3a9b050f04dcd146335c92f', 'jpg', 'image/jpeg', 'site', 'event-list', '', 'ChromaStock_28074901.jpg', 'data/attachments/module/site/block/event-list/1444/7887e3e544d655c4f3a9b050f04dcd146335c92f.jpg'),
(49, NULL, 1, 21986, 138, 7, '2014-03-13 17:12:06', '2014-03-13 17:12:06', 'e466afdea1492b05498dc301aaf54dadedeaec8d', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'lady.jpg', 'data/attachments/module/site/block/news-list/138/e466afdea1492b05498dc301aaf54dadedeaec8d.jpg'),
(50, NULL, 1, 475258, 138, 7, '2014-03-13 17:13:49', '2014-03-13 17:13:49', '19891c0cd2c5e7a874bf8e11c48d8597831455ff', 'jpg', 'image/jpeg', 'site', 'news-list', '', '1102700771-553.jpg', 'data/attachments/module/site/block/news-list/138/19891c0cd2c5e7a874bf8e11c48d8597831455ff.jpg'),
(51, 50, 1, 372205, 138, NULL, '2014-03-13 17:14:39', '2014-03-13 17:13:49', '19a210d843912620ac3b58430a65946692e05ec6', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,924.3125,643', '1102700771-553.jpg', 'data/attachments/module/site/block/news-list/138/19a210d843912620ac3b58430a65946692e05ec6.jpg'),
(52, NULL, 1, 364421, NULL, 7, '2014-03-13 17:25:02', '2014-03-13 17:25:02', '2056297e575d8864e3f5f902f4a4ff48d5edcd05', 'jpg', 'image/jpeg', 'site', 'news-list', '', '1114162880-44.jpg', 'data/attachments/module/site/block/news-list/1445/2056297e575d8864e3f5f902f4a4ff48d5edcd05.jpg'),
(53, 52, 1, 195407, NULL, NULL, '2014-03-13 17:25:24', '2014-03-13 17:25:02', 'a57688fd671027c78bd602e10e841e9f780de082', 'jpg', 'image/jpeg', 'site', 'news-list', '0,3,632.5,443', '1114162880-44.jpg', 'data/attachments/module/site/block/news-list/1445/a57688fd671027c78bd602e10e841e9f780de082.jpg'),
(54, NULL, 1, 732305, 1447, 7, '2014-03-13 17:49:42', '2014-03-13 17:49:42', '4d75a57af2e65605ed51c32e7f9ac3ccec48024d', 'jpg', 'image/jpeg', 'site', 'news-list', '', '1130854331-6050.jpg', 'data/attachments/module/site/block/news-list/1447/4d75a57af2e65605ed51c32e7f9ac3ccec48024d.jpg'),
(55, 54, 1, 652431, 1447, NULL, '2014-03-13 17:50:41', '2014-03-13 17:49:42', '57d19c2b785155601a0b82ac32f45857daacd2bf', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,1085.3125,755', '1130854331-6050.jpg', 'data/attachments/module/site/block/news-list/1447/57d19c2b785155601a0b82ac32f45857daacd2bf.jpg'),
(56, NULL, 1, 487600, 1501, 1, '2014-03-14 20:35:55', '2014-03-14 20:35:55', 'c9cf65a34f796c14652e02f3bd8b01d7a3c04e32', 'jpg', 'image/jpeg', 'site', 'banner-small', '0,0,875,300', 'iStock_000002573112Large.jpg', 'data/attachments/module/site/block/banner-small/1501/c9cf65a34f796c14652e02f3bd8b01d7a3c04e32.jpg'),
(57, NULL, 1, 7191215, NULL, 1, '2014-03-18 08:48:11', '2014-03-18 08:48:11', '4a10c076f4eea8672069704626d42c3e1a2c8dbb', 'jpg', 'image/jpeg', 'site', 'event-list', '', 'ChromaStock_28074901.jpg', 'data/attachments/module/site/block/event-list/1444/4a10c076f4eea8672069704626d42c3e1a2c8dbb.jpg'),
(58, NULL, 1, 17023, NULL, 1, '2014-03-18 13:00:05', '2014-03-18 13:00:05', '6028afcca6045b4ae33cbc421047be2c77d5e1a3', 'png', 'image/png', 'site', 'news-list', '', 'Rozmowa-rekrutacyjna-od-podszewki-spotkanie-z-rekruterem-z-Getin-Noble-Bank_medium.png', 'data/attachments/module/site/block/news-list/1445/6028afcca6045b4ae33cbc421047be2c77d5e1a3.png'),
(59, 58, 1, 38481, NULL, NULL, '2014-03-18 13:00:19', '2014-03-18 13:00:05', '8f06668dfb4531e4d38bf259dbe25b8b757d49fc', 'png', 'image/png', 'site', 'news-list', '0,178,1200,1012.7826086956521', 'Rozmowa-rekrutacyjna-od-podszewki-spotkanie-z-rekruterem-z-Getin-Noble-Bank_medium.png', 'data/attachments/module/site/block/news-list/1445/8f06668dfb4531e4d38bf259dbe25b8b757d49fc.png'),
(60, NULL, 1, 8254, NULL, 1, '2014-03-18 13:02:28', '2014-03-18 13:02:28', 'ea5bac5bd7fb80e7eeddbcccc8076c3785623d1f', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'International-Week-w-AFiB-Vistula_medium.jpg', 'data/attachments/module/site/block/news-list/1445/ea5bac5bd7fb80e7eeddbcccc8076c3785623d1f.jpg'),
(61, 60, 1, 364807, NULL, NULL, '2014-03-18 13:02:38', '2014-03-18 13:02:28', 'df5274a8e92ae4b385366c3d70913e64f5a69e49', 'jpg', 'image/jpeg', 'site', 'news-list', '0,177,1159,983.2608695652174', 'International-Week-w-AFiB-Vistula_medium.jpg', 'data/attachments/module/site/block/news-list/1445/df5274a8e92ae4b385366c3d70913e64f5a69e49.jpg'),
(62, NULL, 1, 9386, NULL, 1, '2014-03-18 13:04:05', '2014-03-18 13:04:05', '84beb95d296dbc6e700466d84b142a7c316e603a', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'ABSOLUTORIUM_medium.jpg', 'data/attachments/module/site/block/news-list/1445/84beb95d296dbc6e700466d84b142a7c316e603a.jpg'),
(63, 62, 1, 413396, NULL, NULL, '2014-03-18 13:04:14', '2014-03-18 13:04:05', '5aa27608a628c6cb1052302a0034b134f4cc9761', 'jpg', 'image/jpeg', 'site', 'news-list', '0,227,1194,1057.608695652174', 'ABSOLUTORIUM_medium.jpg', 'data/attachments/module/site/block/news-list/1445/5aa27608a628c6cb1052302a0034b134f4cc9761.jpg'),
(64, NULL, 1, 37241, NULL, 1, '2014-03-18 13:05:48', '2014-03-18 13:05:48', 'a350675897ba36bc36726f71c72d4bd8166bdc99', 'jpg', 'image/jpeg', 'site', 'news-list', '', '676933b3ea5fd468e8d3202a45ffd5de.jpg', 'data/attachments/module/site/block/news-list/1445/a350675897ba36bc36726f71c72d4bd8166bdc99.jpg'),
(65, 64, 1, 258190, NULL, NULL, '2014-03-18 13:05:54', '2014-03-18 13:05:48', '71998b571880fb0ca4fe05433212118dfb550431', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,970.3125,675', '676933b3ea5fd468e8d3202a45ffd5de.jpg', 'data/attachments/module/site/block/news-list/1445/71998b571880fb0ca4fe05433212118dfb550431.jpg'),
(66, NULL, 1, 54750, NULL, 1, '2014-03-18 13:27:10', '2014-03-18 13:27:10', 'ed043db43d630e120a55ffd27993acabe8d3e90c', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'EDU_0094-Kopiowanie_galeria.jpg', 'data/attachments/module/site/block/news-list/1445/ed043db43d630e120a55ffd27993acabe8d3e90c.jpg'),
(67, 66, 1, 603719, NULL, NULL, '2014-03-18 13:27:22', '2014-03-18 13:27:10', '1352185fb6eb4b2fb0ef0c7cf5bb6ea4871546ce', 'jpg', 'image/jpeg', 'site', 'news-list', '0,49,1200,883.7826086956521', 'EDU_0094-Kopiowanie_galeria.jpg', 'data/attachments/module/site/block/news-list/1445/1352185fb6eb4b2fb0ef0c7cf5bb6ea4871546ce.jpg'),
(68, NULL, 1, 413587, NULL, 1, '2014-03-19 16:35:58', '2014-03-19 16:35:58', '93fa6e1b1625459c656e0d921254b39bd17c6533', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,875,300', 'gala2014.jpg', 'data/attachments/module/site/block/small-banner/1541/93fa6e1b1625459c656e0d921254b39bd17c6533.jpg'),
(69, NULL, 1, 618863, NULL, 1, '2014-03-19 16:39:39', '2014-03-19 16:39:39', '787c5989043d2481289bbdfd88efdaffae102f27', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,875,300', 'u352.jpg', 'data/attachments/module/site/block/small-banner/1541/787c5989043d2481289bbdfd88efdaffae102f27.jpg'),
(70, NULL, 1, 413587, NULL, 1, '2014-03-19 16:41:40', '2014-03-19 16:41:40', '09400d7ea07eb5a59ca2bf2d5fa4d7472c7026fb', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,875,300', 'gala2014.jpg', 'data/attachments/module/site/block/small-banner/1541/09400d7ea07eb5a59ca2bf2d5fa4d7472c7026fb.jpg'),
(71, NULL, 1, 413587, NULL, 1, '2014-03-19 16:43:02', '2014-03-19 16:43:02', 'd86eea2f6b428c9e96757c15efe7a0e8443a55e4', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,875,300', 'gala2014.jpg', 'data/attachments/module/site/block/small-banner/1541/d86eea2f6b428c9e96757c15efe7a0e8443a55e4.jpg'),
(72, NULL, 1, 268892, NULL, 1, '2014-03-19 16:44:14', '2014-03-19 16:44:14', '97662c0b748331405b74520e6797bf71a4868d2f', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,875,300', 'facebook_please_leave_me.jpg', 'data/attachments/module/site/block/small-banner/1541/97662c0b748331405b74520e6797bf71a4868d2f.jpg'),
(73, NULL, 1, 342489, NULL, 1, '2014-03-19 16:44:28', '2014-03-19 16:44:28', '20d868ab9039aa2dc22e0999a48ae9a1b5a6f30d', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,875,300', 'u352-100x100.jpg', 'data/attachments/module/site/block/small-banner/1541/20d868ab9039aa2dc22e0999a48ae9a1b5a6f30d.jpg'),
(74, NULL, 1, 407287, NULL, 1, '2014-03-19 16:46:25', '2014-03-19 16:46:25', '4b0084086e3dcf716f591a73c91e2a0cca56418d', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,875,300', 'u371.jpg', 'data/attachments/module/site/block/small-banner/1541/4b0084086e3dcf716f591a73c91e2a0cca56418d.jpg'),
(75, NULL, 1, 1006381, NULL, 1, '2014-03-19 16:57:47', '2014-03-19 16:57:46', 'c41487a41b9a8943a6555287cf7450b084580e0b', 'png', 'image/png', 'site', 'small-banner', '0,0,875,300', 'Lezzetti.png', 'data/attachments/module/site/block/small-banner/1541/c41487a41b9a8943a6555287cf7450b084580e0b.png'),
(76, NULL, 1, 68719, NULL, 1, '2014-03-19 17:01:30', '2014-03-19 17:01:30', '61defecf361af364826c9f965174a3c66cedfdc8', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'u352.jpg', 'data/attachments/module/site/block/small-banner/1541/61defecf361af364826c9f965174a3c66cedfdc8.jpg'),
(77, 76, 1, 213406, NULL, NULL, '2014-03-19 17:01:30', '2014-03-19 17:01:30', '282fb3661de88ec47b2e3cc9947ec3b86e24b088', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,875,300', 'u352.jpg', 'data/attachments/module/site/block/small-banner/1541/282fb3661de88ec47b2e3cc9947ec3b86e24b088.jpg'),
(78, NULL, 1, 66305, 1544, 1, '2014-03-19 22:36:59', '2014-03-19 22:36:59', '16c3916059a306f15479b1c6b718fc72245202a0', 'jpg', 'image/jpeg', 'site', 'small-banner', '', '8954028.jpg', 'data/attachments/module/site/block/small-banner/1544/16c3916059a306f15479b1c6b718fc72245202a0.jpg'),
(79, 78, 1, 304393, 1544, NULL, '2014-03-19 22:38:38', '2014-03-19 22:36:59', 'c6f4015d6ba5fd60275b4856a9b039fc70702343', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,219,1200,630.4285714285714', '8954028.jpg', 'data/attachments/module/site/block/small-banner/1544/c6f4015d6ba5fd60275b4856a9b039fc70702343.jpg'),
(80, NULL, 1, 22819, NULL, 1, '2014-03-20 11:44:15', '2014-03-20 11:44:15', '623ae102c9ed4ce446a6215e813f1263ac83ad7e', 'jpg', 'image/jpeg', 'site', 'event-list', '', 'u371.jpg', 'data/attachments/module/site/block/event-list/1444/623ae102c9ed4ce446a6215e813f1263ac83ad7e.jpg'),
(81, NULL, 1, 367429, NULL, 1, '2014-03-21 15:53:10', '2014-03-21 15:53:10', '9064cf028606b6d8e1dd9d5b129a756e40fbb433', 'jpg', 'image/jpeg', 'site', 'big-banner', '', 'banner-1-1.jpg', 'data/attachments/module/site/block/big-banner/385/9064cf028606b6d8e1dd9d5b129a756e40fbb433.jpg'),
(82, 81, 1, 376619, NULL, NULL, '2014-03-21 15:53:10', '2014-03-21 15:53:10', '1e605433dba0ae4f10bb226d233c9ccaa3874c0f', 'jpg', 'image/jpeg', 'site', 'big-banner', '0,0,2000,430', 'banner-1-1.jpg', 'data/attachments/module/site/block/big-banner/385/1e605433dba0ae4f10bb226d233c9ccaa3874c0f.jpg'),
(83, NULL, 1, 160035, NULL, 1, '2014-03-21 15:54:15', '2014-03-21 15:54:15', '7219432c70bb52417f3db41e658cf7bf35fa9c36', 'jpg', 'image/jpeg', 'site', 'big-banner', '', 'b21burns_jean018.jpg', 'data/attachments/module/site/block/big-banner/385/7219432c70bb52417f3db41e658cf7bf35fa9c36.jpg'),
(84, NULL, 1, 237366, NULL, 1, '2014-03-21 15:54:32', '2014-03-21 15:54:32', '965627c1e1de715fad2e2ed106fbf4f83508415d', 'jpg', 'image/jpeg', 'site', 'big-banner', '', '00S40NP4.jpg', 'data/attachments/module/site/block/big-banner/385/965627c1e1de715fad2e2ed106fbf4f83508415d.jpg'),
(85, 84, 1, 93477, NULL, NULL, '2014-03-21 15:54:40', '2014-03-21 15:54:32', '251feee6ca97de40665cf1e0ce576edda2824f37', 'jpg', 'image/jpeg', 'site', 'big-banner', '0,108,682,254.63', '00S40NP4.jpg', 'data/attachments/module/site/block/big-banner/385/251feee6ca97de40665cf1e0ce576edda2824f37.jpg'),
(86, NULL, 1, 420041, 1614, 1, '2014-03-21 18:31:01', '2014-03-21 18:31:01', 'e427cfdd2b58950aa88c1ec0b4533d0a5fcff490', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'PR7_7870_stitch.jpg', 'data/attachments/module/site/block/small-banner/1614/e427cfdd2b58950aa88c1ec0b4533d0a5fcff490.jpg'),
(87, 86, 1, 386897, 1614, NULL, '2014-03-21 18:33:20', '2014-03-21 18:31:01', '009181d6a3afe5510a2a24aa27d6f2fd0fc0bf30', 'jpg', 'image/jpeg', 'site', 'small-banner', '48,9,1191.3333333333333,401', 'PR7_7870_stitch.jpg', 'data/attachments/module/site/block/small-banner/1614/009181d6a3afe5510a2a24aa27d6f2fd0fc0bf30.jpg'),
(88, NULL, 1, 816089, 1614, 1, '2014-03-21 18:36:05', '2014-03-21 18:36:05', 'b5027bde4482993deadfce03a536f9f4d1561e7e', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'Budynek_VU.jpg', 'data/attachments/module/site/block/small-banner/1614/b5027bde4482993deadfce03a536f9f4d1561e7e.jpg'),
(89, 88, 1, 460840, 1614, NULL, '2014-03-21 18:36:42', '2014-03-21 18:36:05', 'eea30572d70de2c7aab47fd8c8d00c1397bb9303', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,176,1200,587.4285714285714', 'Budynek_VU.jpg', 'data/attachments/module/site/block/small-banner/1614/eea30572d70de2c7aab47fd8c8d00c1397bb9303.jpg'),
(90, NULL, 1, 412175, 1548, 1, '2014-03-23 11:09:25', '2014-03-23 11:09:25', 'e01eaa33659ebba6ea3e6b7ad7e2ed57f36de18e', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'książka.jpg', 'data/attachments/module/site/block/small-banner/1548/e01eaa33659ebba6ea3e6b7ad7e2ed57f36de18e.jpg'),
(91, 90, 1, 306805, 1548, NULL, '2014-03-23 11:10:03', '2014-03-23 11:09:25', '4714ffe2ef54dc0da8efd636d82fab4e49ea26ae', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,295,1192.9166666666665,704', 'książka.jpg', 'data/attachments/module/site/block/small-banner/1548/4714ffe2ef54dc0da8efd636d82fab4e49ea26ae.jpg'),
(92, NULL, 1, 391048, 1930, 1, '2014-03-23 11:17:10', '2014-03-23 11:17:10', '38b070ce35ac3aa21b18c7bcd96ded606bd24822', 'jpg', 'image/jpeg', 'site', 'small-banner', '', '22529197.jpg', 'data/attachments/module/site/block/small-banner/1930/38b070ce35ac3aa21b18c7bcd96ded606bd24822.jpg'),
(93, 92, 1, 250013, 1930, NULL, '2014-03-23 11:18:01', '2014-03-23 11:17:10', '21c252c4401ccdee242d62de84f4d52f0ab1e9ab', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,155,1200,566.4285714285714', '22529197.jpg', 'data/attachments/module/site/block/small-banner/1930/21c252c4401ccdee242d62de84f4d52f0ab1e9ab.jpg'),
(94, NULL, 1, 463587, 1946, 1, '2014-03-23 23:03:37', '2014-03-23 23:03:37', '8d637b6b1523e3ce26119cab8d7552a36d86a72d', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'StKo_260909_081 (Small).jpg', 'data/attachments/module/site/block/small-banner/1946/8d637b6b1523e3ce26119cab8d7552a36d86a72d.jpg'),
(95, 94, 1, 103659, 1946, NULL, '2014-03-23 23:03:57', '2014-03-23 23:03:37', '4edbc7d7834a1fa2848487f7b05de24d2dcb4872', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,875,300', 'StKo_260909_081 (Small).jpg', 'data/attachments/module/site/block/small-banner/1946/4edbc7d7834a1fa2848487f7b05de24d2dcb4872.jpg'),
(96, NULL, 1, 532775, 1946, 1, '2014-03-23 23:04:35', '2014-03-23 23:04:35', '250124e06d9ee5d6e3f7d29a5f1c589fa3b240f0', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'StKo_260909_121.jpg', 'data/attachments/module/site/block/small-banner/1946/250124e06d9ee5d6e3f7d29a5f1c589fa3b240f0.jpg'),
(97, NULL, 1, 463587, 1946, 1, '2014-03-23 23:04:54', '2014-03-23 23:04:54', '97d079b42eb48ca7b5dae08b00404245e19d029e', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'StKo_260909_081 (Small).jpg', 'data/attachments/module/site/block/small-banner/1946/97d079b42eb48ca7b5dae08b00404245e19d029e.jpg'),
(98, 97, 1, 103659, 1946, NULL, '2014-03-23 23:04:54', '2014-03-23 23:04:54', '87c7a9e5bebabe57ca4419d9ff2a58cd4b5218b3', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,875,300', 'StKo_260909_081 (Small).jpg', 'data/attachments/module/site/block/small-banner/1946/87c7a9e5bebabe57ca4419d9ff2a58cd4b5218b3.jpg'),
(99, NULL, 1, 474640, 1946, 1, '2014-03-23 23:05:30', '2014-03-23 23:05:30', '2ad055efa338c3f20d0c8d567acdaa6690b583bc', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'studia za darmo.JPG', 'data/attachments/module/site/block/small-banner/1946/2ad055efa338c3f20d0c8d567acdaa6690b583bc.jpg'),
(100, 99, 1, 123168, 1946, NULL, '2014-03-23 23:05:30', '2014-03-23 23:05:30', 'cdf826d844cdd4e7fef98ef291dd16d87a649d2b', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,875,300', 'studia za darmo.JPG', 'data/attachments/module/site/block/small-banner/1946/cdf826d844cdd4e7fef98ef291dd16d87a649d2b.jpg'),
(101, NULL, 1, 285207, 1946, 1, '2014-03-23 23:07:11', '2014-03-23 23:07:11', 'dd3cc05a4ba84df3d1ad98f0ee5f9e1722d78402', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'StKo_260909_121 (Kopiowanie).jpg', 'data/attachments/module/site/block/small-banner/1946/dd3cc05a4ba84df3d1ad98f0ee5f9e1722d78402.jpg'),
(102, 101, 1, 54796, 1946, NULL, '2014-03-23 23:07:11', '2014-03-23 23:07:11', 'b184b9bff19bd2503856ea8ad618757382036d7f', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,875,300', 'StKo_260909_121 (Kopiowanie).jpg', 'data/attachments/module/site/block/small-banner/1946/b184b9bff19bd2503856ea8ad618757382036d7f.jpg'),
(103, NULL, 1, 319457, 1946, 1, '2014-03-23 23:09:25', '2014-03-23 23:09:25', 'a77981a932068c525897808a4135b0a40809b49a', 'jpg', 'image/jpeg', 'site', 'small-banner', '', '1.jpg', 'data/attachments/module/site/block/small-banner/1946/a77981a932068c525897808a4135b0a40809b49a.jpg'),
(104, 103, 1, 233020, 1946, NULL, '2014-03-23 23:09:55', '2014-03-23 23:09:25', '3357d30d97e184846638e4b88617af0605cf5061', 'jpg', 'image/jpeg', 'site', 'small-banner', '7,161,1200,570.0285714285715', '1.jpg', 'data/attachments/module/site/block/small-banner/1946/3357d30d97e184846638e4b88617af0605cf5061.jpg'),
(105, NULL, 1, 190644, NULL, 1, '2014-03-24 00:05:17', '2014-03-24 00:05:17', '867753165051ff5e376ae381a8a3fa29c36d28b6', 'jpg', 'image/jpeg', 'site', 'big-banner', '', 'banner-33.jpg', 'data/attachments/module/site/block/big-banner/385/867753165051ff5e376ae381a8a3fa29c36d28b6.jpg'),
(106, 105, 1, 191938, NULL, NULL, '2014-03-24 00:06:53', '2014-03-24 00:05:17', '1d71d4e9e9fb75d68395aa0e615422b0cbacad67', 'jpg', 'image/jpeg', 'site', 'big-banner', '0,0,2000,430', 'banner-33.jpg', 'data/attachments/module/site/block/big-banner/385/1d71d4e9e9fb75d68395aa0e615422b0cbacad67.jpg'),
(107, NULL, 1, 225962, NULL, 1, '2014-03-24 00:07:36', '2014-03-24 00:07:36', '84605f43787303e518dea806e4816f138e858619', 'jpg', 'image/jpeg', 'site', 'big-banner', '', 'banner-34.jpg', 'data/attachments/module/site/block/big-banner/385/84605f43787303e518dea806e4816f138e858619.jpg'),
(108, 107, 1, 227524, NULL, NULL, '2014-03-24 00:07:36', '2014-03-24 00:07:36', '9754a0277b0f9ba01f1bfda04611a712a8301060', 'jpg', 'image/jpeg', 'site', 'big-banner', '0,0,2000,430', 'banner-34.jpg', 'data/attachments/module/site/block/big-banner/385/9754a0277b0f9ba01f1bfda04611a712a8301060.jpg'),
(109, NULL, 1, 318916, 1947, 1, '2014-03-24 00:43:48', '2014-03-24 00:43:48', '2789cd514a901c9c29d2aeb9d3b257009aefc6ff', 'jpg', 'image/jpeg', 'site', 'small-banner', '', '2.jpg', 'data/attachments/module/site/block/small-banner/1947/2789cd514a901c9c29d2aeb9d3b257009aefc6ff.jpg'),
(110, 109, 1, 245776, 1947, NULL, '2014-03-24 00:44:30', '2014-03-24 00:43:48', 'c6d9d685d8be24a97b5681963fd9a61a57444f72', 'jpg', 'image/jpeg', 'site', 'small-banner', '3,48,1200,458.40000000000003', '2.jpg', 'data/attachments/module/site/block/small-banner/1947/c6d9d685d8be24a97b5681963fd9a61a57444f72.jpg'),
(111, NULL, 1, 270236, NULL, 1, '2014-03-25 13:26:46', '2014-03-25 13:26:46', 'a5c0c8aa8a1c308a236a00e7f73ed35aaf43358c', 'jpg', 'image/jpeg', 'site', 'big-banner', '', 'banner_22.jpg', 'data/attachments/module/site/block/big-banner/385/a5c0c8aa8a1c308a236a00e7f73ed35aaf43358c.jpg'),
(112, 111, 1, 276453, NULL, NULL, '2014-03-25 13:26:46', '2014-03-25 13:26:46', '2def7500b5e4b9cc63cbd3cb495ae17980227c0c', 'jpg', 'image/jpeg', 'site', 'big-banner', '0,0,2000,430', 'banner_22.jpg', 'data/attachments/module/site/block/big-banner/385/2def7500b5e4b9cc63cbd3cb495ae17980227c0c.jpg'),
(113, NULL, 1, 359243, NULL, 1, '2014-03-25 15:39:07', '2014-03-25 15:39:07', 'a379ae9615501e14e9ff739a6af6add42c8697c3', 'jpg', 'image/jpeg', 'site', 'big-banner', '', 'banner_22q.jpg', 'data/attachments/module/site/block/big-banner/385/a379ae9615501e14e9ff739a6af6add42c8697c3.jpg'),
(114, 113, 1, 369031, NULL, NULL, '2014-03-25 15:39:08', '2014-03-25 15:39:07', '83ba6ea621125812181d31c528a5788675c1bd2f', 'jpg', 'image/jpeg', 'site', 'big-banner', '0,0,2000,430', 'banner_22q.jpg', 'data/attachments/module/site/block/big-banner/385/83ba6ea621125812181d31c528a5788675c1bd2f.jpg'),
(115, NULL, 1, 182782, NULL, 1, '2014-03-25 21:53:15', '2014-03-25 21:53:15', '5b51391979fdfab6d56c4defdf4368698a34078d', 'jpg', 'image/jpeg', 'site', 'big-banner', '', 'banner_23.jpg', 'data/attachments/module/site/block/big-banner/385/5b51391979fdfab6d56c4defdf4368698a34078d.jpg'),
(116, 115, 1, 187831, NULL, NULL, '2014-03-25 21:53:15', '2014-03-25 21:53:15', '0f582913272b4e10825dc8f4d12fa365e2976776', 'jpg', 'image/jpeg', 'site', 'big-banner', '0,0,2000,430', 'banner_23.jpg', 'data/attachments/module/site/block/big-banner/385/0f582913272b4e10825dc8f4d12fa365e2976776.jpg'),
(117, NULL, 1, 227020, NULL, 1, '2014-03-26 10:27:19', '2014-03-26 10:27:19', '496a19077b83cd1b6ed8ce57067217aa17b0b245', 'jpg', 'image/jpeg', 'site', 'big-banner', '', 'banner_23.jpg', 'data/attachments/module/site/block/big-banner/385/496a19077b83cd1b6ed8ce57067217aa17b0b245.jpg'),
(118, 117, 1, 234556, NULL, NULL, '2014-03-26 10:27:19', '2014-03-26 10:27:19', '94e91d7e7595f775cf9bb929ec13c94cdc67038e', 'jpg', 'image/jpeg', 'site', 'big-banner', '0,0,2000,430', 'banner_23.jpg', 'data/attachments/module/site/block/big-banner/385/94e91d7e7595f775cf9bb929ec13c94cdc67038e.jpg'),
(119, NULL, 1, 325518, NULL, 1, '2014-03-26 10:27:36', '2014-03-26 10:27:36', '8304a2e26986d8819097ec462968649c06a4cef2', 'jpg', 'image/jpeg', 'site', 'big-banner', '', 'banner_24.jpg', 'data/attachments/module/site/block/big-banner/385/8304a2e26986d8819097ec462968649c06a4cef2.jpg'),
(120, 119, 1, 334401, NULL, NULL, '2014-03-26 10:27:37', '2014-03-26 10:27:36', 'c3dcf0ae2f15f0635a2ccd3039e51fea2ce70103', 'jpg', 'image/jpeg', 'site', 'big-banner', '0,0,2000,430', 'banner_24.jpg', 'data/attachments/module/site/block/big-banner/385/c3dcf0ae2f15f0635a2ccd3039e51fea2ce70103.jpg'),
(121, NULL, 1, 264645, NULL, 1, '2014-03-26 10:27:53', '2014-03-26 10:27:53', 'ebc936f98534f8b49aeee34dc64d65af67d9761b', 'jpg', 'image/jpeg', 'site', 'big-banner', '', 'banner_25.jpg', 'data/attachments/module/site/block/big-banner/385/ebc936f98534f8b49aeee34dc64d65af67d9761b.jpg'),
(122, 121, 1, 273928, NULL, NULL, '2014-03-26 10:27:53', '2014-03-26 10:27:53', 'ae38f5f15e761513ee5917846b55a79b20bf4cdd', 'jpg', 'image/jpeg', 'site', 'big-banner', '0,0,2000,430', 'banner_25.jpg', 'data/attachments/module/site/block/big-banner/385/ae38f5f15e761513ee5917846b55a79b20bf4cdd.jpg'),
(123, NULL, 1, 313053, NULL, 1, '2014-03-26 10:28:08', '2014-03-26 10:28:08', '1ec7e88a4cd026981eb87adc87223d91dee21152', 'jpg', 'image/jpeg', 'site', 'big-banner', '', 'banner_26.jpg', 'data/attachments/module/site/block/big-banner/385/1ec7e88a4cd026981eb87adc87223d91dee21152.jpg'),
(124, 123, 1, 321338, NULL, NULL, '2014-03-26 10:28:09', '2014-03-26 10:28:08', '75600e2c6c43f56e069523201713156225b8125c', 'jpg', 'image/jpeg', 'site', 'big-banner', '0,0,2000,430', 'banner_26.jpg', 'data/attachments/module/site/block/big-banner/385/75600e2c6c43f56e069523201713156225b8125c.jpg'),
(125, NULL, 1, 251047, NULL, 1, '2014-03-26 10:28:33', '2014-03-26 10:28:33', '5aa116d27176f63f2f9378c62a20b0207fae533b', 'jpg', 'image/jpeg', 'site', 'big-banner', '', 'banner_27.jpg', 'data/attachments/module/site/block/big-banner/385/5aa116d27176f63f2f9378c62a20b0207fae533b.jpg'),
(126, 125, 1, 258484, NULL, NULL, '2014-03-26 10:28:34', '2014-03-26 10:28:33', 'b11825c54d52b09405d448d43b38ad2407a004b0', 'jpg', 'image/jpeg', 'site', 'big-banner', '0,0,2000,430', 'banner_27.jpg', 'data/attachments/module/site/block/big-banner/385/b11825c54d52b09405d448d43b38ad2407a004b0.jpg'),
(127, NULL, 1, 222278, NULL, 1, '2014-03-26 11:44:55', '2014-03-26 11:44:55', 'ebe804ed5e8121f8179ef79a87236aa275d74f14', 'jpg', 'image/jpeg', 'site', 'big-banner', '', 'banner_25.jpg', 'data/attachments/module/site/block/big-banner/385/ebe804ed5e8121f8179ef79a87236aa275d74f14.jpg'),
(128, 127, 1, 229237, NULL, NULL, '2014-03-26 11:44:55', '2014-03-26 11:44:55', '9b6d4136a5d35c5b2590670b3c5b07f083a892b9', 'jpg', 'image/jpeg', 'site', 'big-banner', '0,0,2000,430', 'banner_25.jpg', 'data/attachments/module/site/block/big-banner/385/9b6d4136a5d35c5b2590670b3c5b07f083a892b9.jpg'),
(129, NULL, 1, 312903, NULL, 1, '2014-03-26 13:33:40', '2014-03-26 13:33:40', '54172c52a6dbcba812da3029a17c82c210ea303e', 'jpg', 'image/jpeg', 'site', 'big-banner', '', 'banner_24.jpg', 'data/attachments/module/site/block/big-banner/385/54172c52a6dbcba812da3029a17c82c210ea303e.jpg'),
(130, 129, 1, 321727, NULL, NULL, '2014-03-26 13:33:40', '2014-03-26 13:33:40', 'c85503456d1dc937b1c8873eaf5746a3b689607a', 'jpg', 'image/jpeg', 'site', 'big-banner', '0,0,2000,430', 'banner_24.jpg', 'data/attachments/module/site/block/big-banner/385/c85503456d1dc937b1c8873eaf5746a3b689607a.jpg'),
(131, NULL, 1, 261523, NULL, 1, '2014-03-26 13:35:04', '2014-03-26 13:35:04', '5059e19bf2fac0eac5539d14e7233cd9240923cc', 'jpg', 'image/jpeg', 'site', 'big-banner', '', 'banner_23.jpg', 'data/attachments/module/site/block/big-banner/385/5059e19bf2fac0eac5539d14e7233cd9240923cc.jpg'),
(132, 131, 1, 270301, NULL, NULL, '2014-03-26 13:35:04', '2014-03-26 13:35:04', '55bcb18c6a1f3c4031118c579a08301f90f23298', 'jpg', 'image/jpeg', 'site', 'big-banner', '0,0,2000,430', 'banner_23.jpg', 'data/attachments/module/site/block/big-banner/385/55bcb18c6a1f3c4031118c579a08301f90f23298.jpg'),
(133, NULL, 1, 236436, NULL, 1, '2014-03-26 13:35:36', '2014-03-26 13:35:36', 'cdea42e6a4e3638ebaca2eec8c6a736661511b5e', 'jpg', 'image/jpeg', 'site', 'big-banner', '', 'banner_27.jpg', 'data/attachments/module/site/block/big-banner/385/cdea42e6a4e3638ebaca2eec8c6a736661511b5e.jpg'),
(134, 133, 1, 243751, NULL, NULL, '2014-03-26 13:35:37', '2014-03-26 13:35:36', '95ad614a9ffdb8f024f1aa04e5143c6313b7fd42', 'jpg', 'image/jpeg', 'site', 'big-banner', '0,0,2000,430', 'banner_27.jpg', 'data/attachments/module/site/block/big-banner/385/95ad614a9ffdb8f024f1aa04e5143c6313b7fd42.jpg'),
(135, NULL, 1, 222278, NULL, 1, '2014-03-26 13:38:25', '2014-03-26 13:38:25', 'da00017a86d6559a2c94a539b0e46ed326693a53', 'jpg', 'image/jpeg', 'site', 'big-banner', '', 'banner_25.jpg', 'data/attachments/module/site/block/big-banner/385/da00017a86d6559a2c94a539b0e46ed326693a53.jpg'),
(136, 135, 1, 229237, NULL, NULL, '2014-03-26 13:38:26', '2014-03-26 13:38:25', '89a498b18859140432a3f76b73a02cb98f88d4be', 'jpg', 'image/jpeg', 'site', 'big-banner', '0,0,2000,430', 'banner_25.jpg', 'data/attachments/module/site/block/big-banner/385/89a498b18859140432a3f76b73a02cb98f88d4be.jpg'),
(137, NULL, 1, 336818, NULL, 1, '2014-03-26 13:39:14', '2014-03-26 13:39:14', 'b77415f99c87066267d1c1ccf419908f559a927f', 'jpg', 'image/jpeg', 'site', 'big-banner', '', 'banner_22.jpg', 'data/attachments/module/site/block/big-banner/385/b77415f99c87066267d1c1ccf419908f559a927f.jpg'),
(138, 137, 1, 345690, NULL, NULL, '2014-03-26 13:39:14', '2014-03-26 13:39:14', 'a59ab56f3e9110131e4d9134c4413acb8ecb22eb', 'jpg', 'image/jpeg', 'site', 'big-banner', '0,0,2000,430', 'banner_22.jpg', 'data/attachments/module/site/block/big-banner/385/a59ab56f3e9110131e4d9134c4413acb8ecb22eb.jpg'),
(139, NULL, 1, 307790, NULL, 1, '2014-03-26 13:39:50', '2014-03-26 13:39:50', 'cef8dcedb5b53927716aabd450daa96733bb2bd6', 'jpg', 'image/jpeg', 'site', 'big-banner', '', 'banner_26.jpg', 'data/attachments/module/site/block/big-banner/385/cef8dcedb5b53927716aabd450daa96733bb2bd6.jpg'),
(140, 139, 1, 317208, NULL, NULL, '2014-03-26 13:39:50', '2014-03-26 13:39:50', '81c80d4ae4eadb7407246fd296eac6e647649381', 'jpg', 'image/jpeg', 'site', 'big-banner', '0,0,2000,430', 'banner_26.jpg', 'data/attachments/module/site/block/big-banner/385/81c80d4ae4eadb7407246fd296eac6e647649381.jpg'),
(141, NULL, 1, 618863, NULL, 1, '2014-03-27 12:43:43', '2014-03-27 12:43:43', '6365d6139b295424ae2182066d3965be50732741', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'u352.jpg', 'data/attachments/module/site/block/image-slider/2197/6365d6139b295424ae2182066d3965be50732741.jpg'),
(142, 141, 1, 516265, NULL, NULL, '2014-03-27 12:44:06', '2014-03-27 12:43:43', '5c012302822db70abb6c750686535b29ebf89c4f', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,973.1875,677', 'u352.jpg', 'data/attachments/module/site/block/image-slider/2197/5c012302822db70abb6c750686535b29ebf89c4f.jpg'),
(143, NULL, 1, 407287, NULL, 1, '2014-03-27 12:44:21', '2014-03-27 12:44:21', '556073dbdcdc75192501351f3719ffbb852140c4', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'u371.jpg', 'data/attachments/module/site/block/image-slider/2197/556073dbdcdc75192501351f3719ffbb852140c4.jpg'),
(144, 143, 1, 408623, NULL, NULL, '2014-03-27 12:44:38', '2014-03-27 12:44:21', '6def5ad4d1646115fb8d0a0a5b2ad91c3880ae57', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,1157.1875,805', 'u371.jpg', 'data/attachments/module/site/block/image-slider/2197/6def5ad4d1646115fb8d0a0a5b2ad91c3880ae57.jpg'),
(145, NULL, 1, 470397, NULL, 1, '2014-03-27 12:45:24', '2014-03-27 12:45:24', '7489ec342258e28e68603c70d0bf77481ce04cf5', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'biznes foto.jpg', 'data/attachments/module/site/block/image-slider/2197/7489ec342258e28e68603c70d0bf77481ce04cf5.jpg'),
(146, 145, 1, 418306, NULL, NULL, '2014-03-27 12:45:49', '2014-03-27 12:45:24', '2d49d4c96c5853b3eb42073d1259781b16efe2e7', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,3,1200,837.7826086956521', 'biznes foto.jpg', 'data/attachments/module/site/block/image-slider/2197/2d49d4c96c5853b3eb42073d1259781b16efe2e7.jpg'),
(147, NULL, 1, 992908, NULL, 1, '2014-03-27 12:46:23', '2014-03-27 12:46:23', '167bc895fb08656521ac0fd6d126ddba9fb6bdea', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'Fotolia_52895963_M.jpg', 'data/attachments/module/site/block/image-slider/2197/167bc895fb08656521ac0fd6d126ddba9fb6bdea.jpg'),
(148, 147, 1, 961265, NULL, NULL, '2014-03-27 12:47:00', '2014-03-27 12:46:23', 'c3d9aabaf133559c90ea5e4559f421c5ea70b25c', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,1148.5625,799', 'Fotolia_52895963_M.jpg', 'data/attachments/module/site/block/image-slider/2197/c3d9aabaf133559c90ea5e4559f421c5ea70b25c.jpg'),
(149, NULL, 1, 241239, NULL, 1, '2014-03-27 14:27:35', '2014-03-27 14:27:35', '981340bc5825483351203f12d329521a83c5eb34', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'acg.jpg', 'data/attachments/module/site/block/image-slider/2198/981340bc5825483351203f12d329521a83c5eb34.jpg'),
(150, NULL, 1, 170036, NULL, 1, '2014-03-27 14:28:33', '2014-03-27 14:28:33', 'b9397534510f6035752a900c20d243af446c6a81', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'images (1).jpg', 'data/attachments/module/site/block/image-slider/2198/b9397534510f6035752a900c20d243af446c6a81.jpg'),
(151, NULL, 1, 238135, NULL, 1, '2014-03-27 14:30:00', '2014-03-27 14:30:00', '61947fd3da8fa28abed46dc21e51f5011c5d339b', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'images.jpg', 'data/attachments/module/site/block/image-slider/2198/61947fd3da8fa28abed46dc21e51f5011c5d339b.jpg'),
(152, 151, 1, 49370, NULL, NULL, '2014-03-27 14:30:18', '2014-03-27 14:30:00', '61e2d1a9518d1f642579d3a9ffc85f8e0e835a70', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,336.375,234', 'images.jpg', 'data/attachments/module/site/block/image-slider/2198/61e2d1a9518d1f642579d3a9ffc85f8e0e835a70.jpg'),
(153, NULL, 1, 198893, NULL, 1, '2014-03-27 16:40:17', '2014-03-27 16:40:17', 'b53dbd681aac35fc5cdbe1a28fdd8fd13c4f86d1', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo.jpg', 'data/attachments/module/site/block/image-slider/2198/b53dbd681aac35fc5cdbe1a28fdd8fd13c4f86d1.jpg'),
(154, NULL, 1, 111579, NULL, 1, '2014-03-28 01:17:19', '2014-03-28 01:17:19', '5261c326d614d8f0a28e3458c8d3afb3bcee807a', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'plus-logo-pan.jpg', 'data/attachments/module/site/block/image-slider/2198/5261c326d614d8f0a28e3458c8d3afb3bcee807a.jpg'),
(155, NULL, 1, 206367, NULL, 1, '2014-03-28 01:22:40', '2014-03-28 01:22:40', 'eb26dab05563d199f9c319d10e9f59391747b430', 'jpg', 'image/jpeg', 'site', 'news-list', '', '2014-03-24_220231.jpg', 'data/attachments/module/site/block/news-list/1445/eb26dab05563d199f9c319d10e9f59391747b430.jpg'),
(156, 155, 1, 100553, NULL, NULL, '2014-03-28 01:27:22', '2014-03-28 01:22:40', '5aace31bddbf4088258915a1fe743dbf5ce75e0c', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,572.125,398', '2014-03-24_220231.jpg', 'data/attachments/module/site/block/news-list/1445/5aace31bddbf4088258915a1fe743dbf5ce75e0c.jpg'),
(157, NULL, 1, 198893, NULL, 1, '2014-03-28 01:27:35', '2014-03-28 01:27:35', 'f6e5406d725d65b455fed62a560f0cf1a0b0db66', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'logo.jpg', 'data/attachments/module/site/block/news-list/1445/f6e5406d725d65b455fed62a560f0cf1a0b0db66.jpg'),
(158, NULL, 1, 310567, NULL, 1, '2014-03-28 01:30:50', '2014-03-28 01:30:50', '2acfcd37f1534c72b1e93d5743e96060009d4158', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'world-jigsaw.jpg', 'data/attachments/module/site/block/news-list/1445/2acfcd37f1534c72b1e93d5743e96060009d4158.jpg'),
(159, NULL, 1, 202872, NULL, 1, '2014-03-28 01:39:10', '2014-03-28 01:39:10', 'af28010af80de230038a4324ef10aa5bdf8c62fa', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'Intelektualny.jpg', 'data/attachments/module/site/block/news-list/1445/af28010af80de230038a4324ef10aa5bdf8c62fa.jpg'),
(160, NULL, 1, 252193, NULL, 1, '2014-03-28 01:46:30', '2014-03-28 01:46:30', '613c72d226a681aba1eac7bc50bcb94c0e6a68b9', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'JMrowiec.jpg', 'data/attachments/module/site/block/news-list/1445/613c72d226a681aba1eac7bc50bcb94c0e6a68b9.jpg'),
(161, NULL, 1, 252193, NULL, 1, '2014-03-28 01:49:56', '2014-03-28 01:49:56', '3b631fdb0a100e814256b2ea0a950136ba49396f', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'JMrowiec.jpg', 'data/attachments/module/site/block/news-list/1445/3b631fdb0a100e814256b2ea0a950136ba49396f.jpg'),
(162, NULL, 1, 591942, NULL, 1, '2014-03-28 01:53:26', '2014-03-28 01:53:26', '6367f1982922b1fd4d412f11bdd196ac5708c560', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'foto9.jpg', 'data/attachments/module/site/block/news-list/1445/6367f1982922b1fd4d412f11bdd196ac5708c560.jpg'),
(163, NULL, 1, 372762, NULL, 1, '2014-03-28 01:57:25', '2014-03-28 01:57:25', 'd8388be79db81985e19e3b468f0f46019ed63ce7', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'Informatycy AFIB w Turcji.jpg', 'data/attachments/module/site/block/news-list/1445/d8388be79db81985e19e3b468f0f46019ed63ce7.jpg'),
(164, NULL, 1, 383911, NULL, 1, '2014-03-28 02:00:41', '2014-03-28 02:00:41', 'edc3c71d829ea8814f2789ea0d17339f80253ce8', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'Studenci AFiBV w Turcji.jpg', 'data/attachments/module/site/block/news-list/1445/edc3c71d829ea8814f2789ea0d17339f80253ce8.jpg'),
(165, NULL, 1, 225474, NULL, 1, '2014-03-28 02:05:33', '2014-03-28 02:05:33', 'bab67f9141181c28ea18593af1cbefe1d49fd6db', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'Konkurs matematyczny.jpg', 'data/attachments/module/site/block/news-list/1445/bab67f9141181c28ea18593af1cbefe1d49fd6db.jpg'),
(166, NULL, 1, 268416, NULL, 1, '2014-03-28 02:26:18', '2014-03-28 02:26:18', '9aecace4b3acfa39691139e835155f1ceb103e9d', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'małe i duże firmy.jpg', 'data/attachments/module/site/block/news-list/1445/9aecace4b3acfa39691139e835155f1ceb103e9d.jpg'),
(167, NULL, 1, 291298, NULL, 1, '2014-03-28 02:35:19', '2014-03-28 02:35:19', '914aeb12e91a406b7262fc5b33819c6f0190b784', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'Konkurs fotograficzny.jpg', 'data/attachments/module/site/block/news-list/1445/914aeb12e91a406b7262fc5b33819c6f0190b784.jpg');
INSERT INTO `attachments` (`attachment_id`, `parent_attachment_id`, `is_published`, `file_size`, `page_block_id`, `last_update_author_id`, `last_update_date`, `creation_date`, `hash_for_file_name`, `extension`, `mime_type`, `module_name`, `block_name`, `image_crop_rect_coords`, `original_file_name`, `file_path`) VALUES
(168, NULL, 1, 116070, NULL, 1, '2014-03-29 21:05:43', '2014-03-29 21:05:43', '96a18055f39646354b3e3de86eebd29c2d05241a', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-acg.jpg', 'data/attachments/module/site/block/image-slider/2198/96a18055f39646354b3e3de86eebd29c2d05241a.jpg'),
(169, NULL, 1, 83043, NULL, 1, '2014-03-29 21:06:16', '2014-03-29 21:06:16', 'c3c352c0f710d2b10e74af81208fd77317635ddb', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-cbre.jpg', 'data/attachments/module/site/block/image-slider/2198/c3c352c0f710d2b10e74af81208fd77317635ddb.jpg'),
(170, NULL, 1, 109247, NULL, 1, '2014-03-29 21:06:30', '2014-03-29 21:06:30', '287ee0272904e57e86797b183b605e2371da4bf9', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-cordlime.jpg', 'data/attachments/module/site/block/image-slider/2198/287ee0272904e57e86797b183b605e2371da4bf9.jpg'),
(171, NULL, 1, 117912, NULL, 1, '2014-03-29 21:06:54', '2014-03-29 21:06:54', '97d0f6793e2867ea8569ffa1392ab5ca6bcb8dee', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-infopraca.jpg', 'data/attachments/module/site/block/image-slider/2198/97d0f6793e2867ea8569ffa1392ab5ca6bcb8dee.jpg'),
(172, NULL, 1, 120149, NULL, 1, '2014-03-29 21:08:13', '2014-03-29 21:08:13', '42d6a4d3deff4c1f344c6abc05e8979dc75ed565', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-mpi.jpg', 'data/attachments/module/site/block/image-slider/2198/42d6a4d3deff4c1f344c6abc05e8979dc75ed565.jpg'),
(173, NULL, 1, 218544, NULL, 1, '2014-03-29 21:08:31', '2014-03-29 21:08:31', 'c2afaf5801b960223414df19fcd53ec04d7cd833', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-polsat_cyfrowy.jpg', 'data/attachments/module/site/block/image-slider/2198/c2afaf5801b960223414df19fcd53ec04d7cd833.jpg'),
(174, NULL, 1, 182243, NULL, 1, '2014-03-29 21:08:59', '2014-03-29 21:08:59', '5aa792a82a87155169278b74c6ade358a5544d77', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-radwag.jpg', 'data/attachments/module/site/block/image-slider/2198/5aa792a82a87155169278b74c6ade358a5544d77.jpg'),
(175, NULL, 1, 142286, NULL, 1, '2014-03-29 21:09:57', '2014-03-29 21:09:57', 'a11b6dd3581c15d91eba28ecb0c0b6665e5b3c6b', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-wzbk.jpg', 'data/attachments/module/site/block/image-slider/2198/a11b6dd3581c15d91eba28ecb0c0b6665e5b3c6b.jpg'),
(176, NULL, 1, 124382, NULL, 1, '2014-03-29 21:10:21', '2014-03-29 21:10:21', '0f0477e3d5c188425ad5a82c5d322228786fe288', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-mg.jpg', 'data/attachments/module/site/block/image-slider/2198/0f0477e3d5c188425ad5a82c5d322228786fe288.jpg'),
(177, 176, 1, 119421, NULL, NULL, '2014-04-09 10:48:07', '2014-03-29 21:10:21', 'e202d7bbb9609ca81edf64211e47d62450626f46', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,82,1200,748.6666666666666', 'logo-mg.jpg', 'data/attachments/module/site/block/image-slider/2198/e202d7bbb9609ca81edf64211e47d62450626f46.jpg'),
(178, NULL, 1, 300650, NULL, 1, '2014-03-29 21:10:58', '2014-03-29 21:10:58', 'c8c925176f9ba3dbac280d519332686942f7e977', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-alior.jpg', 'data/attachments/module/site/block/image-slider/2198/c8c925176f9ba3dbac280d519332686942f7e977.jpg'),
(179, NULL, 1, 473870, NULL, 1, '2014-03-29 21:16:03', '2014-03-29 21:16:03', '34800b5888163d0734e25d161d14082b32716f98', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'Fotolia_59113641_M.jpg', 'data/attachments/module/site/block/news-list/1445/34800b5888163d0734e25d161d14082b32716f98.jpg'),
(180, 179, 1, 464611, NULL, NULL, '2014-03-29 21:16:37', '2014-03-29 21:16:03', '4cec57f9f88b569859aacbe45a23cfda7d2cd92e', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,1148.5625,799', 'Fotolia_59113641_M.jpg', 'data/attachments/module/site/block/news-list/1445/4cec57f9f88b569859aacbe45a23cfda7d2cd92e.jpg'),
(181, NULL, 1, 291298, NULL, 1, '2014-03-30 11:38:16', '2014-03-30 11:38:16', 'dcfb2c722d73803536116a96452edacd576c637d', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'Konkurs fotograficzny.jpg', 'data/attachments/module/site/block/news-list/1445/dcfb2c722d73803536116a96452edacd576c637d.jpg'),
(182, NULL, 1, 252193, 2187, 1, '2014-03-31 16:12:17', '2014-03-31 16:12:17', '139608142943e7118b3e8772cdcc75d7fba98ebb', 'jpg', 'image/jpeg', 'site', 'event-list', '', 'JMrowiec.jpg', 'data/attachments/module/site/block/event-list/2187/139608142943e7118b3e8772cdcc75d7fba98ebb.jpg'),
(183, NULL, 1, 202872, 2187, 1, '2014-03-31 17:08:40', '2014-03-31 17:08:40', 'bae98eb33a803d84d8db3c899dbeb1ba27ee2ab4', 'jpg', 'image/jpeg', 'site', 'event-list', '', 'Intelektualny.jpg', 'data/attachments/module/site/block/event-list/2187/bae98eb33a803d84d8db3c899dbeb1ba27ee2ab4.jpg'),
(184, NULL, 1, 435101, 2187, 1, '2014-03-31 17:13:35', '2014-03-31 17:13:35', 'e291d9ca1645016fb8f2933b2421e2925382e4d5', 'jpg', 'image/jpeg', 'site', 'event-list', '', 'Konferencja Dekada Gierka.jpg', 'data/attachments/module/site/block/event-list/2187/e291d9ca1645016fb8f2933b2421e2925382e4d5.jpg'),
(185, NULL, 1, 320642, 2187, 1, '2014-04-01 00:22:50', '2014-04-01 00:22:50', 'e5e95d61a4dc79121c85203433655220b26de7b1', 'jpg', 'image/jpeg', 'site', 'event-list', '', 'partnerstwo.jpg', 'data/attachments/module/site/block/event-list/2187/e5e95d61a4dc79121c85203433655220b26de7b1.jpg'),
(186, NULL, 1, 195831, NULL, 1, '2014-04-01 00:26:08', '2014-04-01 00:26:08', '8a19f232d527fce68d7857960f7ca8d8aa445e2f', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'ncn.jpg', 'data/attachments/module/site/block/news-list/1445/8a19f232d527fce68d7857960f7ca8d8aa445e2f.jpg'),
(187, NULL, 1, 38015, NULL, 1, '2014-04-02 14:25:04', '2014-04-02 14:25:04', '63055ba10b7220643afef626cb8c3f0cc065deaa', 'png', 'image/png', 'site', 'image-slider', '', 'slider-logo1.png', 'data/attachments/module/site/block/image-slider/2198/63055ba10b7220643afef626cb8c3f0cc065deaa.png'),
(188, NULL, 1, 64405, NULL, 1, '2014-04-03 15:08:45', '2014-04-03 15:08:45', '0692f752983972092c43008495d9bab0f2833cdf', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'NCN.jpg', 'data/attachments/module/site/block/news-list/1445/0692f752983972092c43008495d9bab0f2833cdf.jpg'),
(189, NULL, 1, 167125, NULL, 1, '2014-04-03 15:14:53', '2014-04-03 15:14:53', '17dacaecf189f183e704e06d35684b2dab9c51a4', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'NCN.jpg', 'data/attachments/module/site/block/news-list/1445/17dacaecf189f183e704e06d35684b2dab9c51a4.jpg'),
(190, NULL, 1, 322760, 1546, 1, '2014-04-04 00:05:39', '2014-04-04 00:05:39', '905baa78ca5d3417b82b46b9cfb36566040038b4', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'PR7_7870_stitch (Kopiowanie).jpg', 'data/attachments/module/site/block/small-banner/1546/905baa78ca5d3417b82b46b9cfb36566040038b4.jpg'),
(191, 190, 1, 171492, 1546, NULL, '2014-04-04 00:05:39', '2014-04-04 00:05:39', '803ecae7659acf9c0aeed47bbc60baf8aeddc462', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,875,300', 'PR7_7870_stitch (Kopiowanie).jpg', 'data/attachments/module/site/block/small-banner/1546/803ecae7659acf9c0aeed47bbc60baf8aeddc462.jpg'),
(192, NULL, 1, 391780, NULL, 1, '2014-04-07 13:30:09', '2014-04-07 13:30:09', '147b2c5914c6fea87648d316f6587030b2203fe2', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'JOB_SPOT_poster.jpg', 'data/attachments/module/site/block/news-list/1445/147b2c5914c6fea87648d316f6587030b2203fe2.jpg'),
(193, NULL, 1, 212032, NULL, 1, '2014-04-07 13:31:10', '2014-04-07 13:31:10', '2fe6ee895463b14b2228d9f03dc7c94b047eb37a', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'Biuro Karier AFiB Vistula.jpg', 'data/attachments/module/site/block/news-list/1445/2fe6ee895463b14b2228d9f03dc7c94b047eb37a.jpg'),
(194, NULL, 1, 353456, NULL, 1, '2014-04-07 13:40:32', '2014-04-07 13:40:32', '5eb9a4e00ac1ae68aa8807713236e92b66ea95be', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'job.jpg', 'data/attachments/module/site/block/news-list/1445/5eb9a4e00ac1ae68aa8807713236e92b66ea95be.jpg'),
(195, NULL, 1, 426407, 2306, 1, '2014-04-07 22:54:55', '2014-04-07 22:54:55', 'edd41f0c00613e39243d1124ccdfeed9899542bf', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'Vistula_part1(21) (Small).jpg', 'data/attachments/module/site/block/small-banner/2306/edd41f0c00613e39243d1124ccdfeed9899542bf.jpg'),
(196, 195, 1, 118843, 2306, NULL, '2014-04-07 22:54:56', '2014-04-07 22:54:55', '7667dfa98f35407f3ae9e8f8bd99181c7ee97997', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,875,300', 'Vistula_part1(21) (Small).jpg', 'data/attachments/module/site/block/small-banner/2306/7667dfa98f35407f3ae9e8f8bd99181c7ee97997.jpg'),
(197, NULL, 1, 717346, 2313, 1, '2014-04-08 10:56:13', '2014-04-08 10:56:13', '676b83d2f3c2d12f60e71083b260d736ec8336c7', 'jpg', 'image/jpeg', 'site', 'image-slider', '', '954381_58369817.jpg', 'data/attachments/module/site/block/image-slider/2313/676b83d2f3c2d12f60e71083b260d736ec8336c7.jpg'),
(198, NULL, 1, 407597, NULL, 1, '2014-04-08 13:50:25', '2014-04-08 13:50:25', 'b6f2ca87d45aec9126b4816d03fd74390246579e', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'PR7_7862_stitch.jpg', 'data/attachments/module/site/block/image-slider/2309/b6f2ca87d45aec9126b4816d03fd74390246579e.jpg'),
(199, NULL, 1, 552561, NULL, 1, '2014-04-08 13:51:57', '2014-04-08 13:51:57', '009a3d515de80dc4b98f6b54783aa5a9e90d4716', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'Obraz1.jpg', 'data/attachments/module/site/block/image-slider/2309/009a3d515de80dc4b98f6b54783aa5a9e90d4716.jpg'),
(200, NULL, 1, 742429, NULL, 1, '2014-04-08 13:52:21', '2014-04-08 13:52:21', 'b4a0f8d6236bb3ad5587c2c3a4785c99d7d373a3', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'Budynek_1.JPG', 'data/attachments/module/site/block/image-slider/2309/b4a0f8d6236bb3ad5587c2c3a4785c99d7d373a3.jpg'),
(201, NULL, 1, 550644, NULL, 1, '2014-04-08 13:53:27', '2014-04-08 13:53:27', '859510852a9500f0ade0c6d534726e46931fcb2b', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'Obraz4.jpg', 'data/attachments/module/site/block/image-slider/2309/859510852a9500f0ade0c6d534726e46931fcb2b.jpg'),
(202, NULL, 1, 793330, NULL, 1, '2014-04-08 13:54:47', '2014-04-08 13:54:47', 'cc2ba5c2fb7f5fef2d2192897f9b2ccbf6cb478d', 'jpg', 'image/jpeg', 'site', 'image-slider', '', '0 (209).jpg', 'data/attachments/module/site/block/image-slider/2309/cc2ba5c2fb7f5fef2d2192897f9b2ccbf6cb478d.jpg'),
(203, NULL, 1, 447416, NULL, 1, '2014-04-08 13:55:24', '2014-04-08 13:55:24', '44d6dba63aa8a06139e05b443fab35bd78233a46', 'jpg', 'image/jpeg', 'site', 'image-slider', '', '0 (320) (Small).jpg', 'data/attachments/module/site/block/image-slider/2309/44d6dba63aa8a06139e05b443fab35bd78233a46.jpg'),
(204, NULL, 1, 619429, NULL, 1, '2014-04-08 13:55:58', '2014-04-08 13:55:58', '4447d6ccbb39b6acfb4021e154a9bcfe559af471', 'jpg', 'image/jpeg', 'site', 'image-slider', '', '0 (320).jpg', 'data/attachments/module/site/block/image-slider/2309/4447d6ccbb39b6acfb4021e154a9bcfe559af471.jpg'),
(205, NULL, 1, 447416, NULL, 1, '2014-04-08 13:56:21', '2014-04-08 13:56:21', 'f365bac4b9278b3f1d800991e9f4f69326229e41', 'jpg', 'image/jpeg', 'site', 'image-slider', '', '0 (320) (Small).jpg', 'data/attachments/module/site/block/image-slider/2309/f365bac4b9278b3f1d800991e9f4f69326229e41.jpg'),
(206, NULL, 1, 452859, NULL, 1, '2014-04-08 13:57:40', '2014-04-08 13:57:40', 'e188362d3070d9f5efc074f240a67b14267c711f', 'jpg', 'image/jpeg', 'site', 'image-slider', '', '0 (302) (Small).jpg', 'data/attachments/module/site/block/image-slider/2309/e188362d3070d9f5efc074f240a67b14267c711f.jpg'),
(207, NULL, 1, 722688, NULL, 1, '2014-04-08 13:59:02', '2014-04-08 13:59:02', 'd5db57366e9e1d101ef2f4956b7b10206b6d2f4d', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'IMG_0864.jpg', 'data/attachments/module/site/block/image-slider/2309/d5db57366e9e1d101ef2f4956b7b10206b6d2f4d.jpg'),
(208, NULL, 1, 749700, 2323, 1, '2014-04-08 17:37:39', '2014-04-08 17:37:39', '79966c8d2a36b0908254c415bc4b09e5cf5bacad', 'jpg', 'image/jpeg', 'site', 'image-slider', '', '_DSC6513 copy.jpg', 'data/attachments/module/site/block/image-slider/2323/79966c8d2a36b0908254c415bc4b09e5cf5bacad.jpg'),
(209, NULL, 1, 676441, 2323, 1, '2014-04-08 17:39:41', '2014-04-08 17:39:41', 'fe2329be53b8a223066c06672219e96ee33be6ea', 'jpg', 'image/jpeg', 'site', 'image-slider', '', '_DSC6586 copy.jpg', 'data/attachments/module/site/block/image-slider/2323/fe2329be53b8a223066c06672219e96ee33be6ea.jpg'),
(210, NULL, 1, 750734, 2323, 1, '2014-04-08 17:40:22', '2014-04-08 17:40:22', '4c04cdfabfa02e2564a78f29124377985ba3b793', 'jpg', 'image/jpeg', 'site', 'image-slider', '', '_DSC6218 copy.jpg', 'data/attachments/module/site/block/image-slider/2323/4c04cdfabfa02e2564a78f29124377985ba3b793.jpg'),
(211, NULL, 1, 660119, 2323, 1, '2014-04-08 17:41:09', '2014-04-08 17:41:09', '6074b3a722afd292425eece1e97c18efa8673bee', 'jpg', 'image/jpeg', 'site', 'image-slider', '', '_DSC6343 copy.jpg', 'data/attachments/module/site/block/image-slider/2323/6074b3a722afd292425eece1e97c18efa8673bee.jpg'),
(212, NULL, 1, 749700, 2323, 1, '2014-04-08 17:43:06', '2014-04-08 17:43:06', '87a42ce9b77ec4425788c89439055a04037ad405', 'jpg', 'image/jpeg', 'site', 'image-slider', '', '_DSC6513 copy.jpg', 'data/attachments/module/site/block/image-slider/2323/87a42ce9b77ec4425788c89439055a04037ad405.jpg'),
(213, NULL, 1, 671567, 2323, 1, '2014-04-08 17:43:43', '2014-04-08 17:43:43', '841402bf9920968b9bcbebeb04e1e4c9e6143636', 'jpg', 'image/jpeg', 'site', 'image-slider', '', '_DSC6373 copy.jpg', 'data/attachments/module/site/block/image-slider/2323/841402bf9920968b9bcbebeb04e1e4c9e6143636.jpg'),
(214, NULL, 1, 697285, 2323, 1, '2014-04-08 17:44:23', '2014-04-08 17:44:23', 'b847e7d1a97fc088c7e6cf9c765daa2dfd4d42d9', 'jpg', 'image/jpeg', 'site', 'image-slider', '', '_DSC6424 copy.jpg', 'data/attachments/module/site/block/image-slider/2323/b847e7d1a97fc088c7e6cf9c765daa2dfd4d42d9.jpg'),
(215, NULL, 1, 723810, 2323, 1, '2014-04-08 17:45:20', '2014-04-08 17:45:20', '3a2dde33b38bbaa900676cfc54712343b42106c1', 'jpg', 'image/jpeg', 'site', 'image-slider', '', '_DSC6195 copy.jpg', 'data/attachments/module/site/block/image-slider/2323/3a2dde33b38bbaa900676cfc54712343b42106c1.jpg'),
(216, NULL, 1, 656189, 2323, 1, '2014-04-08 17:45:47', '2014-04-08 17:45:47', '2fb2ffe1d436670f7641b8c15140269329bc89a9', 'jpg', 'image/jpeg', 'site', 'image-slider', '', '_DSC6421 copy.jpg', 'data/attachments/module/site/block/image-slider/2323/2fb2ffe1d436670f7641b8c15140269329bc89a9.jpg'),
(217, NULL, 1, 77120, NULL, 1, '2014-04-09 10:19:05', '2014-04-09 10:19:05', '593dfdb6c209d0dd7c84af5dd58aa77c830ff52e', 'png', 'image/png', 'site', 'event-list', '', 'aktualnosci-bruksela_3_dni-171-214-800-600.png', 'data/attachments/module/site/block/event-list/1444/593dfdb6c209d0dd7c84af5dd58aa77c830ff52e.png'),
(218, 217, 1, 72164, NULL, NULL, '2014-04-09 10:19:05', '2014-04-09 10:19:05', '1fe808d41dfb915089cd2617bd4d08612a8c3d31', 'png', 'image/png', 'site', 'event-list', '0,0,230,160', 'aktualnosci-bruksela_3_dni-171-214-800-600.png', 'data/attachments/module/site/block/event-list/1444/1fe808d41dfb915089cd2617bd4d08612a8c3d31.png'),
(219, NULL, 1, 92034, NULL, 1, '2014-04-09 10:29:18', '2014-04-09 10:29:18', '8223b3e1cfe1dbe47dfe13153768747549f7f33e', 'png', 'image/png', 'site', 'images-pile', '', 'aktualnosci-bruksela_3_dni-171-215-800-600.png', 'data/attachments/module/site/block/images-pile/2333/8223b3e1cfe1dbe47dfe13153768747549f7f33e.png'),
(220, 219, 1, 92034, NULL, NULL, '2014-04-09 10:29:18', '2014-04-09 10:29:18', '58b1837d08bf93f714bd298d65d69e806fe24dbc', 'png', 'image/png', 'site', 'images-pile', '0,0,250,188', 'aktualnosci-bruksela_3_dni-171-215-800-600.png', 'data/attachments/module/site/block/images-pile/2333/58b1837d08bf93f714bd298d65d69e806fe24dbc.png'),
(221, NULL, 1, 32431, NULL, 1, '2014-04-09 10:29:57', '2014-04-09 10:29:57', '79c0bf508db0201e2467ec3df53095aeee3bddb2', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'Fotolia_41002280_X.jpg', 'data/attachments/module/site/block/images-pile/2333/79c0bf508db0201e2467ec3df53095aeee3bddb2.jpg'),
(222, 221, 1, 29271, NULL, NULL, '2014-04-09 10:29:57', '2014-04-09 10:29:57', '65b895b8507a50aaff24d0cededf0cfecfe63b37', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'Fotolia_41002280_X.jpg', 'data/attachments/module/site/block/images-pile/2333/65b895b8507a50aaff24d0cededf0cfecfe63b37.jpg'),
(223, NULL, 1, 44905, NULL, 1, '2014-04-09 10:31:07', '2014-04-09 10:31:07', 'a84096b3f38a885a807c59f5794432f8a7453b8d', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'u371.jpg', 'data/attachments/module/site/block/bachelor-studies/1993/a84096b3f38a885a807c59f5794432f8a7453b8d.jpg'),
(224, 223, 1, 28682, NULL, NULL, '2014-04-09 10:31:07', '2014-04-09 10:31:07', '5866b3903a858ac38e50e8af3d4ce65104b9c81e', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'u371.jpg', 'data/attachments/module/site/block/bachelor-studies/1993/5866b3903a858ac38e50e8af3d4ce65104b9c81e.jpg'),
(225, 168, 1, 107103, NULL, NULL, '2014-04-09 10:46:54', '2014-03-29 21:05:43', '1c8c15ab27da90d85d32964c6ee7be9c80a95064', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,64,1200,730.6666666666666', 'logo-acg.jpg', 'data/attachments/module/site/block/image-slider/2198/1c8c15ab27da90d85d32964c6ee7be9c80a95064.jpg'),
(226, 178, 1, 301028, NULL, NULL, '2014-04-09 10:59:43', '2014-03-29 21:10:58', '223aed5b6df65daa150596401cbb54d4fa9aca7f', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,1200,666.6666666666666', 'logo-alior.jpg', 'data/attachments/module/site/block/image-slider/2198/223aed5b6df65daa150596401cbb54d4fa9aca7f.jpg'),
(227, 169, 1, 75552, NULL, NULL, '2014-04-09 10:47:24', '2014-03-29 21:06:16', '61b4ab1c73ca91c9bc302b4fce6c694145bdcd0c', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,100,1200,766.6666666666666', 'logo-cbre.jpg', 'data/attachments/module/site/block/image-slider/2198/61b4ab1c73ca91c9bc302b4fce6c694145bdcd0c.jpg'),
(228, 170, 1, 103667, NULL, NULL, '2014-04-09 10:47:39', '2014-03-29 21:06:30', 'edf9defcaf14024a57296b6f1fd45c327b6a7fa7', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,76,1200,742.6666666666666', 'logo-cordlime.jpg', 'data/attachments/module/site/block/image-slider/2198/edf9defcaf14024a57296b6f1fd45c327b6a7fa7.jpg'),
(229, 171, 1, 112330, NULL, NULL, '2014-04-09 10:47:55', '2014-03-29 21:06:54', '1923e42329d5b4deee3b1cd93371aeff057f219f', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,67,1200,733.6666666666666', 'logo-infopraca.jpg', 'data/attachments/module/site/block/image-slider/2198/1923e42329d5b4deee3b1cd93371aeff057f219f.jpg'),
(230, 172, 1, 112960, NULL, NULL, '2014-04-09 10:48:22', '2014-03-29 21:08:13', 'cb2b7ff82b59772c731e22f6ebe2e274ecaf26bc', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,79,1200,745.6666666666666', 'logo-mpi.jpg', 'data/attachments/module/site/block/image-slider/2198/cb2b7ff82b59772c731e22f6ebe2e274ecaf26bc.jpg'),
(231, 173, 1, 214942, NULL, NULL, '2014-04-09 10:48:36', '2014-03-29 21:08:31', '2b31bcda04dd69fd54cb9b8f9007cf4af1211c26', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,76,1200,742.6666666666666', 'logo-polsat_cyfrowy.jpg', 'data/attachments/module/site/block/image-slider/2198/2b31bcda04dd69fd54cb9b8f9007cf4af1211c26.jpg'),
(232, 174, 1, 178890, NULL, NULL, '2014-04-09 10:48:51', '2014-03-29 21:08:59', 'cac90f8d99d88958cf412571d117a24c8ce063ff', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,69,1200,735.6666666666666', 'logo-radwag.jpg', 'data/attachments/module/site/block/image-slider/2198/cac90f8d99d88958cf412571d117a24c8ce063ff.jpg'),
(233, 175, 1, 134976, NULL, NULL, '2014-04-09 10:49:06', '2014-03-29 21:09:57', '6ae4fd02d0acf99494f42fbdea4fb9d15ec8d91a', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,72,1200,738.6666666666666', 'logo-wzbk.jpg', 'data/attachments/module/site/block/image-slider/2198/6ae4fd02d0acf99494f42fbdea4fb9d15ec8d91a.jpg'),
(234, NULL, 1, 7966, NULL, 1, '2014-04-09 10:58:38', '2014-04-09 10:58:38', '0dfbfb8345c84ae4054c93334d114f445cd74811', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-acg.jpg', 'data/attachments/module/site/block/image-slider/2198/0dfbfb8345c84ae4054c93334d114f445cd74811.jpg'),
(235, 234, 1, 7542, NULL, NULL, '2014-04-09 10:58:49', '2014-04-09 10:58:38', '93af185b26092cc2af60ee86e340b387d13e8cc4', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,7,135,82', 'logo-acg.jpg', 'data/attachments/module/site/block/image-slider/2198/93af185b26092cc2af60ee86e340b387d13e8cc4.jpg'),
(236, NULL, 1, 11475, NULL, 1, '2014-04-09 10:59:23', '2014-04-09 10:59:23', '0532f6b9674c1b750f39d0aeab8fb9676f39d03c', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-alior.jpg', 'data/attachments/module/site/block/image-slider/2198/0532f6b9674c1b750f39d0aeab8fb9676f39d03c.jpg'),
(237, 236, 1, 11179, NULL, NULL, '2014-04-09 10:59:23', '2014-04-09 10:59:23', 'e47435928366d7fe913618e494c5148f3b9f8f65', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,135,75', 'logo-alior.jpg', 'data/attachments/module/site/block/image-slider/2198/e47435928366d7fe913618e494c5148f3b9f8f65.jpg'),
(238, NULL, 1, 7966, NULL, 1, '2014-04-09 10:59:48', '2014-04-09 10:59:48', 'c792c0dcf3df1641c320b736db61ab76aa82ca07', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-acg.jpg', 'data/attachments/module/site/block/image-slider/2198/c792c0dcf3df1641c320b736db61ab76aa82ca07.jpg'),
(239, 238, 1, 7471, NULL, NULL, '2014-04-09 10:59:48', '2014-04-09 10:59:48', '43c0c21aa8645e5c2fe20f6f7ac55109e6db6854', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,135,75', 'logo-acg.jpg', 'data/attachments/module/site/block/image-slider/2198/43c0c21aa8645e5c2fe20f6f7ac55109e6db6854.jpg'),
(240, NULL, 1, 11475, NULL, 1, '2014-04-09 10:59:53', '2014-04-09 10:59:53', '7cba98ec23b9b7fce6b935770017c0536a4adf0b', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-alior.jpg', 'data/attachments/module/site/block/image-slider/2198/7cba98ec23b9b7fce6b935770017c0536a4adf0b.jpg'),
(241, 240, 1, 11651, NULL, NULL, '2014-04-09 10:59:58', '2014-04-09 10:59:53', 'cc17ed9e32e0422bbd8bf4c058a65b4eaba842e1', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,9,135,84', 'logo-alior.jpg', 'data/attachments/module/site/block/image-slider/2198/cc17ed9e32e0422bbd8bf4c058a65b4eaba842e1.jpg'),
(242, NULL, 1, 7206, NULL, 1, '2014-04-09 11:00:23', '2014-04-09 11:00:23', '7b8ca4436d64549004b4cfb2fa24ea86dfc016f4', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-cbre.jpg', 'data/attachments/module/site/block/image-slider/2198/7b8ca4436d64549004b4cfb2fa24ea86dfc016f4.jpg'),
(243, 242, 1, 7457, NULL, NULL, '2014-04-09 11:00:29', '2014-04-09 11:00:23', '1252d8f83189fbdb6b7fc942d8a405140e06f239', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,7,135,82', 'logo-cbre.jpg', 'data/attachments/module/site/block/image-slider/2198/1252d8f83189fbdb6b7fc942d8a405140e06f239.jpg'),
(244, NULL, 1, 7893, NULL, 1, '2014-04-09 11:00:40', '2014-04-09 11:00:40', '91c0e28b0ff1bbd2474fed476478c2b2db8a1713', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-cordlime.jpg', 'data/attachments/module/site/block/image-slider/2198/91c0e28b0ff1bbd2474fed476478c2b2db8a1713.jpg'),
(245, 244, 1, 7677, NULL, NULL, '2014-04-09 11:00:44', '2014-04-09 11:00:40', '8af21748bca0567e804cdf6288d32b54dd7a2709', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,8,135,83', 'logo-cordlime.jpg', 'data/attachments/module/site/block/image-slider/2198/8af21748bca0567e804cdf6288d32b54dd7a2709.jpg'),
(246, NULL, 1, 8046, NULL, 1, '2014-04-09 11:00:58', '2014-04-09 11:00:58', 'd2f543c6384c565ac455acdd8f0821f511d08b82', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-infopraca.jpg', 'data/attachments/module/site/block/image-slider/2198/d2f543c6384c565ac455acdd8f0821f511d08b82.jpg'),
(247, 246, 1, 7548, NULL, NULL, '2014-04-09 11:00:58', '2014-04-09 11:00:58', '57822bf989beb5cbfb6a8783b03650de56226a74', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,135,75', 'logo-infopraca.jpg', 'data/attachments/module/site/block/image-slider/2198/57822bf989beb5cbfb6a8783b03650de56226a74.jpg'),
(248, NULL, 1, 8046, NULL, 1, '2014-04-09 11:01:15', '2014-04-09 11:01:15', 'c3a2b7a709f80eff431d871153c713fe9381d5fb', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-infopraca.jpg', 'data/attachments/module/site/block/image-slider/2198/c3a2b7a709f80eff431d871153c713fe9381d5fb.jpg'),
(249, 248, 1, 7816, NULL, NULL, '2014-04-09 11:01:21', '2014-04-09 11:01:15', 'aaaad6768246a80ab4d2fc0ca0ecb38ba7d0fe7b', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,10,135,85', 'logo-infopraca.jpg', 'data/attachments/module/site/block/image-slider/2198/aaaad6768246a80ab4d2fc0ca0ecb38ba7d0fe7b.jpg'),
(250, NULL, 1, 8210, NULL, 1, '2014-04-09 11:01:50', '2014-04-09 11:01:50', '935495441035cf22fa748144a90eefe7ca51bfb9', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-mg.jpg', 'data/attachments/module/site/block/image-slider/2198/935495441035cf22fa748144a90eefe7ca51bfb9.jpg'),
(251, 250, 1, 7934, NULL, NULL, '2014-04-09 11:01:55', '2014-04-09 11:01:50', '1614d3bf8764a4bb6d8802e869d53e672c4c28e1', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,8,135,83', 'logo-mg.jpg', 'data/attachments/module/site/block/image-slider/2198/1614d3bf8764a4bb6d8802e869d53e672c4c28e1.jpg'),
(252, NULL, 1, 7910, NULL, 1, '2014-04-09 11:02:15', '2014-04-09 11:02:15', '192fcbb181fce4a1d752e33055f8fb4142609398', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-mpi.jpg', 'data/attachments/module/site/block/image-slider/2198/192fcbb181fce4a1d752e33055f8fb4142609398.jpg'),
(253, 252, 1, 7466, NULL, NULL, '2014-04-09 11:02:21', '2014-04-09 11:02:15', 'e18bdb7258b33a4a3852ed269252d6ca6ab37807', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,7,135,82', 'logo-mpi.jpg', 'data/attachments/module/site/block/image-slider/2198/e18bdb7258b33a4a3852ed269252d6ca6ab37807.jpg'),
(254, NULL, 1, 11457, NULL, 1, '2014-04-09 11:02:40', '2014-04-09 11:02:40', '7924cdb1dff4c25a1802b417f02b11dfdb8cc0da', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-polsat_cyfrowy.jpg', 'data/attachments/module/site/block/image-slider/2198/7924cdb1dff4c25a1802b417f02b11dfdb8cc0da.jpg'),
(255, 254, 1, 11078, NULL, NULL, '2014-04-09 11:02:45', '2014-04-09 11:02:40', '4f54c570738a9d1e041ddfd56676842b729658b3', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,10,135,85', 'logo-polsat_cyfrowy.jpg', 'data/attachments/module/site/block/image-slider/2198/4f54c570738a9d1e041ddfd56676842b729658b3.jpg'),
(256, NULL, 1, 11909, NULL, 1, '2014-04-09 11:03:19', '2014-04-09 11:03:19', 'e35fedb2916378d620ab4a120410f1a18c8d8b31', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-radwag.jpg', 'data/attachments/module/site/block/image-slider/2198/e35fedb2916378d620ab4a120410f1a18c8d8b31.jpg'),
(257, 256, 1, 11573, NULL, NULL, '2014-04-09 11:03:29', '2014-04-09 11:03:19', '19a71f883c6db43a110f8d81772abee75abd7fbb', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,9,135,84', 'logo-radwag.jpg', 'data/attachments/module/site/block/image-slider/2198/19a71f883c6db43a110f8d81772abee75abd7fbb.jpg'),
(258, NULL, 1, 8391, NULL, 1, '2014-04-09 11:03:58', '2014-04-09 11:03:58', 'b589b73fcc464908f6264f5a0f6b669122e07b98', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-wzbk.jpg', 'data/attachments/module/site/block/image-slider/2198/b589b73fcc464908f6264f5a0f6b669122e07b98.jpg'),
(259, 258, 1, 8569, NULL, NULL, '2014-04-09 11:04:05', '2014-04-09 11:03:58', '89c0a1a287c2f127369fe21355a888306ef2fcbd', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,9,135,84', 'logo-wzbk.jpg', 'data/attachments/module/site/block/image-slider/2198/89c0a1a287c2f127369fe21355a888306ef2fcbd.jpg'),
(260, NULL, 1, 7893, NULL, 1, '2014-04-09 11:04:45', '2014-04-09 11:04:45', 'f42a36d7c4bcedb4ba80031ae356918a653bdb21', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-cordlime.jpg', 'data/attachments/module/site/block/image-slider/2198/f42a36d7c4bcedb4ba80031ae356918a653bdb21.jpg'),
(261, 260, 1, 7312, NULL, NULL, '2014-04-09 11:04:53', '2014-04-09 11:04:45', '3003fead6521d0b1b525d851ffab333147f118f7', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,10,135,85', 'logo-cordlime.jpg', 'data/attachments/module/site/block/image-slider/2198/3003fead6521d0b1b525d851ffab333147f118f7.jpg'),
(262, NULL, 1, 32893, NULL, 1, '2014-04-09 11:11:52', '2014-04-09 11:11:52', '981b9317d5459cc99f08a7ff30587fe890a73dc0', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'api_thumb_450 (15).jpg', 'data/attachments/module/site/block/bachelor-studies/2144/981b9317d5459cc99f08a7ff30587fe890a73dc0.jpg'),
(263, 262, 1, 33212, NULL, NULL, '2014-04-09 11:11:52', '2014-04-09 11:11:52', 'a4cb216946cf133cda9e7f8c92b956fc1c39eb86', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'api_thumb_450 (15).jpg', 'data/attachments/module/site/block/bachelor-studies/2144/a4cb216946cf133cda9e7f8c92b956fc1c39eb86.jpg'),
(264, NULL, 1, 32893, NULL, 1, '2014-04-09 11:12:07', '2014-04-09 11:12:07', '2d028c71152041d7fc9b1b51c8ee33bd30d84646', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'api_thumb_450 (15).jpg', 'data/attachments/module/site/block/bachelor-studies/2144/2d028c71152041d7fc9b1b51c8ee33bd30d84646.jpg'),
(265, 264, 1, 33212, NULL, NULL, '2014-04-09 11:12:39', '2014-04-09 11:12:07', '57dd3580e324c1f9046d98acc0616da1ad099193', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'api_thumb_450 (15).jpg', 'data/attachments/module/site/block/bachelor-studies/2144/57dd3580e324c1f9046d98acc0616da1ad099193.jpg'),
(266, NULL, 1, 32893, NULL, 1, '2014-04-09 11:12:44', '2014-04-09 11:12:44', '8b68429277d70972951f79379d2bcbda71c9df65', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'api_thumb_450 (15).jpg', 'data/attachments/module/site/block/bachelor-studies/2144/8b68429277d70972951f79379d2bcbda71c9df65.jpg'),
(267, 266, 1, 33212, NULL, NULL, '2014-04-09 11:12:44', '2014-04-09 11:12:44', '02ec581455d2f5a2af7aabcd0800eb8b2e055058', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'api_thumb_450 (15).jpg', 'data/attachments/module/site/block/bachelor-studies/2144/02ec581455d2f5a2af7aabcd0800eb8b2e055058.jpg'),
(268, NULL, 1, 32893, NULL, 1, '2014-04-09 11:13:42', '2014-04-09 11:13:42', '7aff63de17524f5a540f62f21d5df3a0ade8b2fa', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'api_thumb_450 (15).jpg', 'data/attachments/module/site/block/bachelor-studies/2144/7aff63de17524f5a540f62f21d5df3a0ade8b2fa.jpg'),
(269, 268, 1, 33212, NULL, NULL, '2014-04-09 11:13:42', '2014-04-09 11:13:42', 'c2105ed33b23355570296f245fec2a6238565d98', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'api_thumb_450 (15).jpg', 'data/attachments/module/site/block/bachelor-studies/2144/c2105ed33b23355570296f245fec2a6238565d98.jpg'),
(270, NULL, 1, 21033, NULL, 1, '2014-04-09 11:15:12', '2014-04-09 11:15:12', 'd00d42db4a353005d806413e9d02ba576f5b4ca4', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', '26579063.jpg', 'data/attachments/module/site/block/bachelor-studies/2151/d00d42db4a353005d806413e9d02ba576f5b4ca4.jpg'),
(271, 270, 1, 21383, NULL, NULL, '2014-04-09 11:15:12', '2014-04-09 11:15:12', '07ff5f2780b3d1de778fde3a13bc46f055dd8c05', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', '26579063.jpg', 'data/attachments/module/site/block/bachelor-studies/2151/07ff5f2780b3d1de778fde3a13bc46f055dd8c05.jpg'),
(272, NULL, 1, 34955, NULL, 1, '2014-04-09 11:17:53', '2014-04-09 11:17:53', '0e87bf87cafc0d5bc15329629394085053a95b98', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', '32963547.jpg', 'data/attachments/module/site/block/bachelor-studies/2150/0e87bf87cafc0d5bc15329629394085053a95b98.jpg'),
(273, 272, 1, 27121, NULL, NULL, '2014-04-09 11:17:53', '2014-04-09 11:17:53', '5dc5f23a8e4caa44bde3947ec7a38eb0a3c6ee00', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', '32963547.jpg', 'data/attachments/module/site/block/bachelor-studies/2150/5dc5f23a8e4caa44bde3947ec7a38eb0a3c6ee00.jpg'),
(274, NULL, 1, 34955, NULL, 1, '2014-04-09 11:19:18', '2014-04-09 11:19:18', '99f0430d038bce0a725db276f11c5ea707f8e6b2', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', '32963547.jpg', 'data/attachments/module/site/block/bachelor-studies/2150/99f0430d038bce0a725db276f11c5ea707f8e6b2.jpg'),
(275, 274, 1, 27121, NULL, NULL, '2014-04-09 11:19:18', '2014-04-09 11:19:18', '1efe47f040448411bbc96c2c67ab721e78b4e57a', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', '32963547.jpg', 'data/attachments/module/site/block/bachelor-studies/2150/1efe47f040448411bbc96c2c67ab721e78b4e57a.jpg'),
(276, NULL, 1, 22581, NULL, 1, '2014-04-09 11:20:36', '2014-04-09 11:20:36', 'cf6757f97a4f8f59fddde44992cd62bf7ce96e77', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'api_thumb_450 (4).jpg', 'data/attachments/module/site/block/bachelor-studies/2149/cf6757f97a4f8f59fddde44992cd62bf7ce96e77.jpg'),
(277, 276, 1, 22852, NULL, NULL, '2014-04-09 11:20:36', '2014-04-09 11:20:36', '9f8fbd883d7d4674f6226534e3fce52d17065713', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'api_thumb_450 (4).jpg', 'data/attachments/module/site/block/bachelor-studies/2149/9f8fbd883d7d4674f6226534e3fce52d17065713.jpg'),
(278, NULL, 1, 56336, NULL, 1, '2014-04-09 11:21:24', '2014-04-09 11:21:24', 'd78cf8828c7983272c9477293a63252d282add20', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', '32963561.jpg', 'data/attachments/module/site/block/bachelor-studies/2152/d78cf8828c7983272c9477293a63252d282add20.jpg'),
(279, 278, 1, 26615, NULL, NULL, '2014-04-09 11:36:30', '2014-04-09 11:21:24', '9bc2d3d7f6f7f54d21ddfdab76066fd8dbde8e93', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', '32963561.jpg', 'data/attachments/module/site/block/bachelor-studies/2152/9bc2d3d7f6f7f54d21ddfdab76066fd8dbde8e93.jpg'),
(280, NULL, 1, 46438, NULL, 1, '2014-04-09 11:38:36', '2014-04-09 11:38:36', 'd0a86f8f8d97ef5a822a11eaf83120de5b84d787', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'api_thumb_450 (16).jpg', 'data/attachments/module/site/block/bachelor-studies/2154/d0a86f8f8d97ef5a822a11eaf83120de5b84d787.jpg'),
(281, 280, 1, 30873, NULL, NULL, '2014-04-09 11:38:36', '2014-04-09 11:38:36', 'df6c9a80e65b2b6262ed2fc5081c69536565c084', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'api_thumb_450 (16).jpg', 'data/attachments/module/site/block/bachelor-studies/2154/df6c9a80e65b2b6262ed2fc5081c69536565c084.jpg'),
(282, NULL, 1, 32776, NULL, 1, '2014-04-09 11:48:01', '2014-04-09 11:48:01', 'f7a77a850a1d05aeb26ced90c6ae242c6537e66a', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'Informatyka.jpg', 'data/attachments/module/site/block/bachelor-studies/2144/f7a77a850a1d05aeb26ced90c6ae242c6537e66a.jpg'),
(283, 282, 1, 32978, NULL, NULL, '2014-04-09 11:48:01', '2014-04-09 11:48:01', '95b832566adbbeae606e9f3105caac862b091430', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'Informatyka.jpg', 'data/attachments/module/site/block/bachelor-studies/2144/95b832566adbbeae606e9f3105caac862b091430.jpg'),
(284, NULL, 1, 40604, 2334, 1, '2014-04-09 12:09:10', '2014-04-09 12:09:10', 'c7ba73c636f43de1398fa8c5dce3e049806656ec', 'jpg', 'image/jpeg', 'site', 'images-pile', '', '4347369.jpg', 'data/attachments/module/site/block/images-pile/2334/c7ba73c636f43de1398fa8c5dce3e049806656ec.jpg'),
(285, 284, 1, 40287, 2334, NULL, '2014-04-09 12:09:10', '2014-04-09 12:09:10', 'd9466869b13ce8a53211e9b1f890f5abcc6556e8', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', '4347369.jpg', 'data/attachments/module/site/block/images-pile/2334/d9466869b13ce8a53211e9b1f890f5abcc6556e8.jpg'),
(286, NULL, 1, 31361, NULL, 1, '2014-04-09 13:12:37', '2014-04-09 13:12:37', '853770af2923f0b213486eb3f32b394d411d423a', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'ekonomia2.jpg', 'data/attachments/module/site/block/bachelor-studies/2037/853770af2923f0b213486eb3f32b394d411d423a.jpg'),
(287, 286, 1, 957, NULL, NULL, '2014-04-10 15:54:48', '2014-04-09 13:12:37', 'f11a9f8f6f960cc33a32a2b1d0cd229caf7166a8', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,18,18', 'ekonomia2.jpg', 'data/attachments/module/site/block/bachelor-studies/2037/f11a9f8f6f960cc33a32a2b1d0cd229caf7166a8.jpg'),
(288, NULL, 1, 66442, 2336, 1, '2014-04-09 15:10:30', '2014-04-09 15:10:30', 'a53dc75762a0132f37a5afc5a37fb740a7f72cd2', 'jpg', 'image/jpeg', 'site', 'images-pile', '', '55WI48TO.jpg', 'data/attachments/module/site/block/images-pile/2336/a53dc75762a0132f37a5afc5a37fb740a7f72cd2.jpg'),
(289, 288, 1, 36440, 2336, NULL, '2014-04-09 15:10:37', '2014-04-09 15:10:30', 'e9aeaa4be650d1a1731d35834309bd10da271bd2', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,66,250,254', '55WI48TO.jpg', 'data/attachments/module/site/block/images-pile/2336/e9aeaa4be650d1a1731d35834309bd10da271bd2.jpg'),
(290, NULL, 1, 40330, NULL, 1, '2014-04-09 17:05:39', '2014-04-09 17:05:39', '0c57da3334ad1341892f219139c1413cfdefc125', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'index,edc3c71d829ea8814f2789ea0d17339f80253ce8,Studenci AFiBV w Turcji.jpg', 'data/attachments/module/site/block/news-list/1445/0c57da3334ad1341892f219139c1413cfdefc125.jpg'),
(291, 290, 1, 33926, NULL, NULL, '2014-04-09 17:05:39', '2014-04-09 17:05:39', 'b20cf0f9ba893fcef431fb2353a73db85fef908b', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,230,160', 'index,edc3c71d829ea8814f2789ea0d17339f80253ce8,Studenci AFiBV w Turcji.jpg', 'data/attachments/module/site/block/news-list/1445/b20cf0f9ba893fcef431fb2353a73db85fef908b.jpg'),
(292, NULL, 1, 40330, NULL, 1, '2014-04-09 17:06:20', '2014-04-09 17:06:20', '2e97fe4c11a100d5c425ac6dce0fcfd510c77bf7', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'Studenci AFiBV w Turcji.jpg', 'data/attachments/module/site/block/news-list/1445/2e97fe4c11a100d5c425ac6dce0fcfd510c77bf7.jpg'),
(293, 292, 1, 33750, NULL, NULL, '2014-04-09 17:06:47', '2014-04-09 17:06:20', '98b5157e83f620f93db40f378edcb7de3919c80d', 'jpg', 'image/jpeg', 'site', 'news-list', '55,0,285,160', 'Studenci AFiBV w Turcji.jpg', 'data/attachments/module/site/block/news-list/1445/98b5157e83f620f93db40f378edcb7de3919c80d.jpg'),
(294, NULL, 1, 40330, NULL, 1, '2014-04-09 17:06:41', '2014-04-09 17:06:41', '0f1fe4f0aee90c59954c468f134200f314f51817', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'Studenci-AFiBV-w-Turcji.jpg', 'data/attachments/module/site/block/news-list/1445/0f1fe4f0aee90c59954c468f134200f314f51817.jpg'),
(295, 294, 1, 33926, NULL, NULL, '2014-04-09 17:06:41', '2014-04-09 17:06:41', '607be797b1a691c9474cca4fefe720f41126d68e', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,230,160', 'Studenci-AFiBV-w-Turcji.jpg', 'data/attachments/module/site/block/news-list/1445/607be797b1a691c9474cca4fefe720f41126d68e.jpg'),
(296, NULL, 1, 32565, NULL, 1, '2014-04-09 17:07:38', '2014-04-09 17:07:38', 'dfd08c76b667b818d973f796304aac33d3624c4c', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'konkurs.jpg', 'data/attachments/module/site/block/news-list/1445/dfd08c76b667b818d973f796304aac33d3624c4c.jpg'),
(297, 296, 1, 28269, NULL, NULL, '2014-04-09 17:07:38', '2014-04-09 17:07:38', '2e91cb2e7dcb11cbe547bf3ed690f0c1d12d47d8', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,230,160', 'konkurs.jpg', 'data/attachments/module/site/block/news-list/1445/2e91cb2e7dcb11cbe547bf3ed690f0c1d12d47d8.jpg'),
(298, NULL, 1, 32565, NULL, 1, '2014-04-09 17:08:09', '2014-04-09 17:08:09', '0bea4cf8dad246b7b2d3e01f74a7e20d4923b0a1', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'konkurs.jpg', 'data/attachments/module/site/block/news-list/1445/0bea4cf8dad246b7b2d3e01f74a7e20d4923b0a1.jpg'),
(299, 298, 1, 28269, NULL, NULL, '2014-04-09 17:08:09', '2014-04-09 17:08:09', '45b59a9a0332a24af31caee315e2ddae444f3f8f', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,230,160', 'konkurs.jpg', 'data/attachments/module/site/block/news-list/1445/45b59a9a0332a24af31caee315e2ddae444f3f8f.jpg'),
(300, NULL, 1, 23383, NULL, 1, '2014-04-09 17:08:41', '2014-04-09 17:08:41', '98744d5e3dbe6fbb280b3eed34b91e1b55810d30', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'pangea.jpg', 'data/attachments/module/site/block/news-list/1445/98744d5e3dbe6fbb280b3eed34b91e1b55810d30.jpg'),
(301, 300, 1, 20837, NULL, NULL, '2014-04-09 17:08:41', '2014-04-09 17:08:41', 'd4a851aa8a0df2a2dbef90e3716d2b396b0a8e5b', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,230,160', 'pangea.jpg', 'data/attachments/module/site/block/news-list/1445/d4a851aa8a0df2a2dbef90e3716d2b396b0a8e5b.jpg'),
(302, 165, 1, 195638, NULL, NULL, '2014-04-09 17:08:42', '2014-03-28 02:05:33', '0b4870193018460dbe6a0db27c94e3a4b83b9322', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,957.375,666', 'Konkurs matematyczny.jpg', 'data/attachments/module/site/block/news-list/1445/0b4870193018460dbe6a0db27c94e3a4b83b9322.jpg'),
(303, NULL, 1, 23383, NULL, 1, '2014-04-09 17:09:01', '2014-04-09 17:09:01', 'd5fda26ef27c0a6e5da544138af2e2e36364cf3d', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'pangea.jpg', 'data/attachments/module/site/block/news-list/1445/d5fda26ef27c0a6e5da544138af2e2e36364cf3d.jpg'),
(304, 303, 1, 20837, NULL, NULL, '2014-04-09 17:09:01', '2014-04-09 17:09:01', 'ec458b873a00f62e94d282b2926c7c6fb2dd0440', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,230,160', 'pangea.jpg', 'data/attachments/module/site/block/news-list/1445/ec458b873a00f62e94d282b2926c7c6fb2dd0440.jpg'),
(305, NULL, 1, 23383, NULL, 1, '2014-04-09 17:09:40', '2014-04-09 17:09:40', '90dc8edf15d201c643fcbdabdd2ca046521b8fe2', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'pangea.jpg', 'data/attachments/module/site/block/news-list/1445/90dc8edf15d201c643fcbdabdd2ca046521b8fe2.jpg'),
(306, 305, 1, 20837, NULL, NULL, '2014-04-09 17:09:40', '2014-04-09 17:09:40', 'd3b8d7ccf5ef5e8b2123cb82341162f02b40595d', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,230,160', 'pangea.jpg', 'data/attachments/module/site/block/news-list/1445/d3b8d7ccf5ef5e8b2123cb82341162f02b40595d.jpg'),
(307, NULL, 1, 48039, NULL, 1, '2014-04-09 17:10:46', '2014-04-09 17:10:46', 'a61e00554ba98e5a027f232e5129033a9ab17a50', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'intern.jpg', 'data/attachments/module/site/block/news-list/1445/a61e00554ba98e5a027f232e5129033a9ab17a50.jpg'),
(308, 307, 1, 37838, NULL, NULL, '2014-04-09 17:10:52', '2014-04-09 17:10:46', 'f2983950e2d5cbbc057fe87cb255b045dcfa735b', 'jpg', 'image/jpeg', 'site', 'news-list', '53,0,283,160', 'intern.jpg', 'data/attachments/module/site/block/news-list/1445/f2983950e2d5cbbc057fe87cb255b045dcfa735b.jpg'),
(309, NULL, 1, 40330, NULL, 1, '2014-04-09 17:11:28', '2014-04-09 17:11:28', 'd2259a5b8ff13081a1b47ab1a8f12d36d36e14ce', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'student-afib.jpg', 'data/attachments/module/site/block/news-list/1445/d2259a5b8ff13081a1b47ab1a8f12d36d36e14ce.jpg'),
(310, 309, 1, 33926, NULL, NULL, '2014-04-09 17:11:28', '2014-04-09 17:11:28', '1714ddde98f186eb16cc2bce771b9217435d4a9f', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,230,160', 'student-afib.jpg', 'data/attachments/module/site/block/news-list/1445/1714ddde98f186eb16cc2bce771b9217435d4a9f.jpg'),
(311, NULL, 1, 40330, NULL, 1, '2014-04-09 17:11:55', '2014-04-09 17:11:55', '62b6220d69ef1d3788ad86eca10ab38c5a59f8b4', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'student-afib.jpg', 'data/attachments/module/site/block/news-list/1445/62b6220d69ef1d3788ad86eca10ab38c5a59f8b4.jpg'),
(312, 311, 1, 33416, NULL, NULL, '2014-04-09 17:12:04', '2014-04-09 17:11:55', 'a9b6e2d7b8d455799a9c53e7db237bc2b8c50256', 'jpg', 'image/jpeg', 'site', 'news-list', '56,0,286,160', 'student-afib.jpg', 'data/attachments/module/site/block/news-list/1445/a9b6e2d7b8d455799a9c53e7db237bc2b8c50256.jpg'),
(313, NULL, 1, 43682, NULL, 1, '2014-04-09 17:13:23', '2014-04-09 17:13:23', '9a6dafca3e538265b43a8352cac49f0a938947c9', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'inf.jpg', 'data/attachments/module/site/block/news-list/1445/9a6dafca3e538265b43a8352cac49f0a938947c9.jpg'),
(314, 313, 1, 43766, NULL, NULL, '2014-04-09 17:13:30', '2014-04-09 17:13:23', '6bef2ef79109c20ee45cd39cdd6c9ca64d977492', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,230,160', 'inf.jpg', 'data/attachments/module/site/block/news-list/1445/6bef2ef79109c20ee45cd39cdd6c9ca64d977492.jpg'),
(315, NULL, 1, 11799, NULL, 1, '2014-04-09 17:14:19', '2014-04-09 17:14:19', 'cb128d8ad550d9364435d87b95279ec57bed9a3f', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'afib3.jpg', 'data/attachments/module/site/block/news-list/1445/cb128d8ad550d9364435d87b95279ec57bed9a3f.jpg'),
(316, 315, 1, 10359, NULL, NULL, '2014-04-09 17:14:36', '2014-04-09 17:14:19', '64eaa4c1939fe06afbc16975cc8289f6974f8908', 'jpg', 'image/jpeg', 'site', 'news-list', '9,0,239,160', 'afib3.jpg', 'data/attachments/module/site/block/news-list/1445/64eaa4c1939fe06afbc16975cc8289f6974f8908.jpg'),
(317, NULL, 1, 32565, NULL, 1, '2014-04-09 17:14:57', '2014-04-09 17:14:57', '5e97a86a4ddba46a1cf91641597db7535bab8cb5', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'konkurs.jpg', 'data/attachments/module/site/block/news-list/1445/5e97a86a4ddba46a1cf91641597db7535bab8cb5.jpg'),
(318, 317, 1, 29442, NULL, NULL, '2014-04-09 17:15:05', '2014-04-09 17:14:57', '1f6a0f2fb29aafa346f50470c43b8aa5c4379287', 'jpg', 'image/jpeg', 'site', 'news-list', '26,0,256,160', 'konkurs.jpg', 'data/attachments/module/site/block/news-list/1445/1f6a0f2fb29aafa346f50470c43b8aa5c4379287.jpg'),
(319, NULL, 1, 27413, NULL, 1, '2014-04-09 17:15:33', '2014-04-09 17:15:33', '0138dbc5cf980e3191852e35db7f970f79af8fd6', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'spolpraca.jpg', 'data/attachments/module/site/block/news-list/1445/0138dbc5cf980e3191852e35db7f970f79af8fd6.jpg'),
(320, 319, 1, 23607, NULL, NULL, '2014-04-09 17:15:33', '2014-04-09 17:15:33', '48ac7faa586802352e4751ab23ad73baf87dd211', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,230,160', 'spolpraca.jpg', 'data/attachments/module/site/block/news-list/1445/48ac7faa586802352e4751ab23ad73baf87dd211.jpg'),
(321, NULL, 1, 37845, NULL, 1, '2014-04-09 17:16:02', '2014-04-09 17:16:02', '39ddbc7a74c20599250669c6be527f222a9a5a6c', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'jobspot.jpg', 'data/attachments/module/site/block/news-list/1445/39ddbc7a74c20599250669c6be527f222a9a5a6c.jpg'),
(322, 321, 1, 32553, NULL, NULL, '2014-04-09 17:16:08', '2014-04-09 17:16:02', '4ede8c9f31f85e98cdaa7800db55969a6ea3c451', 'jpg', 'image/jpeg', 'site', 'news-list', '28,0,258,160', 'jobspot.jpg', 'data/attachments/module/site/block/news-list/1445/4ede8c9f31f85e98cdaa7800db55969a6ea3c451.jpg'),
(323, NULL, 1, 27568, NULL, 1, '2014-04-09 17:16:31', '2014-04-09 17:16:31', '84dfbdb106b4a5eedc48511e5b3e330c877a6850', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'shake.jpg', 'data/attachments/module/site/block/news-list/1445/84dfbdb106b4a5eedc48511e5b3e330c877a6850.jpg'),
(324, 323, 1, 24509, NULL, NULL, '2014-04-09 17:16:37', '2014-04-09 17:16:31', '1b76b186d67eba9fc2760b3f633c3caff6531261', 'jpg', 'image/jpeg', 'site', 'news-list', '28,0,258,160', 'shake.jpg', 'data/attachments/module/site/block/news-list/1445/1b76b186d67eba9fc2760b3f633c3caff6531261.jpg'),
(325, NULL, 1, 20486, NULL, 1, '2014-04-09 17:17:04', '2014-04-09 17:17:04', '719820a1f3b655eb78982493e57b1082687312f6', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'kapital-int.jpg', 'data/attachments/module/site/block/news-list/1445/719820a1f3b655eb78982493e57b1082687312f6.jpg'),
(326, 325, 1, 18293, NULL, NULL, '2014-04-09 17:17:11', '2014-04-09 17:17:04', 'f165b10630c2e62b65da1433bdfc7e0d91ac46fe', 'jpg', 'image/jpeg', 'site', 'news-list', '21,0,251,160', 'kapital-int.jpg', 'data/attachments/module/site/block/news-list/1445/f165b10630c2e62b65da1433bdfc7e0d91ac46fe.jpg'),
(327, NULL, 1, 43682, NULL, 1, '2014-04-09 17:18:21', '2014-04-09 17:18:21', '97fdef875612b00ff77ac75969b9e898186689cf', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'inf.jpg', 'data/attachments/module/site/block/news-list/1445/97fdef875612b00ff77ac75969b9e898186689cf.jpg'),
(328, 327, 1, 36069, NULL, NULL, '2014-04-10 13:36:59', '2014-04-09 17:18:21', '48d8c5a299a5b56013a9ffdf00803bd1a5c43666', 'jpg', 'image/jpeg', 'site', 'news-list', '0,11,230,138.77777777777777', 'inf.jpg', 'data/attachments/module/site/block/news-list/1445/48d8c5a299a5b56013a9ffdf00803bd1a5c43666.jpg'),
(329, NULL, 1, 7206, NULL, 1, '2014-04-09 17:45:30', '2014-04-09 17:45:30', '6c220aea7a4de9a639d4e22bba3ffd0c8d495cdc', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'logo-cbre.jpg', 'data/attachments/module/site/block/image-slider/2198/6c220aea7a4de9a639d4e22bba3ffd0c8d495cdc.jpg'),
(330, 329, 1, 7398, NULL, NULL, '2014-04-09 17:45:36', '2014-04-09 17:45:30', '02ad8279da250e37abf5a4190ded7dea9ae2e4bf', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,8,135,83', 'logo-cbre.jpg', 'data/attachments/module/site/block/image-slider/2198/02ad8279da250e37abf5a4190ded7dea9ae2e4bf.jpg'),
(331, NULL, 1, 208348, NULL, 1, '2014-04-10 09:26:05', '2014-04-10 09:26:05', 'd2c270bb7d42348a2ec07bc156e83c35a455ca79', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'Fotolia_41002280_X.jpg', 'data/attachments/module/site/block/small-banner/1541/d2c270bb7d42348a2ec07bc156e83c35a455ca79.jpg'),
(332, 331, 1, 127738, NULL, NULL, '2014-04-10 09:26:17', '2014-04-10 09:26:05', 'd94a0226ac8c75495c8313286751ff85a35d73be', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,126,875,426', 'Fotolia_41002280_X.jpg', 'data/attachments/module/site/block/small-banner/1541/d94a0226ac8c75495c8313286751ff85a35d73be.jpg'),
(333, NULL, 1, 37599, NULL, 1, '2014-04-10 10:07:57', '2014-04-10 10:07:57', '80dded59157c41ee1617f4b7be6233506db04c30', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'iStock_000013346702Large.jpg', 'data/attachments/module/site/block/news-list/1445/80dded59157c41ee1617f4b7be6233506db04c30.jpg'),
(334, 333, 1, 33987, NULL, NULL, '2014-04-10 10:08:15', '2014-04-10 10:07:57', 'fe3e5a11b46480e4c8a8fb26952657ae8f1bcf6a', 'jpg', 'image/jpeg', 'site', 'news-list', '37,0,267,160', 'iStock_000013346702Large.jpg', 'data/attachments/module/site/block/news-list/1445/fe3e5a11b46480e4c8a8fb26952657ae8f1bcf6a.jpg');
INSERT INTO `attachments` (`attachment_id`, `parent_attachment_id`, `is_published`, `file_size`, `page_block_id`, `last_update_author_id`, `last_update_date`, `creation_date`, `hash_for_file_name`, `extension`, `mime_type`, `module_name`, `block_name`, `image_crop_rect_coords`, `original_file_name`, `file_path`) VALUES
(335, NULL, 1, 37556, NULL, 1, '2014-04-10 10:13:23', '2014-04-10 10:13:23', 'd952eb4e94f4dc748fbb7657ff9db557db5b9b60', 'jpg', 'image/jpeg', 'site', 'news-list', '', '10171194_442346809233973_3560355641217455548_n.jpg', 'data/attachments/module/site/block/news-list/1445/d952eb4e94f4dc748fbb7657ff9db557db5b9b60.jpg'),
(336, 335, 1, 34981, NULL, NULL, '2014-04-11 11:33:10', '2014-04-10 10:13:23', '4ab48766e690becddf804646eb3c9829a19cf102', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,288,160', '10171194_442346809233973_3560355641217455548_n.jpg', 'data/attachments/module/site/block/news-list/1445/4ab48766e690becddf804646eb3c9829a19cf102.jpg'),
(337, NULL, 1, 32776, NULL, 1, '2014-04-10 12:30:50', '2014-04-10 12:30:50', '2bf8b2598ec7ca3f3b8a80cab0e6e1e67c238c5a', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'Informatyka.jpg', 'data/attachments/module/site/block/bachelor-studies/2279/2bf8b2598ec7ca3f3b8a80cab0e6e1e67c238c5a.jpg'),
(338, 337, 1, 32978, NULL, NULL, '2014-04-10 12:31:39', '2014-04-10 12:30:50', '1f5f46f88cd29f12fc0e7074a6fec7e700ac8f5a', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'Informatyka.jpg', 'data/attachments/module/site/block/bachelor-studies/2279/1f5f46f88cd29f12fc0e7074a6fec7e700ac8f5a.jpg'),
(339, NULL, 1, 603249, 2354, 1, '2014-04-10 14:13:25', '2014-04-10 14:13:25', '3aad623e9b5225eecb9a63fc5f07fa4e08c2c2ad', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'B0E5YFWV.jpg', 'data/attachments/module/site/block/small-banner/2354/3aad623e9b5225eecb9a63fc5f07fa4e08c2c2ad.jpg'),
(340, 339, 1, 158060, 2354, NULL, '2014-04-10 14:13:25', '2014-04-10 14:13:25', 'e05ad91994123e94c86af4919c4a9950f8691f7d', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,839,300', 'B0E5YFWV.jpg', 'data/attachments/module/site/block/small-banner/2354/e05ad91994123e94c86af4919c4a9950f8691f7d.jpg'),
(341, NULL, 1, 603249, 2354, 1, '2014-04-10 14:13:47', '2014-04-10 14:13:47', '9b0d39bea0c9a4d4985f8ee8a4ae21aae816f7ad', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'B0E5YFWV.jpg', 'data/attachments/module/site/block/small-banner/2354/9b0d39bea0c9a4d4985f8ee8a4ae21aae816f7ad.jpg'),
(342, 341, 1, 165674, 2354, NULL, '2014-04-10 14:14:06', '2014-04-10 14:13:47', '69f4c6f861ae477f73541680464f693ace21b48a', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,256,839,543.6571428571428', 'B0E5YFWV.jpg', 'data/attachments/module/site/block/small-banner/2354/69f4c6f861ae477f73541680464f693ace21b48a.jpg'),
(343, NULL, 1, 265519, 2354, 1, '2014-04-10 14:23:23', '2014-04-10 14:23:23', '4b5244b0b426b962d2e6af8aa422f6e4920299f4', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'iStock_000012106929Large.jpg', 'data/attachments/module/site/block/small-banner/2354/4b5244b0b426b962d2e6af8aa422f6e4920299f4.jpg'),
(344, 343, 1, 127187, 2354, NULL, '2014-04-10 14:23:23', '2014-04-10 14:23:23', '168c777f57ea4aeb9f2ffa692581d79e7c173ff7', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,875,300', 'iStock_000012106929Large.jpg', 'data/attachments/module/site/block/small-banner/2354/168c777f57ea4aeb9f2ffa692581d79e7c173ff7.jpg'),
(345, NULL, 1, 275584, 2354, 1, '2014-04-10 14:24:24', '2014-04-10 14:24:24', '318d2b7575fd99e8c2c3190c40ed672247cb7108', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'iStock_000004702408Large.jpg', 'data/attachments/module/site/block/small-banner/2354/318d2b7575fd99e8c2c3190c40ed672247cb7108.jpg'),
(346, 345, 1, 119924, 2354, NULL, '2014-04-10 14:24:24', '2014-04-10 14:24:24', 'f300e9a34264146968fa7cd7cc37d25f9c8cb1b6', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,875,300', 'iStock_000004702408Large.jpg', 'data/attachments/module/site/block/small-banner/2354/f300e9a34264146968fa7cd7cc37d25f9c8cb1b6.jpg'),
(347, NULL, 1, 342935, 2354, 1, '2014-04-10 14:24:42', '2014-04-10 14:24:42', '3dd00607e811cd32035fb714405c012a1e5d6e78', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'azjata2.jpg', 'data/attachments/module/site/block/small-banner/2354/3dd00607e811cd32035fb714405c012a1e5d6e78.jpg'),
(348, 347, 1, 166679, 2354, NULL, '2014-04-10 14:24:42', '2014-04-10 14:24:42', 'fda3c140f2a036f328aa96a85230bd25b878ed3e', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,875,300', 'azjata2.jpg', 'data/attachments/module/site/block/small-banner/2354/fda3c140f2a036f328aa96a85230bd25b878ed3e.jpg'),
(349, NULL, 1, 291746, 2354, 1, '2014-04-10 14:25:20', '2014-04-10 14:25:20', 'c66859416c76e5ba8408c2a1ca53e0f1b07a7942', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'biznes foto.jpg', 'data/attachments/module/site/block/small-banner/2354/c66859416c76e5ba8408c2a1ca53e0f1b07a7942.jpg'),
(350, 349, 1, 128625, 2354, NULL, '2014-04-10 14:25:20', '2014-04-10 14:25:20', '46acf0925ee0e116dc73021af983931c0fbb6f38', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,875,300', 'biznes foto.jpg', 'data/attachments/module/site/block/small-banner/2354/46acf0925ee0e116dc73021af983931c0fbb6f38.jpg'),
(351, NULL, 1, 291746, NULL, 1, '2014-04-10 14:28:24', '2014-04-10 14:28:24', '2a6970d96969520e5d8701177d7207eebdcd5a1a', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'biznes foto.jpg', 'data/attachments/module/site/block/small-banner/1541/2a6970d96969520e5d8701177d7207eebdcd5a1a.jpg'),
(352, 351, 1, 128625, NULL, NULL, '2014-04-10 14:28:24', '2014-04-10 14:28:24', '9556580d4d56e145fc6b6614469901689ba12678', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,875,300', 'biznes foto.jpg', 'data/attachments/module/site/block/small-banner/1541/9556580d4d56e145fc6b6614469901689ba12678.jpg'),
(353, NULL, 1, 291746, 2354, 1, '2014-04-10 14:29:19', '2014-04-10 14:29:19', 'c210d3bca6a048a82b8c98d779d3606bb8526d0f', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'biznes foto.jpg', 'data/attachments/module/site/block/small-banner/2354/c210d3bca6a048a82b8c98d779d3606bb8526d0f.jpg'),
(354, 353, 1, 128625, 2354, NULL, '2014-04-10 14:29:19', '2014-04-10 14:29:19', '44a09f909996f95d85e36be8153281ab6f203147', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,875,300', 'biznes foto.jpg', 'data/attachments/module/site/block/small-banner/2354/44a09f909996f95d85e36be8153281ab6f203147.jpg'),
(355, NULL, 1, 225779, 2354, 1, '2014-04-10 14:30:03', '2014-04-10 14:30:03', '4fd3fd333bb1c15667e1c2005f030f1f53cb3805', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'Fotolia_40776623_XXL.jpg', 'data/attachments/module/site/block/small-banner/2354/4fd3fd333bb1c15667e1c2005f030f1f53cb3805.jpg'),
(356, 355, 1, 144287, 2354, NULL, '2014-04-10 14:31:24', '2014-04-10 14:30:03', 'f51cd259906ddc8df63cbc344a08867e5597d5aa', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,85,875,385', 'Fotolia_40776623_XXL.jpg', 'data/attachments/module/site/block/small-banner/2354/f51cd259906ddc8df63cbc344a08867e5597d5aa.jpg'),
(357, NULL, 1, 291746, 2354, 4, '2014-04-10 14:31:14', '2014-04-10 14:31:14', '131d39fb0868fd6c7191aedcf6780cfc77883c61', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'biznes foto.jpg', 'data/attachments/module/site/block/small-banner/2354/131d39fb0868fd6c7191aedcf6780cfc77883c61.jpg'),
(358, 357, 1, 128625, 2354, NULL, '2014-04-10 14:31:14', '2014-04-10 14:31:14', '7c85092a3b0159b8c2decb676340bccd2711bbbb', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,875,300', 'biznes foto.jpg', 'data/attachments/module/site/block/small-banner/2354/7c85092a3b0159b8c2decb676340bccd2711bbbb.jpg'),
(359, NULL, 1, 291746, 2354, 1, '2014-04-10 14:31:46', '2014-04-10 14:31:46', '227d4a4d4f45a7f7fc028e1215c30289babd559c', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'biznes foto.jpg', 'data/attachments/module/site/block/small-banner/2354/227d4a4d4f45a7f7fc028e1215c30289babd559c.jpg'),
(360, 359, 1, 132210, 2354, NULL, '2014-04-10 14:31:57', '2014-04-10 14:31:46', 'ba9800b9ca3836bcd4d56e94c89c6ad87e6f4c51', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,153,875,453', 'biznes foto.jpg', 'data/attachments/module/site/block/small-banner/2354/ba9800b9ca3836bcd4d56e94c89c6ad87e6f4c51.jpg'),
(361, NULL, 1, 40412, NULL, 1, '2014-04-10 17:01:48', '2014-04-10 17:01:48', 'd8ef66aa47866479ddf95972a55f330538e6ecfc', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'Fotolia_34713387_M.jpg', 'data/attachments/module/site/block/images-pile/2364/d8ef66aa47866479ddf95972a55f330538e6ecfc.jpg'),
(362, 361, 1, 37969, NULL, NULL, '2014-04-10 17:01:53', '2014-04-10 17:01:48', 'f8ea6a32e32374339b599d858fcf14c4dd9209d0', 'jpg', 'image/jpeg', 'site', 'images-pile', '17,0,267,188', 'Fotolia_34713387_M.jpg', 'data/attachments/module/site/block/images-pile/2364/f8ea6a32e32374339b599d858fcf14c4dd9209d0.jpg'),
(363, NULL, 1, 49761, NULL, 1, '2014-04-10 17:02:29', '2014-04-10 17:02:29', '6f3e368337395ef057e9a778f4fcaaf63403d265', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'graduates.jpg', 'data/attachments/module/site/block/images-pile/2364/6f3e368337395ef057e9a778f4fcaaf63403d265.jpg'),
(364, 363, 1, 45750, NULL, NULL, '2014-04-10 17:02:30', '2014-04-10 17:02:29', '3eee665538b17b3d2ffc86aa895bf20d14a34905', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'graduates.jpg', 'data/attachments/module/site/block/images-pile/2364/3eee665538b17b3d2ffc86aa895bf20d14a34905.jpg'),
(365, NULL, 1, 41534, NULL, 1, '2014-04-10 17:02:42', '2014-04-10 17:02:42', '76d5b686965e300dbc118a1e68edd075d5242e9a', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'Fotolia_32576060_XS.jpg', 'data/attachments/module/site/block/images-pile/2364/76d5b686965e300dbc118a1e68edd075d5242e9a.jpg'),
(366, 365, 1, 40005, NULL, NULL, '2014-04-10 17:02:42', '2014-04-10 17:02:42', '74d44f8affc72ebd3a1ef0285c778d96da57eb3d', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'Fotolia_32576060_XS.jpg', 'data/attachments/module/site/block/images-pile/2364/74d44f8affc72ebd3a1ef0285c778d96da57eb3d.jpg'),
(367, NULL, 1, 32580, NULL, 1, '2014-04-10 17:03:41', '2014-04-10 17:03:41', 'fd87e0a40027e56a26563e68bab2587254250bd3', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'Fotolia_48899050_X.jpg', 'data/attachments/module/site/block/images-pile/2364/fd87e0a40027e56a26563e68bab2587254250bd3.jpg'),
(368, 367, 1, 29682, NULL, NULL, '2014-04-10 17:03:41', '2014-04-10 17:03:41', '1f1cd5e2fe7a20e29ef6c2a464c16cc8a2f8927f', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'Fotolia_48899050_X.jpg', 'data/attachments/module/site/block/images-pile/2364/1f1cd5e2fe7a20e29ef6c2a464c16cc8a2f8927f.jpg'),
(369, NULL, 1, 51269, NULL, 1, '2014-04-10 17:05:00', '2014-04-10 17:05:00', '283ddcfbc356889ef4efc656c37684aa52efa83b', 'jpg', 'image/jpeg', 'site', 'images-pile', '', '1370658.jpg', 'data/attachments/module/site/block/images-pile/2364/283ddcfbc356889ef4efc656c37684aa52efa83b.jpg'),
(370, 369, 1, 51417, NULL, NULL, '2014-04-10 17:05:00', '2014-04-10 17:05:00', '76ef61e07cd8f8ce6dd9dc3f4fe42929e80ee4b6', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', '1370658.jpg', 'data/attachments/module/site/block/images-pile/2364/76ef61e07cd8f8ce6dd9dc3f4fe42929e80ee4b6.jpg'),
(371, NULL, 1, 39490, NULL, 1, '2014-04-10 17:06:30', '2014-04-10 17:06:30', 'd0b590570eab05255a93b995bb40020ee2b0be24', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'iStock_000013988558Large.jpg', 'data/attachments/module/site/block/images-pile/2364/d0b590570eab05255a93b995bb40020ee2b0be24.jpg'),
(372, 371, 1, 36599, NULL, NULL, '2014-04-10 17:06:30', '2014-04-10 17:06:30', 'cbcdb557162c29d654472b0444675037bf6b7e0b', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'iStock_000013988558Large.jpg', 'data/attachments/module/site/block/images-pile/2364/cbcdb557162c29d654472b0444675037bf6b7e0b.jpg'),
(373, NULL, 1, 40316, NULL, 1, '2014-04-10 17:19:20', '2014-04-10 17:19:20', '571f978ce7a8fc0c883009bf15b2d65570ff1fb7', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'iStock_000014667459Large.jpg', 'data/attachments/module/site/block/images-pile/2364/571f978ce7a8fc0c883009bf15b2d65570ff1fb7.jpg'),
(374, 373, 1, 40639, NULL, NULL, '2014-04-10 17:19:20', '2014-04-10 17:19:20', '3ca8b8ce8bab2caaeedc28a623fdbf8a1d4baafd', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'iStock_000014667459Large.jpg', 'data/attachments/module/site/block/images-pile/2364/3ca8b8ce8bab2caaeedc28a623fdbf8a1d4baafd.jpg'),
(375, NULL, 1, 49476, NULL, 1, '2014-04-10 17:22:37', '2014-04-10 17:22:37', '9fd6b796d9f99027d276828c76e52c66c77463d6', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'Fotolia_52973827_X.jpg', 'data/attachments/module/site/block/images-pile/2364/9fd6b796d9f99027d276828c76e52c66c77463d6.jpg'),
(376, 375, 1, 49532, NULL, NULL, '2014-04-10 17:22:37', '2014-04-10 17:22:37', '1d1ba23c4e46dc6c9bd07046b27601c41bb9e9c7', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'Fotolia_52973827_X.jpg', 'data/attachments/module/site/block/images-pile/2364/1d1ba23c4e46dc6c9bd07046b27601c41bb9e9c7.jpg'),
(377, NULL, 1, 61486, NULL, 1, '2014-04-10 18:51:12', '2014-04-10 18:51:12', 'c3d0b1a1bfba5505ac7dd39654e50593dd73ff76', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'iStock_000017172396Large.jpg', 'data/attachments/module/site/block/images-pile/2364/c3d0b1a1bfba5505ac7dd39654e50593dd73ff76.jpg'),
(378, 377, 1, 55574, NULL, NULL, '2014-04-10 18:51:12', '2014-04-10 18:51:12', '6b84dfe9de53f7a2d4281596ea13cddee6f17418', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'iStock_000017172396Large.jpg', 'data/attachments/module/site/block/images-pile/2364/6b84dfe9de53f7a2d4281596ea13cddee6f17418.jpg'),
(379, NULL, 1, 46682, NULL, 1, '2014-04-10 18:52:52', '2014-04-10 18:52:52', '5a1fd708646fa3e489ad52f9b62622c6c7b5b19b', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'iStock_000006326911Large.jpg', 'data/attachments/module/site/block/images-pile/2364/5a1fd708646fa3e489ad52f9b62622c6c7b5b19b.jpg'),
(380, 379, 1, 43547, NULL, NULL, '2014-04-10 18:52:52', '2014-04-10 18:52:52', '4fbf0d668d892597a9b88e6a8f9bc68c90de1da1', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'iStock_000006326911Large.jpg', 'data/attachments/module/site/block/images-pile/2364/4fbf0d668d892597a9b88e6a8f9bc68c90de1da1.jpg'),
(381, NULL, 1, 42682, NULL, 1, '2014-04-10 18:54:24', '2014-04-10 18:54:24', '1eaefcebab4ec99e47c9282176cf22014cd0c996', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'iStock_000013712694Large.jpg', 'data/attachments/module/site/block/images-pile/2364/1eaefcebab4ec99e47c9282176cf22014cd0c996.jpg'),
(382, 381, 1, 37662, NULL, NULL, '2014-04-10 18:54:24', '2014-04-10 18:54:24', '28722824e18dbf9ad53b398825920ca92fb8efc2', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'iStock_000013712694Large.jpg', 'data/attachments/module/site/block/images-pile/2364/28722824e18dbf9ad53b398825920ca92fb8efc2.jpg'),
(383, NULL, 1, 65013, NULL, 1, '2014-04-10 18:56:20', '2014-04-10 18:56:20', '57e02c7818305a818b04f7c2e66d3e0e741e5118', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'Fotolia_51699782_X.jpg', 'data/attachments/module/site/block/images-pile/2364/57e02c7818305a818b04f7c2e66d3e0e741e5118.jpg'),
(384, 383, 1, 61268, NULL, NULL, '2014-04-10 18:56:20', '2014-04-10 18:56:20', '7b75f981f25d7dbc73e344fe4d458c8b2bedfcac', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'Fotolia_51699782_X.jpg', 'data/attachments/module/site/block/images-pile/2364/7b75f981f25d7dbc73e344fe4d458c8b2bedfcac.jpg'),
(385, NULL, 1, 40412, NULL, 1, '2014-04-10 18:58:40', '2014-04-10 18:58:40', '7eff67594e06663e11fd775c630fdcaeaa64ba7f', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'Hotelarstwo_1.jpg', 'data/attachments/module/site/block/images-pile/2364/7eff67594e06663e11fd775c630fdcaeaa64ba7f.jpg'),
(386, 385, 1, 36261, NULL, NULL, '2014-04-10 18:58:40', '2014-04-10 18:58:40', 'e248921e60a6ff6aaf562706572242e7dde17652', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'Hotelarstwo_1.jpg', 'data/attachments/module/site/block/images-pile/2364/e248921e60a6ff6aaf562706572242e7dde17652.jpg'),
(387, NULL, 1, 40412, NULL, 1, '2014-04-10 18:59:05', '2014-04-10 18:59:05', 'f853991c2ad4ac7abd8f20277005524b4f5a5685', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'Fotolia_34713387_M.jpg', 'data/attachments/module/site/block/images-pile/2364/f853991c2ad4ac7abd8f20277005524b4f5a5685.jpg'),
(388, 387, 1, 36261, NULL, NULL, '2014-04-10 18:59:05', '2014-04-10 18:59:05', '1778b9712a1b8be794a61768227a2fbd022cf785', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'Fotolia_34713387_M.jpg', 'data/attachments/module/site/block/images-pile/2364/1778b9712a1b8be794a61768227a2fbd022cf785.jpg'),
(389, NULL, 1, 57746, NULL, 1, '2014-04-11 10:33:31', '2014-04-11 10:33:31', '9eff0e28af750e5c15406c4ab48af1602f2a544d', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'inf.jpg', 'data/attachments/module/site/block/news-list/1445/9eff0e28af750e5c15406c4ab48af1602f2a544d.jpg'),
(390, 389, 1, 47430, NULL, NULL, '2014-04-11 10:33:37', '2014-04-11 10:33:31', '18c88afaea084877e528729c19c48b2821b11c89', 'jpg', 'image/jpeg', 'site', 'news-list', '0,10,270,160', 'inf.jpg', 'data/attachments/module/site/block/news-list/1445/18c88afaea084877e528729c19c48b2821b11c89.jpg'),
(391, NULL, 1, 30207, NULL, 1, '2014-04-11 10:33:56', '2014-04-11 10:33:56', 'cd7a3326a814684f9ceee19615dd6d1cc5d0b851', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'student-afib.jpg', 'data/attachments/module/site/block/news-list/1445/cd7a3326a814684f9ceee19615dd6d1cc5d0b851.jpg'),
(392, 391, 1, 30651, NULL, NULL, '2014-04-11 10:34:01', '2014-04-11 10:33:56', '0ac50809df3db3e91f5733aa98d111c05e243f01', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'student-afib.jpg', 'data/attachments/module/site/block/news-list/1445/0ac50809df3db3e91f5733aa98d111c05e243f01.jpg'),
(393, NULL, 1, 57746, NULL, 1, '2014-04-11 10:34:16', '2014-04-11 10:34:16', 'cd30c3b4066a84693b2756fa787547c87679114e', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'inf.jpg', 'data/attachments/module/site/block/news-list/1445/cd30c3b4066a84693b2756fa787547c87679114e.jpg'),
(394, 393, 1, 48167, NULL, NULL, '2014-04-11 10:34:16', '2014-04-11 10:34:16', '7c6271808c5672a6a53a1d9c9ce90bad29d30a95', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'inf.jpg', 'data/attachments/module/site/block/news-list/1445/7c6271808c5672a6a53a1d9c9ce90bad29d30a95.jpg'),
(395, NULL, 1, 44431, NULL, 1, '2014-04-11 10:34:31', '2014-04-11 10:34:31', 'cc43f2974d8fdefe040cb89cf8653e68fe817df7', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'intern.jpg', 'data/attachments/module/site/block/news-list/1445/cc43f2974d8fdefe040cb89cf8653e68fe817df7.jpg'),
(396, 395, 1, 39337, NULL, NULL, '2014-04-11 10:34:31', '2014-04-11 10:34:31', '9fb063b5f9663850f90fe5264c31c269bb3bf7d0', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'intern.jpg', 'data/attachments/module/site/block/news-list/1445/9fb063b5f9663850f90fe5264c31c269bb3bf7d0.jpg'),
(397, NULL, 1, 44431, NULL, 1, '2014-04-11 10:34:42', '2014-04-11 10:34:42', '471dcc96668500afd120ee90e6cd0b3298d5e3af', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'intern.jpg', 'data/attachments/module/site/block/news-list/1445/471dcc96668500afd120ee90e6cd0b3298d5e3af.jpg'),
(398, 397, 1, 40017, NULL, NULL, '2014-04-11 10:34:47', '2014-04-11 10:34:42', 'bfaed5b73941f9e4239114807ed996e81c4a7ca4', 'jpg', 'image/jpeg', 'site', 'news-list', '32,0,302,150', 'intern.jpg', 'data/attachments/module/site/block/news-list/1445/bfaed5b73941f9e4239114807ed996e81c4a7ca4.jpg'),
(399, NULL, 1, 24809, NULL, 1, '2014-04-11 10:35:11', '2014-04-11 10:35:11', '59ff52a8bea5fa95e0eb3cd1dc2854f25dba1ebc', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'konkurs.jpg', 'data/attachments/module/site/block/news-list/1445/59ff52a8bea5fa95e0eb3cd1dc2854f25dba1ebc.jpg'),
(400, 399, 1, 25221, NULL, NULL, '2014-04-11 10:35:15', '2014-04-11 10:35:11', 'a139dfbb61dc7dc487919f0aa3907b329b48b263', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'konkurs.jpg', 'data/attachments/module/site/block/news-list/1445/a139dfbb61dc7dc487919f0aa3907b329b48b263.jpg'),
(401, NULL, 1, 9018, NULL, 1, '2014-04-11 10:35:27', '2014-04-11 10:35:27', '3e7fe18aecafb6a399dffea0af3b7dc6606357fe', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'afib3.jpg', 'data/attachments/module/site/block/news-list/1445/3e7fe18aecafb6a399dffea0af3b7dc6606357fe.jpg'),
(402, 401, 1, 9191, NULL, NULL, '2014-04-11 10:35:27', '2014-04-11 10:35:27', 'b9ba835f66a65a5fe7b6f66e3312ce450c4359ac', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'afib3.jpg', 'data/attachments/module/site/block/news-list/1445/b9ba835f66a65a5fe7b6f66e3312ce450c4359ac.jpg'),
(403, NULL, 1, 19606, NULL, 1, '2014-04-11 10:35:47', '2014-04-11 10:35:47', '49a849ca80e61f9a7263b91ba15ab3ccf588a964', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'spolpraca.jpg', 'data/attachments/module/site/block/news-list/1445/49a849ca80e61f9a7263b91ba15ab3ccf588a964.jpg'),
(404, 403, 1, 20123, NULL, NULL, '2014-04-11 10:35:47', '2014-04-11 10:35:47', 'a264e2e10f297ab25ea508cf544ca44045cc87d9', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'spolpraca.jpg', 'data/attachments/module/site/block/news-list/1445/a264e2e10f297ab25ea508cf544ca44045cc87d9.jpg'),
(405, NULL, 1, 36618, NULL, 1, '2014-04-11 10:36:20', '2014-04-11 10:36:20', 'c1a311533b4ca3996a13e2cca38be3c6db4515c1', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'jobspot.jpg', 'data/attachments/module/site/block/news-list/1445/c1a311533b4ca3996a13e2cca38be3c6db4515c1.jpg'),
(406, 405, 1, 36844, NULL, NULL, '2014-04-11 10:36:20', '2014-04-11 10:36:20', '0507caa0c1a71745eef9e2547891cbb7d142a4c5', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'jobspot.jpg', 'data/attachments/module/site/block/news-list/1445/0507caa0c1a71745eef9e2547891cbb7d142a4c5.jpg'),
(407, NULL, 1, 19459, NULL, 1, '2014-04-11 10:36:36', '2014-04-11 10:36:36', '6db145629e3c10e05dd59f3d1c644b1048d4965e', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'shake.jpg', 'data/attachments/module/site/block/news-list/1445/6db145629e3c10e05dd59f3d1c644b1048d4965e.jpg'),
(408, 407, 1, 19905, NULL, NULL, '2014-04-11 10:36:36', '2014-04-11 10:36:36', '7f70bdc52ee9ff2bf63fd6ede6b767bb0020bfb1', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'shake.jpg', 'data/attachments/module/site/block/news-list/1445/7f70bdc52ee9ff2bf63fd6ede6b767bb0020bfb1.jpg'),
(409, NULL, 1, 15560, NULL, 1, '2014-04-11 10:36:53', '2014-04-11 10:36:53', '96e19f538c18243f56221a9c40d717459d9716f9', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'kapital-int.jpg', 'data/attachments/module/site/block/news-list/1445/96e19f538c18243f56221a9c40d717459d9716f9.jpg'),
(410, 409, 1, 15760, NULL, NULL, '2014-04-11 10:36:53', '2014-04-11 10:36:53', '05de51b14266c93c67c21e67370a63f5144ed13d', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'kapital-int.jpg', 'data/attachments/module/site/block/news-list/1445/05de51b14266c93c67c21e67370a63f5144ed13d.jpg'),
(411, NULL, 1, 53271, NULL, 1, '2014-04-11 10:54:53', '2014-04-11 10:54:53', '9342418116d9219c2aebfbf7e61ecaa769a7a9f6', 'jpg', 'image/jpeg', 'site', 'images-pile', '', '11311945.jpg', 'data/attachments/module/site/block/images-pile/2364/9342418116d9219c2aebfbf7e61ecaa769a7a9f6.jpg'),
(412, 411, 1, 48087, NULL, NULL, '2014-04-11 10:54:53', '2014-04-11 10:54:53', '7e949d25ada351c271e6468978613a7c53a02a58', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', '11311945.jpg', 'data/attachments/module/site/block/images-pile/2364/7e949d25ada351c271e6468978613a7c53a02a58.jpg'),
(413, NULL, 1, 78526, NULL, 1, '2014-04-11 11:03:09', '2014-04-11 11:03:09', '90c8fc584e208d7fb08983dbd66bf57dc3306aec', 'jpg', 'image/jpeg', 'site', 'images-pile', '', '36515375.jpg', 'data/attachments/module/site/block/images-pile/2364/90c8fc584e208d7fb08983dbd66bf57dc3306aec.jpg'),
(414, 413, 1, 60284, NULL, NULL, '2014-04-11 11:03:10', '2014-04-11 11:03:09', '15f701a2a2bf7ea004f82b877679701069f8d240', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', '36515375.jpg', 'data/attachments/module/site/block/images-pile/2364/15f701a2a2bf7ea004f82b877679701069f8d240.jpg'),
(415, NULL, 1, 53139, NULL, 1, '2014-04-11 11:04:27', '2014-04-11 11:04:27', '19a229d460a86ee18bbd1b968293ebab12256437', 'jpg', 'image/jpeg', 'site', 'images-pile', '', '30613921.jpg', 'data/attachments/module/site/block/images-pile/2364/19a229d460a86ee18bbd1b968293ebab12256437.jpg'),
(416, 415, 1, 39553, NULL, NULL, '2014-04-11 11:04:27', '2014-04-11 11:04:27', '8e9ede875ad4408b4ca17102f50e2ac42ff9d358', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', '30613921.jpg', 'data/attachments/module/site/block/images-pile/2364/8e9ede875ad4408b4ca17102f50e2ac42ff9d358.jpg'),
(417, NULL, 1, 69466, NULL, 1, '2014-04-11 11:06:13', '2014-04-11 11:06:13', 'd74b7732513803c6f402cbcd98c920f0c9668b3d', 'jpg', 'image/jpeg', 'site', 'images-pile', '', '15398947.jpg', 'data/attachments/module/site/block/images-pile/2364/d74b7732513803c6f402cbcd98c920f0c9668b3d.jpg'),
(418, 417, 1, 51713, NULL, NULL, '2014-04-11 11:06:13', '2014-04-11 11:06:13', '481d656cb9a17d65fefa0fe9a48534193cdef6ce', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', '15398947.jpg', 'data/attachments/module/site/block/images-pile/2364/481d656cb9a17d65fefa0fe9a48534193cdef6ce.jpg'),
(419, NULL, 1, 50783, NULL, 1, '2014-04-11 11:08:22', '2014-04-11 11:08:22', '48ea5cfccf600503c7548e3f7a445823e2346e59', 'jpg', 'image/jpeg', 'site', 'images-pile', '', '13942466.jpg', 'data/attachments/module/site/block/images-pile/2364/48ea5cfccf600503c7548e3f7a445823e2346e59.jpg'),
(420, 419, 1, 45001, NULL, NULL, '2014-04-11 11:08:22', '2014-04-11 11:08:22', 'fca71dfc28797803db89e1a2a20c1be88aa2dd12', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', '13942466.jpg', 'data/attachments/module/site/block/images-pile/2364/fca71dfc28797803db89e1a2a20c1be88aa2dd12.jpg'),
(421, NULL, 1, 56564, NULL, 1, '2014-04-11 11:10:46', '2014-04-11 11:10:46', 'a5ab5c8396fae2ac74bd11f4857c5af1fa7cbad3', 'jpg', 'image/jpeg', 'site', 'images-pile', '', '30604345.jpg', 'data/attachments/module/site/block/images-pile/2364/a5ab5c8396fae2ac74bd11f4857c5af1fa7cbad3.jpg'),
(422, 421, 1, 43291, NULL, NULL, '2014-04-11 11:10:46', '2014-04-11 11:10:46', '693cf8a6598340c3a6eb46c0bc5a4a59384da477', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', '30604345.jpg', 'data/attachments/module/site/block/images-pile/2364/693cf8a6598340c3a6eb46c0bc5a4a59384da477.jpg'),
(423, NULL, 1, 46121, NULL, 1, '2014-04-11 11:11:05', '2014-04-11 11:11:05', '44b09aee579e7b40c0fdbc5b03cdc577804ff109', 'jpg', 'image/jpeg', 'site', 'images-pile', '', '12033717.jpg', 'data/attachments/module/site/block/images-pile/2364/44b09aee579e7b40c0fdbc5b03cdc577804ff109.jpg'),
(424, 423, 1, 39744, NULL, NULL, '2014-04-11 11:11:05', '2014-04-11 11:11:05', '98b41bfb2aa53c57533498f7ba25ec129143171c', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', '12033717.jpg', 'data/attachments/module/site/block/images-pile/2364/98b41bfb2aa53c57533498f7ba25ec129143171c.jpg'),
(425, NULL, 1, 47042, NULL, 1, '2014-04-11 11:15:36', '2014-04-11 11:15:36', 'a8537eac8865d1430ca78390579e7690e445ba0c', 'jpg', 'image/jpeg', 'site', 'images-pile', '', '21171597.jpg', 'data/attachments/module/site/block/images-pile/2364/a8537eac8865d1430ca78390579e7690e445ba0c.jpg'),
(426, 425, 1, 43393, NULL, NULL, '2014-04-11 11:15:36', '2014-04-11 11:15:36', '85086bec41205c7443fc1cb2e791fba45397d495', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', '21171597.jpg', 'data/attachments/module/site/block/images-pile/2364/85086bec41205c7443fc1cb2e791fba45397d495.jpg'),
(427, NULL, 1, 33130, NULL, 1, '2014-04-11 11:34:02', '2014-04-11 11:34:02', '40f4f3853d3cf15f4316e85bd8e2b98aedc52cf1', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'foto.jpg', 'data/attachments/module/site/block/news-list/1445/40f4f3853d3cf15f4316e85bd8e2b98aedc52cf1.jpg'),
(428, 427, 1, 30687, NULL, NULL, '2014-04-11 11:34:11', '2014-04-11 11:34:02', '2e41d990eb6283d47a81b0d6709c1261e81cda88', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'foto.jpg', 'data/attachments/module/site/block/news-list/1445/2e41d990eb6283d47a81b0d6709c1261e81cda88.jpg'),
(429, NULL, 1, 35778, NULL, 1, '2014-04-11 11:35:41', '2014-04-11 11:35:41', '7cd6ec333c5dcae6e1c33787d235a8d427c2a90b', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'info2.jpg', 'data/attachments/module/site/block/news-list/1445/7cd6ec333c5dcae6e1c33787d235a8d427c2a90b.jpg'),
(430, 429, 1, 33775, NULL, NULL, '2014-04-11 11:35:46', '2014-04-11 11:35:41', '6d66374e3f8afebd7d11f914e0474f696a042ed1', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'info2.jpg', 'data/attachments/module/site/block/news-list/1445/6d66374e3f8afebd7d11f914e0474f696a042ed1.jpg'),
(431, NULL, 1, 40152, NULL, 1, '2014-04-11 11:35:55', '2014-04-11 11:35:55', 'a1dce207be398f9efbba23db0461ce7bb5a0abea', 'jpg', 'image/jpeg', 'site', 'images-pile', '', '13578346.jpg', 'data/attachments/module/site/block/images-pile/2364/a1dce207be398f9efbba23db0461ce7bb5a0abea.jpg'),
(432, 431, 1, 38561, NULL, NULL, '2014-04-11 11:35:55', '2014-04-11 11:35:55', 'ba31613d393c5ab8819d79a42dd6dec163124995', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', '13578346.jpg', 'data/attachments/module/site/block/images-pile/2364/ba31613d393c5ab8819d79a42dd6dec163124995.jpg'),
(433, NULL, 1, 50158, NULL, 1, '2014-04-11 11:58:51', '2014-04-11 11:58:51', 'cf7b291392e0defd9565552641ff60266694e316', 'jpg', 'image/jpeg', 'site', 'images-pile', '', '41587451.jpg', 'data/attachments/module/site/block/images-pile/2364/cf7b291392e0defd9565552641ff60266694e316.jpg'),
(434, 433, 1, 35037, NULL, NULL, '2014-04-11 11:58:51', '2014-04-11 11:58:51', '947ee2be1d365e1cff2d5a07e9f84c9b458cd56f', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', '41587451.jpg', 'data/attachments/module/site/block/images-pile/2364/947ee2be1d365e1cff2d5a07e9f84c9b458cd56f.jpg'),
(435, NULL, 1, 46962, NULL, 1, '2014-04-11 12:19:52', '2014-04-11 12:19:52', '2cece52694b2d28bb360478fd8b839a145513481', 'jpg', 'image/jpeg', 'site', 'images-pile', '', '12286955.jpg', 'data/attachments/module/site/block/images-pile/2364/2cece52694b2d28bb360478fd8b839a145513481.jpg'),
(436, 435, 1, 45602, NULL, NULL, '2014-04-11 12:19:52', '2014-04-11 12:19:52', '0b2e657eb632b1750f56128ada0dfe018ae5dbd4', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', '12286955.jpg', 'data/attachments/module/site/block/images-pile/2364/0b2e657eb632b1750f56128ada0dfe018ae5dbd4.jpg'),
(437, NULL, 1, 8502, NULL, 1, '2014-04-11 12:50:05', '2014-04-11 12:50:05', '84f4a8e17ad5e7592e03a95ca5d903552b029ea2', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'PIH.jpg', 'data/attachments/module/site/block/image-slider/2198/84f4a8e17ad5e7592e03a95ca5d903552b029ea2.jpg'),
(438, 437, 1, 7165, NULL, NULL, '2014-04-11 12:50:11', '2014-04-11 12:50:05', '55da563f2e76b0d620846ffe9280103ae1a5c708', 'jpg', 'image/jpeg', 'site', 'image-slider', '58,0,193,75', 'PIH.jpg', 'data/attachments/module/site/block/image-slider/2198/55da563f2e76b0d620846ffe9280103ae1a5c708.jpg'),
(439, NULL, 1, 62114, NULL, 1, '2014-04-11 18:21:38', '2014-04-11 18:21:38', '3eb6739a82ab8fed366e12fd8fd51a3a1cbf531c', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'PR7_7862_stitch.jpg', 'data/attachments/module/site/block/image-slider/2381/3eb6739a82ab8fed366e12fd8fd51a3a1cbf531c.jpg'),
(440, 439, 1, 36049, NULL, NULL, '2014-04-11 18:21:38', '2014-04-11 18:21:38', 'a5bc58bdab756c2e64ee811778caf1d6de56fc70', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', 'PR7_7862_stitch.jpg', 'data/attachments/module/site/block/image-slider/2381/a5bc58bdab756c2e64ee811778caf1d6de56fc70.jpg'),
(441, NULL, 1, 48995, NULL, 1, '2014-04-11 18:25:05', '2014-04-11 18:25:05', 'fc8b38581ba525351ef4edef7bbfd93e4cd91f2b', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'AFiB V_1.jpg', 'data/attachments/module/site/block/image-slider/2381/fc8b38581ba525351ef4edef7bbfd93e4cd91f2b.jpg'),
(442, 441, 1, 46018, NULL, NULL, '2014-04-11 18:25:05', '2014-04-11 18:25:05', 'fd8ffdd99cb7698b2679a229dd595b211a18921e', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', 'AFiB V_1.jpg', 'data/attachments/module/site/block/image-slider/2381/fd8ffdd99cb7698b2679a229dd595b211a18921e.jpg'),
(443, NULL, 1, 47767, NULL, 1, '2014-04-11 18:25:05', '2014-04-11 18:25:05', 'b86225bbbbb5102983d11a70d33829cea419ec4f', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'AFIBV_3.jpg', 'data/attachments/module/site/block/image-slider/2381/b86225bbbbb5102983d11a70d33829cea419ec4f.jpg'),
(444, 443, 1, 43128, NULL, NULL, '2014-04-11 18:25:05', '2014-04-11 18:25:05', '0d112be79e0fec612bceca079447614b49d70929', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', 'AFIBV_3.jpg', 'data/attachments/module/site/block/image-slider/2381/0d112be79e0fec612bceca079447614b49d70929.jpg'),
(445, NULL, 1, 40964, NULL, 1, '2014-04-11 18:25:06', '2014-04-11 18:25:06', '98f190ba0b58a6c1cf4df479e49a9f0f27751d76', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'AFIBV_5.jpg', 'data/attachments/module/site/block/image-slider/2381/98f190ba0b58a6c1cf4df479e49a9f0f27751d76.jpg'),
(446, 445, 1, 37185, NULL, NULL, '2014-04-11 18:25:06', '2014-04-11 18:25:06', 'edc2282117b7a085ca1887724c3f2de138dfdcac', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', 'AFIBV_5.jpg', 'data/attachments/module/site/block/image-slider/2381/edc2282117b7a085ca1887724c3f2de138dfdcac.jpg'),
(447, NULL, 1, 49107, NULL, 1, '2014-04-11 18:25:06', '2014-04-11 18:25:06', '15729731b388d216ba3ae6578a3484482e8af556', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'AFIBV_7.jpg', 'data/attachments/module/site/block/image-slider/2381/15729731b388d216ba3ae6578a3484482e8af556.jpg'),
(448, 447, 1, 46057, NULL, NULL, '2014-04-11 18:25:06', '2014-04-11 18:25:06', '22bcaa3939f0c8a0d0fb71f4c4be6f4a11531b4d', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', 'AFIBV_7.jpg', 'data/attachments/module/site/block/image-slider/2381/22bcaa3939f0c8a0d0fb71f4c4be6f4a11531b4d.jpg'),
(449, NULL, 1, 46163, NULL, 1, '2014-04-11 18:25:06', '2014-04-11 18:25:06', '3ac826861d564f389729e6c69de70efae08d7401', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'AFIBV-4.jpg', 'data/attachments/module/site/block/image-slider/2381/3ac826861d564f389729e6c69de70efae08d7401.jpg'),
(450, 449, 1, 42938, NULL, NULL, '2014-04-11 18:25:06', '2014-04-11 18:25:06', '7904d46d348523cadb2642efe8636e5344f3d578', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', 'AFIBV-4.jpg', 'data/attachments/module/site/block/image-slider/2381/7904d46d348523cadb2642efe8636e5344f3d578.jpg'),
(451, NULL, 1, 48995, NULL, 1, '2014-04-11 18:25:35', '2014-04-11 18:25:35', '7642e032b4c83a3e07ca5efb4a905d7750b25ef1', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'AFiB V_1.jpg', 'data/attachments/module/site/block/image-slider/2381/7642e032b4c83a3e07ca5efb4a905d7750b25ef1.jpg'),
(452, 451, 1, 46018, NULL, NULL, '2014-04-11 18:25:35', '2014-04-11 18:25:35', 'a55479acbb8c3e1393621de58b71252872b0e216', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', 'AFiB V_1.jpg', 'data/attachments/module/site/block/image-slider/2381/a55479acbb8c3e1393621de58b71252872b0e216.jpg'),
(453, NULL, 1, 49107, NULL, 1, '2014-04-11 18:25:52', '2014-04-11 18:25:52', '479709047f352e20eb4641c4b2f5673838e80026', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'AFIBV_7.jpg', 'data/attachments/module/site/block/image-slider/2381/479709047f352e20eb4641c4b2f5673838e80026.jpg'),
(454, 453, 1, 46057, NULL, NULL, '2014-04-11 18:25:52', '2014-04-11 18:25:52', '1baf05d576fe259bd151d2229f6db55cc41b7594', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', 'AFIBV_7.jpg', 'data/attachments/module/site/block/image-slider/2381/1baf05d576fe259bd151d2229f6db55cc41b7594.jpg'),
(455, NULL, 1, 46414, NULL, 1, '2014-04-11 20:36:52', '2014-04-11 20:36:52', '9eb33a7b163dbaee285601b1b13054a5ec35a913', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'ChromaStock_12286955.jpg', 'data/attachments/module/site/block/images-pile/2360/9eb33a7b163dbaee285601b1b13054a5ec35a913.jpg'),
(456, 455, 1, 45028, NULL, NULL, '2014-04-11 20:36:52', '2014-04-11 20:36:52', 'c517d71a854c976be9aa309242b5bade0b529f14', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'ChromaStock_12286955.jpg', 'data/attachments/module/site/block/images-pile/2360/c517d71a854c976be9aa309242b5bade0b529f14.jpg'),
(457, NULL, 1, 37507, NULL, 1, '2014-04-11 20:38:25', '2014-04-11 20:38:25', '5992106343004bd62a216e20deefcec7b156112a', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'ChromaStock_13578346.jpg', 'data/attachments/module/site/block/images-pile/2360/5992106343004bd62a216e20deefcec7b156112a.jpg'),
(458, 457, 1, 35783, NULL, NULL, '2014-04-11 20:38:25', '2014-04-11 20:38:25', '73ce138b454b52ddb483b67bbd29c7396717a12e', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'ChromaStock_13578346.jpg', 'data/attachments/module/site/block/images-pile/2360/73ce138b454b52ddb483b67bbd29c7396717a12e.jpg'),
(459, NULL, 1, 51206, NULL, 1, '2014-04-11 20:39:37', '2014-04-11 20:39:37', '283a9b71e2d421defcb3e235fc3ed0e77cd8e9af', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'ChromaStock_30613921.jpg', 'data/attachments/module/site/block/images-pile/2360/283a9b71e2d421defcb3e235fc3ed0e77cd8e9af.jpg'),
(460, 459, 1, 37968, NULL, NULL, '2014-04-11 20:39:37', '2014-04-11 20:39:37', '0a828a20de8609bc51be1a59f0ded716ffd3f824', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'ChromaStock_30613921.jpg', 'data/attachments/module/site/block/images-pile/2360/0a828a20de8609bc51be1a59f0ded716ffd3f824.jpg'),
(461, NULL, 1, 68980, NULL, 1, '2014-04-11 20:40:22', '2014-04-11 20:40:22', '9cfe809a3cea1b291ab05356c63fcd58447d0034', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'ChromaStock_15398947.jpg', 'data/attachments/module/site/block/images-pile/2360/9cfe809a3cea1b291ab05356c63fcd58447d0034.jpg'),
(462, 461, 1, 51186, NULL, NULL, '2014-04-11 20:40:22', '2014-04-11 20:40:22', 'c3cec4add46f3658ab11e5b60cbea2d153114d5f', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'ChromaStock_15398947.jpg', 'data/attachments/module/site/block/images-pile/2360/c3cec4add46f3658ab11e5b60cbea2d153114d5f.jpg'),
(463, NULL, 1, 54985, NULL, 1, '2014-04-11 20:41:14', '2014-04-11 20:41:14', '2d317046a678cf70defd0fde95c97b10e58aa47a', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'ChromaStock_30604345.jpg', 'data/attachments/module/site/block/images-pile/2360/2d317046a678cf70defd0fde95c97b10e58aa47a.jpg'),
(464, 463, 1, 42073, NULL, NULL, '2014-04-11 20:41:14', '2014-04-11 20:41:14', '96ba93839f96b26dffe578b3d9510242db6125c9', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'ChromaStock_30604345.jpg', 'data/attachments/module/site/block/images-pile/2360/96ba93839f96b26dffe578b3d9510242db6125c9.jpg'),
(465, NULL, 1, 47149, NULL, 1, '2014-04-11 20:43:18', '2014-04-11 20:43:18', '17cf088a0fd49834e51c74640203d7c089d96d69', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'ChromaStock_21171597.jpg', 'data/attachments/module/site/block/images-pile/2360/17cf088a0fd49834e51c74640203d7c089d96d69.jpg'),
(466, 465, 1, 43158, NULL, NULL, '2014-04-11 20:43:18', '2014-04-11 20:43:18', 'b4858b6792f66df848525d85147ce07d57d53a55', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'ChromaStock_21171597.jpg', 'data/attachments/module/site/block/images-pile/2360/b4858b6792f66df848525d85147ce07d57d53a55.jpg'),
(467, NULL, 1, 53599, NULL, 1, '2014-04-11 20:44:37', '2014-04-11 20:44:37', 'b89a49e88d3b62c1a2ddc09d7a077233bf447df4', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'ChromaStock_11311945.jpg', 'data/attachments/module/site/block/images-pile/2360/b89a49e88d3b62c1a2ddc09d7a077233bf447df4.jpg'),
(468, 467, 1, 48378, NULL, NULL, '2014-04-11 20:44:37', '2014-04-11 20:44:37', 'ce47d2115670a8b0d23847331fba2189864e82ef', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'ChromaStock_11311945.jpg', 'data/attachments/module/site/block/images-pile/2360/ce47d2115670a8b0d23847331fba2189864e82ef.jpg'),
(469, NULL, 1, 49864, NULL, 1, '2014-04-11 20:48:04', '2014-04-11 20:48:04', 'fe4c092ee73faf0026923d4d1be46e060749a35b', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'ChromaStock_13942466 (Kopiowanie).jpg', 'data/attachments/module/site/block/images-pile/2360/fe4c092ee73faf0026923d4d1be46e060749a35b.jpg'),
(470, 469, 1, 44523, NULL, NULL, '2014-04-11 20:48:04', '2014-04-11 20:48:04', '00e7d786a8c0df150b709c6ce4ba7cc90971b0c2', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'ChromaStock_13942466 (Kopiowanie).jpg', 'data/attachments/module/site/block/images-pile/2360/00e7d786a8c0df150b709c6ce4ba7cc90971b0c2.jpg'),
(471, NULL, 1, 49412, NULL, 1, '2014-04-11 20:51:38', '2014-04-11 20:51:38', '014515bc36b62caf5e156ad78972f3144a09d212', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'ChromaStock_13942466 (Kopiowanie).jpg', 'data/attachments/module/site/block/images-pile/2360/014515bc36b62caf5e156ad78972f3144a09d212.jpg'),
(472, 471, 1, 43784, NULL, NULL, '2014-04-11 20:51:38', '2014-04-11 20:51:38', '12c3d09d476e7b54c9e9f4bad93236e3205bb98e', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'ChromaStock_13942466 (Kopiowanie).jpg', 'data/attachments/module/site/block/images-pile/2360/12c3d09d476e7b54c9e9f4bad93236e3205bb98e.jpg'),
(473, NULL, 1, 49412, NULL, 1, '2014-04-11 20:54:01', '2014-04-11 20:54:01', 'f6ff6fb075126549aff3260daeab6c6a5f150330', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'ChromaStock_13942466 (Kopiowanie).jpg', 'data/attachments/module/site/block/images-pile/2360/f6ff6fb075126549aff3260daeab6c6a5f150330.jpg'),
(474, 473, 1, 43784, NULL, NULL, '2014-04-11 20:54:01', '2014-04-11 20:54:01', '771d8d320d519a54ae15490db4c00fcc5b591199', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'ChromaStock_13942466 (Kopiowanie).jpg', 'data/attachments/module/site/block/images-pile/2360/771d8d320d519a54ae15490db4c00fcc5b591199.jpg'),
(475, NULL, 1, 79340, NULL, 1, '2014-04-11 20:56:11', '2014-04-11 20:56:11', '579a5705e4590e725a06ffca936e60d2446f8ff9', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'ChromaStock_36515375.jpg', 'data/attachments/module/site/block/images-pile/2360/579a5705e4590e725a06ffca936e60d2446f8ff9.jpg'),
(476, 475, 1, 60786, NULL, NULL, '2014-04-11 20:56:11', '2014-04-11 20:56:11', '52cdebdba46b06331b5f04a9a0a2b4853384dba0', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'ChromaStock_36515375.jpg', 'data/attachments/module/site/block/images-pile/2360/52cdebdba46b06331b5f04a9a0a2b4853384dba0.jpg'),
(477, NULL, 1, 51704, NULL, 1, '2014-04-11 20:57:53', '2014-04-11 20:57:53', 'cc161045956753392d017fbe2ce298d179c8a44e', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'ChromaStock_41587451.jpg', 'data/attachments/module/site/block/images-pile/2360/cc161045956753392d017fbe2ce298d179c8a44e.jpg'),
(478, 477, 1, 40117, NULL, NULL, '2014-04-11 20:58:17', '2014-04-11 20:57:53', '509a95ebe390c455ac7c65b8593145499427af60', 'jpg', 'image/jpeg', 'site', 'images-pile', '57,0,307,188', 'ChromaStock_41587451.jpg', 'data/attachments/module/site/block/images-pile/2360/509a95ebe390c455ac7c65b8593145499427af60.jpg'),
(479, NULL, 1, 44730, NULL, 1, '2014-04-11 21:06:06', '2014-04-11 21:06:06', '63e49b970384a7fba031d1e32a1c492978fb484f', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'dziennikarstwo.jpg', 'data/attachments/module/site/block/images-pile/2360/63e49b970384a7fba031d1e32a1c492978fb484f.jpg'),
(480, 479, 1, 44969, NULL, NULL, '2014-04-11 21:06:06', '2014-04-11 21:06:06', '899a329e897d3f88c5a4abb26fc70ad636d2adb5', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'dziennikarstwo.jpg', 'data/attachments/module/site/block/images-pile/2360/899a329e897d3f88c5a4abb26fc70ad636d2adb5.jpg'),
(481, NULL, 1, 35467, NULL, 1, '2014-04-11 21:08:10', '2014-04-11 21:08:10', '690baafd8280d32bd628274e972dd15f11bb3580', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'Fotolia_48899050_X.jpg', 'data/attachments/module/site/block/bachelor-studies/2037/690baafd8280d32bd628274e972dd15f11bb3580.jpg'),
(482, 481, 1, 22662, NULL, NULL, '2014-04-11 21:08:10', '2014-04-11 21:08:10', 'c986f6127ffdcd3395735b2dc4d958f72477aafc', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'Fotolia_48899050_X.jpg', 'data/attachments/module/site/block/bachelor-studies/2037/c986f6127ffdcd3395735b2dc4d958f72477aafc.jpg'),
(483, NULL, 1, 27812, NULL, 1, '2014-04-11 21:13:15', '2014-04-11 21:13:15', '2850092b8ad639729de01aeab1d4e40b9b5d435a', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'ekonomia.jpg', 'data/attachments/module/site/block/bachelor-studies/2037/2850092b8ad639729de01aeab1d4e40b9b5d435a.jpg'),
(484, 483, 1, 28118, NULL, NULL, '2014-04-11 21:13:15', '2014-04-11 21:13:15', '80ca9e86461a07c2542868874bae2ec7260aff90', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'ekonomia.jpg', 'data/attachments/module/site/block/bachelor-studies/2037/80ca9e86461a07c2542868874bae2ec7260aff90.jpg'),
(485, NULL, 1, 26135, NULL, 1, '2014-04-11 21:14:42', '2014-04-11 21:14:42', '2fd0e16104a88c104cf310ccc43bad673339ac92', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'ekonomia1.jpg', 'data/attachments/module/site/block/bachelor-studies/2037/2fd0e16104a88c104cf310ccc43bad673339ac92.jpg'),
(486, 485, 1, 26463, NULL, NULL, '2014-04-11 21:14:42', '2014-04-11 21:14:42', 'e119831df4bd225e267fe0da50e53352d79e1bd7', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'ekonomia1.jpg', 'data/attachments/module/site/block/bachelor-studies/2037/e119831df4bd225e267fe0da50e53352d79e1bd7.jpg'),
(487, NULL, 1, 50589, NULL, 1, '2014-04-11 21:18:26', '2014-04-11 21:18:26', '1a48e977bb1a052ef92ddb0650fde9a68913c830', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'ChromaStock_12286955.jpg', 'data/attachments/module/site/block/bachelor-studies/2144/1a48e977bb1a052ef92ddb0650fde9a68913c830.jpg'),
(488, 487, 1, 38295, NULL, NULL, '2014-04-11 21:18:26', '2014-04-11 21:18:26', 'a9b419436c44f378b4e3ee793d098c5b5024aecc', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'ChromaStock_12286955.jpg', 'data/attachments/module/site/block/bachelor-studies/2144/a9b419436c44f378b4e3ee793d098c5b5024aecc.jpg'),
(489, NULL, 1, 50589, NULL, 1, '2014-04-11 21:20:19', '2014-04-11 21:20:19', '723efcc6d22651e4eaa072e730a221701595c7e2', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'ChromaStock_12286955.jpg', 'data/attachments/module/site/block/bachelor-studies/2144/723efcc6d22651e4eaa072e730a221701595c7e2.jpg'),
(490, 489, 1, 38295, NULL, NULL, '2014-04-11 21:20:19', '2014-04-11 21:20:19', '4a2f504843527e1f36f5c53e85c8c599fb8a2fb4', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'ChromaStock_12286955.jpg', 'data/attachments/module/site/block/bachelor-studies/2144/4a2f504843527e1f36f5c53e85c8c599fb8a2fb4.jpg'),
(491, NULL, 1, 22581, NULL, 1, '2014-04-11 22:44:23', '2014-04-11 22:44:23', '67a898b6b6b5ffb0889eb47a6d4ca1b0ed714880', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'api_thumb_450 (4).jpg', 'data/attachments/module/site/block/bachelor-studies/2149/67a898b6b6b5ffb0889eb47a6d4ca1b0ed714880.jpg'),
(492, 491, 1, 22852, NULL, NULL, '2014-04-11 22:44:23', '2014-04-11 22:44:23', 'fc0252ce88b101cf5f5b49c80147253604c512cf', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'api_thumb_450 (4).jpg', 'data/attachments/module/site/block/bachelor-studies/2149/fc0252ce88b101cf5f5b49c80147253604c512cf.jpg'),
(493, NULL, 1, 21033, NULL, 1, '2014-04-11 22:44:44', '2014-04-11 22:44:44', '5facd3ebabc9fd49999bc3b91e368c82f1682aa9', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', '26579063.jpg', 'data/attachments/module/site/block/bachelor-studies/2149/5facd3ebabc9fd49999bc3b91e368c82f1682aa9.jpg'),
(494, 493, 1, 21383, NULL, NULL, '2014-04-11 22:44:44', '2014-04-11 22:44:44', '5215919b0215fb1f85b39e604372caa35506454e', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', '26579063.jpg', 'data/attachments/module/site/block/bachelor-studies/2149/5215919b0215fb1f85b39e604372caa35506454e.jpg'),
(495, NULL, 1, 22581, NULL, 1, '2014-04-12 01:14:20', '2014-04-12 01:14:20', 'a6389509a3b85775aeac9fd60792ecd79349a800', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'api_thumb_450 (4).jpg', 'data/attachments/module/site/block/bachelor-studies/2149/a6389509a3b85775aeac9fd60792ecd79349a800.jpg'),
(496, 495, 1, 22852, NULL, NULL, '2014-04-12 01:14:20', '2014-04-12 01:14:20', 'ee48f7b19a5e2b9c6ce46fb1fd7bb4777bb0b271', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'api_thumb_450 (4).jpg', 'data/attachments/module/site/block/bachelor-studies/2149/ee48f7b19a5e2b9c6ce46fb1fd7bb4777bb0b271.jpg'),
(497, NULL, 1, 36005, NULL, 1, '2014-04-12 01:14:43', '2014-04-12 01:14:43', '0fe8754ce3b7eb3535825b7e83e30402a840f087', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'api_thumb_450 (3).jpg', 'data/attachments/module/site/block/bachelor-studies/2149/0fe8754ce3b7eb3535825b7e83e30402a840f087.jpg'),
(498, 497, 1, 29373, NULL, NULL, '2014-04-12 01:14:43', '2014-04-12 01:14:43', '2fb63c1d68ca703ca65e156485be18814713e46f', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'api_thumb_450 (3).jpg', 'data/attachments/module/site/block/bachelor-studies/2149/2fb63c1d68ca703ca65e156485be18814713e46f.jpg'),
(499, NULL, 1, 42382, NULL, 1, '2014-04-12 01:15:03', '2014-04-12 01:15:03', '3cdd2cbe3fe78498a8a757005aa8916994ea6d0d', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'api_thumb_450 (2).jpg', 'data/attachments/module/site/block/bachelor-studies/2149/3cdd2cbe3fe78498a8a757005aa8916994ea6d0d.jpg'),
(500, 499, 1, 29810, NULL, NULL, '2014-04-12 01:15:03', '2014-04-12 01:15:03', '137000da0c5545d17d4804cf6e79184d744448ba', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'api_thumb_450 (2).jpg', 'data/attachments/module/site/block/bachelor-studies/2149/137000da0c5545d17d4804cf6e79184d744448ba.jpg');
INSERT INTO `attachments` (`attachment_id`, `parent_attachment_id`, `is_published`, `file_size`, `page_block_id`, `last_update_author_id`, `last_update_date`, `creation_date`, `hash_for_file_name`, `extension`, `mime_type`, `module_name`, `block_name`, `image_crop_rect_coords`, `original_file_name`, `file_path`) VALUES
(501, NULL, 1, 32580, NULL, 1, '2014-04-12 02:38:32', '2014-04-12 02:38:32', '8fe91d3e63a751e6f12afaa9279b3e95caac59c7', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'Fotolia_48899050_X.jpg', 'data/attachments/module/site/block/images-pile/2366/8fe91d3e63a751e6f12afaa9279b3e95caac59c7.jpg'),
(502, 501, 1, 29682, NULL, NULL, '2014-04-12 02:38:32', '2014-04-12 02:38:32', 'efc52cc5de7efdceac12a48b0814c1d57e7129fe', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'Fotolia_48899050_X.jpg', 'data/attachments/module/site/block/images-pile/2366/efc52cc5de7efdceac12a48b0814c1d57e7129fe.jpg'),
(503, NULL, 1, 51704, NULL, 1, '2014-04-12 02:40:59', '2014-04-12 02:40:59', '082f22a6414c9947b9bfd607c8a540d2b86e0250', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'ChromaStock_41587451.jpg', 'data/attachments/module/site/block/images-pile/2366/082f22a6414c9947b9bfd607c8a540d2b86e0250.jpg'),
(504, 503, 1, 39524, NULL, NULL, '2014-04-12 02:41:09', '2014-04-12 02:40:59', 'b4fdf8613028acb311ff8a3fd7244df7771752af', 'jpg', 'image/jpeg', 'site', 'images-pile', '67,0,317,188', 'ChromaStock_41587451.jpg', 'data/attachments/module/site/block/images-pile/2366/b4fdf8613028acb311ff8a3fd7244df7771752af.jpg'),
(505, NULL, 1, 45437, NULL, 1, '2014-04-12 02:47:28', '2014-04-12 02:47:28', '1b9bb137db6cf96ea67d19527636eac4698b646d', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'ChromaStock_7962456.jpg', 'data/attachments/module/site/block/images-pile/2368/1b9bb137db6cf96ea67d19527636eac4698b646d.jpg'),
(506, 505, 1, 41201, NULL, NULL, '2014-04-12 02:47:28', '2014-04-12 02:47:28', '361690d3dcc760f22737b93269e9b3055352f5c2', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'ChromaStock_7962456.jpg', 'data/attachments/module/site/block/images-pile/2368/361690d3dcc760f22737b93269e9b3055352f5c2.jpg'),
(507, NULL, 1, 35782, NULL, 1, '2014-04-12 02:48:15', '2014-04-12 02:48:15', 'a07326eb47963b72af738b4135822c2e0e84e415', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'ChromaStock_25671165.jpg', 'data/attachments/module/site/block/images-pile/2368/a07326eb47963b72af738b4135822c2e0e84e415.jpg'),
(508, 507, 1, 35964, NULL, NULL, '2014-04-12 02:48:15', '2014-04-12 02:48:15', 'f7975842fc74f61295c04331021b528dd9b84690', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'ChromaStock_25671165.jpg', 'data/attachments/module/site/block/images-pile/2368/f7975842fc74f61295c04331021b528dd9b84690.jpg'),
(509, NULL, 1, 46414, NULL, 1, '2014-04-12 02:48:56', '2014-04-12 02:48:56', '2ca0cc23b3a3fae7e0da88d9e60b8cb806e03187', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'ChromaStock_12286955.jpg', 'data/attachments/module/site/block/images-pile/2368/2ca0cc23b3a3fae7e0da88d9e60b8cb806e03187.jpg'),
(510, 509, 1, 45028, NULL, NULL, '2014-04-12 02:48:56', '2014-04-12 02:48:56', 'c8ec763beb53e2dc2381b19bddb2646be0c93412', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'ChromaStock_12286955.jpg', 'data/attachments/module/site/block/images-pile/2368/c8ec763beb53e2dc2381b19bddb2646be0c93412.jpg'),
(511, NULL, 1, 51436, NULL, 1, '2014-04-12 02:50:35', '2014-04-12 02:50:35', 'ea53629bd3985596ab231060f7abaa5a284b597d', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'ChromaStock_11311277.jpg', 'data/attachments/module/site/block/images-pile/2368/ea53629bd3985596ab231060f7abaa5a284b597d.jpg'),
(512, 511, 1, 47041, NULL, NULL, '2014-04-12 02:50:35', '2014-04-12 02:50:35', '4f4d0a5987d0c6de0def6257f97104cf29466bee', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'ChromaStock_11311277.jpg', 'data/attachments/module/site/block/images-pile/2368/4f4d0a5987d0c6de0def6257f97104cf29466bee.jpg'),
(513, NULL, 1, 49330, NULL, 1, '2014-04-12 03:07:35', '2014-04-12 03:07:35', '0de0584b3739266b80fe835d55b7faeade33cbad', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'bieg.jpg', 'data/attachments/module/site/block/news-list/2391/0de0584b3739266b80fe835d55b7faeade33cbad.jpg'),
(514, 513, 1, 49347, NULL, NULL, '2014-04-12 03:07:35', '2014-04-12 03:07:35', 'a779a3bce9bb7f87089c0efd56f9d277245a7265', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'bieg.jpg', 'data/attachments/module/site/block/news-list/2391/a779a3bce9bb7f87089c0efd56f9d277245a7265.jpg'),
(515, NULL, 1, 49330, NULL, 1, '2014-04-12 03:12:24', '2014-04-12 03:12:24', '01992fb8992ebf67e1e74f62b8118418c4622584', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'bieg.jpg', 'data/attachments/module/site/block/news-list/2391/01992fb8992ebf67e1e74f62b8118418c4622584.jpg'),
(516, 515, 1, 49347, NULL, NULL, '2014-04-12 03:12:35', '2014-04-12 03:12:24', 'cd92e9cda3387288b4463ddff903a2a70b318ab4', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'bieg.jpg', 'data/attachments/module/site/block/news-list/2391/cd92e9cda3387288b4463ddff903a2a70b318ab4.jpg'),
(517, NULL, 1, 36836, NULL, 1, '2014-04-12 13:16:23', '2014-04-12 13:16:23', '6bcdc7587cc5c51386f21082d2d21d85544e1713', 'jpg', 'image/jpeg', 'site', 'employees', '', 'azjata.jpg', 'data/attachments/module/site/block/employees/2393/6bcdc7587cc5c51386f21082d2d21d85544e1713.jpg'),
(518, 517, 1, 22006, NULL, NULL, '2014-04-12 13:16:23', '2014-04-12 13:16:23', 'a148fc12390f522dfad4530b869415d7752b627f', 'jpg', 'image/jpeg', 'site', 'employees', '0,0,185,185', 'azjata.jpg', 'data/attachments/module/site/block/employees/2393/a148fc12390f522dfad4530b869415d7752b627f.jpg'),
(519, NULL, 1, 40764, NULL, 1, '2014-04-13 07:50:58', '2014-04-13 07:50:58', 'b24698a349724daa0bdb02b55f9725bb26979fee', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'shutterstock_118050325.jpg', 'data/attachments/module/site/block/bachelor-studies/2154/b24698a349724daa0bdb02b55f9725bb26979fee.jpg'),
(520, 519, 1, 28345, NULL, NULL, '2014-04-13 07:51:08', '2014-04-13 07:50:58', '19ffb3e95e3f6cb5bf12933762475416b241be13', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'shutterstock_118050325.jpg', 'data/attachments/module/site/block/bachelor-studies/2154/19ffb3e95e3f6cb5bf12933762475416b241be13.jpg'),
(521, NULL, 1, 1365, NULL, 1, '2014-04-13 11:43:39', '2014-04-13 11:43:39', '4572800df2a62165c8b3a83b367c1d2601faec8f', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'tło.jpg', 'data/attachments/module/site/block/bachelor-studies/2150/4572800df2a62165c8b3a83b367c1d2601faec8f.jpg'),
(522, 521, 1, 1365, NULL, NULL, '2014-04-13 11:43:39', '2014-04-13 11:43:39', 'c15ff0ff99ff69237a8ba4fd0079c6915930b3f4', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'tło.jpg', 'data/attachments/module/site/block/bachelor-studies/2150/c15ff0ff99ff69237a8ba4fd0079c6915930b3f4.jpg'),
(523, NULL, 1, 1365, NULL, 1, '2014-04-13 11:44:05', '2014-04-13 11:44:05', '64bd27220739c95e2fda07cc93d7ffd467c734b5', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'tło.jpg', 'data/attachments/module/site/block/bachelor-studies/2037/64bd27220739c95e2fda07cc93d7ffd467c734b5.jpg'),
(524, 523, 1, 1365, NULL, NULL, '2014-04-13 11:44:05', '2014-04-13 11:44:05', 'e2af472df0cf9d5bf74308b3892ae650ce73a023', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'tło.jpg', 'data/attachments/module/site/block/bachelor-studies/2037/e2af472df0cf9d5bf74308b3892ae650ce73a023.jpg'),
(525, NULL, 1, 1365, NULL, 1, '2014-04-13 11:44:56', '2014-04-13 11:44:56', '44aca8a2bf761d1459e666c3205e401b644b4b56', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'tło.jpg', 'data/attachments/module/site/block/bachelor-studies/2144/44aca8a2bf761d1459e666c3205e401b644b4b56.jpg'),
(526, 525, 1, 1365, NULL, NULL, '2014-04-13 11:44:56', '2014-04-13 11:44:56', '27b52881bc047ed5332aea7a292a6f39d1e1f689', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'tło.jpg', 'data/attachments/module/site/block/bachelor-studies/2144/27b52881bc047ed5332aea7a292a6f39d1e1f689.jpg'),
(527, NULL, 1, 1365, NULL, 1, '2014-04-13 11:45:55', '2014-04-13 11:45:55', '2a3a6aa4b7b5f7c2fe1efee6658b7a05a1d7ae26', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'tło.jpg', 'data/attachments/module/site/block/bachelor-studies/2149/2a3a6aa4b7b5f7c2fe1efee6658b7a05a1d7ae26.jpg'),
(528, 527, 1, 1365, NULL, NULL, '2014-04-13 11:45:55', '2014-04-13 11:45:55', '21c27493417fcace6b3bc12ebbb4d3565c6250e0', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'tło.jpg', 'data/attachments/module/site/block/bachelor-studies/2149/21c27493417fcace6b3bc12ebbb4d3565c6250e0.jpg'),
(529, NULL, 1, 1365, NULL, 1, '2014-04-13 11:46:20', '2014-04-13 11:46:20', 'a52400943d4d159739cf1f488613161dcf8df038', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'tło.jpg', 'data/attachments/module/site/block/bachelor-studies/2151/a52400943d4d159739cf1f488613161dcf8df038.jpg'),
(530, 529, 1, 1365, NULL, NULL, '2014-04-13 11:46:20', '2014-04-13 11:46:20', 'f087fed72e453cec1ccf419a8ebc0da9ae4bb0b5', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'tło.jpg', 'data/attachments/module/site/block/bachelor-studies/2151/f087fed72e453cec1ccf419a8ebc0da9ae4bb0b5.jpg'),
(531, NULL, 1, 1365, NULL, 1, '2014-04-13 11:47:12', '2014-04-13 11:47:12', '064cf350c637922f4dc892c446beec608ff44365', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'tło.jpg', 'data/attachments/module/site/block/bachelor-studies/2154/064cf350c637922f4dc892c446beec608ff44365.jpg'),
(532, 531, 1, 1365, NULL, NULL, '2014-04-13 11:47:12', '2014-04-13 11:47:12', 'b44b7700bb03ac98787c6822081602a16b3a6455', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'tło.jpg', 'data/attachments/module/site/block/bachelor-studies/2154/b44b7700bb03ac98787c6822081602a16b3a6455.jpg'),
(533, NULL, 1, 31917, NULL, 1, '2014-04-13 13:23:27', '2014-04-13 13:23:27', '5d5ba2e06c62eec7688a65c4a9d896e22c999ecc', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'iStock_000003299207Medium.jpg', 'data/attachments/module/site/block/image-slider/2400/5d5ba2e06c62eec7688a65c4a9d896e22c999ecc.jpg'),
(534, 533, 1, 30168, NULL, NULL, '2014-04-13 13:23:27', '2014-04-13 13:23:27', '81b16a4e917a1c50f0222c0ebf7081de4c4e06f8', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', 'iStock_000003299207Medium.jpg', 'data/attachments/module/site/block/image-slider/2400/81b16a4e917a1c50f0222c0ebf7081de4c4e06f8.jpg'),
(535, NULL, 1, 32675, NULL, 1, '2014-04-13 13:24:00', '2014-04-13 13:24:00', '4cf43353fe6113b943cb7f002284bcabfc9b3907', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'StKo_260909_121.jpg', 'data/attachments/module/site/block/image-slider/2400/4cf43353fe6113b943cb7f002284bcabfc9b3907.jpg'),
(536, 535, 1, 30160, NULL, NULL, '2014-04-13 13:24:00', '2014-04-13 13:24:00', '791a1bcb30009b5889b57a8b5d8749b1c18894be', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', 'StKo_260909_121.jpg', 'data/attachments/module/site/block/image-slider/2400/791a1bcb30009b5889b57a8b5d8749b1c18894be.jpg'),
(537, NULL, 1, 43229, NULL, 1, '2014-04-13 13:24:53', '2014-04-13 13:24:53', '146d60ddc75e81cfa43258dc93a83812b4d92b9e', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'StKo_260909_081.jpg', 'data/attachments/module/site/block/image-slider/2400/146d60ddc75e81cfa43258dc93a83812b4d92b9e.jpg'),
(538, 537, 1, 39561, NULL, NULL, '2014-04-13 13:24:53', '2014-04-13 13:24:53', '0111ed3326ed0ed45e886cf7042a004b0e4822c6', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', 'StKo_260909_081.jpg', 'data/attachments/module/site/block/image-slider/2400/0111ed3326ed0ed45e886cf7042a004b0e4822c6.jpg'),
(539, NULL, 1, 45218, NULL, 1, '2014-04-13 13:26:45', '2014-04-13 13:26:45', '59daf8540ba536599f573a99c24f99f722988ee2', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'DSC_0437.JPG', 'data/attachments/module/site/block/image-slider/2400/59daf8540ba536599f573a99c24f99f722988ee2.jpg'),
(540, 539, 1, 42450, NULL, NULL, '2014-04-13 13:26:45', '2014-04-13 13:26:45', '1f2b04960eb1fec4a5284b445bfbc260345feaae', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', 'DSC_0437.JPG', 'data/attachments/module/site/block/image-slider/2400/1f2b04960eb1fec4a5284b445bfbc260345feaae.jpg'),
(541, NULL, 1, 43685, NULL, 1, '2014-04-13 13:29:16', '2014-04-13 13:29:16', 'fa5657944f212d6ce107660d3164e3895e072bcb', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'DSC_0437 (Kopiowanie).JPG', 'data/attachments/module/site/block/image-slider/2400/fa5657944f212d6ce107660d3164e3895e072bcb.jpg'),
(542, 541, 1, 41186, NULL, NULL, '2014-04-13 13:29:16', '2014-04-13 13:29:16', '5bb44c14a033caedc705d2e699af354105410d3d', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', 'DSC_0437 (Kopiowanie).JPG', 'data/attachments/module/site/block/image-slider/2400/5bb44c14a033caedc705d2e699af354105410d3d.jpg'),
(543, NULL, 1, 43685, NULL, 1, '2014-04-13 13:29:33', '2014-04-13 13:29:33', '1108ee0d3104458906c24fc451e605b6ab3cf0ec', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'DSC_0437 (Kopiowanie).JPG', 'data/attachments/module/site/block/image-slider/2400/1108ee0d3104458906c24fc451e605b6ab3cf0ec.jpg'),
(544, 543, 1, 41186, NULL, NULL, '2014-04-13 13:29:33', '2014-04-13 13:29:33', '91e83f91fa5a2461a202b60ae56f7bafddbb2256', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', 'DSC_0437 (Kopiowanie).JPG', 'data/attachments/module/site/block/image-slider/2400/91e83f91fa5a2461a202b60ae56f7bafddbb2256.jpg'),
(545, NULL, 1, 28739, NULL, 1, '2014-04-13 15:17:21', '2014-04-13 15:17:21', 'a7e56ed7c21322848889ce240d54fc64497dcaec', 'jpg', 'image/jpeg', 'site', 'employees', '', 'Rektor KRybinski.jpg', 'data/attachments/module/site/block/employees/2396/a7e56ed7c21322848889ce240d54fc64497dcaec.jpg'),
(546, 545, 1, 22169, NULL, NULL, '2014-04-13 15:17:21', '2014-04-13 15:17:21', 'a176874ff9d58076f40c36a993aff3941e2210d3', 'jpg', 'image/jpeg', 'site', 'employees', '0,0,185,185', 'Rektor KRybinski.jpg', 'data/attachments/module/site/block/employees/2396/a176874ff9d58076f40c36a993aff3941e2210d3.jpg'),
(547, NULL, 1, 29203, NULL, 1, '2014-04-13 15:17:36', '2014-04-13 15:17:36', '830e767a0f5d99f27523bb69f69423147e9500fc', 'jpg', 'image/jpeg', 'site', 'employees', '', 'Rektor (Small).jpg', 'data/attachments/module/site/block/employees/2396/830e767a0f5d99f27523bb69f69423147e9500fc.jpg'),
(548, 547, 1, 22678, NULL, NULL, '2014-04-13 15:17:36', '2014-04-13 15:17:36', '3ad2e0d4d5c5107128c7bf1cd14c44680e36bf20', 'jpg', 'image/jpeg', 'site', 'employees', '0,0,185,185', 'Rektor (Small).jpg', 'data/attachments/module/site/block/employees/2396/3ad2e0d4d5c5107128c7bf1cd14c44680e36bf20.jpg'),
(549, NULL, 1, 28409, NULL, 1, '2014-04-13 15:17:42', '2014-04-13 15:17:42', '089d4b042bdd5bd993035c78f770c122f837ea9a', 'jpg', 'image/jpeg', 'site', 'employees', '', 'Rektor (Kopiowanie).jpg', 'data/attachments/module/site/block/employees/2396/089d4b042bdd5bd993035c78f770c122f837ea9a.jpg'),
(550, 549, 1, 21971, NULL, NULL, '2014-04-13 15:17:42', '2014-04-13 15:17:42', '59146cd4d5151ebefbd3a35b57e800209a6ed4ff', 'jpg', 'image/jpeg', 'site', 'employees', '0,0,185,185', 'Rektor (Kopiowanie).jpg', 'data/attachments/module/site/block/employees/2396/59146cd4d5151ebefbd3a35b57e800209a6ed4ff.jpg'),
(551, NULL, 1, 49870, NULL, 1, '2014-04-13 16:15:25', '2014-04-13 16:15:25', 'f8e23782fd68efd245e73d93fff0509edcf7a2b7', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'shutterstock_73965616.jpg', 'data/attachments/module/site/block/news-list/1445/f8e23782fd68efd245e73d93fff0509edcf7a2b7.jpg'),
(552, 551, 1, 42261, NULL, NULL, '2014-04-13 16:15:26', '2014-04-13 16:15:25', '5b7a219fd9fa7d93956bb84bd95d158bd75494fc', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'shutterstock_73965616.jpg', 'data/attachments/module/site/block/news-list/1445/5b7a219fd9fa7d93956bb84bd95d158bd75494fc.jpg'),
(553, NULL, 1, 33253, NULL, 1, '2014-04-13 19:45:46', '2014-04-13 19:45:46', '857b3abbf3e07bcca53b555a77b96a9fc41945fd', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'api_thumb_450 (8).jpg', 'data/attachments/module/site/block/images-pile/2361/857b3abbf3e07bcca53b555a77b96a9fc41945fd.jpg'),
(554, 553, 1, 31481, NULL, NULL, '2014-04-13 19:45:46', '2014-04-13 19:45:46', 'ae1fb97d91890c6e044933551e91afa0c6d217c1', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'api_thumb_450 (8).jpg', 'data/attachments/module/site/block/images-pile/2361/ae1fb97d91890c6e044933551e91afa0c6d217c1.jpg'),
(555, NULL, 1, 28625, NULL, 1, '2014-04-13 19:46:35', '2014-04-13 19:46:35', '4c46b6a46ee32333e9912db192480a1562952882', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_186564986.jpg', 'data/attachments/module/site/block/images-pile/2361/4c46b6a46ee32333e9912db192480a1562952882.jpg'),
(556, 555, 1, 26888, NULL, NULL, '2014-04-13 19:46:35', '2014-04-13 19:46:35', '3d5866ff1aa526ca5ca68dee16720621404ad429', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_186564986.jpg', 'data/attachments/module/site/block/images-pile/2361/3d5866ff1aa526ca5ca68dee16720621404ad429.jpg'),
(557, NULL, 1, 47237, NULL, 1, '2014-04-13 20:09:35', '2014-04-13 20:09:35', '1a0c2419a838940f495c8f079a4545f557af7756', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'Wielkanoc_2014_ (Kopiowanie).jpg', 'data/attachments/module/site/block/news-list/1445/1a0c2419a838940f495c8f079a4545f557af7756.jpg'),
(558, 557, 1, 40593, NULL, NULL, '2014-04-13 20:09:35', '2014-04-13 20:09:35', '54230bc09d5b977e3777cb0ffa60b90a676fca57', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'Wielkanoc_2014_ (Kopiowanie).jpg', 'data/attachments/module/site/block/news-list/1445/54230bc09d5b977e3777cb0ffa60b90a676fca57.jpg'),
(559, NULL, 1, 47237, NULL, 1, '2014-04-13 20:09:50', '2014-04-13 20:09:50', '8b45b319de81070786c126bde0ac26c949047b33', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'Wielkanoc_2014_ (Kopiowanie).jpg', 'data/attachments/module/site/block/news-list/1445/8b45b319de81070786c126bde0ac26c949047b33.jpg'),
(560, 559, 1, 40593, NULL, NULL, '2014-04-13 20:09:51', '2014-04-13 20:09:50', 'f53473bfe882472263f30a9910a0c5e1c1698918', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'Wielkanoc_2014_ (Kopiowanie).jpg', 'data/attachments/module/site/block/news-list/1445/f53473bfe882472263f30a9910a0c5e1c1698918.jpg'),
(561, NULL, 1, 40664, NULL, 1, '2014-04-13 20:20:42', '2014-04-13 20:20:42', '93ff3cc659253529fb8212bf29562e1712bd6df2', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'wielkanoc2014_.jpg', 'data/attachments/module/site/block/news-list/1445/93ff3cc659253529fb8212bf29562e1712bd6df2.jpg'),
(562, 561, 1, 40784, NULL, NULL, '2014-04-13 20:20:42', '2014-04-13 20:20:42', 'e3c0f326f9441011cb2fc762f8fe7187a93bd731', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'wielkanoc2014_.jpg', 'data/attachments/module/site/block/news-list/1445/e3c0f326f9441011cb2fc762f8fe7187a93bd731.jpg'),
(563, NULL, 1, 28409, NULL, 1, '2014-04-13 22:37:55', '2014-04-13 22:37:55', '5f668eef9674bdf864e066f748643e3c18b3bc4f', 'jpg', 'image/jpeg', 'site', 'employees', '', 'Rektor (Kopiowanie).jpg', 'data/attachments/module/site/block/employees/2396/5f668eef9674bdf864e066f748643e3c18b3bc4f.jpg'),
(564, 563, 1, 21971, NULL, NULL, '2014-04-13 22:37:55', '2014-04-13 22:37:55', '2597938de93beed25a51b2586fe42bba90cb3c8e', 'jpg', 'image/jpeg', 'site', 'employees', '0,0,185,185', 'Rektor (Kopiowanie).jpg', 'data/attachments/module/site/block/employees/2396/2597938de93beed25a51b2586fe42bba90cb3c8e.jpg'),
(565, NULL, 1, 196823, NULL, 1, '2014-04-13 23:37:52', '2014-04-13 23:37:52', 'c94daed8956769ec84b08eec3bd8408903334eaa', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'studia w języku angielsku.jpg', 'data/attachments/module/site/block/small-banner/2423/c94daed8956769ec84b08eec3bd8408903334eaa.jpg'),
(566, 565, 1, 199261, NULL, NULL, '2014-04-13 23:37:52', '2014-04-13 23:37:52', '6368bc635463cbf626d837a88d98aa8f1525085b', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,900,317', 'studia w języku angielsku.jpg', 'data/attachments/module/site/block/small-banner/2423/6368bc635463cbf626d837a88d98aa8f1525085b.jpg'),
(567, NULL, 1, 130441, NULL, 1, '2014-04-13 23:56:08', '2014-04-13 23:56:08', 'a1427b89c79a9d70a2c7ed66fe9f4f946c1e31fd', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'dziekanat.jpg', 'data/attachments/module/site/block/small-banner/2424/a1427b89c79a9d70a2c7ed66fe9f4f946c1e31fd.jpg'),
(568, 567, 1, 133199, NULL, NULL, '2014-04-13 23:56:08', '2014-04-13 23:56:08', 'd134f6573d1970136e81a5a6f98654640934d2d1', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,900,317', 'dziekanat.jpg', 'data/attachments/module/site/block/small-banner/2424/d134f6573d1970136e81a5a6f98654640934d2d1.jpg'),
(569, NULL, 1, 14070, NULL, 4, '2014-04-14 08:57:15', '2014-04-14 08:57:15', '041fff0852aa1574f0829bb0c42efccd2adf41d7', 'jpg', 'image/jpeg', 'site', 'employees', '', '2.jpg', 'data/attachments/module/site/block/employees/2396/041fff0852aa1574f0829bb0c42efccd2adf41d7.jpg'),
(570, 569, 1, 11867, NULL, NULL, '2014-04-14 08:57:15', '2014-04-14 08:57:15', 'ec9e5781f619ad27069af9aeb5f4a1248b1bbeea', 'jpg', 'image/jpeg', 'site', 'employees', '0,0,185,185', '2.jpg', 'data/attachments/module/site/block/employees/2396/ec9e5781f619ad27069af9aeb5f4a1248b1bbeea.jpg'),
(571, NULL, 1, 51130, NULL, 1, '2014-04-14 15:17:24', '2014-04-14 15:17:24', '335fee854da550ec27275354fdb49ad9b565878f', 'png', 'image/png', 'site', 'news-list', '', 'express_Centralna_Dyspozytornia_04_2014_pdf__1_strona_.png', 'data/attachments/module/site/block/news-list/2431/335fee854da550ec27275354fdb49ad9b565878f.png'),
(572, 571, 1, 46167, NULL, NULL, '2014-04-14 15:17:35', '2014-04-14 15:17:24', '4eba8f5b90996ef90bdd1838b63088b9a1dfd2bd', 'png', 'image/png', 'site', 'news-list', '0,18,270,168', 'express_Centralna_Dyspozytornia_04_2014_pdf__1_strona_.png', 'data/attachments/module/site/block/news-list/2431/4eba8f5b90996ef90bdd1838b63088b9a1dfd2bd.png'),
(573, NULL, 1, 51130, NULL, 1, '2014-04-14 15:20:32', '2014-04-14 15:20:32', '6612cbd06557bbab7d0008e3c952488bc36a5501', 'png', 'image/png', 'site', 'news-list', '', 'express_Centralna_Dyspozytornia_04_2014_pdf__1_strona_.png', 'data/attachments/module/site/block/news-list/2431/6612cbd06557bbab7d0008e3c952488bc36a5501.png'),
(574, 573, 1, 45707, NULL, NULL, '2014-04-14 15:20:38', '2014-04-14 15:20:32', '4c3bdace5b120127cd617f90a5986a16f4e15aa1', 'png', 'image/png', 'site', 'news-list', '0,22,270,172', 'express_Centralna_Dyspozytornia_04_2014_pdf__1_strona_.png', 'data/attachments/module/site/block/news-list/2431/4c3bdace5b120127cd617f90a5986a16f4e15aa1.png'),
(575, NULL, 1, 51130, NULL, 1, '2014-04-14 15:25:58', '2014-04-14 15:25:58', 'aad399b319fcfb70958a36f6dcc379091eefc1ec', 'png', 'image/png', 'site', 'news-list', '', 'express_Centralna_Dyspozytornia_04_2014_pdf__1_strona_.png', 'data/attachments/module/site/block/news-list/2431/aad399b319fcfb70958a36f6dcc379091eefc1ec.png'),
(576, 575, 1, 43865, NULL, NULL, '2014-04-14 15:25:59', '2014-04-14 15:25:58', '96e381fbc1385977c171caba8459f855921cf23b', 'png', 'image/png', 'site', 'news-list', '0,0,270,150', 'express_Centralna_Dyspozytornia_04_2014_pdf__1_strona_.png', 'data/attachments/module/site/block/news-list/2431/96e381fbc1385977c171caba8459f855921cf23b.png'),
(577, NULL, 1, 30215, NULL, 1, '2014-04-14 15:52:31', '2014-04-14 15:52:31', '2f731b6282487229f58bc49bba669995f02b94be', 'jpg', 'image/jpeg', 'site', 'news-list', '', '5383982.jpg', 'data/attachments/module/site/block/news-list/2434/2f731b6282487229f58bc49bba669995f02b94be.jpg'),
(578, 577, 1, 26237, NULL, NULL, '2014-04-14 15:56:51', '2014-04-14 15:52:31', '91c6d794e29bb025d289bf96656524e8b64a8e7d', 'jpg', 'image/jpeg', 'site', 'news-list', '0,15,270,165', '5383982.jpg', 'data/attachments/module/site/block/news-list/2434/91c6d794e29bb025d289bf96656524e8b64a8e7d.jpg'),
(579, NULL, 1, 45719, NULL, 1, '2014-04-14 20:36:42', '2014-04-14 20:36:42', '4da4828d04f3f4e5cc0aa597102c9cf10d138ed9', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_151266965 (Kopiowanie).jpg', 'data/attachments/module/site/block/images-pile/2366/4da4828d04f3f4e5cc0aa597102c9cf10d138ed9.jpg'),
(580, 579, 1, 42412, NULL, NULL, '2014-04-14 20:36:42', '2014-04-14 20:36:42', 'ac59259d92daf9631e4e42a8bae353d134fc26e5', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_151266965 (Kopiowanie).jpg', 'data/attachments/module/site/block/images-pile/2366/ac59259d92daf9631e4e42a8bae353d134fc26e5.jpg'),
(581, NULL, 1, 33632, NULL, 1, '2014-04-14 20:40:32', '2014-04-14 20:40:32', '54117f2821347f86283d45c57a6cc59a7c59257b', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_152115344.jpg', 'data/attachments/module/site/block/images-pile/2366/54117f2821347f86283d45c57a6cc59a7c59257b.jpg'),
(582, 581, 1, 32061, NULL, NULL, '2014-04-14 20:40:32', '2014-04-14 20:40:32', '01120c1c92f5defce40623a7883834bd7c3c7f99', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_152115344.jpg', 'data/attachments/module/site/block/images-pile/2366/01120c1c92f5defce40623a7883834bd7c3c7f99.jpg'),
(583, NULL, 1, 153423, NULL, 1, '2014-04-14 21:47:46', '2014-04-14 21:47:46', '9bdb1b22a09e6a82ac7a2b5ace8d0f0ffb053a53', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'Biuro karier 1.jpg', 'data/attachments/module/site/block/small-banner/2447/9bdb1b22a09e6a82ac7a2b5ace8d0f0ffb053a53.jpg'),
(584, 583, 1, 156721, NULL, NULL, '2014-04-14 21:47:46', '2014-04-14 21:47:46', '28edd7f2e48d8ff9a6648a4dd42957e51a45dda5', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,900,317', 'Biuro karier 1.jpg', 'data/attachments/module/site/block/small-banner/2447/28edd7f2e48d8ff9a6648a4dd42957e51a45dda5.jpg'),
(585, NULL, 1, 116382, NULL, 1, '2014-04-14 21:48:09', '2014-04-14 21:48:09', 'cce612c7c36fd5271545517a037d6cb5903aefb0', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'Biuro karier 2.jpg', 'data/attachments/module/site/block/small-banner/2447/cce612c7c36fd5271545517a037d6cb5903aefb0.jpg'),
(586, 585, 1, 119874, NULL, NULL, '2014-04-14 21:48:09', '2014-04-14 21:48:09', 'a6c3f48925548a1967c9b3e3cc761e478cd19793', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,900,317', 'Biuro karier 2.jpg', 'data/attachments/module/site/block/small-banner/2447/a6c3f48925548a1967c9b3e3cc761e478cd19793.jpg'),
(587, NULL, 1, 45170, NULL, 1, '2014-04-14 21:54:22', '2014-04-14 21:54:22', '9c9a2716142d11bd281d81d631e2bef28d29987a', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'ChromaStock_24192915.jpg', 'data/attachments/module/site/block/news-list/1445/9c9a2716142d11bd281d81d631e2bef28d29987a.jpg'),
(588, 587, 1, 39036, NULL, NULL, '2014-04-14 21:54:22', '2014-04-14 21:54:22', 'fe9adfd254cb82b88465394dc998ace18aafad7c', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'ChromaStock_24192915.jpg', 'data/attachments/module/site/block/news-list/1445/fe9adfd254cb82b88465394dc998ace18aafad7c.jpg'),
(589, NULL, 1, 42942, NULL, 1, '2014-04-14 21:57:30', '2014-04-14 21:57:30', 'acf1c1b747686313eb78be522f32b5cd95a66432', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'shutterstock_114006016 (Kopiowanie).jpg', 'data/attachments/module/site/block/news-list/1445/acf1c1b747686313eb78be522f32b5cd95a66432.jpg'),
(590, 589, 1, 35484, NULL, NULL, '2014-04-14 21:57:30', '2014-04-14 21:57:30', '1253c86ebefedb2c7b405521edf6a1e5947bae8a', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'shutterstock_114006016 (Kopiowanie).jpg', 'data/attachments/module/site/block/news-list/1445/1253c86ebefedb2c7b405521edf6a1e5947bae8a.jpg'),
(591, NULL, 1, 42643, NULL, 1, '2014-04-14 22:28:46', '2014-04-14 22:28:46', '9d1a405a33afd54f80179898acff0ae313499d7b', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_160835312.jpg', 'data/attachments/module/site/block/images-pile/2360/9d1a405a33afd54f80179898acff0ae313499d7b.jpg'),
(592, 591, 1, 37478, NULL, NULL, '2014-04-14 22:28:46', '2014-04-14 22:28:46', '8eb0eb07417c108888966d434fd4e99e91eb17e4', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_160835312.jpg', 'data/attachments/module/site/block/images-pile/2360/8eb0eb07417c108888966d434fd4e99e91eb17e4.jpg'),
(593, NULL, 1, 50657, NULL, 1, '2014-04-14 22:35:51', '2014-04-14 22:35:51', '63155fc0fae2bae6e49e638c7d7262ba57dc4929', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_89090926.jpg', 'data/attachments/module/site/block/images-pile/2360/63155fc0fae2bae6e49e638c7d7262ba57dc4929.jpg'),
(594, 593, 1, 45729, NULL, NULL, '2014-04-14 22:35:51', '2014-04-14 22:35:51', '81e2827c6c3cc60245cb4b61f39266a6b9aa70c0', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_89090926.jpg', 'data/attachments/module/site/block/images-pile/2360/81e2827c6c3cc60245cb4b61f39266a6b9aa70c0.jpg'),
(595, NULL, 1, 50191, NULL, 1, '2014-04-14 22:49:43', '2014-04-14 22:49:43', 'ae10077ff6b2f010bf53fa77b8b3df37fef53413', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_128015852.jpg', 'data/attachments/module/site/block/images-pile/2360/ae10077ff6b2f010bf53fa77b8b3df37fef53413.jpg'),
(596, 595, 1, 40962, NULL, NULL, '2014-04-14 22:49:43', '2014-04-14 22:49:43', 'ecd54f398fe2a6806b24899c200ee81622ae9ceb', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_128015852.jpg', 'data/attachments/module/site/block/images-pile/2360/ecd54f398fe2a6806b24899c200ee81622ae9ceb.jpg'),
(597, NULL, 1, 42221, NULL, 1, '2014-04-14 22:54:25', '2014-04-14 22:54:25', '629e8f13540ff972952fce33b010f3f7de8f8552', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_181123703 (1).jpg', 'data/attachments/module/site/block/images-pile/2360/629e8f13540ff972952fce33b010f3f7de8f8552.jpg'),
(598, 597, 1, 37244, NULL, NULL, '2014-04-14 22:55:31', '2014-04-14 22:54:25', '10802479c29bbc82f9820fa87fbb877c0abb3a0b', 'jpg', 'image/jpeg', 'site', 'images-pile', '18,0,268,188', 'shutterstock_181123703 (1).jpg', 'data/attachments/module/site/block/images-pile/2360/10802479c29bbc82f9820fa87fbb877c0abb3a0b.jpg'),
(599, NULL, 1, 36209, NULL, 1, '2014-04-14 23:00:21', '2014-04-14 23:00:21', '72a57e1f966fb6b118a20d61a09ad5bab972f61c', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_129991661.jpg', 'data/attachments/module/site/block/images-pile/2360/72a57e1f966fb6b118a20d61a09ad5bab972f61c.jpg'),
(600, 599, 1, 35685, NULL, NULL, '2014-04-14 23:00:21', '2014-04-14 23:00:21', '5c32459d01b31af5ef78d06fc0bc00a862d3d878', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_129991661.jpg', 'data/attachments/module/site/block/images-pile/2360/5c32459d01b31af5ef78d06fc0bc00a862d3d878.jpg'),
(601, NULL, 1, 46645, NULL, 1, '2014-04-15 00:15:55', '2014-04-15 00:15:55', 'fedebc546119bcaf6a2c5c76764dfd3d17bb00e0', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_154951472 (Kopiowanie).jpg', 'data/attachments/module/site/block/images-pile/2416/fedebc546119bcaf6a2c5c76764dfd3d17bb00e0.jpg'),
(602, 601, 1, 44345, NULL, NULL, '2014-04-15 00:15:55', '2014-04-15 00:15:55', '50f4e618b60da56cdf448276ee4512ee624c91b8', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_154951472 (Kopiowanie).jpg', 'data/attachments/module/site/block/images-pile/2416/50f4e618b60da56cdf448276ee4512ee624c91b8.jpg'),
(603, NULL, 1, 50807, NULL, 1, '2014-04-15 00:17:13', '2014-04-15 00:17:13', '994e5be4ab13e29631c2a6a6c91a889993ddaebc', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_105216713.jpg', 'data/attachments/module/site/block/images-pile/2416/994e5be4ab13e29631c2a6a6c91a889993ddaebc.jpg'),
(604, 603, 1, 45302, NULL, NULL, '2014-04-15 00:17:13', '2014-04-15 00:17:13', 'b5203bea8cc48eb4613044d66397505ac74ca362', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_105216713.jpg', 'data/attachments/module/site/block/images-pile/2416/b5203bea8cc48eb4613044d66397505ac74ca362.jpg'),
(605, NULL, 1, 38076, NULL, 1, '2014-04-15 00:18:07', '2014-04-15 00:18:07', 'c182c5ccda4415894cd952246f624f4b35e0b7ed', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_115555096.jpg', 'data/attachments/module/site/block/images-pile/2416/c182c5ccda4415894cd952246f624f4b35e0b7ed.jpg'),
(606, 605, 1, 38349, NULL, NULL, '2014-04-15 00:18:07', '2014-04-15 00:18:07', '90a7616334d95633d4e0f8b19b0a6d80d81898c9', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_115555096.jpg', 'data/attachments/module/site/block/images-pile/2416/90a7616334d95633d4e0f8b19b0a6d80d81898c9.jpg'),
(607, NULL, 1, 48364, NULL, 1, '2014-04-15 00:19:06', '2014-04-15 00:19:06', 'a6a26cdc379f5ad27f86b13676d5d6c626619176', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_125338145.jpg', 'data/attachments/module/site/block/images-pile/2416/a6a26cdc379f5ad27f86b13676d5d6c626619176.jpg'),
(608, 607, 1, 44832, NULL, NULL, '2014-04-15 00:19:06', '2014-04-15 00:19:06', '2cc4f219cfe22bf5ad1cdebb21229293470d1f09', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_125338145.jpg', 'data/attachments/module/site/block/images-pile/2416/2cc4f219cfe22bf5ad1cdebb21229293470d1f09.jpg'),
(609, NULL, 1, 56231, NULL, 1, '2014-04-15 00:20:33', '2014-04-15 00:20:33', '82af67fc101854c724ce81b98c23b6a7a86b4e69', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_141032806.jpg', 'data/attachments/module/site/block/images-pile/2416/82af67fc101854c724ce81b98c23b6a7a86b4e69.jpg'),
(610, 609, 1, 50713, NULL, NULL, '2014-04-15 00:20:33', '2014-04-15 00:20:33', '10edf7c418410ad00a5080ebc69c5f7da25f1955', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_141032806.jpg', 'data/attachments/module/site/block/images-pile/2416/10edf7c418410ad00a5080ebc69c5f7da25f1955.jpg'),
(611, NULL, 1, 31280, NULL, 1, '2014-04-15 00:21:05', '2014-04-15 00:21:05', '3d5b8932c540ef755dab203e8f03208ce58f64a5', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_73532917.jpg', 'data/attachments/module/site/block/images-pile/2416/3d5b8932c540ef755dab203e8f03208ce58f64a5.jpg'),
(612, 611, 1, 30975, NULL, NULL, '2014-04-15 00:21:05', '2014-04-15 00:21:05', '04d52b2c73e5b68b97c5635cef855fda3c971652', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_73532917.jpg', 'data/attachments/module/site/block/images-pile/2416/04d52b2c73e5b68b97c5635cef855fda3c971652.jpg'),
(613, NULL, 1, 30007, NULL, 1, '2014-04-15 00:26:58', '2014-04-15 00:26:58', '37247ea39f2c3077a31426c82aa94eaa01d4cfef', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_135401798.jpg', 'data/attachments/module/site/block/images-pile/2416/37247ea39f2c3077a31426c82aa94eaa01d4cfef.jpg'),
(614, 613, 1, 28626, NULL, NULL, '2014-04-15 00:26:58', '2014-04-15 00:26:58', '8ac91317ad08f5c0545c58ac181d17852f5d3512', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_135401798.jpg', 'data/attachments/module/site/block/images-pile/2416/8ac91317ad08f5c0545c58ac181d17852f5d3512.jpg'),
(615, NULL, 1, 39110, NULL, 1, '2014-04-15 00:29:40', '2014-04-15 00:29:40', '64e7bb3f4aeaa65b5271e82f1c7afde874d875aa', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_159197729 (1).jpg', 'data/attachments/module/site/block/images-pile/2416/64e7bb3f4aeaa65b5271e82f1c7afde874d875aa.jpg'),
(616, 615, 1, 32130, NULL, NULL, '2014-04-15 00:29:40', '2014-04-15 00:29:40', '9a66a1a4d60d77d334393f0edd447b0f393ed383', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_159197729 (1).jpg', 'data/attachments/module/site/block/images-pile/2416/9a66a1a4d60d77d334393f0edd447b0f393ed383.jpg'),
(617, NULL, 1, 34575, NULL, 1, '2014-04-15 00:38:30', '2014-04-15 00:38:30', '787ebb113837543d6b676e5acd0e3f180487ca41', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_153138065.jpg', 'data/attachments/module/site/block/images-pile/2416/787ebb113837543d6b676e5acd0e3f180487ca41.jpg'),
(618, 617, 1, 32875, NULL, NULL, '2014-04-15 00:38:30', '2014-04-15 00:38:30', 'cd98a2cfbe57137dd65888dd3c028245a6a18913', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_153138065.jpg', 'data/attachments/module/site/block/images-pile/2416/cd98a2cfbe57137dd65888dd3c028245a6a18913.jpg'),
(619, NULL, 1, 42153, NULL, 1, '2014-04-15 00:39:35', '2014-04-15 00:39:35', '02a5c8ff8c9e1d059633ddc6eea38ce301327e43', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_153117146.jpg', 'data/attachments/module/site/block/images-pile/2416/02a5c8ff8c9e1d059633ddc6eea38ce301327e43.jpg'),
(620, 619, 1, 40218, NULL, NULL, '2014-04-15 00:39:35', '2014-04-15 00:39:35', '90052dae8e4502f23d0e8135c887fb06268fc1ec', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_153117146.jpg', 'data/attachments/module/site/block/images-pile/2416/90052dae8e4502f23d0e8135c887fb06268fc1ec.jpg'),
(621, NULL, 1, 38545, NULL, 1, '2014-04-15 00:46:42', '2014-04-15 00:46:42', '31dd719e1232158b73c67182bb1a1e63f47f776f', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_183272549.jpg', 'data/attachments/module/site/block/images-pile/2416/31dd719e1232158b73c67182bb1a1e63f47f776f.jpg'),
(622, 621, 1, 36945, NULL, NULL, '2014-04-15 00:46:42', '2014-04-15 00:46:42', 'cc43da931fff4a280438c97841678ab65453c9ef', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_183272549.jpg', 'data/attachments/module/site/block/images-pile/2416/cc43da931fff4a280438c97841678ab65453c9ef.jpg'),
(623, NULL, 1, 42153, NULL, 1, '2014-04-15 00:47:42', '2014-04-15 00:47:42', 'e1d96cfdb5e5a629a19ea7b2eb3d4970c66fe77d', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_153117146.jpg', 'data/attachments/module/site/block/images-pile/2416/e1d96cfdb5e5a629a19ea7b2eb3d4970c66fe77d.jpg'),
(624, 623, 1, 40218, NULL, NULL, '2014-04-15 00:47:42', '2014-04-15 00:47:42', '2dedfd383b6bd1a0d9401b83983be25656dd3a1e', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_153117146.jpg', 'data/attachments/module/site/block/images-pile/2416/2dedfd383b6bd1a0d9401b83983be25656dd3a1e.jpg'),
(625, NULL, 1, 40714, NULL, 1, '2014-04-15 00:53:37', '2014-04-15 00:53:37', '00ba5c87937a2583b00669988693e5152b5a68e0', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_172014281 (Kopiowanie).jpg', 'data/attachments/module/site/block/images-pile/2416/00ba5c87937a2583b00669988693e5152b5a68e0.jpg'),
(626, 625, 1, 38354, NULL, NULL, '2014-04-15 00:53:37', '2014-04-15 00:53:37', '12c519ab3e673f1b933d817762eb13b9f5b90aec', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_172014281 (Kopiowanie).jpg', 'data/attachments/module/site/block/images-pile/2416/12c519ab3e673f1b933d817762eb13b9f5b90aec.jpg'),
(627, NULL, 1, 40688, NULL, 1, '2014-04-15 01:03:15', '2014-04-15 01:03:15', 'd2a72a9c06401a2b4a1850d04d19d5ac0ea47fc0', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_103055114.jpg', 'data/attachments/module/site/block/images-pile/2416/d2a72a9c06401a2b4a1850d04d19d5ac0ea47fc0.jpg'),
(628, 627, 1, 37490, NULL, NULL, '2014-04-15 01:03:15', '2014-04-15 01:03:15', 'ce65bc40d094fa5b0f32be31aefc4bcdefc6a877', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,187', 'shutterstock_103055114.jpg', 'data/attachments/module/site/block/images-pile/2416/ce65bc40d094fa5b0f32be31aefc4bcdefc6a877.jpg'),
(629, NULL, 1, 48776, NULL, 1, '2014-04-15 09:50:34', '2014-04-15 09:50:34', 'aaae2aa3b2fe3825071c8ea54bbbf8dfa2f5f0a5', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_127978619.jpg', 'data/attachments/module/site/block/images-pile/2416/aaae2aa3b2fe3825071c8ea54bbbf8dfa2f5f0a5.jpg'),
(630, 629, 1, 45345, NULL, NULL, '2014-04-15 09:50:34', '2014-04-15 09:50:34', '417601bd7fceaefc4b80f7962ddd4250b4475b0e', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_127978619.jpg', 'data/attachments/module/site/block/images-pile/2416/417601bd7fceaefc4b80f7962ddd4250b4475b0e.jpg'),
(631, NULL, 1, 42802, NULL, 1, '2014-04-15 09:51:02', '2014-04-15 09:51:02', '6ee493534df0076de1c1a30e45dfeba02fb784ce', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_102017071.jpg', 'data/attachments/module/site/block/images-pile/2416/6ee493534df0076de1c1a30e45dfeba02fb784ce.jpg'),
(632, 631, 1, 36948, NULL, NULL, '2014-04-15 09:51:02', '2014-04-15 09:51:02', '36ef09c2eace17fbc2a33c41b12430e4f590f4a0', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_102017071.jpg', 'data/attachments/module/site/block/images-pile/2416/36ef09c2eace17fbc2a33c41b12430e4f590f4a0.jpg'),
(633, NULL, 1, 29221, NULL, 1, '2014-04-15 09:56:29', '2014-04-15 09:56:29', 'a7a138e53e13df1964958b99cc4854e4a44dd5f7', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_177093365 (1).jpg', 'data/attachments/module/site/block/images-pile/2416/a7a138e53e13df1964958b99cc4854e4a44dd5f7.jpg'),
(634, 633, 1, 29779, NULL, NULL, '2014-04-15 09:56:29', '2014-04-15 09:56:29', '6b8b61f17a9278f90715766d858efb5270b86107', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_177093365 (1).jpg', 'data/attachments/module/site/block/images-pile/2416/6b8b61f17a9278f90715766d858efb5270b86107.jpg'),
(635, NULL, 1, 64619, NULL, 1, '2014-04-15 10:01:35', '2014-04-15 10:01:35', 'c1bae760637b04bbb2965695b9ab1341fb4d7088', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_176887370.jpg', 'data/attachments/module/site/block/images-pile/2416/c1bae760637b04bbb2965695b9ab1341fb4d7088.jpg'),
(636, 635, 1, 58205, NULL, NULL, '2014-04-15 10:01:35', '2014-04-15 10:01:35', '74f884f2bd421a250230a6031433c0a589e03554', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,187', 'shutterstock_176887370.jpg', 'data/attachments/module/site/block/images-pile/2416/74f884f2bd421a250230a6031433c0a589e03554.jpg'),
(637, NULL, 1, 64619, NULL, 1, '2014-04-15 10:02:53', '2014-04-15 10:02:53', '753fa4e8cbc8b815369a44dd252897f27dacbb96', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_176887370.jpg', 'data/attachments/module/site/block/images-pile/2416/753fa4e8cbc8b815369a44dd252897f27dacbb96.jpg'),
(638, 637, 1, 58205, NULL, NULL, '2014-04-15 10:02:53', '2014-04-15 10:02:53', '7a3af9081e628eddc14d4dc4d20245672b5ee23c', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,187', 'shutterstock_176887370.jpg', 'data/attachments/module/site/block/images-pile/2416/7a3af9081e628eddc14d4dc4d20245672b5ee23c.jpg'),
(639, NULL, 1, 38740, NULL, 1, '2014-04-15 10:12:44', '2014-04-15 10:12:44', 'b6763d455341ef4439d51e9d7869a749383486fd', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'iStock_000014616600Large.jpg', 'data/attachments/module/site/block/images-pile/2416/b6763d455341ef4439d51e9d7869a749383486fd.jpg'),
(640, 639, 1, 36032, NULL, NULL, '2014-04-15 10:12:44', '2014-04-15 10:12:44', 'd166f6495314af468093e5694875998250ed5733', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'iStock_000014616600Large.jpg', 'data/attachments/module/site/block/images-pile/2416/d166f6495314af468093e5694875998250ed5733.jpg'),
(641, NULL, 1, 58129, NULL, 1, '2014-04-15 10:14:03', '2014-04-15 10:14:03', '5038d88079caddb3002aa0adea81587d44bf03f0', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'ChromaStock_30236701.jpg', 'data/attachments/module/site/block/images-pile/2416/5038d88079caddb3002aa0adea81587d44bf03f0.jpg'),
(642, 641, 1, 55782, NULL, NULL, '2014-04-15 10:14:03', '2014-04-15 10:14:03', 'a858a01cb6b946db1f0661d5d2c1b6d2c65cd94d', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'ChromaStock_30236701.jpg', 'data/attachments/module/site/block/images-pile/2416/a858a01cb6b946db1f0661d5d2c1b6d2c65cd94d.jpg'),
(643, NULL, 1, 60386, NULL, 1, '2014-04-15 10:44:12', '2014-04-15 10:44:12', '96711fe2914fbb5c1b1a3492f545758059ffa864', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'Fotolia_41354983_XS.jpg', 'data/attachments/module/site/block/news-list/1445/96711fe2914fbb5c1b1a3492f545758059ffa864.jpg'),
(644, 643, 1, 40248, NULL, NULL, '2014-04-15 10:44:12', '2014-04-15 10:44:12', '89ba51bc8e469fe11e1176fe26cb874ae14e92ab', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'Fotolia_41354983_XS.jpg', 'data/attachments/module/site/block/news-list/1445/89ba51bc8e469fe11e1176fe26cb874ae14e92ab.jpg'),
(645, NULL, 1, 19656, NULL, 1, '2014-04-15 11:04:34', '2014-04-15 11:04:34', '94cb99bd46215a4e33954b5c39e3600206057b15', 'png', 'image/png', 'site', 'image-slider', '', 'vistula-logo-kopia.png', 'data/attachments/module/site/block/image-slider/2198/94cb99bd46215a4e33954b5c39e3600206057b15.png'),
(646, 645, 1, 12740, NULL, NULL, '2014-04-15 11:04:43', '2014-04-15 11:04:34', 'd9f188f30a0233e20f5467609ffc2d23bc697c7c', 'png', 'image/png', 'site', 'image-slider', '0,25,135,100', 'vistula-logo-kopia.png', 'data/attachments/module/site/block/image-slider/2198/d9f188f30a0233e20f5467609ffc2d23bc697c7c.png'),
(647, NULL, 1, 21445, NULL, 1, '2014-04-15 11:19:29', '2014-04-15 11:19:29', 'fd10547070ba604a180f3711533c67f32fcacec7', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'znak_BZWBK_SANTANDER.jpg', 'data/attachments/module/site/block/image-slider/2198/fd10547070ba604a180f3711533c67f32fcacec7.jpg'),
(648, 647, 1, 10304, NULL, NULL, '2014-04-15 11:19:29', '2014-04-15 11:19:29', '168a2554ae037e95512e665623833743ebd0119e', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,135,75', 'znak_BZWBK_SANTANDER.jpg', 'data/attachments/module/site/block/image-slider/2198/168a2554ae037e95512e665623833743ebd0119e.jpg'),
(649, NULL, 1, 9413, NULL, 1, '2014-04-15 11:27:21', '2014-04-15 11:27:21', '7fde74eeb9b3a32ddd001d882a8e95d63f4bcea5', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'bank zachodni.jpg', 'data/attachments/module/site/block/image-slider/2198/7fde74eeb9b3a32ddd001d882a8e95d63f4bcea5.jpg'),
(650, 649, 1, 9824, NULL, NULL, '2014-04-15 11:27:21', '2014-04-15 11:27:21', 'c0db1fb76aa1708d83bdac1d69297db61d3988d1', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,135,75', 'bank zachodni.jpg', 'data/attachments/module/site/block/image-slider/2198/c0db1fb76aa1708d83bdac1d69297db61d3988d1.jpg'),
(651, NULL, 1, 6535, NULL, 1, '2014-04-15 11:34:30', '2014-04-15 11:34:30', '6bc424b5947eeb19dce4a00dd9b8f90c2299e913', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'PLUS.jpg', 'data/attachments/module/site/block/image-slider/2198/6bc424b5947eeb19dce4a00dd9b8f90c2299e913.jpg'),
(652, 651, 1, 6793, NULL, NULL, '2014-04-15 11:34:30', '2014-04-15 11:34:30', 'e04cebc135e6dc4921c3374c1f294567bf2e36c7', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,135,75', 'PLUS.jpg', 'data/attachments/module/site/block/image-slider/2198/e04cebc135e6dc4921c3374c1f294567bf2e36c7.jpg'),
(653, NULL, 1, 4148, NULL, 1, '2014-04-15 11:40:30', '2014-04-15 11:40:30', '8dde031babd23e6c1b2cd4f33afa8a92f5133829', 'gif', 'image/gif', 'site', 'image-slider', '', 'animex2.gif', 'data/attachments/module/site/block/image-slider/2198/8dde031babd23e6c1b2cd4f33afa8a92f5133829.gif'),
(654, 653, 1, 2579, NULL, NULL, '2014-04-15 11:40:30', '2014-04-15 11:40:30', 'ed6a79f2cfc5ca0e2c33f744b0ac9d32d18163ef', 'gif', 'image/gif', 'site', 'image-slider', '0,0,135,75', 'animex2.gif', 'data/attachments/module/site/block/image-slider/2198/ed6a79f2cfc5ca0e2c33f744b0ac9d32d18163ef.gif'),
(655, NULL, 1, 6441, NULL, 1, '2014-04-15 11:40:49', '2014-04-15 11:40:49', 'c5ea1a5567cfd58ac35d1ba9a9e650155bba2bc5', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'animex.jpg', 'data/attachments/module/site/block/image-slider/2198/c5ea1a5567cfd58ac35d1ba9a9e650155bba2bc5.jpg'),
(656, 655, 1, 6533, NULL, NULL, '2014-04-15 11:40:49', '2014-04-15 11:40:49', '635af5a47310745c42cefa5dced51eda0d981061', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,135,75', 'animex.jpg', 'data/attachments/module/site/block/image-slider/2198/635af5a47310745c42cefa5dced51eda0d981061.jpg'),
(657, NULL, 1, 9180, NULL, 1, '2014-04-15 11:44:19', '2014-04-15 11:44:19', 'fee752283f14598ddb4055fd3972da2af4ad78c8', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'Emmerson.jpg', 'data/attachments/module/site/block/image-slider/2198/fee752283f14598ddb4055fd3972da2af4ad78c8.jpg'),
(658, 657, 1, 9373, NULL, NULL, '2014-04-15 11:44:19', '2014-04-15 11:44:19', '3ad2a0d37517ed8044b1fe167630f25da5bcfa61', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,135,75', 'Emmerson.jpg', 'data/attachments/module/site/block/image-slider/2198/3ad2a0d37517ed8044b1fe167630f25da5bcfa61.jpg'),
(659, NULL, 1, 7588, NULL, 1, '2014-04-15 11:56:36', '2014-04-15 11:56:36', 'cb291a28093366f88bd69a6e3540926f93ba1108', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'city-handlowy-logo.jpg', 'data/attachments/module/site/block/image-slider/2198/cb291a28093366f88bd69a6e3540926f93ba1108.jpg'),
(660, 659, 1, 7143, NULL, NULL, '2014-04-15 11:56:36', '2014-04-15 11:56:36', 'b0e032ef72a1afc6e869ccaa678d40f26505e27c', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,135,75', 'city-handlowy-logo.jpg', 'data/attachments/module/site/block/image-slider/2198/b0e032ef72a1afc6e869ccaa678d40f26505e27c.jpg'),
(661, NULL, 1, 9180, NULL, 1, '2014-04-15 11:57:51', '2014-04-15 11:57:51', '1bc1dde6a77a6025b6f0eadbffa103d8cbbc350a', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'Emmerson.jpg', 'data/attachments/module/site/block/image-slider/2198/1bc1dde6a77a6025b6f0eadbffa103d8cbbc350a.jpg'),
(662, 661, 1, 9373, NULL, NULL, '2014-04-15 11:57:51', '2014-04-15 11:57:51', 'fde53f6a6a5ba86e556ddf03755b761cc3f8c635', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,135,75', 'Emmerson.jpg', 'data/attachments/module/site/block/image-slider/2198/fde53f6a6a5ba86e556ddf03755b761cc3f8c635.jpg'),
(663, NULL, 1, 5770, NULL, 1, '2014-04-15 12:01:55', '2014-04-15 12:01:55', '9b39e0be5dee3c3ee4f1561038b881e4820caf9b', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'Nobel Bank.jpg', 'data/attachments/module/site/block/image-slider/2198/9b39e0be5dee3c3ee4f1561038b881e4820caf9b.jpg'),
(664, 663, 1, 5817, NULL, NULL, '2014-04-15 12:01:55', '2014-04-15 12:01:55', '52023f2102494669ee70a67369aaa2db41b03ef6', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,135,75', 'Nobel Bank.jpg', 'data/attachments/module/site/block/image-slider/2198/52023f2102494669ee70a67369aaa2db41b03ef6.jpg');
INSERT INTO `attachments` (`attachment_id`, `parent_attachment_id`, `is_published`, `file_size`, `page_block_id`, `last_update_author_id`, `last_update_date`, `creation_date`, `hash_for_file_name`, `extension`, `mime_type`, `module_name`, `block_name`, `image_crop_rect_coords`, `original_file_name`, `file_path`) VALUES
(665, NULL, 1, 44466, 2323, 1, '2014-04-15 12:31:53', '2014-04-15 12:31:53', '4d6d5629537c10f4ee7e1d71cc350f8b34759faa', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'test.jpg', 'data/attachments/module/site/block/image-slider/2323/4d6d5629537c10f4ee7e1d71cc350f8b34759faa.jpg'),
(666, 665, 1, 39019, 2323, NULL, '2014-04-15 12:31:59', '2014-04-15 12:31:53', 'b58929c92e80f02c288c02533520cd60d76a1f7b', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,7,250,157', 'test.jpg', 'data/attachments/module/site/block/image-slider/2323/b58929c92e80f02c288c02533520cd60d76a1f7b.jpg'),
(667, NULL, 1, 10406, NULL, 1, '2014-04-15 12:32:18', '2014-04-15 12:32:18', 'd5e2282e2b8b6d2189eb00bcf61d35e2060fa55b', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'Instutu turystyki.jpg', 'data/attachments/module/site/block/image-slider/2198/d5e2282e2b8b6d2189eb00bcf61d35e2060fa55b.jpg'),
(668, 667, 1, 8935, NULL, NULL, '2014-04-15 12:32:53', '2014-04-15 12:32:18', '304d6fa04d8fabb809fd312f12a1464e2e076b86', 'jpg', 'image/jpeg', 'site', 'image-slider', '51,0,186,75', 'Instutu turystyki.jpg', 'data/attachments/module/site/block/image-slider/2198/304d6fa04d8fabb809fd312f12a1464e2e076b86.jpg'),
(669, NULL, 1, 13656, NULL, 1, '2014-04-15 12:39:26', '2014-04-15 12:39:26', 'e7262063c832d3b1ef1d1d3eb4e7f424529252e7', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'mt polska.jpg', 'data/attachments/module/site/block/image-slider/2198/e7262063c832d3b1ef1d1d3eb4e7f424529252e7.jpg'),
(670, 669, 1, 7777, NULL, NULL, '2014-04-15 12:39:26', '2014-04-15 12:39:26', '09f7be2de5985efa728de9ce7e002ecc6216754c', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,135,75', 'mt polska.jpg', 'data/attachments/module/site/block/image-slider/2198/09f7be2de5985efa728de9ce7e002ecc6216754c.jpg'),
(671, NULL, 1, 7292, NULL, 1, '2014-04-15 12:41:18', '2014-04-15 12:41:18', '0017cd6aed84f5c1b8d220938ded6b771f586691', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'mttargi.jpg', 'data/attachments/module/site/block/image-slider/2198/0017cd6aed84f5c1b8d220938ded6b771f586691.jpg'),
(672, 671, 1, 7483, NULL, NULL, '2014-04-15 12:41:18', '2014-04-15 12:41:18', 'cf50d1637b20e7a73ad4a24ea06f37a22b9259be', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,135,75', 'mttargi.jpg', 'data/attachments/module/site/block/image-slider/2198/cf50d1637b20e7a73ad4a24ea06f37a22b9259be.jpg'),
(673, NULL, 1, 5600, NULL, 1, '2014-04-15 12:46:26', '2014-04-15 12:46:26', 'c4fed4720e0258e8fb5bfda4e5c91ec364909582', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'qloc.jpg', 'data/attachments/module/site/block/image-slider/2198/c4fed4720e0258e8fb5bfda4e5c91ec364909582.jpg'),
(674, 673, 1, 5742, NULL, NULL, '2014-04-15 12:46:26', '2014-04-15 12:46:26', 'bf3a884679d568e2be687b9f2b102c90c95a7447', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,135,75', 'qloc.jpg', 'data/attachments/module/site/block/image-slider/2198/bf3a884679d568e2be687b9f2b102c90c95a7447.jpg'),
(675, NULL, 1, 5359, NULL, 1, '2014-04-15 12:50:31', '2014-04-15 12:50:31', '85926830fa5a0837ecb7d525aa1f8fcfaa11a7f7', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'upc.jpg', 'data/attachments/module/site/block/image-slider/2198/85926830fa5a0837ecb7d525aa1f8fcfaa11a7f7.jpg'),
(676, 675, 1, 5458, NULL, NULL, '2014-04-15 12:50:31', '2014-04-15 12:50:31', '71c0dff75f65fe4d1ead8f329d93d921868c2aa1', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,135,75', 'upc.jpg', 'data/attachments/module/site/block/image-slider/2198/71c0dff75f65fe4d1ead8f329d93d921868c2aa1.jpg'),
(677, NULL, 1, 5359, NULL, 1, '2014-04-15 12:57:12', '2014-04-15 12:57:12', '3a203cd84f684cabb8ba9dcb242c8eab142b0f7e', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'upc.jpg', 'data/attachments/module/site/block/image-slider/2198/3a203cd84f684cabb8ba9dcb242c8eab142b0f7e.jpg'),
(678, 677, 1, 5458, NULL, NULL, '2014-04-15 12:57:12', '2014-04-15 12:57:12', 'fe08596afc439115b7b7a5b1dc97ef6e4aa5ee4e', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,135,75', 'upc.jpg', 'data/attachments/module/site/block/image-slider/2198/fe08596afc439115b7b7a5b1dc97ef6e4aa5ee4e.jpg'),
(679, NULL, 1, 3895, NULL, 1, '2014-04-15 12:58:41', '2014-04-15 12:58:41', '6d019d429af55fb091b91c2547c97d945ff67190', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'Saint_gobain.jpg', 'data/attachments/module/site/block/image-slider/2198/6d019d429af55fb091b91c2547c97d945ff67190.jpg'),
(680, 679, 1, 3894, NULL, NULL, '2014-04-15 12:58:41', '2014-04-15 12:58:41', '5ab5d4187cdc28ad9386a34e0b92844bb6d5c49d', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,135,75', 'Saint_gobain.jpg', 'data/attachments/module/site/block/image-slider/2198/5ab5d4187cdc28ad9386a34e0b92844bb6d5c49d.jpg'),
(681, NULL, 1, 1365, NULL, 1, '2014-04-15 13:09:40', '2014-04-15 13:09:40', 'ef7c940d081b0f749cd5f4ee061790424a5d6b97', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'tło.jpg', 'data/attachments/module/site/block/bachelor-studies/2279/ef7c940d081b0f749cd5f4ee061790424a5d6b97.jpg'),
(682, 681, 1, 1365, NULL, NULL, '2014-04-15 13:09:41', '2014-04-15 13:09:40', '17744bc3c11da6d73c274fa8cdf72ab8c3ba85e0', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '0,0,200,200', 'tło.jpg', 'data/attachments/module/site/block/bachelor-studies/2279/17744bc3c11da6d73c274fa8cdf72ab8c3ba85e0.jpg'),
(683, NULL, 1, 16112, NULL, 1, '2014-04-15 13:11:06', '2014-04-15 13:11:06', '5dd63728f997fb215669ad3cb93f50d71082c454', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'tło.jpg', 'data/attachments/module/site/block/news-list/2431/5dd63728f997fb215669ad3cb93f50d71082c454.jpg'),
(684, 683, 1, 9173, NULL, NULL, '2014-04-15 13:11:06', '2014-04-15 13:11:06', 'beaecd797710e72513f8928f91643bb92d2c036f', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'tło.jpg', 'data/attachments/module/site/block/news-list/2431/beaecd797710e72513f8928f91643bb92d2c036f.jpg'),
(685, NULL, 1, 16112, NULL, 1, '2014-04-15 13:11:25', '2014-04-15 13:11:25', '29ed9972b528f330416247a2dec119130b3b6774', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'tło.jpg', 'data/attachments/module/site/block/news-list/2431/29ed9972b528f330416247a2dec119130b3b6774.jpg'),
(686, 685, 1, 9173, NULL, NULL, '2014-04-15 13:11:25', '2014-04-15 13:11:25', '3dc5662ddb74fc53df0854e34eac5d2360a347bf', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'tło.jpg', 'data/attachments/module/site/block/news-list/2431/3dc5662ddb74fc53df0854e34eac5d2360a347bf.jpg'),
(687, NULL, 1, 16112, NULL, 1, '2014-04-15 13:11:43', '2014-04-15 13:11:43', '678b0bfbd529d2157521020860d2dde1a928b6eb', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'tło.jpg', 'data/attachments/module/site/block/news-list/2431/678b0bfbd529d2157521020860d2dde1a928b6eb.jpg'),
(688, 687, 1, 9173, NULL, NULL, '2014-04-15 13:11:43', '2014-04-15 13:11:43', 'a8d73acaa345e77b0caee40f13527652cc2ee300', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'tło.jpg', 'data/attachments/module/site/block/news-list/2431/a8d73acaa345e77b0caee40f13527652cc2ee300.jpg'),
(689, NULL, 1, 44199, NULL, 1, '2014-04-15 13:28:23', '2014-04-15 13:28:23', '14ab38a72d509d1903b2f4f6a8e2507e028260ba', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'ChromaStock_8832604.jpg', 'data/attachments/module/site/block/news-list/1445/14ab38a72d509d1903b2f4f6a8e2507e028260ba.jpg'),
(690, 689, 1, 43383, NULL, NULL, '2014-04-15 13:28:23', '2014-04-15 13:28:23', '4e19291111d1d472b0c1378b6d0cddb100cae310', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'ChromaStock_8832604.jpg', 'data/attachments/module/site/block/news-list/1445/4e19291111d1d472b0c1378b6d0cddb100cae310.jpg'),
(691, NULL, 1, 32181, NULL, 1, '2014-04-15 13:45:28', '2014-04-15 13:45:28', '23c5f3031d5b3325ae09d73f853c730e78818fb1', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'shutterstock_164107232.jpg', 'data/attachments/module/site/block/news-list/2434/23c5f3031d5b3325ae09d73f853c730e78818fb1.jpg'),
(692, 691, 1, 25289, NULL, NULL, '2014-04-15 13:45:28', '2014-04-15 13:45:28', '3bbfb3ee09c7897a01ddd6a08f35bec8a2d217db', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'shutterstock_164107232.jpg', 'data/attachments/module/site/block/news-list/2434/3bbfb3ee09c7897a01ddd6a08f35bec8a2d217db.jpg'),
(693, NULL, 1, 16112, NULL, 1, '2014-04-15 13:46:03', '2014-04-15 13:46:03', 'b3ef32ade8b27a294587af0e13153c1c6200113c', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'tło.jpg', 'data/attachments/module/site/block/news-list/2434/b3ef32ade8b27a294587af0e13153c1c6200113c.jpg'),
(694, 693, 1, 9173, NULL, NULL, '2014-04-15 13:46:03', '2014-04-15 13:46:03', 'b86130f6982c15d277fceac907cbc7d24c8e96e5', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'tło.jpg', 'data/attachments/module/site/block/news-list/2434/b86130f6982c15d277fceac907cbc7d24c8e96e5.jpg'),
(695, NULL, 1, 16112, NULL, 1, '2014-04-15 13:46:20', '2014-04-15 13:46:20', '068a45e2abd54246b7f15e591fd9424ad8c346af', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'tło.jpg', 'data/attachments/module/site/block/news-list/2434/068a45e2abd54246b7f15e591fd9424ad8c346af.jpg'),
(696, 695, 1, 9173, NULL, NULL, '2014-04-15 13:46:20', '2014-04-15 13:46:20', 'cadef18114ed0773edf3145d8295047b35b486ab', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'tło.jpg', 'data/attachments/module/site/block/news-list/2434/cadef18114ed0773edf3145d8295047b35b486ab.jpg'),
(697, NULL, 1, 16112, NULL, 1, '2014-04-15 13:47:19', '2014-04-15 13:47:19', '88c88ff44bcca12e286902d0821864084db87576', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'tło.jpg', 'data/attachments/module/site/block/news-list/2434/88c88ff44bcca12e286902d0821864084db87576.jpg'),
(698, 697, 1, 9173, NULL, NULL, '2014-04-15 13:47:19', '2014-04-15 13:47:19', '059fa088a358789b1de219dbb956c7d7e244a6a9', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'tło.jpg', 'data/attachments/module/site/block/news-list/2434/059fa088a358789b1de219dbb956c7d7e244a6a9.jpg'),
(699, NULL, 1, 16112, NULL, 1, '2014-04-15 13:49:29', '2014-04-15 13:49:29', 'dd294b60a48060fe3dae1100c4a9b48642c67338', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'tło.jpg', 'data/attachments/module/site/block/news-list/2434/dd294b60a48060fe3dae1100c4a9b48642c67338.jpg'),
(700, 699, 1, 9173, NULL, NULL, '2014-04-15 13:49:30', '2014-04-15 13:49:29', 'bda449b790b5a8b4a5d6b0c22fa74b8bc39b2689', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'tło.jpg', 'data/attachments/module/site/block/news-list/2434/bda449b790b5a8b4a5d6b0c22fa74b8bc39b2689.jpg'),
(701, NULL, 1, 16112, NULL, 1, '2014-04-15 13:50:39', '2014-04-15 13:50:39', '72f9708c0b38dd89ddcb980b4aa0553e5bff3e0c', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'tło.jpg', 'data/attachments/module/site/block/news-list/2434/72f9708c0b38dd89ddcb980b4aa0553e5bff3e0c.jpg'),
(702, 701, 1, 9173, NULL, NULL, '2014-04-15 13:50:39', '2014-04-15 13:50:39', 'ff934e88410a252590a4294834707f7c748cf386', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'tło.jpg', 'data/attachments/module/site/block/news-list/2434/ff934e88410a252590a4294834707f7c748cf386.jpg'),
(703, NULL, 1, 16112, NULL, 1, '2014-04-15 13:52:51', '2014-04-15 13:52:51', '885a7c5a2f45a60426907c317147f52a660a3558', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'tło.jpg', 'data/attachments/module/site/block/news-list/2434/885a7c5a2f45a60426907c317147f52a660a3558.jpg'),
(704, 703, 1, 9173, NULL, NULL, '2014-04-15 13:52:51', '2014-04-15 13:52:51', '9cd7d2a17e7f3c7baebffb89136f24259f9f60bf', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'tło.jpg', 'data/attachments/module/site/block/news-list/2434/9cd7d2a17e7f3c7baebffb89136f24259f9f60bf.jpg'),
(705, NULL, 1, 16112, NULL, 1, '2014-04-15 14:20:58', '2014-04-15 14:20:58', '21ba2bfd84efc4c178373c0a2a287d24b8619614', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'tło.jpg', 'data/attachments/module/site/block/news-list/2431/21ba2bfd84efc4c178373c0a2a287d24b8619614.jpg'),
(706, 705, 1, 9173, NULL, NULL, '2014-04-15 14:20:58', '2014-04-15 14:20:58', '0ca56167f16e1188f0b84673d58cf7c8da81226b', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'tło.jpg', 'data/attachments/module/site/block/news-list/2431/0ca56167f16e1188f0b84673d58cf7c8da81226b.jpg'),
(707, NULL, 1, 49914, NULL, 1, '2014-04-15 14:55:30', '2014-04-15 14:55:30', '8743ec3b380ff9f21c083a4ac2c206cf48bb12b0', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_106223336.jpg', 'data/attachments/module/site/block/images-pile/2416/8743ec3b380ff9f21c083a4ac2c206cf48bb12b0.jpg'),
(708, 707, 1, 44653, NULL, NULL, '2014-04-15 14:55:30', '2014-04-15 14:55:30', 'cfb9e4f043d991f0a805356161afeef2873d75c3', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,187', 'shutterstock_106223336.jpg', 'data/attachments/module/site/block/images-pile/2416/cfb9e4f043d991f0a805356161afeef2873d75c3.jpg'),
(709, NULL, 1, 49341, NULL, 1, '2014-04-15 15:00:05', '2014-04-15 15:00:05', '3aeef8d1174f6d4e129761ee68a52f0d1e4c4b14', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_141892999.jpg', 'data/attachments/module/site/block/images-pile/2416/3aeef8d1174f6d4e129761ee68a52f0d1e4c4b14.jpg'),
(710, 709, 1, 44933, NULL, NULL, '2014-04-15 15:00:05', '2014-04-15 15:00:05', '3b03cc9e08992915dcc2b4dc9303cd0173366181', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_141892999.jpg', 'data/attachments/module/site/block/images-pile/2416/3b03cc9e08992915dcc2b4dc9303cd0173366181.jpg'),
(711, NULL, 1, 49341, NULL, 1, '2014-04-15 15:00:24', '2014-04-15 15:00:24', '00b593ca16e23f38c427b81b9d7f759a65cb3424', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_141892999.jpg', 'data/attachments/module/site/block/images-pile/2416/00b593ca16e23f38c427b81b9d7f759a65cb3424.jpg'),
(712, 711, 1, 44933, NULL, NULL, '2014-04-15 15:00:24', '2014-04-15 15:00:24', '9418e8f4b7e027eda5f29abc0a2dbdc5865f9290', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_141892999.jpg', 'data/attachments/module/site/block/images-pile/2416/9418e8f4b7e027eda5f29abc0a2dbdc5865f9290.jpg'),
(713, NULL, 1, 37704, NULL, 1, '2014-04-15 15:25:32', '2014-04-15 15:25:32', 'd6e2d37e81d22277310fc1319629a13d8bf20f09', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_103838987.jpg', 'data/attachments/module/site/block/images-pile/2416/d6e2d37e81d22277310fc1319629a13d8bf20f09.jpg'),
(714, 713, 1, 35050, NULL, NULL, '2014-04-15 15:25:32', '2014-04-15 15:25:32', '8da0cf58b5067b02863745c60bab2b0895f1e1d7', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,187', 'shutterstock_103838987.jpg', 'data/attachments/module/site/block/images-pile/2416/8da0cf58b5067b02863745c60bab2b0895f1e1d7.jpg'),
(715, NULL, 1, 37704, NULL, 1, '2014-04-15 15:25:58', '2014-04-15 15:25:58', '7ccc11a92271a9b4cb005c99868e9e90c54b8ff9', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_103838987.jpg', 'data/attachments/module/site/block/images-pile/2416/7ccc11a92271a9b4cb005c99868e9e90c54b8ff9.jpg'),
(716, 715, 1, 35050, NULL, NULL, '2014-04-15 15:25:58', '2014-04-15 15:25:58', '29f272619bdcb0beb4f286f944a437f0e6bfa9b6', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,187', 'shutterstock_103838987.jpg', 'data/attachments/module/site/block/images-pile/2416/29f272619bdcb0beb4f286f944a437f0e6bfa9b6.jpg'),
(717, NULL, 1, 49845, NULL, 1, '2014-04-15 15:31:16', '2014-04-15 15:31:16', 'cfffdad53d572d44c9439165f44ce217bfa43d84', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_158522147.jpg', 'data/attachments/module/site/block/images-pile/2416/cfffdad53d572d44c9439165f44ce217bfa43d84.jpg'),
(718, 717, 1, 46435, NULL, NULL, '2014-04-15 15:31:16', '2014-04-15 15:31:16', '592474dd67816e4b4bab89e55d9baa9873f1233f', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_158522147.jpg', 'data/attachments/module/site/block/images-pile/2416/592474dd67816e4b4bab89e55d9baa9873f1233f.jpg'),
(719, NULL, 1, 49845, NULL, 1, '2014-04-15 15:34:45', '2014-04-15 15:34:45', 'b8f88cbd03bd1059da6f4e829f2eca18c639f0da', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_158522147.jpg', 'data/attachments/module/site/block/images-pile/2416/b8f88cbd03bd1059da6f4e829f2eca18c639f0da.jpg'),
(720, 719, 1, 46435, NULL, NULL, '2014-04-15 15:34:45', '2014-04-15 15:34:45', '5b8da82d370b5730da4aa00251c56fd675e01448', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_158522147.jpg', 'data/attachments/module/site/block/images-pile/2416/5b8da82d370b5730da4aa00251c56fd675e01448.jpg'),
(721, NULL, 1, 39019, NULL, 1, '2014-04-15 15:39:31', '2014-04-15 15:39:31', '7933af4ba06bd2239df74bfdd65d875a05fb6e6c', 'jpg', 'image/jpeg', 'site', 'images-pile', '', 'shutterstock_153957728.jpg', 'data/attachments/module/site/block/images-pile/2416/7933af4ba06bd2239df74bfdd65d875a05fb6e6c.jpg'),
(722, 721, 1, 38463, NULL, NULL, '2014-04-15 15:39:31', '2014-04-15 15:39:31', '29180a90cd551cb1443b63e37dfa2446ec3fb3ef', 'jpg', 'image/jpeg', 'site', 'images-pile', '0,0,250,188', 'shutterstock_153957728.jpg', 'data/attachments/module/site/block/images-pile/2416/29180a90cd551cb1443b63e37dfa2446ec3fb3ef.jpg'),
(723, NULL, 1, 45645, NULL, 1, '2014-04-15 21:40:37', '2014-04-15 21:40:37', '46cabea8b458652a9af254389d699f9e6ea9af73', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '', 'shutterstock_153117146.jpg', 'data/attachments/module/site/block/bachelor-studies/2021/46cabea8b458652a9af254389d699f9e6ea9af73.jpg'),
(724, 723, 1, 34659, NULL, NULL, '2014-04-15 21:41:07', '2014-04-15 21:40:37', '9ac0ba6bbbf6af93657eb8fc5cc9b15f5e89c2cc', 'jpg', 'image/jpeg', 'site', 'bachelor-studies', '18,0,218,200', 'shutterstock_153117146.jpg', 'data/attachments/module/site/block/bachelor-studies/2021/9ac0ba6bbbf6af93657eb8fc5cc9b15f5e89c2cc.jpg'),
(725, NULL, 1, 46048, 2465, 1, '2014-04-15 22:09:05', '2014-04-15 22:09:05', '12cc22963f3a0744effab36f084737645ff6e10e', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'SAM_2199 (Kopiowanie).JPG', 'data/attachments/module/site/block/image-slider/2465/12cc22963f3a0744effab36f084737645ff6e10e.jpg'),
(726, 725, 1, 38062, 2465, NULL, '2014-04-15 22:09:05', '2014-04-15 22:09:05', '5ffedaba5afe5bcec023c857ec22a94b9b9f7ecd', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', 'SAM_2199 (Kopiowanie).JPG', 'data/attachments/module/site/block/image-slider/2465/5ffedaba5afe5bcec023c857ec22a94b9b9f7ecd.jpg'),
(727, NULL, 1, 40996, 2465, 1, '2014-04-15 22:09:25', '2014-04-15 22:09:25', 'dc6776cadde98386d7b7d9d3dfe001a1fd3e8e20', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'SAM_2203 (Kopiowanie).JPG', 'data/attachments/module/site/block/image-slider/2465/dc6776cadde98386d7b7d9d3dfe001a1fd3e8e20.jpg'),
(728, 727, 1, 34186, 2465, NULL, '2014-04-15 22:09:25', '2014-04-15 22:09:25', 'c84fd8590ecc31fb9f82e34e3743b9809e4490e0', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', 'SAM_2203 (Kopiowanie).JPG', 'data/attachments/module/site/block/image-slider/2465/c84fd8590ecc31fb9f82e34e3743b9809e4490e0.jpg'),
(729, NULL, 1, 32675, NULL, 1, '2014-04-15 22:16:01', '2014-04-15 22:16:01', '104cde64d7ff5af0e1ae978e106251111010f12d', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'StKo_260909_121.jpg', 'data/attachments/module/site/block/image-slider/2466/104cde64d7ff5af0e1ae978e106251111010f12d.jpg'),
(730, 729, 1, 30160, NULL, NULL, '2014-04-15 22:16:01', '2014-04-15 22:16:01', 'b80e43733a2517e43e89d204afcf19236ebbafeb', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', 'StKo_260909_121.jpg', 'data/attachments/module/site/block/image-slider/2466/b80e43733a2517e43e89d204afcf19236ebbafeb.jpg'),
(731, NULL, 1, 41176, NULL, 1, '2014-04-15 22:16:39', '2014-04-15 22:16:39', 'aef7ffb3cebedec946fdb32862c2ac982baca5b0', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'StKo_260909_133.jpg', 'data/attachments/module/site/block/image-slider/2466/aef7ffb3cebedec946fdb32862c2ac982baca5b0.jpg'),
(732, 731, 1, 37720, NULL, NULL, '2014-04-15 22:16:39', '2014-04-15 22:16:39', '5c020966977ea5313accde62717669efd33de7a9', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', 'StKo_260909_133.jpg', 'data/attachments/module/site/block/image-slider/2466/5c020966977ea5313accde62717669efd33de7a9.jpg'),
(733, NULL, 1, 46509, NULL, 1, '2014-04-15 22:17:54', '2014-04-15 22:17:54', '04bb52a186e29cb4bb420444dfe749bb0b9acdba', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'StKo_260909_007.jpg', 'data/attachments/module/site/block/image-slider/2466/04bb52a186e29cb4bb420444dfe749bb0b9acdba.jpg'),
(734, 733, 1, 43233, NULL, NULL, '2014-04-15 22:17:54', '2014-04-15 22:17:54', 'c48df04c36721153c27c534ef7bd376de6851fdd', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', 'StKo_260909_007.jpg', 'data/attachments/module/site/block/image-slider/2466/c48df04c36721153c27c534ef7bd376de6851fdd.jpg'),
(735, NULL, 1, 43890, NULL, 1, '2014-04-15 22:22:54', '2014-04-15 22:22:54', '3394ba7ae078289c574384c199f26defe08c481d', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'absolwentki (Kopiowanie).jpg', 'data/attachments/module/site/block/image-slider/2466/3394ba7ae078289c574384c199f26defe08c481d.jpg'),
(736, 735, 1, 37348, NULL, NULL, '2014-04-15 22:22:54', '2014-04-15 22:22:54', 'a5b460482bc969daa6ac397f85982e2907f5f006', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', 'absolwentki (Kopiowanie).jpg', 'data/attachments/module/site/block/image-slider/2466/a5b460482bc969daa6ac397f85982e2907f5f006.jpg'),
(737, NULL, 1, 42228, NULL, 1, '2014-04-15 22:24:35', '2014-04-15 22:24:35', 'dc638a761b8e66044b2b84de380200e6e9609a1d', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'INAUGURACJA (189).jpg', 'data/attachments/module/site/block/image-slider/2466/dc638a761b8e66044b2b84de380200e6e9609a1d.jpg'),
(738, 737, 1, 36043, NULL, NULL, '2014-04-15 22:24:35', '2014-04-15 22:24:35', 'c2070add87fbc7a79d68d2356e9b1965a360daff', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', 'INAUGURACJA (189).jpg', 'data/attachments/module/site/block/image-slider/2466/c2070add87fbc7a79d68d2356e9b1965a360daff.jpg'),
(739, NULL, 1, 49531, NULL, 1, '2014-04-15 22:27:44', '2014-04-15 22:27:44', '591b38a32d708225b3ac4694992b6192b3254d97', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'INAUGURACJA (225).jpg', 'data/attachments/module/site/block/image-slider/2466/591b38a32d708225b3ac4694992b6192b3254d97.jpg'),
(740, 739, 1, 47228, NULL, NULL, '2014-04-15 22:27:44', '2014-04-15 22:27:44', 'ffc7717ec13f372c5361cb2cbfcb38c663ae83f5', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', 'INAUGURACJA (225).jpg', 'data/attachments/module/site/block/image-slider/2466/ffc7717ec13f372c5361cb2cbfcb38c663ae83f5.jpg'),
(741, NULL, 1, 34165, NULL, 1, '2014-04-15 22:29:41', '2014-04-15 22:29:41', '234540a776ceb4dc6330f9dd5f5aab21c4d4b7c2', 'jpg', 'image/jpeg', 'site', 'image-slider', '', '497Z6667.JPG', 'data/attachments/module/site/block/image-slider/2466/234540a776ceb4dc6330f9dd5f5aab21c4d4b7c2.jpg'),
(742, 741, 1, 31522, NULL, NULL, '2014-04-15 22:29:41', '2014-04-15 22:29:41', '232cf52839e30f589e0fae6f2cbaf3d3924cd77b', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', '497Z6667.JPG', 'data/attachments/module/site/block/image-slider/2466/232cf52839e30f589e0fae6f2cbaf3d3924cd77b.jpg'),
(743, NULL, 1, 48230, NULL, 1, '2014-04-15 22:31:10', '2014-04-15 22:31:10', '3dfac2a7a3a2efa03a39b5cd16507e5b07ad2e5a', 'jpg', 'image/jpeg', 'site', 'image-slider', '', '497Z6784.JPG', 'data/attachments/module/site/block/image-slider/2466/3dfac2a7a3a2efa03a39b5cd16507e5b07ad2e5a.jpg'),
(744, 743, 1, 44125, NULL, NULL, '2014-04-15 22:31:10', '2014-04-15 22:31:10', 'a95055b69b8630e5490f67c0575af3c46fc68bda', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', '497Z6784.JPG', 'data/attachments/module/site/block/image-slider/2466/a95055b69b8630e5490f67c0575af3c46fc68bda.jpg'),
(745, NULL, 1, 589277, 2467, 1, '2014-04-15 22:36:53', '2014-04-15 22:36:53', '302b8a122fcf5f4ff4cc305d988855a6bf74fea2', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'shutterstock_94461523.jpg', 'data/attachments/module/site/block/small-banner/2467/302b8a122fcf5f4ff4cc305d988855a6bf74fea2.jpg'),
(746, 745, 1, 145351, 2467, NULL, '2014-04-15 22:36:53', '2014-04-15 22:36:53', '30e9304e8f4fc6405c6e1808afd2f16083d216bf', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,900,400', 'shutterstock_94461523.jpg', 'data/attachments/module/site/block/small-banner/2467/30e9304e8f4fc6405c6e1808afd2f16083d216bf.jpg'),
(747, NULL, 1, 317245, NULL, 1, '2014-04-15 22:48:09', '2014-04-15 22:48:09', '0bd52bf201f9d4bf446f66f4c731eec6ba2c76af', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'shutterstock_71663362.jpg', 'data/attachments/module/site/block/small-banner/2469/0bd52bf201f9d4bf446f66f4c731eec6ba2c76af.jpg'),
(748, 747, 1, 214460, NULL, NULL, '2014-04-15 22:48:09', '2014-04-15 22:48:09', 'dceb6d89606dc2879150ddac0ac1c39dbd3addbf', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,900,400', 'shutterstock_71663362.jpg', 'data/attachments/module/site/block/small-banner/2469/dceb6d89606dc2879150ddac0ac1c39dbd3addbf.jpg'),
(749, NULL, 1, 275852, NULL, 1, '2014-04-15 22:51:33', '2014-04-15 22:51:33', 'c896539f9dfbb6174289b5850e655f323db07ec3', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'shutterstock_69133186.jpg', 'data/attachments/module/site/block/small-banner/2469/c896539f9dfbb6174289b5850e655f323db07ec3.jpg'),
(750, 749, 1, 186296, NULL, NULL, '2014-04-15 22:51:33', '2014-04-15 22:51:33', '48a366109cc7dcf2c85bf7f0b7dff55182b12301', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,900,400', 'shutterstock_69133186.jpg', 'data/attachments/module/site/block/small-banner/2469/48a366109cc7dcf2c85bf7f0b7dff55182b12301.jpg'),
(751, NULL, 1, 283122, NULL, 1, '2014-04-16 00:12:24', '2014-04-16 00:12:24', 'efbbd32b8a8dc9e1c1524119bbaabf78e7303f48', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'shutterstock_114006016 (Kopiowanie).jpg', 'data/attachments/module/site/block/small-banner/2473/efbbd32b8a8dc9e1c1524119bbaabf78e7303f48.jpg'),
(752, 751, 1, 179355, NULL, NULL, '2014-04-16 00:12:24', '2014-04-16 00:12:24', '6c5aea1784ac9f7ae807a2f552942f98b1030a46', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,900,400', 'shutterstock_114006016 (Kopiowanie).jpg', 'data/attachments/module/site/block/small-banner/2473/6c5aea1784ac9f7ae807a2f552942f98b1030a46.jpg'),
(753, NULL, 1, 258288, NULL, 1, '2014-04-16 00:13:47', '2014-04-16 00:13:47', '1d4f070cc339b217b010453107164563f2fe4bc1', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'ChromaStock_1760748 (Kopiowanie).jpg', 'data/attachments/module/site/block/small-banner/2473/1d4f070cc339b217b010453107164563f2fe4bc1.jpg'),
(754, 753, 1, 179741, NULL, NULL, '2014-04-16 00:13:47', '2014-04-16 00:13:47', 'c408a661e7903487b039b8862b3622c7ee0c2505', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,900,400', 'ChromaStock_1760748 (Kopiowanie).jpg', 'data/attachments/module/site/block/small-banner/2473/c408a661e7903487b039b8862b3622c7ee0c2505.jpg'),
(755, NULL, 1, 227196, NULL, 1, '2014-04-16 00:16:14', '2014-04-16 00:16:14', 'aa47efb725ec71cd6815681615f8a4edba92b7e1', 'jpg', 'image/jpeg', 'site', 'small-banner', '', 'shutterstock_186067874 (Kopiowanie).jpg', 'data/attachments/module/site/block/small-banner/2473/aa47efb725ec71cd6815681615f8a4edba92b7e1.jpg'),
(756, 755, 1, 155030, NULL, NULL, '2014-04-16 00:16:14', '2014-04-16 00:16:14', '49c89f8130bfaa88138d4b95d9ae9a70736edd23', 'jpg', 'image/jpeg', 'site', 'small-banner', '0,0,900,400', 'shutterstock_186067874 (Kopiowanie).jpg', 'data/attachments/module/site/block/small-banner/2473/49c89f8130bfaa88138d4b95d9ae9a70736edd23.jpg'),
(757, NULL, 1, 42942, NULL, 1, '2014-04-16 00:35:21', '2014-04-16 00:35:21', '02bfaf1bd09d835bd594f4c88082eb27c9316696', 'jpg', 'image/jpeg', 'site', 'event-list', '', 'shutterstock_114006016 (Kopiowanie).jpg', 'data/attachments/module/site/block/event-list/2267/02bfaf1bd09d835bd594f4c88082eb27c9316696.jpg'),
(758, 757, 1, 35484, NULL, NULL, '2014-04-16 00:35:21', '2014-04-16 00:35:21', '701d8d3c791fa905bde600e4b94d9982ab0e3951', 'jpg', 'image/jpeg', 'site', 'event-list', '0,0,270,150', 'shutterstock_114006016 (Kopiowanie).jpg', 'data/attachments/module/site/block/event-list/2267/701d8d3c791fa905bde600e4b94d9982ab0e3951.jpg'),
(759, NULL, 1, 38083, NULL, 1, '2014-04-16 00:37:49', '2014-04-16 00:37:49', '99783cb4ffe10a5de3bcce5d158784f9acb64e8a', 'jpg', 'image/jpeg', 'site', 'event-list', '', 'shutterstock_172014281 (Kopiowanie).jpg', 'data/attachments/module/site/block/event-list/2267/99783cb4ffe10a5de3bcce5d158784f9acb64e8a.jpg'),
(760, 759, 1, 31292, NULL, NULL, '2014-04-16 00:37:49', '2014-04-16 00:37:49', '38892bbec8f9b468e4af24143d2b5071f2db36ea', 'jpg', 'image/jpeg', 'site', 'event-list', '0,0,270,150', 'shutterstock_172014281 (Kopiowanie).jpg', 'data/attachments/module/site/block/event-list/2267/38892bbec8f9b468e4af24143d2b5071f2db36ea.jpg'),
(761, NULL, 1, 42942, NULL, 1, '2014-04-16 09:14:02', '2014-04-16 09:14:02', '1540bb4bfe0fec5d5e6fd670c0dde48d85803e5a', 'jpg', 'image/jpeg', 'site', 'event-list', '', 'shutterstock_114006016 (Kopiowanie).jpg', 'data/attachments/module/site/block/event-list/2451/1540bb4bfe0fec5d5e6fd670c0dde48d85803e5a.jpg'),
(762, 761, 1, 35484, NULL, NULL, '2014-04-16 09:14:02', '2014-04-16 09:14:02', '10fdcb4dbfc10c139d98d0d41e27d5e3bea0811c', 'jpg', 'image/jpeg', 'site', 'event-list', '0,0,270,150', 'shutterstock_114006016 (Kopiowanie).jpg', 'data/attachments/module/site/block/event-list/2451/10fdcb4dbfc10c139d98d0d41e27d5e3bea0811c.jpg'),
(763, NULL, 1, 23526, 2919, 1, '2015-12-08 11:16:52', '2015-12-08 11:16:52', '557b56fd3619f8b0face684bf3e62ff16d373a6e', 'png', 'image/png', 'site', 'navigation-menu', '', 'dalej.png', 'data/attachments/module/site/block/navigation-menu/2919/557b56fd3619f8b0face684bf3e62ff16d373a6e.png'),
(764, 763, 1, 23526, 2919, NULL, '2015-12-08 11:16:53', '2015-12-08 11:16:52', 'e628439e79de20b376cd43d98e9dd6bb11acef13', 'png', 'image/png', 'site', 'navigation-menu', '0,0,300,300', 'dalej.png', 'data/attachments/module/site/block/navigation-menu/2919/e628439e79de20b376cd43d98e9dd6bb11acef13.png'),
(765, NULL, 1, 44937, 2924, 1, '2015-12-08 11:38:01', '2015-12-08 11:38:01', 'c6ad8dfd9b6dcdc82d1647edfdb9a3f81e38f796', 'png', 'image/png', 'site', 'event-list', '', 'start.png', 'data/attachments/module/site/block/event-list/2924/c6ad8dfd9b6dcdc82d1647edfdb9a3f81e38f796.png'),
(766, 765, 1, 28144, 2924, NULL, '2015-12-08 11:38:12', '2015-12-08 11:38:01', '770aa6cd24ffb8999ec11da0ccfb866d88380a69', 'png', 'image/png', 'site', 'event-list', '0,39,270,189', 'start.png', 'data/attachments/module/site/block/event-list/2924/770aa6cd24ffb8999ec11da0ccfb866d88380a69.png'),
(767, NULL, 1, 304323, 2927, 1, '2015-12-09 09:51:04', '2015-12-09 09:51:04', 'dc98769690dbfe1d61d67c6d4aa528fa6f6bb672', 'png', 'image/png', 'site', 'big-banner', '', 'ulotka_1_interaktywna-1.png', 'data/attachments/module/site/block/big-banner/2927/dc98769690dbfe1d61d67c6d4aa528fa6f6bb672.png'),
(768, 767, 1, 304323, 2927, NULL, '2015-12-09 09:51:06', '2015-12-09 09:51:04', '218f44fef50ad7c4aadf7e0637efad27f9bfa080', 'png', 'image/png', 'site', 'big-banner', '0,0,2000,430', 'ulotka_1_interaktywna-1.png', 'data/attachments/module/site/block/big-banner/2927/218f44fef50ad7c4aadf7e0637efad27f9bfa080.png'),
(769, NULL, 1, 37319, 2931, 1, '2015-12-09 10:01:15', '2015-12-09 10:01:15', '0a7c11cf7d3b806075dc8181f8fcc9324869a7d6', 'png', 'image/png', 'site', 'news-list', '', 'logo_pom_okragle.png', 'data/attachments/module/site/block/news-list/2931/0a7c11cf7d3b806075dc8181f8fcc9324869a7d6.png'),
(770, 769, 1, 20727, 2931, NULL, '2015-12-09 10:01:15', '2015-12-09 10:01:15', 'c844910e3fed9e1e9c1d20d1c1689fd5278a9093', 'png', 'image/png', 'site', 'news-list', '0,0,270,150', 'logo_pom_okragle.png', 'data/attachments/module/site/block/news-list/2931/c844910e3fed9e1e9c1d20d1c1689fd5278a9093.png'),
(771, NULL, 1, 208972, 2958, 1, '2015-12-09 12:30:45', '2015-12-09 12:30:45', '65bbf06a5f5130573f6cd55d657786d1c905d2b7', 'png', 'image/png', 'site', 'image-gallery', '', '1.png', 'data/attachments/module/site/block/image-gallery/2958/65bbf06a5f5130573f6cd55d657786d1c905d2b7.png'),
(772, 771, 1, 187264, 2958, NULL, '2015-12-09 12:30:46', '2015-12-09 12:30:45', '5ed0270a15f0d6b805beb91914b93f3cb4869e78', 'png', 'image/png', 'site', 'image-gallery', '0,0,400,300', '1.png', 'data/attachments/module/site/block/image-gallery/2958/5ed0270a15f0d6b805beb91914b93f3cb4869e78.png'),
(773, NULL, 1, 229427, 2958, 1, '2015-12-09 12:31:07', '2015-12-09 12:31:07', 'bfb3bd07dbf7639bc5073ee6fce532a1cc08b9bd', 'png', 'image/png', 'site', 'image-gallery', '', '2.png', 'data/attachments/module/site/block/image-gallery/2958/bfb3bd07dbf7639bc5073ee6fce532a1cc08b9bd.png'),
(774, 773, 1, 203672, 2958, NULL, '2015-12-09 12:31:07', '2015-12-09 12:31:07', '9ff7aeec7e05113bb972540e55d9245d73650b9a', 'png', 'image/png', 'site', 'image-gallery', '0,0,400,300', '2.png', 'data/attachments/module/site/block/image-gallery/2958/9ff7aeec7e05113bb972540e55d9245d73650b9a.png'),
(775, NULL, 1, 212167, 2958, 1, '2015-12-09 12:31:36', '2015-12-09 12:31:36', '24237981accc88b99045dd0730bd061b03ba6e8a', 'png', 'image/png', 'site', 'image-gallery', '', '3.png', 'data/attachments/module/site/block/image-gallery/2958/24237981accc88b99045dd0730bd061b03ba6e8a.png'),
(776, 775, 1, 191159, 2958, NULL, '2015-12-09 12:31:36', '2015-12-09 12:31:36', '56a4937cf9269c2dba960fd0af23410a6748f8a4', 'png', 'image/png', 'site', 'image-gallery', '0,0,400,300', '3.png', 'data/attachments/module/site/block/image-gallery/2958/56a4937cf9269c2dba960fd0af23410a6748f8a4.png'),
(777, NULL, 1, 164517, 2958, 1, '2015-12-09 12:31:53', '2015-12-09 12:31:53', 'cbb8dc2a9dac39aeea6b2f9a31aa6ef794f896c2', 'png', 'image/png', 'site', 'image-gallery', '', '4.png', 'data/attachments/module/site/block/image-gallery/2958/cbb8dc2a9dac39aeea6b2f9a31aa6ef794f896c2.png'),
(778, 777, 1, 148013, 2958, NULL, '2015-12-09 12:31:54', '2015-12-09 12:31:53', '7a134bd72ffe4e3aea5c5e2aa4b7b44a814094b8', 'png', 'image/png', 'site', 'image-gallery', '0,0,400,300', '4.png', 'data/attachments/module/site/block/image-gallery/2958/7a134bd72ffe4e3aea5c5e2aa4b7b44a814094b8.png'),
(779, NULL, 1, 76370, 2960, 1, '2015-12-09 12:35:19', '2015-12-09 12:35:19', '87d2ea3f2a7976b47393826c493f1d206cee20a3', 'png', 'image/png', 'site', 'image-slider', '', '1.png', 'data/attachments/module/site/block/image-slider/2960/87d2ea3f2a7976b47393826c493f1d206cee20a3.png'),
(780, 779, 1, 70143, 2960, NULL, '2015-12-09 12:35:20', '2015-12-09 12:35:19', '50f2f02d650145d1c09434fa892a61978ace2102', 'png', 'image/png', 'site', 'image-slider', '0,0,250,150', '1.png', 'data/attachments/module/site/block/image-slider/2960/50f2f02d650145d1c09434fa892a61978ace2102.png'),
(781, NULL, 1, 83890, 2960, 1, '2015-12-09 12:35:31', '2015-12-09 12:35:31', '5c4e848c3229ece8c7df87ac1b3ada0a10b7af93', 'png', 'image/png', 'site', 'image-slider', '', '2.png', 'data/attachments/module/site/block/image-slider/2960/5c4e848c3229ece8c7df87ac1b3ada0a10b7af93.png'),
(782, 781, 1, 76173, 2960, NULL, '2015-12-09 12:35:31', '2015-12-09 12:35:31', 'bc5cd28d6b21055a4e3582361bf31520281144f3', 'png', 'image/png', 'site', 'image-slider', '0,0,250,150', '2.png', 'data/attachments/module/site/block/image-slider/2960/bc5cd28d6b21055a4e3582361bf31520281144f3.png'),
(783, NULL, 1, 79417, 2960, 1, '2015-12-09 12:36:15', '2015-12-09 12:36:15', 'e5b281ed7544f029da3a7fc6e52af368e1df955b', 'png', 'image/png', 'site', 'image-slider', '', '3.png', 'data/attachments/module/site/block/image-slider/2960/e5b281ed7544f029da3a7fc6e52af368e1df955b.png'),
(784, 783, 1, 74671, 2960, NULL, '2015-12-09 12:36:15', '2015-12-09 12:36:15', 'd78610a283749cd1901849b203a84ccb55c2ed32', 'png', 'image/png', 'site', 'image-slider', '0,0,250,150', '3.png', 'data/attachments/module/site/block/image-slider/2960/d78610a283749cd1901849b203a84ccb55c2ed32.png'),
(785, NULL, 1, 63307, 2960, 1, '2015-12-09 12:36:33', '2015-12-09 12:36:33', 'aaf6770dc8992a3fdbe473c34d02d0ea23ee7ec2', 'png', 'image/png', 'site', 'image-slider', '', '4.png', 'data/attachments/module/site/block/image-slider/2960/aaf6770dc8992a3fdbe473c34d02d0ea23ee7ec2.png'),
(786, 785, 1, 56930, 2960, NULL, '2015-12-09 12:36:34', '2015-12-09 12:36:33', 'c58f67079e12e815e8876c3782abc9492a9a67dc', 'png', 'image/png', 'site', 'image-slider', '0,0,250,150', '4.png', 'data/attachments/module/site/block/image-slider/2960/c58f67079e12e815e8876c3782abc9492a9a67dc.png'),
(787, NULL, 1, 94305, 2965, 1, '2015-12-09 12:45:42', '2015-12-09 12:45:42', '7f0766b54e22787aba8d6b37b02e9942e00d32d6', 'png', 'image/png', 'site', 'images-pile', '', '1.png', 'data/attachments/module/site/block/images-pile/2965/7f0766b54e22787aba8d6b37b02e9942e00d32d6.png'),
(788, 787, 1, 83883, 2965, NULL, '2015-12-09 12:45:42', '2015-12-09 12:45:42', '4a77c2ac0eba8428c9127da441ce57f5104845ed', 'png', 'image/png', 'site', 'images-pile', '0,0,250,188', '1.png', 'data/attachments/module/site/block/images-pile/2965/4a77c2ac0eba8428c9127da441ce57f5104845ed.png'),
(789, NULL, 1, 103499, 2965, 1, '2015-12-09 12:47:35', '2015-12-09 12:47:35', 'fed5d3077f7f0c1301c53d91ef9f3506b00c77e7', 'png', 'image/png', 'site', 'images-pile', '', '2.png', 'data/attachments/module/site/block/images-pile/2965/fed5d3077f7f0c1301c53d91ef9f3506b00c77e7.png'),
(790, 789, 1, 91639, 2965, NULL, '2015-12-09 12:47:35', '2015-12-09 12:47:35', '99faf014d1cdb6fa838a97cbb95beeaab2a6bde4', 'png', 'image/png', 'site', 'images-pile', '0,0,250,188', '2.png', 'data/attachments/module/site/block/images-pile/2965/99faf014d1cdb6fa838a97cbb95beeaab2a6bde4.png'),
(791, NULL, 1, 97390, 2965, 1, '2015-12-09 12:47:46', '2015-12-09 12:47:46', '5ee1daf586e1d9b122b0f0b00a8bc7fd6798036a', 'png', 'image/png', 'site', 'images-pile', '', '3.png', 'data/attachments/module/site/block/images-pile/2965/5ee1daf586e1d9b122b0f0b00a8bc7fd6798036a.png'),
(792, 791, 1, 87858, 2965, NULL, '2015-12-09 12:47:46', '2015-12-09 12:47:46', '2b6000b6861db1b12ae78f3f3a5f5fcfbe05f60a', 'png', 'image/png', 'site', 'images-pile', '0,0,250,188', '3.png', 'data/attachments/module/site/block/images-pile/2965/2b6000b6861db1b12ae78f3f3a5f5fcfbe05f60a.png'),
(793, NULL, 1, 77130, 2965, 1, '2015-12-09 12:48:42', '2015-12-09 12:48:42', '2e3dac5a50579918c366240025e339f3c7d423bb', 'png', 'image/png', 'site', 'images-pile', '', '4.png', 'data/attachments/module/site/block/images-pile/2965/2e3dac5a50579918c366240025e339f3c7d423bb.png'),
(794, 793, 1, 69341, 2965, NULL, '2015-12-09 12:48:42', '2015-12-09 12:48:42', 'dcbd74692c6a86d966d1d60fba2ec90ead8035d9', 'png', 'image/png', 'site', 'images-pile', '0,0,250,188', '4.png', 'data/attachments/module/site/block/images-pile/2965/dcbd74692c6a86d966d1d60fba2ec90ead8035d9.png'),
(795, NULL, 1, 644327, 2967, 1, '2015-12-09 12:57:03', '2015-12-09 12:57:03', 'bd634a84690021b4dc250b5760d88cd7bfeaab3a', 'png', 'image/png', 'site', 'small-banner', '', '1.png', 'data/attachments/module/site/block/small-banner/2967/bd634a84690021b4dc250b5760d88cd7bfeaab3a.png'),
(796, 795, 1, 272107, 2967, NULL, '2015-12-09 12:57:25', '2015-12-09 12:57:03', 'ae1c7fafc746987fa447f175aa6a09e94ebe93d6', 'png', 'image/png', 'site', 'small-banner', '0,52,900,302', '1.png', 'data/attachments/module/site/block/small-banner/2967/ae1c7fafc746987fa447f175aa6a09e94ebe93d6.png'),
(797, NULL, 1, 87575, 2931, 1, '2015-12-09 13:02:21', '2015-12-09 13:02:21', '9e1921f6bd9a8399e940db44da60f24f5f03dbb9', 'png', 'image/png', 'site', 'news-list', '', '1.png', 'data/attachments/module/site/block/news-list/2931/9e1921f6bd9a8399e940db44da60f24f5f03dbb9.png'),
(798, 797, 1, 75119, 2931, NULL, '2015-12-09 13:02:21', '2015-12-09 13:02:21', '1bba0dfafe20670cd7fddd162049bf868e24bfe8', 'png', 'image/png', 'site', 'news-list', '0,0,270,150', '1.png', 'data/attachments/module/site/block/news-list/2931/1bba0dfafe20670cd7fddd162049bf868e24bfe8.png'),
(799, NULL, 1, 96325, 2931, 1, '2015-12-09 13:02:34', '2015-12-09 13:02:34', 'b27b9cd49b0c0a70f46db1f397a526f30e297a3d', 'png', 'image/png', 'site', 'news-list', '', '2.png', 'data/attachments/module/site/block/news-list/2931/b27b9cd49b0c0a70f46db1f397a526f30e297a3d.png'),
(800, 799, 1, 81078, 2931, NULL, '2015-12-09 13:02:34', '2015-12-09 13:02:34', '9f702aabab521a53f3a413d809afaacc31db40f7', 'png', 'image/png', 'site', 'news-list', '0,0,270,150', '2.png', 'data/attachments/module/site/block/news-list/2931/9f702aabab521a53f3a413d809afaacc31db40f7.png'),
(801, NULL, 1, 90698, 2931, 1, '2015-12-09 13:02:42', '2015-12-09 13:02:42', '288e23a422fcd80db6f3cf529df00f503f37b302', 'png', 'image/png', 'site', 'news-list', '', '3.png', 'data/attachments/module/site/block/news-list/2931/288e23a422fcd80db6f3cf529df00f503f37b302.png'),
(802, 801, 1, 80092, 2931, NULL, '2015-12-09 13:02:42', '2015-12-09 13:02:42', '1c89a2d28635f835bf7c9e0d5051f9a9e89233c2', 'png', 'image/png', 'site', 'news-list', '0,0,270,150', '3.png', 'data/attachments/module/site/block/news-list/2931/1c89a2d28635f835bf7c9e0d5051f9a9e89233c2.png'),
(803, NULL, 1, 71988, 2931, 1, '2015-12-09 13:02:48', '2015-12-09 13:02:48', '99fbaf709559883761ff4557446e8faf60f1c32b', 'png', 'image/png', 'site', 'news-list', '', '4.png', 'data/attachments/module/site/block/news-list/2931/99fbaf709559883761ff4557446e8faf60f1c32b.png'),
(804, 803, 1, 60105, 2931, NULL, '2015-12-09 13:02:48', '2015-12-09 13:02:48', '5d782c7b27f52d34eddf9230eec030bf28da0c69', 'png', 'image/png', 'site', 'news-list', '0,0,270,150', '4.png', 'data/attachments/module/site/block/news-list/2931/5d782c7b27f52d34eddf9230eec030bf28da0c69.png'),
(805, NULL, 1, 96325, 2931, 1, '2015-12-09 13:15:29', '2015-12-09 13:15:29', '112f83cad103c33cc3804ad7cf7b8e6cabda2c52', 'png', 'image/png', 'site', 'news-list', '', '2.png', 'data/attachments/module/site/block/news-list/2931/112f83cad103c33cc3804ad7cf7b8e6cabda2c52.png'),
(806, 805, 1, 81078, 2931, NULL, '2015-12-09 13:15:29', '2015-12-09 13:15:29', 'a54b4a02a8658febe136971c75a4eef38e68d0ef', 'png', 'image/png', 'site', 'news-list', '0,0,270,150', '2.png', 'data/attachments/module/site/block/news-list/2931/a54b4a02a8658febe136971c75a4eef38e68d0ef.png'),
(807, NULL, 1, 90698, 2931, 1, '2015-12-09 13:16:20', '2015-12-09 13:16:20', 'e8dd336c36b756f950855ad5b0bd4db1a11a6adf', 'png', 'image/png', 'site', 'news-list', '', '3.png', 'data/attachments/module/site/block/news-list/2931/e8dd336c36b756f950855ad5b0bd4db1a11a6adf.png'),
(808, 807, 1, 80092, 2931, NULL, '2015-12-09 13:16:20', '2015-12-09 13:16:20', 'e6cc8c9ea4534caf41a72bdf9e015dffbafcd784', 'png', 'image/png', 'site', 'news-list', '0,0,270,150', '3.png', 'data/attachments/module/site/block/news-list/2931/e6cc8c9ea4534caf41a72bdf9e015dffbafcd784.png'),
(809, NULL, 1, 90698, 2931, 1, '2015-12-09 13:21:14', '2015-12-09 13:21:14', '51e6658b3ccc1a4f07364662b1c4de9245d7284a', 'png', 'image/png', 'site', 'news-list', '', '3.png', 'data/attachments/module/site/block/news-list/2931/51e6658b3ccc1a4f07364662b1c4de9245d7284a.png'),
(810, 809, 1, 80092, 2931, NULL, '2015-12-09 13:21:14', '2015-12-09 13:21:14', 'f475926083a462daf2c3e6bc2555cd9cfcc008af', 'png', 'image/png', 'site', 'news-list', '0,0,270,150', '3.png', 'data/attachments/module/site/block/news-list/2931/f475926083a462daf2c3e6bc2555cd9cfcc008af.png'),
(811, NULL, 1, 71988, 2931, 1, '2015-12-09 13:23:10', '2015-12-09 13:23:10', 'bcb2def7abbc37df1499621c447186b1e1d024e7', 'png', 'image/png', 'site', 'news-list', '', '4.png', 'data/attachments/module/site/block/news-list/2931/bcb2def7abbc37df1499621c447186b1e1d024e7.png'),
(812, 811, 1, 60105, 2931, NULL, '2015-12-09 13:23:10', '2015-12-09 13:23:10', '6fd87df332533f91d206f500f438d4c6a7b98565', 'png', 'image/png', 'site', 'news-list', '0,0,270,150', '4.png', 'data/attachments/module/site/block/news-list/2931/6fd87df332533f91d206f500f438d4c6a7b98565.png'),
(813, NULL, 1, 33335, 2931, 1, '2015-12-09 13:30:37', '2015-12-09 13:30:37', '87460a0b8d9a14db6ed20154a0d0cd7f266491b5', 'jpg', 'image/jpeg', 'site', 'news-list', '', 'oferta.jpg', 'data/attachments/module/site/block/news-list/2931/87460a0b8d9a14db6ed20154a0d0cd7f266491b5.jpg'),
(814, 813, 1, 33641, 2931, NULL, '2015-12-09 13:30:37', '2015-12-09 13:30:37', 'bd9fcc0e1f4bd2ceaa6f55af8be11e08982b41e0', 'jpg', 'image/jpeg', 'site', 'news-list', '0,0,270,150', 'oferta.jpg', 'data/attachments/module/site/block/news-list/2931/bd9fcc0e1f4bd2ceaa6f55af8be11e08982b41e0.jpg'),
(815, NULL, 1, 47877, 2924, 1, '2015-12-09 13:34:58', '2015-12-09 13:34:58', '95128e9d3184ca4c212868683d6525379f7d4390', 'png', 'image/png', 'site', 'event-list', '', 'home-bg-phone-2.png', 'data/attachments/module/site/block/event-list/2924/95128e9d3184ca4c212868683d6525379f7d4390.png'),
(816, 815, 1, 32000, 2924, NULL, '2015-12-09 13:34:58', '2015-12-09 13:34:58', '4e1c94e2ff90764adac42242590d0908eaf1c057', 'png', 'image/png', 'site', 'event-list', '0,0,270,150', 'home-bg-phone-2.png', 'data/attachments/module/site/block/event-list/2924/4e1c94e2ff90764adac42242590d0908eaf1c057.png'),
(817, NULL, 1, 11537, 2924, 1, '2015-12-09 14:01:40', '2015-12-09 14:01:40', '8b55e984798c82d05ad8bfb9fcc253e7b53ca774', 'png', 'image/png', 'site', 'event-list', '', 'kontakt-bg-old.png', 'data/attachments/module/site/block/event-list/2924/8b55e984798c82d05ad8bfb9fcc253e7b53ca774.png'),
(818, 817, 1, 11507, 2924, NULL, '2015-12-09 14:01:40', '2015-12-09 14:01:40', 'ce46a81df381123add707260497ced69007a9b8a', 'png', 'image/png', 'site', 'event-list', '0,0,270,150', 'kontakt-bg-old.png', 'data/attachments/module/site/block/event-list/2924/ce46a81df381123add707260497ced69007a9b8a.png'),
(819, NULL, 1, 980635, 2927, 1, '2015-12-09 14:10:56', '2015-12-09 14:10:56', 'a37c9cd1054dae5dacdf03919fae0242cf82468e', 'png', 'image/png', 'site', 'big-banner', '', 'foto3.png', 'data/attachments/module/site/block/big-banner/2927/a37c9cd1054dae5dacdf03919fae0242cf82468e.png'),
(820, 819, 1, 977457, 2927, NULL, '2015-12-09 14:11:01', '2015-12-09 14:10:56', '9441f5c2ef208064ff0114b99d239fb4f1cbcff6', 'png', 'image/png', 'site', 'big-banner', '0,0,2000,430', 'foto3.png', 'data/attachments/module/site/block/big-banner/2927/9441f5c2ef208064ff0114b99d239fb4f1cbcff6.png'),
(821, NULL, 1, 965040, 2927, 1, '2015-12-09 14:12:05', '2015-12-09 14:12:05', 'a74760f16aee7d74061ea4e9dc9991ea072f2f5c', 'png', 'image/png', 'site', 'big-banner', '', 'foto3.png', 'data/attachments/module/site/block/big-banner/2927/a74760f16aee7d74061ea4e9dc9991ea072f2f5c.png'),
(822, 821, 1, 965040, 2927, NULL, '2015-12-09 14:12:09', '2015-12-09 14:12:05', '0620713302a3286253a54a98618873c8a5b80624', 'png', 'image/png', 'site', 'big-banner', '0,0,2000,430', 'foto3.png', 'data/attachments/module/site/block/big-banner/2927/0620713302a3286253a54a98618873c8a5b80624.png'),
(823, NULL, 1, 31255, NULL, 1, '2016-01-04 16:36:41', '2016-01-04 16:36:41', 'f02dd138a0deb82fc812278b16b1d4712e129ef1', 'png', 'image/png', 'site', 'image-slider', '', 'radio-kampus-logo.png', 'data/attachments/module/site/block/image-slider/2985/f02dd138a0deb82fc812278b16b1d4712e129ef1.png'),
(824, 823, 1, 29338, NULL, NULL, '2016-01-04 16:36:41', '2016-01-04 16:36:41', '57f8bedbccb3be05978df36e80834e8021b0ad0c', 'png', 'image/png', 'site', 'image-slider', '0,0,250,150', 'radio-kampus-logo.png', 'data/attachments/module/site/block/image-slider/2985/57f8bedbccb3be05978df36e80834e8021b0ad0c.png'),
(825, NULL, 1, 24106, NULL, 1, '2016-01-04 16:47:59', '2016-01-04 16:47:59', '1aa59ed6a0a38f99a8d49f658f4ea96a5c8904aa', 'gif', 'image/gif', 'site', 'image-slider', '', 'B''galla Mask.gif', 'data/attachments/module/site/block/image-slider/2986/1aa59ed6a0a38f99a8d49f658f4ea96a5c8904aa.gif'),
(826, 825, 1, 17559, NULL, NULL, '2016-01-04 16:47:59', '2016-01-04 16:47:59', '5042fc6b57568af8cfe5e9c519a2dc3bfa4e06b4', 'gif', 'image/gif', 'site', 'image-slider', '0,0,250,150', 'B''galla Mask.gif', 'data/attachments/module/site/block/image-slider/2986/5042fc6b57568af8cfe5e9c519a2dc3bfa4e06b4.gif'),
(827, NULL, 1, 11508, NULL, 1, '2016-01-04 16:48:25', '2016-01-04 16:48:25', '09fabe857124e73f5f22b45ef05082e869d66ca5', 'png', 'image/png', 'site', 'image-slider', '', 'Bluehound-.png', 'data/attachments/module/site/block/image-slider/2986/09fabe857124e73f5f22b45ef05082e869d66ca5.png'),
(828, 827, 1, 5023, NULL, NULL, '2016-01-04 16:48:25', '2016-01-04 16:48:25', '37ec8a6100e646b050c8bec20305cf825370b2ae', 'png', 'image/png', 'site', 'image-slider', '0,0,250,150', 'Bluehound-.png', 'data/attachments/module/site/block/image-slider/2986/37ec8a6100e646b050c8bec20305cf825370b2ae.png'),
(829, NULL, 1, 14291, NULL, 1, '2016-01-04 16:48:54', '2016-01-04 16:48:54', '696e7586334569ab1d5be1ccfc07b92d45ee983b', 'png', 'image/png', 'site', 'image-slider', '', 'Doggie-.png', 'data/attachments/module/site/block/image-slider/2986/696e7586334569ab1d5be1ccfc07b92d45ee983b.png'),
(830, 829, 1, 9069, NULL, NULL, '2016-01-04 16:48:54', '2016-01-04 16:48:54', '28a990b2e9f38bc9f96a4c6ca1d1d8d0eb00c2a2', 'png', 'image/png', 'site', 'image-slider', '0,0,250,150', 'Doggie-.png', 'data/attachments/module/site/block/image-slider/2986/28a990b2e9f38bc9f96a4c6ca1d1d8d0eb00c2a2.png'),
(831, NULL, 1, 11508, NULL, 1, '2016-01-04 16:49:30', '2016-01-04 16:49:30', 'b25f30834f22108469c574dd29b9887c55a07132', 'png', 'image/png', 'site', 'image-slider', '', 'Bluehound-.png', 'data/attachments/module/site/block/image-slider/2986/b25f30834f22108469c574dd29b9887c55a07132.png'),
(832, 831, 1, 5023, NULL, NULL, '2016-01-04 16:49:30', '2016-01-04 16:49:30', 'e7f049ac6a76e2a35e28bf6db5abb1200cccdddf', 'png', 'image/png', 'site', 'image-slider', '0,0,250,150', 'Bluehound-.png', 'data/attachments/module/site/block/image-slider/2986/e7f049ac6a76e2a35e28bf6db5abb1200cccdddf.png'),
(833, NULL, 1, 11657, NULL, 1, '2016-01-04 16:49:30', '2016-01-04 16:49:30', 'fd7cf0ee23d4a877a8557d71af977b605be97b23', 'png', 'image/png', 'site', 'image-slider', '', 'Pointy-.png', 'data/attachments/module/site/block/image-slider/2986/fd7cf0ee23d4a877a8557d71af977b605be97b23.png');
INSERT INTO `attachments` (`attachment_id`, `parent_attachment_id`, `is_published`, `file_size`, `page_block_id`, `last_update_author_id`, `last_update_date`, `creation_date`, `hash_for_file_name`, `extension`, `mime_type`, `module_name`, `block_name`, `image_crop_rect_coords`, `original_file_name`, `file_path`) VALUES
(834, 833, 1, 5213, NULL, NULL, '2016-01-04 16:49:30', '2016-01-04 16:49:30', '93682633d8e8bfc4558b98a2aa3f10267d2ebfbe', 'png', 'image/png', 'site', 'image-slider', '0,0,250,150', 'Pointy-.png', 'data/attachments/module/site/block/image-slider/2986/93682633d8e8bfc4558b98a2aa3f10267d2ebfbe.png'),
(835, NULL, 1, 12800, NULL, 1, '2016-01-04 16:49:43', '2016-01-04 16:49:43', '66a6fae166f00627b93614c6c97539722a00a88f', 'png', 'image/png', 'site', 'image-slider', '', 'PurpGuy-.png', 'data/attachments/module/site/block/image-slider/2986/66a6fae166f00627b93614c6c97539722a00a88f.png'),
(836, 835, 1, 7748, NULL, NULL, '2016-01-04 16:49:43', '2016-01-04 16:49:43', 'b956a854368f7826ee9cdc164f28b3d6f4d4a939', 'png', 'image/png', 'site', 'image-slider', '0,0,250,150', 'PurpGuy-.png', 'data/attachments/module/site/block/image-slider/2986/b956a854368f7826ee9cdc164f28b3d6f4d4a939.png'),
(837, NULL, 1, 32447, NULL, 1, '2016-01-04 16:50:07', '2016-01-04 16:50:07', '56ee02ac1c1d89899c2469a518871f2d4ae77186', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'Pointy.jpg', 'data/attachments/module/site/block/image-slider/2986/56ee02ac1c1d89899c2469a518871f2d4ae77186.jpg'),
(838, 837, 1, 17006, NULL, NULL, '2016-01-04 16:50:07', '2016-01-04 16:50:07', 'f9b854e2600d4d91a5cb2cd5a2d5dc45f933de55', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,250,150', 'Pointy.jpg', 'data/attachments/module/site/block/image-slider/2986/f9b854e2600d4d91a5cb2cd5a2d5dc45f933de55.jpg'),
(839, NULL, 1, 14500, 2985, 1, '2016-01-05 12:45:47', '2016-01-05 12:45:47', '954ae407476fd16ced25cffc543fcbbd62db99fb', 'png', 'image/png', 'site', 'image-slider', '', 'drew-poland.png', 'data/attachments/module/site/block/image-slider/2985/954ae407476fd16ced25cffc543fcbbd62db99fb.png'),
(840, 839, 1, 7257, 2985, NULL, '2016-01-05 12:45:47', '2016-01-05 12:45:47', '346b5b965b8ee9af1d2e2306b75adba9020957aa', 'png', 'image/png', 'site', 'image-slider', '0,0,129,80', 'drew-poland.png', 'data/attachments/module/site/block/image-slider/2985/346b5b965b8ee9af1d2e2306b75adba9020957aa.png'),
(841, NULL, 1, 14500, 2985, 1, '2016-01-05 12:45:47', '2016-01-05 12:45:47', 'e6a5196de91f56900939916f198da7a989e59e4b', 'png', 'image/png', 'site', 'image-slider', '', 'drew-poland.png', 'data/attachments/module/site/block/image-slider/2985/e6a5196de91f56900939916f198da7a989e59e4b.png'),
(842, 841, 1, 14197, 2985, NULL, '2016-01-05 13:03:38', '2016-01-05 12:45:47', '8c9deec3687df0b1ed25c14f9bd999285157ca25', 'png', 'image/png', 'site', 'image-slider', '0,6,126,132', 'drew-poland.png', 'data/attachments/module/site/block/image-slider/2985/8c9deec3687df0b1ed25c14f9bd999285157ca25.png'),
(843, NULL, 1, 23139, 2985, 1, '2016-01-05 12:46:46', '2016-01-05 12:46:46', '7f021eae58d062de3656bf581a1e89b8f2ec914c', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'glusi.jpg', 'data/attachments/module/site/block/image-slider/2985/7f021eae58d062de3656bf581a1e89b8f2ec914c.jpg'),
(844, 843, 1, 10201, 2985, NULL, '2016-01-05 12:47:28', '2016-01-05 12:46:46', '12cbf05be17c533263855508ee7de0b013303ec2', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,130,80', 'glusi.jpg', 'data/attachments/module/site/block/image-slider/2985/12cbf05be17c533263855508ee7de0b013303ec2.jpg'),
(845, NULL, 1, 12753, 2985, 1, '2016-01-05 12:55:31', '2016-01-05 12:55:31', 'c85a73f79d438cb38b67d3d4cb2e2b0d47aaf20a', 'png', 'image/png', 'site', 'image-slider', '', 'mtv24.png', 'data/attachments/module/site/block/image-slider/2985/c85a73f79d438cb38b67d3d4cb2e2b0d47aaf20a.png'),
(846, 845, 1, 11990, 2985, NULL, '2016-01-05 12:55:31', '2016-01-05 12:55:31', '1d477e7737725f02aec86277fa52c85e7b05690c', 'png', 'image/png', 'site', 'image-slider', '0,0,140,70', 'mtv24.png', 'data/attachments/module/site/block/image-slider/2985/1d477e7737725f02aec86277fa52c85e7b05690c.png'),
(847, NULL, 1, 15642, 2985, 1, '2016-01-05 13:01:14', '2016-01-05 13:01:14', '55df6608de0a7a5a26b5626584c14efe5e5aa93b', 'png', 'image/png', 'site', 'image-slider', '', 'drew-poland.png', 'data/attachments/module/site/block/image-slider/2985/55df6608de0a7a5a26b5626584c14efe5e5aa93b.png'),
(848, 847, 1, 3919, 2985, NULL, '2016-01-05 13:01:14', '2016-01-05 13:01:14', '1d65f30a37d40f02e28c246342a6d49d95967698', 'png', 'image/png', 'site', 'image-slider', '0,0,134,46', 'drew-poland.png', 'data/attachments/module/site/block/image-slider/2985/1d65f30a37d40f02e28c246342a6d49d95967698.png'),
(849, NULL, 1, 15642, 2985, 1, '2016-01-05 13:02:10', '2016-01-05 13:02:10', 'd877e989f32cc73058cd96dc79c76417b909eb01', 'png', 'image/png', 'site', 'image-slider', '', 'drew-poland.png', 'data/attachments/module/site/block/image-slider/2985/d877e989f32cc73058cd96dc79c76417b909eb01.png'),
(850, 849, 1, 15635, 2985, NULL, '2016-01-05 13:02:10', '2016-01-05 13:02:10', 'e3fd696c36857d88f15583f7cba27be5257021ee', 'png', 'image/png', 'site', 'image-slider', '0,0,134,135', 'drew-poland.png', 'data/attachments/module/site/block/image-slider/2985/e3fd696c36857d88f15583f7cba27be5257021ee.png'),
(851, NULL, 1, 9980, 2985, 1, '2016-01-05 13:02:10', '2016-01-05 13:02:10', 'c53971195d12d5653a18a8f7ed4006049c466527', 'png', 'image/png', 'site', 'image-slider', '', 'drew-poland.png', 'data/attachments/module/site/block/image-slider/2985/c53971195d12d5653a18a8f7ed4006049c466527.png'),
(852, 851, 1, 9980, 2985, NULL, '2016-01-05 13:02:10', '2016-01-05 13:02:10', 'd432fcd27d79d9ce712ae7893daa4090e5ff870a', 'png', 'image/png', 'site', 'image-slider', '0,0,131,134', 'drew-poland.png', 'data/attachments/module/site/block/image-slider/2985/d432fcd27d79d9ce712ae7893daa4090e5ff870a.png'),
(853, NULL, 1, 9980, 2985, 1, '2016-01-05 13:02:43', '2016-01-05 13:02:43', '498b9ed9845820c858f6e1608654b645d8c68ebb', 'png', 'image/png', 'site', 'image-slider', '', 'drew-poland.png', 'data/attachments/module/site/block/image-slider/2985/498b9ed9845820c858f6e1608654b645d8c68ebb.png'),
(854, 853, 1, 2648, 2985, NULL, '2016-01-05 13:02:43', '2016-01-05 13:02:43', '4c561268b9a082febd47f4d25ed83b2d3009b15b', 'png', 'image/png', 'site', 'image-slider', '0,0,131,46', 'drew-poland.png', 'data/attachments/module/site/block/image-slider/2985/4c561268b9a082febd47f4d25ed83b2d3009b15b.png'),
(855, NULL, 1, 64310, 2985, 1, '2016-01-05 13:03:51', '2016-01-05 13:03:51', '0abc539cfbd3e18dca2c910c14e5e908d55d8ba3', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'glusi.jpg', 'data/attachments/module/site/block/image-slider/2985/0abc539cfbd3e18dca2c910c14e5e908d55d8ba3.jpg'),
(856, 855, 1, 19565, 2985, NULL, '2016-01-05 13:03:51', '2016-01-05 13:03:51', 'e90f7537f2a388b0f5460ae4e6813b6990dcf304', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,135,135', 'glusi.jpg', 'data/attachments/module/site/block/image-slider/2985/e90f7537f2a388b0f5460ae4e6813b6990dcf304.jpg'),
(857, NULL, 1, 7203, 2985, 1, '2016-01-05 13:22:09', '2016-01-05 13:22:09', '9aa0dc4201863ecf7fb309faf83cfe8d80f3ed41', 'png', 'image/png', 'site', 'image-slider', '', 'drew-poland.png', 'data/attachments/module/site/block/image-slider/2985/9aa0dc4201863ecf7fb309faf83cfe8d80f3ed41.png'),
(858, 857, 1, 7200, 2985, NULL, '2016-01-05 13:22:23', '2016-01-05 13:22:09', '90ce248665431156808434cda88b40c77251b9ff', 'png', 'image/png', 'site', 'image-slider', '0,0,300,84.99999999999957', 'drew-poland.png', 'data/attachments/module/site/block/image-slider/2985/90ce248665431156808434cda88b40c77251b9ff.png'),
(859, NULL, 1, 18144, 2985, 1, '2016-01-05 13:22:35', '2016-01-05 13:22:35', 'bc22bdfb085339511787a84cd81df124294e7c13', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'glusi.jpg', 'data/attachments/module/site/block/image-slider/2985/bc22bdfb085339511787a84cd81df124294e7c13.jpg'),
(860, 859, 1, 18427, 2985, NULL, '2016-01-05 13:22:35', '2016-01-05 13:22:35', '6fd2c5af1126018666a6103f63d80cc6423d5e1f', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,300,85', 'glusi.jpg', 'data/attachments/module/site/block/image-slider/2985/6fd2c5af1126018666a6103f63d80cc6423d5e1f.jpg'),
(861, NULL, 1, 10080, 2985, 1, '2016-01-05 13:23:51', '2016-01-05 13:23:51', 'df5bb257cc5fb68c8315707c4ad038590a87ded7', 'png', 'image/png', 'site', 'image-slider', '', 'mpl-parking.png', 'data/attachments/module/site/block/image-slider/2985/df5bb257cc5fb68c8315707c4ad038590a87ded7.png'),
(862, 861, 1, 10080, 2985, NULL, '2016-01-05 13:23:51', '2016-01-05 13:23:51', '41b0d27a8e15efabd949e0ea43a67c294bfec1a7', 'png', 'image/png', 'site', 'image-slider', '0,0,300,85', 'mpl-parking.png', 'data/attachments/module/site/block/image-slider/2985/41b0d27a8e15efabd949e0ea43a67c294bfec1a7.png'),
(863, NULL, 1, 16153, 2985, 1, '2016-01-05 13:24:43', '2016-01-05 13:24:43', 'db16bc8da7bd560bb44d6ab3dc77e05c17afed72', 'png', 'image/png', 'site', 'image-slider', '', 'mpl-taxi.png', 'data/attachments/module/site/block/image-slider/2985/db16bc8da7bd560bb44d6ab3dc77e05c17afed72.png'),
(864, 863, 1, 16153, 2985, NULL, '2016-01-05 13:24:43', '2016-01-05 13:24:43', '645a13cb49b4d7e4d5bc826a7df3cd832fac5733', 'png', 'image/png', 'site', 'image-slider', '0,0,300,85', 'mpl-taxi.png', 'data/attachments/module/site/block/image-slider/2985/645a13cb49b4d7e4d5bc826a7df3cd832fac5733.png'),
(865, NULL, 1, 9585, 2985, 1, '2016-01-05 13:25:17', '2016-01-05 13:25:17', 'e0437b8b6b0512665b7eb7a84ca6b57a8dd2295c', 'png', 'image/png', 'site', 'image-slider', '', 'mpl.png', 'data/attachments/module/site/block/image-slider/2985/e0437b8b6b0512665b7eb7a84ca6b57a8dd2295c.png'),
(866, 865, 1, 9585, 2985, NULL, '2016-01-05 13:25:17', '2016-01-05 13:25:17', '315796413736c7491a24eb6c135130bf482cce7a', 'png', 'image/png', 'site', 'image-slider', '0,0,300,85', 'mpl.png', 'data/attachments/module/site/block/image-slider/2985/315796413736c7491a24eb6c135130bf482cce7a.png'),
(867, NULL, 1, 34128, 2985, 1, '2016-01-05 13:26:04', '2016-01-05 13:26:04', 'db501ce74855db95e1fc42130270d76f7e933741', 'png', 'image/png', 'site', 'image-slider', '', 'mtv24.png', 'data/attachments/module/site/block/image-slider/2985/db501ce74855db95e1fc42130270d76f7e933741.png'),
(868, 867, 1, 23154, 2985, NULL, '2016-01-05 13:26:05', '2016-01-05 13:26:04', '068df85ea01b522a9888d958729957a0153f6673', 'png', 'image/png', 'site', 'image-slider', '0,0,300,85', 'mtv24.png', 'data/attachments/module/site/block/image-slider/2985/068df85ea01b522a9888d958729957a0153f6673.png'),
(869, NULL, 1, 34128, 2985, 1, '2016-01-05 13:26:05', '2016-01-05 13:26:05', '37b61aee298ee6b08cd1d03d710c0411b10d12b7', 'png', 'image/png', 'site', 'image-slider', '', 'mtv24.png', 'data/attachments/module/site/block/image-slider/2985/37b61aee298ee6b08cd1d03d710c0411b10d12b7.png'),
(870, 869, 1, 25713, 2985, NULL, '2016-01-05 13:26:19', '2016-01-05 13:26:05', '00f5f75bc63b8ed68cf60dad1dc91851d7ef4155', 'png', 'image/png', 'site', 'image-slider', '0,24,300,108.99999999999957', 'mtv24.png', 'data/attachments/module/site/block/image-slider/2985/00f5f75bc63b8ed68cf60dad1dc91851d7ef4155.png'),
(871, NULL, 1, 26412, 2985, 1, '2016-01-05 13:26:53', '2016-01-05 13:26:53', '0c843da506228798fd6f03d54ef59b74dc777ad0', 'png', 'image/png', 'site', 'image-slider', '', 'secover.png', 'data/attachments/module/site/block/image-slider/2985/0c843da506228798fd6f03d54ef59b74dc777ad0.png'),
(872, 871, 1, 26412, 2985, NULL, '2016-01-05 13:26:53', '2016-01-05 13:26:53', 'b45bdca6403bbb41a7c5d38dfd03d1159203c893', 'png', 'image/png', 'site', 'image-slider', '0,0,300,85', 'secover.png', 'data/attachments/module/site/block/image-slider/2985/b45bdca6403bbb41a7c5d38dfd03d1159203c893.png'),
(873, NULL, 1, 8074, 2985, 1, '2016-01-05 13:27:23', '2016-01-05 13:27:23', 'eec19ff745940c544a7a18dadd87dc79e53c6e9e', 'png', 'image/png', 'site', 'image-slider', '', 'sgtir.png', 'data/attachments/module/site/block/image-slider/2985/eec19ff745940c544a7a18dadd87dc79e53c6e9e.png'),
(874, 873, 1, 8074, 2985, NULL, '2016-01-05 13:27:23', '2016-01-05 13:27:23', '48a7b38c83c99ead0c7243cceb7d2e305e9d9e2c', 'png', 'image/png', 'site', 'image-slider', '0,0,300,85', 'sgtir.png', 'data/attachments/module/site/block/image-slider/2985/48a7b38c83c99ead0c7243cceb7d2e305e9d9e2c.png'),
(875, NULL, 1, 11828, 2985, 1, '2016-01-05 13:27:59', '2016-01-05 13:27:59', '91f9bf1bc64fb7525a795291adf2538dfbb284a7', 'png', 'image/png', 'site', 'image-slider', '', 'skbank.png', 'data/attachments/module/site/block/image-slider/2985/91f9bf1bc64fb7525a795291adf2538dfbb284a7.png'),
(876, 875, 1, 11828, 2985, NULL, '2016-01-05 13:27:59', '2016-01-05 13:27:59', '1c9fc43d40bc8db0cda74f177d90c15959f19e96', 'png', 'image/png', 'site', 'image-slider', '0,0,300,85', 'skbank.png', 'data/attachments/module/site/block/image-slider/2985/1c9fc43d40bc8db0cda74f177d90c15959f19e96.png'),
(877, NULL, 1, 9779, 2985, 1, '2016-01-05 13:28:32', '2016-01-05 13:28:32', 'c634a806355d2b96323d4a4d08767abb061741c4', 'png', 'image/png', 'site', 'image-slider', '', 'tgde.png', 'data/attachments/module/site/block/image-slider/2985/c634a806355d2b96323d4a4d08767abb061741c4.png'),
(878, 877, 1, 9779, 2985, NULL, '2016-01-05 13:28:32', '2016-01-05 13:28:32', 'c163deb22b7f08917e2015b78aa1e47849ed523d', 'png', 'image/png', 'site', 'image-slider', '0,0,300,85', 'tgde.png', 'data/attachments/module/site/block/image-slider/2985/c163deb22b7f08917e2015b78aa1e47849ed523d.png'),
(879, NULL, 1, 10356, 2985, 1, '2016-01-05 13:29:03', '2016-01-05 13:29:03', 'd3ca2b45615dd729266304d1fc8a10f0a7d238f8', 'png', 'image/png', 'site', 'image-slider', '', 'vistula.png', 'data/attachments/module/site/block/image-slider/2985/d3ca2b45615dd729266304d1fc8a10f0a7d238f8.png'),
(880, 879, 1, 10356, 2985, NULL, '2016-01-05 13:29:03', '2016-01-05 13:29:03', 'eda55bb1602c92d1a31a4f326031294652c5a69b', 'png', 'image/png', 'site', 'image-slider', '0,0,300,85', 'vistula.png', 'data/attachments/module/site/block/image-slider/2985/eda55bb1602c92d1a31a4f326031294652c5a69b.png'),
(881, NULL, 1, 32502, 2985, 1, '2016-01-05 13:29:45', '2016-01-05 13:29:45', '1a798ebe786d9ff7192fd68b631f4a5d74464bb8', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'zbikowski.jpg', 'data/attachments/module/site/block/image-slider/2985/1a798ebe786d9ff7192fd68b631f4a5d74464bb8.jpg'),
(882, 881, 1, 27409, 2985, NULL, '2016-01-05 13:29:45', '2016-01-05 13:29:45', '1c9a709c2567ff154a61ce8bc164fca4cee73dee', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,300,85', 'zbikowski.jpg', 'data/attachments/module/site/block/image-slider/2985/1c9a709c2567ff154a61ce8bc164fca4cee73dee.jpg'),
(883, NULL, 1, 32502, 2985, 1, '2016-01-05 13:29:45', '2016-01-05 13:29:45', '0cd4b54fd84796e17f72e7f18790e7199092ff5b', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'zbikowski.jpg', 'data/attachments/module/site/block/image-slider/2985/0cd4b54fd84796e17f72e7f18790e7199092ff5b.jpg'),
(884, 883, 1, 27409, 2985, NULL, '2016-01-05 13:29:45', '2016-01-05 13:29:45', '335e3836a96c590a89602a9f5a29838c8004f957', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,300,85', 'zbikowski.jpg', 'data/attachments/module/site/block/image-slider/2985/335e3836a96c590a89602a9f5a29838c8004f957.jpg'),
(885, NULL, 1, 15975, 2985, 1, '2016-01-05 13:31:31', '2016-01-05 13:31:31', '7eace419d1e7f9fed0a900342feb6a8d588274d1', 'png', 'image/png', 'site', 'image-slider', '', 'mtv24.png', 'data/attachments/module/site/block/image-slider/2985/7eace419d1e7f9fed0a900342feb6a8d588274d1.png'),
(886, 885, 1, 15975, 2985, NULL, '2016-01-05 13:31:31', '2016-01-05 13:31:31', '86afa8e6b73a7893df7d08ebe3b72174e01486c7', 'png', 'image/png', 'site', 'image-slider', '0,0,300,85', 'mtv24.png', 'data/attachments/module/site/block/image-slider/2985/86afa8e6b73a7893df7d08ebe3b72174e01486c7.png'),
(887, NULL, 1, 22690, 2985, 1, '2016-01-05 13:31:45', '2016-01-05 13:31:45', '0f333d778a95eba0fa2cb130e2164cac57dce4af', 'jpg', 'image/jpeg', 'site', 'image-slider', '', 'zbikowski.jpg', 'data/attachments/module/site/block/image-slider/2985/0f333d778a95eba0fa2cb130e2164cac57dce4af.jpg'),
(888, 887, 1, 22838, 2985, NULL, '2016-01-05 13:31:45', '2016-01-05 13:31:45', 'fc4e293aedd22840643cd4e26a452bd0d1c6a3b1', 'jpg', 'image/jpeg', 'site', 'image-slider', '0,0,300,85', 'zbikowski.jpg', 'data/attachments/module/site/block/image-slider/2985/fc4e293aedd22840643cd4e26a452bd0d1c6a3b1.jpg'),
(889, NULL, 1, 5682, 2985, 1, '2016-01-05 13:36:12', '2016-01-05 13:36:12', '705a16e5445f239265cee1994de8b860657c24e7', 'png', 'image/png', 'site', 'image-slider', '', 'mpl-parking.png', 'data/attachments/module/site/block/image-slider/2985/705a16e5445f239265cee1994de8b860657c24e7.png'),
(890, 889, 1, 5682, 2985, NULL, '2016-01-05 13:36:12', '2016-01-05 13:36:12', '99faa448cd2e80437325d38c59b9ee7696e9abd8', 'png', 'image/png', 'site', 'image-slider', '0,0,176,50', 'mpl-parking.png', 'data/attachments/module/site/block/image-slider/2985/99faa448cd2e80437325d38c59b9ee7696e9abd8.png'),
(891, NULL, 1, 83505, 2987, 1, '2016-01-05 13:49:01', '2016-01-05 13:49:01', 'f2f177bc6c37f8b2580f71fcc44e3d9381c860e9', 'png', 'image/png', 'site', 'employee', '', '284509_278151145623463_1466982556_n.png', 'data/attachments/module/site/block/employee/2987/f2f177bc6c37f8b2580f71fcc44e3d9381c860e9.png'),
(892, 891, 1, 62181, 2987, NULL, '2016-01-05 13:49:01', '2016-01-05 13:49:01', '6f4de39317dbc9163a8a7ba19f1fab65cb7c9299', 'png', 'image/png', 'site', 'employee', '0,0,185,185', '284509_278151145623463_1466982556_n.png', 'data/attachments/module/site/block/employee/2987/6f4de39317dbc9163a8a7ba19f1fab65cb7c9299.png'),
(893, NULL, 1, 34795, NULL, 1, '2016-01-07 12:33:33', '2016-01-07 12:33:33', '9b14b8ac7e4d8ff2fbe7480a45be40fbc14b3a3b', 'gif', 'image/gif', 'site', 'navigation-menu', '', 'Baule Mask.gif', 'data/attachments/module/site/block/navigation-menu/2993/9b14b8ac7e4d8ff2fbe7480a45be40fbc14b3a3b.gif'),
(894, 893, 1, 36896, NULL, NULL, '2016-01-07 12:33:33', '2016-01-07 12:33:33', 'bdf5e2ddef1fde61a7d0afa951dba15ad31111cd', 'gif', 'image/gif', 'site', 'navigation-menu', '0,0,300,293', 'Baule Mask.gif', 'data/attachments/module/site/block/navigation-menu/2993/bdf5e2ddef1fde61a7d0afa951dba15ad31111cd.gif'),
(895, NULL, 1, 12608, 2987, 1, '2016-01-08 10:48:54', '2016-01-08 10:48:54', '8ee76e98dd5bae1ad33b0c49d4d2b9c328ba90ba', 'jpg', 'image/jpeg', 'site', 'employee', '', 'DSCN14320001_1.JPG', 'data/attachments/module/site/block/employee/2987/8ee76e98dd5bae1ad33b0c49d4d2b9c328ba90ba.jpg'),
(896, 895, 1, 9353, 2987, NULL, '2016-01-08 10:48:54', '2016-01-08 10:48:54', 'e5da22d5e766ac7748d3d8b587118701cddd509b', 'jpg', 'image/jpeg', 'site', 'employee', '0,0,185,185', 'DSCN14320001_1.JPG', 'data/attachments/module/site/block/employee/2987/e5da22d5e766ac7748d3d8b587118701cddd509b.jpg');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `blocks`
--

CREATE TABLE `blocks` (
  `block_id` smallint(5) UNSIGNED NOT NULL,
  `shared_content_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Used only when block is dynamic.',
  `is_system` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_enabled` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `is_dynamic` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Dynamic blocks are shared across multiple pages.',
  `has_data_in_search_index` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `order` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `language_category_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Info about how to treat this block - as english? polish? russian? etc.',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `glyphicon` varchar(32) COLLATE utf8_polish_ci DEFAULT '',
  `php_class_name` varchar(32) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `config` text COLLATE utf8_polish_ci NOT NULL COMMENT 'Config in XML format.'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `blocks`
--

INSERT INTO `blocks` (`block_id`, `shared_content_id`, `is_system`, `is_enabled`, `is_dynamic`, `has_data_in_search_index`, `order`, `language_category_id`, `last_update_author_id`, `last_update_date`, `glyphicon`, `php_class_name`, `config`) VALUES
(1, NULL, 1, 1, 0, 1, 1, NULL, NULL, '2014-02-18 20:41:13', 'fa fa-text-height', 'Text', '<?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize></xml>'),
(3, NULL, 1, 1, 0, 1, 3, NULL, NULL, '2014-02-18 20:41:13', 'fa fa-th-list', 'NewsList', '<?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize><itemCountPerPage><![CDATA[5]]></itemCountPerPage></xml>'),
(4, NULL, 1, 1, 0, 0, 4, NULL, NULL, '2014-02-18 20:41:13', 'fa fa-list-alt', 'FutureEventList', '<?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize><itemCountPerPage><![CDATA[5]]></itemCountPerPage></xml>'),
(5, NULL, 1, 1, 0, 0, 5, NULL, NULL, '2014-02-18 20:41:13', 'fa fa-bars', 'NavigationMenu', '<?xml version="1.0"?><xml><orientation><![CDATA[vertical]]></orientation><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize></xml>'),
(6, NULL, 1, 1, 0, 0, 6, NULL, NULL, '2014-02-18 20:41:13', 'fa fa-users', 'UserDataEdit', '<?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize></xml>'),
(7, NULL, 1, 1, 0, 0, 7, NULL, NULL, '2014-02-18 20:41:13', 'fa fa-search', 'Search', '<?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize></xml>'),
(8, NULL, 1, 1, 0, 0, 8, NULL, NULL, '2014-02-18 20:41:13', 'fa fa-list', 'SearchResults', '<?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize></xml>'),
(9, NULL, 1, 1, 0, 0, 9, NULL, NULL, '2014-02-18 20:41:13', 'fa fa-credit-card', 'Header', '<?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize><tagType><![CDATA[predefined]]></tagType><predefinedTag><![CDATA[h3]]></predefinedTag><customTag><![CDATA[]]></customTag></xml>'),
(10, NULL, 1, 1, 0, 1, 10, NULL, NULL, '2014-02-18 20:41:13', 'fa fa-bookmark-o', 'TabsControl', '<?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize></xml>'),
(11, NULL, 1, 1, 0, 0, 11, NULL, NULL, '2014-02-18 20:41:13', 'fa fa-anchor', 'MainMenu', '<?xml version="1.0"?><xml><orientation><![CDATA[horizontal]]></orientation><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize></xml>'),
(12, NULL, 1, 1, 0, 0, 11, NULL, NULL, '2014-02-18 20:41:13', 'fa fa-archive', 'NewsDetails', '<?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize></xml>'),
(13, NULL, 1, 1, 0, 0, 11, NULL, NULL, '2014-02-18 20:41:13', 'fa fa-share', 'ShortcutMenu', '<?xml version="1.0"?><xml><orientation><![CDATA[horizontal]]></orientation><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize></xml>'),
(14, NULL, 1, 1, 0, 0, 11, NULL, NULL, '2014-02-18 20:41:13', 'fa fa-pagelines', 'BigBanner', '<?xml version="1.0"?><xml><orientation><![CDATA[horizontal]]></orientation><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize></xml>'),
(20, NULL, 1, 1, 0, 0, 12, NULL, NULL, '2014-02-20 12:59:37', 'fa fa-pagelines', 'NewsSlider', '<?xml version="1.0"?><xml><titleSize><![CDATA[2]]></titleSize><titleVisible><![CDATA[1]]></titleVisible><slideIntervalInMs><![CDATA[5000]]></slideIntervalInMs><animationsTimeInMs><![CDATA[1500]]></animationsTimeInMs><wrappingType><![CDATA[last]]></wrappingType></xml>'),
(30, NULL, 1, 1, 0, 0, 13, NULL, NULL, '2014-02-23 20:28:53', 'fa fa-pagelines', 'SmallBanner', ' <?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize></xml>'),
(31, NULL, 1, 1, 0, 0, 0, NULL, NULL, '2014-03-05 14:16:08', 'fa fa-pagelines', 'NewsAdd', '<?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize></xml>'),
(32, NULL, 1, 1, 0, 1, 0, NULL, NULL, '2014-03-05 14:16:08', 'fa fa-list-ul', 'EventList', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[1]]></titleVisible><itemCountPerPage><![CDATA[5]]></itemCountPerPage><defaultSubjectForMailAboutEventApproval><![CDATA[]]></defaultSubjectForMailAboutEventApproval><defaultContentsForMailAboutEventApproval><![CDATA[]]></defaultContentsForMailAboutEventApproval><subjectForMailForUserAboutUserSignedUpForEvent><![CDATA[Dziękujemy za zapisanie się na wydarzenie]]></subjectForMailForUserAboutUserSignedUpForEvent><contentsForMailForUserAboutUserSignedUpForEvent><![CDATA[<p><strong>Zapisałeś się na wydarzenie:</strong><br />\n&quot;{{event_title}}&quot; - {{event_link}}</p>\n\n<p>Start: {{event_start_date}} &nbsp;o godzinie {{event_start_hour}}</p>\n\n<p>Gdzie: {{event_place}}</p>\n\n<p><strong>Dane zgłaszającego:</strong></p>\n\n<p>Tytuł naukowy: {{user_academic_title}}</p>\n\n<p>Imie Nawisko:&nbsp;{{user_name_and_surname}}</p>\n\n<p>Instytucja:&nbsp;{{user_institution_name}}</p>\n\n<p>Dane do fakt.:&nbsp;{{user_invoice_data}}</p>\n\n<p>Telefon:&nbsp;{{user_phone}}</p>\n\n<p>Email:&nbsp;{{user_email}}</p>\n\n<p>Uwagi:&nbsp;{{user_attentions}}</p>\n\n<hr />\n<p>W razie pytań lub niejasności prosimy o kontakt maliowy pod adres {{contact_email}} lub {{contact_phone}}</p>]]></contentsForMailForUserAboutUserSignedUpForEvent><subjectForMailForAdminAboutUserSignedUpForEvent><![CDATA[Nowy użytkownik zapisał się na wydarzenie]]></subjectForMailForAdminAboutUserSignedUpForEvent><contentsForMailForAdminAboutUserSignedUpForEvent><![CDATA[<p><strong>Nowy zapis na wydarzenie:</strong><br />\n&quot;{{event_title}}&quot; - {{event_link}}</p>\n\n<p>Start: {{event_start_date}} &nbsp;o godzinie {{event_start_hour}}</p>\n\n<p>Gdzie: {{event_place}}</p>\n\n<p><strong>Dane zgłaszającego:</strong></p>\n\n<p>Tytuł naukowy: {{user_academic_title}}</p>\n\n<p>Imie Nawisko:&nbsp;{{user_name_and_surname}}</p>\n\n<p>Instytucja:&nbsp;{{user_institution_name}}</p>\n\n<p>Dane do fakt.:&nbsp;{{user_invoice_data}}</p>\n\n<p>Telefon:&nbsp;{{user_phone}}</p>\n\n<p>Email:&nbsp;{{user_email}}</p>\n\n<p>Uwagi:&nbsp;{{user_attentions}}</p>\n\n<hr />\n<p>Przejdź do listy zgłoszeń: {{event_list_link}}</p>]]></contentsForMailForAdminAboutUserSignedUpForEvent></xml>'),
(33, NULL, 1, 1, 0, 0, 0, NULL, NULL, '2014-03-05 14:16:08', 'fa fa-outdent', 'OldEventList', '<?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize><itemCountPerPage><![CDATA[5]]></itemCountPerPage></xml>'),
(34, NULL, 1, 1, 0, 0, 0, NULL, NULL, '2014-03-05 14:16:08', 'fa fa-pagelines', 'EventDetails', '<?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize></xml>'),
(60, NULL, 1, 1, 0, 1, 0, NULL, NULL, '2014-03-24 13:25:19', 'fa fa-book', 'PostgraduateStudies', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize></xml>\n'),
(61, NULL, 1, 1, 0, 1, 0, NULL, NULL, '2014-03-24 15:43:43', 'fa fa-book', 'BachelorStudies', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize></xml>\n'),
(65, NULL, 1, 1, 0, 1, 0, NULL, NULL, '2014-03-27 12:34:02', 'fa fa-pagelines', 'Accordion', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize></xml>'),
(66, NULL, 1, 1, 0, 0, 0, NULL, NULL, '2014-03-27 12:34:02', 'fa fa-pagelines', 'ImageSlider', '<?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize><imageWidth><![CDATA[250]]></imageWidth><imageHeight><![CDATA[150]]></imageHeight><slideIntervalInMs><![CDATA[5000]]></slideIntervalInMs><animationsTimeInMs><![CDATA[1500]]></animationsTimeInMs><wrappingType><![CDATA[last]]></wrappingType></xml>'),
(70, NULL, 1, 1, 0, 0, 0, NULL, NULL, '2014-04-09 09:55:54', 'fa fa-pagelines', 'ImagesPile', '<?xml version="1.0"?><xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[1]]></titleVisible><backgroundColor><![CDATA[#fff]]></backgroundColor><gutter><![CDATA[40]]></gutter><pileAngles><![CDATA[2]]></pileAngles><delay><![CDATA[0]]></delay><pileAnimationOpenSpeed><![CDATA[400]]></pileAnimationOpenSpeed><pileAnimationOpenEasing><![CDATA[ease-in-out]]></pileAnimationOpenEasing><pileAnimationCloseSpeed><![CDATA[400]]></pileAnimationCloseSpeed><pileAnimationCloseEasing><![CDATA[ease-in-out]]></pileAnimationCloseEasing><otherPileAnimationOpenSpeed><![CDATA[400]]></otherPileAnimationOpenSpeed><otherPileAnimationOpenEasing><![CDATA[ease-in-out]]></otherPileAnimationOpenEasing><otherPileAnimationCloseEasing><![CDATA[ease-in-out]]></otherPileAnimationCloseEasing></xml>'),
(71, NULL, 1, 1, 0, 1, 0, NULL, NULL, '2014-04-09 09:55:54', 'fa fa-pagelines', 'Employees', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize></xml>'),
(79, NULL, 1, 1, 0, 0, 0, NULL, NULL, '2014-04-12 13:14:15', 'fa fa-pagelines', 'EmployeeDetails', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize></xml>'),
(92, NULL, 1, 1, 0, 0, 0, NULL, NULL, '2015-12-08 07:35:08', '', 'ImageGallery', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[2]]></titleSize><titleVisible><![CDATA[1]]></titleVisible></xml>'),
(93, NULL, 1, 1, 0, 0, 0, NULL, NULL, '2015-12-08 07:35:08', '', 'Popup', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[2]]></titleSize><titleVisible><![CDATA[0]]></titleVisible></xml>'),
(95, 763, 0, 1, 1, 0, 0, 2, 1, '2015-12-08 11:24:18', '', 'MainMenu', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[4]]></titleSize><titleVisible><![CDATA[0]]></titleVisible><categoryId><![CDATA[114]]></categoryId><orientation><![CDATA[horizontal]]></orientation></xml>'),
(96, 773, 0, 1, 1, 0, 0, 2, 1, '2015-12-09 12:03:29', '', 'NavigationMenu', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[1]]></titleVisible><categoryId><![CDATA[115]]></categoryId><orientation><![CDATA[vertical]]></orientation><displayUncollapseButtons><![CDATA[1]]></displayUncollapseButtons></xml>'),
(97, 766, 0, 1, 1, 0, 0, 2, 1, '2015-12-09 12:54:05', '', 'BigBanner', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[0]]></titleVisible><animationTime><![CDATA[3000]]></animationTime><timeBetweenSlides><![CDATA[7000]]></timeBetweenSlides><animationType><![CDATA[fadeout]]></animationType></xml>');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `blocks_properties`
--

CREATE TABLE `blocks_properties` (
  `properties_id` mediumint(8) UNSIGNED NOT NULL,
  `block_id` smallint(5) UNSIGNED NOT NULL,
  `language_id` smallint(5) UNSIGNED DEFAULT NULL,
  `name` varchar(64) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `default_content` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `blocks_properties`
--

INSERT INTO `blocks_properties` (`properties_id`, `block_id`, `language_id`, `name`, `default_content`) VALUES
(1, 1, 1, 'Text block', 'Example text. Please edit this.'),
(2, 1, 2, 'Blok tekstowy', 'Przykładowy tekst. Proszę go zmienić.'),
(3, 1, 3, 'Текстовый блок', 'Текст примера. Пожалуйста, измените его.'),
(7, 3, 1, 'News list', 'There are no news yet.'),
(8, 3, 2, 'Lista aktualności', 'Nie ma jeszcze żadnych aktualności.'),
(9, 3, 3, 'список новостей', 'Там в настоящее время нет новостей.'),
(10, 4, 1, 'Future events', 'There are no events yet.'),
(11, 4, 2, 'Nadchodzące wydarzenia', 'Nie ma jeszcze wydarzeń.'),
(12, 4, 3, 'Будущие события', 'Там в настоящее время никаких событий.'),
(13, 5, 1, 'Navigation menu', 'There are no attached pages to this menu yet.'),
(14, 5, 2, 'Menu nawigacyjne', 'Nie podpięto jeszcze żadnych stron do tego menu.'),
(15, 5, 3, 'меню навигации', 'Там нет страницы, связанные с этим меню.'),
(16, 6, 1, 'User data edit', ''),
(17, 6, 2, 'Edycja danych użytkownika', ''),
(18, 6, 3, 'Редактировать данные пользователя', ''),
(19, 7, 1, 'Search', ''),
(20, 7, 2, 'Wyszukiwarka', ''),
(21, 7, 3, 'поиск', ''),
(22, 8, 1, 'Search results', 'Nothing was found.'),
(23, 8, 2, 'Wyniki wyszukiwania', 'Nic nie znaleziono.'),
(24, 8, 3, 'Результаты поиска', 'Nic nie znaleziono.'),
(25, 9, 1, 'Header', 'This is example header.'),
(26, 9, 2, 'Nagłówek', 'To jest przykładowy nagłówek.'),
(27, 9, 3, 'заголовок', 'Это пример заголовка.'),
(28, 10, 1, 'Tabs placeholder', ''),
(29, 10, 2, 'Zakładki', ''),
(30, 10, 3, 'закладки', ''),
(31, 11, 1, 'Main menu', 'There are no attached pages to this menu yet.'),
(32, 11, 2, 'Menu główne', 'Nie podpięto jeszcze żadnych stron do tego menu.'),
(33, 11, 3, 'Главное меню', 'Там нет страницы, связанные с этим меню.'),
(34, 12, 1, 'News details', ''),
(35, 12, 2, 'Detale aktualności', ''),
(36, 12, 3, 'Подробнее Новости', ''),
(37, 13, 1, 'Shortcut menu', 'There are no attached pages to this menu yet.'),
(38, 13, 2, 'Menu skrótów', 'Nie podpięto jeszcze żadnych stron do tego menu.'),
(39, 13, 3, 'контекстное меню', 'Там нет страницы, связанные с этим меню.'),
(40, 14, 1, 'Big banner', 'No banners found.'),
(41, 14, 2, 'Duży baner', 'Brak banerów.'),
(42, 14, 3, 'Большой баннер', 'нет Баннеры.'),
(58, 20, 1, 'News Slider', ' '),
(59, 20, 2, 'Aktualności - slider', ' '),
(60, 20, 3, 'Aktualności - slider', ' '),
(88, 30, 1, 'Mały baner', ''),
(89, 30, 2, 'Small Banner', ''),
(90, 30, 3, 'Small Banner', ''),
(91, 31, 1, 'News add-form block', ''),
(92, 31, 2, 'Formularz dodawania aktualności', ''),
(93, 31, 3, 'Formularz dodawania aktualności', ''),
(94, 32, 1, 'Event list', ''),
(95, 32, 2, 'Lista wydarzeń', ''),
(96, 32, 3, 'список событий', ''),
(97, 33, 1, 'Old event list', ''),
(98, 33, 2, 'Lista wydarzeń archiwalnych', ''),
(99, 33, 3, 'Lista wydarzeń archiwalnych', ''),
(102, 34, 1, 'Event details', ''),
(105, 34, 2, 'Detale wydarzenia', ''),
(108, 34, 3, 'сведения о событии', ''),
(184, 60, 1, 'Postgraduate studies', ''),
(185, 60, 2, 'Studia podyplomowe', ''),
(186, 60, 3, 'аспирантура', ''),
(187, 61, 1, 'Bachelor studies', ''),
(188, 61, 2, 'Studia licencjackie', ''),
(189, 61, 3, 'Степень бакалавра', ''),
(199, 65, 1, 'Accordion', ''),
(200, 65, 2, 'Akordeon', ''),
(201, 65, 3, 'аккордеон', ''),
(202, 66, 1, 'Image slider', ''),
(203, 66, 2, 'Slider obrazów', ''),
(204, 66, 3, 'слайдер Изображение', ''),
(214, 70, 1, 'Images pile', ''),
(215, 70, 2, 'Stos obrazów', ''),
(216, 70, 3, 'изображения куча', ''),
(217, 71, 1, 'Employees', ''),
(218, 71, 2, 'Pracownicy', ''),
(219, 71, 3, 'Сотрудники', ''),
(241, 79, 1, 'Employee''s data', ''),
(242, 79, 2, 'Dane pracownika', ''),
(243, 79, 3, 'Данные работника', ''),
(280, 92, 1, 'Image gallery', ''),
(281, 92, 2, 'Galeria obrazów', ''),
(282, 92, 3, 'галерея изображений', ''),
(283, 93, 1, 'Popup', ''),
(284, 93, 2, 'Wyskakujące okno', ''),
(285, 93, 3, 'Всплывающее', ''),
(289, 95, 1, 'menu-glowne', ''),
(290, 95, 2, 'main-menu', ''),
(291, 95, 3, 'meniu-glownoje', ''),
(292, 96, 1, 'navigation menu cms', ''),
(293, 96, 2, 'menu nawigacyjne cms', ''),
(294, 96, 3, 'meniu nawigacyjnoje cms', ''),
(295, 97, 1, 'big baner cms', ''),
(296, 97, 2, 'duży baner cms', ''),
(297, 97, 3, 'bołszoj banier cms', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_accordion`
--

CREATE TABLE `block_accordion` (
  `panel_id` smallint(5) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED DEFAULT NULL,
  `is_published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `order` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `name` varchar(64) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `contents` text COLLATE utf8_polish_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `block_accordion`
--

INSERT INTO `block_accordion` (`panel_id`, `content_id`, `is_published`, `order`, `last_update_author_id`, `last_update_date`, `name`, `contents`) VALUES
(1, 771, 0, 0, 1, '2016-01-04 15:42:42', 'Dodawanie i zarządzanie Blokiem Akordeon', '<p><strong>Dodawanie</strong></p>\n\n<p>Będąc zalogowanym jako administrator włączamy tryb edycji klikając myszką na przycisk z ikoną &nbsp;puzla w prawym dolnym rogu.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px" /></p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png" /></p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony, wyszukujemy blok o nazwie &bdquo;Akordeon&rdquo;, można w tym celu użyć wyszukiwarki blok&oacute;w np. wybierając z listy &bdquo;Typ bloku&rdquo; pozycję &bdquo;Akordeon&rdquo;. Następnie chwytamy myszką ikonę strzałki skierowanej ku g&oacute;rze i przeciągamy blok w wybrany na stronie slot na bloki.Wstawiony blok akordeon posiada w ustawieniach możliwość zmiany rozmiaru tytułu i czy ma on być widoczny. Aby ustawić tytuł klikamy ikonę trybiku z ustawieniami.<img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_12_37_35.png" /></p>\n\n<p>Akordeon oferuje poziome rozwijane zakładki, podczas rozwijania jednej z nich, wcześniej rozwinięta zostaje zwinięta i tak dalej przy klikaniu na inne zakładki. Aby dodać zakładki klikamy na bloku tekst &bdquo;+ Dodaj nowy panel&rdquo;.</p>\n\n<p>Utworzony nowy panel czyli nasza pozioma zakładka daje nam teraz wiele możliwości.</p>\n\n<p>Tekst &bdquo;Nowy panel (1)&rdquo; możemy zmienić klikając na niego dwukrotnie i usuwając a następnie wpisując własny tekst.</p>\n\n<p>Utworzone panele możemy łatwo usuwać korzystając z ikonki kosza z prawej strony każdego panelu.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Akordeon.png" /></p>'),
(2, 795, 0, 0, 1, '2016-01-04 14:37:21', 'Dodawanie nowej strony', '<p style="margin-left: 1.25cm;">Po zalogowaniu się&nbsp;klikamy na widoczny z lewej strony przycisk panelu narzędzi administratora.</p>\n\n<p style="margin-left: 1.25cm;"><span style="letter-spacing:0.0175em; line-height:1.875em">Wybieramy pozycję &bdquo;STRONY&rdquo;.</span></p>\n\n<p style="margin-left:0.64cm">W otwartym oknie ustawień wybieramy przycisk &bdquo;Nowa strona&rdquo;.</p>\n\n<p style="margin-left:0.64cm">Otworzy nam się formularz dodawania nowej strony, kt&oacute;rego poszczeg&oacute;lne elementy wraz z charakterystyką przedstawia poniższy opis.</p>\n\n<p style="margin-left:0.64cm"><strong>1.&nbsp;</strong>Tytuł &ndash; może zawierać dowolne znaki, litery, cyfry i znaki specjalne .</p>\n\n<p style="margin-left:0.64cm"><strong>2.&nbsp;</strong>Język &ndash; wyb&oacute;r języka dla nowej strony, domyślnie polski, dostępny angielski i rosyjski.</p>\n\n<p style="margin-left:0.64cm"><strong>3.&nbsp;</strong>Adres URL &ndash; domyślnie adres ustawi się sam na podstawie tytułu nowej strony, można jednak go zmienić na inny niż domyślny, nie zalecamy jednak samodzielnej zmiany adresu URL &ndash; zmiany tylko dla zaawansowanych użytkownik&oacute;w.</p>\n\n<p style="margin-left:0.64cm"><strong>4.&nbsp;</strong>Strona nadrzędna &ndash; umieszcza w adresie do naszej nowej strony wybraną stronę nadrzędną w taki spos&oacute;b: pol/page/<strong>strona_nadrzedna</strong>/<strong>Adres_URL&nbsp;</strong>nadając jakby rodzica naszej nowej stronie.</p>\n\n<p style="margin-left:0.64cm"><strong>5.&nbsp;</strong>English (US) &ndash; ekwiwalentna strona dla języka angielskiego jeśli taka istnieje, domyślnie jest to strona gł&oacute;wna. Wybieramy tu stronę kt&oacute;ra wyświetli się po przełączeniu języka na angielski.</p>\n\n<p style="margin-left:0.64cm"><strong>6.&nbsp;</strong>Russian &ndash; ekwiwalentna strona dla języka rosyjskiego jeśli taka istnieje, domyślnie jest to strona gł&oacute;wna. Wybieramy tu stronę kt&oacute;ra wyświetli się po przełączeniu języka na rosyjski.</p>\n\n<p style="margin-left:0.64cm"><strong>7.</strong>&nbsp;Meta &bdquo;keywords&rdquo; - umieszczamy tu kluczowe słowa, kt&oacute;re najlepiej charakteryzują zawartość dokumentu. Postaraj się wyobrazić sobie jakich sł&oacute;w może użyć potencjalny użytkownik oraz jakie ewentualne błędy lub liter&oacute;wki mogą się wkraść do jego zapytania. Weź r&oacute;wnież pod uwagę, że nie wszyscy używają polskich znak&oacute;w diakrytycznych.</p>\n\n<p style="margin-left:0.64cm"><strong>8.&nbsp;</strong>Meta &bdquo;description&rdquo; - opis zawartości strony, zostanie wyświetlony przez wyszukiwarki zaraz po tytule strony. Należy go dobrać bardzo starannie, tak aby stanowił zachętę do odwiedzenia właśnie tej strony.</p>\n\n<p style="margin-left:0.64cm"><strong>9.&nbsp;</strong>Częstotliwość zmian na stronie &ndash; polepsza wyniki wyszukiwania w google, im częściej tym lepiej.</p>\n\n<p style="margin-left:0.64cm"><strong>10.&nbsp;</strong>Priorytet dla adresu URL &ndash; określa ważność adresu URL dla wyszukiwarki, można w podpiętych opcjach zaznaczyć czy wyszukiwarki mają indeksować naszą stronę i tworzyć jej kopię, można też zabronić przeglądarkom trzymać tę kopię w pamięci podręcznej.</p>\n\n<p style="margin-left:0.64cm"><strong>11.&nbsp;</strong>Gł&oacute;wny szablon strony &ndash; aktualnie jest jeden szablon &bdquo;Domyślny&rdquo;.</p>\n\n<p style="margin-left:0.64cm"><strong>12.&nbsp;</strong>Układ &ndash; układ do wstawiania element&oacute;w na stronie, do wyboru są 4 układy:</p>\n\n<p style="margin-left:0.64cm"><strong>-&nbsp;</strong>dwie kolumny - podstrona</p>\n\n<p style="margin-left:0.64cm">- jedna kolumna</p>\n\n<p style="margin-left:0.64cm">- dwie kolumny</p>\n\n<p style="margin-left:0.64cm">- trzy kolumny</p>\n\n<p style="margin-left:0.64cm"><strong>13.&nbsp;</strong>Opcja ustawia nową stronę jako &bdquo;włączoną&rdquo; od razu po zapisaniu.</p>\n\n<p style="margin-left:0.64cm"><strong>Edycja strony dostępna jest po kliknięciu tego przycisku, formularz edycji zawiera te same elementy jak formularz dodawania nowej strony.</strong></p>'),
(3, 795, 0, 1, 1, '2016-01-04 14:42:08', 'Dodawanie nowego Użytkownika', '<p><strong>Dodanie/edycja Użytkownika.</strong></p>\n\n<p><span style="letter-spacing:0.0175em; line-height:1.875em">Po zalogowaniu się&nbsp;klikamy na widoczny z lewej strony przycisk panelu narzędzi administratora.</span></p>\n\n<p style="margin-left:0.64cm">Wybieramy pozycję &bdquo;UŻYTKOWNICY&rdquo;.</p>\n\n<p style="margin-left:0.64cm">W otwartym oknie ustawień wybieramy przycisk &bdquo;Nowy użytkownik&rdquo;.</p>\n\n<p style="margin-left:0.64cm">Otworzy nam się formularz dodawania nowego użytkownika, kt&oacute;rego poszczeg&oacute;lne elementy wraz z charakterystyką przedstawia poniższy opis.</p>\n\n<p style="margin-left:0.64cm"><strong>1.&nbsp;</strong>Login &ndash; nazwa kt&oacute;rej nowy użytkownik będzie używał do logowania się, może zawierać dowolne znaki, litery, cyfry i znaki specjalne .</p>\n\n<p style="margin-left:0.64cm"><strong>2.&nbsp;</strong>Typ konta &ndash; wyb&oacute;r jednego z trzech typ&oacute;w konta:</p>\n\n<p style="margin-left:0.64cm"><strong>-</strong>&nbsp;User: z tymi uprawnieniami użytkownik może logować się, przeglądać treści publiczne strony, zapisywać się na wydarzenia.</p>\n\n<p style="margin-left:0.64cm"><strong>-</strong>&nbsp;Moderator: dziedziczy od User , poza tym ma uprawnienia do wprowadzania zmian użytkownik&oacute;w i stron.</p>\n\n<p style="margin-left:0.64cm"><strong>-&nbsp;</strong>Administrator: dziedziczy od moderatora, z tymi uprawnieniami użytkownik może dodawać, edytować i usuwać i bloki (dostępny tryb projektowy).</p>\n\n<p style="margin-left:0.64cm"><strong>3.&nbsp;</strong>E-mail &ndash; e-mail nowego użytkownika.</p>\n\n<p style="margin-left:0.64cm"><strong>4.&nbsp;</strong>Nowe hasło &ndash; hasło należy nadać samemu, jest to jedyna droga żeby m&oacute;c się nim potem logować, hasło generowane automatycznie jest zabezpieczone szyfrowaniem, może mieć dowolną liczbę znak&oacute;w.</p>\n\n<p style="margin-left:0.64cm"><strong>5.&nbsp;</strong>Podstawowe dane dotyczące nowego użytkownika.</p>\n\n<p><strong>Edycja danych użytkownika dostępna jest po kliknięciu ikony edycji w wierszu z użytkownikiem, u kt&oacute;rego chcemy wprowadzić zmiany, formularz edycji zawiera te same elementy jak formularz dodawania nowego użytkownika.</strong></p>'),
(4, 795, 0, 2, 1, '2016-01-04 15:04:37', 'Dodawanie bloków na stronie', '<p><strong>Dodawanie blok&oacute;w.</strong></p>\n\n<p style="margin-left:0.64cm">Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną puzla w prawym dolnym rogu ekranu.</p>\n\n<p style="margin-left:0.64cm"><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px" /></p>\n\n<p style="margin-left:0.64cm">W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p style="margin-left:0.64cm"><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png" /></p>\n\n<p style="margin-left:0.64cm">Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony</p>\n\n<p>1. Kategoria językowa &ndash; ustawienie języka, każdemu blokowi można przypisać kategorię językową, tworząc nowy blok czy edytując już istniejący &ndash; więcej w pkt 6b.</p>\n\n<p>2. Typ bloku &ndash; lista wszystkich typ&oacute;w blok&oacute;w w systemie.</p>\n\n<p>3. Szukaj po nazwie &ndash; wyszukiwarka blok&oacute;w, należy wpisać całą nazwę bądź jej fragment.</p>\n\n<p>4. Ikona dodania bloku do strony &ndash; należy chwycić ikonę i przeciągnąć ją nad wyznaczone pola na stronie, miejsca, w kt&oacute;rych można upuścić blok zaznaczone są na zdjęciu &bdquo;<strong>Sloty do wstawiania blok&oacute;w</strong>&rdquo; poniżej tej listy, w momencie gdy blok znajdzie się nad takim miejscem, zrobi się ono większe i podświetlone na pomarańczowo &ndash; wtedy można upuścić blok i znajdzie się on na stronie.</p>\n\n<p>5. Ikona przestawiania &ndash; można porządkować bloki poprzez przesuwanie ich w inne miejsca tabeli, domyślnie ustawione są alfabetycznie.</p>\n\n<p>6. Ikona edycji &ndash; po kliknięciu w ikonę pojawi się okno ustawień opisane na zdjęciu &bdquo;<strong>Okno ustawień edycji właściwości bloku</strong>&rdquo; poniżej tej listy.</p>\n\n<p style="margin-left:0.5cm"><strong>6a.&nbsp;</strong>Nazwa bloku we wszystkich językach &ndash; nadanie nazwy dla bloku w języku polskim, angielskim i rosyjskim</p>\n\n<p style="margin-left:0.5cm"><strong>6b.&nbsp;</strong>Traktuj jako &ndash; przypisanie bloku do kategorii języka, domyślnie jest to &bdquo;wszystkie kategorie&rdquo;, czyli bloki będą widoczne na wszystkich stronach niezależnie od języka strony, można ustawić język polski, angielski lub rosyjski &ndash; wtedy dany blok będzie widoczny tylko na stronie z ustawieniem języka adekwatnym do wybranego języka dla bloku.</p>\n\n<p>7. Ikona info &ndash; info o bloku.</p>\n\n<p><strong>Sloty do wstawiania blok&oacute;w</strong></p>\n\n<p>Teoretycznie bloki można upuszczać w dowolnym slocie, jednak niekt&oacute;re sloty mają lepsze predyspozycje do wyświetlania niekt&oacute;rych blok&oacute;w, są one zaznaczone na zdjęciu poniżej.Po upuszczeniu bloku w dobre miejsce doda się on na stronie.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_14_47_21.png" /></p>'),
(5, 795, 0, 3, 1, '2016-01-04 15:44:10', 'Tworzenie bloków uniwersalnych', '<p><strong>Tworzenie blok&oacute;w dynamicznych.</strong></p>\n\n<p>Każdy blok posiada swoje ustawienia,&nbsp;na belce ustawień pierwsza ikona dyskietki oznacza, że można ten blok zapisać jako dynamiczny i dodać go potem na innej stronie bez potrzeby tworzenia jego treści od początku.</p>\n\n<p>Po kliknięciu w ikonę otworzy nam się okno zapisu.</p>\n\n<p>1. Należy podać nazwy bloku dla wszystkich język&oacute;w, może być to wsp&oacute;lna nazwa dla wszystkich język&oacute;w.</p>\n\n<p>2. Wybrany język ma wpływ na wyszukiwanie bloku w wyszukiwarce blok&oacute;w w polu &bdquo;kategoria językowa&rdquo;.</p>\n\n<p>Po zapisaniu blok jest dostępny po najechaniu kursorem na ikonę dyskietki umieszczoną obok ikony plusa do dodawania blok&oacute;w w lewej dolnej części ekranu.</p>\n\n<p>&nbsp;</p>\n\n<p>Tak za zapisany blok można upuszczać na dowolnych stronach, usunięcie takiego bloku ze strony nie ma wpływu na inne jego wystąpienia na innych stronach. Natomiast usunięcie tego bloku z listy blok&oacute;w dynamicznych spowoduje usunięcie go z listy i tym samym z wszystkich stron, na kt&oacute;rych on występuje.</p>'),
(6, 795, 0, 4, 1, '2016-01-04 15:35:05', 'Ustawienia bloków', '<p><strong>Ustawienia blok&oacute;w.</strong></p>\n\n<p>Każdy blok wstawiony na stronę po najechaniu na niego kursorem wyświetli kilka opcji na swojej belce.</p>\n\n<p>W lewym g&oacute;rnym rogu przed numerem ID i nazwą znajduje się &bdquo;krzyżyk&rdquo; służący do przerzucenia bloku do innego slotu. Działanie ikon z prawej strony przedstawia opis poniżej:</p>\n\n<p>1. Zapisz jako blok dynamiczny &ndash; domyślnie wstawiony blok jest widoczny tylko na stronie, na kt&oacute;rej go wstawiliśmy, gotowy wypełniony naszymi treściami blok można zapisać jako dynamiczny i dodać go potem na innej stronie bez potrzeby tworzenia go od początku. Zapisany w ten spos&oacute;b blok jest dostępny po najechaniu kursorem na ikonę dyskietki umieszczoną pod ikoną plusa do dodawania blok&oacute;w.</p>\n\n<p>2. Opublikuj/ukryj blok &ndash; klikając na tą ikonę można ukryć blok, w momencie wyjścia z trybu projektowego nie będzie on widoczny, po ponownym kliknięciu na tą ikonę, blok będzie opublikowany.</p>\n\n<p>3. Okno zarządzania &ndash; po kliknięciu otworzy się okno zarządzania blokiem, w przypadku bloku menu gł&oacute;wnego będą to ustawienia dotyczące menu, dodawanie, usuwanie, ukrywanie, publikacja i edycja kategorii menu oraz jego element&oacute;w.</p>\n\n<p>4. Ustawienia &ndash; pod tą ikoną kryją się og&oacute;lne ustawienia, m. in. Rozmiar tytułu menu i czy ma być widoczny, z kt&oacute;rej kategorii mają być wyświetlane elementy menu, czy ma być to menu pionowe bądź poziome.</p>\n\n<p>5. Usuwanie bloku &ndash; kliknięcie na ikonę kosza otworzy okno z potwierdzeniem usunięcia bloku, zostanie on usunięty ze strony a nie listy blok&oacute;w. &nbsp; &nbsp;</p>'),
(7, 795, 0, 5, 1, '2016-01-04 15:46:22', 'Dodawanie i edycja menu', '<p><strong>Dodawanie i edycja menu &ndash; menu gł&oacute;wne, menu nawigacyjne i menu skr&oacute;t&oacute;w.</strong></p>\n\n<p>Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną puzla.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px" /></p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png" /></p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony. Do dodawania menu do strony przygotowane są trzy bloki, wstawmy pierwszy z nich &ndash; kolejne będą wstawiane analogicznie. Wyszukujemy blok o nazwie &bdquo;Menu gł&oacute;wne&rdquo;, można w tym celu użyć wyszukiwarki blok&oacute;w np. wybierając z listy &bdquo;Typ bloku&rdquo; pozycję &bdquo;Menu gł&oacute;wne&rdquo;. Następnie chwytamy myszką ikonę zaznaczoną na zdjęciu poniżej i przeciągamy blok w wybrany na stronie slot na bloki.</p>\n\n<p>Przechodzimy do ustawień klikając na belce bloku ikonę trybiku.</p>\n\n<p>Otworzy nam się okno z ustawieniami dla tego bloku.</p>\n\n<p>1. Tytuł -ustawiamy rozmiar tytułu naszej galerii oraz czy ma on być widoczny .</p>\n\n<p>2. Kategoria &ndash;wybieramy kategorię, do kt&oacute;rej są przypisywane elementy menu, w systemie stworzona jest &bdquo;Domyślna kategoria&rdquo; gotowa do użycia.</p>\n\n<p>3. Orientacja &ndash; możemy wybrać jak ma się wyświetlać nasze menu &ndash; poziomo lub pionowo.</p>\n\n<p>4. Pozostałe opcje &ndash; tu możemy dodać przycisk (mała ikona plusa) rozwijania/zwijania do każdej pozycji menu.</p>\n\n<p>Aby dodać elementy do menu klikamy na belce bloku ikonę folderu &ndash; okno zarządzania.</p>\n\n<p>Następnie przycisk na otwartym oknie &bdquo;Kategorię&rdquo;, jeśli chcemy stworzyć inną niż domyślną kategorię dla naszych element&oacute;w menu lub przycisk &bdquo;Nowy element&rdquo; żeby dodać pozycję do menu.</p>\n\n<p>W formularzu dodawania nowego elementu po kolei zmieniamy ustawienia.</p>\n\n<p>1. Kategoria &ndash; wybieramy kategorię element&oacute;w menu.</p>\n\n<p>2. Element nadrzędny &ndash; tą opcją kontrolujemy ustawienie poszczeg&oacute;lnych element&oacute;w menu względem siebie. Możemy wybrać, przy każdym nowym elemencie menu, kt&oacute;ry element ma być nadrzędny względem wstawianego.</p>\n\n<p>3. Typ adresu URL &ndash; wybieramy wewnętrzny, zewnętrzny lub brak.</p>\n\n<p>4. Adres URL &ndash; wybieramy dokąd ma prowadzić element menu, jeśli w typie adresu URL wybrana została pozycja &bdquo;wewnętrzny&rdquo; dostępna tu będzie lista wszystkich naszych stron, jeśli wybrane zostało &bdquo;brak&rdquo; to pole będzie nieczynne.</p>\n\n<p>5. Typ elementu &ndash; możemy zdecydować jak będzie się prezentować nasz element menu. DO wyboru jest kilka opcji:</p>\n\n<p>- tylko tekst</p>\n\n<p>- tylko ikona</p>\n\n<p>- tylko obraz</p>\n\n<p>- ikona + tekst</p>\n\n<p>- obraz + tekst</p>\n\n<p>6. Tytuł &ndash; wpisujemy tytuł naszego elementu menu.</p>\n\n<p>W ten spos&oacute;b dodaliśmy pierwszą pozycje w menu, analogicznie tworzymy kolejne, dodając grupy i elementy menu.</p>\n\n<p>Menu nawigacyjne i menu skr&oacute;t&oacute;w posiadają takie same ustawienia i okna zarządzania, r&oacute;żnią się jedynie wyświetlaniem. Zdjęcie poniżej przedstawia zestawienie trzech typ&oacute;w menu obok siebie.</p>'),
(8, 795, 0, 6, 1, '2016-01-04 15:50:57', 'Blok tekstowy', '<p><strong>1. Dodawanie i edycja blok&oacute;w tekstowych.</strong></p>\n\n<p>Będąc zalogowanym jako administrator włączamy tryb edycji klikając myszką na przycisk z ikoną puzla w prawym dolnym rogu ekranu. W trybie edycji dookoła ekranu przebiega czerwona przerywana linia.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px" /></p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png" style="height:154px; width:359px" /></p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony, wyszukujemy blok o nazwie &bdquo;Blok tekstowy&rdquo;, można w tym celu użyć wyszukiwarki blok&oacute;w np. wybierając z listy &bdquo;Typ bloku&rdquo; pozycję &bdquo;Blok tekstowy&rdquo;. Następnie chwytamy myszką ikonę zaznaczoną na zdjęciu poniżej i przeciągamy blok w wybrany na stronie slot na bloki.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_19_33.png" /></p>\n\n<p>Wstawiony blok tekstowy posiada standardowe ustawienia.&nbsp;&nbsp;Aby zacząć edycję i odkryć jego szerokie możliwości należy dwukrotnie kliknąć myszką na tekst &bdquo;Przykładowy tekst. Proszę go zmienić.&rdquo; lub ikonę edycji.</p>\n\n<p>Otworzy nam się narzędzie o nazwie &bdquo;CKEditor&rdquo;. Jest wizualnym&nbsp;edytorem HTML&nbsp;rozpowszechnianym na licencji&nbsp;Open Source, kt&oacute;ry pozwala na łatwe wprowadzanie tekstu za pomocą&nbsp;interfejsu&nbsp;przypominającego programy typu&nbsp;Microsoft Word.&nbsp;Formatowanie tekstu&nbsp;odbywa się za pomocą zestawu przycisk&oacute;w umożliwiających wybranie&nbsp;czcionki, rozmiaru, koloru liter i tła, ustawienie pogrubienia, podkreślenia czy&nbsp;kursywy, jak r&oacute;wnież wyr&oacute;wnanie tekstu (do lewej, do prawej, wycentrowanie,&nbsp;wyjustowanie). Poza tym użytkownik ma do dyspozycji funkcje takie jak wstawienie listy (wypunktowanej lub numerowanej), formularzy i wszystkich jego element&oacute;w, tabeli, obrazka, video czy odnośnika (linku).</p>\n\n<p><strong>1a. Dodawanie link&oacute;w.</strong></p>\n\n<p>Klikamy ikonę odnośnika na panelu CKEditora, w ustawieniach podajemy żądane parametry linku.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start(1).png" /></p>\n\n<p>1. Informacje &ndash; wybieramy typ odnośnika (Adres URL, Odnośnik wewnątrz strony &ndash; kotwica, adres-email), wpisujemy adres ręcznie bądź klikamy przycisk &bdquo;Przeglądaj&rdquo; i wybieramy element, do kt&oacute;rego ma prowadzić odnośnik.</p>\n\n<p>2. Obiekt docelowy &ndash; wybieramy z listy, gdzie ma się otworzyć to, do czego prowadzi link.</p>\n\n<p>- ramka</p>\n\n<p>- wyskakujące okno</p>\n\n<p>- nowe okno (_blank)</p>\n\n<p>- okno najwyżej w hierarchii (_top)</p>\n\n<p>- to samo okno (_self)</p>\n\n<p>- okno nadrzędne (_parent)</p>\n\n<p>3. Wyślij &ndash; wysyłamy plik na serwer, po wysłaniu ustawi nam się adres URL do tego pliku w zakładce &bdquo;1. Informacje&rdquo;.</p>\n\n<p>4. Zaawansowane &ndash; tu znajdują się zaawansowane opcje dotyczące odnośnika.</p>\n\n<p>Zatwierdzamy nasze ustawienia przyciskiem &bdquo;ok&rdquo; i gotowe. Link można ustawić dla tekstu bądź dowolnego elementu wstawionego do bloku tekstowego, np. grafiki.</p>\n\n<p><a href="http://google.com" target="_blank">Przykład linku do google.com</a></p>\n\n<p><strong>1b. Dodawanie obrazk&oacute;w.</strong></p>\n\n<p>Klikamy ikonę grafiki na panelu CKEditora, w ustawieniach podajemy żądane parametry obrazka.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start(2).png" /></p>\n\n<p>1. Informacje o obrazku &ndash; podajemy adres URL do pliku z obrazkiem, możemy kliknąć na &bdquo;Przeglądaj&rdquo; w celu wybrania obrazka z biblioteki CKEditora. Dobrą praktyką jest wypełnianie pola &bdquo;Teks zastępczy&rdquo; &ndash; pojawi się on zamiast obrazka w przeglądarkach, kt&oacute;re z r&oacute;żnych przyczyn nie wyświetlają obrazk&oacute;w. Podajemy szerokość i wysokość, rozmiar obramowania w px, odstęp poziomy i pionowy od innych element&oacute;w strony oraz wyr&oacute;wnanie obrazka względem innych element&oacute;w&nbsp;na stronie (do lewej bądź prawej).</p>\n\n<p>Przycisk &bdquo;Compress Image&rdquo; służy do zmniejszenia wagi pliku z obrazkiem, przykładowo jeśli nasz plik z obrazkiem ma zbyt dużą rozdzielczość i zbyt dużo KB przez co dłużej się ładuje na stronę, możemy wpisać wysokość i szerokość obrazka jaka nam jest potrzebna i kliknąć przycisk kompresji &ndash; stworzy nam się kopia naszego obrazka &bdquo;lżejsza&rdquo; dla strony i to ona się wyświetli po zatwierdzeniu zmian przyciskiem &bdquo;ok&rdquo;.</p>\n\n<p>Z kolei przycisk &bdquo;Make Responsive&rdquo; służy do naprawienia wyświetlania obrazk&oacute;w w r&oacute;żnych rozdzielczościach strony, dodaję on klasę responsywną do styli obrazka przez co nasz obrazek zmniejsza się wraz ze zmniejszaniem okna przeglądarki, szerokość i wysokość obrazka są wtedy niedostępne.</p>\n\n<p>2. Hiperłącze &ndash; można użyć obrazka jako linku, w tej zakładce wpisujemy adres URL elementu, do kt&oacute;rego ma prowadzić hiperłącze i gdzie ten element ma się wyświetlić.</p>\n\n<p>3. Wyślij &ndash; możemy wysłać do biblioteki CKEditora pliki z obrazkami i ustawić do nich adres URL.</p>\n\n<p>4. Zaawansowane &ndash; tu znajdują się zaawansowane opcje dotyczące obrazka, dodawanie klas wyświetlania, opisy itp.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20codilab/codilab_ppt-1.png" />Przykład obrazka 3000x1687px</p>\n\n<p><strong>1c. Dodawanie tabeli.</strong></p>\n\n<p>Klikamy ikonę tabeli na panelu CKEditora, w ustawieniach podajemy żądane właściwości nowej tabeli.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start(3).png" /></p>\n\n<p>1. Właściwości tabeli &ndash; podajemy liczbę wierszy i kolumn, wysokość i szerokość dotyczy całej tabeli. Dalej wybieramy jak maja być wyświetlane nagł&oacute;wki, odstęp pomiędzy kom&oacute;rkami, grubość obramowania i dopełnienie kom&oacute;rki. Można ustawić wyr&oacute;wnanie tabeli do prawej, lewej lub do środka strony, dalej tytuł i opis w podsumowaniu.</p>\n\n<p>2. Zaawansowane &ndash; tu znajdują się zaawansowane opcje dotyczące tabeli, dodawanie klas wyświetlania, style itp.</p>\n\n<table align="center" border="1" cellpadding="1" cellspacing="1" style="background-color:rgb(255, 255, 255); letter-spacing:0.245px; width:870px">\n	<caption>Przykład tabeli</caption>\n	<thead>\n		<tr>\n			<th scope="col" style="background-color:rgb(255, 255, 102); text-align:center; vertical-align:middle">Pierwszy wiersz pierwsza kolumna</th>\n			<th scope="col" style="background-color:rgb(255, 255, 102); text-align:center; vertical-align:middle">Pierwszy wiersz druga kolumna</th>\n			<th scope="col" style="background-color:rgb(255, 255, 102); text-align:center; vertical-align:middle">Pierwszy wiersz trzecia kolumna</th>\n			<th scope="col" style="background-color:rgb(255, 255, 102); text-align:center; vertical-align:middle">Pierwszy wiersz czwarta kolumna</th>\n		</tr>\n	</thead>\n	<tbody>\n		<tr>\n			<td class="td-responsible">&nbsp;</td>\n			<td class="td-responsible">&nbsp;</td>\n			<td class="td-responsible">&nbsp;</td>\n			<td class="td-responsible">&nbsp;</td>\n		</tr>\n		<tr>\n			<td class="td-responsible">&nbsp;</td>\n			<td class="td-responsible">&nbsp;</td>\n			<td class="td-responsible">&nbsp;</td>\n			<td class="td-responsible">&nbsp;</td>\n		</tr>\n		<tr>\n			<td class="td-responsible">&nbsp;</td>\n			<td class="td-responsible">&nbsp;</td>\n			<td class="td-responsible">&nbsp;</td>\n			<td class="td-responsible">&nbsp;</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p><strong>1d. Formatowanie tekstu.</strong></p>\n\n<p>CKEditor oferuje bardzo dużo możliwości edytowania tekstu, w swojej palecie przycisk&oacute;w do formatowania ma m.in.&nbsp;<strong>pogrubienie</strong>,&nbsp;<em>kursywa</em>,&nbsp;<u>podkreślenie</u>,&nbsp;<s>przekreślenie</s>,&nbsp;<span style="font-size:10.5px">indeks dolny</span>,<span style="font-size:10.5px">&nbsp;indeks g&oacute;rny</span>, usuń formatowanie, lista numerowana, lista wypunktowana, zmniejsz wcięcie, zwiększ wcięcie, cytat, wyr&oacute;wnaj do lewej, wyśrodkuj, wyr&oacute;wnaj do prawej, wyjustuj, kierunek tekstu od lewej strony do prawej, kierunek tekstu od prawej strony do lewej, trzy gotowe szablony zawartości, wytnij, kopiuj, wklej, wklej jako zwykły tekst, wklej z programu MS Word, og&oacute;lne style formatujące, format nagł&oacute;wk&oacute;w, czcionka, rozmiar, kolor tekstu i kolor tła. Dodatkowo w CKEditorze można tworzyć formularze z wszystkimi ich elementami jak pole wyboru (checkbox), przycisk opcji (radio), pole tekstowe, obszar tekstowy, lista wyboru, przycisk, przycisk graficzny, pole ukryte. Za pomocą tych narzędzi można dowolnie sformatować tekst dla naszych potrzeb.</p>\n\n<p><strong>1e. Czyszczenie tekstu.</strong></p>\n\n<p>Do czyszczenia tekstu CKEditor oferuje nam trzy przyciski.</p>\n\n<p>1. Wklej jako czysty tekst &ndash; po kliknięciu pojawi się okienko, w kt&oacute;rym wklejamy nasz tekst i zapisujemy, wstawiony w ten spos&oacute;b tekst będzie pozbawiony formatowania.</p>\n\n<p>2. Wklej z programu MS Word &ndash; po kliknięciu na przycisk pojawi się okienko, w kt&oacute;rym wklejamy tekst skopiowany z programy MS Word i zatwierdzamy &bdquo;ok&rdquo;. Wstawi się nam tekst zachowując formatowanie źr&oacute;dłowe.</p>\n\n<p>3. Usuń formatowanie &ndash; niekiedy potrzebujemy wstawić tekst z r&oacute;żnych źr&oacute;deł, kopiując go kopiujemy też jego formatowanie, aby dopasował się do naszego tekstu trzeba go zaznaczyć i kliknąć przycisk &bdquo;Usuń formatowanie&rdquo;</p>\n\n<p><strong>1f. Wstawianie video z YouTube.</strong></p>\n\n<p>W bardzo szybki i łatwy spos&oacute;b możemy dodać video z witryny YouTube, wystarczy kliknąć ikonę YouTube na panelu CKEditora.</p>\n\n<p>Znajdujemy film w serwisie YouTube i kopiujemy jego adres z paska adresu, wklejamy go do pola &bdquo;Wklej link URL do wideo&rdquo; w otwartym oknie &bdquo;Załącznik wideo z YouTube&rdquo;, podajemy wysokość i szerokość dla wyświetlanego filmu, wybieramy dodatkowe dostępne opcje wedle potrzeby:</p>\n\n<p>- &nbsp;Pokaż sugerowane filmy po zakończeniu odtwarzania</p>\n\n<p>- &nbsp;Użyj starego kodu</p>\n\n<p>- Włącz rozszerzony tryb prywatności</p>\n\n<p>- Autoodtwarzanie</p>\n\n<p>- Rozpocznij od (ss lub mm:ss lub gg:mm:ss)</p>\n\n<p>Przykład filmu z serwisu YouTube</p>\n\n<p><iframe align="middle" class="img-responsive" frameborder="0" scrolling="no" src="//www.youtube.com/embed/iCkYw3cRwLo"></iframe></p>'),
(9, 795, 0, 7, 1, '2016-01-04 15:41:22', 'Korzystanie z CKEditora', '<p><strong>Korzystanie z CKEditora.</strong></p>\n\n<p>CKEditor jest edytorem o dużych możliwościach, jego wszystkie narzędzia dostępne są po dwukrotnie kliknąć myszką na tekst &bdquo;Przykładowy tekst. Proszę go zmienić.&rdquo; lub ikonę edycji.</p>\n\n<p>CKEditor&nbsp;jest wizualnym&nbsp;edytorem HTML&nbsp;rozpowszechnianym na licencji&nbsp;Open Source, kt&oacute;ry pozwala na łatwe wprowadzanie tekstu za pomocą&nbsp;interfejsu&nbsp;przypominającego programy typu&nbsp;Microsoft Word.&nbsp;Formatowanie tekstu&nbsp;odbywa się za pomocą zestawu przycisk&oacute;w umożliwiających wybranie&nbsp;czcionki, rozmiaru, koloru liter i tła, ustawienie pogrubienia, podkreślenia czy&nbsp;kursywy, jak r&oacute;wnież wyr&oacute;wnanie tekstu (do lewej, do prawej, wycentrowanie,&nbsp;wyjustowanie). Poza tym użytkownik ma do dyspozycji funkcje takie jak wstawienie listy (wypunktowanej lub numerowanej), formularzy i wszystkich jego element&oacute;w, tabeli, obrazka, video czy odnośnika (linku).</p>\n\n<p>Poniżej znajdują się wszystkie przyciski CKEditora, wymienione z nazw poniżej grafik w kierunku od lewej do prawej.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Detale_aktualnos%CC%81ci.png" style="height:110px; width:839px" /></p>\n\n<p>Pogrubienie, kursywa, podkreślenie, przekreślenie, indeks dolny, indeks g&oacute;rny, usuń formatowanie, lista numerowana, lista wypunktowana, zmniejsz wcięcie, zwiększ wcięcie, cytat, utw&oacute;rz pojemnik Div, wyr&oacute;wnaj do lewej, wyśrodkuj, wyr&oacute;wnaj do prawej, wyjustuj, kierunek tekstu od lewej strony do prawej, kierunek tekstu od prawej strony do lewej, set language, wstaw/edytuj odnośnik, usuń odnośnik, wstaw/edytuj kotwicę, obrazek, flash, tabela, wstaw poziomą linie, wstaw znak specjalny, wstaw podział strony, iFrame, załącznik video z Youtube.</p>\n\n<p>Szablony, wytnij, kopiuj, wklej, wklej jako zwykły tekst, wklej z programu MS Word, cofnij, pon&oacute;w, znajdź, zamień, zaznacz wszystko, sprawdź pisownię podczas pisania, formularz, pole wyboru (checkbox), przycisk opcji (radio), pole tekstowe, obszar tekstowy, lista wyboru, przycisk, przycisk graficzny, pole ukryte.</p>\n\n<p>Style formatujące, format, czcionka, rozmiar, kolor tekstu, kolor tła, pokaż bloki, pokaż informacje o CKEditor.</p>'),
(10, 795, 0, 8, 1, '2016-01-04 15:42:07', 'Blok Akordeon', '<p><strong>Dodawanie</strong></p>\n\n<p>Będąc zalogowanym jako administrator włączamy tryb edycji klikając myszką na przycisk z ikoną &nbsp;puzla w prawym dolnym rogu.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px" /></p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png" /></p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony, wyszukujemy blok o nazwie &bdquo;Akordeon&rdquo;, można w tym celu użyć wyszukiwarki blok&oacute;w np. wybierając z listy &bdquo;Typ bloku&rdquo; pozycję &bdquo;Akordeon&rdquo;. Następnie chwytamy myszką ikonę strzałki skierowanej ku g&oacute;rze i przeciągamy blok w wybrany na stronie slot na bloki.Wstawiony blok akordeon posiada w ustawieniach możliwość zmiany rozmiaru tytułu i czy ma on być widoczny. Aby ustawić tytuł klikamy ikonę trybiku z ustawieniami.<img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_12_37_35.png" /></p>\n\n<p>Akordeon oferuje poziome rozwijane zakładki, podczas rozwijania jednej z nich, wcześniej rozwinięta zostaje zwinięta i tak dalej przy klikaniu na inne zakładki. Aby dodać zakładki klikamy na bloku tekst &bdquo;+ Dodaj nowy panel&rdquo;.</p>\n\n<p>Utworzony nowy panel czyli nasza pozioma zakładka daje nam teraz wiele możliwości.</p>\n\n<p>Tekst &bdquo;Nowy panel (1)&rdquo; możemy zmienić klikając na niego dwukrotnie i usuwając a następnie wpisując własny tekst.</p>\n\n<p>Utworzone panele możemy łatwo usuwać korzystając z ikonki kosza z prawej strony każdego panelu.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Akordeon.png" /></p>'),
(11, 795, 0, 9, 1, '2016-01-04 14:41:05', 'Dodawanie aktualności', '<p><strong>1. Dodawanie aktualności.</strong></p>\n\n<p>Obsługę aktualności zapewniają cztery bloki &ndash; &bdquo;Lista aktualności&rdquo; , &bdquo; Detale aktualności&rdquo;, &bdquo;Formularz dodawania aktualności&rdquo; i &bdquo;Aktualności slider&rdquo;. Zacznijmy od wstawienia bloku Lista aktualności. Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną puzla w prawej dolnej części ekranu.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px" /></p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png" /></p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony, wyszukujemy blok o nazwie &bdquo;Lista aktualności&rdquo;, można w tym celu użyć wyszukiwarki blok&oacute;w np. wybierając z listy &bdquo;Typ bloku&rdquo; pozycję &bdquo;Lista aktualności&rdquo;. Następnie chwytamy myszką ikonę zaznaczoną na zdjęciu poniżej i przeciągamy blok w wybrany na stronie slot na bloki.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_19_33.png" /></p>\n\n<p>Wstawiony blok Lista aktualności posiada pr&oacute;cz standardowych ustawień, także unikalne dla tego bloku. Na belce Listy aktualności klikamy ikonę folderu - &bdquo;okna zarządzania&rdquo;.</p>\n\n<p>Otworzy nam się panel, w kt&oacute;rym utworzymy naszą kategorię aktualności a potem nową aktualność, kt&oacute;rą przypiszemy do tej kategorii. Klikamy przycisk &bdquo;Kategorie&rdquo;.</p>\n\n<p>A następnie przycisk &bdquo;Nowa kategoria&rdquo;.</p>\n\n<p>W panelu wpisujemy tytuł dla kategorii aktualności, będzie potem można przypisywać wiele aktualności pod tą kategorię i wyświetlać wszystkie naraz w danej kategorii, możemy zaznaczyć &bdquo;opublikuj po zapisaniu&rdquo; w celu natychmiastowego opublikowania tej kategorii i kliknąć &bdquo;Zapisz&rdquo;.</p>\n\n<p>Teraz możemy już tworzyć nowe aktualności. Zamykamy panel &bdquo;Kategorie - Zarządzanie&rdquo; i klikamy przycisk &bdquo;Nowa aktualność&rdquo;. Uzupełniamy formularz dodawania nowej aktualności.</p>\n\n<p>1. Kategoria &ndash; wybieramy z listy kategorię, do kt&oacute;rej przypiszemy nasze wydarzenie. W naszym przykładzie jest tylko jedna kategoria, kt&oacute;rą stworzyliśmy powyżej.</p>\n\n<p>2. Tytuł &ndash; nadajemy tytuł wydarzeniu, w naszym przykładzie jest to &bdquo;Nowe wydarzenie&rdquo;</p>\n\n<p>3. Adres URL &ndash; w momencie wprowadzania tytułu ustawi się automatycznie.</p>\n\n<p>4. Etykieta &ndash; pojawi się pod tytułem jako jego dopełnienie.</p>\n\n<p>5, 6. Data, Godzina rozpoczęcia &ndash; domyślnie ustawia się na obecną od momentu dodawania aktualności, można ustawić inną datę i godzinę rozpoczęcia.</p>\n\n<p>7.&nbsp;Obraz &ndash; możemy dodać obraz do wydarzenia lub kilka obraz&oacute;w i ustawić klikając na k&oacute;łeczko w prawym g&oacute;rnym rogu loadera, kt&oacute;ry obraz ma być jako zdjęcie gł&oacute;wne wyświetlane na liście aktualności. Ikoną strzałki ładujemy plik obrazu, możemy też użyć edycji ikoną oł&oacute;wka.</p>\n\n<p>8.&nbsp;Treść &ndash; opisujemy tutaj aktualność korzystając z CKEdytora.</p>\n\n<p>9. Zajawka &ndash; zajawka będzie wyświetlana na liście wydarzeń jako skr&oacute;cona treść, można użyć do 350 znak&oacute;w, cała treść będzie dostępna w detalach aktualności po kliknięciu na aktualność, o tym w dalszej części opisu.</p>\n\n<p>Po zapisaniu wszystkich wprowadzonych treści w bloku lista aktualności pojawi się nasza nowa aktualność. Teraz możemy skorzystać z ustawień dostępnych na belce bloku &bdquo;Lista aktualności&rdquo;.</p>\n\n<p>Klikamy na ikonę trybiku.</p>\n\n<p>Pojawi nam się okno z ustawieniami, kt&oacute;rych opis jest poniżej.</p>\n\n<p>1. Rozmiar tytułu &ndash; określa wielkość tytułu listy wydarzeń i czy ma być widoczny na stronie.</p>\n\n<p>2. Ilość element&oacute;w &ndash; ustawiamy ile naraz jest widocznych element&oacute;w na stronie paginacji.</p>\n\n<p>3. Wyświetl detale aktualności na stronie &ndash; &bdquo;Detale aktualności&rdquo; to blok, w kt&oacute;rym pojawi się nasza aktualność po kliknięciu na nią w liście aktualności. Blok &bdquo;Detale aktualności&rdquo; można umieścić na innej stronie i to na nią będzie nas kierowało po kliknięciu na aktualność z listy aktualności, można też umieścić detale aktualności na tej samej stronie. Domyślnie ustawia się strona startowa (gł&oacute;wna).</p>\n\n<p>Ukrywanie listy aktualności &ndash; jeśli blok &bdquo;Detale aktualności&rdquo; znajduje się na tej samej stronie co lista aktualności można ukryć listę w momencie wyświetlania detali aktualności.</p>\n\n<p>Analogicznie jak blok &bdquo;Lista aktualności&rdquo; dodajemy teraz blok &bdquo;Detale aktualności&rdquo;. Możemy go umieścić na dowolnej stronie, w ustawieniach bloku &bdquo;Lista aktualności&rdquo; wystarczy podać adres strony (tytuł strony), do kt&oacute;rej go wstawiamy, przeciągamy go w wybrany slot na bloki i klikamy na jego belce ikonę trybiku.</p>\n\n<p>Wybieramy rozmiar tytułu i czy ma być on widoczny. W ustawieniach wyświetlania można użyć opcji ukrywania tego bloku jeśli znajduje się on na tej samej stronie co blok &bdquo;Lista aktualności&rdquo;, czyli będzie on ukryty jeśli widoczna będzie lista aktualności.</p>\n\n<p>Tak jak bloki powyżej możemy wstawić na stronie pozostałe bloki czyli &bdquo;Formularz dodawania aktualności&rdquo; i &bdquo;Aktualności slider&rdquo;. Pierwszy z wymienionych blok&oacute;w wyświetli formularz dla gości, kt&oacute;rzy będą mogli dodawać aktualności. Dodane w ten spos&oacute;b aktualności trafią do listy aktualności, jednak nie będą opublikowane, dopiero po pomyślnej weryfikacji przez moderatora lub administratora strony będą widoczne. Klikamy na belce bloku formularza ikonę trybiku &ndash; ustawień.</p>\n\n<p>Ustawiamy rozmiar tytułu i czy ma on być widoczny. Dalszą opcją jest wyb&oacute;r bloku, z kt&oacute;rym chcemy skojarzyć, id bloku widoczne jest na jego belce z lewej strony.</p>\n\n<p>Ostatni blok związany z aktualnościami to &bdquo;Aktualności slider&rdquo;, wyświetla listę aktualności w postaci slidera, dodajemy go do strony jak inne bloki. Przyjrzyjmy się jego ustawieniom, klikamy na wstawionym bloku &bdquo;Aktualności slider&rdquo; ikonę trybiku, otworzy nam się formularz z ustawieniami jak poniżej.</p>\n\n<p>Na samej g&oacute;rze ustawiamy rozmiar tytułu i czy ma on być widoczny.</p>\n\n<p>1. Wyświetl aktualności z bloku &ndash; wybieramy stronę i blok z listą aktualności, kt&oacute;ra się na niej znajduje, slider będzie pobierał z niej aktualności do wyświetlania.</p>\n\n<p>2. Wyświetl aktualności z kategorii &ndash; wybieramy kategorię aktualności, będą się wyświetlać tylko aktualności z danej kategorii, domyślnie jest ustawione &bdquo;Wszystkie kategorie&rdquo;.</p>\n\n<p>3. Całkowita ilość aktualności w sliderze &ndash; wybieramy ilość aktualności do wyświetlenia.</p>\n\n<p>4. Ustawienia przewijania &ndash; ustawiamy czas pomiędzy przejściami (slajdami), czas samego przejścia oraz typ przewijania, dostępnych są 4 typy oraz brak przewijania:</p>\n\n<p>- skocz na początek po wyświetleniu ostatniego elementu</p>\n\n<p>- skocz na koniec po wyświetleniu pierwszego elementu</p>\n\n<p>- użyj pierwszej i drugiej opcji jednocześnie</p>\n\n<p>- użyj zapętlonego przewijania</p>\n\n<p><strong>2. Edycja aktualności.</strong></p>\n\n<p>Na belce bloku Listy aktualności klikamy ikonę folderu - &bdquo;okna zarządzania&rdquo;.</p>\n\n<p>Otworzy nam się panel, w kt&oacute;rym możemy edytować wybrane wydarzenie</p>\n\n<p>lub skorzystać z ikony szybkiej edycji umieszczonej przy tytule wydarzenia na liście wydarzeń.</p>\n\n<p>Przy edycji aktualności otworzy nam się panel, kt&oacute;ry posiada te same elementy jak przy dodawaniu nowej aktualności (patrz - Dodaj nową aktualność - powyżej. Po zakończeniu edycji klikamy &bdquo;Zapisz&rdquo;.</p>');
INSERT INTO `block_accordion` (`panel_id`, `content_id`, `is_published`, `order`, `last_update_author_id`, `last_update_date`, `name`, `contents`) VALUES
(12, 795, 0, 10, 1, '2016-01-04 15:55:17', 'Tworzenie i zarządzanie wydarzeniami', '<p><strong>1. Dodawanie wydarzeń.</strong></p>\n\n<p>Do tworzenia wydarzeń są zaprojektowane cztery bloki &ndash; &bdquo;Lista wydarzeń&rdquo; , &bdquo; Detale wydarzenia&rdquo;, &bdquo;Lista wydarzeń archiwalnych&rdquo; i &bdquo;Nadchodzące wydarzenia&rdquo;. Wszystkie bloki związane z wydarzeniami są ze sobą powiązane ale najpierw zacznijmy od wstawienia bloku Lista wydarzeń. Będąc zalogowanym jako administrator włączamy tryb edycji klikając myszką na przycisk z ikoną puzla w prawym dolnym rogu strony.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px" /></p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.&nbsp;</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png" /></p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony, wyszukujemy blok o nazwie &bdquo;Lista wydarzeń&rdquo;, można w tym celu użyć wyszukiwarki blok&oacute;w np. wybierając z listy &bdquo;Typ bloku&rdquo; pozycję &bdquo;Lista wydarzeń&rdquo;. Następnie chwytamy myszką ikonę zaznaczoną na zdjęciu poniżej i przeciągamy blok w wybrany na stronie slot na bloki.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_12_37_35.png" /></p>\n\n<p>Wstawiony blok Lista wydarzeń posiada pr&oacute;cz standardowych ustawień, także unikalne dla tego bloku. Na belce Listy wydarzeń klikamy ikonę folderu - &bdquo;okna zarządzania&rdquo;.</p>\n\n<p>Otworzy nam się panel, w kt&oacute;rym utworzymy naszą kategorię wydarzeń i a potem samo wydarzenie, kt&oacute;re przypiszemy do tej kategorii.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Wydarzenia.png" /></p>\n\n<p>Klikamy przycisk &bdquo;Kategorie&rdquo;.</p>\n\n<p>A następnie przycisk &bdquo;Nowa kategoria&rdquo;.</p>\n\n<p>W panelu wpisujemy tytuł dla kategorii wydarzeń (w tym przykładzie jest to &bdquo;Nowa kategoria&rdquo;), będzie potem można przypisywać wiele wydarzeń pod tą kategorię i wyświetlać wszystkie naraz w danej kategorii, możemy zaznaczyć &bdquo;opublikuj po zapisaniu&rdquo; w celu natychmiastowego opublikowania tej kategorii i kliknąć &bdquo;Zapisz&rdquo;.</p>\n\n<p>Teraz możemy już tworzyć wydarzenia. Zamykamy panel &bdquo;Kategorie - wydarzenia&rdquo; i klikamy przycisk &bdquo;Nowe wydarzenie&rdquo;. Uzupełniamy formularz dodawania nowego wydarzenia, kt&oacute;ry zawiera pola:</p>\n\n<p>1. Kategoria &ndash; wybieramy z listy kategorię, do kt&oacute;rej przypiszemy nasze wydarzenie. W naszym przykładzie jest tylko jedna kategoria, kt&oacute;rą stworzyliśmy powyżej.</p>\n\n<p>2. Tytuł &ndash; nadajemy tytuł wydarzeniu, w naszym przykładzie jest to &bdquo;Nowe wydarzenie&rdquo;</p>\n\n<p>3. Adres URL &ndash; w momencie wprowadzania tytułu ustawi się automatycznie.</p>\n\n<p>4. Etykieta &ndash; pojawi się pod tytułem jako jego dopełnienie.</p>\n\n<p>5, 6, 7. &nbsp;Czas trwania, Godzina rozpoczęcia, Godzina zakończenia &ndash; domyślnie ustawia się na tydzień od momentu dodawania wydarzenia, można ustawić dowolna datę i godzinę. W zależności od tego czy wydarzenie już minęło (ma minioną datę jego zakończenia), jest aktualne (teraźniejsza data jest pomiędzy datą rozpoczęcia i zakończenia wydarzenia) lub przyszłe (data rozpoczęcia wydarzenia jest p&oacute;źniejsza od teraźniejszej) możemy wyświetlać je z listy wydarzeń odpowiednio w blokach &bdquo;wydarzenia archiwalne&rdquo;, &bdquo;detale wydarzenia&rdquo; i &bdquo;nadchodzące wydarzenia&rdquo;</p>\n\n<p>8.&nbsp;Obraz &ndash; możemy dodać obraz do wydarzenia lub kilka obraz&oacute;w i ustawić klikając na k&oacute;łeczko w prawym g&oacute;rnym rogu loadera, kt&oacute;ry obraz ma być jako zdjęcie gł&oacute;wne wyświetlane na liście wydarzeń. Ikoną strzałki ładujemy plik obrazu, możemy też użyć edycji ikoną oł&oacute;wka.</p>\n\n<p>9.&nbsp;Treść &ndash; opisujemy tutaj wydarzenie, po zapisaniu klikając edycję tego wydarzenia dostępnych będzie więcej opcji do uzupełnienia szczeg&oacute;ł&oacute;w wydarzenia.</p>\n\n<p>10. Zajawka &ndash; zajawka będzie wyświetlana na liście wydarzeń jako skr&oacute;cona treść, można użyć do 350 znak&oacute;w, cała treść będzie dostępna po kliknięciu na wydarzenie w detalu wydarzenia, o tym w dalszej części opisu.</p>\n\n<p>11. Uwagi &ndash; nie będą nigdzie publikowane, dla własnej potrzeby możemy zostawiać stosowną notatkę.</p>\n\n<p>12. Możliwość zapisania się na wydarzenia &ndash; po wybraniu opcji &bdquo;włączone&rdquo; - &bdquo;tak&rdquo; pojawi się miejsce na wpisanie danych kontaktowych osoby odpowiedzialnej za rozmowy o wydarzeniach, adresu e-mail i telefonu. Zostanie ona poinformowana o utworzeniu wydarzenia i o zapisaniu się na to wydarzenie przez każdego zapisującego się Gościa. Pod wydarzeniem na liście wydarzeń pojawi się przycisk dla Gości, będą oni mogli zapisać się na wydarzenie, każda zapisana osoba trafi na listę dostępną w panelu administracyjnym &bdquo;LISTA WYDARZEŃ&rdquo;.</p>\n\n<p>13. Pozostałe opcje &ndash; kilka opcji dotyczących wydarzenia.</p>\n\n<p>Po zapisaniu wszystkich wprowadzonych treści w bloku lista wydarzeń pojawi się nasze nowe wydarzenie. Teraz możemy skorzystać z ustawień dostępnych na belce bloku &bdquo;Lista wydarzeń&rdquo;.</p>\n\n<p>Klikamy na ikonę trybiku.</p>\n\n<p>Pojawi nam się okno z ustawieniami, kt&oacute;rych opis jest poniżej.</p>\n\n<p>1. Rozmiar tytułu &ndash; określa wielkość tytułu listy wydarzeń i czy ma być widoczny na stronie.</p>\n\n<p>2. Ilość element&oacute;w &ndash; ustawiamy ile naraz jest widocznych element&oacute;w na stronie paginacji.</p>\n\n<p>3. Wyświetl wydarzenia z kategorii &ndash; należy wybrać kategorię, do kt&oacute;rej są przypisane wydarzenia i tylko z tej kategorii będą wyświetlane.</p>\n\n<p>4. Wyświetl detale wydarzenia na stronie &ndash; &bdquo;Detale wydarzenia&rdquo; to blok, w kt&oacute;rym pojawi się nasze wydarzenie po kliknięciu na nie w liście wydarzeń. Blok &bdquo;Detale wydarzenia&rdquo; można umieścić na innej stronie i to na nią będzie nas kierowało po kliknięciu na wydarzenie z listy, można też umieścić detale wydarzenia na tej samej stronie.</p>\n\n<p>5. Ukrywanie listy wydarzeń &ndash; jeśli blok &bdquo;Detale wydarzenia&rdquo; znajduje się na tej samej stronie co lista wydarzeń można ukryć listę w momencie wyświetlania detali wydarzenia z listy wydarzeń.</p>\n\n<p style="margin-left:1.88cm">Można pozwolić gościom lub zarejestrowanym użytkownikom na dodawanie nowych wydarzeń, po zaznaczeniu drugiej opcji pojawi się przycisk (jeśli blok posiada przynajmniej jedną kategorię), kt&oacute;ry im to umożliwi, będzie on widoczny zar&oacute;wno na liście wydarzeń jak i w detalach wydarzenia. Każde nowo dodane wydarzenie musi zostać zaakceptowane przez moderatora lub administratora zanim stanie się opublikowane. W momencie zaznaczenia opcji pojawi się miejsce na wpisanie ustawień dla maila informującego o zatwierdzeniu wydarzenia i dane kontaktowe osoby odpowiedzialnej za rozmowy o wydarzeniach. Można też zredagować treść maili z informacjami o dodaniu wydarzenia przez gościa jak i odpowiedzi zwrotnej do niego.</p>\n\n<p style="margin-left:1.88cm">Ostatnia opcja umożliwi gościom dodawanie zdjęć w formularzu dodawania nowego wydarzenia i przesyłanie ich na serwer, nie jest to zalecane ze względu na bezpieczeństwo.</p>\n\n<p>Analogicznie jak blok &bdquo;Lista wydarzeń&rdquo; dodajemy teraz blok &bdquo;Detale wydarzenia&rdquo;. Możemy go umieścić na dowolnej stronie, w ustawieniach bloku &bdquo;Lista wydarzeń&rdquo; wystarczy podać adres strony, do kt&oacute;rej go wstawiamy, przeciągamy go w wybrany slot na bloki i klikamy na jego belce ikonę trybiku.</p>\n\n<p>Wybieramy rozmiar tytułu i czy ma być on widoczny. W ustawieniach wyświetlania można użyć opcji ukrywania tego bloku jeśli znajduje się on na tej samej stronie co blok &bdquo;Lista wydarzeń&rdquo;, czyli będzie on ukryty jeśli widoczna będzie lista wydarzeń.</p>\n\n<p>Tak jak bloki powyżej możemy wstawić na stronie pozostałe bloki czyli &bdquo;Nadchodzące wydarzenia&rdquo; i &bdquo;Wydarzenia archiwalne&rdquo;. W pierwszym z wymienionych blok&oacute;w będą się pojawiały wydarzenia z przyszła datą, w drugim wydarzenia minione. W ustawieniach tych blok&oacute;w są takie same elementy do zmiany ustawień tych blok&oacute;w.</p>\n\n<p>1. Rozmiar tytułu &ndash; wybieramy rozmiar tytułu i czy ma on być widoczny.</p>\n\n<p>2. Ilość element&oacute;w &ndash; ustawiamy ile naraz jest widocznych element&oacute;w na stronie paginacji.</p>\n\n<p>3. Wyświetl archiwalne wydarzenia z bloku &ndash; wybieramy z listy stronę i na niej blok, z kt&oacute;rego będą wyświetlane wydarzenia.</p>\n\n<p>4. Wyświetl archiwalne/nadchodzące wydarzenia z kategorii &ndash; wybieramy z listy kategorię wydarzeń lub zostawiamy domyślnie &ndash; wszystkie kategorie.</p>\n\n<p style="margin-left:1.27cm">Tak jak w bloku lista wydarzeń tak i w tych dw&oacute;ch blokach (lista wydarzeń archiwalnych i nadchodzące wydarzenia) możemy je ukryć jeśli na stronie znajdują się detale wydarzenia zaznaczając opcje &bdquo;Ukryj listę wydarzeń jeśli aktualny adres URL prowadzi do detali wydarzenia&rdquo;.</p>\n\n<p><strong>2. Edycja wydarzeń.</strong></p>\n\n<p>Na belce bloku Listy wydarzeń klikamy ikonę folderu - &bdquo;okna zarządzania&rdquo;. Analogicznie edytujemy wydarzenia z &bdquo;Listy wydarzeń archiwalnych&rdquo; i &bdquo;Nadchodzących wydarzeń&rdquo;.Otworzy nam się panel, w kt&oacute;rym możemy edytować wybrane wydarzenie, lub skorzystać z ikony szybkiej edycji umieszczonej przy tytule wydarzenia na liście wydarzeń.</p>\n\n<p>Przy edycji wydarzenia otworzy nam się panel, kt&oacute;ry posiada te same elementy jak przy dodawaniu nowego wydarzenia (patrz - Dodaj nowe wydarzenie - powyżej) powiększony o bardziej szczeg&oacute;łowe elementy typu: Gdzie, Wstęp, Opis, Kontakt, Dodatkowe Informacje. Po zakończeniu edycji klikamy &bdquo;Zapisz&rdquo;.</p>'),
(13, 795, 0, 11, 1, '2016-01-04 15:37:12', 'Blok Zakładki', '<p>Blok Zakładki jest prosty w zarządzaniu.&nbsp;</p>\n\n<p>Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną puzla.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px"></p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi bloków, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png"></p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, które można użyć do tworzenia strony, wyszukujemy blok o nazwie „Zakładki”, można w tym celu użyć wyszukiwarki bloków np. wybierając z listy „Typ bloku” pozycję „Zakładki”. Następnie chwytamy myszką ikonę strzałki skierowanej ku górze (pojawi się po najechaniu myszką na blok Zakładek) i przeciągamy blok w wybrany na stronie slot na bloki.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_19_33.png"></p>\n\n<p>Wprowadzamy treść w jednej zakładce, jeśli chcemy dodajemy kolejną zakładkę klikając ikonę plusa przy zakładce.</p>'),
(14, 795, 0, 12, 1, '2016-01-04 16:01:42', 'Galeria obrazów', '<p><strong>Dodawanie i edycja grafiki &ndash; galeria obraz&oacute;w.</strong></p>\n\n<p>Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną puzla.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px" /></p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png" /></p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony. Do dodawania grafiki do strony przygotowanych jest kilka blok&oacute;w, wstawmy pierwszy z nich &ndash; kolejne będą wstawiane analogicznie. Wyszukujemy blok o nazwie &bdquo;Galeria obraz&oacute;w&rdquo;, można w tym celu użyć wyszukiwarki blok&oacute;w np. wybierając z listy &bdquo;Typ bloku&rdquo; pozycję &bdquo;Galeria obraz&oacute;w&rdquo;. Następnie chwytamy myszką ikonę zaznaczoną na zdjęciu poniżej i przeciągamy blok w wybrany na stronie slot na bloki.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_19_33.png" /></p>\n\n<p>Przechodzimy do ustawień klikając na belce bloku ikonę trybiku.</p>\n\n<p>Otworzy nam się okno z ustawieniami dla tego bloku.</p>\n\n<p>Ustawiamy rozmiar tytułu naszej galerii oraz czy ma on być widoczny a w ustawieniach wyświetlania decydujemy ile obraz&oacute;w ma być widocznych na stronie. &bdquo;0&rdquo; oznacza brak limitu. Wartość dodatnia włączy limit widocznych obraz&oacute;w i kiedy zostanie on osiągnięty zostanie wyświetlony przycisk &bdquo;Pokaż / ukryj więcej obraz&oacute;w&rdquo;.</p>\n\n<p>Aby dodać zdjęcia do galerii klikamy na belce bloku ikonę folderu &ndash; okno zarządzania.</p>\n\n<p>Następnie niebieski przycisk na otwartym oknie &bdquo;Nowy obrazek&rdquo;.</p>\n\n<p>W formularzu dodawania obrazka wypełniamy tytuł i autora a następnie dodajemy obrazek.</p>\n\n<p>Po dodaniu obrazka klikamy &bdquo;Zapisz&rdquo;, możemy dodawać kolejne obrazki</p>'),
(15, 795, 0, 13, 1, '2016-01-04 15:59:25', 'Stos obrazów', '<p><strong>Dodawanie i edycja grafiki &ndash; stos obraz&oacute;w.</strong></p>\n\n<p>Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną puzla.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px" /></p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png" /></p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony. Do dodawania grafiki do strony przygotowanych jest kilka blok&oacute;w, wstawmy pierwszy z nich &ndash; kolejne będą wstawiane analogicznie. Wyszukujemy blok o nazwie &bdquo;Galeria obraz&oacute;w&rdquo;, można w tym celu użyć wyszukiwarki blok&oacute;w np. wybierając z listy &bdquo;Typ bloku&rdquo; pozycję &bdquo;Galeria obraz&oacute;w&rdquo;. Następnie chwytamy myszką ikonę zaznaczoną na zdjęciu poniżej i przeciągamy blok w wybrany na stronie slot na bloki.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_19_33.png" /></p>\n\n<p>Przechodzimy do ustawień klikając na belce bloku ikonę trybiku. Otworzy się panel opisany poniżej.</p>\n\n<p>1. Tytuł &ndash; ustawiamy rozmiar tytułu oraz czy ma on być widoczny.</p>\n\n<p>2. Podstawowe ustawienia &ndash; ustawiamy kolor tła i czy ma być włączone, odstęp pomiędzy elementami stosu, możemy zaznaczyć żeby przy załadowaniu bloku otwierała się pierwsza dostępna grupa obraz&oacute;w, ustawiamy kąty obrotu dla bardziej realistycznego efektu stosu, możemy wybrać losowe kąty.</p>\n\n<p>3. Ustawienia animacji dla klikniętego stosu &ndash; ustawiamy prędkości otwierania i zamykania stos&oacute;w oraz ich płynności, dostępnych jest 5:</p>\n\n<p>- linear</p>\n\n<p>- easy -in</p>\n\n<p>- easy-out</p>\n\n<p>- easy-in-out</p>\n\n<p>4. Ustawienia animacji dla pozostałych stos&oacute;w &ndash; jak w powyższym punkcie.</p>\n\n<p>Dalszą czynnością jest dodanie obraz&oacute;w do naszego bloku stos obraz&oacute;w. Klikamy na belce ikonę folderu &ndash; okno zarządzania. W otwartym oknie klikamy niebieski przycisk &bdquo;Zarządzaj grupami&rdquo; a następnie w otwartym oknie kolejny przycisk &bdquo;Nowa grupa&rdquo;. Wpisujemy tytuł dla grupy i klikamy &bdquo;Zapisz&rdquo;. Zamykamy okno &bdquo;Grupy &ndash; zarządzanie&rdquo; i teraz możemy dodać element stosu klikając na niebieski przycisk &bdquo;Nowy element&rdquo;. W oknie dodawania nowego elementu zobaczymy poniższy formularz.</p>\n\n<p>1. Grupa &ndash; wybieramy grupę obraz&oacute;w, kt&oacute;re znajdą się w stosie.</p>\n\n<p>2. Typ adresu URL &ndash; możemy określić czy dany obrazek ma nam służyć jako link, z listy wyboru wybieramy adres wewnętrzny, zewnętrzny lub brak.</p>\n\n<p>3. Adres URL &ndash; jeśli w typie adresu URL wybierzemy adres wewnętrzny, na liście Adresu URL będą dostępne wszystkie nasze strony i do wybranej przez nas będzie kierował link po kliknięciu na obrazek.</p>\n\n<p>4. Obraz &ndash; dodajemy obrazek jak w przypadku galerii (patrz - wyżej).</p>\n\n<p>5. Tytuł &ndash; ustawiamy tytuł dla obrazka, będzie on widoczny po najechaniu myszką na obrazek w &bdquo;dymku&rdquo; .</p>\n\n<p>Po dodaniu obrazka jako elementu stosu można dodać ich więcej tworząc większy stos obraz&oacute;w, a tworząc więcej grup więcej stos&oacute;w.</p>'),
(16, 795, 0, 14, 1, '2016-01-04 16:01:14', 'Slider obrazów', '<p><strong>Dodawanie i edycja grafiki &ndash; slider obraz&oacute;w.</strong></p>\n\n<p>Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną puzla.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px" /></p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png" /></p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony. Do dodawania grafiki do strony przygotowanych jest kilka blok&oacute;w, wstawmy pierwszy z nich &ndash; kolejne będą wstawiane analogicznie. Wyszukujemy blok o nazwie &bdquo;Galeria obraz&oacute;w&rdquo;, można w tym celu użyć wyszukiwarki blok&oacute;w np. wybierając z listy &bdquo;Typ bloku&rdquo; pozycję &bdquo;Galeria obraz&oacute;w&rdquo;. Następnie chwytamy myszką ikonę zaznaczoną na zdjęciu poniżej i przeciągamy blok w wybrany na stronie slot na bloki.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_19_33.png" /></p>\n\n<p>Przechodzimy do ustawień klikając na belce bloku ikonę trybiku.&nbsp;Otworzy nam się okno opisane poniżej.</p>\n\n<p>1. Tytuł &ndash; ustawiamy rozmiar tytułu oraz czy ma on być widoczny.</p>\n\n<p>2. Ustawienia wyświetlania &ndash; ustawiamy rozmiar miniatury slidera w pikselach, następnie po zaznaczeniu opcji &bdquo;Pokaż obraz w kolorze tylko, gdy kursor myszy znajduje się nad nim&rdquo;, nasze obrazy na slajdach będą o odcieniach skali szarości, najeżdżając myszką na obraz zmieni się na kolorowy.</p>\n\n<p>3. Ustawienia przewijania - &ndash; ustawiamy czas pomiędzy przejściami (slajdami), czas samego przejścia oraz typ przewijania, dostępnych są 4 typy oraz brak przewijania:</p>\n\n<p>- skocz na początek po wyświetleniu ostatniego elementu</p>\n\n<p>- skocz na koniec po wyświetleniu pierwszego elementu</p>\n\n<p>- użyj pierwszej i drugiej opcji jednocześnie</p>\n\n<p>- użyj zapętlonego przewijania</p>\n\n<p>Dalszą czynnością jest dodanie obraz&oacute;w do naszego bloku slidera. Klikamy na belce ikonę folderu &ndash; okno zarządzania. W otwartym oknie klikamy niebieski przycisk &bdquo;Nowy obrazek&rdquo; i ustawiamy dostępne opcje.</p>\n\n<p>1. Typ adresu URL &ndash; możemy określić czy dany obrazek ma nam służyć jako link, z listy wyboru wybieramy adres wewnętrzny, zewnętrzny lub brak.</p>\n\n<p>2. Adres URL &ndash; jeśli w typie adresu URL wybierzemy adres wewnętrzny, na liście Adresu URL będą dostępne wszystkie nasze strony i do wybranej przez nas będzie kierował link po kliknięciu na obrazek.</p>\n\n<p>3. Obraz &ndash; dodajemy obrazek jak w przypadku galerii (patrz - wyżej).</p>\n\n<p>4. Tytuł &ndash; ustawiamy tytuł dla obrazka, będzie on widoczny po najechaniu myszką na obrazek w &bdquo;dymku&rdquo; .</p>\n\n<p>Po dodaniu obrazka można dodać ich więcej tworząc własny przewijany slider obraz&oacute;w.</p>'),
(17, 795, 0, 15, 1, '2016-01-04 16:04:29', 'Mały baner', '<p><strong>Dodawanie i edycja grafiki &ndash; Small baner.</strong></p>\n\n<p>Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną puzla.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px" /></p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png" /></p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony. Do dodawania grafiki do strony przygotowanych jest kilka blok&oacute;w, wstawmy pierwszy z nich &ndash; kolejne będą wstawiane analogicznie. Wyszukujemy blok o nazwie &bdquo;Galeria obraz&oacute;w&rdquo;, można w tym celu użyć wyszukiwarki blok&oacute;w np. wybierając z listy &bdquo;Typ bloku&rdquo; pozycję &bdquo;Galeria obraz&oacute;w&rdquo;. Następnie chwytamy myszką ikonę zaznaczoną na zdjęciu poniżej i przeciągamy blok w wybrany na stronie slot na bloki.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_19_33.png" /></p>\n\n<p>Przechodzimy do ustawień klikając na belce bloku ikonę trybiku. Otworzy się panel opisany poniżej:</p>\n\n<p>1. Tytuł &ndash; ustawiamy rozmiar tytułu oraz czy ma on być widoczny.</p>\n\n<p>2. Ustawienia wyświetlania &ndash; ustawiamy czas trwania przejścia miedzy slajdami. W typie slajd&oacute;w mamy do wyboru 16 r&oacute;żnych przejść, w Slider Theme 4 sk&oacute;rki (default, light, dark i bar), a w ostatniej liście wyb&oacute;r czy mają być widoczne strzałki do przewijania obraz&oacute;w banneru.</p>\n\n<p>Dalszą czynnością jest dodanie obraz&oacute;w do naszego bloku Small banner. Klikamy na belce ikonę folderu &ndash; okno zarządzania. W otwartym oknie klikamy niebieski przycisk &bdquo;New Baner. W oknie dodawania nowego banneru zobaczymy poniższy formularz.</p>\n\n<p>1. Tytuł &ndash; należy wypełnić pole tytuł, można zostawić domyślny tytuł, nie będzie on widoczny na stronie, identyfikuje on baner na liście baner&oacute;w w oknie zarządzania.</p>\n\n<p>2. Obraz &ndash; dodajemy obrazek jak w przypadku galerii (patrz - wyżej).</p>\n\n<p>3. Treść &ndash; treść będzie widoczna na banerze, jeśli zostawimy pustą treść &ndash; będzie wyświetlany sam obraz .</p>\n\n<p>Do edytowania wszystkich obraz&oacute;w we wszystkich wymienionych wyżej blokach służy ikona edycji w oknie zarządzania tych blok&oacute;w, zdjęcie przedstawia edycję w bloku &bdquo;Duży banner&rdquo; ale analogicznie ta ikona znajduje się w pozostałych blokach.</p>'),
(18, 795, 0, 16, 1, '2016-01-04 16:05:50', 'Duży baner', '<p><strong>Dodawanie i edycja grafiki &ndash; duży baner.</strong></p>\n\n<p>Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną puzla.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px" /></p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png" /></p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony. Do dodawania grafiki do strony przygotowanych jest kilka blok&oacute;w, wstawmy pierwszy z nich &ndash; kolejne będą wstawiane analogicznie. Wyszukujemy blok o nazwie &bdquo;Galeria obraz&oacute;w&rdquo;, można w tym celu użyć wyszukiwarki blok&oacute;w np. wybierając z listy &bdquo;Typ bloku&rdquo; pozycję &bdquo;Galeria obraz&oacute;w&rdquo;. Następnie chwytamy myszką ikonę zaznaczoną na zdjęciu poniżej i przeciągamy blok w wybrany na stronie slot na bloki.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_19_33.png" /></p>\n\n<p>Przechodzimy do ustawień klikając na belce bloku ikonę trybiku. Otworzy się panel opisany poniżej:</p>\n\n<p>1. Tytuł &ndash; ustawiamy rozmiar tytułu oraz czy ma on być widoczny.</p>\n\n<p>2. Ustawienia wyświetlania &ndash; ustawiamy czas trwania animacji i czas przejścia miedzy slajdami. W typie slajd&oacute;w mamy do wyboru:</p>\n\n<p>- none</p>\n\n<p>- fade</p>\n\n<p>- fadeout</p>\n\n<p>- scrollHorz</p>\n\n<p>Dalszą czynnością jest dodanie obraz&oacute;w do naszego bloku Duży baner. Klikamy na belce ikonę folderu &ndash; okno zarządzania. W otwartym oknie klikamy niebieski przycisk &bdquo;New Baner. W oknie dodawania nowego baneru zobaczymy poniższy formularz.</p>\n\n<p>1. Tytuł &ndash; należy wypełnić pole tytuł i zaznaczyć czy ma on być widoczny nad obrazem.</p>\n\n<p>2. Typ adresu URL &ndash; możemy określić czy dany obrazek ma nam służyć jako link, z listy wyboru wybieramy adres wewnętrzny, zewnętrzny lub brak.</p>\n\n<p>3. Adres URL &ndash; jeśli w typie adresu URL wybierzemy adres wewnętrzny, na liście Adresu URL będą dostępne wszystkie nasze strony i do wybranej przez nas będzie kierował link po kliknięciu na tekst &bdquo;Czytaj więcej&rdquo;.</p>\n\n<p>4. Obraz &ndash; dodajemy obrazek jak w przypadku galerii (patrz - wyżej).</p>\n\n<p>5. Treść &ndash; treść będzie widoczna na banerze, można zaznaczyć czy ma być wyświetlana .</p>\n\n<p>Można dodawać więcej obraz&oacute;w wedle uznania, najlepszym slotem do umieszczania bloku Duży banner jest najszerszy dostępny, sięgający od brzegu do brzegu ekranu.</p>');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_bachelor_studies`
--

CREATE TABLE `block_bachelor_studies` (
  `content_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `time` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `degree_type` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `language` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_bachelor_studies_tabs`
--

CREATE TABLE `block_bachelor_studies_tabs` (
  `tab_id` smallint(5) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED DEFAULT NULL,
  `order` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `name` varchar(128) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `contents` text COLLATE utf8_polish_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_banner_small`
--

CREATE TABLE `block_banner_small` (
  `banner_id` smallint(5) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED DEFAULT NULL,
  `attachment_id` mediumint(8) UNSIGNED DEFAULT NULL COMMENT 'Image.',
  `is_published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `order` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `title` varchar(128) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `contents` text COLLATE utf8_polish_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `block_banner_small`
--

INSERT INTO `block_banner_small` (`banner_id`, `content_id`, `attachment_id`, `is_published`, `order`, `last_update_author_id`, `last_update_date`, `title`, `contents`) VALUES
(1, 782, 796, 1, 0, 1, '2015-12-09 12:57:51', 'Pierwszy baner', '<p>Przykładowy tekst do małego baneru.</p>');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_big_banner`
--

CREATE TABLE `block_big_banner` (
  `banner_id` smallint(5) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED DEFAULT NULL,
  `page_id` smallint(5) UNSIGNED DEFAULT NULL,
  `attachment_id` mediumint(8) UNSIGNED DEFAULT NULL COMMENT 'Image.',
  `is_published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_title_visible` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_contents_visible` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `order` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `url_address_type` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `external_url` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `clean_url` varchar(128) COLLATE utf8_polish_ci DEFAULT '',
  `title` varchar(128) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `contents` text COLLATE utf8_polish_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `block_big_banner`
--

INSERT INTO `block_big_banner` (`banner_id`, `content_id`, `page_id`, `attachment_id`, `is_published`, `is_title_visible`, `is_contents_visible`, `order`, `url_address_type`, `last_update_author_id`, `last_update_date`, `external_url`, `clean_url`, `title`, `contents`) VALUES
(1, 766, 248, 768, 1, 0, 0, 0, 0, 1, '2015-12-09 14:23:49', '', '', 'CodiLab', ''),
(2, 766, 248, 822, 1, 0, 0, 0, 0, 1, '2015-12-09 14:24:19', '', '', 'Drugi baner', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_employees_degrees`
--

CREATE TABLE `block_employees_degrees` (
  `degree_id` smallint(5) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED DEFAULT NULL,
  `is_published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `title` varchar(128) COLLATE utf8_polish_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `block_employees_degrees`
--

INSERT INTO `block_employees_degrees` (`degree_id`, `content_id`, `is_published`, `last_update_author_id`, `last_update_date`, `title`) VALUES
(1, 799, 1, 1, '2016-01-05 16:02:09', 'Frontend Developer'),
(2, 799, 1, 1, '2016-01-08 10:48:27', 'Grafik, projektant stron internetowych');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_employees_employees`
--

CREATE TABLE `block_employees_employees` (
  `employee_id` mediumint(10) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED DEFAULT NULL,
  `degree_id` smallint(5) UNSIGNED DEFAULT NULL,
  `attachment_id` mediumint(8) UNSIGNED DEFAULT NULL COMMENT 'Image.',
  `is_published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `name_and_surname` varchar(128) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `position` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `about` text COLLATE utf8_polish_ci NOT NULL,
  `scientific_career` mediumtext COLLATE utf8_polish_ci NOT NULL,
  `most_important_publications` mediumtext COLLATE utf8_polish_ci NOT NULL,
  `contact_info` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `block_employees_employees`
--

INSERT INTO `block_employees_employees` (`employee_id`, `content_id`, `degree_id`, `attachment_id`, `is_published`, `last_update_author_id`, `last_update_date`, `name_and_surname`, `position`, `about`, `scientific_career`, `most_important_publications`, `contact_info`) VALUES
(1, 799, 1, 892, 1, 1, '2016-01-08 11:00:55', 'Norbert Banulski', '', '<p>Absolwent Wyższej Szkoły Menedżerskiej w Warszawie, interesuje się kodowaniem stron internetowych oraz web design&#39;em.</p>', '<p>2005 tytuł zawodowy technika ekonomisty.</p>\n\n<p>2008 licencjat na kierunku Informatyki w specjalności Grafika i programowanie aplikacji internetowych.</p>\n\n<p>2011 magister na kierunku Zarządzanie w specjalności Rachunkowość i audyt.</p>\n\n<p>od 2014 praca w Codilab.</p>', '<p>ciągły rozw&oacute;j zawodowy:)</p>', '<p>nr tel.: 504 564 377</p>\n\n<p>email: norbert.banulski@codilab.pl</p>'),
(2, 799, 2, 896, 1, 1, '2016-01-08 10:51:49', 'Wioleta Przybysz', '', '<p><span style="background-color:rgb(249, 249, 249); color:rgb(44, 45, 48); font-family:slack-lato,applelogo,sans-serif; font-size:15px">Absolwentka Instytutu Edukacji Artystycznej w sztukach plastycznych  na Akademii Pedagogiki Specjalnej im. Marii Grzegorzewskiej w Warszawie.&nbsp;</span></p>', '<p><span style="background-color:rgb(249, 249, 249); color:rgb(44, 45, 48); font-family:slack-lato,applelogo,sans-serif; font-size:15px">W 2005-2007 uzyskała tytuł zawodowy plastyk ze specjalności - projektowanie przestrzenne  w zakresie architektury wnętrz w Studium Sztuk plastycznych i Technik wizualnych &quot;Proartes&quot; w Warszawie.&nbsp;</span><br />\n<span style="background-color:rgb(249, 249, 249); color:rgb(44, 45, 48); font-family:slack-lato,applelogo,sans-serif; font-size:15px">W 2013 roku obroniła dyplom licencjacki z malarstwa u Profesora Grzegorza Mroczkowskiego. W 2015 roku obroniła dyplom u Profesora Wiesława Bieńkuńskiego z grafiki warsztatowej.&nbsp;</span></p>', '<p><span style="background-color:rgb(249, 249, 249); color:rgb(44, 45, 48); font-family:slack-lato,applelogo,sans-serif; font-size:15px">Interesuje się grafiką projektową, typografią, fotografią, malarstwem. Wystawia swoje prace artystyczne w galeriach w Warszawie i Legionowie.</span></p>', '<p>wioleta.przybysz@codilab.pl</p>');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_event_list`
--

CREATE TABLE `block_event_list` (
  `event_id` int(10) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED DEFAULT NULL,
  `category_id` smallint(6) DEFAULT NULL,
  `attachment_id` mediumint(8) UNSIGNED DEFAULT NULL COMMENT 'Image.',
  `is_published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_promoted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_approved` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_signing_up_enabled` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `approval_date` datetime DEFAULT NULL,
  `event_date_start` datetime DEFAULT NULL,
  `event_date_end` datetime DEFAULT NULL,
  `author_email` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `contact_person_email` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `contact_person_phone` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `clean_url` varchar(128) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `title` varchar(128) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `label` varchar(32) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `teaser` varchar(350) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `contents` text COLLATE utf8_polish_ci NOT NULL,
  `additional_contact_data` text COLLATE utf8_polish_ci NOT NULL,
  `attentions` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `block_event_list`
--

INSERT INTO `block_event_list` (`event_id`, `content_id`, `category_id`, `attachment_id`, `is_published`, `is_promoted`, `is_approved`, `is_signing_up_enabled`, `last_update_author_id`, `last_update_date`, `creation_date`, `approval_date`, `event_date_start`, `event_date_end`, `author_email`, `contact_person_email`, `contact_person_phone`, `clean_url`, `title`, `label`, `teaser`, `contents`, `additional_contact_data`, `attentions`) VALUES
(9, 764, 6, NULL, 1, 0, 1, 0, 1, '2016-01-04 15:55:00', '2015-12-08 11:39:29', NULL, '2015-12-08 11:30:00', '2015-12-15 11:30:00', '', '', '', 'tworzenie-wydarzen', 'Tworzenie wydarzeń', '', 'Do tworzenia wydarzeń są zaprojektowane cztery bloki – „Lista wydarzeń” , „ Detale wydarzenia”, „Lista wydarzeń archiwalnych” i „Nadchodzące wydarzenia”. Wszystkie bloki związane z wydarzeniami są ze sobą powiązane ale najpierw zacznijmy od wstawienia bloku Lista wydarzeń. Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną t', '<p><strong>1. Dodawanie wydarzeń.</strong></p>\n\n<p>Do tworzenia wydarzeń są zaprojektowane cztery bloki &ndash; &bdquo;Lista wydarzeń&rdquo; , &bdquo; Detale wydarzenia&rdquo;, &bdquo;Lista wydarzeń archiwalnych&rdquo; i &bdquo;Nadchodzące wydarzenia&rdquo;. Wszystkie bloki związane z wydarzeniami są ze sobą powiązane ale najpierw zacznijmy od wstawienia bloku Lista wydarzeń. Będąc zalogowanym jako administrator włączamy tryb edycji klikając myszką na przycisk z ikoną puzla w prawym dolnym rogu strony.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px" /></p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.&nbsp;</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png" /></p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony, wyszukujemy blok o nazwie &bdquo;Lista wydarzeń&rdquo;, można w tym celu użyć wyszukiwarki blok&oacute;w np. wybierając z listy &bdquo;Typ bloku&rdquo; pozycję &bdquo;Lista wydarzeń&rdquo;. Następnie chwytamy myszką ikonę zaznaczoną na zdjęciu poniżej i przeciągamy blok w wybrany na stronie slot na bloki.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_12_37_35.png" /></p>\n\n<p>Wstawiony blok Lista wydarzeń posiada pr&oacute;cz standardowych ustawień, także unikalne dla tego bloku. Na belce Listy wydarzeń klikamy ikonę folderu - &bdquo;okna zarządzania&rdquo;.</p>\n\n<p>Otworzy nam się panel, w kt&oacute;rym utworzymy naszą kategorię wydarzeń i a potem samo wydarzenie, kt&oacute;re przypiszemy do tej kategorii.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Wydarzenia.png" /></p>\n\n<p>Klikamy przycisk &bdquo;Kategorie&rdquo;.</p>\n\n<p>A następnie przycisk &bdquo;Nowa kategoria&rdquo;.</p>\n\n<p>W panelu wpisujemy tytuł dla kategorii wydarzeń (w tym przykładzie jest to &bdquo;Nowa kategoria&rdquo;), będzie potem można przypisywać wiele wydarzeń pod tą kategorię i wyświetlać wszystkie naraz w danej kategorii, możemy zaznaczyć &bdquo;opublikuj po zapisaniu&rdquo; w celu natychmiastowego opublikowania tej kategorii i kliknąć &bdquo;Zapisz&rdquo;.</p>\n\n<p>Teraz możemy już tworzyć wydarzenia. Zamykamy panel &bdquo;Kategorie - wydarzenia&rdquo; i klikamy przycisk &bdquo;Nowe wydarzenie&rdquo;. Uzupełniamy formularz dodawania nowego wydarzenia, kt&oacute;ry zawiera pola:</p>\n\n<p>1. Kategoria &ndash; wybieramy z listy kategorię, do kt&oacute;rej przypiszemy nasze wydarzenie. W naszym przykładzie jest tylko jedna kategoria, kt&oacute;rą stworzyliśmy powyżej.</p>\n\n<p>2. Tytuł &ndash; nadajemy tytuł wydarzeniu, w naszym przykładzie jest to &bdquo;Nowe wydarzenie&rdquo;</p>\n\n<p>3. Adres URL &ndash; w momencie wprowadzania tytułu ustawi się automatycznie.</p>\n\n<p>4. Etykieta &ndash; pojawi się pod tytułem jako jego dopełnienie.</p>\n\n<p>5, 6, 7. &nbsp;Czas trwania, Godzina rozpoczęcia, Godzina zakończenia &ndash; domyślnie ustawia się na tydzień od momentu dodawania wydarzenia, można ustawić dowolna datę i godzinę. W zależności od tego czy wydarzenie już minęło (ma minioną datę jego zakończenia), jest aktualne (teraźniejsza data jest pomiędzy datą rozpoczęcia i zakończenia wydarzenia) lub przyszłe (data rozpoczęcia wydarzenia jest p&oacute;źniejsza od teraźniejszej) możemy wyświetlać je z listy wydarzeń odpowiednio w blokach &bdquo;wydarzenia archiwalne&rdquo;, &bdquo;detale wydarzenia&rdquo; i &bdquo;nadchodzące wydarzenia&rdquo;</p>\n\n<p>8.&nbsp;Obraz &ndash; możemy dodać obraz do wydarzenia lub kilka obraz&oacute;w i ustawić klikając na k&oacute;łeczko w prawym g&oacute;rnym rogu loadera, kt&oacute;ry obraz ma być jako zdjęcie gł&oacute;wne wyświetlane na liście wydarzeń. Ikoną strzałki ładujemy plik obrazu, możemy też użyć edycji ikoną oł&oacute;wka.</p>\n\n<p>9.&nbsp;Treść &ndash; opisujemy tutaj wydarzenie, po zapisaniu klikając edycję tego wydarzenia dostępnych będzie więcej opcji do uzupełnienia szczeg&oacute;ł&oacute;w wydarzenia.</p>\n\n<p>10. Zajawka &ndash; zajawka będzie wyświetlana na liście wydarzeń jako skr&oacute;cona treść, można użyć do 350 znak&oacute;w, cała treść będzie dostępna po kliknięciu na wydarzenie w detalu wydarzenia, o tym w dalszej części opisu.</p>\n\n<p>11. Uwagi &ndash; nie będą nigdzie publikowane, dla własnej potrzeby możemy zostawiać stosowną notatkę.</p>\n\n<p>12. Możliwość zapisania się na wydarzenia &ndash; po wybraniu opcji &bdquo;włączone&rdquo; - &bdquo;tak&rdquo; pojawi się miejsce na wpisanie danych kontaktowych osoby odpowiedzialnej za rozmowy o wydarzeniach, adresu e-mail i telefonu. Zostanie ona poinformowana o utworzeniu wydarzenia i o zapisaniu się na to wydarzenie przez każdego zapisującego się Gościa. Pod wydarzeniem na liście wydarzeń pojawi się przycisk dla Gości, będą oni mogli zapisać się na wydarzenie, każda zapisana osoba trafi na listę dostępną w panelu administracyjnym &bdquo;LISTA WYDARZEŃ&rdquo;.</p>\n\n<p>13. Pozostałe opcje &ndash; kilka opcji dotyczących wydarzenia.</p>\n\n<p>Po zapisaniu wszystkich wprowadzonych treści w bloku lista wydarzeń pojawi się nasze nowe wydarzenie. Teraz możemy skorzystać z ustawień dostępnych na belce bloku &bdquo;Lista wydarzeń&rdquo;.</p>\n\n<p>Klikamy na ikonę trybiku.</p>\n\n<p>Pojawi nam się okno z ustawieniami, kt&oacute;rych opis jest poniżej.</p>\n\n<p>1. Rozmiar tytułu &ndash; określa wielkość tytułu listy wydarzeń i czy ma być widoczny na stronie.</p>\n\n<p>2. Ilość element&oacute;w &ndash; ustawiamy ile naraz jest widocznych element&oacute;w na stronie paginacji.</p>\n\n<p>3. Wyświetl wydarzenia z kategorii &ndash; należy wybrać kategorię, do kt&oacute;rej są przypisane wydarzenia i tylko z tej kategorii będą wyświetlane.</p>\n\n<p>4. Wyświetl detale wydarzenia na stronie &ndash; &bdquo;Detale wydarzenia&rdquo; to blok, w kt&oacute;rym pojawi się nasze wydarzenie po kliknięciu na nie w liście wydarzeń. Blok &bdquo;Detale wydarzenia&rdquo; można umieścić na innej stronie i to na nią będzie nas kierowało po kliknięciu na wydarzenie z listy, można też umieścić detale wydarzenia na tej samej stronie.</p>\n\n<p>5. Ukrywanie listy wydarzeń &ndash; jeśli blok &bdquo;Detale wydarzenia&rdquo; znajduje się na tej samej stronie co lista wydarzeń można ukryć listę w momencie wyświetlania detali wydarzenia z listy wydarzeń.</p>\n\n<p style="margin-left:1.88cm">Można pozwolić gościom lub zarejestrowanym użytkownikom na dodawanie nowych wydarzeń, po zaznaczeniu drugiej opcji pojawi się przycisk (jeśli blok posiada przynajmniej jedną kategorię), kt&oacute;ry im to umożliwi, będzie on widoczny zar&oacute;wno na liście wydarzeń jak i w detalach wydarzenia. Każde nowo dodane wydarzenie musi zostać zaakceptowane przez moderatora lub administratora zanim stanie się opublikowane. W momencie zaznaczenia opcji pojawi się miejsce na wpisanie ustawień dla maila informującego o zatwierdzeniu wydarzenia i dane kontaktowe osoby odpowiedzialnej za rozmowy o wydarzeniach. Można też zredagować treść maili z informacjami o dodaniu wydarzenia przez gościa jak i odpowiedzi zwrotnej do niego.</p>\n\n<p style="margin-left:1.88cm">Ostatnia opcja umożliwi gościom dodawanie zdjęć w formularzu dodawania nowego wydarzenia i przesyłanie ich na serwer, nie jest to zalecane ze względu na bezpieczeństwo.</p>\n\n<p>Analogicznie jak blok &bdquo;Lista wydarzeń&rdquo; dodajemy teraz blok &bdquo;Detale wydarzenia&rdquo;. Możemy go umieścić na dowolnej stronie, w ustawieniach bloku &bdquo;Lista wydarzeń&rdquo; wystarczy podać adres strony, do kt&oacute;rej go wstawiamy, przeciągamy go w wybrany slot na bloki i klikamy na jego belce ikonę trybiku.</p>\n\n<p>Wybieramy rozmiar tytułu i czy ma być on widoczny. W ustawieniach wyświetlania można użyć opcji ukrywania tego bloku jeśli znajduje się on na tej samej stronie co blok &bdquo;Lista wydarzeń&rdquo;, czyli będzie on ukryty jeśli widoczna będzie lista wydarzeń.</p>\n\n<p>Tak jak bloki powyżej możemy wstawić na stronie pozostałe bloki czyli &bdquo;Nadchodzące wydarzenia&rdquo; i &bdquo;Wydarzenia archiwalne&rdquo;. W pierwszym z wymienionych blok&oacute;w będą się pojawiały wydarzenia z przyszła datą, w drugim wydarzenia minione. W ustawieniach tych blok&oacute;w są takie same elementy do zmiany ustawień tych blok&oacute;w.</p>\n\n<p>1. Rozmiar tytułu &ndash; wybieramy rozmiar tytułu i czy ma on być widoczny.</p>\n\n<p>2. Ilość element&oacute;w &ndash; ustawiamy ile naraz jest widocznych element&oacute;w na stronie paginacji.</p>\n\n<p>3. Wyświetl archiwalne wydarzenia z bloku &ndash; wybieramy z listy stronę i na niej blok, z kt&oacute;rego będą wyświetlane wydarzenia.</p>\n\n<p>4. Wyświetl archiwalne/nadchodzące wydarzenia z kategorii &ndash; wybieramy z listy kategorię wydarzeń lub zostawiamy domyślnie &ndash; wszystkie kategorie.</p>\n\n<p style="margin-left:1.27cm">Tak jak w bloku lista wydarzeń tak i w tych dw&oacute;ch blokach (lista wydarzeń archiwalnych i nadchodzące wydarzenia) możemy je ukryć jeśli na stronie znajdują się detale wydarzenia zaznaczając opcje &bdquo;Ukryj listę wydarzeń jeśli aktualny adres URL prowadzi do detali wydarzenia&rdquo;.</p>\n\n<p><strong>2. Edycja wydarzeń.</strong></p>\n\n<p>Na belce bloku Listy wydarzeń klikamy ikonę folderu - &bdquo;okna zarządzania&rdquo;. Analogicznie edytujemy wydarzenia z &bdquo;Listy wydarzeń archiwalnych&rdquo; i &bdquo;Nadchodzących wydarzeń&rdquo;.Otworzy nam się panel, w kt&oacute;rym możemy edytować wybrane wydarzenie, lub skorzystać z ikony szybkiej edycji umieszczonej przy tytule wydarzenia na liście wydarzeń.</p>\n\n<p>Przy edycji wydarzenia otworzy nam się panel, kt&oacute;ry posiada te same elementy jak przy dodawaniu nowego wydarzenia (patrz - Dodaj nowe wydarzenie - powyżej) powiększony o bardziej szczeg&oacute;łowe elementy typu: Gdzie, Wstęp, Opis, Kontakt, Dodatkowe Informacje. Po zakończeniu edycji klikamy &bdquo;Zapisz&rdquo;.</p>', '', ''),
(10, 764, 6, NULL, 1, 0, 1, 0, 1, '2016-01-04 15:43:58', '2015-12-09 13:36:37', NULL, '2015-12-09 13:30:00', '2015-12-16 13:30:00', '', '', '', 'tworzenie-blokow-uniwersalnych', 'Tworzenie bloków uniwersalnych', '', 'Każdy blok posiada swoje ustawienia, na belce ustawień pierwsza ikona dyskietki oznacza, że można ten blok zapisać jako dynamiczny i dodać go potem na innej stronie bez potrzeby tworzenia jego treści od początku.', '<p><strong>Tworzenie blok&oacute;w dynamicznych.</strong></p>\n\n<p>Każdy blok posiada swoje ustawienia,&nbsp;na belce ustawień pierwsza ikona dyskietki oznacza, że można ten blok zapisać jako dynamiczny i dodać go potem na innej stronie bez potrzeby tworzenia jego treści od początku.</p>\n\n<p>Po kliknięciu w ikonę otworzy nam się okno zapisu.</p>\n\n<p>1. Należy podać nazwy bloku dla wszystkich język&oacute;w, może być to wsp&oacute;lna nazwa dla wszystkich język&oacute;w.</p>\n\n<p>2. Wybrany język ma wpływ na wyszukiwanie bloku w wyszukiwarce blok&oacute;w w polu &bdquo;kategoria językowa&rdquo;.</p>\n\n<p>Po zapisaniu blok jest dostępny po najechaniu kursorem na ikonę dyskietki umieszczoną obok ikony plusa do dodawania blok&oacute;w w lewej dolnej części ekranu.</p>\n\n<p>&nbsp;</p>\n\n<p>Tak za zapisany blok można upuszczać na dowolnych stronach, usunięcie takiego bloku ze strony nie ma wpływu na inne jego wystąpienia na innych stronach. Natomiast usunięcie tego bloku z listy blok&oacute;w dynamicznych spowoduje usunięcie go z listy i tym samym z wszystkich stron, na kt&oacute;rych on występuje.</p>\n\n<p>&nbsp;</p>', '', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_event_list_additional_data`
--

CREATE TABLE `block_event_list_additional_data` (
  `data_id` mediumint(8) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED NOT NULL,
  `type_id` smallint(5) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `contents` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `block_event_list_additional_data`
--

INSERT INTO `block_event_list_additional_data` (`data_id`, `content_id`, `type_id`, `event_id`, `last_update_author_id`, `last_update_date`, `contents`) VALUES
(39, 764, 61, 9, 1, '2015-12-09 14:01:47', '<p>CMS</p>'),
(40, 764, 62, 9, 1, '2015-12-09 14:01:47', '<p>Tworzenie wydarzeń.</p>'),
(41, 764, 63, 9, 1, '2015-12-09 14:01:47', '<p>Wydarzenia mogą być przyszłe bądź archiwalne, w ustawieniach można włączyć przycisk dla gości, kt&oacute;rzy sami mogą informować o wydarzeniu wypełniając formularz kontaktowy.</p>'),
(42, 764, 64, 9, 1, '2015-12-09 14:01:47', '<p>norbert.banulski@codilab.pl</p>'),
(43, 764, 65, 9, 1, '2015-12-09 14:01:47', '<p>Więcej informacji na www.codilab.pl</p>'),
(44, 764, 61, 10, 1, '2015-12-09 13:43:09', 'CMS'),
(45, 764, 62, 10, 1, '2015-12-09 13:43:09', '<p>Jak zapisywać bloki jako dynamiczne.</p>'),
(46, 764, 63, 10, 1, '2015-12-09 13:43:09', '<p>Zwykły blok po zapisaniu jako dynamiczny zachowuje swoje treści i można go w prosty spos&oacute;b zamieszczać na istniejących stronach.</p>'),
(47, 764, 64, 10, 1, '2015-12-09 13:43:09', '<p>norbert.banulski@codilab.pl</p>'),
(48, 764, 65, 10, 1, '2015-12-09 13:43:09', '<p>Więcej informacji na www.codilab.pl</p>');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_event_list_additional_data_types`
--

CREATE TABLE `block_event_list_additional_data_types` (
  `type_id` smallint(5) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED NOT NULL,
  `is_published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `name` varchar(32) COLLATE utf8_polish_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `block_event_list_additional_data_types`
--

INSERT INTO `block_event_list_additional_data_types` (`type_id`, `content_id`, `is_published`, `last_update_author_id`, `last_update_date`, `name`) VALUES
(61, 764, 1, NULL, '2015-12-08 11:25:47', 'Gdzie'),
(62, 764, 1, NULL, '2015-12-08 11:25:47', 'Wstęp'),
(63, 764, 1, NULL, '2015-12-08 11:25:47', 'Opis'),
(64, 764, 1, NULL, '2015-12-08 11:25:47', 'Kontakt'),
(65, 764, 1, NULL, '2015-12-08 11:25:47', 'Dodatkowe informacje');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_event_list_categories`
--

CREATE TABLE `block_event_list_categories` (
  `category_id` smallint(5) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED DEFAULT NULL,
  `is_published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `name` varchar(32) COLLATE utf8_polish_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `block_event_list_categories`
--

INSERT INTO `block_event_list_categories` (`category_id`, `content_id`, `is_published`, `last_update_author_id`, `last_update_date`, `name`) VALUES
(6, 764, 1, 1, '2015-12-08 11:36:50', 'Bloki');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_event_list_event_images`
--

CREATE TABLE `block_event_list_event_images` (
  `image_id` mediumint(8) NOT NULL,
  `content_id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `attachment_id` mediumint(8) UNSIGNED NOT NULL COMMENT 'Image.',
  `is_main` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `order` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `block_event_list_event_images`
--

INSERT INTO `block_event_list_event_images` (`image_id`, `content_id`, `event_id`, `attachment_id`, `is_main`, `order`, `last_update_author_id`, `last_update_date`, `title`) VALUES
(7, 764, 9, 818, 1, 0, 1, '2015-12-09 14:01:47', ''),
(8, 764, 10, 816, 1, 0, 1, '2015-12-09 13:43:09', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_event_list_user_subscriptions`
--

CREATE TABLE `block_event_list_user_subscriptions` (
  `subscription_id` int(10) NOT NULL,
  `content_id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `gender` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `type_of_request` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Private person or Institution.',
  `newsletter_agreement` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `subscription_date` datetime DEFAULT NULL,
  `academic_title` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `name_and_surname` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `phone` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `institution_name` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `invoice_data` text COLLATE utf8_polish_ci NOT NULL,
  `attentions` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_images_pile_groups`
--

CREATE TABLE `block_images_pile_groups` (
  `group_id` smallint(5) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED DEFAULT NULL,
  `order` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `is_published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `title` varchar(128) COLLATE utf8_polish_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `block_images_pile_groups`
--

INSERT INTO `block_images_pile_groups` (`group_id`, `content_id`, `order`, `is_published`, `last_update_author_id`, `last_update_date`, `title`) VALUES
(1, 781, 0, 1, 1, '2015-12-09 12:45:28', 'Codilab-cms');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_images_pile_items`
--

CREATE TABLE `block_images_pile_items` (
  `item_id` mediumint(8) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `group_id` smallint(5) UNSIGNED DEFAULT NULL,
  `page_id` smallint(5) UNSIGNED DEFAULT NULL,
  `attachment_id` mediumint(8) UNSIGNED DEFAULT NULL COMMENT 'Image.',
  `order` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `is_published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `url_address_type` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `external_url` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `block_images_pile_items`
--

INSERT INTO `block_images_pile_items` (`item_id`, `content_id`, `group_id`, `page_id`, `attachment_id`, `order`, `is_published`, `url_address_type`, `last_update_author_id`, `last_update_date`, `external_url`, `title`) VALUES
(1, 781, 1, 248, 788, 0, 1, 2, 1, '2015-12-09 12:45:58', '', 'Pierwszy obrazek'),
(2, 781, 1, 248, 790, 0, 1, 2, 1, '2015-12-09 12:47:37', '', 'Drugi obrazek'),
(3, 781, 1, 248, 792, 0, 1, 2, 1, '2015-12-09 12:48:00', '', 'Trzeci obrazek'),
(4, 781, 1, 248, 794, 0, 1, 2, 1, '2015-12-09 12:48:59', '', 'Czwarty obrazek');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_image_gallery`
--

CREATE TABLE `block_image_gallery` (
  `image_id` mediumint(8) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED NOT NULL,
  `attachment_id` mediumint(8) UNSIGNED DEFAULT NULL COMMENT 'Image.',
  `order` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `is_published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `title` varchar(128) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `author` varchar(128) COLLATE utf8_polish_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `block_image_gallery`
--

INSERT INTO `block_image_gallery` (`image_id`, `content_id`, `attachment_id`, `order`, `is_published`, `last_update_author_id`, `last_update_date`, `title`, `author`) VALUES
(1, 774, 772, 0, 1, 1, '2015-12-09 12:30:51', 'Pierwszy obrazek', ''),
(2, 774, 774, 0, 1, 1, '2015-12-09 12:31:11', 'Drugi obrazek', ''),
(3, 774, 776, 0, 1, 1, '2015-12-09 12:31:38', 'Trzeci obrazek', ''),
(4, 774, 778, 0, 1, 1, '2015-12-09 12:31:55', 'Czwarty obrazek', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_image_slider`
--

CREATE TABLE `block_image_slider` (
  `image_id` mediumint(8) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED NOT NULL,
  `attachment_id` mediumint(8) UNSIGNED DEFAULT NULL COMMENT 'Image.',
  `page_id` smallint(5) UNSIGNED DEFAULT NULL,
  `order` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `is_published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `url_address_type` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `external_url` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `title` varchar(64) COLLATE utf8_polish_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `block_image_slider`
--

INSERT INTO `block_image_slider` (`image_id`, `content_id`, `attachment_id`, `page_id`, `order`, `is_published`, `url_address_type`, `last_update_author_id`, `last_update_date`, `external_url`, `title`) VALUES
(1, 776, 780, 248, 0, 1, 2, 1, '2015-12-09 12:35:21', '', 'Pierwszy obrazek'),
(2, 776, 782, 248, 0, 1, 2, 1, '2015-12-09 12:35:38', '', 'Drugi obrazek'),
(3, 776, 784, 248, 0, 1, 2, 1, '2015-12-09 12:36:22', '', 'Trzeci obrazek'),
(4, 776, 786, 248, 0, 1, 2, 1, '2015-12-09 12:36:42', '', 'Czwarty obrazek'),
(5, 797, 858, 248, 0, 1, 1, 1, '2016-01-05 13:35:07', 'http://www.drew-poland.pl', 'Drew-poland'),
(6, 797, 860, 248, 0, 1, 1, 1, '2016-01-05 13:23:41', 'http://www.glusi24.info', 'Glusi24'),
(8, 797, 864, 248, 0, 1, 1, 1, '2016-01-05 13:25:06', 'http://www.krktaxi.pl', 'MPL Taxi'),
(9, 797, 866, 248, 0, 1, 1, 1, '2016-01-05 13:25:43', 'http://www.mplservices.pl', 'MPL Services'),
(10, 797, 886, 248, 0, 1, 1, 1, '2016-01-05 13:31:33', 'http://www.mtv24.tv', 'MTV 24'),
(11, 797, 872, 248, 0, 1, 1, 1, '2016-01-05 13:27:13', 'http://secover.pl', 'Secover'),
(12, 797, 874, 248, 0, 1, 1, 1, '2016-01-05 13:27:49', 'http://sgtir.vistula.edu.pl', 'SGTiR'),
(13, 797, 876, 248, 0, 1, 1, 1, '2016-01-05 13:28:23', 'http://www.skbank.pl', 'Sk Bank'),
(14, 797, 878, 248, 0, 1, 1, 1, '2016-01-05 13:28:56', 'http://www.tgde.pl', 'TGDE'),
(15, 797, 880, 248, 0, 1, 1, 1, '2016-01-05 13:29:22', 'http://vistula.edu.pl', 'Vistula'),
(16, 797, 888, 248, 0, 1, 1, 1, '2016-01-05 13:31:47', 'http://www.zbikowskicoaching.info', 'Zbikowski Coaching'),
(17, 797, 890, 248, 0, 1, 1, 1, '2016-01-05 13:36:41', 'http://www.krakowairport-parking.pl', 'MPL Parking');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_main_menu_information`
--

CREATE TABLE `block_main_menu_information` (
  `info_id` smallint(5) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED DEFAULT NULL,
  `item_id` smallint(5) UNSIGNED DEFAULT NULL,
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `title` varchar(128) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `contents` text COLLATE utf8_polish_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `block_main_menu_information`
--

INSERT INTO `block_main_menu_information` (`info_id`, `content_id`, `item_id`, `last_update_author_id`, `last_update_date`, `title`, `contents`) VALUES
(484, 763, 542, 1, '2015-12-09 10:31:19', 'Wydarzenia', 'Do tworzenia wydarzeń są zaprojektowane cztery bloki &ndash; &bdquo;Lista wydarzeń&rdquo; , &bdquo; Detale wydarzenia&rdquo;, &bdquo;Lista wydarzeń archiwalnych&rdquo; i &bdquo;Nadchodzące wydarzenia&rdquo;.'),
(485, 763, 543, 1, '2015-12-09 10:30:50', 'Aktualności', 'Obsługę aktualności zapewniają cztery bloki &ndash; &bdquo;Lista aktualności&rdquo; , &bdquo; Detale aktualności&rdquo;, &bdquo;Formularz dodawania aktualności&rdquo; i &bdquo;Aktualności slider&rdquo;.'),
(486, 763, 540, 1, '2015-12-08 11:21:09', 'Blok tekstowy', 'Kliknij aby edytować'),
(487, 763, 541, 1, '2015-12-08 11:21:09', 'Akordeon', 'Kliknij aby edytować'),
(488, 763, 558, 1, '2015-12-09 13:01:28', 'Grafika', 'Do prezentacji grafiki na stronie zaprojektowanych jest 5 blok&oacute;w: &quot;galeria obraz&oacute;w&quot;, &quot;stos obraz&oacute;w&quot;, &quot;slider obraz&oacute;w&quot;, &quot;mały baner&quot;, &quot;duży baner&quot;.'),
(489, 763, 570, 1, '2016-01-04 13:54:58', 'Lista wszystkich artykułów', 'Kliknij aby edytować');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_navigation_menu_categories`
--

CREATE TABLE `block_navigation_menu_categories` (
  `category_id` tinyint(3) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED NOT NULL,
  `is_published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `title` varchar(32) COLLATE utf8_polish_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `block_navigation_menu_categories`
--

INSERT INTO `block_navigation_menu_categories` (`category_id`, `content_id`, `is_published`, `last_update_author_id`, `last_update_date`, `title`) VALUES
(113, 763, 1, NULL, '2015-12-08 11:12:46', 'Domyślna kategoria'),
(114, 763, 1, NULL, '2015-12-08 11:13:18', 'Bloki'),
(115, 773, 1, NULL, '2015-12-09 11:53:29', 'Domyślna kategoria');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_navigation_menu_items`
--

CREATE TABLE `block_navigation_menu_items` (
  `item_id` smallint(5) UNSIGNED NOT NULL,
  `parent_id` smallint(5) UNSIGNED DEFAULT NULL,
  `depth` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `content_id` int(10) UNSIGNED DEFAULT NULL,
  `category_id` tinyint(3) UNSIGNED DEFAULT NULL,
  `page_id` smallint(5) UNSIGNED DEFAULT NULL,
  `attachment_id` mediumint(8) UNSIGNED DEFAULT NULL COMMENT 'Image.',
  `order` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `is_published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `url_address_type` tinyint(2) UNSIGNED NOT NULL DEFAULT '0',
  `type` tinyint(2) UNSIGNED NOT NULL DEFAULT '0' COMMENT '0 - Only text. 1 - Only glyphicon. 2 - Only image. 3 - Glyphicon + text. 4. Image + text.',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `bootstrap_glyphicon` varchar(32) COLLATE utf8_polish_ci NOT NULL DEFAULT 'asterisk',
  `image_alt` varchar(32) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `external_url` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `title` varchar(128) COLLATE utf8_polish_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `block_navigation_menu_items`
--

INSERT INTO `block_navigation_menu_items` (`item_id`, `parent_id`, `depth`, `content_id`, `category_id`, `page_id`, `attachment_id`, `order`, `is_published`, `url_address_type`, `type`, `last_update_author_id`, `last_update_date`, `bootstrap_glyphicon`, `image_alt`, `external_url`, `title`) VALUES
(540, NULL, 0, 763, 114, 253, NULL, 0, 1, 0, 0, 1, '2015-12-08 11:14:12', 'adjust', '', '', 'Blok tekstowy'),
(541, NULL, 0, 763, 114, 254, NULL, 1, 1, 0, 0, 1, '2015-12-08 11:20:37', 'list-alt', '', '', 'Akordeon'),
(542, NULL, 0, 763, 114, 255, 764, 2, 1, 0, 0, 1, '2015-12-08 11:22:48', 'adjust', '', '', 'Wydarzenia'),
(543, NULL, 0, 763, 114, 256, NULL, 3, 1, 0, 0, 1, '2015-12-08 11:22:40', 'adjust', '', '', 'Aktualności'),
(544, 542, 1, 763, 114, 257, NULL, 0, 1, 0, 0, 1, '2015-12-08 11:58:10', 'adjust', '', '', 'Detale wydarzenia'),
(545, 543, 1, 763, 114, 258, NULL, 0, 1, 0, 0, 1, '2015-12-09 10:08:17', 'adjust', '', '', 'Detale aktualności'),
(547, 542, 1, 763, 114, 259, NULL, 0, 1, 0, 0, 1, '2015-12-09 10:23:19', 'adjust', '', '', 'Archiwum wydarzeń'),
(548, 542, 1, 763, 114, 260, NULL, 0, 1, 0, 0, 1, '2015-12-09 10:23:44', 'adjust', '', '', 'Nadchodzące wydarzenia'),
(549, NULL, 0, 773, 115, 253, NULL, 0, 1, 0, 0, 1, '2015-12-09 11:55:17', 'adjust', '', '', 'Blok tekstowy'),
(550, NULL, 0, 773, 115, 254, NULL, 1, 1, 0, 0, 1, '2015-12-09 11:55:37', 'adjust', '', '', 'Akordeon'),
(551, NULL, 0, 773, 115, 255, NULL, 2, 1, 0, 0, 1, '2015-12-09 11:55:53', 'adjust', '', '', 'Wydarzenia'),
(552, 551, 1, 773, 115, 260, NULL, 0, 1, 0, 0, 1, '2015-12-09 11:56:10', 'adjust', '', '', 'Nadchodzące wydarzenia'),
(553, 551, 1, 773, 115, 259, NULL, 0, 1, 0, 0, 1, '2015-12-09 11:56:36', 'adjust', '', '', 'Archiwum wydarzeń'),
(554, 551, 1, 773, 115, 257, NULL, 0, 1, 0, 0, 1, '2015-12-09 11:56:51', 'adjust', '', '', 'Detale wydarzenia'),
(555, NULL, 0, 773, 115, 256, NULL, 3, 1, 0, 0, 1, '2015-12-09 11:57:06', 'adjust', '', '', 'Aktualności'),
(557, 555, 1, 773, 115, 258, NULL, 0, 1, 0, 0, 1, '2015-12-09 11:57:41', 'adjust', '', '', 'Detale aktualności'),
(558, NULL, 0, 763, 114, 262, NULL, 4, 1, 0, 0, 1, '2015-12-09 15:19:41', 'adjust', '', '', 'Grafika'),
(559, 558, 1, 763, 114, 263, NULL, 0, 1, 0, 0, 1, '2015-12-09 12:24:54', 'adjust', '', '', 'galeria obrazów'),
(560, 558, 1, 763, 114, 264, NULL, 0, 1, 0, 0, 1, '2015-12-09 12:25:40', 'adjust', '', '', 'slider obrazów'),
(561, 558, 1, 763, 114, 265, NULL, 0, 1, 0, 0, 1, '2015-12-09 12:26:14', 'adjust', '', '', 'stos obrazów'),
(562, 558, 1, 763, 114, 266, NULL, 0, 1, 0, 0, 1, '2015-12-09 12:26:02', 'adjust', '', '', 'duży baner'),
(563, 558, 1, 763, 114, 267, NULL, 0, 1, 0, 0, 1, '2015-12-09 12:26:27', 'adjust', '', '', 'mały baner'),
(564, NULL, 0, 773, 115, 262, NULL, 4, 1, 0, 0, 1, '2015-12-09 12:27:43', 'adjust', '', '', 'Grafika'),
(565, 564, 1, 773, 115, 263, NULL, 0, 1, 0, 0, 1, '2015-12-09 12:27:57', 'adjust', '', '', 'galeria obrazów'),
(566, 564, 1, 773, 115, 264, NULL, 0, 1, 0, 0, 1, '2015-12-09 12:28:12', 'adjust', '', '', 'slider obrazów'),
(567, 564, 1, 773, 115, 265, NULL, 0, 1, 0, 0, 1, '2015-12-09 12:28:26', 'adjust', '', '', 'stos obrazów'),
(568, 564, 1, 773, 115, 266, NULL, 0, 1, 0, 0, 1, '2015-12-09 12:28:39', 'adjust', '', '', 'duży baner'),
(569, 564, 1, 773, 115, 267, NULL, 0, 1, 0, 0, 1, '2015-12-09 12:28:51', 'adjust', '', '', 'mały baner'),
(570, NULL, 0, 763, 114, 268, NULL, 5, 1, 0, 0, 1, '2016-01-04 13:55:46', 'adjust', '', '', 'Wszystkie artykuły'),
(571, NULL, 0, 773, 115, 268, NULL, 5, 1, 0, 0, 1, '2016-01-04 13:57:43', 'adjust', '', '', 'Wszystkie artykuły'),
(572, 570, 1, 763, 114, 270, NULL, 0, 1, 0, 0, 1, '2016-01-05 13:53:50', 'adjust', '', '', 'o nas');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_news_list`
--

CREATE TABLE `block_news_list` (
  `news_id` int(10) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED DEFAULT NULL,
  `category_id` smallint(6) DEFAULT NULL,
  `attachment_id` mediumint(8) UNSIGNED DEFAULT NULL COMMENT 'Image.',
  `is_published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_promoted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `news_date` datetime DEFAULT NULL,
  `clean_url` varchar(128) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `title` varchar(128) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `label` varchar(32) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `teaser` varchar(350) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `contents` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `block_news_list`
--

INSERT INTO `block_news_list` (`news_id`, `content_id`, `category_id`, `attachment_id`, `is_published`, `is_promoted`, `last_update_author_id`, `last_update_date`, `news_date`, `clean_url`, `title`, `label`, `teaser`, `contents`) VALUES
(1, 770, 1, NULL, 1, 0, 1, '2016-01-04 14:40:34', '2015-12-09 10:00:00', 'nowe-aktualnosci', 'Nowe Aktualności', '', 'Obsługę aktualności zapewniają cztery bloki – „Lista aktualności” , „ Detale aktualności”, „Formularz dodawania aktualności” i „Aktualności slider”. Zacznijmy od wstawienia bloku Lista aktualności. Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną trybiku.', '<p><strong>1. Dodawanie aktualności.</strong></p>\n\n<p>Obsługę aktualności zapewniają cztery bloki &ndash; &bdquo;Lista aktualności&rdquo; , &bdquo; Detale aktualności&rdquo;, &bdquo;Formularz dodawania aktualności&rdquo; i &bdquo;Aktualności slider&rdquo;. Zacznijmy od wstawienia bloku Lista aktualności. Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną puzla w prawej dolnej części ekranu.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px" /></p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png" /></p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony, wyszukujemy blok o nazwie &bdquo;Lista aktualności&rdquo;, można w tym celu użyć wyszukiwarki blok&oacute;w np. wybierając z listy &bdquo;Typ bloku&rdquo; pozycję &bdquo;Lista aktualności&rdquo;. Następnie chwytamy myszką ikonę zaznaczoną na zdjęciu poniżej i przeciągamy blok w wybrany na stronie slot na bloki.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_19_33.png" /></p>\n\n<p>Wstawiony blok Lista aktualności posiada pr&oacute;cz standardowych ustawień, także unikalne dla tego bloku. Na belce Listy aktualności klikamy ikonę folderu - &bdquo;okna zarządzania&rdquo;.</p>\n\n<p>Otworzy nam się panel, w kt&oacute;rym utworzymy naszą kategorię aktualności a potem nową aktualność, kt&oacute;rą przypiszemy do tej kategorii. Klikamy przycisk &bdquo;Kategorie&rdquo;.</p>\n\n<p>A następnie przycisk &bdquo;Nowa kategoria&rdquo;.</p>\n\n<p>W panelu wpisujemy tytuł dla kategorii aktualności, będzie potem można przypisywać wiele aktualności pod tą kategorię i wyświetlać wszystkie naraz w danej kategorii, możemy zaznaczyć &bdquo;opublikuj po zapisaniu&rdquo; w celu natychmiastowego opublikowania tej kategorii i kliknąć &bdquo;Zapisz&rdquo;.</p>\n\n<p>Teraz możemy już tworzyć nowe aktualności. Zamykamy panel &bdquo;Kategorie - Zarządzanie&rdquo; i klikamy przycisk &bdquo;Nowa aktualność&rdquo;. Uzupełniamy formularz dodawania nowej aktualności.</p>\n\n<p>1. Kategoria &ndash; wybieramy z listy kategorię, do kt&oacute;rej przypiszemy nasze wydarzenie. W naszym przykładzie jest tylko jedna kategoria, kt&oacute;rą stworzyliśmy powyżej.</p>\n\n<p>2. Tytuł &ndash; nadajemy tytuł wydarzeniu, w naszym przykładzie jest to &bdquo;Nowe wydarzenie&rdquo;</p>\n\n<p>3. Adres URL &ndash; w momencie wprowadzania tytułu ustawi się automatycznie.</p>\n\n<p>4. Etykieta &ndash; pojawi się pod tytułem jako jego dopełnienie.</p>\n\n<p>5, 6. Data, Godzina rozpoczęcia &ndash; domyślnie ustawia się na obecną od momentu dodawania aktualności, można ustawić inną datę i godzinę rozpoczęcia.</p>\n\n<p>7.&nbsp;Obraz &ndash; możemy dodać obraz do wydarzenia lub kilka obraz&oacute;w i ustawić klikając na k&oacute;łeczko w prawym g&oacute;rnym rogu loadera, kt&oacute;ry obraz ma być jako zdjęcie gł&oacute;wne wyświetlane na liście aktualności. Ikoną strzałki ładujemy plik obrazu, możemy też użyć edycji ikoną oł&oacute;wka.</p>\n\n<p>8.&nbsp;Treść &ndash; opisujemy tutaj aktualność korzystając z CKEdytora.</p>\n\n<p>9. Zajawka &ndash; zajawka będzie wyświetlana na liście wydarzeń jako skr&oacute;cona treść, można użyć do 350 znak&oacute;w, cała treść będzie dostępna w detalach aktualności po kliknięciu na aktualność, o tym w dalszej części opisu.</p>\n\n<p>Po zapisaniu wszystkich wprowadzonych treści w bloku lista aktualności pojawi się nasza nowa aktualność. Teraz możemy skorzystać z ustawień dostępnych na belce bloku &bdquo;Lista aktualności&rdquo;.</p>\n\n<p>Klikamy na ikonę trybiku.</p>\n\n<p>Pojawi nam się okno z ustawieniami, kt&oacute;rych opis jest poniżej.</p>\n\n<p>1. Rozmiar tytułu &ndash; określa wielkość tytułu listy wydarzeń i czy ma być widoczny na stronie.</p>\n\n<p>2. Ilość element&oacute;w &ndash; ustawiamy ile naraz jest widocznych element&oacute;w na stronie paginacji.</p>\n\n<p>3. Wyświetl detale aktualności na stronie &ndash; &bdquo;Detale aktualności&rdquo; to blok, w kt&oacute;rym pojawi się nasza aktualność po kliknięciu na nią w liście aktualności. Blok &bdquo;Detale aktualności&rdquo; można umieścić na innej stronie i to na nią będzie nas kierowało po kliknięciu na aktualność z listy aktualności, można też umieścić detale aktualności na tej samej stronie. Domyślnie ustawia się strona startowa (gł&oacute;wna).</p>\n\n<p>Ukrywanie listy aktualności &ndash; jeśli blok &bdquo;Detale aktualności&rdquo; znajduje się na tej samej stronie co lista aktualności można ukryć listę w momencie wyświetlania detali aktualności.</p>\n\n<p>Analogicznie jak blok &bdquo;Lista aktualności&rdquo; dodajemy teraz blok &bdquo;Detale aktualności&rdquo;. Możemy go umieścić na dowolnej stronie, w ustawieniach bloku &bdquo;Lista aktualności&rdquo; wystarczy podać adres strony (tytuł strony), do kt&oacute;rej go wstawiamy, przeciągamy go w wybrany slot na bloki i klikamy na jego belce ikonę trybiku.</p>\n\n<p>Wybieramy rozmiar tytułu i czy ma być on widoczny. W ustawieniach wyświetlania można użyć opcji ukrywania tego bloku jeśli znajduje się on na tej samej stronie co blok &bdquo;Lista aktualności&rdquo;, czyli będzie on ukryty jeśli widoczna będzie lista aktualności.</p>\n\n<p>Tak jak bloki powyżej możemy wstawić na stronie pozostałe bloki czyli &bdquo;Formularz dodawania aktualności&rdquo; i &bdquo;Aktualności slider&rdquo;. Pierwszy z wymienionych blok&oacute;w wyświetli formularz dla gości, kt&oacute;rzy będą mogli dodawać aktualności. Dodane w ten spos&oacute;b aktualności trafią do listy aktualności, jednak nie będą opublikowane, dopiero po pomyślnej weryfikacji przez moderatora lub administratora strony będą widoczne. Klikamy na belce bloku formularza ikonę trybiku &ndash; ustawień.</p>\n\n<p>Ustawiamy rozmiar tytułu i czy ma on być widoczny. Dalszą opcją jest wyb&oacute;r bloku, z kt&oacute;rym chcemy skojarzyć, id bloku widoczne jest na jego belce z lewej strony.</p>\n\n<p>Ostatni blok związany z aktualnościami to &bdquo;Aktualności slider&rdquo;, wyświetla listę aktualności w postaci slidera, dodajemy go do strony jak inne bloki. Przyjrzyjmy się jego ustawieniom, klikamy na wstawionym bloku &bdquo;Aktualności slider&rdquo; ikonę trybiku, otworzy nam się formularz z ustawieniami jak poniżej.</p>\n\n<p>Na samej g&oacute;rze ustawiamy rozmiar tytułu i czy ma on być widoczny.</p>\n\n<p>1. Wyświetl aktualności z bloku &ndash; wybieramy stronę i blok z listą aktualności, kt&oacute;ra się na niej znajduje, slider będzie pobierał z niej aktualności do wyświetlania.</p>\n\n<p>2. Wyświetl aktualności z kategorii &ndash; wybieramy kategorię aktualności, będą się wyświetlać tylko aktualności z danej kategorii, domyślnie jest ustawione &bdquo;Wszystkie kategorie&rdquo;.</p>\n\n<p>3. Całkowita ilość aktualności w sliderze &ndash; wybieramy ilość aktualności do wyświetlenia.</p>\n\n<p>4. Ustawienia przewijania &ndash; ustawiamy czas pomiędzy przejściami (slajdami), czas samego przejścia oraz typ przewijania, dostępnych są 4 typy oraz brak przewijania:</p>\n\n<p>- skocz na początek po wyświetleniu ostatniego elementu</p>\n\n<p>- skocz na koniec po wyświetleniu pierwszego elementu</p>\n\n<p>- użyj pierwszej i drugiej opcji jednocześnie</p>\n\n<p>- użyj zapętlonego przewijania</p>\n\n<p><strong>2. Edycja aktualności.</strong></p>\n\n<p>Na belce bloku Listy aktualności klikamy ikonę folderu - &bdquo;okna zarządzania&rdquo;.</p>\n\n<p>Otworzy nam się panel, w kt&oacute;rym możemy edytować wybrane wydarzenie</p>\n\n<p>lub skorzystać z ikony szybkiej edycji umieszczonej przy tytule wydarzenia na liście wydarzeń.</p>\n\n<p>Przy edycji aktualności otworzy nam się panel, kt&oacute;ry posiada te same elementy jak przy dodawaniu nowej aktualności (patrz - Dodaj nową aktualność - powyżej. Po zakończeniu edycji klikamy &bdquo;Zapisz&rdquo;.</p>'),
(2, 770, 1, NULL, 1, 0, 1, '2016-01-04 14:36:08', '2015-12-09 13:00:00', 'dodawanie-nowej-strony', 'Dodawanie nowej strony', '', 'Po zalogowaniu się klikamy na widoczny z lewej strony przycisk panelu narzędzi administratora. Wybieramy pozycję „STRONY”. W otwartym oknie ustawień wybieramy niebieski przycisk „Nowa strona”. Otworzy nam się formularz dodawania nowej strony, którego poszczególne elementy wraz z charakterystyką przedstawia poniższy opis.', '<p style="margin-left:1.25cm">Po zalogowaniu się&nbsp;klikamy na widoczny z lewej strony przycisk panelu narzędzi administratora.</p>\n\n<p style="margin-left:1.25cm">Wybieramy pozycję &bdquo;STRONY&rdquo;.</p>\n\n<p style="margin-left:0.64cm">W otwartym oknie ustawień wybieramy przycisk &bdquo;Nowa strona&rdquo;.</p>\n\n<p style="margin-left:0.64cm">Otworzy nam się formularz dodawania nowej strony, kt&oacute;rego poszczeg&oacute;lne elementy wraz z charakterystyką przedstawia poniższy opis.</p>\n\n<p style="margin-left:0.64cm"><strong>1. </strong>Tytuł &ndash; może zawierać dowolne znaki, litery, cyfry i znaki specjalne .</p>\n\n<p style="margin-left:0.64cm"><strong>2. </strong>Język &ndash; wyb&oacute;r języka dla nowej strony, domyślnie polski, dostępny angielski i rosyjski.</p>\n\n<p style="margin-left:0.64cm"><strong>3. </strong>Adres URL &ndash; domyślnie adres ustawi się sam na podstawie tytułu nowej strony, można jednak go zmienić na inny niż domyślny, nie zalecamy jednak samodzielnej zmiany adresu URL &ndash; zmiany tylko dla zaawansowanych użytkownik&oacute;w.</p>\n\n<p style="margin-left:0.64cm"><strong>4. </strong>Strona nadrzędna &ndash; umieszcza w adresie do naszej nowej strony wybraną stronę nadrzędną w taki spos&oacute;b: pol/page/<strong>strona_nadrzedna</strong>/<strong>Adres_URL </strong>nadając jakby rodzica naszej nowej stronie.</p>\n\n<p style="margin-left:0.64cm"><strong>5. </strong>English (US) &ndash; ekwiwalentna strona dla języka angielskiego jeśli taka istnieje, domyślnie jest to strona gł&oacute;wna. Wybieramy tu stronę kt&oacute;ra wyświetli się po przełączeniu języka na angielski.</p>\n\n<p style="margin-left:0.64cm"><strong>6. </strong>Russian &ndash; ekwiwalentna strona dla języka rosyjskiego jeśli taka istnieje, domyślnie jest to strona gł&oacute;wna. Wybieramy tu stronę kt&oacute;ra wyświetli się po przełączeniu języka na rosyjski.</p>\n\n<p style="margin-left:0.64cm"><strong>7.</strong> Meta &bdquo;keywords&rdquo; - umieszczamy tu kluczowe słowa, kt&oacute;re najlepiej charakteryzują zawartość dokumentu. Postaraj się wyobrazić sobie jakich sł&oacute;w może użyć potencjalny użytkownik oraz jakie ewentualne błędy lub liter&oacute;wki mogą się wkraść do jego zapytania. Weź r&oacute;wnież pod uwagę, że nie wszyscy używają polskich znak&oacute;w diakrytycznych.</p>\n\n<p style="margin-left:0.64cm"><strong>8. </strong>Meta &bdquo;description&rdquo; - opis zawartości strony, zostanie wyświetlony przez wyszukiwarki zaraz po tytule strony. Należy go dobrać bardzo starannie, tak aby stanowił zachętę do odwiedzenia właśnie tej strony.</p>\n\n<p style="margin-left:0.64cm"><strong>9. </strong>Częstotliwość zmian na stronie &ndash; polepsza wyniki wyszukiwania w google, im częściej tym lepiej.</p>\n\n<p style="margin-left:0.64cm"><strong>10. </strong>Priorytet dla adresu URL &ndash; określa ważność adresu URL dla wyszukiwarki, można w podpiętych opcjach zaznaczyć czy wyszukiwarki mają indeksować naszą stronę i tworzyć jej kopię, można też zabronić przeglądarkom trzymać tę kopię w pamięci podręcznej.</p>\n\n<p style="margin-left:0.64cm"><strong>11. </strong>Gł&oacute;wny szablon strony &ndash; aktualnie jest jeden szablon &bdquo;Domyślny&rdquo;.</p>\n\n<p style="margin-left:0.64cm"><strong>12. </strong>Układ &ndash; układ do wstawiania element&oacute;w na stronie, do wyboru są 4 układy:</p>\n\n<p style="margin-left:0.64cm"><strong>- </strong>dwie kolumny - podstrona</p>\n\n<p style="margin-left:0.64cm">- jedna kolumna</p>\n\n<p style="margin-left:0.64cm">- dwie kolumny</p>\n\n<p style="margin-left:0.64cm">- trzy kolumny</p>\n\n<p style="margin-left:0.64cm"><strong>13. </strong>Opcja ustawia nową stronę jako &bdquo;włączoną&rdquo; od razu po zapisaniu.</p>\n\n<p style="margin-left:0.64cm"><strong>Edycja strony dostępna jest po kliknięciu tego przycisku, formularz edycji zawiera te same elementy jak formularz dodawania nowej strony.</strong></p>'),
(3, 770, 1, NULL, 1, 0, 1, '2016-01-04 14:41:50', '2015-12-09 13:15:00', 'dodawanie-nowego-uzytkownika', 'Dodawanie nowego Użytkownika', '', 'Po zalogowaniu się klikamy na widoczny z lewej strony przycisk panelu narzędzi administratora. Wybieramy pozycję „UŻYTKOWNICY”. W otwartym oknie ustawień wybieramy niebieski przycisk „Nowy użytkownik”. Otworzy nam się formularz dodawania nowego użytkownika, którego poszczególne elementy wraz z charakterystyką przedstawia poniższy opis.', '<p><strong>Dodanie/edycja Użytkownika.</strong></p>\n\n<p style="margin-left:1.25cm">Po zalogowaniu się&nbsp;klikamy na widoczny z lewej strony przycisk panelu narzędzi administratora.</p>\n\n<p style="margin-left:0.64cm">Wybieramy pozycję &bdquo;UŻYTKOWNICY&rdquo;.</p>\n\n<p style="margin-left:0.64cm">W otwartym oknie ustawień wybieramy przycisk &bdquo;Nowy użytkownik&rdquo;.</p>\n\n<p style="margin-left:0.64cm">Otworzy nam się formularz dodawania nowego użytkownika, kt&oacute;rego poszczeg&oacute;lne elementy wraz z charakterystyką przedstawia poniższy opis.</p>\n\n<p style="margin-left:0.64cm"><strong>1. </strong>Login &ndash; nazwa kt&oacute;rej nowy użytkownik będzie używał do logowania się, może zawierać dowolne znaki, litery, cyfry i znaki specjalne .</p>\n\n<p style="margin-left:0.64cm"><strong>2. </strong>Typ konta &ndash; wyb&oacute;r jednego z trzech typ&oacute;w konta:</p>\n\n<p style="margin-left:0.64cm"><strong>-</strong> User: z tymi uprawnieniami użytkownik może logować się, przeglądać treści publiczne strony, zapisywać się na wydarzenia.</p>\n\n<p style="margin-left:0.64cm"><strong>-</strong> Moderator: dziedziczy od User , poza tym ma uprawnienia do wprowadzania zmian użytkownik&oacute;w i stron.</p>\n\n<p style="margin-left:0.64cm"><strong>- </strong>Administrator: dziedziczy od moderatora, z tymi uprawnieniami użytkownik może dodawać, edytować i usuwać i bloki (dostępny tryb projektowy).</p>\n\n<p style="margin-left:0.64cm"><strong>3. </strong>E-mail &ndash; e-mail nowego użytkownika.</p>\n\n<p style="margin-left:0.64cm"><strong>4. </strong>Nowe hasło &ndash; hasło należy nadać samemu, jest to jedyna droga żeby m&oacute;c się nim potem logować, hasło generowane automatycznie jest zabezpieczone szyfrowaniem, może mieć dowolną liczbę znak&oacute;w.<strong> </strong></p>\n\n<p style="margin-left:0.64cm"><strong>5. </strong>Podstawowe dane dotyczące nowego użytkownika.</p>\n\n<p><strong>Edycja danych użytkownika dostępna jest po kliknięciu ikony edycji w wierszu z użytkownikiem, u kt&oacute;rego chcemy wprowadzić zmiany, formularz edycji zawiera te same elementy jak formularz dodawania nowego użytkownika.</strong></p>'),
(4, 770, 1, NULL, 1, 0, 1, '2016-01-04 15:04:07', '2015-12-09 13:15:00', 'dodawanie-blokow-na-stronie', 'Dodawanie bloków na stronie', '', 'Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną trybiku. W trybie edycji widoczne są narzędzia do obsługi bloków, najeżdżamy kursorem na przycisk z ikoną plusa. Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, które można użyć do tworzenia strony', '<p><strong>Dodawanie blok&oacute;w.</strong></p>\n\n<p style="margin-left:0.64cm">Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną puzla w prawym dolnym rogu ekranu.</p>\n\n<p style="margin-left:0.64cm"><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px" /></p>\n\n<p style="margin-left:0.64cm">W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p style="margin-left:0.64cm"><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png" /></p>\n\n<p style="margin-left:0.64cm">Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony</p>\n\n<p>1. Kategoria językowa &ndash; ustawienie języka, każdemu blokowi można przypisać kategorię językową, tworząc nowy blok czy edytując już istniejący &ndash; więcej w pkt 6b.</p>\n\n<p>2. Typ bloku &ndash; lista wszystkich typ&oacute;w blok&oacute;w w systemie.</p>\n\n<p>3. Szukaj po nazwie &ndash; wyszukiwarka blok&oacute;w, należy wpisać całą nazwę bądź jej fragment.</p>\n\n<p>4. Ikona dodania bloku do strony &ndash; należy chwycić ikonę i przeciągnąć ją nad wyznaczone pola na stronie, miejsca, w kt&oacute;rych można upuścić blok zaznaczone są na zdjęciu &bdquo;<strong>Sloty do wstawiania blok&oacute;w</strong>&rdquo; poniżej tej listy, w momencie gdy blok znajdzie się nad takim miejscem, zrobi się ono większe i podświetlone na pomarańczowo &ndash; wtedy można upuścić blok i znajdzie się on na stronie.</p>\n\n<p>5. Ikona przestawiania &ndash; można porządkować bloki poprzez przesuwanie ich w inne miejsca tabeli, domyślnie ustawione są alfabetycznie.</p>\n\n<p>6. Ikona edycji &ndash; po kliknięciu w ikonę pojawi się okno ustawień opisane na zdjęciu &bdquo;<strong>Okno ustawień edycji właściwości bloku</strong>&rdquo; poniżej tej listy.</p>\n\n<p style="margin-left:0.5cm"><strong>6a. </strong>Nazwa bloku we wszystkich językach &ndash; nadanie nazwy dla bloku w języku polskim, angielskim i rosyjskim</p>\n\n<p style="margin-left:0.5cm"><strong>6b. </strong>Traktuj jako &ndash; przypisanie bloku do kategorii języka, domyślnie jest to &bdquo;wszystkie kategorie&rdquo;, czyli bloki będą widoczne na wszystkich stronach niezależnie od języka strony, można ustawić język polski, angielski lub rosyjski &ndash; wtedy dany blok będzie widoczny tylko na stronie z ustawieniem języka adekwatnym do wybranego języka dla bloku.</p>\n\n<p>7. Ikona info &ndash; info o bloku.</p>\n\n<p><strong>Sloty do wstawiania blok&oacute;w</strong></p>\n\n<p>Teoretycznie bloki można upuszczać w dowolnym slocie, jednak niekt&oacute;re sloty mają lepsze predyspozycje do wyświetlania niekt&oacute;rych blok&oacute;w, są one zaznaczone na zdjęciu poniżej.Po upuszczeniu bloku w dobre miejsce doda się on na stronie.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_14_47_21.png" /></p>'),
(5, 770, 1, NULL, 1, 0, 1, '2016-01-08 16:35:19', '2015-12-09 13:15:00', 'dodawanie-i-edycja-menu', 'Dodawanie i edycja menu', '', 'Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną trybiku. W trybie edycji widoczne są narzędzia do obsługi bloków, najeżdżamy kursorem na przycisk z ikoną plusa. Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, które można użyć do tworzenia strony. Do dodawania menu do strony przygotowane są trzy bloki, w', '<p><strong>Dodawanie i edycja menu &ndash; menu gł&oacute;wne, menu nawigacyjne i menu skr&oacute;t&oacute;w.</strong></p>\n\n<p>Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną puzla.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px" /></p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png" /></p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony. Do dodawania menu do strony przygotowane są trzy bloki, wstawmy pierwszy z nich &ndash; kolejne będą wstawiane analogicznie. Wyszukujemy blok o nazwie &bdquo;Menu gł&oacute;wne&rdquo;, można w tym celu użyć wyszukiwarki blok&oacute;w np. wybierając z listy &bdquo;Typ bloku&rdquo; pozycję &bdquo;Menu gł&oacute;wne&rdquo;. Następnie chwytamy myszką ikonę zaznaczoną na zdjęciu poniżej i przeciągamy blok w wybrany na stronie slot na bloki.</p>\n\n<p>Przechodzimy do ustawień klikając na belce bloku ikonę trybiku.</p>\n\n<p>Otworzy nam się okno z ustawieniami dla tego bloku.</p>\n\n<p>1. Tytuł -ustawiamy rozmiar tytułu naszej galerii oraz czy ma on być widoczny .</p>\n\n<p>2. Kategoria &ndash;wybieramy kategorię, do kt&oacute;rej są przypisywane elementy menu, w systemie stworzona jest &bdquo;Domyślna kategoria&rdquo; gotowa do użycia.</p>\n\n<p>3. Orientacja &ndash; możemy wybrać jak ma się wyświetlać nasze menu &ndash; poziomo lub pionowo.</p>\n\n<p>4. Pozostałe opcje &ndash; tu możemy dodać przycisk (mała ikona plusa) rozwijania/zwijania do każdej pozycji menu.</p>\n\n<p>Aby dodać elementy do menu klikamy na belce bloku ikonę folderu &ndash; okno zarządzania.</p>\n\n<p>Następnie przycisk na otwartym oknie &bdquo;Kategorię&rdquo;, jeśli chcemy stworzyć inną niż domyślną kategorię dla naszych element&oacute;w menu lub przycisk &bdquo;Nowy element&rdquo; żeby dodać pozycję do menu.</p>\n\n<p>W formularzu dodawania nowego elementu po kolei zmieniamy ustawienia.</p>\n\n<p>1. Kategoria &ndash; wybieramy kategorię element&oacute;w menu.</p>\n\n<p>2. Element nadrzędny &ndash; tą opcją kontrolujemy ustawienie poszczeg&oacute;lnych element&oacute;w menu względem siebie. Możemy wybrać, przy każdym nowym elemencie menu, kt&oacute;ry element ma być nadrzędny względem wstawianego.</p>\n\n<p>3. Typ adresu URL &ndash; wybieramy wewnętrzny, zewnętrzny lub brak.</p>\n\n<p>4. Adres URL &ndash; wybieramy dokąd ma prowadzić element menu, jeśli w typie adresu URL wybrana została pozycja &bdquo;wewnętrzny&rdquo; dostępna tu będzie lista wszystkich naszych stron, jeśli wybrane zostało &bdquo;brak&rdquo; to pole będzie nieczynne.</p>\n\n<p>5. Typ elementu &ndash; możemy zdecydować jak będzie się prezentować nasz element menu. DO wyboru jest kilka opcji:</p>\n\n<p>- tylko tekst</p>\n\n<p>- tylko ikona</p>\n\n<p>- tylko obraz</p>\n\n<p>- ikona + tekst</p>\n\n<p>- obraz + tekst</p>\n\n<p>6. Tytuł &ndash; wpisujemy tytuł naszego elementu menu.</p>\n\n<p>W ten spos&oacute;b dodaliśmy pierwszą pozycje w menu, analogicznie tworzymy kolejne, dodając grupy i elementy menu.</p>\n\n<p>Menu nawigacyjne i menu skr&oacute;t&oacute;w posiadają takie same ustawienia i okna zarządzania, r&oacute;żnią się jedynie wyświetlaniem. Zdjęcie poniżej przedstawia zestawienie trzech typ&oacute;w menu obok siebie.</p>\n\n<p>&nbsp;</p>');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_news_list_categories`
--

CREATE TABLE `block_news_list_categories` (
  `category_id` smallint(5) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED DEFAULT NULL,
  `is_published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `name` varchar(32) COLLATE utf8_polish_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `block_news_list_categories`
--

INSERT INTO `block_news_list_categories` (`category_id`, `content_id`, `is_published`, `last_update_author_id`, `last_update_date`, `name`) VALUES
(1, 770, 1, 1, '2015-12-09 10:00:29', 'Aktualności');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_news_list_news_images`
--

CREATE TABLE `block_news_list_news_images` (
  `image_id` mediumint(8) NOT NULL,
  `content_id` int(10) UNSIGNED NOT NULL,
  `news_id` int(10) UNSIGNED NOT NULL,
  `attachment_id` mediumint(8) UNSIGNED NOT NULL COMMENT 'Image.',
  `is_main` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `order` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `block_news_list_news_images`
--

INSERT INTO `block_news_list_news_images` (`image_id`, `content_id`, `news_id`, `attachment_id`, `is_main`, `order`, `last_update_author_id`, `last_update_date`, `title`) VALUES
(1, 770, 1, 798, 1, 3, 1, '2015-12-09 13:02:51', ''),
(2, 770, 1, 800, 0, 2, 1, '2015-12-09 13:02:51', ''),
(3, 770, 1, 802, 0, 1, 1, '2015-12-09 13:02:51', ''),
(4, 770, 1, 804, 0, 0, 1, '2015-12-09 13:02:51', ''),
(5, 770, 2, 806, 1, 0, 1, '2015-12-09 13:19:44', ''),
(6, 770, 3, 810, 1, 0, 1, '2015-12-09 13:27:42', ''),
(7, 770, 4, 812, 1, 0, 1, '2015-12-09 13:26:12', ''),
(8, 770, 5, 814, 1, 0, 1, '2015-12-09 13:32:37', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_offer_degrees`
--

CREATE TABLE `block_offer_degrees` (
  `degree_id` tinyint(3) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED DEFAULT NULL,
  `is_published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `type` tinyint(2) UNSIGNED NOT NULL DEFAULT '3',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `title` varchar(128) COLLATE utf8_polish_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_offer_majors`
--

CREATE TABLE `block_offer_majors` (
  `major_id` smallint(5) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED DEFAULT NULL,
  `degree_id` tinyint(3) DEFAULT NULL,
  `is_published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_stationary` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `is_remote` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `order` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `bootstrap_glyphicon` varchar(32) COLLATE utf8_polish_ci NOT NULL DEFAULT 'asterisk',
  `title` varchar(32) COLLATE utf8_polish_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_popup`
--

CREATE TABLE `block_popup` (
  `content_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `contents` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `block_popup`
--

INSERT INTO `block_popup` (`content_id`, `last_update_author_id`, `last_update_date`, `title`, `contents`) VALUES
(787, NULL, NULL, 'Prezentacja Systemu Zarządzania Treścią CodiLab', '<div>\n<video controls="controls" height="auto" id="video2016016105733" poster="/ckeditor-files/images/grafiki%20codilab/home-bg-2.png" width="100%"><source src="/ckeditor-files/files/codilab-cms-1024x768.mp4" type="video/mp4" />Your browser doesn&#39;t support video.<br />\nPlease download the file: <a href="/ckeditor-files/files/codilab-cms-1024x768.mp4">video/mp4</a></video>\n</div>\n\n<p>&nbsp;</p>');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_postgraduate_studies`
--

CREATE TABLE `block_postgraduate_studies` (
  `content_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `duration` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `language` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `to_whom` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_postgraduate_studies_tabs`
--

CREATE TABLE `block_postgraduate_studies_tabs` (
  `tab_id` smallint(5) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED DEFAULT NULL,
  `order` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `name` varchar(128) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `contents` text COLLATE utf8_polish_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `block_tabs_control`
--

CREATE TABLE `block_tabs_control` (
  `tab_id` smallint(5) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED DEFAULT NULL,
  `is_published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `order` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `name` varchar(128) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `contents` text COLLATE utf8_polish_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `block_tabs_control`
--

INSERT INTO `block_tabs_control` (`tab_id`, `content_id`, `is_published`, `order`, `last_update_author_id`, `last_update_date`, `name`, `contents`) VALUES
(1, 784, 0, 1, 1, '2016-01-11 14:27:00', 'CKEditor', '<p><strong>Korzystanie z CKEditora.</strong></p>\n\n<p>CKEditor jest edytorem o dużych możliwościach, jego wszystkie narzędzia dostępne są po dwukrotnie kliknąć myszką na tekst &bdquo;Przykładowy tekst. Proszę go zmienić.&rdquo; lub ikonę edycji.</p>\n\n<p>CKEditor&nbsp;jest wizualnym&nbsp;edytorem HTML&nbsp;rozpowszechnianym na licencji&nbsp;Open Source, kt&oacute;ry pozwala na łatwe wprowadzanie tekstu za pomocą&nbsp;interfejsu&nbsp;przypominającego programy typu&nbsp;Microsoft Word.&nbsp;Formatowanie tekstu&nbsp;odbywa się za pomocą zestawu przycisk&oacute;w umożliwiających wybranie&nbsp;czcionki, rozmiaru, koloru liter i tła, ustawienie pogrubienia, podkreślenia czy&nbsp;kursywy, jak r&oacute;wnież wyr&oacute;wnanie tekstu (do lewej, do prawej, wycentrowanie,&nbsp;wyjustowanie). Poza tym użytkownik ma do dyspozycji funkcje takie jak wstawienie listy (wypunktowanej lub numerowanej), formularzy i wszystkich jego element&oacute;w, tabeli, obrazka, video czy odnośnika (linku).</p>\n\n<p>Poniżej znajdują się wszystkie przyciski CKEditora, wymienione z nazw w kierunku od lewej do prawej.</p>\n\n<p>Pogrubienie, kursywa, podkreślenie, przekreślenie, indeks dolny, indeks g&oacute;rny, usuń formatowanie, lista numerowana, lista wypunktowana, zmniejsz wcięcie, zwiększ wcięcie, cytat, utw&oacute;rz pojemnik Div, wyr&oacute;wnaj do lewej, wyśrodkuj, wyr&oacute;wnaj do prawej, wyjustuj, kierunek tekstu od lewej strony do prawej, kierunek tekstu od prawej strony do lewej, set language, wstaw/edytuj odnośnik, usuń odnośnik, wstaw/edytuj kotwicę, obrazek, flash, tabela, wstaw poziomą linie, wstaw znak specjalny, wstaw podział strony, iFrame, załącznik video z Youtube.</p>\n\n<p>Szablony, wytnij, kopiuj, wklej, wklej jako zwykły tekst, wklej z programu MS Word, cofnij, pon&oacute;w, znajdź, zamień, zaznacz wszystko, sprawdź pisownię podczas pisania, formularz, pole wyboru (checkbox), przycisk opcji (radio), pole tekstowe, obszar tekstowy, lista wyboru, przycisk, przycisk graficzny, pole ukryte.</p>\n\n<p>Style formatujące, format, czcionka, rozmiar, kolor tekstu, kolor tła, pokaż bloki, pokaż informacje o CKEditor.</p>'),
(2, 784, 0, 2, 1, '2015-12-09 13:40:00', 'Ustawienia bloków', '<p><strong>Ustawienia blok&oacute;w.</strong></p>\n\n<p>Każdy blok wstawiony na stronę po najechaniu na niego kursorem wyświetli kilka opcji na swojej belce.</p>\n\n<p>W lewym g&oacute;rnym rogu przed numerem ID i nazwą znajduje się &bdquo;krzyżyk&rdquo; służący do przerzucenia bloku do innego slotu. Działanie ikon z prawej strony przedstawia opis poniżej:</p>\n\n<p>1. Zapisz jako blok dynamiczny &ndash; domyślnie wstawiony blok jest widoczny tylko na stronie, na kt&oacute;rej go wstawiliśmy, gotowy wypełniony naszymi treściami blok można zapisać jako dynamiczny i dodać go potem na innej stronie bez potrzeby tworzenia go od początku. Zapisany w ten spos&oacute;b blok jest dostępny po najechaniu kursorem na ikonę dyskietki umieszczoną pod ikoną plusa do dodawania blok&oacute;w.</p>\n\n<p>2. Opublikuj/ukryj blok &ndash; klikając na tą ikonę można ukryć blok, w momencie wyjścia z trybu projektowego nie będzie on widoczny, po ponownym kliknięciu na tą ikonę, blok będzie opublikowany.</p>\n\n<p>3. Okno zarządzania &ndash; po kliknięciu otworzy się okno zarządzania blokiem, w przypadku bloku menu gł&oacute;wnego będą to ustawienia dotyczące menu, dodawanie, usuwanie, ukrywanie, publikacja i edycja kategorii menu oraz jego element&oacute;w.</p>\n\n<p>4. Ustawienia &ndash; pod tą ikoną kryją się og&oacute;lne ustawienia, m. in. Rozmiar tytułu menu i czy ma być widoczny, z kt&oacute;rej kategorii mają być wyświetlane elementy menu, czy ma być to menu pionowe bądź poziome.</p>\n\n<p>5. Usuwanie bloku &ndash; kliknięcie na ikonę kosza otworzy okno z potwierdzeniem usunięcia bloku, zostanie on usunięty ze strony a nie listy blok&oacute;w. &nbsp; &nbsp;</p>'),
(3, 784, 0, 3, 1, '2016-01-04 15:37:22', 'O bloku Zakładki', '<p>Blok Zakładki jest prosty w zarządzaniu.&nbsp;</p>\n\n<p>Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną puzla.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px" /></p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png" /></p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony, wyszukujemy blok o nazwie &bdquo;Zakładki&rdquo;, można w tym celu użyć wyszukiwarki blok&oacute;w np. wybierając z listy &bdquo;Typ bloku&rdquo; pozycję &bdquo;Zakładki&rdquo;. Następnie chwytamy myszką ikonę strzałki skierowanej ku g&oacute;rze (pojawi się po najechaniu myszką na blok Zakładek) i przeciągamy blok w wybrany na stronie slot na bloki.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_19_33.png" /></p>\n\n<p>Wprowadzamy treść w jednej zakładce, jeśli chcemy dodajemy kolejną zakładkę klikając ikonę plusa przy zakładce.</p>');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `languages`
--

CREATE TABLE `languages` (
  `language_id` smallint(5) UNSIGNED NOT NULL,
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `zend2_locale` char(5) COLLATE utf8_polish_ci NOT NULL DEFAULT 'en_US',
  `iso_code` varchar(3) COLLATE utf8_polish_ci NOT NULL DEFAULT '' COMMENT 'ISO 639-2 Code.',
  `html_iso_code` varchar(5) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `english_name` varchar(64) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `native_name` varchar(64) COLLATE utf8_polish_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `languages`
--

INSERT INTO `languages` (`language_id`, `last_update_author_id`, `last_update_date`, `zend2_locale`, `iso_code`, `html_iso_code`, `english_name`, `native_name`) VALUES
(1, NULL, '2014-02-20 19:52:59', 'en_US', 'eng', 'en-us', 'English (US)', 'English (US)'),
(2, NULL, '2014-02-20 19:52:59', 'pl_PL', 'pol', 'pl', 'Polish', 'Polski'),
(3, NULL, '2014-02-20 19:53:00', 'ru_RU', 'rus', 'ru', 'Russian', 'Русский');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `layouts`
--

CREATE TABLE `layouts` (
  `layout_id` smallint(5) UNSIGNED NOT NULL,
  `file_path` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `layouts`
--

INSERT INTO `layouts` (`layout_id`, `file_path`) VALUES
(1, 'layout.phtml');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `layouts_properties`
--

CREATE TABLE `layouts_properties` (
  `properties_id` mediumint(8) UNSIGNED NOT NULL,
  `layout_id` smallint(5) UNSIGNED NOT NULL,
  `language_id` smallint(5) UNSIGNED DEFAULT NULL,
  `name` varchar(64) COLLATE utf8_polish_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `layouts_properties`
--

INSERT INTO `layouts_properties` (`properties_id`, `layout_id`, `language_id`, `name`) VALUES
(1, 1, 1, 'Default'),
(2, 1, 2, 'Domyślny'),
(3, 1, 3, 'дефолт');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pages`
--

CREATE TABLE `pages` (
  `page_id` smallint(5) UNSIGNED NOT NULL,
  `parent_id` smallint(5) UNSIGNED DEFAULT NULL,
  `language_id` smallint(5) UNSIGNED DEFAULT NULL,
  `layout_id` smallint(5) UNSIGNED DEFAULT NULL,
  `sub_layout_id` smallint(5) UNSIGNED DEFAULT NULL,
  `is_published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `views` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `url_priority` decimal(2,1) UNSIGNED NOT NULL DEFAULT '0.5' COMMENT 'URL priority for search engines. Can be used also in sitemap.xml.',
  `allow_indexing` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Allow indexing by search engines.',
  `allow_backing_up` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Allow making copy by search engines.',
  `disallow_caching` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Disallow caching page in web browser.',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `clean_url` varchar(128) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `frequency_of_changes` varchar(16) COLLATE utf8_polish_ci NOT NULL DEFAULT 'always',
  `title` varchar(128) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `meta_keywords` varchar(512) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `meta_description` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `pages`
--

INSERT INTO `pages` (`page_id`, `parent_id`, `language_id`, `layout_id`, `sub_layout_id`, `is_published`, `views`, `url_priority`, `allow_indexing`, `allow_backing_up`, `disallow_caching`, `last_update_author_id`, `last_update_date`, `clean_url`, `frequency_of_changes`, `title`, `meta_keywords`, `meta_description`) VALUES
(248, NULL, 2, 1, 2, 1, 63, 0.5, 1, 1, 0, 1, '2015-12-09 09:09:16', '', 'always', 'Start', '', ''),
(251, NULL, 1, 1, 4, 1, 0, 0.5, 1, 1, 0, 1, '2015-12-08 10:36:30', '', 'always', 'Start', '', ''),
(252, NULL, 3, 1, 4, 1, 0, 0.5, 1, 1, 0, 1, '2015-12-08 10:37:03', '', 'always', 'Start', '', ''),
(253, NULL, 2, 1, 4, 1, 3, 0.5, 1, 1, 0, 1, '2015-12-08 10:38:22', 'blok-tekstowy', 'always', 'Blok tekstowy', '', ''),
(254, NULL, 2, 1, 4, 1, 6, 0.5, 1, 1, 0, 1, '2015-12-08 11:09:02', 'akordeon', 'always', 'Akordeon', '', ''),
(255, NULL, 2, 1, 4, 1, 3, 0.5, 1, 1, 0, 1, '2015-12-08 11:10:20', 'wydarzenia', 'always', 'Wydarzenia', '', ''),
(256, NULL, 2, 1, 4, 1, 2, 0.5, 1, 1, 0, 1, '2015-12-08 11:12:09', 'aktualnosci', 'always', 'Aktualności', '', ''),
(257, 255, 2, 1, 4, 1, 1, 0.5, 1, 1, 0, 1, '2015-12-08 11:57:24', 'detale-wydarzenia', 'always', 'Detale wydarzenia', '', ''),
(258, 256, 2, 1, 4, 1, 2, 0.5, 1, 1, 0, 1, '2015-12-09 10:03:22', 'detale-aktualnosci', 'always', 'Detale aktualności', '', ''),
(259, 255, 2, 1, 4, 1, 0, 0.5, 1, 1, 0, 1, '2015-12-09 10:21:29', 'archiwum-wydarzen', 'always', 'Archiwum wydarzeń', '', ''),
(260, 255, 2, 1, 4, 1, 3, 0.5, 1, 1, 0, 1, '2015-12-09 10:21:53', 'nadchodzace-wydarzenia', 'always', 'Nadchodzące wydarzenia', '', ''),
(262, NULL, 2, 1, 4, 1, 3, 0.5, 1, 1, 0, 1, '2015-12-09 12:21:28', 'grafika', 'always', 'Grafika', '', ''),
(263, 262, 2, 1, 4, 1, 4, 0.5, 1, 1, 0, 1, '2015-12-09 12:22:39', 'galeria-obrazow', 'always', 'galeria obrazów', '', ''),
(264, 262, 2, 1, 4, 1, 1, 0.5, 1, 1, 0, 1, '2015-12-09 12:23:13', 'slider-obrazow', 'always', 'slider obrazów', '', ''),
(265, 262, 2, 1, 4, 1, 5, 0.5, 1, 1, 0, 1, '2015-12-09 12:23:30', 'stos-obrazow', 'always', 'stos obrazów', '', ''),
(266, 262, 2, 1, 4, 1, 0, 0.5, 1, 1, 0, 1, '2015-12-09 12:23:59', 'duzy-baner', 'always', 'duży baner', '', ''),
(267, NULL, 2, 1, 4, 1, 0, 0.5, 1, 1, 0, 1, '2015-12-09 12:24:26', 'maly-baner', 'always', 'mały baner', '', ''),
(268, NULL, 2, 1, 4, 1, 5, 0.5, 1, 1, 0, 1, '2016-01-04 13:53:19', 'lista-wszystkich-artykulow', 'always', 'Lista wszystkich artykułów', '', ''),
(269, NULL, 2, 1, 4, 1, 0, 0.5, 1, 1, 0, 1, '2016-01-04 14:46:53', 'test', 'always', 'test', '', ''),
(270, NULL, 2, 1, 1, 1, 6, 0.5, 1, 1, 0, 1, '2016-01-05 13:42:40', 'o-nas', 'always', 'o nas', '', ''),
(271, NULL, 2, 1, 4, 1, 1, 0.5, 1, 1, 0, 1, '2016-01-07 10:55:05', 'pusta', 'always', 'pusta', '', ''),
(272, NULL, 2, 1, 4, 1, 2, 0.5, 1, 1, 0, 1, '2016-01-07 12:18:32', 'wyniki-wyszukiwania', 'always', 'wyniki-wyszukiwania', '', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pages_blocks`
--

CREATE TABLE `pages_blocks` (
  `page_block_id` int(10) UNSIGNED NOT NULL,
  `page_id` smallint(5) UNSIGNED DEFAULT NULL,
  `block_id` smallint(5) UNSIGNED DEFAULT NULL,
  `is_parent_block_dynamic` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `slot_number` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Slot id in HTML DOM.',
  `order` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `is_published` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Unpublished blocks are visible only in project mode.',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `config` text COLLATE utf8_polish_ci NOT NULL COMMENT 'Config in XML format.'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `pages_blocks`
--

INSERT INTO `pages_blocks` (`page_block_id`, `page_id`, `block_id`, `is_parent_block_dynamic`, `slot_number`, `order`, `is_published`, `last_update_author_id`, `last_update_date`, `config`) VALUES
(2919, 248, 95, 1, 5, 84, 1, 1, '2015-12-08 11:12:46', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[4]]></titleSize><titleVisible><![CDATA[0]]></titleVisible><categoryId><![CDATA[114]]></categoryId><orientation><![CDATA[horizontal]]></orientation></xml>'),
(2920, 253, 95, 1, 5, 82, 1, 1, '2015-12-08 11:25:15', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[4]]></titleSize><titleVisible><![CDATA[0]]></titleVisible><categoryId><![CDATA[114]]></categoryId><orientation><![CDATA[horizontal]]></orientation></xml>'),
(2921, 254, 95, 1, 5, 81, 1, 1, '2015-12-08 11:25:15', ''),
(2922, 255, 95, 1, 5, 80, 1, 1, '2015-12-08 11:25:15', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[4]]></titleSize><titleVisible><![CDATA[0]]></titleVisible><categoryId><![CDATA[114]]></categoryId><orientation><![CDATA[horizontal]]></orientation></xml>'),
(2923, 256, 95, 1, 5, 77, 1, 1, '2015-12-08 11:25:15', ''),
(2924, 255, 32, 0, 10, 79, 1, 1, '2015-12-08 11:25:47', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[1]]></titleVisible><itemCountPerPage><![CDATA[5]]></itemCountPerPage><categoryId><![CDATA[6]]></categoryId><detailsPageId><![CDATA[257]]></detailsPageId><defaultSubjectForMailAboutEventApproval><![CDATA[]]></defaultSubjectForMailAboutEventApproval><defaultContentsForMailAboutEventApproval><![CDATA[]]></defaultContentsForMailAboutEventApproval><contactPersonEmail><![CDATA[]]></contactPersonEmail><contactPersonPhone><![CDATA[]]></contactPersonPhone><subjectForMailForUserAboutUserSignedUpForEvent><![CDATA[Dziękujemy za zapisanie się na wydarzenie]]></subjectForMailForUserAboutUserSignedUpForEvent><contentsForMailForUserAboutUserSignedUpForEvent><![CDATA[<p><strong>Zapisałeś się na wydarzenie:</strong><br />\n&quot;{{event_title}}&quot; - {{event_link}}</p>\n\n<p>Start: {{event_start_date}} &nbsp;o godzinie {{event_start_hour}}</p>\n\n<p>Gdzie: {{event_place}}</p>\n\n<p><strong>Dane zgłaszającego:</strong></p>\n\n<p>Tytuł naukowy: {{user_academic_title}}</p>\n\n<p>Imie Nawisko:&nbsp;{{user_name_and_surname}}</p>\n\n<p>Instytucja:&nbsp;{{user_institution_name}}</p>\n\n<p>Dane do fakt.:&nbsp;{{user_invoice_data}}</p>\n\n<p>Telefon:&nbsp;{{user_phone}}</p>\n\n<p>Email:&nbsp;{{user_email}}</p>\n\n<p>Uwagi:&nbsp;{{user_attentions}}</p>\n\n<hr />\n<p>W razie pytań lub niejasności prosimy o kontakt maliowy pod adres {{contact_email}} lub {{contact_phone}}</p>]]></contentsForMailForUserAboutUserSignedUpForEvent><subjectForMailForAdminAboutUserSignedUpForEvent><![CDATA[Nowy użytkownik zapisał się na wydarzenie]]></subjectForMailForAdminAboutUserSignedUpForEvent><contentsForMailForAdminAboutUserSignedUpForEvent><![CDATA[<p><strong>Nowy zapis na wydarzenie:</strong><br />\n&quot;{{event_title}}&quot; - {{event_link}}</p>\n\n<p>Start: {{event_start_date}} &nbsp;o godzinie {{event_start_hour}}</p>\n\n<p>Gdzie: {{event_place}}</p>\n\n<p><strong>Dane zgłaszającego:</strong></p>\n\n<p>Tytuł naukowy: {{user_academic_title}}</p>\n\n<p>Imie Nawisko:&nbsp;{{user_name_and_surname}}</p>\n\n<p>Instytucja:&nbsp;{{user_institution_name}}</p>\n\n<p>Dane do fakt.:&nbsp;{{user_invoice_data}}</p>\n\n<p>Telefon:&nbsp;{{user_phone}}</p>\n\n<p>Email:&nbsp;{{user_email}}</p>\n\n<p>Uwagi:&nbsp;{{user_attentions}}</p>\n\n<hr />\n<p>Przejdź do listy zgłoszeń: {{event_list_link}}</p>]]></contentsForMailForAdminAboutUserSignedUpForEvent></xml>'),
(2925, 257, 34, 0, 10, 31, 1, 1, '2015-12-08 12:01:32', ''),
(2926, 257, 95, 1, 5, 71, 1, 1, '2015-12-08 12:02:28', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[4]]></titleSize><titleVisible><![CDATA[0]]></titleVisible><categoryId><![CDATA[114]]></categoryId><orientation><![CDATA[horizontal]]></orientation></xml>'),
(2927, 248, 97, 1, 6, 76, 1, 1, '2015-12-09 09:05:00', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[0]]></titleVisible><animationTime><![CDATA[3000]]></animationTime><timeBetweenSlides><![CDATA[7000]]></timeBetweenSlides><animationType><![CDATA[fadeout]]></animationType></xml>'),
(2929, 248, 4, 0, 20, 1, 1, 1, '2015-12-09 09:09:30', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[1]]></titleVisible><itemCountPerPage><![CDATA[5]]></itemCountPerPage><eventListPageBlockId><![CDATA[2924]]></eventListPageBlockId><eventListCategoryId><![CDATA[0]]></eventListCategoryId></xml>'),
(2930, 248, 20, 0, 24, 32, 1, 1, '2015-12-09 09:59:39', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[2]]></titleSize><titleVisible><![CDATA[1]]></titleVisible><newsListPageBlockId><![CDATA[2931]]></newsListPageBlockId><newsListCategoryId><![CDATA[0]]></newsListCategoryId><totalItemCount><![CDATA[10]]></totalItemCount><slideIntervalInMs><![CDATA[5000]]></slideIntervalInMs><animationsTimeInMs><![CDATA[1500]]></animationsTimeInMs><wrappingType><![CDATA[last]]></wrappingType></xml>'),
(2931, 256, 3, 0, 10, 72, 1, 1, '2015-12-09 10:00:06', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[1]]></titleVisible><itemCountPerPage><![CDATA[5]]></itemCountPerPage><detailsPageId><![CDATA[258]]></detailsPageId></xml>'),
(2932, 258, 95, 1, 5, 67, 1, 1, '2015-12-09 10:03:39', ''),
(2933, 254, 65, 0, 10, 70, 1, 1, '2015-12-09 10:09:50', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[1]]></titleVisible></xml>'),
(2934, 253, 1, 0, 10, 69, 1, 1, '2015-12-09 10:32:19', ''),
(2935, 253, 96, 1, 20, 68, 1, 1, '2015-12-09 11:53:29', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[1]]></titleVisible><categoryId><![CDATA[115]]></categoryId><orientation><![CDATA[vertical]]></orientation><displayUncollapseButtons><![CDATA[1]]></displayUncollapseButtons></xml>'),
(2936, 254, 96, 1, 20, 66, 1, 1, '2015-12-09 12:03:59', ''),
(2937, 256, 96, 1, 20, 65, 1, 1, '2015-12-09 12:03:59', ''),
(2938, 257, 96, 1, 20, 64, 1, 1, '2015-12-09 12:03:59', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[1]]></titleVisible><categoryId><![CDATA[115]]></categoryId><orientation><![CDATA[vertical]]></orientation><displayUncollapseButtons><![CDATA[1]]></displayUncollapseButtons></xml>'),
(2939, 258, 96, 1, 20, 63, 1, 1, '2015-12-09 12:03:59', ''),
(2940, 259, 96, 1, 20, 62, 1, 1, '2015-12-09 12:03:59', ''),
(2941, 260, 96, 1, 20, 61, 1, 1, '2015-12-09 12:03:59', ''),
(2943, 255, 96, 1, 20, 59, 1, 1, '2015-12-09 12:04:52', ''),
(2944, 262, 96, 1, 20, 58, 1, 1, '2015-12-09 12:29:11', ''),
(2945, 263, 96, 1, 20, 57, 1, 1, '2015-12-09 12:29:11', ''),
(2946, 264, 96, 1, 20, 56, 1, 1, '2015-12-09 12:29:11', ''),
(2947, 265, 96, 1, 20, 55, 1, 1, '2015-12-09 12:29:11', ''),
(2948, 266, 96, 1, 20, 54, 1, 1, '2015-12-09 12:29:11', ''),
(2949, 267, 96, 1, 20, 53, 1, 1, '2015-12-09 12:29:11', ''),
(2950, 259, 95, 1, 5, 52, 1, 1, '2015-12-09 12:29:38', ''),
(2951, 260, 95, 1, 5, 51, 1, 1, '2015-12-09 12:29:39', ''),
(2953, 262, 95, 1, 5, 49, 1, 1, '2015-12-09 12:29:39', ''),
(2954, 263, 95, 1, 5, 48, 1, 1, '2015-12-09 12:29:39', ''),
(2955, 264, 95, 1, 5, 47, 1, 1, '2015-12-09 12:29:39', ''),
(2956, 266, 95, 1, 5, 46, 1, 1, '2015-12-09 12:29:39', ''),
(2957, 267, 95, 1, 5, 36, 1, 1, '2015-12-09 12:29:39', ''),
(2958, 263, 92, 0, 10, 45, 1, 1, '2015-12-09 12:30:10', ''),
(2959, 263, 1, 0, 10, 44, 1, 1, '2015-12-09 12:32:25', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[0]]></titleVisible></xml>'),
(2960, 264, 66, 0, 10, 43, 1, 1, '2015-12-09 12:34:45', ''),
(2961, 264, 1, 0, 10, 42, 1, 1, '2015-12-09 12:37:05', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[0]]></titleVisible></xml>'),
(2962, 265, 1, 0, 10, 38, 1, 1, '2015-12-09 12:40:15', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[0]]></titleVisible></xml>'),
(2963, 266, 1, 0, 10, 40, 1, 1, '2015-12-09 12:40:51', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[0]]></titleVisible></xml>'),
(2964, 267, 1, 0, 10, 36, 1, 1, '2015-12-09 12:41:14', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[0]]></titleVisible></xml>'),
(2965, 265, 70, 0, 10, 39, 1, 1, '2015-12-09 12:45:06', ''),
(2966, 266, 97, 1, 6, 24, 1, 1, '2015-12-09 12:54:20', ''),
(2967, 267, 30, 0, 10, 37, 1, 1, '2015-12-09 12:56:38', ''),
(2968, 258, 12, 0, 10, 26, 1, 1, '2015-12-09 13:06:32', ''),
(2969, 248, 10, 0, 10, 34, 1, 1, '2015-12-09 13:37:22', ''),
(2970, 262, 1, 0, 10, 33, 1, 1, '2015-12-09 15:24:03', ''),
(2972, 248, 93, 0, 24, 31, 1, 1, '2015-12-09 15:31:17', ''),
(2973, 257, 1, 0, 10, 30, 1, 1, '2016-01-04 12:43:28', ''),
(2974, 259, 33, 0, 10, 29, 1, 1, '2016-01-04 13:02:46', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[1]]></titleVisible><itemCountPerPage><![CDATA[5]]></itemCountPerPage><eventListPageBlockId><![CDATA[2924]]></eventListPageBlockId><eventListCategoryId><![CDATA[0]]></eventListCategoryId></xml>'),
(2975, 259, 1, 0, 10, 28, 1, 1, '2016-01-04 13:04:24', ''),
(2976, 260, 4, 0, 10, 27, 1, 1, '2016-01-04 13:08:18', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[1]]></titleVisible><itemCountPerPage><![CDATA[5]]></itemCountPerPage><eventListPageBlockId><![CDATA[2924]]></eventListPageBlockId><eventListCategoryId><![CDATA[0]]></eventListCategoryId></xml>'),
(2977, 260, 1, 0, 10, 26, 1, 1, '2016-01-04 13:08:40', ''),
(2978, 258, 1, 0, 10, 25, 1, 1, '2016-01-04 13:10:20', ''),
(2979, 265, 95, 1, 5, 23, 1, 1, '2016-01-04 13:56:39', ''),
(2980, 268, 95, 1, 5, 22, 1, 1, '2016-01-04 13:56:39', ''),
(2981, 268, 96, 1, 20, 13, 1, 1, '2016-01-04 13:57:04', ''),
(2983, 268, 65, 0, 10, 21, 1, 1, '2016-01-04 14:26:48', ''),
(2985, 248, 66, 0, 27, 16, 1, 1, '2016-01-05 12:43:07', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[0]]></titleVisible><imageWidth><![CDATA[176]]></imageWidth><imageHeight><![CDATA[50]]></imageHeight><imageInGrayscale><![CDATA[1]]></imageInGrayscale><slideIntervalInMs><![CDATA[5000]]></slideIntervalInMs><animationsTimeInMs><![CDATA[1500]]></animationsTimeInMs><wrappingType><![CDATA[circular]]></wrappingType></xml>'),
(2986, 270, 79, 0, 10, 15, 1, 1, '2016-01-05 13:42:51', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[0]]></titleVisible></xml>'),
(2987, 270, 71, 0, 10, 14, 1, 1, '2016-01-05 13:43:36', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[0]]></titleVisible><detailsPageId><![CDATA[270]]></detailsPageId></xml>'),
(2988, 270, 95, 1, 5, 0, 1, 1, '2016-01-05 13:53:17', ''),
(2991, 272, 8, 0, 10, 10, 1, 1, '2016-01-07 12:18:55', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[0]]></titleVisible><highlightFoundWords><![CDATA[1]]></highlightFoundWords></xml>'),
(2992, 272, 9, 0, 24, 9, 1, 1, '2016-01-07 12:19:27', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[0]]></titleVisible><tagType><![CDATA[predefined]]></tagType><predefinedTag><![CDATA[h3]]></predefinedTag><customTag><![CDATA[]]></customTag></xml>'),
(2995, 271, 65, 0, 10, 1, 1, 1, '2016-01-11 15:42:10', ''),
(2996, 248, 1, 0, 20, 2, 1, 1, '2016-01-15 18:07:30', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pages_blocks_contents`
--

CREATE TABLE `pages_blocks_contents` (
  `content_id` int(10) UNSIGNED NOT NULL,
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `title` text COLLATE utf8_polish_ci NOT NULL,
  `config` text COLLATE utf8_polish_ci NOT NULL COMMENT 'Config in XML format.',
  `content` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `pages_blocks_contents`
--

INSERT INTO `pages_blocks_contents` (`content_id`, `last_update_author_id`, `last_update_date`, `title`, `config`, `content`) VALUES
(763, 1, '2015-12-08 11:12:46', 'Menu główne', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[4]]></titleSize><titleVisible><![CDATA[0]]></titleVisible><categoryId><![CDATA[114]]></categoryId><orientation><![CDATA[horizontal]]></orientation></xml>', 'Nie podpięto jeszcze żadnych stron do tego menu.'),
(764, 1, '2015-12-08 11:25:47', 'Lista wydarzeń', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[1]]></titleVisible><itemCountPerPage><![CDATA[5]]></itemCountPerPage><categoryId><![CDATA[6]]></categoryId><detailsPageId><![CDATA[257]]></detailsPageId><defaultSubjectForMailAboutEventApproval><![CDATA[]]></defaultSubjectForMailAboutEventApproval><defaultContentsForMailAboutEventApproval><![CDATA[]]></defaultContentsForMailAboutEventApproval><contactPersonEmail><![CDATA[]]></contactPersonEmail><contactPersonPhone><![CDATA[]]></contactPersonPhone><subjectForMailForUserAboutUserSignedUpForEvent><![CDATA[Dziękujemy za zapisanie się na wydarzenie]]></subjectForMailForUserAboutUserSignedUpForEvent><contentsForMailForUserAboutUserSignedUpForEvent><![CDATA[<p><strong>Zapisałeś się na wydarzenie:</strong><br />\n&quot;{{event_title}}&quot; - {{event_link}}</p>\n\n<p>Start: {{event_start_date}} &nbsp;o godzinie {{event_start_hour}}</p>\n\n<p>Gdzie: {{event_place}}</p>\n\n<p><strong>Dane zgłaszającego:</strong></p>\n\n<p>Tytuł naukowy: {{user_academic_title}}</p>\n\n<p>Imie Nawisko:&nbsp;{{user_name_and_surname}}</p>\n\n<p>Instytucja:&nbsp;{{user_institution_name}}</p>\n\n<p>Dane do fakt.:&nbsp;{{user_invoice_data}}</p>\n\n<p>Telefon:&nbsp;{{user_phone}}</p>\n\n<p>Email:&nbsp;{{user_email}}</p>\n\n<p>Uwagi:&nbsp;{{user_attentions}}</p>\n\n<hr />\n<p>W razie pytań lub niejasności prosimy o kontakt maliowy pod adres {{contact_email}} lub {{contact_phone}}</p>]]></contentsForMailForUserAboutUserSignedUpForEvent><subjectForMailForAdminAboutUserSignedUpForEvent><![CDATA[Nowy użytkownik zapisał się na wydarzenie]]></subjectForMailForAdminAboutUserSignedUpForEvent><contentsForMailForAdminAboutUserSignedUpForEvent><![CDATA[<p><strong>Nowy zapis na wydarzenie:</strong><br />\n&quot;{{event_title}}&quot; - {{event_link}}</p>\n\n<p>Start: {{event_start_date}} &nbsp;o godzinie {{event_start_hour}}</p>\n\n<p>Gdzie: {{event_place}}</p>\n\n<p><strong>Dane zgłaszającego:</strong></p>\n\n<p>Tytuł naukowy: {{user_academic_title}}</p>\n\n<p>Imie Nawisko:&nbsp;{{user_name_and_surname}}</p>\n\n<p>Instytucja:&nbsp;{{user_institution_name}}</p>\n\n<p>Dane do fakt.:&nbsp;{{user_invoice_data}}</p>\n\n<p>Telefon:&nbsp;{{user_phone}}</p>\n\n<p>Email:&nbsp;{{user_email}}</p>\n\n<p>Uwagi:&nbsp;{{user_attentions}}</p>\n\n<hr />\n<p>Przejdź do listy zgłoszeń: {{event_list_link}}</p>]]></contentsForMailForAdminAboutUserSignedUpForEvent></xml>', ''),
(765, 1, '2015-12-08 12:01:32', 'Detale wydarzenia', '<?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize></xml>', ''),
(766, 1, '2015-12-09 09:05:00', 'Duży baner', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[0]]></titleVisible><animationTime><![CDATA[3000]]></animationTime><timeBetweenSlides><![CDATA[7000]]></timeBetweenSlides><animationType><![CDATA[fadeout]]></animationType></xml>', 'Brak banerów.'),
(768, 1, '2015-12-09 09:09:30', 'Nadchodzące wydarzenia', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[1]]></titleVisible><itemCountPerPage><![CDATA[5]]></itemCountPerPage><eventListPageBlockId><![CDATA[2924]]></eventListPageBlockId><eventListCategoryId><![CDATA[0]]></eventListCategoryId></xml>', 'Nie ma jeszcze wydarzeń.'),
(769, 1, '2015-12-09 09:59:39', 'Aktualności - slider', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[2]]></titleSize><titleVisible><![CDATA[1]]></titleVisible><newsListPageBlockId><![CDATA[2931]]></newsListPageBlockId><newsListCategoryId><![CDATA[0]]></newsListCategoryId><totalItemCount><![CDATA[10]]></totalItemCount><slideIntervalInMs><![CDATA[5000]]></slideIntervalInMs><animationsTimeInMs><![CDATA[1500]]></animationsTimeInMs><wrappingType><![CDATA[last]]></wrappingType></xml>', ' '),
(770, 1, '2015-12-09 10:00:06', 'Lista aktualności', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[1]]></titleVisible><itemCountPerPage><![CDATA[5]]></itemCountPerPage><detailsPageId><![CDATA[258]]></detailsPageId></xml>', 'Nie ma jeszcze żadnych aktualności.'),
(771, 1, '2015-12-09 10:09:50', 'Akordeon', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[1]]></titleVisible></xml>', ''),
(772, 1, '2015-12-09 10:32:19', 'Blok tekstowy', '<?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize></xml>', '<p><strong>1. Dodawanie i edycja bloków tekstowych.</strong></p>\n\n<p>Będąc zalogowanym jako administrator włączamy tryb edycji klikając myszką na przycisk z ikoną puzla w prawym dolnym rogu ekranu. W trybie edycji dookoła ekranu przebiega czerwona przerywana linia.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px"></p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi bloków, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png" style="height:154px; width:359px"></p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, które można użyć do tworzenia strony, wyszukujemy blok o nazwie „Blok tekstowy”, można w tym celu użyć wyszukiwarki bloków np. wybierając z listy „Typ bloku” pozycję „Blok tekstowy”. Następnie chwytamy myszką ikonę zaznaczoną na zdjęciu poniżej i przeciągamy blok w wybrany na stronie slot na bloki.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_19_33.png"></p>\n\n<p>Wstawiony blok tekstowy posiada standardowe ustawienia.&nbsp;&nbsp;Aby zacząć edycję i odkryć jego szerokie możliwości należy dwukrotnie kliknąć myszką na tekst „Przykładowy tekst. Proszę go zmienić.” lub ikonę edycji.</p>\n\n<p>Otworzy nam się narzędzie o nazwie „CKEditor”. Jest wizualnym&nbsp;edytorem HTML&nbsp;rozpowszechnianym na licencji&nbsp;Open Source, który pozwala na łatwe wprowadzanie tekstu za pomocą&nbsp;interfejsu&nbsp;przypominającego programy typu&nbsp;Microsoft Word.&nbsp;Formatowanie tekstu&nbsp;odbywa się za pomocą zestawu przycisków umożliwiających wybranie&nbsp;czcionki, rozmiaru, koloru liter i tła, ustawienie pogrubienia, podkreślenia czy&nbsp;kursywy, jak również wyrównanie tekstu (do lewej, do prawej, wycentrowanie,&nbsp;wyjustowanie). Poza tym użytkownik ma do dyspozycji funkcje takie jak wstawienie listy (wypunktowanej lub numerowanej), formularzy i wszystkich jego elementów, tabeli, obrazka, video czy odnośnika (linku).</p>\n\n<p><strong>1a. Dodawanie linków.</strong></p>\n\n<p>Klikamy ikonę odnośnika na panelu CKEditora, w ustawieniach podajemy żądane parametry linku.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start(1).png"></p>\n\n<p>1. Informacje – wybieramy typ odnośnika (Adres URL, Odnośnik wewnątrz strony – kotwica, adres-email), wpisujemy adres ręcznie bądź klikamy przycisk „Przeglądaj” i wybieramy element, do którego ma prowadzić odnośnik.</p>\n\n<p>2. Obiekt docelowy – wybieramy z listy, gdzie ma się otworzyć to, do czego prowadzi link.</p>\n\n<p>- ramka</p>\n\n<p>- wyskakujące okno</p>\n\n<p>- nowe okno (_blank)</p>\n\n<p>- okno najwyżej w hierarchii (_top)</p>\n\n<p>- to samo okno (_self)</p>\n\n<p>- okno nadrzędne (_parent)</p>\n\n<p>3. Wyślij – wysyłamy plik na serwer, po wysłaniu ustawi nam się adres URL do tego pliku w zakładce „1. Informacje”.</p>\n\n<p>4. Zaawansowane – tu znajdują się zaawansowane opcje dotyczące odnośnika.</p>\n\n<p>Zatwierdzamy nasze ustawienia przyciskiem „ok” i gotowe. Link można ustawić dla tekstu bądź dowolnego elementu wstawionego do bloku tekstowego, np. grafiki.</p>\n\n<p><a href="http://google.com" target="_blank">Przykład linku do google.com</a></p>\n\n<p><strong>1b. Dodawanie obrazków.</strong></p>\n\n<p>Klikamy ikonę grafiki na panelu CKEditora, w ustawieniach podajemy żądane parametry obrazka.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start(2).png"></p>\n\n<p>1. Informacje o obrazku – podajemy adres URL do pliku z obrazkiem, możemy kliknąć na „Przeglądaj” w celu wybrania obrazka z biblioteki CKEditora. Dobrą praktyką jest wypełnianie pola „Teks zastępczy” – pojawi się on zamiast obrazka w przeglądarkach, które z różnych przyczyn nie wyświetlają obrazków. Podajemy szerokość i wysokość, rozmiar obramowania w px, odstęp poziomy i pionowy od innych elementów strony oraz wyrównanie obrazka względem innych elementów&nbsp;na stronie (do lewej bądź prawej).</p>\n\n<p>Przycisk „Compress Image” służy do zmniejszenia wagi pliku z obrazkiem, przykładowo jeśli nasz plik z obrazkiem ma zbyt dużą rozdzielczość i zbyt dużo KB przez co dłużej się ładuje na stronę, możemy wpisać wysokość i szerokość obrazka jaka nam jest potrzebna i kliknąć przycisk kompresji – stworzy nam się kopia naszego obrazka „lżejsza” dla strony i to ona się wyświetli po zatwierdzeniu zmian przyciskiem „ok”.</p>\n\n<p>Z kolei przycisk „Make Responsive” służy do naprawienia wyświetlania obrazków w różnych rozdzielczościach strony, dodaję on klasę responsywną do styli obrazka przez co nasz obrazek zmniejsza się wraz ze zmniejszaniem okna przeglądarki, szerokość i wysokość obrazka są wtedy niedostępne.</p>\n\n<p>2. Hiperłącze – można użyć obrazka jako linku, w tej zakładce wpisujemy adres URL elementu, do którego ma prowadzić hiperłącze i gdzie ten element ma się wyświetlić.</p>\n\n<p>3. Wyślij – możemy wysłać do biblioteki CKEditora pliki z obrazkami i ustawić do nich adres URL.</p>\n\n<p>4. Zaawansowane – tu znajdują się zaawansowane opcje dotyczące obrazka, dodawanie klas wyświetlania, opisy itp.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20codilab/codilab_ppt-1.png">Przykład obrazka 3000x1687px</p>\n\n<p><strong>1c. Dodawanie tabeli.</strong></p>\n\n<p>Klikamy ikonę tabeli na panelu CKEditora, w ustawieniach podajemy żądane właściwości nowej tabeli.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start(3).png"></p>\n\n<p>1. Właściwości tabeli – podajemy liczbę wierszy i kolumn, wysokość i szerokość dotyczy całej tabeli. Dalej wybieramy jak maja być wyświetlane nagłówki, odstęp pomiędzy komórkami, grubość obramowania i dopełnienie komórki. Można ustawić wyrównanie tabeli do prawej, lewej lub do środka strony, dalej tytuł i opis w podsumowaniu.</p>\n\n<p>2. Zaawansowane – tu znajdują się zaawansowane opcje dotyczące tabeli, dodawanie klas wyświetlania, style itp.</p>\n\n<table align="center" border="1" cellpadding="1" cellspacing="1" style="width:100%">\n	<caption>Przykład tabeli</caption>\n	<thead>\n		<tr>\n			<th scope="col" style="background-color:rgb(255, 255, 102); text-align:center; vertical-align:middle">Pierwszy wiersz pierwsza kolumna</th>\n			<th scope="col" style="background-color:rgb(255, 255, 102); text-align:center; vertical-align:middle">Pierwszy wiersz druga kolumna</th>\n			<th scope="col" style="background-color:rgb(255, 255, 102); text-align:center; vertical-align:middle">Pierwszy wiersz trzecia kolumna</th>\n			<th scope="col" style="background-color:rgb(255, 255, 102); text-align:center; vertical-align:middle">Pierwszy wiersz czwarta kolumna</th>\n		</tr>\n	</thead>\n	<tbody>\n		<tr>\n			<td class="td-responsible">&nbsp;</td>\n			<td class="td-responsible">&nbsp;</td>\n			<td class="td-responsible">&nbsp;</td>\n			<td class="td-responsible">&nbsp;</td>\n		</tr>\n		<tr>\n			<td class="td-responsible">&nbsp;</td>\n			<td class="td-responsible">&nbsp;</td>\n			<td class="td-responsible">&nbsp;</td>\n			<td class="td-responsible">&nbsp;</td>\n		</tr>\n		<tr>\n			<td class="td-responsible">&nbsp;</td>\n			<td class="td-responsible">&nbsp;</td>\n			<td class="td-responsible">&nbsp;</td>\n			<td class="td-responsible">&nbsp;</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p><strong>1d. Formatowanie tekstu.</strong></p>\n\n<p>CKEditor oferuje bardzo dużo możliwości edytowania tekstu, w swojej palecie przycisków do formatowania ma m.in. <strong>pogrubienie</strong>, <em>kursywa</em>, <u>podkreślenie</u>, <s>przekreślenie</s>, <sub>indeks dolny</sub>,<sup> indeks górny</sup>, usuń formatowanie, lista numerowana, lista wypunktowana, zmniejsz wcięcie, zwiększ wcięcie, cytat, wyrównaj do lewej, wyśrodkuj, wyrównaj do prawej, wyjustuj, kierunek tekstu od lewej strony do prawej, kierunek tekstu od prawej strony do lewej, trzy gotowe szablony zawartości, wytnij, kopiuj, wklej, wklej jako zwykły tekst, wklej z programu MS Word, ogólne style formatujące, format nagłówków, czcionka, rozmiar, kolor tekstu i kolor tła. Dodatkowo w CKEditorze można tworzyć formularze z wszystkimi ich elementami jak pole wyboru (checkbox), przycisk opcji (radio), pole tekstowe, obszar tekstowy, lista wyboru, przycisk, przycisk graficzny, pole ukryte. Za pomocą tych narzędzi można dowolnie sformatować tekst dla naszych potrzeb.</p>\n\n<p><strong>1e. Czyszczenie tekstu.</strong></p>\n\n<p>Do czyszczenia tekstu CKEditor oferuje nam trzy przyciski.</p>\n\n<p>1. Wklej jako czysty tekst – po kliknięciu pojawi się okienko, w którym wklejamy nasz tekst i zapisujemy, wstawiony w ten sposób tekst będzie pozbawiony formatowania.</p>\n\n<p>2. Wklej z programu MS Word – po kliknięciu na przycisk pojawi się okienko, w którym wklejamy tekst skopiowany z programy MS Word i zatwierdzamy „ok”. Wstawi się nam tekst zachowując formatowanie źródłowe.</p>\n\n<p>3. Usuń formatowanie – niekiedy potrzebujemy wstawić tekst z różnych źródeł, kopiując go kopiujemy też jego formatowanie, aby dopasował się do naszego tekstu trzeba go zaznaczyć i kliknąć przycisk „Usuń formatowanie”</p>\n\n<p><strong>1f. Wstawianie video z YouTube.</strong></p>\n\n<p>W bardzo szybki i łatwy sposób możemy dodać video z witryny YouTube, wystarczy kliknąć ikonę YouTube na panelu CKEditora.</p>\n\n<p>Znajdujemy film w serwisie YouTube i kopiujemy jego adres z paska adresu, wklejamy go do pola „Wklej link URL do wideo” w otwartym oknie „Załącznik wideo z YouTube”, podajemy wysokość i szerokość dla wyświetlanego filmu, wybieramy dodatkowe dostępne opcje wedle potrzeby:</p>\n\n<p>- &nbsp;Pokaż sugerowane filmy po zakończeniu odtwarzania</p>\n\n<p>- &nbsp;Użyj starego kodu</p>\n\n<p>- Włącz rozszerzony tryb prywatności</p>\n\n<p>- Autoodtwarzanie</p>\n\n<p>- Rozpocznij od (ss lub mm:ss lub gg:mm:ss)</p>\n\n<p>Przykład filmu z serwisu YouTube</p>\n\n<p><iframe align="middle" class="img-responsive" frameborder="0" scrolling="no" src="//www.youtube.com/embed/iCkYw3cRwLo"></iframe></p>\n'),
(773, 1, '2015-12-09 11:58:22', '<p>Menu nawigacyjne</p>\n', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[1]]></titleVisible><categoryId><![CDATA[115]]></categoryId><orientation><![CDATA[vertical]]></orientation><displayUncollapseButtons><![CDATA[1]]></displayUncollapseButtons></xml>', 'Nie podpięto jeszcze żadnych stron do tego menu.'),
(774, 1, '2015-12-09 12:30:10', 'Galeria obrazów', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[2]]></titleSize><titleVisible><![CDATA[1]]></titleVisible></xml>', ''),
(775, 1, '2015-12-09 12:32:25', 'Blok tekstowy', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[0]]></titleVisible></xml>', '<p><strong>Dodawanie i edycja grafiki &ndash; galeria obraz&oacute;w.</strong></p>\n\n<p>Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną puzla.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px" /></p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png" /></p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony. Do dodawania grafiki do strony przygotowanych jest kilka blok&oacute;w, wstawmy pierwszy z nich &ndash; kolejne będą wstawiane analogicznie. Wyszukujemy blok o nazwie &bdquo;Galeria obraz&oacute;w&rdquo;, można w tym celu użyć wyszukiwarki blok&oacute;w np. wybierając z listy &bdquo;Typ bloku&rdquo; pozycję &bdquo;Galeria obraz&oacute;w&rdquo;. Następnie chwytamy myszką ikonę zaznaczoną na zdjęciu poniżej i przeciągamy blok w wybrany na stronie slot na bloki.<img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_19_33.png" /><span style="letter-spacing:0.0175em; line-height:1.875em">Przechodzimy do ustawień klikając na belce bloku ikonę trybiku.</span></p>\n\n<p>Otworzy nam się okno z ustawieniami dla tego bloku.</p>\n\n<p>Ustawiamy rozmiar tytułu naszej galerii oraz czy ma on być widoczny a w ustawieniach wyświetlania decydujemy ile obraz&oacute;w ma być widocznych na stronie. &bdquo;0&rdquo; oznacza brak limitu. Wartość dodatnia włączy limit widocznych obraz&oacute;w i kiedy zostanie on osiągnięty zostanie wyświetlony przycisk &bdquo;Pokaż / ukryj więcej obraz&oacute;w&rdquo;.</p>\n\n<p>Aby dodać zdjęcia do galerii klikamy na belce bloku ikonę folderu &ndash; okno zarządzania.</p>\n\n<p>Następnie niebieski przycisk na otwartym oknie &bdquo;Nowy obrazek&rdquo;.</p>\n\n<p>W formularzu dodawania obrazka wypełniamy tytuł i autora a następnie dodajemy obrazek.</p>\n\n<p>Po dodaniu obrazka klikamy &bdquo;Zapisz&rdquo;, możemy dodawać kolejne obrazki.</p>\n\n<p>&nbsp;</p>\n'),
(776, 1, '2015-12-09 12:34:45', 'Slider obrazów', '<?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize><imageWidth><![CDATA[250]]></imageWidth><imageHeight><![CDATA[150]]></imageHeight><slideIntervalInMs><![CDATA[5000]]></slideIntervalInMs><animationsTimeInMs><![CDATA[1500]]></animationsTimeInMs><wrappingType><![CDATA[last]]></wrappingType></xml>', ''),
(777, 1, '2015-12-09 12:37:05', 'Blok tekstowy', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[0]]></titleVisible></xml>', '<p><strong>Dodawanie i edycja grafiki &ndash; slider obraz&oacute;w.</strong></p>\n\n<p>Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną puzla.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px" /></p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png" /></p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony. Do dodawania grafiki do strony przygotowanych jest kilka blok&oacute;w, wstawmy pierwszy z nich &ndash; kolejne będą wstawiane analogicznie. Wyszukujemy blok o nazwie &bdquo;Galeria obraz&oacute;w&rdquo;, można w tym celu użyć wyszukiwarki blok&oacute;w np. wybierając z listy &bdquo;Typ bloku&rdquo; pozycję &bdquo;Galeria obraz&oacute;w&rdquo;. Następnie chwytamy myszką ikonę zaznaczoną na zdjęciu poniżej i przeciągamy blok w wybrany na stronie slot na bloki.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_19_33.png" /></p>\n\n<p>Przechodzimy do ustawień klikając na belce bloku ikonę trybiku.&nbsp;Otworzy nam się okno opisane poniżej.</p>\n\n<p>1. Tytuł &ndash; ustawiamy rozmiar tytułu oraz czy ma on być widoczny.</p>\n\n<p>2. Ustawienia wyświetlania &ndash; ustawiamy rozmiar miniatury slidera w pikselach, następnie po zaznaczeniu opcji &bdquo;Pokaż obraz w kolorze tylko, gdy kursor myszy znajduje się nad nim&rdquo;, nasze obrazy na slajdach będą o odcieniach skali szarości, najeżdżając myszką na obraz zmieni się na kolorowy.</p>\n\n<p>3. Ustawienia przewijania - &ndash; ustawiamy czas pomiędzy przejściami (slajdami), czas samego przejścia oraz typ przewijania, dostępnych są 4 typy oraz brak przewijania:</p>\n\n<p>- skocz na początek po wyświetleniu ostatniego elementu</p>\n\n<p>- skocz na koniec po wyświetleniu pierwszego elementu</p>\n\n<p>- użyj pierwszej i drugiej opcji jednocześnie</p>\n\n<p>- użyj zapętlonego przewijania</p>\n\n<p>Dalszą czynnością jest dodanie obraz&oacute;w do naszego bloku slidera. Klikamy na belce ikonę folderu &ndash; okno zarządzania. W otwartym oknie klikamy niebieski przycisk &bdquo;Nowy obrazek&rdquo; i ustawiamy dostępne opcje.</p>\n\n<p>1. Typ adresu URL &ndash; możemy określić czy dany obrazek ma nam służyć jako link, z listy wyboru wybieramy adres wewnętrzny, zewnętrzny lub brak.</p>\n\n<p>2. Adres URL &ndash; jeśli w typie adresu URL wybierzemy adres wewnętrzny, na liście Adresu URL będą dostępne wszystkie nasze strony i do wybranej przez nas będzie kierował link po kliknięciu na obrazek.</p>\n\n<p>3. Obraz &ndash; dodajemy obrazek jak w przypadku galerii (patrz - wyżej).</p>\n\n<p>4. Tytuł &ndash; ustawiamy tytuł dla obrazka, będzie on widoczny po najechaniu myszką na obrazek w &bdquo;dymku&rdquo; .</p>\n\n<p>Po dodaniu obrazka można dodać ich więcej tworząc własny przewijany slider obraz&oacute;w.</p>\n\n<p>&nbsp;</p>\n'),
(778, 1, '2015-12-09 12:40:15', 'Blok tekstowy', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[0]]></titleVisible></xml>', '<p><strong>Dodawanie i edycja grafiki &ndash; stos obraz&oacute;w.</strong></p>\n\n<p>Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną puzla.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px" /></p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png" /></p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony. Do dodawania grafiki do strony przygotowanych jest kilka blok&oacute;w, wstawmy pierwszy z nich &ndash; kolejne będą wstawiane analogicznie. Wyszukujemy blok o nazwie &bdquo;Galeria obraz&oacute;w&rdquo;, można w tym celu użyć wyszukiwarki blok&oacute;w np. wybierając z listy &bdquo;Typ bloku&rdquo; pozycję &bdquo;Galeria obraz&oacute;w&rdquo;. Następnie chwytamy myszką ikonę zaznaczoną na zdjęciu poniżej i przeciągamy blok w wybrany na stronie slot na bloki.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_19_33.png" /></p>\n\n<p>Przechodzimy do ustawień klikając na belce bloku ikonę trybiku. Otworzy się panel opisany poniżej.</p>\n\n<p>1. Tytuł &ndash; ustawiamy rozmiar tytułu oraz czy ma on być widoczny.</p>\n\n<p>2. Podstawowe ustawienia &ndash; ustawiamy kolor tła i czy ma być włączone, odstęp pomiędzy elementami stosu, możemy zaznaczyć żeby przy załadowaniu bloku otwierała się pierwsza dostępna grupa obraz&oacute;w, ustawiamy kąty obrotu dla bardziej realistycznego efektu stosu, możemy wybrać losowe kąty.</p>\n\n<p>3. Ustawienia animacji dla klikniętego stosu &ndash; ustawiamy prędkości otwierania i zamykania stos&oacute;w oraz ich płynności, dostępnych jest 5:</p>\n\n<p>- linear</p>\n\n<p>- easy -in</p>\n\n<p>- easy-out</p>\n\n<p>- easy-in-out</p>\n\n<p>4. Ustawienia animacji dla pozostałych stos&oacute;w &ndash; jak w powyższym punkcie.</p>\n\n<p>Dalszą czynnością jest dodanie obraz&oacute;w do naszego bloku stos obraz&oacute;w. Klikamy na belce ikonę folderu &ndash; okno zarządzania. W otwartym oknie klikamy niebieski przycisk &bdquo;Zarządzaj grupami&rdquo; a następnie w otwartym oknie kolejny przycisk &bdquo;Nowa grupa&rdquo;. Wpisujemy tytuł dla grupy i klikamy &bdquo;Zapisz&rdquo;. Zamykamy okno &bdquo;Grupy &ndash; zarządzanie&rdquo; i teraz możemy dodać element stosu klikając na niebieski przycisk &bdquo;Nowy element&rdquo;. W oknie dodawania nowego elementu zobaczymy poniższy formularz.</p>\n\n<p>1. Grupa &ndash; wybieramy grupę obraz&oacute;w, kt&oacute;re znajdą się w stosie.</p>\n\n<p>2. Typ adresu URL &ndash; możemy określić czy dany obrazek ma nam służyć jako link, z listy wyboru wybieramy adres wewnętrzny, zewnętrzny lub brak.</p>\n\n<p>3. Adres URL &ndash; jeśli w typie adresu URL wybierzemy adres wewnętrzny, na liście Adresu URL będą dostępne wszystkie nasze strony i do wybranej przez nas będzie kierował link po kliknięciu na obrazek.</p>\n\n<p>4. Obraz &ndash; dodajemy obrazek jak w przypadku galerii (patrz - wyżej).</p>\n\n<p>5. Tytuł &ndash; ustawiamy tytuł dla obrazka, będzie on widoczny po najechaniu myszką na obrazek w &bdquo;dymku&rdquo; .</p>\n\n<p>Po dodaniu obrazka jako elementu stosu można dodać ich więcej tworząc większy stos obraz&oacute;w, a tworząc więcej grup więcej stos&oacute;w.</p>\n\n<p>&nbsp;</p>\n'),
(779, 1, '2015-12-09 12:40:51', 'Blok tekstowy', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[0]]></titleVisible></xml>', '<p><strong>Dodawanie i edycja grafiki – duży baner.</strong></p>\n\n<p>Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną puzla.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px"></p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi bloków, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png"></p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, które można użyć do tworzenia strony. Do dodawania grafiki do strony przygotowanych jest kilka bloków, wstawmy pierwszy z nich – kolejne będą wstawiane analogicznie. Wyszukujemy blok o nazwie „Galeria obrazów”, można w tym celu użyć wyszukiwarki bloków np. wybierając z listy „Typ bloku” pozycję „Galeria obrazów”. Następnie chwytamy myszką ikonę zaznaczoną na zdjęciu poniżej i przeciągamy blok w wybrany na stronie slot na bloki.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_19_33.png"></p>\n\n<p>Przechodzimy do ustawień klikając na belce bloku ikonę trybiku. Otworzy się panel opisany poniżej:</p>\n\n<p>1. Tytuł – ustawiamy rozmiar tytułu oraz czy ma on być widoczny.</p>\n\n<p>2. Ustawienia wyświetlania – ustawiamy czas trwania animacji i czas przejścia miedzy slajdami. W typie slajdów mamy do wyboru:</p>\n\n<p>- none</p>\n\n<p>- fade</p>\n\n<p>- fadeout</p>\n\n<p>- scrollHorz</p>\n\n<p>Dalszą czynnością jest dodanie obrazów do naszego bloku Duży baner. Klikamy na belce ikonę folderu – okno zarządzania. W otwartym oknie klikamy niebieski przycisk „New Baner. W oknie dodawania nowego baneru zobaczymy poniższy formularz.</p>\n\n<p>1. Tytuł – należy wypełnić pole tytuł i zaznaczyć czy ma on być widoczny nad obrazem.</p>\n\n<p>2. Typ adresu URL – możemy określić czy dany obrazek ma nam służyć jako link, z listy wyboru wybieramy adres wewnętrzny, zewnętrzny lub brak.</p>\n\n<p>3. Adres URL – jeśli w typie adresu URL wybierzemy adres wewnętrzny, na liście Adresu URL będą dostępne wszystkie nasze strony i do wybranej przez nas będzie kierował link po kliknięciu na tekst „Czytaj więcej”.</p>\n\n<p>4. Obraz – dodajemy obrazek jak w przypadku galerii (patrz - wyżej).</p>\n\n<p>5. Treść – treść będzie widoczna na banerze, można zaznaczyć czy ma być wyświetlana .</p>\n\n<p>Można dodawać więcej obrazów wedle uznania, najlepszym slotem do umieszczania bloku Duży banner jest najszerszy dostępny, sięgający od brzegu do brzegu ekranu.</p>\n'),
(780, 1, '2015-12-09 12:41:14', 'Blok tekstowy', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[0]]></titleVisible></xml>', '<p><strong>Dodawanie i edycja grafiki &ndash; Small baner.</strong></p>\n\n<p>Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną puzla.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px" /></p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png" /></p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony. Do dodawania grafiki do strony przygotowanych jest kilka blok&oacute;w, wstawmy pierwszy z nich &ndash; kolejne będą wstawiane analogicznie. Wyszukujemy blok o nazwie &bdquo;Galeria obraz&oacute;w&rdquo;, można w tym celu użyć wyszukiwarki blok&oacute;w np. wybierając z listy &bdquo;Typ bloku&rdquo; pozycję &bdquo;Galeria obraz&oacute;w&rdquo;. Następnie chwytamy myszką ikonę zaznaczoną na zdjęciu poniżej i przeciągamy blok w wybrany na stronie slot na bloki.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_19_33.png" /></p>\n\n<p>Przechodzimy do ustawień klikając na belce bloku ikonę trybiku. Otworzy się panel opisany poniżej:</p>\n\n<p>1. Tytuł &ndash; ustawiamy rozmiar tytułu oraz czy ma on być widoczny.</p>\n\n<p>2. Ustawienia wyświetlania &ndash; ustawiamy czas trwania przejścia miedzy slajdami. W typie slajd&oacute;w mamy do wyboru 16 r&oacute;żnych przejść, w Slider Theme 4 sk&oacute;rki (default, light, dark i bar), a w ostatniej liście wyb&oacute;r czy mają być widoczne strzałki do przewijania obraz&oacute;w banneru.</p>\n\n<p>Dalszą czynnością jest dodanie obraz&oacute;w do naszego bloku Small banner. Klikamy na belce ikonę folderu &ndash; okno zarządzania. W otwartym oknie klikamy niebieski przycisk &bdquo;New Baner. W oknie dodawania nowego banneru zobaczymy poniższy formularz.</p>\n\n<p>1. Tytuł &ndash; należy wypełnić pole tytuł, można zostawić domyślny tytuł, nie będzie on widoczny na stronie, identyfikuje on baner na liście baner&oacute;w w oknie zarządzania.</p>\n\n<p>2. Obraz &ndash; dodajemy obrazek jak w przypadku galerii (patrz - wyżej).</p>\n\n<p>3. Treść &ndash; treść będzie widoczna na banerze, jeśli zostawimy pustą treść &ndash; będzie wyświetlany sam obraz .</p>\n\n<p>Do edytowania wszystkich obraz&oacute;w we wszystkich wymienionych wyżej blokach służy ikona edycji w oknie zarządzania tych blok&oacute;w, zdjęcie przedstawia edycję w bloku &bdquo;Duży banner&rdquo; ale analogicznie ta ikona znajduje się w pozostałych blokach.</p>\n'),
(781, 1, '2015-12-09 12:45:06', 'Stos obrazów', '<?xml version="1.0"?><xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[1]]></titleVisible><backgroundColor><![CDATA[#fff]]></backgroundColor><gutter><![CDATA[40]]></gutter><pileAngles><![CDATA[2]]></pileAngles><delay><![CDATA[0]]></delay><pileAnimationOpenSpeed><![CDATA[400]]></pileAnimationOpenSpeed><pileAnimationOpenEasing><![CDATA[ease-in-out]]></pileAnimationOpenEasing><pileAnimationCloseSpeed><![CDATA[400]]></pileAnimationCloseSpeed><pileAnimationCloseEasing><![CDATA[ease-in-out]]></pileAnimationCloseEasing><otherPileAnimationOpenSpeed><![CDATA[400]]></otherPileAnimationOpenSpeed><otherPileAnimationOpenEasing><![CDATA[ease-in-out]]></otherPileAnimationOpenEasing><otherPileAnimationCloseEasing><![CDATA[ease-in-out]]></otherPileAnimationCloseEasing></xml>', ''),
(782, 1, '2015-12-09 12:56:38', 'Small Banner', ' <?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize></xml>', ''),
(783, 1, '2015-12-09 13:06:32', 'Detale aktualności', '<?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize></xml>', ''),
(784, 1, '2015-12-09 13:37:22', 'Zakładki', '<?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize></xml>', ''),
(785, 1, '2015-12-09 15:27:40', '<p>Grafika</p>\n', '<?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize></xml>', '<p><strong>1. Dodawanie i edycja grafiki &ndash; galeria obraz&oacute;w.</strong></p>\n\n<p>Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną trybiku.</p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony. Do dodawania grafiki do strony przygotowanych jest kilka blok&oacute;w, wstawmy pierwszy z nich &ndash; kolejne będą wstawiane analogicznie. Wyszukujemy blok o nazwie &bdquo;Galeria obraz&oacute;w&rdquo;, można w tym celu użyć wyszukiwarki blok&oacute;w np. wybierając z listy &bdquo;Typ bloku&rdquo; pozycję &bdquo;Galeria obraz&oacute;w&rdquo;. Następnie chwytamy myszką ikonę zaznaczoną na zdjęciu poniżej i przeciągamy blok w wybrany na stronie slot na bloki.</p>\n\n<p>Przechodzimy do ustawień klikając na belce bloku ikonę trybiku.</p>\n\n<p>Otworzy nam się okno z ustawieniami dla tego bloku.</p>\n\n<p>Ustawiamy rozmiar tytułu naszej galerii oraz czy ma on być widoczny a w ustawieniach wyświetlania decydujemy ile obraz&oacute;w ma być widocznych na stronie. &bdquo;0&rdquo; oznacza brak limitu. Wartość dodatnia włączy limit widocznych obraz&oacute;w i kiedy zostanie on osiągnięty zostanie wyświetlony przycisk &bdquo;Pokaż / ukryj więcej obraz&oacute;w&rdquo;.</p>\n\n<p>Aby dodać zdjęcia do galerii klikamy na belce bloku ikonę folderu &ndash; okno zarządzania.</p>\n\n<p>Następnie niebieski przycisk na otwartym oknie &bdquo;Nowy obrazek&rdquo;.</p>\n\n<p>W formularzu dodawania obrazka wypełniamy tytuł i autora a następnie dodajemy obrazek.</p>\n\n<p>Po dodaniu obrazka klikamy &bdquo;Zapisz&rdquo;, możemy dodawać kolejne obrazki.</p>\n\n<p><strong>2. Dodawanie i edycja grafiki &ndash; slider obraz&oacute;w.</strong></p>\n\n<p>Tak samo jak galerię wstawiamy na stronę blok &bdquo;Slider obraz&oacute;w&rdquo;. Klikamy na jego belce ikonę trybiku - ustawienia. Otworzy nam się okno opisane poniżej.</p>\n\n<p>1. Tytuł &ndash; ustawiamy rozmiar tytułu oraz czy ma on być widoczny.</p>\n\n<p>2. Ustawienia wyświetlania &ndash; ustawiamy rozmiar miniatury slidera w pikselach, następnie po zaznaczeniu opcji &bdquo;Pokaż obraz w kolorze tylko, gdy kursor myszy znajduje się nad nim&rdquo;, nasze obrazy na slajdach będą o odcieniach skali szarości, najeżdżając myszką na obraz zmieni się na kolorowy.</p>\n\n<p><span style="letter-spacing:0.0175em; line-height:1.875em">3. Ustawienia przewijania - &ndash; ustawiamy czas pomiędzy przejściami (slajdami), czas samego przejścia oraz typ przewijania, dostępnych są 4 typy oraz brak przewijania:</span></p>\n\n<p><span style="letter-spacing:0.0175em; line-height:1.875em">- skocz na początek po wyświetleniu ostatniego elementu</span></p>\n\n<p><span style="letter-spacing:0.0175em; line-height:1.875em">- skocz na koniec po wyświetleniu pierwszego elementu</span></p>\n\n<p><span style="letter-spacing:0.0175em; line-height:1.875em">- użyj pierwszej i drugiej opcji jednocześnie</span></p>\n\n<p>- użyj zapętlonego przewijania</p>\n\n<p>Dalszą czynnością jest dodanie obraz&oacute;w do naszego bloku slidera. Klikamy na belce ikonę folderu &ndash; okno zarządzania. W otwartym oknie klikamy niebieski przycisk &bdquo;Nowy obrazek&rdquo; i ustawiamy dostępne opcje.</p>\n\n<p>1. Typ adresu URL &ndash; możemy określić czy dany obrazek ma nam służyć jako link, z listy wyboru wybieramy adres wewnętrzny, zewnętrzny lub brak.</p>\n\n<p>2. Adres URL &ndash; jeśli w typie adresu URL wybierzemy adres wewnętrzny, na liście Adresu URL będą dostępne wszystkie nasze strony i do wybranej przez nas będzie kierował link po kliknięciu na obrazek.</p>\n\n<p>3. Obraz &ndash; dodajemy obrazek jak w przypadku galerii (patrz - wyżej).</p>\n\n<p>4. Tytuł &ndash; ustawiamy tytuł dla obrazka, będzie on widoczny po najechaniu myszką na obrazek w &bdquo;dymku&rdquo; .</p>\n\n<p>Po dodaniu obrazka można dodać ich więcej tworząc własny przewijany slider obraz&oacute;w.</p>\n\n<p><strong>3. Dodawanie i edycja grafiki &ndash; stos obraz&oacute;w.</strong></p>\n\n<p>Tak samo jak galerię i slider obraz&oacute;w wstawiamy na stronę blok &bdquo;Stos obraz&oacute;w&rdquo;. Klikamy na jego belce ikonę trybiku - ustawienia. Otworzy nam się okno opisane poniżej.</p>\n\n<p>1. Tytuł &ndash; ustawiamy rozmiar tytułu oraz czy ma on być widoczny.</p>\n\n<p>2. Podstawowe ustawienia &ndash; ustawiamy kolor tła i czy ma być włączone, odstęp pomiędzy elementami stosu, możemy zaznaczyć żeby przy załadowaniu bloku otwierała się pierwsza dostępna grupa obraz&oacute;w, ustawiamy kąty obrotu dla bardziej realistycznego efektu stosu, możemy wybrać losowe kąty.</p>\n\n<p><span style="letter-spacing:0.0175em; line-height:1.875em">3. Ustawienia animacji dla klikniętego stosu &ndash; ustawiamy prędkości otwierania i zamykania stos&oacute;w oraz ich płynności, dostępnych jest 5:</span></p>\n\n<p><span style="letter-spacing:0.0175em; line-height:1.875em">- linear</span></p>\n\n<p><span style="letter-spacing:0.0175em; line-height:1.875em">- easy -in</span></p>\n\n<p>- easy-out</p>\n\n<p>- easy-in-out</p>\n\n<p>4. Ustawienia animacji dla pozostałych stos&oacute;w &ndash; jak w powyższym punkcie.</p>\n\n<p>Dalszą czynnością jest dodanie obraz&oacute;w do naszego bloku stos obraz&oacute;w. Klikamy na belce ikonę folderu &ndash; okno zarządzania. W otwartym oknie klikamy niebieski przycisk &bdquo;Zarządzaj grupami&rdquo; a następnie w otwartym oknie kolejny przycisk &bdquo;Nowa grupa&rdquo;. Wpisujemy tytuł dla grupy i klikamy &bdquo;Zapisz&rdquo;. Zamykamy okno &bdquo;Grupy &ndash; zarządzanie&rdquo; i teraz możemy dodać element stosu klikając na niebieski przycisk &bdquo;Nowy element&rdquo;. W oknie dodawania nowego elementu zobaczymy poniższy formularz.</p>\n\n<p>1. Grupa &ndash; wybieramy grupę obraz&oacute;w, kt&oacute;re znajdą się w stosie.</p>\n\n<p>2. Typ adresu URL &ndash; możemy określić czy dany obrazek ma nam służyć jako link, z listy wyboru wybieramy adres wewnętrzny, zewnętrzny lub brak.</p>\n\n<p>3 .Adres URL &ndash; jeśli w typie adresu URL wybierzemy adres wewnętrzny, na liście Adresu URL będą dostępne wszystkie nasze strony i do wybranej przez nas będzie kierował link po kliknięciu na obrazek.</p>\n\n<p>4. Obraz &ndash; dodajemy obrazek jak w przypadku galerii (patrz - wyżej).</p>\n\n<p>5. Tytuł &ndash; ustawiamy tytuł dla obrazka, będzie on widoczny po najechaniu myszką na obrazek w &bdquo;dymku&rdquo; .</p>\n\n<p>Po dodaniu obrazka jako elementu stosu można dodać ich więcej tworząc większy stos obraz&oacute;w, a tworząc więcej grup więcej stos&oacute;w.</p>\n\n<p><strong>4. Dodawanie i edycja grafiki &ndash; duży baner.</strong></p>\n\n<p>Tak samo jak galerię, slider obraz&oacute;w i stos obraz&oacute;w wstawiamy na stronę blok &bdquo;Duży baner&rdquo;. Klikamy na jego belce ikonę trybiku - ustawienia. Otworzy nam się okno opisane poniżej.</p>\n\n<p>1. Tytuł &ndash; ustawiamy rozmiar tytułu oraz czy ma on być widoczny.</p>\n\n<p>2. Ustawienia wyświetlania &ndash; ustawiamy czas trwania animacji i czas przejścia miedzy slajdami. W typie slajd&oacute;w mamy do wyboru:</p>\n\n<p><span style="letter-spacing:0.0175em; line-height:1.875em">- none</span></p>\n\n<p><span style="letter-spacing:0.0175em; line-height:1.875em">- fade</span></p>\n\n<p><span style="letter-spacing:0.0175em; line-height:1.875em">- fadeout</span></p>\n\n<p><span style="letter-spacing:0.0175em; line-height:1.875em">- scrollHorz</span></p>\n\n<p>Dalszą czynnością jest dodanie obraz&oacute;w do naszego bloku Duży baner. Klikamy na belce ikonę folderu &ndash; okno zarządzania. W otwartym oknie klikamy niebieski przycisk &bdquo;New Baner. W oknie dodawania nowego baneru zobaczymy poniższy formularz.</p>\n\n<p>1. Tytuł &ndash; należy wypełnić pole tytuł i zaznaczyć czy ma on być widoczny nad obrazem.</p>\n\n<p>2. Typ adresu URL &ndash; możemy określić czy dany obrazek ma nam służyć jako link, z listy wyboru wybieramy adres wewnętrzny, zewnętrzny lub brak.</p>\n\n<p>3. Adres URL &ndash; jeśli w typie adresu URL wybierzemy adres wewnętrzny, na liście Adresu URL będą dostępne wszystkie nasze strony i do wybranej przez nas będzie kierował link po kliknięciu na tekst &bdquo;Czytaj więcej&rdquo;.</p>\n\n<p>4. Obraz &ndash; dodajemy obrazek jak w przypadku galerii (patrz - wyżej).</p>\n\n<p>5. Treść &ndash; treść będzie widoczna na banerze, można zaznaczyć czy ma być wyświetlana .</p>\n\n<p>Można dodawać więcej obraz&oacute;w wedle uznania, najlepszym slotem do umieszczania bloku Duży banner jest najszerszy dostępny, sięgający od brzegu do brzegu ekranu.</p>\n\n<p><strong>5. Dodawanie i edycja grafiki &ndash; Small baner.</strong></p>\n\n<p>Tak samo jak galerię, slider obraz&oacute;w, stos obraz&oacute;w i duży banner wstawiamy na stronę blok &bdquo;Small baner&rdquo;. Klikamy na jego belce ikonę trybiku - ustawienia. Otworzy nam się okno opisane poniżej.</p>\n\n<p>1. Tytuł &ndash; ustawiamy rozmiar tytułu oraz czy ma on być widoczny.</p>\n\n<p>2. Ustawienia wyświetlania &ndash; ustawiamy czas trwania przejścia miedzy slajdami. W typie slajd&oacute;w mamy do wyboru 16 r&oacute;żnych przejść, w Slider Theme 4 sk&oacute;rki (default, light, dark i bar), a w ostatniej liście wyb&oacute;r czy mają być widoczne strzałki do przewijania obraz&oacute;w banneru.</p>\n\n<p>Dalszą czynnością jest dodanie obraz&oacute;w do naszego bloku Small banner. Klikamy na belce ikonę folderu &ndash; okno zarządzania. W otwartym oknie klikamy niebieski przycisk &bdquo;New Baner. W oknie dodawania nowego banneru zobaczymy poniższy formularz.</p>\n\n<p>1. Tytuł &ndash; należy wypełnić pole tytuł, można zostawić domyślny tytuł, nie będzie on widoczny na stronie, identyfikuje on baner na liście baner&oacute;w w oknie zarządzania.</p>\n\n<p>2. Obraz &ndash; dodajemy obrazek jak w przypadku galerii (patrz - wyżej).</p>\n\n<p>3. Treść &ndash; treść będzie widoczna na banerze, jeśli zostawimy pustą treść &ndash; będzie wyświetlany sam obraz .</p>\n\n<p>Do edytowania wszystkich obraz&oacute;w we wszystkich wymienionych wyżej blokach służy ikona edycji w oknie zarządzania tych blok&oacute;w, zdjęcie przedstawia edycję w bloku &bdquo;Duży banner&rdquo; ale analogicznie ta ikona znajduje się w pozostałych blokach.</p>\n'),
(787, 1, '2015-12-09 15:31:17', 'Wyskakujące okno', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[2]]></titleSize><titleVisible><![CDATA[0]]></titleVisible></xml>', ''),
(788, 1, '2016-01-04 12:47:05', '<p>Informacja na temat Detali Wydarzenia</p>\n', '<?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize></xml>', '<p>Na tej stronie umieszczony jest blok "Detale wydarzenia". Będąc na stronie z listą wydarzeń, wydarzeniami nadchodzącymi lub archiwalnymi po kliknięciu w wydarzenie, jego szczegóły otworzą się na tej stronie w bloku poniżej. Więcej informacji o zarządzaniu wydarzeniami znajduje się w na liście wydarzeń - w menu "Wydarzenia".</p>\n'),
(789, 1, '2016-01-04 13:02:46', 'Lista wydarzeń archiwalnych', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[1]]></titleVisible><itemCountPerPage><![CDATA[5]]></itemCountPerPage><eventListPageBlockId><![CDATA[2924]]></eventListPageBlockId><eventListCategoryId><![CDATA[0]]></eventListCategoryId></xml>', ''),
(790, 1, '2016-01-04 13:07:29', '<p>Informacja na temat bloku &quot;Lista wydarzeń archiwalnych&quot;</p>\n', '<?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize></xml>', '<p>Na tej stronie umieszczony jest blok "Lista wydarzeń archiwalnych". Na liście poniżej znajdują się wydarzenia z minioną datą. Więcej informacji o zarządzaniu wydarzeniami znajduje się w na liście wydarzeń - w menu "Wydarzenia".</p>\n'),
(791, 1, '2016-01-04 13:08:18', 'Nadchodzące wydarzenia', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[1]]></titleVisible><itemCountPerPage><![CDATA[5]]></itemCountPerPage><eventListPageBlockId><![CDATA[2924]]></eventListPageBlockId><eventListCategoryId><![CDATA[0]]></eventListCategoryId></xml>', 'Nie ma jeszcze wydarzeń.'),
(792, 1, '2016-01-04 13:08:40', 'Blok tekstowy', '<?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize></xml>', '<p>Na tej stronie umieszczony jest blok &quot;Nadchodzące wydarzenia&quot;. Na liście poniżej znajdują się wydarzenia z przyszłą datą. Więcej informacji o zarządzaniu wydarzeniami znajduje się w na liście wydarzeń - w menu &quot;Wydarzenia&quot;.</p>\n'),
(793, 1, '2016-01-04 13:10:20', 'Blok tekstowy', '<?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize></xml>', '<p>Na tej stronie umieszczony jest blok "Detale aktualności". Będąc na stronie z listą aktualności lub sliderem aktualności po kliknięciu w aktualność, jej szczegóły otworzą się na tej stronie w bloku poniżej. Więcej informacji o zarządzaniu aktualnościami znajduje się w na liście aktualności - w menu "Aktualności" lub na stronie głównej w sliderze aktualności.</p>\n'),
(795, 1, '2016-01-04 14:26:48', 'Akordeon', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize></xml>', ''),
(797, 1, '2016-01-05 12:43:07', 'Slider obrazów', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[0]]></titleVisible><imageWidth><![CDATA[176]]></imageWidth><imageHeight><![CDATA[50]]></imageHeight><imageInGrayscale><![CDATA[1]]></imageInGrayscale><slideIntervalInMs><![CDATA[5000]]></slideIntervalInMs><animationsTimeInMs><![CDATA[1500]]></animationsTimeInMs><wrappingType><![CDATA[circular]]></wrappingType></xml>', ''),
(798, 1, '2016-01-05 13:42:51', 'Dane pracownika', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[0]]></titleVisible></xml>', ''),
(799, 1, '2016-01-05 13:43:36', 'Pracownicy', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[0]]></titleVisible><detailsPageId><![CDATA[270]]></detailsPageId></xml>', ''),
(802, 1, '2016-01-07 12:18:55', 'Wyniki wyszukiwania', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[0]]></titleVisible><highlightFoundWords><![CDATA[1]]></highlightFoundWords></xml>', 'Nic nie znaleziono.'),
(803, 1, '2016-01-07 12:19:27', 'Nagłówek', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize><titleVisible><![CDATA[0]]></titleVisible><tagType><![CDATA[predefined]]></tagType><predefinedTag><![CDATA[h3]]></predefinedTag><customTag><![CDATA[]]></customTag></xml>', '<h3>Wyniki wyszukiwania:</h3>\n'),
(806, 1, '2016-01-11 15:42:10', 'Akordeon', '<?xml version="1.0"?>\n<xml><titleSize><![CDATA[3]]></titleSize></xml>', ''),
(807, 1, '2016-01-15 18:10:48', '<p>Wstawianie blok&oacute;w na stronę</p>\n', '<?xml version="1.0"?><xml><titleVisible><![CDATA[1]]></titleVisible><titleSize><![CDATA[3]]></titleSize></xml>', '<div>\n<video controls="controls" height="auto" id="1" poster="/ckeditor-files/images/grafiki%20codilab/home-bg-2.png" width="100%"><source src="/ckeditor-files/files/codilab-cms-1024x768.mp4" type="video/mp4">Your browser doesn''t support video.<br>\nPlease download the file: <a href="/ckeditor-files/files/codilab-cms-1024x768.mp4">video/mp4</a></video>\n</div>\n\n<p>&nbsp;</p>\n');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pages_blocks_contents_association`
--

CREATE TABLE `pages_blocks_contents_association` (
  `association_id` int(10) UNSIGNED NOT NULL,
  `page_block_id` int(10) UNSIGNED DEFAULT NULL,
  `content_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `pages_blocks_contents_association`
--

INSERT INTO `pages_blocks_contents_association` (`association_id`, `page_block_id`, `content_id`) VALUES
(2916, 2919, 763),
(2917, 2920, 763),
(2918, 2921, 763),
(2919, 2922, 763),
(2920, 2923, 763),
(2921, 2924, 764),
(2922, 2925, 765),
(2923, 2926, 763),
(2924, 2927, 766),
(2926, 2929, 768),
(2927, 2930, 769),
(2928, 2931, 770),
(2929, 2932, 763),
(2930, 2933, 771),
(2931, 2934, 772),
(2932, 2935, 773),
(2933, 2936, 773),
(2934, 2937, 773),
(2935, 2938, 773),
(2936, 2939, 773),
(2937, 2940, 773),
(2938, 2941, 773),
(2940, 2943, 773),
(2941, 2944, 773),
(2942, 2945, 773),
(2943, 2946, 773),
(2944, 2947, 773),
(2945, 2948, 773),
(2946, 2949, 773),
(2947, 2950, 763),
(2948, 2951, 763),
(2950, 2953, 763),
(2951, 2954, 763),
(2952, 2955, 763),
(2953, 2956, 763),
(2954, 2957, 763),
(2955, 2958, 774),
(2956, 2959, 775),
(2957, 2960, 776),
(2958, 2961, 777),
(2959, 2962, 778),
(2960, 2963, 779),
(2961, 2964, 780),
(2962, 2965, 781),
(2963, 2966, 766),
(2964, 2967, 782),
(2965, 2968, 783),
(2966, 2969, 784),
(2967, 2970, 785),
(2969, 2972, 787),
(2970, 2973, 788),
(2971, 2974, 789),
(2972, 2975, 790),
(2973, 2976, 791),
(2974, 2977, 792),
(2975, 2978, 793),
(2976, 2979, 763),
(2977, 2980, 763),
(2978, 2981, 773),
(2980, 2983, 795),
(2982, 2985, 797),
(2983, 2986, 798),
(2984, 2987, 799),
(2985, 2988, 763),
(2988, 2991, 802),
(2989, 2992, 803),
(2992, 2995, 806),
(2993, 2996, 807);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pages_language_equivalents`
--

CREATE TABLE `pages_language_equivalents` (
  `equivalent_id` int(10) UNSIGNED NOT NULL,
  `page_id` smallint(5) UNSIGNED DEFAULT NULL,
  `language_id` smallint(5) UNSIGNED DEFAULT NULL,
  `associated_page_id` smallint(5) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `pages_language_equivalents`
--

INSERT INTO `pages_language_equivalents` (`equivalent_id`, `page_id`, `language_id`, `associated_page_id`) VALUES
(733, 251, 2, 248),
(734, 251, 3, 248),
(735, 252, 1, 251),
(736, 252, 2, 248),
(737, 253, 1, 248),
(738, 253, 3, 248),
(739, 254, 1, 248),
(740, 254, 3, 248),
(741, 255, 1, 248),
(742, 255, 3, 248),
(743, 256, 1, 248),
(744, 256, 3, 248),
(745, 257, 1, 248),
(746, 257, 3, 248),
(751, 248, 1, 251),
(752, 248, 3, 251),
(753, 258, 1, 248),
(754, 258, 3, 248),
(755, 259, 1, 248),
(756, 259, 3, 248),
(757, 260, 1, 248),
(758, 260, 3, 248),
(761, 262, 1, 248),
(762, 262, 3, 248),
(763, 263, 1, 248),
(764, 263, 3, 248),
(765, 264, 1, 248),
(766, 264, 3, 248),
(767, 265, 1, 248),
(768, 265, 3, 248),
(769, 266, 1, 248),
(770, 266, 3, 248),
(771, 267, 1, 248),
(772, 267, 3, 248),
(773, 268, 1, 248),
(774, 268, 3, 248),
(775, 269, 1, 248),
(776, 269, 3, 248),
(777, 270, 1, 248),
(778, 270, 3, 248),
(779, 271, 1, 248),
(780, 271, 3, 248),
(781, 272, 1, 248),
(782, 272, 3, 248);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `search_index`
--

CREATE TABLE `search_index` (
  `result_id` int(10) UNSIGNED NOT NULL,
  `page_id` smallint(5) UNSIGNED NOT NULL,
  `language_id` smallint(5) UNSIGNED NOT NULL COMMENT 'Page language. Typically, a user wants to find something just in his language.',
  `content_id` int(10) UNSIGNED DEFAULT NULL,
  `block_element_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Id of specific record. Can be event_id, news_id etc.',
  `date` datetime DEFAULT NULL,
  `translatable_search_result_category` varchar(128) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `block_element_url_parameters` varchar(256) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `page_title` varchar(128) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `meta_description` text COLLATE utf8_polish_ci NOT NULL COMMENT 'Used when a user wants to find a page.',
  `block_element_contents` text COLLATE utf8_polish_ci NOT NULL COMMENT 'Block element contents.',
  `block_contents` text COLLATE utf8_polish_ci NOT NULL COMMENT 'Block contents (copied using content_id).'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `search_index`
--

INSERT INTO `search_index` (`result_id`, `page_id`, `language_id`, `content_id`, `block_element_id`, `date`, `translatable_search_result_category`, `block_element_url_parameters`, `page_title`, `meta_description`, `block_element_contents`, `block_contents`) VALUES
(7, 255, 2, 764, 9, '2015-12-08 11:30:00', 'Event', 'tworzenie-wydarzen-764-9', 'Wydarzenia', '', '<p><strong>1. Dodawanie wydarzeń.</strong></p>\n\n<p>Do tworzenia wydarzeń są zaprojektowane cztery bloki &ndash; &bdquo;Lista wydarzeń&rdquo; , &bdquo; Detale wydarzenia&rdquo;, &bdquo;Lista wydarzeń archiwalnych&rdquo; i &bdquo;Nadchodzące wydarzenia&rdquo;. Wszystkie bloki związane z wydarzeniami są ze sobą powiązane ale najpierw zacznijmy od wstawienia bloku Lista wydarzeń. Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną trybiku w prawym dolnym rogu strony.</p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony, wyszukujemy blok o nazwie &bdquo;Lista wydarzeń&rdquo;, można w tym celu użyć wyszukiwarki blok&oacute;w np. wybierając z listy &bdquo;Typ bloku&rdquo; pozycję &bdquo;Lista wydarzeń&rdquo;. Następnie chwytamy myszką ikonę zaznaczoną na zdjęciu poniżej i przeciągamy blok w wybrany na stronie slot na bloki.</p>\n\n<p>Wstawiony blok Lista wydarzeń posiada pr&oacute;cz standardowych ustawień, także unikalne dla tego bloku. Na belce Listy wydarzeń klikamy ikonę folderu - &bdquo;okna zarządzania&rdquo;.</p>\n\n<p>Otworzy nam się panel, w kt&oacute;rym utworzymy naszą kategorię wydarzeń i a potem samo wydarzenie, kt&oacute;re przypiszemy do tej kategorii. Klikamy przycisk &bdquo;Kategorie&rdquo;.</p>\n\n<p>A następnie niebieski przycisk &bdquo;Nowa kategoria&rdquo;.</p>\n\n<p>W panelu wpisujemy tytuł dla kategorii wydarzeń (w tym przykładzie jest to &bdquo;Nowa kategoria&rdquo;), będzie potem można przypisywać wiele wydarzeń pod tą kategorię i wyświetlać wszystkie naraz w danej kategorii, możemy zaznaczyć &bdquo;opublikuj po zapisaniu&rdquo; w celu natychmiastowego opublikowania tej kategorii i kliknąć &bdquo;Zapisz&rdquo;.</p>\n\n<p>Teraz możemy już tworzyć wydarzenia. Zamykamy panel &bdquo;Kategorie - wydarzenia&rdquo; i klikamy przycisk &bdquo;Nowe wydarzenie&rdquo;. Uzupełniamy formularz dodawania nowego wydarzenia, kt&oacute;ry zawiera pola:</p>\n\n<p>1. Kategoria &ndash; wybieramy z listy kategorię, do kt&oacute;rej przypiszemy nasze wydarzenie. W naszym przykładzie jest tylko jedna kategoria, kt&oacute;rą stworzyliśmy powyżej.</p>\n\n<p>2. Tytuł &ndash; nadajemy tytuł wydarzeniu, w naszym przykładzie jest to &bdquo;Nowe wydarzenie&rdquo;</p>\n\n<p>3. Adres URL &ndash; w momencie wprowadzania tytułu ustawi się automatycznie.</p>\n\n<p>4. Etykieta &ndash; pojawi się pod tytułem jako jego dopełnienie.</p>\n\n<p>5, 6, 7. &nbsp;Czas trwania, Godzina rozpoczęcia, Godzina zakończenia &ndash; domyślnie ustawia się na tydzień od momentu dodawania wydarzenia, można ustawić dowolna datę i godzinę. W zależności od tego czy wydarzenie już minęło (ma minioną datę jego zakończenia), jest aktualne (teraźniejsza data jest pomiędzy datą rozpoczęcia i zakończenia wydarzenia) lub przyszłe (data rozpoczęcia wydarzenia jest p&oacute;źniejsza od teraźniejszej) możemy wyświetlać je z listy wydarzeń odpowiednio w blokach &bdquo;wydarzenia archiwalne&rdquo;, &bdquo;detale wydarzenia&rdquo; i &bdquo;nadchodzące wydarzenia&rdquo;</p>\n\n<p>8.&nbsp;Obraz &ndash; możemy dodać obraz do wydarzenia lub kilka obraz&oacute;w i ustawić klikając na k&oacute;łeczko w prawym g&oacute;rnym rogu loadera, kt&oacute;ry obraz ma być jako zdjęcie gł&oacute;wne wyświetlane na liście wydarzeń. Ikoną strzałki ładujemy plik obrazu, możemy też użyć edycji ikoną oł&oacute;wka.</p>\n\n<p>9.&nbsp;Treść &ndash; opisujemy tutaj wydarzenie, po zapisaniu klikając edycję tego wydarzenia dostępnych będzie więcej opcji do uzupełnienia szczeg&oacute;ł&oacute;w wydarzenia.</p>\n\n<p>10. Zajawka &ndash; zajawka będzie wyświetlana na liście wydarzeń jako skr&oacute;cona treść, można użyć do 350 znak&oacute;w, cała treść będzie dostępna po kliknięciu na wydarzenie w detalu wydarzenia, o tym w dalszej części opisu.</p>\n\n<p>11. Uwagi &ndash; nie będą nigdzie publikowane, dla własnej potrzeby możemy zostawiać stosowną notatkę.</p>\n\n<p>12. Możliwość zapisania się na wydarzenia &ndash; po wybraniu opcji &bdquo;włączone&rdquo; - &bdquo;tak&rdquo; pojawi się miejsce na wpisanie danych kontaktowych osoby odpowiedzialnej za rozmowy o wydarzeniach, adresu e-mail i telefonu. Zostanie ona poinformowana o utworzeniu wydarzenia i o zapisaniu się na to wydarzenie przez każdego zapisującego się Gościa. Pod wydarzeniem na liście wydarzeń pojawi się przycisk dla Gości, będą oni mogli zapisać się na wydarzenie, każda zapisana osoba trafi na listę dostępną w panelu administracyjnym &bdquo;LISTA WYDARZEŃ&rdquo;.</p>\n\n<p>13. Pozostałe opcje &ndash; kilka opcji dotyczących wydarzenia.</p>\n\n<p>Po zapisaniu wszystkich wprowadzonych treści w bloku lista wydarzeń pojawi się nasze nowe wydarzenie. Teraz możemy skorzystać z ustawień dostępnych na belce bloku &bdquo;Lista wydarzeń&rdquo;.</p>\n\n<p>Klikamy na ikonę trybiku.</p>\n\n<p>Pojawi nam się okno z ustawieniami, kt&oacute;rych opis jest poniżej.</p>\n\n<p>1. Rozmiar tytułu &ndash; określa wielkość tytułu listy wydarzeń i czy ma być widoczny na stronie.</p>\n\n<p>2. Ilość element&oacute;w &ndash; ustawiamy ile naraz jest widocznych element&oacute;w na stronie paginacji.</p>\n\n<p>3. Wyświetl wydarzenia z kategorii &ndash; należy wybrać kategorię, do kt&oacute;rej są przypisane wydarzenia i tylko z tej kategorii będą wyświetlane.</p>\n\n<p>4. Wyświetl detale wydarzenia na stronie &ndash; &bdquo;Detale wydarzenia&rdquo; to blok, w kt&oacute;rym pojawi się nasze wydarzenie po kliknięciu na nie w liście wydarzeń. Blok &bdquo;Detale wydarzenia&rdquo; można umieścić na innej stronie i to na nią będzie nas kierowało po kliknięciu na wydarzenie z listy, można też umieścić detale wydarzenia na tej samej stronie.</p>\n\n<p>5. Ukrywanie listy wydarzeń &ndash; jeśli blok &bdquo;Detale wydarzenia&rdquo; znajduje się na tej samej stronie co lista wydarzeń można ukryć listę w momencie wyświetlania detali wydarzenia z listy wydarzeń.</p>\n\n<p style="margin-left:1.88cm">Można pozwolić gościom lub zarejestrowanym użytkownikom na dodawanie nowych wydarzeń, po zaznaczeniu drugiej opcji pojawi się przycisk (jeśli blok posiada przynajmniej jedną kategorię), kt&oacute;ry im to umożliwi, będzie on widoczny zar&oacute;wno na liście wydarzeń jak i w detalach wydarzenia. Każde nowo dodane wydarzenie musi zostać zaakceptowane przez moderatora lub administratora zanim stanie się opublikowane. W momencie zaznaczenia opcji pojawi się miejsce na wpisanie ustawień dla maila informującego o zatwierdzeniu wydarzenia i dane kontaktowe osoby odpowiedzialnej za rozmowy o wydarzeniach. Można też zredagować treść maili z informacjami o dodaniu wydarzenia przez gościa jak i odpowiedzi zwrotnej do niego.</p>\n\n<p style="margin-left:1.88cm">Ostatnia opcja umożliwi gościom dodawanie zdjęć w formularzu dodawania nowego wydarzenia i przesyłanie ich na serwer, nie jest to zalecane ze względu na bezpieczeństwo.</p>\n\n<p>Analogicznie jak blok &bdquo;Lista wydarzeń&rdquo; dodajemy teraz blok &bdquo;Detale wydarzenia&rdquo;. Możemy go umieścić na dowolnej stronie, w ustawieniach bloku &bdquo;Lista wydarzeń&rdquo; wystarczy podać adres strony, do kt&oacute;rej go wstawiamy, przeciągamy go w wybrany slot na bloki i klikamy na jego belce ikonę trybiku.</p>\n\n<p>Wybieramy rozmiar tytułu i czy ma być on widoczny. W ustawieniach wyświetlania można użyć opcji ukrywania tego bloku jeśli znajduje się on na tej samej stronie co blok &bdquo;Lista wydarzeń&rdquo;, czyli będzie on ukryty jeśli widoczna będzie lista wydarzeń.</p>\n\n<p>Tak jak bloki powyżej możemy wstawić na stronie pozostałe bloki czyli &bdquo;Nadchodzące wydarzenia&rdquo; i &bdquo;Wydarzenia archiwalne&rdquo;. W pierwszym z wymienionych blok&oacute;w będą się pojawiały wydarzenia z przyszła datą, w drugim wydarzenia minione. W ustawieniach tych blok&oacute;w są takie same elementy do zmiany ustawień tych blok&oacute;w.</p>\n\n<p>1. Rozmiar tytułu &ndash; wybieramy rozmiar tytułu i czy ma on być widoczny.</p>\n\n<p>2. Ilość element&oacute;w &ndash; ustawiamy ile naraz jest widocznych element&oacute;w na stronie paginacji.</p>\n\n<p>3. Wyświetl archiwalne wydarzenia z bloku &ndash; wybieramy z listy stronę i na niej blok, z kt&oacute;rego będą wyświetlane wydarzenia.</p>\n\n<p>4. Wyświetl archiwalne/nadchodzące wydarzenia z kategorii &ndash; wybieramy z listy kategorię wydarzeń lub zostawiamy domyślnie &ndash; wszystkie kategorie.</p>\n\n<p style="margin-left:1.27cm">Tak jak w bloku lista wydarzeń tak i w tych dw&oacute;ch blokach (lista wydarzeń archiwalnych i nadchodzące wydarzenia) możemy je ukryć jeśli na stronie znajdują się detale wydarzenia zaznaczając opcje &bdquo;Ukryj listę wydarzeń jeśli aktualny adres URL prowadzi do detali wydarzenia&rdquo;.</p>\n\n<p><strong>2. Edycja wydarzeń.</strong></p>\n\n<p>Na belce bloku Listy wydarzeń klikamy ikonę folderu - &bdquo;okna zarządzania&rdquo;. Analogicznie edytujemy wydarzenia z &bdquo;Listy wydarzeń archiwalnych&rdquo; i &bdquo;Nadchodzących wydarzeń&rdquo;.Otworzy nam się panel, w kt&oacute;rym możemy edytować wybrane wydarzenie, lub skorzystać z ikony szybkiej edycji umieszczonej przy tytule wydarzenia na liście wydarzeń.</p>\n\n<p>Przy edycji wydarzenia otworzy nam się panel, kt&oacute;ry posiada te same elementy jak przy dodawaniu nowego wydarzenia (patrz - Dodaj nowe wydarzenie - powyżej) powiększony o bardziej szczeg&oacute;łowe elementy typu: Gdzie, Wstęp, Opis, Kontakt, Dodatkowe Informacje. Po zakończeniu edycji klikamy &bdquo;Zapisz&rdquo;.</p>', ''),
(8, 256, 2, 770, 1, '2015-12-09 10:00:00', '_search_category_news', 'nowe-aktualnosci-770-1', 'Aktualności', '', '<p><strong>1. Dodawanie aktualności.</strong></p>\n\n<p>Obsługę aktualności zapewniają cztery bloki &ndash; &bdquo;Lista aktualności&rdquo; , &bdquo; Detale aktualności&rdquo;, &bdquo;Formularz dodawania aktualności&rdquo; i &bdquo;Aktualności slider&rdquo;. Zacznijmy od wstawienia bloku Lista aktualności. Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną trybiku.</p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony, wyszukujemy blok o nazwie &bdquo;Lista aktualności&rdquo;, można w tym celu użyć wyszukiwarki blok&oacute;w np. wybierając z listy &bdquo;Typ bloku&rdquo; pozycję &bdquo;Lista aktualności&rdquo;. Następnie chwytamy myszką ikonę zaznaczoną na zdjęciu poniżej i przeciągamy blok w wybrany na stronie slot na bloki.</p>\n\n<p>Wstawiony blok Lista aktualności posiada pr&oacute;cz standardowych ustawień, także unikalne dla tego bloku. Na belce Listy aktualności klikamy ikonę folderu - &bdquo;okna zarządzania&rdquo;.</p>\n\n<p>Otworzy nam się panel, w kt&oacute;rym utworzymy naszą kategorię aktualności a potem nową aktualność, kt&oacute;rą przypiszemy do tej kategorii. Klikamy przycisk &bdquo;Kategorie&rdquo;.</p>\n\n<p>A następnie przycisk &bdquo;Nowa kategoria&rdquo;.</p>\n\n<p>W panelu wpisujemy tytuł dla kategorii aktualności, będzie potem można przypisywać wiele aktualności pod tą kategorię i wyświetlać wszystkie naraz w danej kategorii, możemy zaznaczyć &bdquo;opublikuj po zapisaniu&rdquo; w celu natychmiastowego opublikowania tej kategorii i kliknąć &bdquo;Zapisz&rdquo;.</p>\n\n<p>Teraz możemy już tworzyć nowe aktualności. Zamykamy panel &bdquo;Kategorie - Zarządzanie&rdquo; i klikamy przycisk &bdquo;Nowa aktualność&rdquo;. Uzupełniamy formularz dodawania nowej aktualności.</p>\n\n<p>1. Kategoria &ndash; wybieramy z listy kategorię, do kt&oacute;rej przypiszemy nasze wydarzenie. W naszym przykładzie jest tylko jedna kategoria, kt&oacute;rą stworzyliśmy powyżej.</p>\n\n<p>2. Tytuł &ndash; nadajemy tytuł wydarzeniu, w naszym przykładzie jest to &bdquo;Nowe wydarzenie&rdquo;</p>\n\n<p>3. Adres URL &ndash; w momencie wprowadzania tytułu ustawi się automatycznie.</p>\n\n<p>4. Etykieta &ndash; pojawi się pod tytułem jako jego dopełnienie.</p>\n\n<p>5, 6. Data, Godzina rozpoczęcia &ndash; domyślnie ustawia się na obecną od momentu dodawania aktualności, można ustawić inną datę i godzinę rozpoczęcia.</p>\n\n<p>7.&nbsp;Obraz &ndash; możemy dodać obraz do wydarzenia lub kilka obraz&oacute;w i ustawić klikając na k&oacute;łeczko w prawym g&oacute;rnym rogu loadera, kt&oacute;ry obraz ma być jako zdjęcie gł&oacute;wne wyświetlane na liście aktualności. Ikoną strzałki ładujemy plik obrazu, możemy też użyć edycji ikoną oł&oacute;wka.</p>\n\n<p>8.&nbsp;Treść &ndash; opisujemy tutaj aktualność korzystając z CKEdytora.</p>\n\n<p>9. Zajawka &ndash; zajawka będzie wyświetlana na liście wydarzeń jako skr&oacute;cona treść, można użyć do 350 znak&oacute;w, cała treść będzie dostępna w detalach aktualności po kliknięciu na aktualność, o tym w dalszej części opisu.</p>\n\n<p>Po zapisaniu wszystkich wprowadzonych treści w bloku lista aktualności pojawi się nasza nowa aktualność. Teraz możemy skorzystać z ustawień dostępnych na belce bloku &bdquo;Lista aktualności&rdquo;.</p>\n\n<p>Klikamy na ikonę trybiku.</p>\n\n<p>Pojawi nam się okno z ustawieniami, kt&oacute;rych opis jest poniżej.</p>\n\n<p>1. Rozmiar tytułu &ndash; określa wielkość tytułu listy wydarzeń i czy ma być widoczny na stronie.</p>\n\n<p>2. Ilość element&oacute;w &ndash; ustawiamy ile naraz jest widocznych element&oacute;w na stronie paginacji.</p>\n\n<p>3. Wyświetl detale aktualności na stronie &ndash; &bdquo;Detale aktualności&rdquo; to blok, w kt&oacute;rym pojawi się nasza aktualność po kliknięciu na nią w liście aktualności. Blok &bdquo;Detale aktualności&rdquo; można umieścić na innej stronie i to na nią będzie nas kierowało po kliknięciu na aktualność z listy aktualności, można też umieścić detale aktualności na tej samej stronie. Domyślnie ustawia się strona startowa (gł&oacute;wna).</p>\n\n<p>Ukrywanie listy aktualności &ndash; jeśli blok &bdquo;Detale aktualności&rdquo; znajduje się na tej samej stronie co lista aktualności można ukryć listę w momencie wyświetlania detali aktualności.</p>\n\n<p>Analogicznie jak blok &bdquo;Lista aktualności&rdquo; dodajemy teraz blok &bdquo;Detale aktualności&rdquo;. Możemy go umieścić na dowolnej stronie, w ustawieniach bloku &bdquo;Lista aktualności&rdquo; wystarczy podać adres strony (tytuł strony), do kt&oacute;rej go wstawiamy, przeciągamy go w wybrany slot na bloki i klikamy na jego belce ikonę trybiku.</p>\n\n<p>Wybieramy rozmiar tytułu i czy ma być on widoczny. W ustawieniach wyświetlania można użyć opcji ukrywania tego bloku jeśli znajduje się on na tej samej stronie co blok &bdquo;Lista aktualności&rdquo;, czyli będzie on ukryty jeśli widoczna będzie lista aktualności.</p>\n\n<p>Tak jak bloki powyżej możemy wstawić na stronie pozostałe bloki czyli &bdquo;Formularz dodawania aktualności&rdquo; i &bdquo;Aktualności slider&rdquo;. Pierwszy z wymienionych blok&oacute;w wyświetli formularz dla gości, kt&oacute;rzy będą mogli dodawać aktualności. Dodane w ten spos&oacute;b aktualności trafią do listy aktualności, jednak nie będą opublikowane, dopiero po pomyślnej weryfikacji przez moderatora lub administratora strony będą widoczne. Klikamy na belce bloku formularza ikonę trybiku &ndash; ustawień.</p>\n\n<p>Ustawiamy rozmiar tytułu i czy ma on być widoczny. Dalszą opcją jest wyb&oacute;r bloku, z kt&oacute;rym chcemy skojarzyć, id bloku widoczne jest na jego belce z lewej strony.</p>\n\n<p>Ostatni blok związany z aktualnościami to &bdquo;Aktualności slider&rdquo;, wyświetla listę aktualności w postaci slidera, dodajemy go do strony jak inne bloki. Przyjrzyjmy się jego ustawieniom, klikamy na wstawionym bloku &bdquo;Aktualności slider&rdquo; ikonę trybiku, otworzy nam się formularz z ustawieniami jak poniżej.</p>\n\n<p>Na samej g&oacute;rze ustawiamy rozmiar tytułu i czy ma on być widoczny.</p>\n\n<p>1. Wyświetl aktualności z bloku &ndash; wybieramy stronę i blok z listą aktualności, kt&oacute;ra się na niej znajduje, slider będzie pobierał z niej aktualności do wyświetlania.</p>\n\n<p>2. Wyświetl aktualności z kategorii &ndash; wybieramy kategorię aktualności, będą się wyświetlać tylko aktualności z danej kategorii, domyślnie jest ustawione &bdquo;Wszystkie kategorie&rdquo;.</p>\n\n<p>3. Całkowita ilość aktualności w sliderze &ndash; wybieramy ilość aktualności do wyświetlenia.</p>\n\n<p>4. Ustawienia przewijania &ndash; ustawiamy czas pomiędzy przejściami (slajdami), czas samego przejścia oraz typ przewijania, dostępnych są 4 typy oraz brak przewijania:</p>\n\n<p>- skocz na początek po wyświetleniu ostatniego elementu</p>\n\n<p>- skocz na koniec po wyświetleniu pierwszego elementu</p>\n\n<p>- użyj pierwszej i drugiej opcji jednocześnie</p>\n\n<p>- użyj zapętlonego przewijania</p>\n\n<p><strong>2. Edycja aktualności.</strong></p>\n\n<p>Na belce bloku Listy aktualności klikamy ikonę folderu - &bdquo;okna zarządzania&rdquo;.</p>\n\n<p>Otworzy nam się panel, w kt&oacute;rym możemy edytować wybrane wydarzenie</p>\n\n<p>lub skorzystać z ikony szybkiej edycji umieszczonej przy tytule wydarzenia na liście wydarzeń.</p>\n\n<p>Przy edycji aktualności otworzy nam się panel, kt&oacute;ry posiada te same elementy jak przy dodawaniu nowej aktualności (patrz - Dodaj nową aktualność - powyżej. Po zakończeniu edycji klikamy &bdquo;Zapisz&rdquo;.</p>', ''),
(9, 256, 2, 770, 2, '2015-12-09 13:00:00', '_search_category_news', 'dodawanie-nowej-strony-770-2', 'Aktualności', '', '<p style="margin-left:1.25cm">Po zalogowaniu się&nbsp;klikamy na widoczny z lewej strony przycisk panelu narzędzi administratora.</p>\n\n<p style="margin-left:1.25cm"><span style="line-height:1.6em">Wybieramy pozycję &bdquo;STRONY&rdquo;.</span></p>\n\n<p style="margin-left:0.64cm">W otwartym oknie ustawień wybieramy niebieski przycisk &bdquo;Nowa strona&rdquo;.</p>\n\n<p style="margin-left:0.64cm">Otworzy nam się formularz dodawania nowej strony, kt&oacute;rego poszczeg&oacute;lne elementy wraz z charakterystyką przedstawia poniższy opis.</p>\n\n<p style="margin-left:0.64cm"><strong>1. </strong>Tytuł &ndash; może zawierać dowolne znaki, litery, cyfry i znaki specjalne .</p>\n\n<p style="margin-left:0.64cm"><strong>2. </strong>Język &ndash; wyb&oacute;r języka dla nowej strony, domyślnie polski, dostępny angielski i rosyjski.</p>\n\n<p style="margin-left:0.64cm"><strong>3. </strong>Adres URL &ndash; domyślnie adres ustawi się sam na podstawie tytułu nowej strony, można jednak go zmienić na inny niż domyślny, nie zalecamy jednak samodzielnej zmiany adresu URL &ndash; zmiany tylko dla zaawansowanych użytkownik&oacute;w.</p>\n\n<p style="margin-left:0.64cm"><strong>4. </strong>Strona nadrzędna &ndash; umieszcza w adresie do naszej nowej strony wybraną stronę nadrzędną w taki spos&oacute;b: pol/page/<strong>strona_nadrzedna</strong>/<strong>Adres_URL </strong>nadając jakby rodzica naszej nowej stronie.</p>\n\n<p style="margin-left:0.64cm"><strong>5. </strong>English (US) &ndash; ekwiwalentna strona dla języka angielskiego jeśli taka istnieje, domyślnie jest to strona gł&oacute;wna. Wybieramy tu stronę kt&oacute;ra wyświetli się po przełączeniu języka na angielski.</p>\n\n<p style="margin-left:0.64cm"><strong>6. </strong>Russian &ndash; ekwiwalentna strona dla języka rosyjskiego jeśli taka istnieje, domyślnie jest to strona gł&oacute;wna. Wybieramy tu stronę kt&oacute;ra wyświetli się po przełączeniu języka na rosyjski.</p>\n\n<p style="margin-left:0.64cm"><strong>7.</strong> Meta &bdquo;keywords&rdquo; - umieszczamy tu kluczowe słowa, kt&oacute;re najlepiej charakteryzują zawartość dokumentu. Postaraj się wyobrazić sobie jakich sł&oacute;w może użyć potencjalny użytkownik oraz jakie ewentualne błędy lub liter&oacute;wki mogą się wkraść do jego zapytania. Weź r&oacute;wnież pod uwagę, że nie wszyscy używają polskich znak&oacute;w diakrytycznych.</p>\n\n<p style="margin-left:0.64cm"><strong>8. </strong>Meta &bdquo;description&rdquo; - opis zawartości strony, zostanie wyświetlony przez wyszukiwarki zaraz po tytule strony. Należy go dobrać bardzo starannie, tak aby stanowił zachętę do odwiedzenia właśnie tej strony.</p>\n\n<p style="margin-left:0.64cm"><strong>9. </strong>Częstotliwość zmian na stronie &ndash; polepsza wyniki wyszukiwania w google, im częściej tym lepiej.</p>\n\n<p style="margin-left:0.64cm"><strong>10. </strong>Priorytet dla adresu URL &ndash; określa ważność adresu URL dla wyszukiwarki, można w podpiętych opcjach zaznaczyć czy wyszukiwarki mają indeksować naszą stronę i tworzyć jej kopię, można też zabronić przeglądarkom trzymać tę kopię w pamięci podręcznej.</p>\n\n<p style="margin-left:0.64cm"><strong>11. </strong>Gł&oacute;wny szablon strony &ndash; aktualnie jest jeden szablon &bdquo;Domyślny&rdquo;.</p>\n\n<p style="margin-left:0.64cm"><strong>12. </strong>Układ &ndash; układ do wstawiania element&oacute;w na stronie, do wyboru są 4 układy:</p>\n\n<p style="margin-left:0.64cm"><strong>- </strong>dwie kolumny - podstrona</p>\n\n<p style="margin-left:0.64cm">- jedna kolumna</p>\n\n<p style="margin-left:0.64cm">- dwie kolumny</p>\n\n<p style="margin-left:0.64cm">- trzy kolumny</p>\n\n<p style="margin-left:0.64cm"><strong>13. </strong>Opcja ustawia nową stronę jako &bdquo;włączoną&rdquo; od razu po zapisaniu.</p>\n\n<p style="margin-left:0.64cm"><strong>Edycja strony dostępna jest po kliknięciu tego przycisku, formularz edycji zawiera te same elementy jak formularz dodawania nowej strony.</strong></p>', ''),
(10, 256, 2, 770, 3, '2015-12-09 13:15:00', '_search_category_news', 'dodawanie-nowego-uzytkownika-770-3', 'Aktualności', '', '<p><strong>Dodanie/edycja Użytkownika.</strong></p>\n\n<p style="margin-left:1.25cm">Po zalogowaniu się&nbsp;klikamy na widoczny z lewej strony przycisk panelu narzędzi administratora.</p>\n\n<p style="margin-left:0.64cm">Wybieramy pozycję &bdquo;UŻYTKOWNICY&rdquo;.</p>\n\n<p style="margin-left:0.64cm">W otwartym oknie ustawień wybieramy niebieski przycisk &bdquo;Nowy użytkownik&rdquo;.</p>\n\n<p style="margin-left:0.64cm">Otworzy nam się formularz dodawania nowego użytkownika, kt&oacute;rego poszczeg&oacute;lne elementy wraz z charakterystyką przedstawia poniższy opis.</p>\n\n<p style="margin-left:0.64cm"><strong>1. </strong>Login &ndash; nazwa kt&oacute;rej nowy użytkownik będzie używał do logowania się, może zawierać dowolne znaki, litery, cyfry i znaki specjalne .</p>\n\n<p style="margin-left:0.64cm"><strong>2. </strong>Typ konta &ndash; wyb&oacute;r jednego z trzech typ&oacute;w konta:</p>\n\n<p style="margin-left:0.64cm"><strong>-</strong> User: z tymi uprawnieniami użytkownik może logować się, przeglądać treści publiczne strony, zapisywać się na wydarzenia.</p>\n\n<p style="margin-left:0.64cm"><strong>-</strong> Moderator: dziedziczy od User , poza tym ma uprawnienia do wprowadzania zmian użytkownik&oacute;w i stron.</p>\n\n<p style="margin-left:0.64cm"><strong>- </strong>Administrator: dziedziczy od moderatora, z tymi uprawnieniami użytkownik może dodawać, edytować i usuwać i bloki (dostępny tryb projektowy).</p>\n\n<p style="margin-left:0.64cm"><strong>3. </strong>E-mail &ndash; e-mail nowego użytkownika.</p>\n\n<p style="margin-left:0.64cm"><strong>4. </strong>Nowe hasło &ndash; hasło należy nadać samemu, jest to jedyna droga żeby m&oacute;c się nim potem logować, hasło generowane automatycznie jest zabezpieczone szyfrowaniem, może mieć dowolną liczbę znak&oacute;w.<strong> </strong></p>\n\n<p style="margin-left:0.64cm"><strong>5. </strong>Podstawowe dane dotyczące nowego użytkownika.</p>\n\n<p><strong>Edycja danych użytkownika dostępna jest po kliknięciu ikony edycji w wierszu z użytkownikiem, u kt&oacute;rego chcemy wprowadzić zmiany, formularz edycji zawiera te same elementy jak formularz dodawania nowego użytkownika.</strong></p>', ''),
(11, 256, 2, 770, 4, '2015-12-09 13:15:00', '_search_category_news', 'dodawanie-blokow-na-stronie-770-4', 'Aktualności', '', '<p><strong>Dodawanie blok&oacute;w.</strong></p>\n\n<p style="margin-left:0.64cm">Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną trybiku.</p>\n\n<p style="margin-left:0.64cm">W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p style="margin-left:0.64cm">Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony</p>\n\n<ol>\n	<li>\n	<p>Kategoria językowa &ndash; ustawienie języka, każdemu blokowi można przypisać kategorię językową, tworząc nowy blok czy edytując już istniejący &ndash; więcej w pkt 6b.</p>\n	</li>\n	<li>\n	<p>Typ bloku &ndash; lista wszystkich typ&oacute;w blok&oacute;w w systemie.</p>\n	</li>\n	<li>\n	<p>Szukaj po nazwie &ndash; wyszukiwarka blok&oacute;w, należy wpisać całą nazwę bądź jej fragment.</p>\n	</li>\n	<li>\n	<p>Ikona dodania bloku do strony &ndash; należy chwycić ikonę i przeciągnąć ją nad wyznaczone pola na stronie, miejsca, w kt&oacute;rych można upuścić blok zaznaczone są na zdjęciu &bdquo;<strong>Sloty do wstawiania blok&oacute;w</strong>&rdquo; poniżej tej listy, w momencie gdy blok znajdzie się nad takim miejscem, zrobi się ono większe i podświetlone na pomarańczowo &ndash; wtedy można upuścić blok i znajdzie się on na stronie.</p>\n	</li>\n	<li>\n	<p>Ikona przestawiania &ndash; można porządkować bloki poprzez przesuwanie ich w inne miejsca tabeli, domyślnie ustawione są alfabetycznie.</p>\n	</li>\n	<li>\n	<p>Ikona edycji &ndash; po kliknięciu w ikonę pojawi się okno ustawień opisane na zdjęciu &bdquo;<strong>Okno ustawień edycji właściwości bloku</strong>&rdquo; poniżej tej listy.</p>\n	</li>\n</ol>\n\n<p style="margin-left:0.5cm"><strong>6a. </strong>Nazwa bloku we wszystkich językach &ndash; nadanie nazwy dla bloku w języku polskim, angielskim i rosyjskim</p>\n\n<p style="margin-left:0.5cm"><strong>6b. </strong>Traktuj jako &ndash; przypisanie bloku do kategorii języka, domyślnie jest to &bdquo;wszystkie kategorie&rdquo;, czyli bloki będą widoczne na wszystkich stronach niezależnie od języka strony, można ustawić język polski, angielski lub rosyjski &ndash; wtedy dany blok będzie widoczny tylko na stronie z ustawieniem języka adekwatnym do wybranego języka dla bloku.</p>\n\n<ol start="7">\n	<li>\n	<p>Ikona info &ndash; info o bloku.</p>\n	</li>\n</ol>\n\n<p><strong>Sloty do wstawiania blok&oacute;w</strong></p>\n\n<p>Teoretycznie bloki można upuszczać w dowolnym slocie, jednak niekt&oacute;re sloty mają lepsze predyspozycje do wyświetlania niekt&oacute;rych blok&oacute;w, są one zaznaczone na zdjęciu &bdquo;<strong>Sloty do wstawiania blok&oacute;w</strong>&rdquo;.</p>\n\n<p>Po upuszczeniu bloku w dobre miejsce doda się on na stronie.</p>', ''),
(12, 256, 2, 770, 5, '2015-12-09 13:15:00', '_search_category_news', 'dodawanie-i-edycja-menu-770-5', 'Aktualności', '', '<p><strong>Dodawanie i edycja menu &ndash; menu gł&oacute;wne, menu nawigacyjne i menu skr&oacute;t&oacute;w.</strong></p>\n\n<p>Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną trybiku.</p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony. Do dodawania menu do strony przygotowane są trzy bloki, wstawmy pierwszy z nich &ndash; kolejne będą wstawiane analogicznie. Wyszukujemy blok o nazwie &bdquo;Menu gł&oacute;wne&rdquo;, można w tym celu użyć wyszukiwarki blok&oacute;w np. wybierając z listy &bdquo;Typ bloku&rdquo; pozycję &bdquo;Menu gł&oacute;wne&rdquo;. Następnie chwytamy myszką ikonę zaznaczoną na zdjęciu poniżej i przeciągamy blok w wybrany na stronie slot na bloki.</p>\n\n<p>Przechodzimy do ustawień klikając na belce bloku ikonę trybiku.</p>\n\n<p>Otworzy nam się okno z ustawieniami dla tego bloku.</p>\n\n<p>1. Tytuł -ustawiamy rozmiar tytułu naszej galerii oraz czy ma on być widoczny .</p>\n\n<p>2. Kategoria &ndash;wybieramy kategorię, do kt&oacute;rej są przypisywane elementy menu, w systemie stworzona jest &bdquo;Domyślna kategoria&rdquo; gotowa do użycia.</p>\n\n<p>3. Orientacja &ndash; możemy wybrać jak ma się wyświetlać nasze menu &ndash; poziomo lub pionowo.</p>\n\n<p>4. Pozostałe opcje &ndash; tu możemy dodać przycisk (mała ikona plusa) rozwijania/zwijania do każdej pozycji menu.</p>\n\n<p>Aby dodać elementy do menu klikamy na belce bloku ikonę folderu &ndash; okno zarządzania.</p>\n\n<p>Następnie przycisk na otwartym oknie &bdquo;Kategorię&rdquo;, jeśli chcemy stworzyć inną niż domyślną kategorię dla naszych element&oacute;w menu lub przycisk &bdquo;Nowy element&rdquo; żeby dodać pozycję do menu.</p>\n\n<p>W formularzu dodawania nowego elementu po kolei zmieniamy ustawienia.</p>\n\n<p>1. Kategoria &ndash; wybieramy kategorię element&oacute;w menu.</p>\n\n<p>2. Element nadrzędny &ndash; tą opcją kontrolujemy ustawienie poszczeg&oacute;lnych element&oacute;w menu względem siebie. Możemy wybrać, przy każdym nowym elemencie menu, kt&oacute;ry element ma być nadrzędny względem wstawianego.</p>\n\n<p>3. Typ adresu URL &ndash; wybieramy wewnętrzny, zewnętrzny lub brak.</p>\n\n<p>4. Adres URL &ndash; wybieramy dokąd ma prowadzić element menu, jeśli w typie adresu URL wybrana została pozycja &bdquo;wewnętrzny&rdquo; dostępna tu będzie lista wszystkich naszych stron, jeśli wybrane zostało &bdquo;brak&rdquo; to pole będzie nieczynne.</p>\n\n<p>5. Typ elementu &ndash; możemy zdecydować jak będzie się prezentować nasz element menu. DO wyboru jest kilka opcji:</p>\n\n<p><span style="line-height:1.6em">- tylko tekst</span></p>\n\n<p><span style="line-height:1.6em">- tylko ikona</span></p>\n\n<p><span style="line-height:1.6em">- tylko obraz</span></p>\n\n<p><span style="line-height:1.6em">- ikona + tekst</span></p>\n\n<p><span style="line-height:1.6em">- obraz + tekst</span></p>\n\n<p>6. Tytuł &ndash; wpisujemy tytuł naszego elementu menu.</p>\n\n<p>W ten spos&oacute;b dodaliśmy pierwszą pozycje w menu, analogicznie tworzymy kolejne, dodając grupy i elementy menu.</p>\n\n<p>Menu nawigacyjne i menu skr&oacute;t&oacute;w posiadają takie same ustawienia i okna zarządzania, r&oacute;żnią się jedynie wyświetlaniem. Zdjęcie poniżej przedstawia zestawienie trzech typ&oacute;w menu obok siebie.</p>\n\n<p>&nbsp;</p>', ''),
(13, 255, 2, 764, 10, '2015-12-09 13:30:00', 'Event', 'tworzenie-blokow-uniwersalnych-764-10', 'Wydarzenia', '', '<p><strong>Tworzenie blok&oacute;w dynamicznych.</strong></p>\n\n<p>Każdy blok posiada swoje ustawienia,&nbsp;na belce ustawień pierwsza ikona dyskietki oznacza, że można ten blok zapisać jako dynamiczny i dodać go potem na innej stronie bez potrzeby tworzenia jego treści od początku.</p>\n\n<p>Po kliknięciu w ikonę otworzy nam się okno zapisu.</p>\n\n<p>1. Należy podać nazwy bloku dla wszystkich język&oacute;w, może być to wsp&oacute;lna nazwa dla wszystkich język&oacute;w.</p>\n\n<p>2. Wybrany język ma wpływ na wyszukiwanie bloku w wyszukiwarce blok&oacute;w w polu &bdquo;kategoria językowa&rdquo;.</p>\n\n<p>Po zapisaniu blok jest dostępny po najechaniu kursorem na ikonę dyskietki umieszczoną obok ikony plusa do dodawania blok&oacute;w w lewej dolnej części ekranu.</p>\n\n<p>&nbsp;</p>\n\n<p>Tak za zapisany blok można upuszczać na dowolnych stronach, usunięcie takiego bloku ze strony nie ma wpływu na inne jego wystąpienia na innych stronach. Natomiast usunięcie tego bloku z listy blok&oacute;w dynamicznych spowoduje usunięcie go z listy i tym samym z wszystkich stron, na kt&oacute;rych on występuje.</p>\n\n<p><a name="_GoBack"></a></p>\n\n<p>&nbsp;</p>', ''),
(14, 248, 2, 784, 1, NULL, 'Static content', 'ckeditor-1', 'Start', '', '<p><strong>Korzystanie z CKEditora.</strong></p>\n\n<p>CKEditor jest edytorem o dużych możliwościach, jego wszystkie narzędzia dostępne są po dwukrotnie kliknąć myszką na tekst &bdquo;Przykładowy tekst. Proszę go zmienić.&rdquo; lub ikonę edycji.</p>\n\n<p>CKEditor&nbsp;jest wizualnym&nbsp;edytorem HTML&nbsp;rozpowszechnianym na licencji&nbsp;Open Source, kt&oacute;ry pozwala na łatwe wprowadzanie tekstu za pomocą&nbsp;interfejsu&nbsp;przypominającego programy typu&nbsp;Microsoft Word.&nbsp;Formatowanie tekstu&nbsp;odbywa się za pomocą zestawu przycisk&oacute;w umożliwiających wybranie&nbsp;czcionki, rozmiaru, koloru liter i tła, ustawienie pogrubienia, podkreślenia czy&nbsp;kursywy, jak r&oacute;wnież wyr&oacute;wnanie tekstu (do lewej, do prawej, wycentrowanie,&nbsp;wyjustowanie). Poza tym użytkownik ma do dyspozycji funkcje takie jak wstawienie listy (wypunktowanej lub numerowanej), formularzy i wszystkich jego element&oacute;w, tabeli, obrazka, video czy odnośnika (linku).</p>\n\n<p>Poniżej znajdują się wszystkie przyciski CKEditora, wymienione z nazw w kierunku od lewej do prawej.</p>\n\n<p>Pogrubienie, kursywa, podkreślenie, przekreślenie, indeks dolny, indeks g&oacute;rny, usuń formatowanie, lista numerowana, lista wypunktowana, zmniejsz wcięcie, zwiększ wcięcie, cytat, utw&oacute;rz pojemnik Div, wyr&oacute;wnaj do lewej, wyśrodkuj, wyr&oacute;wnaj do prawej, wyjustuj, kierunek tekstu od lewej strony do prawej, kierunek tekstu od prawej strony do lewej, set language, wstaw/edytuj odnośnik, usuń odnośnik, wstaw/edytuj kotwicę, obrazek, flash, tabela, wstaw poziomą linie, wstaw znak specjalny, wstaw podział strony, iFrame, załącznik video z Youtube.</p>\n\n<p>Szablony, wytnij, kopiuj, wklej, wklej jako zwykły tekst, wklej z programu MS Word, cofnij, pon&oacute;w, znajdź, zamień, zaznacz wszystko, sprawdź pisownię podczas pisania, formularz, pole wyboru (checkbox), przycisk opcji (radio), pole tekstowe, obszar tekstowy, lista wyboru, przycisk, przycisk graficzny, pole ukryte.</p>\n\n<p>Style formatujące, format, czcionka, rozmiar, kolor tekstu, kolor tła, pokaż bloki, pokaż informacje o CKEditor.</p>', ''),
(15, 248, 2, 784, 2, NULL, 'Static content', 'ustawienia-blokow-2', 'Start', '', '<p><strong>Ustawienia blok&oacute;w.</strong></p>\n\n<p>Każdy blok wstawiony na stronę po najechaniu na niego kursorem wyświetli kilka opcji na swojej belce.</p>\n\n<p>W lewym g&oacute;rnym rogu przed numerem ID i nazwą znajduje się &bdquo;krzyżyk&rdquo; służący do przerzucenia bloku do innego slotu. Działanie ikon z prawej strony przedstawia opis poniżej:</p>\n\n<p>1. Zapisz jako blok dynamiczny &ndash; domyślnie wstawiony blok jest widoczny tylko na stronie, na kt&oacute;rej go wstawiliśmy, gotowy wypełniony naszymi treściami blok można zapisać jako dynamiczny i dodać go potem na innej stronie bez potrzeby tworzenia go od początku. Zapisany w ten spos&oacute;b blok jest dostępny po najechaniu kursorem na ikonę dyskietki umieszczoną pod ikoną plusa do dodawania blok&oacute;w.</p>\n\n<p>2. Opublikuj/ukryj blok &ndash; klikając na tą ikonę można ukryć blok, w momencie wyjścia z trybu projektowego nie będzie on widoczny, po ponownym kliknięciu na tą ikonę, blok będzie opublikowany.</p>\n\n<p>3. Okno zarządzania &ndash; po kliknięciu otworzy się okno zarządzania blokiem, w przypadku bloku menu gł&oacute;wnego będą to ustawienia dotyczące menu, dodawanie, usuwanie, ukrywanie, publikacja i edycja kategorii menu oraz jego element&oacute;w.</p>\n\n<p>4. Ustawienia &ndash; pod tą ikoną kryją się og&oacute;lne ustawienia, m. in. Rozmiar tytułu menu i czy ma być widoczny, z kt&oacute;rej kategorii mają być wyświetlane elementy menu, czy ma być to menu pionowe bądź poziome.</p>\n\n<p>5. Usuwanie bloku &ndash; kliknięcie na ikonę kosza otworzy okno z potwierdzeniem usunięcia bloku, zostanie on usunięty ze strony a nie listy blok&oacute;w. &nbsp; &nbsp;</p>', ''),
(16, 248, 2, 784, 3, NULL, 'Static content', 'o-bloku-zakladki-3', 'Start', '', '<p>Blok Zakładki jest prosty w zarządzaniu.&nbsp;</p>\n\n<p>Będąc zalogowanym włączamy tryb edycji klikając myszką na przycisk z ikoną puzla.</p>\n\n<p><img alt="" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_08_30_png.png" style="height:137px; width:271px" /></p>\n\n<p>W trybie edycji widoczne są narzędzia do obsługi blok&oacute;w, najeżdżamy kursorem na przycisk z ikoną plusa.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Start.png" /></p>\n\n<p>Z dołu strony wysunie się panel z wszystkimi dostępnymi blokami, kt&oacute;re można użyć do tworzenia strony, wyszukujemy blok o nazwie &bdquo;Zakładki&rdquo;, można w tym celu użyć wyszukiwarki blok&oacute;w np. wybierając z listy &bdquo;Typ bloku&rdquo; pozycję &bdquo;Zakładki&rdquo;. Następnie chwytamy myszką ikonę strzałki skierowanej ku g&oacute;rze (pojawi się po najechaniu myszką na blok Zakładek) i przeciągamy blok w wybrany na stronie slot na bloki.</p>\n\n<p><img alt="" class="img-responsive" src="/ckeditor-files/images/grafiki%20do%20opisu%20blokow/Zrzut_ekranu_2016-01-04_o_11_19_33.png" /></p>\n\n<p>Wprowadzamy treść w jednej zakładce, jeśli chcemy dodajemy kolejną zakładkę klikając ikonę plusa przy zakładce.</p>', ''),
(17, 270, 2, 799, 1, NULL, 'About employee', 'norbert-banulski-799-1', 'o nas', '', '<p>Absolwent Wyższej Szkoły Menedżerskiej w Warszawie, interesuje się kodowaniem stron internetowych oraz web design&#39;em.</p>', ''),
(18, 270, 2, 799, 2, NULL, 'About employee', 'wioleta-przybysz-799-2', 'o nas', '', '<p><span style="background-color:rgb(249, 249, 249); color:rgb(44, 45, 48); font-family:slack-lato,applelogo,sans-serif; font-size:15px">Absolwentka Instytutu Edukacji Artystycznej w sztukach plastycznych  na Akademii Pedagogiki Specjalnej im. Marii Grzegorzewskiej w Warszawie.&nbsp;</span></p>', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `settings`
--

CREATE TABLE `settings` (
  `setting_id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(228) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `value` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `settings`
--

INSERT INTO `settings` (`setting_id`, `name`, `value`) VALUES
(1, 'additional_scripts', ''),
(2, 'default_subject_for_mail_about_newly_added_event', 'CodiLab'),
(3, 'default_contents_for_mail_about_newly_added_event', '<p>New event has been added by {{author_email}} at {{event_added_date}}.</p>'),
(4, 'emails_to_notify_about_important_actions_in_system', ''),
(5, 'show_message_about_cookies', '1'),
(6, 'message_about_cookies_popup_position', 'bottom'),
(7, 'message_about_cookies_eng', '<p>This website uses cookies to help us give you the best experience when you visit our website. By continuing to use this website, you consent to our use of these cookies.</p>'),
(8, 'message_about_cookies_pol', '<p>Ta strona internetowa wykorzystuje ciasteczka (cookies), by polepszyć Tw&oacute;j komfort korzystania z naszej strony. Aby nadal m&oacute;c korzystać z tej strony, musisz wyrazić zgodę na nasze wykorzystanie tych plik&oacute;w.</p>'),
(9, 'message_about_cookies_rus', '<p>This website uses cookies to help us give you the best experience when you visit our website. By continuing to use this website, you consent to our use of these cookies.</p>'),
(10, 'default_subject_for_mail_for_user_about_user_signed_up_for_event', ''),
(11, 'default_contents_for_mail_for_user_about_user_signed_up_for_event', ''),
(12, 'default_subject_for_mail_for_admin_about_user_signed_up_for_event', ''),
(13, 'default_contents_for_mail_for_admin_about_user_signed_up_for_event', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sub_layouts`
--

CREATE TABLE `sub_layouts` (
  `sub_layout_id` smallint(5) UNSIGNED NOT NULL,
  `file_path` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `sub_layouts`
--

INSERT INTO `sub_layouts` (`sub_layout_id`, `file_path`) VALUES
(1, 'one-column.phtml'),
(2, 'two-columns.phtml'),
(3, 'three-columns.phtml'),
(4, 'two-columns-sub-page.phtml');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sub_layouts_properties`
--

CREATE TABLE `sub_layouts_properties` (
  `properties_id` mediumint(8) UNSIGNED NOT NULL,
  `sub_layout_id` smallint(5) UNSIGNED NOT NULL,
  `language_id` smallint(5) UNSIGNED DEFAULT NULL,
  `order` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(64) COLLATE utf8_polish_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `sub_layouts_properties`
--

INSERT INTO `sub_layouts_properties` (`properties_id`, `sub_layout_id`, `language_id`, `order`, `name`) VALUES
(1, 1, 1, 2, 'One column'),
(2, 1, 2, 2, 'Jedna kolumna'),
(3, 1, 3, 2, 'Одна колонка'),
(4, 2, 1, 3, 'Two columns'),
(5, 2, 2, 3, 'Dwie kolumny'),
(6, 2, 3, 3, 'Две колонны'),
(7, 3, 1, 4, 'Three columns'),
(8, 3, 2, 4, 'Trzy kolumny'),
(9, 3, 3, 4, 'Три колонны'),
(10, 4, 1, 1, 'Two columns - sub page'),
(11, 4, 2, 1, 'Dwie kolumny - podstrona'),
(12, 4, 3, 1, 'Две колонны - подстраница');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `system_history`
--

CREATE TABLE `system_history` (
  `event_id` int(10) UNSIGNED NOT NULL,
  `event_date` datetime DEFAULT NULL,
  `message` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `type_id` tinyint(3) UNSIGNED DEFAULT NULL,
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `account_creation_date` datetime DEFAULT NULL,
  `account_activation_date` datetime DEFAULT NULL,
  `salt` char(8) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `activation_code` char(8) COLLATE utf8_polish_ci NOT NULL DEFAULT '' COMMENT 'Used for account activation.',
  `confirmation_code` char(8) COLLATE utf8_polish_ci NOT NULL DEFAULT '' COMMENT 'Used for other services.',
  `password` char(40) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `login` varchar(25) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`user_id`, `active`, `type_id`, `last_update_author_id`, `last_update_date`, `account_creation_date`, `account_activation_date`, `salt`, `activation_code`, `confirmation_code`, `password`, `login`, `email`) VALUES
(1, 1, 1, NULL, '2014-02-20 19:52:59', '2014-02-20 19:52:59', '2014-02-20 19:52:59', 'CcXhts3l', '', '', 'a7f73f128e4fa47e87d0e8cb2d8044fb0f34f4b1', 'admin', 'admin@proimagine.pl'),
(2, 1, 1, NULL, '2014-02-20 19:52:59', '2014-02-20 19:52:59', '2014-02-20 19:52:59', 'JYtMVcwO', '', '', 'f7cdce621c097afdbacaee44f1b8ddc380b3224a', 'fczyrnek', 'filip.czyrnek@proimagine.pl'),
(3, 1, 1, NULL, '2014-02-20 19:52:59', '2014-02-20 19:52:59', '2014-02-20 19:52:59', '3dHvLa21', '', '', 'f6016d049f98c7551695c116a748e501792b1b10', 'dwolosz', 'dawid.wolosz@proimagine.pl'),
(4, 1, 1, NULL, '2014-02-20 19:52:59', '2014-02-20 19:52:59', '2014-02-20 19:52:59', 'zHNm0AB4', '', '', 'fc06c78e91a75865c07f9a637bf185654fc1f3c1', 'wskorodecki', 'wskorodecki@gmail.com'),
(5, 1, 2, NULL, '2014-02-20 19:52:59', '2014-02-20 19:52:59', '2014-02-20 19:52:59', 'ee9bfLLH', '', '', 'e01bc3e98652db1533063550a093efc84195c510', 'user', 'user@dawo.pl'),
(6, 0, 2, NULL, '2014-02-20 19:52:59', '2014-02-20 19:52:59', NULL, 'NHhVrQyn', 'k72D8s6@', '', 'bd094b08731a23c7b2e4feb29eb34d4c2f28e1cd', 'inactive-user', 'inactive-user@dawo.pl'),
(7, 1, 1, 1, '2014-03-13 14:55:10', '2014-03-13 14:55:10', NULL, '7jqg49gV', '3aAPHpGd', '', '79cef14a09df55850bc2b21598014e6e04046302', 'malgosia', 'm.irach@vistula.edu.pl'),
(9, 0, 1, 1, '2014-03-15 02:51:11', '2014-03-15 02:50:01', NULL, 'X9mij4Kn', 'igZUsRh2', '', '4c49c1943ddb4702085f6b0faa60010e5d508d7f', 'kasia', 'k.chmielewska@vistula.edu.pl'),
(10, 1, 1, NULL, '2014-05-19 13:38:33', '2014-05-19 13:38:33', NULL, 'CjAskCMt', '', 'moe8+>N_', '509b40f3ee12abd85cb4c82efdfe22f6a4147f6c', 'nbanulski', 'norbertbwp@wp.pl'),
(11, 1, 1, NULL, '2014-05-19 13:41:43', '2014-05-19 13:41:43', NULL, 'X7NHXuMh', '', '', '3ca52bc7a3e6b1b3c5f387e09d59581b6770118c', 'rszewc', 'rafal.szewc84@gmail.com');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users_account_types`
--

CREATE TABLE `users_account_types` (
  `type_id` tinyint(3) UNSIGNED NOT NULL,
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `role` varchar(16) COLLATE utf8_polish_ci NOT NULL DEFAULT 'guest',
  `name` varchar(45) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `privileges` varchar(512) COLLATE utf8_polish_ci NOT NULL DEFAULT '' COMMENT 'Privileges separated by comma.'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users_account_types`
--

INSERT INTO `users_account_types` (`type_id`, `last_update_author_id`, `last_update_date`, `role`, `name`, `privileges`) VALUES
(1, NULL, '2014-02-20 19:52:59', 'admin', 'Administrator', 'all'),
(2, NULL, '2014-02-20 19:52:59', 'member', 'User', 'none'),
(3, NULL, '2014-02-20 19:52:59', 'moderator', 'Moderator', 'none');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users_details`
--

CREATE TABLE `users_details` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `gender` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '0 - Unspecified.\n1 - Male.\n2 - Female.',
  `last_update_author_id` int(10) UNSIGNED DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `name` varchar(45) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `surname` varchar(45) COLLATE utf8_polish_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users_details`
--

INSERT INTO `users_details` (`user_id`, `gender`, `last_update_author_id`, `last_update_date`, `name`, `surname`) VALUES
(1, 1, NULL, '2014-02-20 19:52:59', 'Unknown', 'Unknown'),
(2, 1, NULL, '2014-02-20 19:52:59', 'Filip', 'Czyrnek'),
(3, 1, NULL, '2014-02-20 19:52:59', 'Dawid', 'Wołosz'),
(4, 1, NULL, '2014-02-20 19:52:59', 'Wojciech', 'Skorodecki'),
(5, 1, NULL, '2014-02-20 19:52:59', 'Unknown', 'Unknown'),
(6, 1, NULL, '2014-02-20 19:52:59', 'Unknown', 'Unknown'),
(7, 2, 1, '2014-03-13 14:55:10', '', ''),
(9, 0, 1, '2014-03-15 02:51:11', '', ''),
(10, 0, 1, '2014-03-15 02:51:11', '', ''),
(11, 0, 1, '2014-03-15 02:51:11', '', '');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `attachments`
--
ALTER TABLE `attachments`
  ADD PRIMARY KEY (`attachment_id`),
  ADD KEY `fk_a_a` (`parent_attachment_id`),
  ADD KEY `fk_a_pb` (`page_block_id`),
  ADD KEY `fk_a_u` (`last_update_author_id`),
  ADD KEY `idx_a_last_update_date` (`last_update_date`),
  ADD KEY `idx_a_hash_for_file_name` (`hash_for_file_name`);

--
-- Indexes for table `blocks`
--
ALTER TABLE `blocks`
  ADD PRIMARY KEY (`block_id`),
  ADD KEY `idx_b_order` (`order`),
  ADD KEY `fk_b_languages` (`language_category_id`),
  ADD KEY `idx_b_last_update_date` (`last_update_date`),
  ADD KEY `fk_b_pbc` (`shared_content_id`),
  ADD KEY `fk_b_u` (`last_update_author_id`);

--
-- Indexes for table `blocks_properties`
--
ALTER TABLE `blocks_properties`
  ADD PRIMARY KEY (`properties_id`),
  ADD KEY `fk_bp_blocks` (`block_id`),
  ADD KEY `fk_bp_languages` (`language_id`);
ALTER TABLE `blocks_properties` ADD FULLTEXT KEY `ft_bp_name` (`name`);

--
-- Indexes for table `block_accordion`
--
ALTER TABLE `block_accordion`
  ADD PRIMARY KEY (`panel_id`),
  ADD KEY `fk_ba_pbc_content_id` (`content_id`),
  ADD KEY `fk_ba_u` (`last_update_author_id`),
  ADD KEY `idx_ba_last_update_date` (`last_update_date`);

--
-- Indexes for table `block_bachelor_studies`
--
ALTER TABLE `block_bachelor_studies`
  ADD PRIMARY KEY (`content_id`),
  ADD KEY `fk_block_bachelor_studies_pbc_content_id` (`content_id`),
  ADD KEY `fk_block_bachelor_studies_u` (`last_update_author_id`),
  ADD KEY `idx_block_bachelor_studies_last_update_date` (`last_update_date`);

--
-- Indexes for table `block_bachelor_studies_tabs`
--
ALTER TABLE `block_bachelor_studies_tabs`
  ADD PRIMARY KEY (`tab_id`),
  ADD KEY `fk_bbst_pbc_content_id` (`content_id`),
  ADD KEY `idx_bbst_last_update_date` (`last_update_date`),
  ADD KEY `fk_bbst_u` (`last_update_author_id`);

--
-- Indexes for table `block_banner_small`
--
ALTER TABLE `block_banner_small`
  ADD PRIMARY KEY (`banner_id`),
  ADD KEY `fk_bbs_pbc_content_id` (`content_id`),
  ADD KEY `fk_bbs_a` (`attachment_id`),
  ADD KEY `fk_bbs_u` (`last_update_author_id`),
  ADD KEY `idx_bbs_last_update_date` (`last_update_date`);

--
-- Indexes for table `block_big_banner`
--
ALTER TABLE `block_big_banner`
  ADD PRIMARY KEY (`banner_id`),
  ADD KEY `fk_bbb_pbc_content_id` (`content_id`),
  ADD KEY `fk_bbb_p_page_id` (`page_id`),
  ADD KEY `fk_bbb_a` (`attachment_id`),
  ADD KEY `idx_bbb_last_update_date` (`last_update_date`),
  ADD KEY `idx_bbb_external_url` (`external_url`),
  ADD KEY `fk_bbb_u` (`last_update_author_id`);

--
-- Indexes for table `block_employees_degrees`
--
ALTER TABLE `block_employees_degrees`
  ADD PRIMARY KEY (`degree_id`),
  ADD KEY `fk_bed_pbc` (`content_id`),
  ADD KEY `idx_bed_last_update_date` (`last_update_date`),
  ADD KEY `fk_bed_u` (`last_update_author_id`);

--
-- Indexes for table `block_employees_employees`
--
ALTER TABLE `block_employees_employees`
  ADD PRIMARY KEY (`employee_id`),
  ADD KEY `fk_bee_pbc` (`content_id`),
  ADD KEY `fk_bee_bed` (`degree_id`),
  ADD KEY `fk_bee_a` (`attachment_id`),
  ADD KEY `idx_bee_last_update_date` (`last_update_date`),
  ADD KEY `fk_bee_u` (`last_update_author_id`);

--
-- Indexes for table `block_event_list`
--
ALTER TABLE `block_event_list`
  ADD PRIMARY KEY (`event_id`),
  ADD KEY `fk_bel_a` (`attachment_id`),
  ADD KEY `fk_bel_pbc` (`content_id`),
  ADD KEY `fk_bel_u` (`last_update_author_id`),
  ADD KEY `idx_bel_last_update_date` (`last_update_date`),
  ADD KEY `idx_bel_creation_date` (`creation_date`),
  ADD KEY `idx_bel_approval_date` (`approval_date`),
  ADD KEY `idx_bel_event_date_start` (`event_date_start`),
  ADD KEY `idx_bel_event_date_end` (`event_date_end`);

--
-- Indexes for table `block_event_list_additional_data`
--
ALTER TABLE `block_event_list_additional_data`
  ADD PRIMARY KEY (`data_id`),
  ADD KEY `fk_belad_bel` (`event_id`),
  ADD KEY `fk_belad_pbc` (`content_id`),
  ADD KEY `fk_belad_u` (`last_update_author_id`),
  ADD KEY `idx_belad_last_update_date` (`last_update_date`);

--
-- Indexes for table `block_event_list_additional_data_types`
--
ALTER TABLE `block_event_list_additional_data_types`
  ADD PRIMARY KEY (`type_id`),
  ADD KEY `fk_beladt_pbc_content_id` (`content_id`),
  ADD KEY `fk_beladt_u` (`last_update_author_id`),
  ADD KEY `idx_beladt_last_update_date` (`last_update_date`);

--
-- Indexes for table `block_event_list_categories`
--
ALTER TABLE `block_event_list_categories`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `fk_belc_pbc_content_id` (`content_id`),
  ADD KEY `fk_belc_u` (`last_update_author_id`),
  ADD KEY `idx_belc_last_update_date` (`last_update_date`);

--
-- Indexes for table `block_event_list_event_images`
--
ALTER TABLE `block_event_list_event_images`
  ADD PRIMARY KEY (`image_id`),
  ADD KEY `fk_belei_pbc_content_id` (`content_id`),
  ADD KEY `fk_belei_a` (`attachment_id`),
  ADD KEY `fk_belei_u` (`last_update_author_id`),
  ADD KEY `idx_belei_last_update_date` (`last_update_date`);

--
-- Indexes for table `block_event_list_user_subscriptions`
--
ALTER TABLE `block_event_list_user_subscriptions`
  ADD PRIMARY KEY (`subscription_id`),
  ADD KEY `fk_belus_pbc_content_id` (`content_id`),
  ADD KEY `idx_belus_subscription_date` (`subscription_date`);

--
-- Indexes for table `block_images_pile_groups`
--
ALTER TABLE `block_images_pile_groups`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `fk_bipg_pbc_content_id` (`content_id`),
  ADD KEY `idx_bipg_order` (`order`),
  ADD KEY `idx_bipg_last_update_date` (`last_update_date`),
  ADD KEY `fk_bipg_u` (`last_update_author_id`);

--
-- Indexes for table `block_images_pile_items`
--
ALTER TABLE `block_images_pile_items`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `fk_bipi_pbc_content_id` (`content_id`),
  ADD KEY `fk_bipi_bipg_group_id` (`group_id`),
  ADD KEY `fk_bipi_p_page_id` (`page_id`),
  ADD KEY `idx_bipi_order` (`order`),
  ADD KEY `idx_bipi_last_update_date` (`last_update_date`),
  ADD KEY `idx_bipi_external_url` (`external_url`),
  ADD KEY `fk_bipi_a` (`attachment_id`),
  ADD KEY `fk_bipi_u` (`last_update_author_id`);

--
-- Indexes for table `block_image_gallery`
--
ALTER TABLE `block_image_gallery`
  ADD PRIMARY KEY (`image_id`),
  ADD KEY `fk_big_pbc` (`content_id`),
  ADD KEY `fk_big_a` (`attachment_id`),
  ADD KEY `idx_big_last_update_date` (`last_update_date`),
  ADD KEY `fk_big_u` (`last_update_author_id`);

--
-- Indexes for table `block_image_slider`
--
ALTER TABLE `block_image_slider`
  ADD PRIMARY KEY (`image_id`),
  ADD KEY `fk_bis_pbc` (`content_id`),
  ADD KEY `fk_bis_a` (`attachment_id`),
  ADD KEY `fk_bis_p` (`page_id`),
  ADD KEY `idx_bis_last_update_date` (`last_update_date`),
  ADD KEY `fk_bis_u` (`last_update_author_id`);

--
-- Indexes for table `block_main_menu_information`
--
ALTER TABLE `block_main_menu_information`
  ADD PRIMARY KEY (`info_id`),
  ADD KEY `fk_bmmi_pbc_content_id` (`content_id`),
  ADD KEY `fk_bmmi_u` (`last_update_author_id`),
  ADD KEY `idx_bmmi_last_update_date` (`last_update_date`);

--
-- Indexes for table `block_navigation_menu_categories`
--
ALTER TABLE `block_navigation_menu_categories`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `fk_bnmc_pbc_content_id` (`content_id`),
  ADD KEY `fk_bnmc_u` (`last_update_author_id`),
  ADD KEY `idx_bnmc_last_update_date` (`last_update_date`);

--
-- Indexes for table `block_navigation_menu_items`
--
ALTER TABLE `block_navigation_menu_items`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `fk_bnmi_pbc_content_id` (`content_id`),
  ADD KEY `fk_bnmi_bnmc_category_id` (`category_id`),
  ADD KEY `fk_bnmi_p_page_id` (`page_id`),
  ADD KEY `idx_bnmi_last_update_date` (`last_update_date`),
  ADD KEY `fk_bnmi_a` (`attachment_id`),
  ADD KEY `fk_bnmi_u` (`last_update_author_id`);

--
-- Indexes for table `block_news_list`
--
ALTER TABLE `block_news_list`
  ADD PRIMARY KEY (`news_id`),
  ADD KEY `fk_bnl_a` (`attachment_id`),
  ADD KEY `fk_bnl_pbc` (`content_id`),
  ADD KEY `idx_bnl_last_update_date` (`last_update_date`),
  ADD KEY `idx_bnl_news_date` (`news_date`),
  ADD KEY `fk_bnl_u` (`last_update_author_id`);

--
-- Indexes for table `block_news_list_categories`
--
ALTER TABLE `block_news_list_categories`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `fk_bnlc_pbc_content_id` (`content_id`),
  ADD KEY `fk_bnlc_u` (`last_update_author_id`),
  ADD KEY `idx_bnlc_last_update_date` (`last_update_date`);

--
-- Indexes for table `block_news_list_news_images`
--
ALTER TABLE `block_news_list_news_images`
  ADD PRIMARY KEY (`image_id`),
  ADD KEY `fk_bnlni_pbc_content_id` (`content_id`),
  ADD KEY `fk_bnlni_a` (`attachment_id`),
  ADD KEY `fk_bnlni_u` (`last_update_author_id`),
  ADD KEY `idx_bnlni_last_update_date` (`last_update_date`);

--
-- Indexes for table `block_offer_degrees`
--
ALTER TABLE `block_offer_degrees`
  ADD PRIMARY KEY (`degree_id`),
  ADD KEY `fk_bod_pbc_content_id` (`content_id`),
  ADD KEY `fk_bod_u` (`last_update_author_id`),
  ADD KEY `idx_bod_last_update_date` (`last_update_date`);

--
-- Indexes for table `block_offer_majors`
--
ALTER TABLE `block_offer_majors`
  ADD PRIMARY KEY (`major_id`),
  ADD KEY `fk_bom_pbc_content_id` (`content_id`),
  ADD KEY `fk_bom_u` (`last_update_author_id`),
  ADD KEY `idx_bom_last_update_date` (`last_update_date`);

--
-- Indexes for table `block_popup`
--
ALTER TABLE `block_popup`
  ADD PRIMARY KEY (`content_id`),
  ADD KEY `fk_bpopup_pbc_content_id` (`content_id`),
  ADD KEY `idx_bpopup_last_update_date` (`last_update_date`),
  ADD KEY `fk_bpopup_u` (`last_update_author_id`);

--
-- Indexes for table `block_postgraduate_studies`
--
ALTER TABLE `block_postgraduate_studies`
  ADD PRIMARY KEY (`content_id`),
  ADD KEY `fk_bps_pbc_content_id` (`content_id`),
  ADD KEY `idx_bps_last_update_date` (`last_update_date`),
  ADD KEY `fk_bps_u` (`last_update_author_id`);

--
-- Indexes for table `block_postgraduate_studies_tabs`
--
ALTER TABLE `block_postgraduate_studies_tabs`
  ADD PRIMARY KEY (`tab_id`),
  ADD KEY `idx_bpst_last_update_date` (`last_update_date`),
  ADD KEY `fk_bpst_pbc_content_id` (`content_id`),
  ADD KEY `fk_bpst_u` (`last_update_author_id`);

--
-- Indexes for table `block_tabs_control`
--
ALTER TABLE `block_tabs_control`
  ADD PRIMARY KEY (`tab_id`),
  ADD KEY `fk_btc_pbc_content_id` (`content_id`),
  ADD KEY `fk_btc_u` (`last_update_author_id`),
  ADD KEY `idx_btc_last_update_date` (`last_update_date`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`language_id`),
  ADD UNIQUE KEY `idx_l_iso_code` (`iso_code`),
  ADD UNIQUE KEY `idx_l_english_name` (`english_name`),
  ADD UNIQUE KEY `idx_l_native_name` (`native_name`),
  ADD KEY `idx_l_last_update_date` (`last_update_date`),
  ADD KEY `fk_l_u` (`last_update_author_id`);

--
-- Indexes for table `layouts`
--
ALTER TABLE `layouts`
  ADD PRIMARY KEY (`layout_id`);

--
-- Indexes for table `layouts_properties`
--
ALTER TABLE `layouts_properties`
  ADD PRIMARY KEY (`properties_id`),
  ADD KEY `fk_lp_layouts` (`layout_id`),
  ADD KEY `fk_lp_languages` (`language_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`),
  ADD UNIQUE KEY `idx_p_parent_id_language_id_clean_url` (`parent_id`,`language_id`,`clean_url`),
  ADD KEY `fk_p_languages` (`language_id`),
  ADD KEY `idx_p_views` (`views`),
  ADD KEY `idx_p_last_update_date` (`last_update_date`),
  ADD KEY `fk_p_u` (`last_update_author_id`),
  ADD KEY `fk_p_p` (`parent_id`);
ALTER TABLE `pages` ADD FULLTEXT KEY `ft_p_title` (`title`);
ALTER TABLE `pages` ADD FULLTEXT KEY `ft_p_meta_description` (`meta_description`);

--
-- Indexes for table `pages_blocks`
--
ALTER TABLE `pages_blocks`
  ADD PRIMARY KEY (`page_block_id`),
  ADD KEY `idx_pb_order` (`order`),
  ADD KEY `idx_pb_last_update_date` (`last_update_date`),
  ADD KEY `fk_pb_p` (`page_id`),
  ADD KEY `fk_pb_b` (`block_id`),
  ADD KEY `fk_pb_u` (`last_update_author_id`);

--
-- Indexes for table `pages_blocks_contents`
--
ALTER TABLE `pages_blocks_contents`
  ADD PRIMARY KEY (`content_id`),
  ADD KEY `idx_pbc_last_update_date` (`last_update_date`),
  ADD KEY `fk_pbc_u` (`last_update_author_id`);

--
-- Indexes for table `pages_blocks_contents_association`
--
ALTER TABLE `pages_blocks_contents_association`
  ADD PRIMARY KEY (`association_id`),
  ADD KEY `fk_pbca_pb` (`page_block_id`),
  ADD KEY `fk_pbca_pbc` (`content_id`);

--
-- Indexes for table `pages_language_equivalents`
--
ALTER TABLE `pages_language_equivalents`
  ADD PRIMARY KEY (`equivalent_id`),
  ADD UNIQUE KEY `idx_ple_p_l` (`page_id`,`language_id`),
  ADD KEY `fk_ple_languages` (`language_id`),
  ADD KEY `fk_ple_p2` (`associated_page_id`);

--
-- Indexes for table `search_index`
--
ALTER TABLE `search_index`
  ADD PRIMARY KEY (`result_id`),
  ADD UNIQUE KEY `uidx_si_page_id_content_id_block_element_id` (`page_id`,`content_id`,`block_element_id`),
  ADD KEY `fk_si_p_page_id` (`page_id`),
  ADD KEY `fk_si_languages` (`language_id`),
  ADD KEY `fk_si_pbc` (`content_id`),
  ADD KEY `idx_si_date` (`date`);
ALTER TABLE `search_index` ADD FULLTEXT KEY `ftidx_si_page_title` (`page_title`);
ALTER TABLE `search_index` ADD FULLTEXT KEY `ftidx_si_meta_description` (`meta_description`);
ALTER TABLE `search_index` ADD FULLTEXT KEY `ftidx_si_block_element_contents` (`block_element_contents`);
ALTER TABLE `search_index` ADD FULLTEXT KEY `ftidx_si_block_contents` (`block_contents`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`setting_id`),
  ADD UNIQUE KEY `uidx_s_name` (`name`);

--
-- Indexes for table `sub_layouts`
--
ALTER TABLE `sub_layouts`
  ADD PRIMARY KEY (`sub_layout_id`);

--
-- Indexes for table `sub_layouts_properties`
--
ALTER TABLE `sub_layouts_properties`
  ADD PRIMARY KEY (`properties_id`),
  ADD KEY `fk_slp_sub_layouts` (`sub_layout_id`),
  ADD KEY `fk_slp_languages` (`language_id`);

--
-- Indexes for table `system_history`
--
ALTER TABLE `system_history`
  ADD PRIMARY KEY (`event_id`),
  ADD KEY `idx_sh_event_date` (`event_date`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `fk_u_uat_type_id` (`type_id`),
  ADD KEY `idx_u_last_update_date` (`last_update_date`),
  ADD KEY `fk_u_u` (`last_update_author_id`);

--
-- Indexes for table `users_account_types`
--
ALTER TABLE `users_account_types`
  ADD PRIMARY KEY (`type_id`),
  ADD UNIQUE KEY `idx_uat_unique_name` (`name`),
  ADD KEY `idx_uat_last_update_date` (`last_update_date`),
  ADD KEY `fk_uat_u` (`last_update_author_id`);

--
-- Indexes for table `users_details`
--
ALTER TABLE `users_details`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `idx_ud_last_update_date` (`last_update_date`),
  ADD KEY `fk_ud_u_2` (`last_update_author_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `attachments`
--
ALTER TABLE `attachments`
  MODIFY `attachment_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=897;
--
-- AUTO_INCREMENT dla tabeli `blocks`
--
ALTER TABLE `blocks`
  MODIFY `block_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;
--
-- AUTO_INCREMENT dla tabeli `blocks_properties`
--
ALTER TABLE `blocks_properties`
  MODIFY `properties_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=298;
--
-- AUTO_INCREMENT dla tabeli `block_accordion`
--
ALTER TABLE `block_accordion`
  MODIFY `panel_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT dla tabeli `block_bachelor_studies_tabs`
--
ALTER TABLE `block_bachelor_studies_tabs`
  MODIFY `tab_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `block_banner_small`
--
ALTER TABLE `block_banner_small`
  MODIFY `banner_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `block_big_banner`
--
ALTER TABLE `block_big_banner`
  MODIFY `banner_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `block_employees_degrees`
--
ALTER TABLE `block_employees_degrees`
  MODIFY `degree_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `block_employees_employees`
--
ALTER TABLE `block_employees_employees`
  MODIFY `employee_id` mediumint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `block_event_list`
--
ALTER TABLE `block_event_list`
  MODIFY `event_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT dla tabeli `block_event_list_additional_data`
--
ALTER TABLE `block_event_list_additional_data`
  MODIFY `data_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT dla tabeli `block_event_list_additional_data_types`
--
ALTER TABLE `block_event_list_additional_data_types`
  MODIFY `type_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT dla tabeli `block_event_list_categories`
--
ALTER TABLE `block_event_list_categories`
  MODIFY `category_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT dla tabeli `block_event_list_event_images`
--
ALTER TABLE `block_event_list_event_images`
  MODIFY `image_id` mediumint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT dla tabeli `block_event_list_user_subscriptions`
--
ALTER TABLE `block_event_list_user_subscriptions`
  MODIFY `subscription_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `block_images_pile_groups`
--
ALTER TABLE `block_images_pile_groups`
  MODIFY `group_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `block_images_pile_items`
--
ALTER TABLE `block_images_pile_items`
  MODIFY `item_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `block_image_gallery`
--
ALTER TABLE `block_image_gallery`
  MODIFY `image_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `block_image_slider`
--
ALTER TABLE `block_image_slider`
  MODIFY `image_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT dla tabeli `block_main_menu_information`
--
ALTER TABLE `block_main_menu_information`
  MODIFY `info_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=490;
--
-- AUTO_INCREMENT dla tabeli `block_navigation_menu_categories`
--
ALTER TABLE `block_navigation_menu_categories`
  MODIFY `category_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;
--
-- AUTO_INCREMENT dla tabeli `block_navigation_menu_items`
--
ALTER TABLE `block_navigation_menu_items`
  MODIFY `item_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=573;
--
-- AUTO_INCREMENT dla tabeli `block_news_list`
--
ALTER TABLE `block_news_list`
  MODIFY `news_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT dla tabeli `block_news_list_categories`
--
ALTER TABLE `block_news_list_categories`
  MODIFY `category_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `block_news_list_news_images`
--
ALTER TABLE `block_news_list_news_images`
  MODIFY `image_id` mediumint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT dla tabeli `block_offer_degrees`
--
ALTER TABLE `block_offer_degrees`
  MODIFY `degree_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `block_offer_majors`
--
ALTER TABLE `block_offer_majors`
  MODIFY `major_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `block_postgraduate_studies_tabs`
--
ALTER TABLE `block_postgraduate_studies_tabs`
  MODIFY `tab_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `block_tabs_control`
--
ALTER TABLE `block_tabs_control`
  MODIFY `tab_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT dla tabeli `languages`
--
ALTER TABLE `languages`
  MODIFY `language_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT dla tabeli `layouts`
--
ALTER TABLE `layouts`
  MODIFY `layout_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `layouts_properties`
--
ALTER TABLE `layouts_properties`
  MODIFY `properties_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT dla tabeli `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=273;
--
-- AUTO_INCREMENT dla tabeli `pages_blocks`
--
ALTER TABLE `pages_blocks`
  MODIFY `page_block_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2997;
--
-- AUTO_INCREMENT dla tabeli `pages_blocks_contents`
--
ALTER TABLE `pages_blocks_contents`
  MODIFY `content_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=808;
--
-- AUTO_INCREMENT dla tabeli `pages_blocks_contents_association`
--
ALTER TABLE `pages_blocks_contents_association`
  MODIFY `association_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2994;
--
-- AUTO_INCREMENT dla tabeli `pages_language_equivalents`
--
ALTER TABLE `pages_language_equivalents`
  MODIFY `equivalent_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=783;
--
-- AUTO_INCREMENT dla tabeli `search_index`
--
ALTER TABLE `search_index`
  MODIFY `result_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT dla tabeli `settings`
--
ALTER TABLE `settings`
  MODIFY `setting_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT dla tabeli `sub_layouts`
--
ALTER TABLE `sub_layouts`
  MODIFY `sub_layout_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `sub_layouts_properties`
--
ALTER TABLE `sub_layouts_properties`
  MODIFY `properties_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT dla tabeli `system_history`
--
ALTER TABLE `system_history`
  MODIFY `event_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT dla tabeli `users_account_types`
--
ALTER TABLE `users_account_types`
  MODIFY `type_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

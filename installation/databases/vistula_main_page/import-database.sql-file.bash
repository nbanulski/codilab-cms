#!/bin/bash

bash ../../installation-scripts/ask-for-database-credentials.bash
mysql -u $DB_USER --password=$DB_PASSWORD --default-character-set=utf8 vistula_main_page < database.sql

#!/bin/bash

INFO_ABOUT_OS="$(uname -a)"
ON_WINDOWS=0

if [[ $INFO_ABOUT_OS == *MINGW* ]]
then
    ON_WINDOWS=1
fi



if [[ $ON_WINDOWS == 0 ]]; then
    CURRENT_USERNAME="$(id -u -n)"
    CURRENT_GROUP="$(id -g -n)"

    echo "| --- Setting correct user and group for entire directory structure."

    if [[ ! -n "$CURRENT_USERNAME" ]]; then
        echo -n "Enter your common (NOT root!) user name: "
    else
        echo -n "Enter your common (NOT root!) user name (default $CURRENT_USERNAME): "
    fi
    read COMMON_USERNAME

    if [[ ! -n "$CURRENT_USERNAME" ]]; then
        echo -n "Enter group name of this user (sometimes it can be the same as your username): "
    else
        echo -n "Enter group name of this user (sometimes it can be the same as your username, default $CURRENT_GROUP): "
    fi
    read USERNAME_GROUP

    if [[ ! -n "$COMMON_USERNAME" ]]; then
        COMMON_USERNAME=$CURRENT_USERNAME
    fi

    if [[ ! -n "$USERNAME_GROUP" ]]; then
        USERNAME_GROUP=$CURRENT_GROUP
    fi

    if [[ -n "$COMMON_USERNAME" && -n "$USERNAME_GROUP" ]]; then # If variable is not empty.
        if [[ $UID != 0 ]]; then
            SUDO_EXISTS=$(sudo -h | wc -l)
            if [ ${SUDO_EXISTS} -gt 10 ]
            then
                sudo chown -R $COMMON_USERNAME:$USERNAME_GROUP ../../proimagine-cms
            else
                echo -e "\e[01;31mNot enough permissions. Please start the script as root or sudo!\e[00m"
            fi
        else
            chown -R $COMMON_USERNAME:$USERNAME_GROUP ../../proimagine-cms
        fi
    else
        echo -e "\e[01;31mYou did not typed the name and / or group name. Setting correct user and group aborted.\e[00m"
    fi



    source installation-scripts/set-correct-file-permissions.bash
fi



echo "| --- Renaming configuration files from *.php.dist to *.php inside config/autoload directory."
if [ ! -f ../config/autoload/database.local.php ]
then
	mv ../config/autoload/database.local.php.dist ../config/autoload/database.local.php
fi

if [ ! -f ../config/autoload/local.php ]
then
	mv ../config/autoload/local.php.dist ../config/autoload/local.php
fi

if [ ! -f ../config/autoload/zenddevelopertools.local.php ]
then
	mv ../config/autoload/zenddevelopertools.local.php.dist ../config/autoload/zenddevelopertools.local.php
fi

echo "| --- Installing the database on the user's request."
echo "Now you can select database from list of available databases and import it."
echo "Of course you can do it later using bash scripts placed in installation/databases/ directory."
echo -n "Do you want to install database now? [y/N]: "
read USER_WANTS_TO_INSTALL_DB
if [[ $USER_WANTS_TO_INSTALL_DB = "y" || $USER_WANTS_TO_INSTALL_DB = "Y" ]]; then
	echo "### --- Databases available to install:"
	cd databases
	index=0;
	for i in $(ls -d *); do
		database_name=${i%%/};
		database_array[index]=$database_name;
		echo "[$index] ${database_name}";
		index=$[index+1];
	done
	max_index=$index
	cd ..

	echo -n "Enter the number of database which you want to import: "
	read db_index
	if [ -n "$db_index" ]
	then
		if [ "$db_index" -gt "-1" ] && [ "$db_index" -le "$max_index" ]; then
			DB_NAME=${database_array[db_index]}
		fi

		export DB_USER=""
		export DB_PASSWORD=""
		source installation-scripts/ask-for-database-credentials.bash

		echo "Installing $DB_NAME database. Please be patient. It may take about 2 minutes on slow HDDs."
		cd databases/$DB_NAME/
		echo -e "\nDropping and re-creating $DB_NAME database:"
		bash drop-and-create-database.bash
		bash import-everything.bash
		cd ../../
	else
		echo -e "\e[01;31mYou did not typed the name of the database. Aborting database installation.\e[00m"
	fi
fi



echo "### --- Done."

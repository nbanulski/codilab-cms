<?php
namespace Auth\Model;

use Zend\Db\TableGateway\TableGateway;

class UsersTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }

    public function getUser($userId)
    {
        $userId = (int)$userId;
        $resultSet = $this->tableGateway->select(array('user_id' => $userId));
        $row = $resultSet->current();
        if (!$row)
        {
            throw new \Exception("Could not find row $userId");
        }

        return $row;
    }
}
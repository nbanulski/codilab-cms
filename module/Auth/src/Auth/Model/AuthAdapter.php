<?php
namespace Auth\Model;

use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result as AuthResult;
use \Site\Controller\DatabaseManager;

class AuthAdapter implements AdapterInterface
{
    protected $dbManager = null;
    protected $tableName = null;
    protected $loginColumnName = null;
    protected $passwordColumnName = null;
    protected $userPasswordSaltEnabled = false;

    protected $passwordSalt = null;
    protected $login = null;
    protected $password = null;

    public function __construct(
        DatabaseManager $dbManager, $tableName, $loginColumnName, $passwordColumnName
    )
    {
        $this->dbManager = $dbManager;
        $this->tableName = $tableName;
        $this->loginColumnName = $loginColumnName;
        $this->passwordColumnName = $passwordColumnName;
    }

    public function setPasswordSalt($passwordSalt)
    {
        $this->passwordSalt = $passwordSalt;

        return $this;
    }

    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    public function enableUserPasswordSalt($enabled)
    {
        $this->userPasswordSaltEnabled = (bool)$enabled;

        return $this;
    }

    public function authenticate()
    {
        $dbManager = $this->dbManager;

        $userData = $this->getUser();
        if(!$userData)
        {
            return new AuthResult(AuthResult::FAILURE_IDENTITY_NOT_FOUND, null);
        }

        $stringToHash = $this->passwordSalt . $this->password;
        if ($this->userPasswordSaltEnabled && ($userData->salt != ''))
        {
            $stringToHash .= $userData->salt;
        }

        $hashedPassword = sha1($stringToHash);
        $prefixColumnsWithTable = false;
        $logged = false;

        $select = $dbManager->getSql()->select($this->tableName);
        $select
            ->columns(array('v' => new \Zend\Db\Sql\Expression('?', '1')), $prefixColumnsWithTable)
            ->where(
                array(
                    $this->loginColumnName => $this->login,
                    $this->passwordColumnName => $hashedPassword,
                    'active' => 1
                )
            )
            ->limit(1);
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();

            $logged = $row ? ($row->v == 1) : false;
        }

        if ($logged)
        {
            unset($userData->salt);

            return new AuthResult(AuthResult::SUCCESS, $userData);
        }
        else
        {
            return new AuthResult(AuthResult::FAILURE_CREDENTIAL_INVALID, null);
        }
    }

    private function getUser()
    {
        $dbManager = $this->dbManager;

        $prefixColumnsWithTable = false;

        $select = $dbManager->getSql()->select($this->tableName);
        $select
            ->columns(array('user_id', 'type_id', 'salt', 'login', 'email'), $prefixColumnsWithTable)
            ->where(
                array(
                    'active' => 1,
                    $this->loginColumnName => $this->login
                )
            )
            ->limit(1);
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }
}
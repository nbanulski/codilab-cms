<?php
namespace Auth\Controller;

use Site\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\Result;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Auth\Form\AuthForm;
use Auth\Model\Auth;

class IndexController extends BaseController
{
    protected $storage = null;

    public function indexAction()
    {
        $messages = null;

        $form = new AuthForm();
        $form->get('submit')->setValue('Login');

        $request = $this->getRequest();
        if ($request->isPost())
        {
            $authFormFilters = new Auth();
            $form->setInputFilter($authFormFilters->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid())
            {
                $messages = 'Wrong login or password.';

                $data = $form->getData();
                $serviceLocator = $this->serviceLocator;
                $dbAdapter = $serviceLocator->get('Zend\Db\Adapter\Adapter');
                $config = $serviceLocator->get('Config');

                $cmsConfig = $config['cms'];
                $passwordSalt = $cmsConfig['password_salt'];

                $login = trim($data['login']);
                $password = trim($data['password']);

                if (($login != '') && ($password != ''))
                {
                    /*$authAdapter = new AuthAdapter(
                        $dbAdapter, 'users', 'login', 'password', "SHA1(CONCAT('$passwordSalt', ?, salt)) AND active=1"
                    );
                    $authAdapter
                        ->setIdentity($login)
                        ->setCredential($password)
                    ;*/

                    $dbManager = $this->serviceLocator->get('DatabaseManager');
                    $authAdapter = new \Auth\Model\AuthAdapter(
                        $dbManager, 'users', 'login', 'password'
                    );
                    $authAdapter
                        ->setPasswordSalt($passwordSalt)
                        ->setLogin($login)
                        ->setPassword($password)
                        ->enableUserPasswordSalt(true)
                    ;

                    $auth = $serviceLocator->get('AuthService');
                    $result = $auth->authenticate($authAdapter);

                    switch ($result->getCode())
                    {
                        /* case Result::FAILURE_IDENTITY_NOT_FOUND:
                          case Result::FAILURE_CREDENTIAL_INVALID:
                          //$messages .= 'Wrong username or password.'. "\n";
                          break; */
                        case Result::SUCCESS:
                            $omitColumns = array('salt', 'password');
                            //$identity = $authAdapter->getResultRowObject(null, $omitColumns);
                            $identity = $result->getIdentity();

                            $usersAccountsTable = new \Zend\Db\TableGateway\TableGateway('users_account_types', $dbAdapter);
                            $rowSet = $usersAccountsTable->select(array('type_id' => $identity->type_id));
                            $accountInfo = $rowSet->current();

                            $languageManager = $this->getLanguageManager();
                            $defaultLanguage = $languageManager->getDefaultLanguage();

                            unset($identity->type_id);
                            $identity->account = $accountInfo;
                            $identity->session = new \stdClass();
                            $identity->session->designMode = false;
                            $identity->session->userLanguage = new \Site\Custom\FlexibleContainer($defaultLanguage);

                            $rememberMe = true;
                            $this->saveSessionDataAndSettings(
                                $identity, $rememberMe, $cmsConfig['remember_me_time_in_seconds']
                            );

                            $session = new \Zend\Session\Container('referer');
                            if ($session->uri != '')
                            {
                                $info = pathinfo($session->uri);
                                if (
                                    ($info['basename'] == 'index') || ($info['filename'] == 'index') ||
                                    ($info['basename'] == 'auth') || ($info['filename'] == 'auth')
                                )
                                {
                                    return $this->redirect()->toRoute('home');
                                }

                                return $this->redirect()->toUrl($session->uri);
                            }

                            return $this->redirect()->toRoute('home');
                            break;
                        /* default:
                          // Unknown error.
                          break; */
                    }
                }
            }
            else
            {
                $messages = 'Please enter the login and password.';
            }
        }

        $layout = $this->layout();
        $layout->setTemplate('layout/login');
        $layout->page = new \Site\Custom\FlexibleContainer();
        $layout->page->title = $this->translate('Sign in');
        $layout->setVariables(
            array(
                'siteName' => $this->cmsConfig->site_name,
                'messages' => $messages,
                'form' => $form
            )
        );

        return new ViewModel();
    }

    public function fastLoginAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            if ($request->isPost())
            {
                $jsonResponse->data = new \stdClass();

                $serviceLocator = $this->serviceLocator;
                $dbAdapter = $serviceLocator->get('Zend\Db\Adapter\Adapter');
                $config = $serviceLocator->get('Config');
                $formData = $request->getPost('formData');
                $isDesignModeEnabled = $request->getPost('isDesignModeEnabled', false) ? true : false;

                $cmsConfig = $config['cms'];
                $passwordSalt = $cmsConfig['password_salt'];

                $login = trim($formData['login']);
                $password = trim($formData['password']);

                $logged = false;
                if (($login != '') && ($password != ''))
                {
                    $authAdapter = new AuthAdapter(
                        $dbAdapter, 'users', 'login', 'password', "SHA1(CONCAT('$passwordSalt', ?, salt)) AND active=1"
                    );
                    $authAdapter
                        ->setIdentity($login)
                        ->setCredential($password)
                    ;

                    $auth = $serviceLocator->get('AuthService');
                    $result = $auth->authenticate($authAdapter);
                    if ($result->getCode() == Result::SUCCESS)
                    {
                        $omitColumns = array('salt', 'password');
                        $identity = $authAdapter->getResultRowObject(null, $omitColumns);

                        $usersAccountsTable = new \Zend\Db\TableGateway\TableGateway('users_account_types', $dbAdapter);
                        $rowSet = $usersAccountsTable->select(array('type_id' => $identity->type_id));
                        $accountInfo = $rowSet->current();

                        $languageManager = $this->getLanguageManager();
                        $defaultLanguage = $languageManager->getDefaultLanguage();

                        unset($identity->type_id);
                        $identity->account = $accountInfo;
                        $identity->session = new \stdClass();
                        $identity->session->designMode = $isDesignModeEnabled;
                        $identity->session->userLanguage = new \Site\Custom\FlexibleContainer($defaultLanguage);

                        $rememberMe = true;
                        $this->saveSessionDataAndSettings(
                            $identity, $rememberMe, $cmsConfig['remember_me_time_in_seconds']
                        );

                        $jsonResponse->data->userInfo = array(
                            'login' => $formData['login']
                        );

                        $logged = true;
                    }
                }

                if (!$logged)
                {
                    $jsonResponse->data->message = 'Wrong login or password.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function logoutAction()
    {
        $this->getSessionStorage()->forgetMe();
        $this->serviceLocator->get('AuthService')->clearIdentity();

        return $this->redirect()->toRoute('auth/default', array('controller' => 'index', 'action' => 'index'));
    }

    protected function getSessionStorage()
    {
        if (!$this->storage)
        {
            $this->storage = $this->serviceLocator->get('Auth\Model\AuthStorage');
        }

        return $this->storage;
    }

    protected function saveSessionDataAndSettings($identity, $rememberMe = true, $rememberMeTimeInSeconds = 3600)
    {
        $sessionStorage = $this->getSessionStorage();
        if ($rememberMe && ($rememberMeTimeInSeconds > 0))
        {
            $sessionStorage->setRememberMe(1, $rememberMeTimeInSeconds);
        }
        $auth = $this->serviceLocator->get('AuthService');
        $auth->setStorage($sessionStorage);

        $storage = $auth->getStorage();
        $storage->write($identity);
    }
}
<?php
namespace Site\View\Helper;

use Zend\View\Helper\AbstractHtmlElement;
use Zend\View\Renderer\PhpRenderer;

class DropdownMultiselect extends AbstractHtmlElement
{
    protected $phpRenderer = null;
    protected $escapeForHtml = null;

    public function __invoke($attributes, $values, $differentTexts = false, $selectedValue = null, $buttonText = false)
    {
        if ($selectedValue !== null)
        {
            $selectedValue = (string)$selectedValue;
        }

        if ($attributes && array_key_exists('class', $attributes) && ($attributes['class'] != ''))
        {
            $attributes['class'] = 'multiselect ' . $attributes['class'];
        }

        unset($attributes['multiple']);

        $parsedValues = $this->escapeValuesArrayForHtml($values);
        $parsedValues = !$differentTexts ? $this->makeTheSameValuesAsText($parsedValues) : $parsedValues;

        $dropDownView = new \Zend\View\Model\ViewModel();
        $dropDownView->setTemplate('partial/dropdown-multiselect.phtml');
        $dropDownView->setVariables(
            array(
                'attributes' => $this->htmlAttribs($attributes),
                'values' => $parsedValues,
                'differentTexts' => $differentTexts,
                'selectedValue' => $selectedValue,
                'buttonText' => $buttonText
            )
        );

        return $this->phpRenderer->render($dropDownView);
    }

    public function setPhpRenderer(PhpRenderer $phpRenderer)
    {
        $this->phpRenderer = $phpRenderer;
    }

    protected function makeTheSameValuesAsText($values)
    {
        $newValuesArray = array();

        if ($values && (is_array($values) || ($values instanceof \Traversable)))
        {
            foreach ($values as $value)
            {
                $convertedValue = (string)$value;

                $newValuesArray[$convertedValue] = $convertedValue;
            }
        }

        return $newValuesArray;
    }

    private function escapeValuesArrayForHtml($values)
    {
        if ($this->escapeForHtml == null)
        {
            $this->escapeForHtml = new \Site\View\Helper\EscapeForHtml();
        }

        $escapeForHtml = $this->escapeForHtml;

        $parsedValues = array();
        if ($values)
        {
            foreach ($values as $value => $text)
            {
                $escapedValue = $escapeForHtml($value);
                $escapedText = $escapeForHtml($text);

                $parsedValues[$escapedValue] = $escapedText;
            }
        }

        return $parsedValues;
    }
}
<?php
namespace Site\View\Helper;

use Zend\View\Helper\AbstractHtmlElement;

class Textarea extends AbstractHtmlElement
{
    public function __invoke($id = false, $name = false, $text = '', $attributes = array())
    {
        $id = (string)$id;
        $name = (string)$name;
        $text = (string)$text;

        $attrId = '';
        $attrName = '';
        $additionalAttributes = '';

        if ($id != '')
        {
            $attrId = ' id="' . $id . '"';
        }

        if ($name !== '')
        {
            $attrName = ' name="' . $name . '"';
        }

        if ($attributes)
        {
            $additionalAttributes = $this->htmlAttribs($attributes);
        }

        return
            '<textarea' .
            $attrId .
            $attrName .
            $additionalAttributes . '>' .
            $text .
            '</textarea>';
    }
}
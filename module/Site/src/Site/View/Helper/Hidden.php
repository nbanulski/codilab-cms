<?php
namespace Site\View\Helper;

class Hidden extends Input
{

    public function __construct()
    {
        parent::__construct('hidden');
    }
}
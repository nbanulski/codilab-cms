<?php
namespace Site\View\Helper;

class Checkbox extends Input
{
    public function __construct()
    {
        parent::__construct('checkbox');
    }
}

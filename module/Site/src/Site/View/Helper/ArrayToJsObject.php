<?php
namespace Site\View\Helper;

use Zend\View\Helper\AbstractHtmlElement;

class ArrayToJsObject extends AbstractHtmlElement
{
    public function __invoke($array)
    {
        return $this->arrayToJsObject($array);
    }

    public function arrayToJsObject($array)
    {
        $jsString = '';

        if ($array && (is_array($array) || ($array instanceof \Traversable)))
        {
            foreach ($array as $key => $value)
            {
                if ($jsString != '')
                {
                    $jsString .= ',';
                }

                $jsString .= "'" . $key . "':";

                if ($value && (is_array($value) || ($value instanceof \Traversable)))
                {
                    $jsString .= $this->arrayToJsObject($value);
                }
                else
                {
                    if (is_string($value))
                    {
                        $value = "'" . $value . "'";
                    }

                    $jsString .= (string)$value;
                }
            }
        }

        return '{' . $jsString . '}';
    }
} 
<?php
namespace Site\View\Helper;

use Zend\View\Helper\AbstractHtmlElement;

class Input extends AbstractHtmlElement
{
    protected $type = 'text';

    public function __construct($type = 'text')
    {
        $this->type = $type;
    }

    public function __invoke($id, $name, $value = 1, $checked = false, $attributes = array())
    {
        $id = (string)$id;
        $name = (string)$name;
        $value = (string)$value;

        $attrId = '';
        $attrName = '';
        $attrValue = '';
        $attrChecked = '';
        $additionalAttributes = '';

        if ($id != '')
        {
            $attrId = ' id="' . $id . '"';
        }

        if ($name !== '')
        {
            $attrName = ' name="' . $name . '"';
        }

        if ($value !== '')
        {
            $attrValue = ' value="' . $value . '"';
        }

        if ($checked)
        {
            $attrChecked = ' checked';
        }

        if ($attributes)
        {
            $additionalAttributes = $this->htmlAttribs($attributes);
        }

        return '<input type="' . $this->type . '"' .
                $attrId .
                $attrName .
                $attrValue .
                $attrChecked .
                $additionalAttributes . ' />';
    }
}

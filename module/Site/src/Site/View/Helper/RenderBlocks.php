<?php
namespace Site\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Http\Request;
use Site\Block\AbstractBlock;

class RenderBlocks extends AbstractHelper
{
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function __invoke($slotNumber)
    {
        return $this->renderBlocksFromSlotNumber($slotNumber);
    }

    public function setRequest($request)
    {
        $this->request = $request;
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function renderBlocksFromSlotNumber($slotNumber)
    {
        $blocksCode = '';
        $view = $this->view;

        if ($view && isset($view->blocks[$slotNumber]) && $view->blocks[$slotNumber])
        {
            $request = $this->getRequest();
            $returnInsteadOfOutputting = true;

            foreach ($view->blocks[$slotNumber] as $block)
            {
                if ($block instanceof AbstractBlock)
                {
                    $blocksCode .= $block->render($request, $returnInsteadOfOutputting);
                }
            }
        }

        return $blocksCode;
    }
}
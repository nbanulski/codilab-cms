<?php
namespace Site\Component;

use \Site\Custom\FlexibleContainer;

class BasicSessionData extends FlexibleContainer
{
    public function __construct($array = null)
    {
        $defaultData = array(
            'user_id' => null,
            'active' => null,
            'last_update_author_id' => null,
            'last_update_date' => null,
            'account_creation_date' => null,
            'account_activation_date' => null,
            'activation_code' => null,
            'confirmation_code' => null,
            'login' => null,
            'email' => null,
            'account' => array(
                'type_id' => null,
                'last_update_author_id' => null,
                'last_update_date' => null,
                'role' => 'guest',
                'name' => 'Guest',
                'privileges' => null
            ),
            'session' => array(
                'designMode' => false
            )
        );
        parent::__construct($defaultData);

        // Process session data from Zend's Identity controller plugin.
        // In this place we receive stdClass object, so it has to be cast to array because \stdClass is not being
        // recognized as instance of \Travesable:
        parent::__construct((array)$array);
    }
}
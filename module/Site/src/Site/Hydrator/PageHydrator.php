<?php
namespace Site\Hydrator;

use Zend\Stdlib\Hydrator\HydratorInterface;
use Site\Controller\PageManager;

class PageHydrator implements HydratorInterface
{
    protected $pageManager = null;

    public function __construct(
        PageManager $pageManager
    )
    {
        $this->pageManager = $pageManager;
    }

    public function extract($object) {}

    public function hydrate(array $data, $object)
    {
        foreach ($data as $property => $value)
        {
            $object[$property] = $value;
        }

        $pageId = (int)$object['page_id'];
        $object['full_url'] = $this->pageManager->getFullPageCleanUrlByPageId($pageId);

        return $object;
    }
} 
<?php
namespace Site\Hydrator;

use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\I18n\View\Helper\Translate;
use Site\Controller\BlockManager;
use Site\Custom\FlexibleContainer;

class EventHydrator implements HydratorInterface
{
    protected $blockManager = null;
    protected $translateHelper = null;
    protected $userLanguage = null;

    public function __construct(
        BlockManager $blockManager, Translate $translateHelper, FlexibleContainer $userLanguage
    )
    {
        $this->blockManager = $blockManager;
        $this->translateHelper = $translateHelper;
        $this->userLanguage = $userLanguage;
    }

    public function extract($object) {}

    public function hydrate(array $data, $object)
    {
        foreach ($data as $property => $value)
        {
            $object[$property] = $value;
        }

        $translate = $this->translateHelper;
        $currentUnixTimestamp = time();
        $eventStartDateUnixTimestamp = strtotime($object['event_date_start']);
        $eventEndDateUnixTimestamp = strtotime($object['event_date_end']);

        if ($eventEndDateUnixTimestamp < $currentUnixTimestamp) // If event date passed.
        {
            $object['startAfter'] = $translate('Event archive', 'default', $this->userLanguage->zend2_locale);
        }
        else if ($eventStartDateUnixTimestamp > $currentUnixTimestamp)
        {
            $dateDiff = $currentUnixTimestamp - $eventStartDateUnixTimestamp;
            $daysLeft = abs(floor($dateDiff / 86400)); // 60 * 60 * 24.

            if ($daysLeft == 0)
            {
                $daysLeftStr = $translate('Today');
            }
            else if ($daysLeft == 1)
            {
                $daysLeftStr = $translate('Tomorrow');
            }
            else
            {
                $daysLeftStr = $daysLeft . ' ' . $translate('days');
            }

            $object['startAfter'] = $daysLeftStr;
        }
        else
        {
            $object['startAfter'] = $translate('Under way', 'default', $this->userLanguage->zend2_locale);
        }

        return $object;
    }
} 
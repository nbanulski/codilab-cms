<?php
namespace Site\Controller;

class BlockNewsAddController extends BaseController
{
    public function addNewsAsGuestAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $customStatus = 'INVALID_DATA';

                if ($request->isPost())
                {
                    if ($request->getPost('dataPosted'))
                    {
                        $formData = new \Site\Custom\FlexibleContainer($request->getPost('formData'));
                        $formData->newsId = null; // For security. We allow only adding.
                        $pageBlockId = (int)$formData->pageBlockId;

                        $blockManager = $this->getBlockManager();
                        $newsListBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);

                        if ($newsListBlock instanceof \Site\Block\NewsList\NewsList)
                        {
                            $result = $newsListBlock->addOrEditNews($formData);

                            if ($result)
                            {
                                $customStatus = 'NEWS_ADDED';
                            }
                            else
                            {
                                $customStatus = 'NEWS_ADDING_ERROR';
                            }
                        }
                    }
                }

                $jsonResponse->meta->customStatus = $customStatus;
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }
}
<?php
namespace Site\Controller;

use Zend\Mvc\Controller\AbstractController;
use Zend\Http\Request;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Sql\Sql;
use Zend\Mvc\MvcEvent;
use FileSystem\Component\FileSystem;
use Graphics\Component\ImageController;
use Site\Custom\FlexibleContainer;

class AttachmentManager extends AbstractController
{
    const FI_FILE_EXTENSION = 'fileExtension';
    const FI_FILE_SIZE = 'fileSize';
    const FI_PAGE_BLOCK_ID = 'pageBlockId';
    const FI_CREATION_DATE = 'creationDate';
    const FI_UNIQUE_HASH_FOR_FILE_NAME = 'uniqueHashForFileName';
    const FI_MIME_TYPE = 'mimeType';
    const FI_MODULE_NAME = 'moduleName';
    const FI_BLOCK_NAME = 'blockName';
    const FI_ORIGINAL_FILE_NAME = 'originalFileName';
    const FI_DESTINATION_FILE_PATH = 'destinationFilePath';

    protected $attachmentsDirectory = 'data/attachments/';
    protected $cmsConfig = null;
    protected $serviceLocator = null;
    protected $dbAdapter = null;
    protected $sql = null;
    protected $request = null;
    protected $language = null;
    protected $userLanguage = null;
    protected $identity = null;
    protected $databaseManager = null;
    protected $blockManager = null;
    protected $fileSystem = null;
    protected $imageController = null;

    public function __construct(
        $cmsConfig,
        ServiceLocatorInterface $serviceLocator,
        Sql $sql,
        Request $request,
        \stdClass $language,
        FlexibleContainer $userLanguage,
        $identity,
        FileSystem $fileSystem
    ) {
        $this->cmsConfig = $cmsConfig;
        $this->serviceLocator = $serviceLocator;
        $this->sql = $sql;
        $this->request = $request;
        $this->dbAdapter = $sql->getAdapter();
        $this->language = $language;
        $this->userLanguage = $userLanguage;
        $this->identity = $identity;
        $this->fileSystem = $fileSystem;
        $this->imageController = new ImageController($fileSystem);
    }

    public function onDispatch(MvcEvent $e)
    {
    }

    public function getDbAdapter()
    {
        if (!$this->dbAdapter)
        {
            $serviceLocator = $this->getServiceLocator();
            $this->dbAdapter = $serviceLocator->get('Zend\Db\Adapter\Adapter');
        }

        return $this->dbAdapter;
    }

    public function getDatabaseManager()
    {
        if ($this->databaseManager == null)
        {
            $this->databaseManager = $this->serviceLocator->get('DatabaseManager');
        }

        return $this->databaseManager;
    }

    public function getBlockManager()
    {
        if ($this->blockManager == null)
        {
            $sql = $this->getDatabaseManager()->getSql();
            $this->blockManager = new BlockManager($this->cmsConfig, $this->serviceLocator, $sql, $this->request, $this->language, $this->identity);
        }

        return $this->blockManager;
    }

    public function setBlockManager(BlockManager $blockManager)
    {
        $this->blockManager = $blockManager;

        return $this;
    }

    public function getFileSystem()
    {
        return $this->fileSystem;
    }

    public function getAttachmentsDirectory()
    {
        return $this->attachmentsDirectory;
    }

    public function setAttachmentsDirectory($attachmentsDirectory)
    {
        $this->attachmentsDirectory = trim((string)$attachmentsDirectory);
    }

    public function generateUniqueHashForFileName($optionalSalt = '')
    {
        $uniqueHash = $this->generateHashUsingOptionalSalt($optionalSalt);
        while ($this->isHashForFileNameAlreadyInUse($uniqueHash))
        {
            $uniqueHash = $this->generateHashUsingOptionalSalt($optionalSalt);
        }

        return $uniqueHash;
    }

    public function getAttachmentById($attachmentId)
    {
        $attachmentId = (int)$attachmentId;

        if ($attachmentId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $select = $this->sql->select();
        $select->from('attachments')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('attachment_id' => $attachmentId));
        $resultSet = $this->getDatabaseManager()->getHydratedResultSet($select);

        return $resultSet->current();
    }

    public function getAttachmentByUniqueHashForFileName($uniqueHashForFileName)
    {
        $uniqueHashForFileName = trim((string)$uniqueHashForFileName);

        if ($uniqueHashForFileName == '')
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $select = $this->sql->select();
        $select
            ->from('attachments')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('hash_for_file_name' => $uniqueHashForFileName));
        $resultSet = $this->getDatabaseManager()->getHydratedResultSet($select);

        return $resultSet->current();
    }

    public function deleteAttachmentsByUniqueHashForFileName($attachments)
    {
        if (!$attachments)
        {
            return true; // All requested attachments were deleted.
        }

        $deleteSuccessful = true;

        $dbManager = $this->getDatabaseManager();
        $dbManager->beginTransaction();

        foreach ($attachments as $uniqueHashForFileName)
        {
            $delete = $this->sql->delete('attachments');
            $delete->where(
                array(
                    'hash_for_file_name' => trim((string)$uniqueHashForFileName)
                )
            );
            $result = $dbManager->executePreparedStatement($delete);
            if (!$result)
            {
                $deleteSuccessful = false;
                break;
            }
        }

        if ($deleteSuccessful)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $deleteSuccessful;
    }

    public function updateAttachment($attachment)
    {
        if (!$attachment)
        {
            return false;
        }

        $attachmentId = (int)$attachment->attachment_id;
        $parentAttachmentId = ($attachment->parent_attachment_id > 0) ? (int)$attachment->parent_attachment_id : null;
        $isPublished = $attachment->is_published ? 1 : 0;
        $fileSize = (int)$attachment->file_size;
        $pageBlockId = ($attachment->page_block_id > 0) ? (int)$attachment->page_block_id : null;
        $lastUpdateAuthorId = ($attachment->last_update_author_id > 0) ? (int)$attachment->last_update_author_id : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();

        $update = $this->sql->update('attachments');
        $update->set(
            array(
                'parent_attachment_id' => $parentAttachmentId,
                'is_published' => $isPublished,
                'file_size' => $fileSize,
                'page_block_id' => $pageBlockId,
                'last_update_author_id' => $lastUpdateAuthorId,
                'last_update_date' => $lastUpdateDate,
                'creation_date' => $attachment->creation_date,
                'hash_for_file_name' => $attachment->hash_for_file_name,
                'extension' => $attachment->extension,
                'mime_type' => $attachment->mime_type,
                'module_name' => $attachment->module_name,
                'block_name' => $attachment->block_name,
                'image_crop_rect_coords' => $attachment->image_crop_rect_coords,
                'original_file_name' => $attachment->original_file_name,
                'file_path' => $attachment->file_path
            )
        )
            ->where(array('attachment_id' => $attachmentId));
        $result = $dbManager->executePreparedStatement($update);
        if (!$result)
        {
            return false;
        }

        $attachmentAfterUpdate = $this->getAttachmentById($attachmentId);
        if ($attachmentAfterUpdate &&
            ($attachmentAfterUpdate->parent_attachment_id == $parentAttachmentId) &&
            ($attachmentAfterUpdate->is_published == $isPublished) &&
            ($attachmentAfterUpdate->file_size == $fileSize) &&
            ($attachmentAfterUpdate->page_block_id == $pageBlockId) &&
            ($attachmentAfterUpdate->last_update_author_id == $lastUpdateAuthorId) &&
            //($attachmentAfterUpdate->last_update_date == $attachment->last_update_date) &&
            ($attachmentAfterUpdate->creation_date == $attachment->creation_date) &&
            ($attachmentAfterUpdate->hash_for_file_name == $attachment->hash_for_file_name) &&
            ($attachmentAfterUpdate->extension == $attachment->extension) &&
            ($attachmentAfterUpdate->mime_type == $attachment->mime_type) &&
            ($attachmentAfterUpdate->module_name == $attachment->module_name) &&
            ($attachmentAfterUpdate->block_name == $attachment->block_name) &&
            ($attachmentAfterUpdate->image_crop_rect_coords == $attachment->image_crop_rect_coords) &&
            ($attachmentAfterUpdate->original_file_name == $attachment->original_file_name) &&
            ($attachmentAfterUpdate->file_path == $attachment->file_path)
        )
        {
            return true;
        }

        return false;
    }

    public function createAttachmentCopyAndReturnId($attachmentId)
    {
        $attachment = $this->getAttachmentById($attachmentId);
        if (!$attachment)
        {
            return false;
        }

        $error = false;

        $fileInfo = $this->makeFileInfoFromAttachment($attachment);
        $fileInfo['parentAttachmentId'] = $attachment->attachment_id;
        $fileInfo['uniqueHashForFileName'] = $this->generateUniqueHashForFileName();
        $fileInfo['destinationFilePath'] = $this->getFilePathFromFileInfo($fileInfo);

        $dbManager = $this->getDatabaseManager();
        $dbManager->beginTransaction();

        $newAttachmentId = $this->insertRecordToAttachmentsTableUsingFileInfoAndReturnId($fileInfo);
        if ($newAttachmentId > 0)
        {
            $sourceFilePath = $attachment->file_path;

            $fileSystem = $this->getFileSystem();

            try
            {
                $fileSystem->copy($sourceFilePath, $fileInfo['destinationFilePath']);
            }
            catch (\Exception $e)
            {
                $error = true;
                $newAttachmentId = false;
            }
        }

        if (!$error)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $newAttachmentId;
    }

    public function cropAttachmentUsingCoordinates($uniqueHashForFileName, $coordinates)
    {
        if ($uniqueHashForFileName == '')
        {
            return false;
        }

        $attachment = $this->getAttachmentByUniqueHashForFileName($uniqueHashForFileName);
        if (!$attachment)
        {
            return false;
        }

        $attachmentId = $attachment->attachment_id;
        $parentAttachment = $attachment;

        if ($attachment->parent_attachment_id > 0)
        {
            $parentAttachment = $this->getAttachmentById($attachment->parent_attachment_id);
        }
        else
        {
            $newAttachmentId = $this->createAttachmentCopyAndReturnId($attachmentId);
            if ($newAttachmentId > 0)
            {
                $attachment = $this->getAttachmentById($newAttachmentId);
            }
        }

        $sourceFilePath = $parentAttachment->file_path;
        $destinationFilePath = $attachment->file_path;

        $imageDimensions = $this->imageController->getImageDimensions($sourceFilePath);
        if (!$imageDimensions || !$imageDimensions[0] || !$imageDimensions[1])
        {
            return false;
        }

        $originalWidth = $imageDimensions[0];
        $originalHeight = $imageDimensions[1];

        $cropMaskCoordsArray = $this->imageController->convertRectCoordsStringToArray($coordinates);

        if (!$cropMaskCoordsArray)
        {
            return false;
        }

        $cropMaskCoordsArray[2] = min($cropMaskCoordsArray[2], $originalWidth);
        $cropMaskCoordsArray[3] = min($cropMaskCoordsArray[3], $originalHeight);

        $imageCropRectCoordsString =
            $cropMaskCoordsArray[0] . ',' .
            $cropMaskCoordsArray[1] . ',' .
            $cropMaskCoordsArray[2] . ',' .
            $cropMaskCoordsArray[3];

        /*$data =
            '$coordinates: ' . $coordinates . "\n" .
            '$imageCropRectCoordsString: ' . $imageCropRectCoordsString . "\n"
        ;
        file_put_contents('/home/wojtek/Desktop/proimagine-cms.messages.log', $data);*/

        $imageChangesSaved = $this->imageController->cropImageUsingRectCoordsStringAndSave(
            $sourceFilePath,
            $imageCropRectCoordsString,
            $destinationFilePath
        );

        if ($imageChangesSaved)
        {
            $fileSystem = $this->getFileSystem();

            $attachment->file_size = $fileSystem->filesize($destinationFilePath);
            $attachment->image_crop_rect_coords = $imageCropRectCoordsString;
            $attachmentUpdated = $this->updateAttachment($attachment);
            if ($attachmentUpdated)
            {
                return $attachment->hash_for_file_name;
            }
        }

        return false;
    }

    public function isHashForFileNameAlreadyInUse($hashForFileName)
    {
        $prefixColumnsWithTable = false;

        $select = $this->sql->select();
        $select->from('attachments')
            ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')), $prefixColumnsWithTable)
            ->where(array('hash_for_file_name' => $hashForFileName));
        $resultSet = $this->getDatabaseManager()->getHydratedResultSet($select);

        $row = $resultSet->current();
        if (!$row)
        {
            throw new \Site\Exception\CmsException('Database error during checking if fileNameHash is unique.');
        }

        return $row->count ? true : false;
    }

    public function insertRecordsToAttachmentsTableUsingFilesInfo($filesInfo)
    {
        $filesInfo = (array)$filesInfo;

        if (!$filesInfo)
        {
            return false;
        }

        $dbManager = $this->getDatabaseManager();
        $dbManager->beginTransaction();

        $allInsertStatementsSuccessful = true;
        foreach ($filesInfo as $fileInfo)
        {
            if (!$this->insertRecordToAttachmentsTableUsingFileInfoAndReturnId($fileInfo))
            {
                $allInsertStatementsSuccessful = false;
                break;
            }
        }

        if ($allInsertStatementsSuccessful)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $allInsertStatementsSuccessful;
    }

    protected function insertRecordToAttachmentsTableUsingFileInfoAndReturnId($fileInfo)
    {
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');
        $parentAttachmentId = isset($fileInfo['parentAttachmentId']) && $fileInfo['parentAttachmentId'] ? (int)$fileInfo['parentAttachmentId'] : null;
        $authorId = isset($fileInfo['authorId']) && $fileInfo['authorId'] ? (int)$fileInfo['authorId'] : null;

        $dbManager = $this->getDatabaseManager();

        $insert = $this->sql->insert();
        $insert
            ->into('attachments')
            ->columns(
                array(
                    'parent_attachment_id',
                    'is_published',
                    'file_size',
                    'page_block_id',
                    'last_update_author_id',
                    'last_update_date',
                    'creation_date',
                    'hash_for_file_name',
                    'extension',
                    'mime_type',
                    'module_name',
                    'block_name',
                    'original_file_name',
                    'file_path'
                )
            )
            ->values(
                array(
                    'parent_attachment_id' => $parentAttachmentId,
                    'is_published' => 1,
                    'file_size' => (int)$fileInfo['fileSize'],
                    'page_block_id' => (int)$fileInfo['pageBlockId'],
                    'last_update_author_id' => $authorId,
                    'last_update_date' => $lastUpdateDate,
                    'creation_date' => $fileInfo['creationDate'],
                    'hash_for_file_name' => $fileInfo['uniqueHashForFileName'],
                    'extension' => $fileInfo[self::FI_FILE_EXTENSION],
                    'mime_type' => $fileInfo['mimeType'],
                    'module_name' => $fileInfo['moduleName'],
                    'block_name' => $fileInfo['blockName'],
                    'original_file_name' => $fileInfo['originalFileName'],
                    'file_path' => $fileInfo['destinationFilePath']
                )
            );
        $attachmentId = $dbManager->executeInsertAndGetLastInsertId($insert);

        return $attachmentId;
    }

    protected function makeFileInfoFromAttachment($attachment)
    {
        if (!$attachment)
        {
            return false;
        }

        $fileInfo = array(
            self::FI_FILE_SIZE => (int)$attachment->file_size,
            self::FI_PAGE_BLOCK_ID => (int)$attachment->page_block_id,
            self::FI_CREATION_DATE => $attachment->creation_date,
            self::FI_UNIQUE_HASH_FOR_FILE_NAME => $attachment->hash_for_file_name,
            self::FI_FILE_EXTENSION => $attachment->extension,
            self::FI_MIME_TYPE => $attachment->mime_type,
            self::FI_MODULE_NAME => $attachment->module_name,
            self::FI_BLOCK_NAME => $attachment->block_name,
            self::FI_ORIGINAL_FILE_NAME => $attachment->original_file_name,
            self::FI_DESTINATION_FILE_PATH => $attachment->file_path
        );

        return $fileInfo;
    }

    protected function getFilePathFromFileInfo($fileInfo)
    {
        $filePath = $this->getAttachmentsDirectory() .
            'module/' . $fileInfo[self::FI_MODULE_NAME];

        if ($fileInfo[self::FI_BLOCK_NAME] != '')
        {
            $filePath .= '/block/' . $fileInfo[self::FI_BLOCK_NAME] .
                '/' . $fileInfo[self::FI_PAGE_BLOCK_ID];
        }

        $filePath .= '/' . $fileInfo[self::FI_UNIQUE_HASH_FOR_FILE_NAME];

        if ($fileInfo[self::FI_FILE_EXTENSION] != '')
        {
            $filePath .= '.' . $fileInfo[self::FI_FILE_EXTENSION];
        }

        return $filePath;
    }

    protected function generateHashUsingOptionalSalt($optionalSalt = '')
    {
        return sha1(mt_rand() . microtime(true) . $optionalSalt);
    }
}
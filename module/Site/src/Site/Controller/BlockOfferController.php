<?php
namespace Site\Controller;

use Zend\View\Model\ViewModel;

class BlockOfferController extends BaseController
{
    const BLOCK_OFFER_DEGREES_TABLE_NAME = 'block_offer_degrees';
    const BLOCK_OFFER_MAJORS_TABLE_NAME = 'block_offer_majors';

    public function addEditDegreeAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $formData = array();

                if ($request->isPost())
                {
                    $pageBlockId = $request->getPost('pageBlockId');
                    $blockManager = $this->getBlockManager();
                    $offerBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);

                    if ($request->getPost('dataPosted'))
                    {
                        $customStatus = 'INVALID_DATA';

                        $formData = new \Site\Custom\FlexibleContainer($request->getPost('formData'));
                        $result = $offerBlock->addOrEditDegree($formData);

                        if ($result)
                        {
                            $customStatus =
                                ($formData->degreeId > 0) ?
                                    'DEGREE_MODIFIED' :
                                    'DEGREE_ADDED';
                        }
                        else
                        {
                            $customStatus =
                                ($formData->degreeId > 0) ?
                                    'DEGREE_MODIFYING_ERROR' :
                                    'DEGREE_ADDING_ERROR';
                        }

                        $jsonResponse->meta->customStatus = $customStatus;
                        $this->response->setContent((string)$jsonResponse);

                        return $this->response;
                    }
                    else
                    {
                        $degreeId = $request->getPost('degreeId');
                        if ($degreeId > 0)
                        {
                            if ($offerBlock)
                            {
                                $degreeData = $offerBlock->getDegreeById($degreeId);

                                $formData = array(
                                    'degreeId' => $degreeId,
                                    'degreeTitle' => $degreeData->name,
                                    'degreeType' => $degreeData->type,
                                    'degreeIsPublished' => $degreeData->is_published ? 1 : 0,
                                    'degreeLastUpdateAuthorId' => $user->user_id
                                );
                            }
                        }
                    }
                }

                $defaultFormData = array(
                    'degreeId' => '',
                    'degreeTitle' => '',
                    'degreeType' => '',
                    'degreeIsPublished' => 0
                );
                $formData = new \Site\Custom\FlexibleContainer(array_merge($defaultFormData, $formData));

                $metadata = new \Zend\Db\Metadata\Metadata($this->dbAdapter);
                $degreesTableColumnsMetadata = $metadata->getColumns(
                    self::BLOCK_OFFER_DEGREES_TABLE_NAME
                );
                $maxLengths = array();
                if ($degreesTableColumnsMetadata)
                {
                    foreach ($degreesTableColumnsMetadata as $column)
                    {
                        $name = $column->getName();

                        $maxLengths[$name] = $column->getCharacterMaximumLength();
                    }
                }

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $addNewDegreeView = new \Zend\View\Model\ViewModel();
                $addNewDegreeView->setTemplate('blocks/offer/add-edit-degree.phtml');
                $addNewDegreeView->setVariables(
                    array(
                        'maxLengths' => $maxLengths,
                        'formData' => $formData
                    )
                );

                $jsonResponse->data = $phpRenderer->render($addNewDegreeView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function addEditMajorAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageBlockId = (int)$request->getPost('pageBlockId');
                $blockManager = $this->getBlockManager();
                $offerBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $formData = array();

                if ($request->isPost())
                {
                    if (
                        $request->getPost('dataPosted') &&
                        ($offerBlock instanceof \Site\Block\Offer\Offer)
                    )
                    {
                        $customStatus = 'INVALID_DATA';

                        $formData = new \Site\Custom\FlexibleContainer($request->getPost('formData'));
                        $result = $offerBlock->addOrEditMajor($formData);

                        if ($result)
                        {
                            $customStatus = ($formData->majorId > 0) ? 'MAJOR_MODIFIED' : 'MAJOR_ADDED';
                        }
                        else
                        {
                            $customStatus = ($formData->majorId > 0) ? 'MAJOR_MODIFYING_ERROR' : 'MAJOR_ADDING_ERROR';
                        }

                        $jsonResponse->meta->customStatus = $customStatus;
                        $this->response->setContent((string)$jsonResponse);

                        return $this->response;
                    }
                    else
                    {
                        $majorId = $request->getPost('majorId');
                        if ($majorId > 0)
                        {
                            if ($offerBlock)
                            {
                                $major = $offerBlock->getMajorById($majorId);

                                $formData = array(
                                    'majorId' => $majorId,
                                    'majorDegreeId' => $major->degree_id,
                                    'majorIsPublished' => $major->is_published ? 1 : 0,
                                    'majorIsStationary' => $major->is_stationary ? 1 : 0,
                                    'majorIsRemote' => $major->is_remote ? 1 : 0,
                                    'majorTextIcon' => $major->bootstrap_glyphicon,
                                    'majorTitle' => $major->name,
                                    'majorLastUpdateAuthorId' => $user->user_id
                                );
                            }
                        }
                    }
                }

                $defaultFormData = array(
                    'majorId' => null,
                    'majorDegreeId' => null,
                    'majorIsPublished' => 0,
                    'majorIsStationary' => 1,
                    'majorIsRemote' => 0,
                    'majorTextIcon' => '',
                    'majorTitle' => ''
                );
                $formData = new \Site\Custom\FlexibleContainer(array_merge($defaultFormData, $formData));

                $metadata = new \Zend\Db\Metadata\Metadata($this->dbAdapter);
                $MajorsTableColumnsMetadata = $metadata->getColumns(
                    self::BLOCK_OFFER_MAJORS_TABLE_NAME
                );
                $maxLengths = array();
                if ($MajorsTableColumnsMetadata)
                {
                    foreach ($MajorsTableColumnsMetadata as $column)
                    {
                        $name = $column->getName();

                        $maxLengths[$name] = $column->getCharacterMaximumLength();
                    }
                }

                $majorDegreesOptions = array();
                $degrees = $this->getDegrees();
                if ($degrees)
                {
                    foreach ($degrees as $degree)
                    {
                        $majorDegreesOptions[$degree->degree_id] = $degree->title;
                    }
                }

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $addNewMajorView = new \Zend\View\Model\ViewModel();
                $addNewMajorView->setTemplate('blocks/offer/add-edit.phtml');
                $addNewMajorView->setVariables(
                    array(
                        'majorDegreesOptions' => $majorDegreesOptions,
                        'maxLengths' => $maxLengths,
                        'formData' => $formData
                    )
                );

                $jsonResponse->data = $phpRenderer->render($addNewMajorView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function deleteDegreeAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageBlockId = $request->getPost('pageBlockId');
                $degreeId = $request->getPost('degreeId');
                $offerBlock = $this->getBlockManager()->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $deleted = $offerBlock->deleteDegreeById($degreeId);

                if ($deleted)
                {
                    $jsonResponse->meta->customStatus = 'DEGREE_DELETED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'DEGREE_DELETION_ERROR';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function deleteMajorAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageBlockId = $request->getPost('pageBlockId');
                $majorId = $request->getPost('majorId');
                $offerBlock = $this->getBlockManager()->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $deleted = $offerBlock->deleteMajorById($majorId);

                if ($deleted)
                {
                    $jsonResponse->meta->customStatus = 'MAJOR_DELETED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'MAJOR_DELETION_ERROR';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getAddEditDegreeViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');

                $blockManager = $this->getBlockManager();
                $addEditDegreeViewHtml = $blockManager->callPageBlockCustomMethod(
                    $pageBlockId,
                    'renderAddEditDegree',
                    array($this->request, true)
                );
                if ($addEditDegreeViewHtml != '')
                {
                    $jsonResponse->data = $addEditDegreeViewHtml;
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate(
                            'Failed to fetch template', 'default', $this->getUserLanguage()->zend2_locale
                        ) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getDegreesManagementViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');

                $blockManager = $this->getBlockManager();
                $degreesManagementViewHtml = $blockManager->callPageBlockCustomMethod(
                    $pageBlockId,
                    'renderDegreesManagement',
                    array($this->request, true)
                );
                if ($degreesManagementViewHtml != '')
                {
                    $jsonResponse->data = $degreesManagementViewHtml;
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate(
                        'Failed to fetch template', 'default', $this->getUserLanguage()->zend2_locale
                    ) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function reorderMajorsAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageBlockId = (int)$request->getPost('pageBlockId');
                $parentMajorId = $request->getPost('parentMajorId');
                $parentMajorId = $parentMajorId ? (int)$parentMajorId : null;
                $majors = $request->getPost('majors');

                $blockManager = $this->getBlockManager();
                $offerBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $reorderingSuccessful = $offerBlock->reorderMajors($parentMajorId, $majors);

                if ($reorderingSuccessful)
                {
                    $jsonResponse->meta->customStatus = 'OFFER_ITEMS_REORDERED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'OFFER_ITEMS_REORDERING_ERROR';
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to reorder majors', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function toggleMajorPublishingAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');
                $majorId = $request->getPost('majorId');
                $published = (int)$request->getPost('published') ? 1 : 0;

                $blockManager = $this->getBlockManager();
                $offerBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $updated = $offerBlock->toggleMajorPublished($majorId, $published);
                if (!$updated)
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to update major', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function toggleDegreePublishingAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageBlockId = (int)$request->getPost('pageBlockId');
                $degreeId = (int)$request->getPost('degreeId');
                $published = (int)$request->getPost('published') ? 1 : 0;

                $blockManager = $this->getBlockManager();
                $offerBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $updated = $offerBlock->toggleDegreePublished($degreeId, $published);
                if (!$updated)
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate(
                            'Failed to update degree', 'default', $this->getUserLanguage()->zend2_locale
                        ) . '.';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    protected function getDegreeCountByContentId($contentId)
    {
        $contentId = (int)$contentId;
        if ($contentId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_OFFER_DEGREES_TABLE_NAME)
            ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')), $prefixColumnsWithTable)
            ->where(array('content_id' => $contentId));
        $resultSet = $dbManager->getHydratedResultSet($select);
        $row = $resultSet->current();
        $count = $row ? $row->count : false;

        return $count;
    }

    protected function getDegrees()
    {
        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_OFFER_DEGREES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable);
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    protected function createNewOrUpdateExistingDegree($pageBlockId, $formData)
    {
        $degreeId = isset($formData['degreeId']) ? (int)$formData['degreeId'] : null;

        if ($degreeId > 0)
        {
            return $this->updateExistingDegree($pageBlockId, $formData);
        }
        else
        {
            return $this->createNewDegree($pageBlockId, $formData);
        }
    }

    protected function selectThisDegreeAsDefaultIfThereIsOnlyOne($pageBlockId, $degreeId)
    {
        $pageBlockId = (int)$pageBlockId;
        $degreeId = (int)$degreeId;

        if (($pageBlockId <= 0) || ($degreeId <= 0))
        {
            return false;
        }

        $blockManager = $this->getBlockManager();
        $pageBlockContentId = $blockManager->getPageBlockContentIdByPageBlockId($pageBlockId);

        if ($this->getDegreeCountByContentId($pageBlockContentId) > 0)
        {
            $offerBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);

            return $offerBlock->selectDegreeAsDefault($degreeId);
        }

        return false;
    }
}
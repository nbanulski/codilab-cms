<?php
namespace Site\Controller;

use Zend\View\Model\ViewModel;

class BlockEmployeesController extends BaseController
{
    const BLOCK_EMPLOYEES_DEGREES_TABLE_NAME = 'block_employees_degrees';
    const BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME = 'block_employees_employees';

    public function addEditDegreeAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $formData = array();

                if ($request->isPost())
                {
                    $pageBlockId = (int)$request->getPost('pageBlockId');
                    $blockManager = $this->getBlockManager();
                    $employeesBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);

                    if ($employeesBlock instanceof \Site\Block\Employees\Employees)
                    {
                        if ($request->getPost('dataPosted'))
                        {
                            $formData = new \Site\Custom\FlexibleContainer($request->getPost('formData'));
                            $result = $employeesBlock->addOrEditDegree($formData);

                            if ($result)
                            {
                                $customStatus =
                                    ($formData->degreeId > 0) ?
                                        'DEGREE_MODIFIED' :
                                        'DEGREE_ADDED';
                            }
                            else
                            {
                                $customStatus =
                                    ($formData->degreeId > 0) ?
                                        'DEGREE_MODIFYING_ERROR' :
                                        'DEGREE_ADDING_ERROR';
                            }

                            $jsonResponse->meta->customStatus = $customStatus;
                            $this->response->setContent((string)$jsonResponse);

                            return $this->response;
                        }
                        else
                        {
                            $degreeId = $request->getPost('degreeId');
                            if ($degreeId > 0)
                            {
                                if ($employeesBlock)
                                {
                                    $degreeData = $employeesBlock->getDegreeById($degreeId);

                                    $formData = array(
                                        'degreeId' => $degreeId,
                                        'degreeTitle' => $degreeData->name,
                                        'degreeIsPublished' => $degreeData->is_published ? 1 : 0,
                                        'degreeLastUpdateAuthorId' => $user->user_id
                                    );
                                }
                            }
                        }
                    }
                }

                $defaultFormData = array(
                    'degreeId' => '',
                    'degreeTitle' => '',
                    'degreeIsPublished' => 0
                );
                $formData = new \Site\Custom\FlexibleContainer(array_merge($defaultFormData, $formData));

                $metadata = new \Zend\Db\Metadata\Metadata($this->dbAdapter);
                $blockEmployeesDegreesTableColumnsMetadata = $metadata->getColumns(
                    self::BLOCK_EMPLOYEES_DEGREES_TABLE_NAME
                );
                $maxLengths = array();
                if ($blockEmployeesDegreesTableColumnsMetadata)
                {
                    foreach ($blockEmployeesDegreesTableColumnsMetadata as $column)
                    {
                        $name = $column->getName();

                        $maxLengths[$name] = $column->getCharacterMaximumLength();
                    }
                }

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $addNewDegreeView = new \Zend\View\Model\ViewModel();
                $addNewDegreeView->setTemplate('blocks/images-pile/add-edit-degree.phtml');
                $addNewDegreeView->setVariables(
                    array(
                        'maxLengths' => $maxLengths,
                        'formData' => $formData
                    )
                );

                $jsonResponse->data = $phpRenderer->render($addNewDegreeView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function addEditEmployeeAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageBlockId = (int)$request->getPost('pageBlockId');
                $blockManager = $this->getBlockManager();
                $employeesBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $formData = array();

                if ($employeesBlock instanceof \Site\Block\Employees\Employees)
                {
                    if ($request->isPost())
                    {
                        if ($request->getPost('dataPosted'))
                        {
                            $formData = new \Site\Custom\FlexibleContainer($request->getPost('formData'));
                            $result = $employeesBlock->addOrEditEmployee($formData);

                            if ($result)
                            {
                                $customStatus = ($formData->employeeId > 0) ? 'EMPLOYEE_MODIFIED' : 'EMPLOYEE_ADDED';
                            }
                            else
                            {
                                $customStatus = ($formData->employeeId > 0) ? 'EMPLOYEE_MODIFYING_ERROR' : 'EMPLOYEE_ADDING_ERROR';
                            }

                            $jsonResponse->meta->customStatus = $customStatus;
                            $this->response->setContent((string)$jsonResponse);

                            return $this->response;
                        }
                        else
                        {
                            $employeeId = $request->getPost('employeeId');
                            if ($employeeId > 0)
                            {
                                if ($employeesBlock)
                                {
                                    $employee = $employeesBlock->getEmployeeById($employeeId);
                                    $attachmentId = $employee->attachment_id;
                                    $attachment = $this->getUploadManager()->getAttachmentById($attachmentId);
                                    $uniqueHashForFileName = $attachment->hash_for_filename;

                                    $formData = array(
                                        'employeeId' => $employeeId,
                                        'employeeDegreeId' => $employee->degree_id,
                                        'employeeIsPublished' => $employee->is_published ? 1 : 0,
                                        'employeeNameAndSurname' => $employee->name_and_surname,
                                        'employeePosition' => $employee->position,
                                        'employeeAbout' => $employee->about,
                                        'employeeScientificCareer' => $employee->scientific_career,
                                        'employeeTheMostImportantPublications' => $employee->most_important_publications,
                                        'employeeContactInfo' => $employee->contact_info,
                                        'employeeUniqueHashForFileName' => $uniqueHashForFileName,
                                        'employeeOriginalFileName' => $attachment->original_file_name
                                    );
                                }
                            }
                        }
                    }
                }

                $defaultFormData = array(
                    'employeeId' => '',
                    'employeeDegreeId' => null,
                    'employeeIsPublished' => 0,
                    'employeeNameAndSurname' => '',
                    'employeePosition' => '',
                    'employeeAbout' => '',
                    'employeeScientificCareer' => '',
                    'employeeTheMostImportantPublications' => '',
                    'employeeContactInfo' => '',
                    'employeeUniqueHashForFileName' => '',
                    'employeeOriginalFileName' => ''
                );
                $formData = new \Site\Custom\FlexibleContainer(array_merge($defaultFormData, $formData));

                $metadata = new \Zend\Db\Metadata\Metadata($this->dbAdapter);
                $blockEmployeesEmployeesTableColumnsMetadata = $metadata->getColumns(
                    self::BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME
                );
                $maxLengths = array();
                if ($blockEmployeesEmployeesTableColumnsMetadata)
                {
                    foreach ($blockEmployeesEmployeesTableColumnsMetadata as $column)
                    {
                        $name = $column->getName();

                        $maxLengths[$name] = $column->getCharacterMaximumLength();
                    }
                }

                $employeeDegreesOptions = array();
                $degrees = $this->getDegrees();
                if ($degrees)
                {
                    foreach ($degrees as $degree)
                    {
                        $employeeDegreesOptions[$degree->degree_id] = $degree->name;
                    }
                }

                $employeeParentEmployeesOptions = array();

                $this->addEditView->setVariables(
                    array(
                        'employeeParentEmployeesOptions' => $employeeParentEmployeesOptions,
                        'employeeDegreesOptions' => $employeeDegreesOptions
                    )
                );

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $addNewEmployeeView = new \Zend\View\Model\ViewModel();
                $addNewEmployeeView->setTemplate('blocks/images-pile/add-edit-employee.phtml');
                $addNewEmployeeView->setVariables(
                    array(
                        'employeeParentEmployeesOptions' => $employeeParentEmployeesOptions,
                        'employeeDegreesOptions' => $employeeDegreesOptions,
                        'maxLengths' => $maxLengths,
                        'formData' => $formData
                    )
                );

                $jsonResponse->data = $phpRenderer->render($addNewEmployeeView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function deleteDegreeAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $deleted = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $degreeId = $request->getPost('degreeId');

                $employeesBlock = $this->getBlockManager()->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($employeesBlock instanceof \Site\Block\Employees\Employees)
                {
                    $deleted = $employeesBlock->deleteDegreeById($degreeId);
                }

                if ($deleted)
                {
                    $jsonResponse->meta->customStatus = 'DEGREE_DELETED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'DEGREE_DELETION_ERROR';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function deleteEmployeeAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageBlockId = $request->getPost('pageBlockId');
                $employeeId = $request->getPost('employeeId');

                $deleted = false;

                $employeesBlock = $this->getBlockManager()->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($employeesBlock instanceof \Site\Block\Employees\Employees)
                {
                    $deleted = $employeesBlock->deleteEmployeeById($employeeId);
                }

                if ($deleted)
                {
                    $jsonResponse->meta->customStatus = 'EMPLOYEE_DELETED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'EMPLOYEE_DELETION_ERROR';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getEmployeesColumnsAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $foundEmployeesColumnCount = 0;
                $foundEmployeesColumns = false;

                $pageBlockId = (int)$request->getPost('pageBlockId');
                $firstLetter = $request->getPost('firstLetter', '');

                $blockManager = $this->getBlockManager();
                $employeesBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($employeesBlock instanceof \Site\Block\Employees\Employees)
                {
                    $foundEmployees = $employeesBlock->findEmployees($firstLetter);
                    $foundEmployeesDivided = $employeesBlock->divideFoundEmployeesByFirstLetter($foundEmployees);

                    if ($foundEmployeesDivided)
                    {
                        foreach ($foundEmployeesDivided as $firstLetter => $employees)
                        {
                            $employeesColumn = array(
                                'firstLetter' => $firstLetter,
                                'employees' => $employees
                            );
                            $foundEmployeesColumns[] = $employeesColumn;
                        }

                        $foundEmployeesColumnCount = count($foundEmployeesColumns);
                    }
                }

                $jsonResponse->data = new \stdClass();
                $jsonResponse->data->foundEmployeesColumnCount = $foundEmployeesColumnCount;
                $jsonResponse->data->foundEmployeesColumns = $foundEmployeesColumns;
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function findEmployeesAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $foundEmployeeCount = 0;
                $foundEmployees = false;

                $pageBlockId = (int)$request->getPost('pageBlockId');
                $formData = new \Site\Custom\FlexibleContainer($request->getPost('formData'));

                $blockManager = $this->getBlockManager();
                $employeesBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($employeesBlock instanceof \Site\Block\Employees\Employees)
                {
                    $alphabetLetter = $formData->alphabetLetter;
                    $nameAndSurname = $formData->nameAndSurname;
                    $degreeId = $formData->degreeId;

                    $foundEmployees = $employeesBlock->findEmployees($alphabetLetter, $nameAndSurname, $degreeId);
                    $foundEmployees = $employeesBlock->convertFoundEmployeesToArray($foundEmployees);

                    if ($foundEmployees)
                    {
                        $foundEmployeeCount = count($foundEmployees);
                    }
                }

                $jsonResponse->data = new \stdClass();
                $jsonResponse->data->foundEmployeeCount = $foundEmployeeCount;
                $jsonResponse->data->foundEmployees = $foundEmployees;
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getAddEditDegreeViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');

                $blockManager = $this->getBlockManager();
                $addEditDegreeViewHtml = $blockManager->callPageBlockCustomMethod(
                    $pageBlockId,
                    'renderAddEditDegree',
                    array($this->request, true)
                );
                if ($addEditDegreeViewHtml != '')
                {
                    $jsonResponse->data = $addEditDegreeViewHtml;
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate(
                            'Failed to fetch template', 'default', $this->getUserLanguage()->zend2_locale
                        ) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getDegreesManagementViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');

                $blockManager = $this->getBlockManager();
                $degreesManagementViewHtml = $blockManager->callPageBlockCustomMethod(
                    $pageBlockId,
                    'renderDegreesManagement',
                    array($this->request, true)
                );
                if ($degreesManagementViewHtml != '')
                {
                    $jsonResponse->data = $degreesManagementViewHtml;
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage =
                        $this->translate(
                            'Failed to fetch template', 'default', $this->getUserLanguage()->zend2_locale
                        ) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function reorderDegreesAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $reorderingSuccessful = false;

                $pageBlockId = (int)$request->getPost('pageBlockId');
                $degrees = $request->getPost('degrees');

                $blockManager = $this->getBlockManager();
                $employeesBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);

                if ($employeesBlock instanceof \Site\Block\Employees\Employees)
                {
                    $reorderingSuccessful = $employeesBlock->reorderDegrees($degrees);
                }

                if ($reorderingSuccessful)
                {
                    $jsonResponse->meta->customStatus = 'EMPLOYEES_REORDERED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'EMPLOYEES_REORDERING_ERROR';
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to reorder employee degrees', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function reorderEmployeesInDegreeAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $reorderingSuccessful = false;

                $pageBlockId = (int)$request->getPost('pageBlockId');
                $degreeId = $request->getPost('degreeId');
                $employees = $request->getPost('employees');

                $blockManager = $this->getBlockManager();
                $employeesBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($employeesBlock instanceof \Site\Block\Employees\Employees)
                {
                    $reorderingSuccessful = $employeesBlock->reorderEmployeesInDegree($degreeId, $employees);
                }

                if ($reorderingSuccessful)
                {
                    $jsonResponse->meta->customStatus = 'EMPLOYEES_REORDERED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'EMPLOYEES_REORDERING_ERROR';
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to reorder employees', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function toggleDegreePublishingAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $updated = false;

                $pageBlockId = (int)$request->getPost('pageBlockId');
                $degreeId = (int)$request->getPost('degreeId');
                $published = (int)$request->getPost('published') ? 1 : 0;

                $blockManager = $this->getBlockManager();
                $employeesBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($employeesBlock instanceof \Site\Block\Employees\Employees)
                {
                    $updated = $employeesBlock->toggleDegreePublished($degreeId, $published);
                }

                if (!$updated)
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate(
                            'Failed to update employee degree', 'default', $this->getUserLanguage()->zend2_locale
                        ) . '.';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function toggleEmployeePublishingAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $updated = false;

                $pageBlockId = (int)$request->getPost('pageBlockId');
                $employeeId = (int)$request->getPost('employeeId');
                $published = (int)$request->getPost('published') ? 1 : 0;

                $blockManager = $this->getBlockManager();
                $employeesBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($employeesBlock instanceof \Site\Block\Employees\Employees)
                {
                    $updated = $employeesBlock->toggleEmployeePublished($employeeId, $published);
                }

                if (!$updated)
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to publish / unpublish employee', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    protected function getDegreeCountByContentId($contentId)
    {
        $contentId = (int)$contentId;
        if ($contentId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_EMPLOYEES_DEGREES_TABLE_NAME)
            ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')), $prefixColumnsWithTable)
            ->where(array('content_id' => $contentId));
        $resultSet = $dbManager->getHydratedResultSet($select);
        $row = $resultSet->current();
        $count = $row ? $row->count : false;

        return $count;
    }

    protected function getDegrees()
    {
        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_EMPLOYEES_DEGREES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable);
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    protected function createNewOrUpdateExistingDegree($pageBlockId, $formData)
    {
        $degreeId = isset($formData['degreeId']) ? (int)$formData['degreeId'] : null;

        if ($degreeId > 0)
        {
            return $this->updateExistingDegree($pageBlockId, $formData);
        }
        else
        {
            return $this->createNewDegree($pageBlockId, $formData);
        }
    }
}
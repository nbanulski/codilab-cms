<?php
namespace Site\Controller;

class UserController extends BaseController
{
    public function listAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageNumber = (int)$request->getPost('pageNumber') ? : 1;
                $itemCountPerPage = (int)$request->getPost('itemCountPerPage') ? : 10;
                $searchText = trim(strip_tags($request->getPost('searchText', '')));

                $select = $this->sql->select();
                $select
                    ->from(array('u' => 'users'))
                    ->columns(array(
                        'user_id',
                        'active',
                        'type_id',
                        'last_update_author_id',
                        'last_update_date',
                        'account_creation_date',
                        'account_activation_date',
                        'login',
                        'email'
                    ))
                    ->join(
                        array('uat' => 'users_account_types'), 'uat.type_id = u.type_id', array('account_type_name' => 'name'), $select::JOIN_LEFT
                    )
                    ->join(
                        array('u2' => 'users'), 'u2.user_id = u.last_update_author_id', array('last_update_author' => 'login'), $select::JOIN_LEFT
                    )
                    ->order('u.account_creation_date DESC, u.account_activation_date DESC');

                if ($searchText != '')
                {
                    $select->where->literal('u.login REGEXP ?', array($searchText));
                }

                $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
                $paginatorAdapter = new \Zend\Paginator\Adapter\DbSelect($select, $this->dbAdapter, $resultSetPrototype);
                $paginator = new \Zend\Paginator\Paginator($paginatorAdapter);
                $paginator->setCurrentPageNumber($pageNumber);
                $paginator->setItemCountPerPage($itemCountPerPage);

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $pagesView = new \Zend\View\Model\ViewModel();
                $pagesView->setTemplate('site/admin/users.phtml');
                $pagesView->setVariables(
                    array(
                        'userLanguage' => $this->getUserLanguage(),
                        'pageNumber' => $pageNumber,
                        'itemCountPerPage' => $itemCountPerPage,
                        'usersPaginator' => $paginator,
                        'searchText' => $searchText
                    )
                );

                $jsonResponse->data = $phpRenderer->render($pagesView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function toggleAccountActivityAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $userId = (int)$request->getPost('userId');
                $active = (int)$request->getPost('active') ? 1 : 0;

                if ($userId != $user->user_id)
                {
                    $userManager = $this->getUserManager();
                    $currentStatus = $userManager->toggleAccountActivity($userId, $active);
                    if ($currentStatus === false)
                    {
                        $jsonResponse->meta->requestErrorMessage =
                            $this->translate(
                                'Failed to change user account status',
                                'default', $this->getUserLanguage()->zend2_locale
                            ) . '.'
                        ;
                    }
                    else
                    {
                        $jsonResponse->meta->customStatus = $currentStatus ? 1 : 0;
                    }
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage =
                        $this->translate(
                            'You cannot change status of your own account',
                            'default', $this->getUserLanguage()->zend2_locale
                        ) . '!'
                    ;
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function deleteAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $userId = $request->getPost('userId');

                if ($userId != $user->user_id)
                {
                    $userManager = $this->getUserManager();
                    $deleted = $userManager->deleteUserById($userId);

                    if ($deleted)
                    {
                        $jsonResponse->meta->customStatus = 'USER_DELETED';
                    }
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage =
                        $this->translate(
                            'You cannot delete your own account',
                            'default', $this->getUserLanguage()->zend2_locale
                        ) . '!'
                    ;
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function addEditAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $userManager = $this->getUserManager();
                $formData = array();

                if ($request->isPost())
                {
                    if ($request->getPost('dataPosted'))
                    {
                        $customStatus = null;

                        $formData = $request->getPost('formData');
                        $formData = new \Site\Custom\FlexibleContainer($formData);
                        if ($formData->count() > 0)
                        {
                            $userPassword = null;
                            if (
                                ($formData->userPassword == $formData->userPassword2) &&
                                $userManager->isPasswordValid($formData->userPassword)
                            )
                            {
                                $userPassword = $formData->userPassword;
                            }

                            $formData->userPassword = $userPassword;
                            $formData->authorId = $user->user_id;

                            $result = $this->createNewOrUpdateExistingUser($formData);
                            if ($result)
                            {
                                $customStatus = ($formData->userId > 0) ? 'USER_MODIFIED' : 'USER_ADDED';
                            }
                        }

                        $jsonResponse->meta->customStatus = $customStatus;
                        $this->response->setContent((string)$jsonResponse);

                        return $this->response;
                    }
                    else
                    {
                        $userId = $request->getPost('userId');
                        if ($userId > 0)
                        {
                            $user = $userManager->getUserById($userId);
                            $userDetails = $userManager->getUserDetailsByUserId($userId);
                            if ($user && $userDetails)
                            {
                                $formData = array(
                                    'userId' => $user->user_id,
                                    'userAccountTypeId' => $user->type_id,
                                    'userLogin' => $user->login,
                                    'userEmail' => $user->email,
                                    'userGender' => $userDetails->gender,
                                    'userName' => $userDetails->name,
                                    'userSurname' => $userDetails->surname
                                );
                            }
                        }
                    }
                }

                $defaultFormData = array(
                    'userId' => null,
                    'userAccountTypeId' => null,
                    'userLogin' => '',
                    'userEmail' => '',
                    'userGender' => 0,
                    'userName' => '',
                    'userSurname' => ''
                );
                $formData = new \Site\Custom\FlexibleContainer(array_merge($defaultFormData, $formData));

                $metadata = new \Zend\Db\Metadata\Metadata($this->dbAdapter);
                $tableName = 'users';
                $tableColumnsMetadata = $metadata->getColumns($tableName);
                $maxLengths = array('users', 'users_details');
                if ($tableColumnsMetadata)
                {
                    foreach ($tableColumnsMetadata as $column)
                    {
                        $name = $column->getName();

                        $maxLengths['users'][$name] = $column->getCharacterMaximumLength();
                    }
                }

                $tableName = 'users_details';
                $tableColumnsMetadata = $metadata->getColumns($tableName);
                if ($tableColumnsMetadata)
                {
                    foreach ($tableColumnsMetadata as $column)
                    {
                        $name = $column->getName();

                        $maxLengths['users_details'][$name] = $column->getCharacterMaximumLength();
                    }
                }

                $accountTypes = $userManager->getAccountTypes();
                $userAccountTypesOptions = array();
                if ($accountTypes)
                {
                    foreach ($accountTypes as $accountType)
                    {
                        $name = $accountType->name;

                        $userAccountTypesOptions[$accountType->type_id] = $name;
                    }
                }

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $addEditUserView = new \Zend\View\Model\ViewModel();
                $addEditUserView->setTemplate('site/admin/add-edit-user.phtml');
                $addEditUserView->setVariables(
                    array(
                        'userLanguage' => $this->getUserLanguage(),
                        'currentLanguage' => $this->language,
                        'maxLengths' => $maxLengths,
                        'userAccountTypesOptions' => $userAccountTypesOptions,
                        'formData' => $formData,
                        'allowedPasswordChars' => $userManager->getAllowedPasswordChars()
                    )
                );

                $jsonResponse->data = $phpRenderer->render($addEditUserView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function resetPasswordAction()
    {
        $messages = '';
        $emailSent = false;

        $request = $this->getRequest();
        if ($request->isPost())
        {
            $loginOrEmail = trim($request->getPost('loginOrEmail', ''));
            $isEmail = mb_strpos($loginOrEmail, '@') !== false;

            if ($loginOrEmail != '')
            {
                $userManager = $this->getUserManager();

                if ($isEmail && !$userManager->getUserByEmail($loginOrEmail))
                {
                    $messages =
                        $this->translate(
                            'There is no user with this e-mail address',
                            'default', $this->getUserLanguage()->zend2_locale
                        ) . '.'
                    ;
                }
                else if (!$isEmail && !$userManager->getUserByLogin($loginOrEmail))
                {
                    $messages =
                        $this->translate(
                            'There is no user with this login',
                            'default', $this->getUserLanguage()->zend2_locale
                        ) . '.'
                    ;
                }
                else
                {
                    $sent = $userManager->sendPasswordResetActivationCode($loginOrEmail);
                    if ($sent)
                    {
                        $messages =
                            $this->translate(
                                'On the e-mail address associated with this account was sent a link by which you ' .
                                'can assign a new password',
                                'default', $this->getUserLanguage()->zend2_locale
                            ) . '.'
                        ;

                        $emailSent = true;
                    }
                    else
                    {
                        $messages =
                            $this->translate(
                                'Failed to successfully complete the password reset process. Please try again later',
                                'default', $this->getUserLanguage()->zend2_locale
                            ) . '.'
                        ;
                    }
                }
            }
        }

        $layout = $this->layout();
        $layout->setTemplate('layout/lockscreen');
        $layout->page = new \Site\Custom\FlexibleContainer();
        $layout->page->title = $this->translate('Password reset', $this->getUserLanguage()->zend2_locale);
        $layout->setVariables(
            array(
                'siteName' => $this->cmsConfig->site_name
            )
        );

        return new \Zend\View\Model\ViewModel(
            array(
                'messages' => $messages,
                'emailSent' => $emailSent
            )
        );
    }

    public function newPasswordAction()
    {
        $messages = '';
        $login = '';
        $confirmationCode = '';
        $passwordChanged = false;
        $wrongLinkOrAccountAlreadyActivated = false;

        $request = $this->getRequest();
        $parameters = $this->getParametersForCurrentPageFromRequest($request);
        $userManager = $this->getUserManager();

        $login = $parameters->account;
        $confirmationCode = $parameters->code;

        if (($login != '') && ($confirmationCode != ''))
        {
            $user = $userManager->getUserByLogin($login);
            if ($user && ($user->confirmation_code == $confirmationCode))
            {
                $login = $user->login;
            }
            else
            {
                $messages .=
                    $this->translate(
                        'Error - account does not exists or wrong URL address was used',
                        'default', $this->getUserLanguage()->zend2_locale
                    ) . '.'
                ;
                $wrongLinkOrAccountAlreadyActivated = true;
            }
        }
        else
        {
            $layout = $this->layout();
            $layout->page->title =
                '404 - ' . $this->translate('page not found', $this->getUserLanguage()->zend2_locale);
            $layout->setVariables(
                array(
                    'siteName' => $this->cmsConfig->site_name
                )
            );

            $response = $this->response;
            $response->getHeaders()->addHeaderLine('Location', $request->getBaseUrl() . '/404');
            $response->setStatusCode(404);

            return new \Zend\View\Model\ViewModel();
        }

        $formTitle = $this->translate('New password for account', $this->getUserLanguage()->zend2_locale);
        if ($login != '')
        {
            $formTitle .= ' <em>' . $login . '</em>';
        }

        if ($request->isPost() && ($messages == ''))
        {
            $password1 = trim($request->getPost('password1', ''));
            $password2 = trim($request->getPost('password2', ''));

            if (($password1 == '') || ($password2 == ''))
            {
                $messages .= $this->translate(
                        'Enter password into both fields',
                        'default', $this->getUserLanguage()->zend2_locale
                    ) . '.'
                ;
            }
            else if ($password1 != $password2)
            {
                $messages .= $this->translate(
                        'You must type IDENTICAL passwords into both fields (case-sensitive)',
                        'default', $this->getUserLanguage()->zend2_locale
                    ) . '.'
                ;
            }
            else
            {
                $passwordChanged = $userManager->setPasswordForUser($user->user_id, $password1);
                if ($passwordChanged)
                {
                    $userManager->removeUserConfirmationCode($user->user_id);
                    $messages .= $this->translate(
                            'The password was changed successfully',
                            'default', $this->getUserLanguage()->zend2_locale
                        ) . '.'
                    ;
                }
                else
                {
                    $messages .= $this->translate(
                            'Could not change password',
                            'default', $this->getUserLanguage()->zend2_locale
                        ) . '.'
                    ;
                }
            }
        }

        $layout = $this->layout();
        $layout->setTemplate('layout/lockscreen');
        $layout->page = new \Site\Custom\FlexibleContainer();
        $layout->page->title = $this->translate('New password for account', $this->getUserLanguage()->zend2_locale);
        $layout->setVariables(
            array(
                'siteName' => $this->cmsConfig->site_name
            )
        );

        return new \Zend\View\Model\ViewModel(
            array(
                'messages' => $messages,
                'formTitle' => $formTitle,
                'login' => $login,
                'passwordChanged' => $passwordChanged,
                'wrongLinkOrAccountAlreadyActivated' => $wrongLinkOrAccountAlreadyActivated
            )
        );
    }

    public function registrationAction()
    {
        $error = false;
        $messages = '';

        $request = $this->getRequest();
        $userLogin = '';
        $userEmail = '';
        $userName = '';
        $userSurname = '';
        $newPassword = '';
        $newPassword2 = '';
        $userBirthDate = '';
        $userAccountTypeName = '';
        $userId = null;
        $errorCode = 0; // 1- Mail Not Sent, can continue 2 - User Not Created, Missing Essential Information in form
        $provinces = new \Site\Custom\FlexibleContainer();

        if ($request->isPost())
        {
            $userManager = $this->getUserManager();
            $userAccountTypeName = strip_tags((string) $request->getPost('userAccountType'));
            $provinces = $userManager->getProvinces();
            if(!empty($userAccountTypeName))
            {
                $userAccountType = $userManager->getUserAccountTypeByRole($userAccountTypeName);
            }
            else
            {
                $userAccountType = $userManager->getUserAccountTypeByRole('member');
            }


            if (!$userAccountType)
            {
                $messages .= $this->translate(
                        'An unknown error occurred on the server side. Please try again later',
                        'default', $this->getUserLanguage()->zend2_locale
                    ) . '.' . "\n"
                ;
                $error = true;
            }

            if (!$error)
            {
                $user = $this->sessionData();
                $authorId = $user ? $user->user_id : null;

                $userAccountTypeId = $userAccountType->type_id;
                $userLogin = $request->getPost('userLogin');
                $userEmail = $request->getPost('userEmail');
                //$userGender = $request->getPost('userGender');
                $userBirthDate = $request->getPost('birthDate');
                $userName = $request->getPost('userName');
                $userSurname = $request->getPost('userSurname');
                $newPassword = $request->getPost('userPassword');
                $newPassword2 = $request->getPost('userPassword2');
                $userPassword = null;
                if (($newPassword == $newPassword2) && $userManager->isPasswordValid($newPassword))
                {
                    $userPassword = $newPassword;
                }

                if (($userLogin != '') && ($userEmail != '') && ($userName != '') && ($userSurname != '') &&
                    ($userPassword != ''))
                {
                    $errorLoginExists = false;
                    $errorEmailExists = false;

                    try
                    {
                        $userId = $this->createNewOrUpdateExistingUser(
                            array(
                                'userId' => null,
                                'userAccountTypeId' => $userAccountTypeId,
                                'userLogin' => $userLogin,
                                'userPassword' => $userPassword,
                                'userEmail' => $userEmail,
                                //'userGender' => $userGender,
                                'userBirthDate' => $userBirthDate,
                                'userName' => $userName,
                                'userSurname' => $userSurname,
                                'authorId' => $authorId
                            )
                        );
                    }
                    catch (\Site\Exception\UserWithThatLoginAlreadyExistsException $loginExistsException)
                    {
                        $errorLoginExists = true;
                        $messages .= $loginExistsException->getMessage();
                    }
                    catch (\Site\Exception\UserWithThatEmailAlreadyExistsException $emailExistsException)
                    {
                        $errorEmailExists = true;
                        $messages .= $emailExistsException->getMessage();
                    }

                    if (!$errorLoginExists && !$errorEmailExists)
                    {
                        $unknownError = true;

                        if ($userId > 0)
                        {
                            try
                            {
                                $userManager->sendAccountActivationCode($userEmail);
                                $unknownError = false;
                            }
                            catch (\Exception $e)
                            {
                                $errorCode = 1;
                            }
                        }

                        if (!$unknownError)
                        {
                            $messages .= $this->translate(
                                    'Your account has been successfully created. On the specified e-mail we have ' .
                                    'sent the link to activate your account and further instructions',
                                    'default', $this->getUserLanguage()->zend2_locale
                                ) . '.' . "\n"
                            ;
                        }
                        else
                        {
                            $messages .= $this->translate(
                                    'An unknown error occurred on the server side. Your data has however been saved',
                                    'default', $this->getUserLanguage()->zend2_locale
                                ) . '.'
                            ;
                        }
                    }
                }
                else
                {
                    $messages .=$this->translate(
                            'Please fill all fields in the form',
                            'default', $this->getUserLanguage()->zend2_locale
                        ) . '.' . "\n"
                    ;
                    $errorCode = 2;
                }
            }
        }

        $layout = $this->layout();
        $layout->setTemplate('layout/lockscreen');
        $layout->page = new \Site\Custom\FlexibleContainer();
        $layout->page->title = $this->translate('Registration', 'default', $this->getUserLanguage()->zend2_locale);
        $layout->setVariables(
            array(
                'siteName' => $this->cmsConfig->site_name
            )
        );

        return new \Zend\View\Model\ViewModel(
            array(
                'errorCode' => $errorCode,
                'userId' => $userId,
                'messages' => $messages,
                'formTitle' => $layout->page->title,
                'provinces' => $provinces,
                'userLogin' => $userLogin,
                'userEmail' => $userEmail,
                'userName' => $userName,
                'userSurname' => $userSurname,
                'userPassword' => $newPassword,
                'userPassword2' => $newPassword2,
                'userBirthDate' => $userBirthDate,
                'userAccountTypeName'=> $userAccountTypeName
            )
        );
    }

    public function registrationExtraInformationAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            if ($request->isPost())
            {
                $userManager = $this->getUserManager();

                $userId = (int)$request->getPost('userId');
                $street = strip_tags((string)$request->getPost('street'));
                $houseNumber = strip_tags((string)$request->getPost('houseNumber'));
                $city = strip_tags((string)$request->getPost('city'));
                $postalCode = strip_tags((string)$request->getPost('postalCode'));
                $province = $request->getPost('province');
                $country = strip_tags((string)$request->getPost('originalCountry'));
                if(is_numeric($province))
                {
                    $province = (int)$province;
                }
                else
                {
                    $province = mb_strtolower($province,"UTF-8");
                    $allProvinces = $userManager->getProvinces();
                    foreach($allProvinces as $singleProvince)
                    {
                        if(strcmp($singleProvince->name, $province)==0)
                        {
                           $province = $singleProvince->province_id;
                        }
                    }
                }
                $phoneNumber = strip_tags($request->getPost('phone'));

                //Add To Database

                $user = $userManager->getUserById($userId);
                if($user->type_id == 4)
                {
                    $userFirmName = strip_tags((string)$request->getPost('firmName'));
                    $userManager->setUserFirmName($userId,$userFirmName);
                }

                if($user->type_id != 1)
                {
                    $existingDetails = $userManager->getUserContactInformationByUserId($userId);
                    if(!$existingDetails)
                    {
                        $userData = array();
                        $userData['userId'] = $userId;
                        $userData['provinceId'] = $province;
                        $userData['postalCode'] = $postalCode;
                        $userData['phoneNumber'] = $phoneNumber;
                        $userData['houseNumber'] = $houseNumber;
                        $userData['streetName'] = $street;
                        $userData['cityName'] = $city;
                        //$userData['originalCountry'] = $country;
                        if($user->type_id != 4)
                        {
                            $countrySaved = $userManager->setUserStudentCountry($userId,$country);
                            if(!$countrySaved)
                            {
                                $jsonResponse->error = 'COUNTRY_SAVE_FAILED';
                            }
                        }
                        $dataSaved = $userManager->createNewUserContactInformation($userData);

                        if($dataSaved)
                        {
                            $jsonResponse->meta->customStatus = 'INFO_ADDED';
                            $jsonResponse->meta->message = $this->translate('Information successfully added', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                        }
                        else
                        {
                            $jsonResponse->meta->customStatus = 'ADDING_INFO_FAIL';
                            $jsonResponse->meta->message = $this->translate('Error: Failed to add user information', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                        }
                    }
                    else
                    {
                        $jsonResponse->meta->customStatus = 'DATA_ALREADY_EXISTS';
                        $jsonResponse->meta->message = $this->translate('Error: Failed to add user information', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                    }

                }
                else
                {
                    //TRIED TO REGISTER AS ADMIN - BLOCK ACTION
                    $jsonResponse->meta->customStatus = 'DATA_REGISTRATION_BLOCKED';
                    $jsonResponse->meta->message = $this->translate('Error: Blocked attempt to register details as user-type: 1 . Power is earned not stolen.', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
                $this->response->setContent((string)$jsonResponse);
            }
            return $this->response;
        }
    }

    public function accountActivationAction()
    {
        $message = '';
        $activationCode = '';
        $user = null;

        $request = $this->getRequest();

        $parameters = $this->getParametersForCurrentPageFromRequest($request);
        $userManager = $this->getUserManager();

        $login = $parameters->account;
        $activationCode = $parameters->code;

        if (($login != '') && ($activationCode != ''))
        {
            $user = $userManager->getUserByLogin($login);
            if ($user && ($user->activation_code == $activationCode))
            {
                $login = $user->login;
            }
            else
            {
                $message = $this->translate(
                    'Error - account does not exists or wrong URL address was used',
                    'default', $this->getUserLanguage()->zend2_locale
                ) . '.';
            }
        }
        else
        {
            $layout = $this->layout();
            $layout->page->title =
                '404 - ' . $this->translate(
                    'page not found', 'default', $this->getUserLanguage()->zend2_locale
                );

            $response = $this->response;
            $response->getHeaders()->addHeaderLine('Location', $request->getBaseUrl() . '/404');
            $response->setStatusCode(404);

            return new \Zend\View\Model\ViewModel();
        }

        if (($login != '') && $user)
        {
            $accountActivated = $userManager->activateUserAccountByUserId($user->user_id);
            if ($accountActivated)
            {
                $message = $this->translate(
                    'Account has been successfully activated',
                    'default', $this->getUserLanguage()->zend2_locale
                ) . '.'
                ;
            }
            else
            {
                $message = $this->translate(
                    'Account has not been successfully activated, please contact us, providing us with your username and e-mail address.',
                    'default', $this->getUserLanguage()->zend2_locale
                ) . '.'
                ;
            }
        }

        $layout = $this->layout();
        $layout->setTemplate('layout/lockscreen');
        $layout->page = new \Site\Custom\FlexibleContainer();
        $layout->page->title = $this->translate('Account activation', 'default', $this->getUserLanguage()->zend2_locale);
        $layout->setVariables(
            array(
                'siteName' => $this->cmsConfig->site_name
            )
        );

        return new \Zend\View\Model\ViewModel(
            array(
                'message' => $message
            )
        );
    }

    protected function createNewOrUpdateExistingUser($formData)
    {
        $formData = new \Site\Custom\FlexibleContainer($formData);

        if ($formData->userId > 0)
        {
            return $this->getUserManager()->updateExistingUser($formData);
        }
        else
        {
            return $this->getUserManager()->createNewUser($formData);
        }
    }
}
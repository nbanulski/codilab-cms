<?php
namespace Site\Controller;

class SettingsController extends BaseController
{
    const SETTINGS_TABLE = 'settings';

    public function getViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $config = $this->getServiceLocator()->get('config');
                $cmsConfig = new \Site\Custom\FlexibleContainer($config['cms']);
                $languages = $this->getLanguageManager()->getAllLanguages();

                $systemSettings = new \Site\Custom\FlexibleContainer();
                $settings = $this->getSystemSettings();
                if ($settings)
                {
                    foreach ($settings as $name => $data)
                    {
                        $systemSettings[$name] = $data->value;
                    }
                }

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $pagesView = new \Zend\View\Model\ViewModel();
                $pagesView->setTemplate('site/admin/settings.phtml');
                $pagesView->setVariables(
                    array(
                        'userLanguage' => $this->getUserLanguage(),
                        'cmsConfig' => $cmsConfig,
                        'languages' => $languages,
                        'systemSettings' => $systemSettings
                    )
                );

                $jsonResponse->data = $phpRenderer->render($pagesView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function saveAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $formData = $request->getPost('formData');

                $saved = $this->saveGlobalConfig($formData);
                $saved = $saved && $this->saveSystemSettings($formData);

                if ($saved)
                {
                    $jsonResponse->data = $this->translate('Settings successfully saved', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                    $jsonResponse->meta->customStatus = 'SETTINGS_SAVED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to save settings', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    protected function saveGlobalConfig($formData)
    {
        $globalConfigSaved = false;

        try
        {
            // Empty config:

            $emptyGlobalConfig = array(
                'cms' => array()
            );
            $emptyGlobalConfig = new \Site\Custom\FlexibleContainer($emptyGlobalConfig);

            // Current config from "global.php" file:

            $globalConfig = require('config/autoload/global.php');
            $currentGlobalConfig = new \Site\Custom\FlexibleContainer($globalConfig);

            // New config (may contain only changed settings - not all!):

            $newGlobalConfig = $emptyGlobalConfig;
            $newGlobalConfig['cms'] = $formData['settings']['global']['cms'];
            $newGlobalConfig = new \Site\Custom\FlexibleContainer($newGlobalConfig);

            // Merged config (current global config with applied changes):

            $mergedGlobalConfig = $currentGlobalConfig->merge($newGlobalConfig);

            // Global config array which is ready for save into file "global.php":

            $globalConfigReadyForSave = $mergedGlobalConfig->toArray(true);

            $globalConfigSaved = $this->saveGlobalConfigIntoFile($globalConfigReadyForSave);
        }
        catch (\Exception $e)
        {

        }

        return $globalConfigSaved;
    }

    protected function saveSystemSettings($formData)
    {
        $formData = new \Site\Custom\FlexibleContainer($formData);

        if (!$formData->systemSettings)
        {
            return true;
        }

        $saved = true;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        foreach ($formData->systemSettings as $name => $value)
        {
            $value = trim((string)$value);

            $update = $sql->update(self::SETTINGS_TABLE);
            $update
                ->set(
                    array(
                        'value' => $value
                    )
                )
                ->where(
                    array(
                        'name' => $name
                    )
                );
            $result = $dbManager->executePreparedStatement($update);
            if (!$result || ($this->getSystemSettingValueByName($name, false) != $value))
            {
                $saved = false;
                break;
            }
        }

        if ($saved)
        {
            $dbManager->commit();

            return true;
        }

        $dbManager->rollback();

        return false;
    }

    protected function getSettingByName($name)
    {
        $name = (string)$name;
        if ($name == '')
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::SETTINGS_TABLE)
            ->columns(array('value'), $prefixColumnsWithTable)
            ->where(array('name' => $name));
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();

            return $row ? $row->value : '';
        }

        return false;
    }

    protected function saveGlobalConfigIntoFile($array)
    {
        if (!is_array($array))
        {
            return false;
        }

        $fileContent =
        '<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
return ';

        $fileContent .= $this->convertArrayToPhpCode($array) . ';';

        return @file_put_contents('config/autoload/global.php', $fileContent) ? true : false;
    }

    protected function convertArrayToPhpCode($array, $depth = 1)
    {
        $indentationCharacters = '    ';
        $indent = str_pad('', $depth * 4, $indentationCharacters);

        $phpCode = 'array(' . "\n";
        $isFirstElement = true;

        foreach ($array as $key => $value)
        {
            if (!$isFirstElement)
            {
                $phpCode .= ",\n";
            }

            $phpCode .= $indent . "'" . $this->escapeValue($key) . "' => ";

            if ($value && (is_array($value) || ($value instanceof \Traversable)))
            {
                $phpCode .= $this->convertArrayToPhpCode($value, $depth + 1);
            }
            else
            {
                $phpCode .= "'" . $this->escapeValue($value) . "'";
            }

            $isFirstElement = false;
        }

        $indent = str_pad('', ($depth - 1) * 4, $indentationCharacters);

        $phpCode .= "\n" . $indent . ')';

        return $phpCode;
    }

    protected function escapeValue($value)
    {
        return str_replace("'", "\'", $value);
    }
}
<?php
namespace Site\Controller;

use Zend\View\Model\ViewModel;

class BlockAccordionController extends BaseController
{
    public function addEditPanelAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                if ($request->isPost())
                {
                    $pageBlockId = (int)$request->getPost('pageBlockId');
                    $blockManager = $this->getBlockManager();
                    $accordionBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                    if ($accordionBlock instanceof \Site\Block\Accordion\Accordion)
                    {
                        $panelData = new \Site\Custom\FlexibleContainer();
                        $panelCount = $accordionBlock->getPanelCount();
                        $maxOrder = $accordionBlock->getMaxPanelsOrder();
                        $panelData->panelOrder = min($maxOrder + 1, 65535);
                        $panelData->panelName .=
                            $this->translate('New panel', 'default', $this->userLanguage->zend2_locale) .
                            ' (' . ++$panelCount . ')'
                        ;
                        $panelId = $accordionBlock->addOrEditPanel($panelData);

                        if ($panelId > 0)
                        {
                            $customStatus = 'PANEL_ADDED';
                        }
                        else
                        {
                            $customStatus = 'PANEL_ADDING_ERROR';
                        }

                        $jsonResponse->meta->customStatus = $customStatus;
                        $jsonResponse->data = $panelId;
                        $this->response->setContent((string)$jsonResponse);

                        return $this->response;
                    }
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function deletePanelAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $deleted = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $panelId = $request->getPost('panelId');
                $blockManager = $this->getBlockManager();
                $accordionBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($accordionBlock instanceof \Site\Block\Accordion\Accordion)
                {
                    $deleted = $accordionBlock->deletePanelById($panelId);
                }

                if ($deleted)
                {
                    $jsonResponse->meta->customStatus = 'PANEL_DELETED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'PANEL_DELETION_ERROR';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }
    
    public function updatePanelNameAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $updated = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $panelId = $request->getPost('panelId');
                $panelName = $request->getPost('panelName');

                $blockManager = $this->getBlockManager();
                $accordionBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($accordionBlock instanceof \Site\Block\Accordion\Accordion)
                {
                    $updated = $accordionBlock->updatePanelName($panelId, $panelName);
                }

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'PANEL_NAME_UPDATED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to update panel name', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updatePanelContentsAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $updated = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $panelId = $request->getPost('panelId');
                $panelContents = $request->getPost('panelContents');

                $blockManager = $this->getBlockManager();
                $accordionBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($accordionBlock instanceof \Site\Block\Accordion\Accordion)
                {
                    $updated = $accordionBlock->updatePanelContents($panelId, $panelContents);
                }

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'PANEL_CONTENTS_UPDATED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to update panel contents', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function reorderPanelsAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $reorderingSuccessful = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $panels = $request->getPost('panels');

                $blockManager = $this->getBlockManager();
                $accordionBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($accordionBlock instanceof \Site\Block\Accordion\Accordion)
                {
                    $reorderingSuccessful = $accordionBlock->reorderPanels($panels);
                }

                if ($reorderingSuccessful)
                {
                    $jsonResponse->meta->customStatus = 'ACCORDION_REORDERED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'ACCORDION_REORDERING_ERROR';
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to reorder panels in accordion', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }
}

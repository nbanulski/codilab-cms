<?php
namespace Site\Controller;

class DialogController extends BaseController
{
    public function getTemplateAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            if ($request->isPost())
            {
                $templateType = strtolower(trim($request->getPost('templateType')));
                if (!in_array($templateType, array('standard', 'confirm')))
                {
                    $templateType = 'standard';
                }
                $withSaveButton = $request->getPost('withSaveButton') ? true : false;
                $id = $this->escapeForHtml(trim($request->getPost('id')));
                $title = $this->escapeForHtml(trim($request->getPost('title')));
                $contents = trim($request->getPost('contents'));

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');

                $dialogView = new \Zend\View\Model\ViewModel();
                $dialogView->setTemplate('partial/dialog-' . $templateType);
                $dialogView->setVariables(
                    array(
                        'userLanguage' => $this->getUserLanguage(),
                        'modalId' => $id,
                        'modalTitle' => $title,
                        'modalContents' => $contents,
                        'modalWithSaveButton' => $withSaveButton
                    )
                );

                $jsonResponse->data = $phpRenderer->render($dialogView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getFastLoginFormViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            if ($request->isPost())
            {
                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');

                $fastLoginFormView = new \Zend\View\Model\ViewModel();
                $fastLoginFormView->setTemplate('partial/fast-login-form');

                $jsonResponse->data = $phpRenderer->render($fastLoginFormView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }
}
<?php
namespace Site\Controller;

class LanguageController extends BaseController
{
    public function switchPageLanguageAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            if ($request->isPost())
            {
                $languageManager = $this->getLanguageManager();

                $languageIsoCode = strtolower(trim($request->getPost('languageIsoCode')));
                $parentPageCleanUrl = strtolower(trim($request->getPost('parentPageCleanUrl')));
                $subPageCleanUrl = strtolower(trim($request->getPost('subPageCleanUrl')));
                $parameters = strtolower(trim($request->getPost('parameters')));

                $uri = $this->getRequest()->getUri();
                $base = sprintf('%s://%s', $uri->getScheme(), $uri->getHost()) . '/';
                $newUrlStart = $base . $languageIsoCode;

                $newLanguage = $languageManager->getLanguageByIsoCode($languageIsoCode);

                $newUrlFragment = $this->getNewUrlFragment($parentPageCleanUrl, $subPageCleanUrl, $newLanguage);
                if ($newUrlFragment != '')
                {
                    $newUrlFragment .= ',' . $parameters;
                }

                $jsonResponse->data = $newUrlStart . $newUrlFragment;
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function switchUserLanguageAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($request->isPost() && $user && ($user->account->role != 'guest'))
            {
                $languageChanged = false;
                $languageManager = $this->getLanguageManager();

                $languageIsoCode = strtolower(trim($request->getPost('languageIsoCode')));
                $language = $languageManager->getLanguageByIsoCode($languageIsoCode);
                if ($language)
                {
                    $user->session->userLanguage = new \Site\Custom\FlexibleContainer($language);

                    $auth = $this->serviceLocator->get('AuthService');
                    $storage = $auth->getStorage();
                    $storage->write($user->toStdClass());

                    $languageChanged = true;
                }

                if ($languageChanged)
                {
                    $jsonResponse->meta->customStatus = 'USER_LANGUAGE_CHANGED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'USER_LANGUAGE_CHANGING_ERROR';
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to change user language', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    protected function getNewUrlFragment($parentPageCleanUrl, $subPageCleanUrl, $newLanguage)
    {
        if ($subPageCleanUrl != '')
        {
            return $this->getNewUrlFragmentForSubPage($parentPageCleanUrl, $subPageCleanUrl, $newLanguage);
        }

        return $this->getNewUrlFragmentForParentPage($parentPageCleanUrl, $newLanguage);
    }

    private function getNewUrlFragmentForParentPage($parentPageCleanUrl, $newLanguage)
    {
        $pageManager = $this->getPageManager();

        $page = $pageManager->getPageByParentIdAndCleanUrl(null, $parentPageCleanUrl);
        if (!$page)
        {
            return false;
        }

        $pageId = (int)$page->page_id;
        $newLanguageId = (int)$newLanguage->language_id;

        $pageEquivalent = $pageManager->getPageLanguageEquivalentByPageIdAndLanguageId($pageId, $newLanguageId);
        if (!$pageEquivalent)
        {
            return false;
        }

        $newPage = $pageManager->getPageById($pageEquivalent->associated_page_id);
        if (!$newPage)
        {
            return false;
        }

        return '/page/' . $newPage->clean_url;
    }

    private function getNewUrlFragmentForSubPage($parentPageCleanUrl, $subPageCleanUrl, $newLanguage)
    {
        $pageManager = $this->getPageManager();

        $page = $pageManager->getPageByParentIdAndCleanUrl(null, $parentPageCleanUrl);
        $subPage = $pageManager->getPageByParentIdAndCleanUrl($page->parent_id, $subPageCleanUrl);

        if (!$page || !$subPage)
        {
            return false;
        }

        $pageId = (int)$page->page_id;
        $subPageId = (int)$subPage->page_id;
        $newLanguageId = (int)$newLanguage->language_id;

        $pageEquivalent = $pageManager->getPageLanguageEquivalentByPageIdAndLanguageId($pageId, $newLanguageId);
        $subPageEquivalent = $pageManager->getPageLanguageEquivalentByPageIdAndLanguageId($subPageId, $newLanguageId);
        if (!$pageEquivalent || !$subPageEquivalent)
        {
            return false;
        }

        $newPage = $pageManager->getPageById($pageEquivalent->associated_page_id);
        $newSubPage = $pageManager->getPageById($subPageEquivalent->associated_page_id);
        if (!$newPage || !$newSubPage)
        {
            return false;
        }

        return '/page/' . $newPage->clean_url . '/' . $newSubPage->clean_url;
    }
}
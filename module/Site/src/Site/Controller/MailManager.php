<?php
namespace Site\Controller;

use Zend\Mvc\Controller\AbstractController;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Mvc\MvcEvent;
use Site\Custom\FlexibleContainer;

class MailManager extends AbstractController
{
    protected $cmsConfig = null;
    protected $serviceLocator = null;
    protected $language = null;
    protected $userLanguage = null;
    protected $identity = null;
    protected $defaultLanguageIsoCode = '';
    protected $senderAddress = '';
    protected $senderHost = '127.0.0.1';
    protected $senderServerName = '';
    protected $senderLogin = '';
    protected $senderPassword = '';
    protected $senderPort = '';
    protected $senderSsl = '';
    protected $senderConnectionClass = '';
    protected $senderTitle = '';
    protected $recipientAddress = '';
    protected $encoding = 'UTF-8';
    protected $textContents = false;
    protected $htmlContents = false;
    protected $subject = '';
    protected $transport = null;

    public function __construct(
        $cmsConfig, ServiceLocatorInterface $serviceLocator,
        \stdClass $language, FlexibleContainer $userLanguage, $identity
    )
    {
        $this->cmsConfig = new \Site\Custom\FlexibleContainer($cmsConfig);
        $this->serviceLocator = $serviceLocator;
        $this->language = $language;
        $this->userLanguage = $userLanguage;
        $this->identity = $identity;

        $this->setDataFromCmsConfig($cmsConfig);
        $this->useDefaultTransport();
    }

    public function onDispatch(MvcEvent $e)
    {

    }

    public function setDataFromCmsConfig($cmsConfig)
    {
        $cmsConfig = new \Site\Custom\FlexibleContainer($cmsConfig);

        $this->defaultLanguageIsoCode = $cmsConfig->default_language_iso_code;
        $this->senderAddress = $cmsConfig->system_email_sender_address;
        $this->senderHost = $cmsConfig->system_email_sender_host;
        $this->senderServerName = $cmsConfig->system_email_sender_server_name;
        $this->senderLogin = $cmsConfig->system_email_sender_login;
        $this->senderPassword = $cmsConfig->system_email_sender_password;
        $this->senderPort = (int)$cmsConfig->system_email_sender_port;
        $this->senderSsl = $cmsConfig->system_email_sender_ssl;
        $this->senderConnectionClass = trim($cmsConfig->system_email_sender_connection_class);
        $this->senderTitle = $cmsConfig->system_email_sender_title;

        return $this;
    }

    public function useDefaultTransport()
    {
        // Sendmail transport:
        $this->transport = new \Zend\Mail\Transport\Sendmail();

        // SMTP transport:
        /*
        $this->transport = new \Zend\Mail\Transport\Smtp();
        $options = new \Zend\Mail\Transport\SmtpOptions(
            array(
                'name' => $this->senderServerName,
                'host' => $this->senderHost,
                'connection_class' => $this->senderConnectionClass,
                'connection_config' => array(
                    'ssl' => $this->senderSsl,
                    'username' => $this->senderLogin,
                    'password' => $this->senderPassword
                ),
                'port' => $this->senderPort
            )
        );
        $this->transport->setOptions($options);
        */

        // File transport:
        /*
          $transport = new \Zend\Mail\Transport\File();
          $options = new \Zend\Mail\Transport\FileOptions(array(
          'path' => 'data/mail/',
          'callback' => function (\Zend\Mail\Transport\File $transport)
          {
          return 'Message_' . microtime(true) . '_' . mt_rand() . '.txt';
          },
          ));
          $transport->setOptions($options);
         */

        return $this;
    }

    public function getTransport()
    {
        return $this->transport;
    }

    public function setTransport(\Zend\Mail\Transport\TransportInterface $transport)
    {
        $this->transport = $transport;

        return $this;
    }

    public function getEncoding()
    {
        return $this->encoding;
    }

    public function setEncoding($encoding)
    {
        $this->encoding = trim((string)$encoding);

        return $this;
    }

    public function getRecipientAddress()
    {
        return $this->recipientAddress;
    }

    public function setRecipientAddress($email)
    {
        $this->recipientAddress = trim((string)$email);

        return $this;
    }

    public function getTextContents()
    {
        return $this->textContents;
    }

    public function setTextContents($content)
    {
        $this->textContents = ($content !== false) ? trim((string)$content) : false;

        return $this;
    }

    public function getHtmlContents()
    {
        return $this->htmlContents;
    }

    public function setHtmlContents($content)
    {
        $this->htmlContents = ($content !== false) ? trim((string)$content) : false;

        return $this;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function setSubject($subject)
    {
        $this->subject = trim((string)$subject);

        return $this;
    }

    public function send()
    {
        $body = new \Zend\Mime\Message();

        if ($this->textContents !== false)
        {
            $text = new \Zend\Mime\Part($this->textContents);
            $text->type = \Zend\Mime\Mime::TYPE_TEXT . '; charset=' . $this->encoding;

            $body->addPart($text);
        }

        if ($this->htmlContents !== false)
        {
            $html = new \Zend\Mime\Part($this->htmlContents);
            $html->type = \Zend\Mime\Mime::TYPE_HTML . '; charset=' . $this->encoding;

            $body->addPart($html);
        }

        $mail = new \Zend\Mail\Message();
        $mail
            ->addFrom($this->senderAddress, $this->senderTitle)
            ->addTo($this->recipientAddress)
            ->setSender($this->senderAddress, $this->senderTitle)
            ->setSubject($this->subject)
            ->setEncoding($this->encoding)
            ->setBody($body)
        ;

        if (($this->htmlContents != false) && ($this->textContents != false)) // Both parts are used.
        {
            $mail->getHeaders()->get('Content-Type')->setType('multipart/alternative');
        }

        // Sending using defined transport type:
        $this->transport->send($mail);
    }
}
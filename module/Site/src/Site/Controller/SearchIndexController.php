<?php
namespace Site\Controller;

/**
 * Class SearchIndexController
 * @package Site\Controller
 * This controller is not working in command line yet! Do not use it in console!
 */

class SearchIndexController extends BaseController
{
    protected $requestSourceDetermined = false;
    protected $requestSource = null;
    protected $newLine = "\n";
    protected $infoFilePath = 'data/logs/search-index-update.log';
    protected $lockFile = 'data/logs/search-index-update.lock';
    protected $stopRequestFile = 'data/logs/search-index-update.stop';

    public function indexAction()
    {
        return new ViewModel();
    }

    public function updateAction()
    {
        $this->determineRequestSourceAndSetProperNewLineString();
        $request = $this->getRequest();

        // Check verbose flag:
        if ($this->requestSource == 'console')
        {
            $verbose = $request->getParam('verbose') || $request->getParam('v');
            $mode = $request->getParam('mode', 'info');
        }
        else
        {
            $verbose = $request->getQuery('verbose') || $request->getQuery('v');
            $mode = $request->getQuery('mode', 'info');
        }

        switch ($mode)
        {
            case 'start':
                return $this->startUpdate($verbose);
                break;
            case 'stop':
                return $this->stopUpdate($verbose);
                break;
            case 'info':
            default:
                return $this->getInfo($verbose);
                break;
        }
    }

    public function getInfo($verbose = false)
    {
        $fileSystem = $this->getFileSystem();

        if ($fileSystem->isFile($this->infoFilePath))
        {
            return file_get_contents($this->infoFilePath);
        }

        return $this->getResponse()->setContent('No indexing is in progress.' . $this->newLine);
    }

    public function startUpdate($verbose = false)
    {
        $fileSystem = $this->getFileSystem();

        $response = array();

        if ($this->isSearchIndexUpdateInProgress())
        {
            return $this->getResponse()->setContent(
                'Cannot start indexing because the previous is still in progress.' . $this->newLine
            );
        }

        $fileSystem->remove($this->stopRequestFile);
        $fileSystem->touch($this->lockFile);

        $response[] = 'Date: ' . date('Y-m-d H:i:s');

        @ini_set('memory_limit', '512M');

        if ($verbose)
        {
            $response[] = 'Reserved memory: ' . ini_get('memory_limit');
        }

        $blockManager = $this->getBlockManager();
        $blocks = $this->getAllBlocks();
        $blockCount = count($blocks);
        $processedBlockCount = 0;
        $failedBlockCount = 0;

        if ($blocks)
        {
            //$this->saveInfoAboutUpdateToFile($blockCount, $processedBlockCount, $failedBlockCount);

            foreach ($blocks as $contentId)
            {
                $blockInstance = $blockManager->createBlockUsingContentId($contentId);
                if ($blockInstance)
                {
                    $blockData = $blockInstance->getBlockData();
                    $pageBlockId = $blockData->page_block_id;

                    try
                    {
                        $blockManager->callPageBlockCustomMethod($pageBlockId, 'updateSearchIndex');
                    }
                    catch (\Site\Exception\BlockHasNoMethodException $e)
                    {
                        //$failedBlockCount++;
                    }
                }
                else
                {
                    //echo 'Some block failed. Content id: ' . $contentId . "<br>\n<br>\n";
                    //var_dump($contentId);

                    $failedBlockCount++;
                }

                $processedBlockCount++;

                //$this->saveInfoAboutUpdateToFile($blockCount, $processedBlockCount, $failedBlockCount);

                if (is_file($this->stopRequestFile))
                {
                    $response[] = "Indexing stopped by user's request.";

                    break;
                }
            }
        }

        $fileSystem->remove($this->infoFilePath);
        $fileSystem->remove($this->lockFile);

        if ($verbose)
        {
            $response[] = 'Number of blocks to index: ' . count($blocks);
            $response[] = 'Number of processed blocks: ' . $processedBlockCount;
            $response[] = 'Number of failed updates: ' . $failedBlockCount;
        }

        $response[] = 'Finished';

        return $this->getResponse()->setContent(join($this->newLine, $response));
    }

    public function stopUpdate($verbose = false)
    {
        $fileSystem = $this->getFileSystem();

        if (!$this->isSearchIndexUpdateInProgress())
        {
            return $this->getResponse()->setContent(
                'There are no currently active search indexing.' . $this->newLine
            );
        }

        $fileSystem->touch($this->stopRequestFile);
    }

    protected function isSearchIndexUpdateInProgress()
    {
        if (is_file($this->lockFile))
        {
            $lastModification = filemtime($this->lockFile);
            if (($lastModification !== false) && ($lastModification < time() - 86400))
            {
                return true;
            }
        }

        return false;
    }

    protected function determineRequestSourceAndSetProperNewLineString()
    {
        if (!$this->requestSourceDetermined)
        {
            $request = $this->getRequest();

            if ($request instanceof ConsoleRequest)
            {
                $this->requestSource = 'console';
            }
            else
            {
                $this->requestSource = 'http';
            }

            $this->requestSourceDetermined = true;
        }

        if ($this->requestSource == 'http')
        {
            $this->newLine = "<br>\n";
        }
    }

    protected function getAllBlocks()
    {
        $prefixColumnsWithTable = true;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(array('pb' => 'pages_blocks'))
            ->columns(array(), $prefixColumnsWithTable)
            ->join(
                array('pbca' => 'pages_blocks_contents_association'),
                'pbca.page_block_id = pb.page_block_id',
                array('content_id'),
                $select::JOIN_LEFT
            )
            ->join(
                array('b' => 'blocks'),
                'b.block_id = pb.block_id',
                array(),
                $select::JOIN_LEFT
            )
            ->where(array('pb.is_parent_block_dynamic' => 0, 'b.has_data_in_search_index' => 1))
            ->group('pbca.content_id');

        //echo $sql->getSqlStringForSqlObject($select);

        $blocksResultSet = $dbManager->getHydratedResultSet($select);

        $select2 = $sql->select();
        $select2
            ->from(array('b' => 'blocks'))
            ->columns(
                array(
                    'content_id' => 'shared_content_id'
                ), $prefixColumnsWithTable
            )
            ->where(array('b.is_enabled' => 1, 'b.is_dynamic' => 1, 'b.has_data_in_search_index' => 1))
            ->group('b.block_id');

        //echo $sql->getSqlStringForSqlObject($select2);

        $dynamicBlocksResultSet = $dbManager->getHydratedResultSet($select2);

        $blocks = array();

        if ($dynamicBlocksResultSet)
        {
            foreach ($dynamicBlocksResultSet as $block)
            {
                $block = (array)$block;
                if (!in_array($block, $blocks))
                {
                    $blocks[] = $block['content_id'];
                }
            }
        }

        if ($blocksResultSet)
        {
            foreach ($blocksResultSet as $block)
            {
                $block = (array)$block;
                if (!in_array($block, $blocks))
                {
                    $blocks[] = $block['content_id'];
                }
            }
        }

        if ($blocks)
        {
            //$blocks = array_unique($blocks, SORT_STRING);

            sort($blocks);
        }

        return $blocks;
    }

    /*protected function saveInfoAboutUpdateToFile($blockCount, $processedBlockCount, $failedBlockCount)
    {
        $data =
            'Processed: ' . $processedBlockCount . ' (' . $failedBlockCount . ' failed) from ' . $blockCount . ' total blocks.' . "\n"
        ;

        file_put_contents($this->infoFilePath, $data);
    }*/
}
<?php
namespace Site\Controller;

use Zend\Mvc\MvcEvent;

class PageController extends BaseController
{
    public function onDispatch(MvcEvent $e)
    {
        $page = $this->getPageObjectFromRequestUrl();
        if ($page)
        {
            $user = $this->identity();
            $logged = ($user && ($user->account)) ? true : false;
            $loggedAsAdmin = $logged && ($user->account->role == 'admin');
            $designMode = ($loggedAsAdmin && $user->session->designMode);

            $pageManager = $this->getPageManager();
            $blockManager = $this->getBlockManager();
            $languageManager = $this->getLanguageManager();
            $currentUserInterfaceLanguage = $this->getUserLanguage();
            $pageLanguage = $languageManager->getLanguageByLanguageId($page->language_id);

            $pageLayout = $pageManager->getLayoutById($page->layout_id);
            $pageSubLayout = $pageManager->getSubLayoutById($page->sub_layout_id);
            $pageBlocks = $pageManager->getPageBlocksByPageId($page->page_id);
            $blocks = $pageManager->makePageBlockArrayUsingPageBlocks($pageBlocks);

            $blockList = null;
            $dynamicBlockList = null;
            if ($loggedAsAdmin && $designMode)
            {
                list($blockList, $dynamicBlockList) =
                    $blockManager->getAllBlocks($currentUserInterfaceLanguage->language_id);
                if ($blockList)
                {
                    $blockList = $blockList->buffer();
                }
                if ($dynamicBlockList)
                {
                    $dynamicBlockList = $dynamicBlockList->buffer();
                }
            }

            $blockListLanguages = array(
                '' => $this->translate('All languages', 'default', $this->getUserLanguage()->zend2_locale)
            );
            $allLanguages = $languageManager->getAllLanguages();
            if ($allLanguages)
            {
                foreach ($allLanguages as $language)
                {
                    $languageId = (int)$language->language_id;
                    $blockListLanguages[$languageId] = $language->english_name;
                }
            }

            $blockListCategories = array(
                '' => $this->translate('All blocks', 'default', $this->getUserLanguage()->zend2_locale)
            );
            $allSystemBlockNames = $blockManager->getAllSystemBlockNames(
                $currentUserInterfaceLanguage->language_id
            );
            if ($allSystemBlockNames)
            {
                foreach ($allSystemBlockNames as $blockInfo)
                {
                    $phpClassName = trim($blockInfo->php_class_name);
                    if ($phpClassName != '')
                    {
                        $blockListCategories[$phpClassName] = $blockInfo->name;
                    }
                }
            }

            $cookies = $e->getRequest()->getCookie();
            $messageAboutCookiesAccepted = property_exists($cookies, 'message_about_cookies_accepted');

            $showMessageAboutCookies = $this->getSystemSettingValueByName('show_message_about_cookies') ? true : false;
            $messageAboutCookiesPopupPosition = 'bottom';
            $messageAboutCookies = false;
            if ($showMessageAboutCookies)
            {
                $messageAboutCookiesPopupPosition =
                    $this->getSystemSettingValueByName('message_about_cookies_popup_position');
                $messageAboutCookies =
                    $this->getSystemSettingValueByName('message_about_cookies_' . $pageLanguage->iso_code);
            }

            $additionalScripts = '';
            $additionalScriptsSetting = $this->getSystemSettingByName('additional_scripts');
            if ($additionalScriptsSetting)
            {
                $additionalScripts = $additionalScriptsSetting->value;
            }

            $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');

            $view = new \Zend\View\Model\ViewModel();
            $view->setTemplate('layout/sub-layout/' . $pageSubLayout->file_path);
            $view->setVariables(
                array(
                    'blocks' => $blocks
                )
            );

            $layout = $e->getViewModel();
            $layout->setTemplate('layout/' . $pageLayout->file_path);
            $layout->setVariables(
                array(
                    'page' => $page,
                    'blocks' => $blocks,
                    // Two variables needed in div#admin-toolwnd:
                    'blockListLanguages' => $blockListLanguages,
                    'blockListCategories' => $blockListCategories,
                    'blockList' => $blockList,
                    'dynamicBlockList' => $dynamicBlockList,
                    'content' => $phpRenderer->render($view),
                    'messageAboutCookiesAccepted' => $messageAboutCookiesAccepted,
                    'showMessageAboutCookies' => $showMessageAboutCookies,
                    'messageAboutCookiesPopupPosition' => $messageAboutCookiesPopupPosition,
                    'messageAboutCookies' => $messageAboutCookies,
                    'additionalScripts' => $additionalScripts
                )
            );

            $response = $this->response;
            $response->setContent($phpRenderer->render($layout));

            $session = $this->sessionData();
            if (!$session || ($session->account->role == 'guest') || ($session->account->role == 'member'))
            {
                $pageManager->incrementPageViewCounter($page->page_id);
            }

            return $response;
        }

        /**
         *  TODO: Instead of this - redirect user to some error page, for example - 404 or show message like
         *  "page cannot be loaded because of server error". Currently ZF2 error will be shown.
         */
        $response = $e->getResponse();
        $response->getHeaders()->addHeaderLine('Location', $e->getRequest()->getBaseUrl() . '/404');
        $response->setStatusCode(404);

        return parent::onDispatch($e);
    }

    protected function getPageObjectFromRequestUrl()
    {
        $pageManager = $this->getPageManager();

        $actionName = mb_strtolower($this->params('action'));
        $cleanUrl = ($actionName != 'index') ? $actionName : '';
        $subPageCleanUrl = $this->getSubPageCleanUrlFromRequest($this->getRequest());

        $page = $pageManager->getPageByParentIdAndCleanUrl(null, $cleanUrl);
        if (!$page)
        {
            return false;
        }

        if ($subPageCleanUrl != '')
        {
            $subPage = $pageManager->getPageByParentIdAndCleanUrl($page->page_id, $subPageCleanUrl);
            if ($subPage)
            {
                return $subPage;
            }
        }

        return $page;
    }
}
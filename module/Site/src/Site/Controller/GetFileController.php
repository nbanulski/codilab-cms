<?php
namespace Site\Controller;

class GetFileController extends BaseController
{
    public function indexAction()
    {
        list($uniqueHashForFileName, $cropCoords) = $this->getUniqueHashForFileNameAndCropCoordsFromRouteParameters();
        if (!$uniqueHashForFileName)
        {
            return $this->responseError404();
        }

        $attachmentManager = $this->getAttachmentManager();
        $attachment = $attachmentManager->getAttachmentByUniqueHashForFileName($uniqueHashForFileName);
        if (!$attachment)
        {
            return $this->responseError404();
        }

        $filePath = $attachment->file_path;
        $fileSystem = new \FileSystem\Component\FileSystem();
        if (!$fileSystem->isFile($filePath))
        {
            return $this->responseError404();
        }

        $fileInfo = array(
            'mimeType' => $attachment->mime_type,
            'fileExtension' => $attachment->extension,
            'fileSize' => $attachment->file_size,
            'filePath' => $filePath,
            'cropCoords' => $cropCoords
        );

        return $this->sendImageToBrowserUsingFileInfo($fileInfo);
    }

    public function tempAction()
    {
        $parameters = $this->getParametersForCurrentPageFromRequest($this->getRequest());
        $fileName = $parameters->key();

        if ($fileName == '')
        {
            return $this->responseError404();
        }

        $filePath = 'data/temp/' . $fileName;
        $fileSystem = new \FileSystem\Component\FileSystem();
        if (!$fileSystem->isFile($filePath))
        {
            return $this->responseError404();
        }

        $fileExtension = $fileSystem->getFileExtension($filePath);
        $mimeType = $this->getMimeTypeFromFileExtension($fileExtension);
        $fileSize = $fileSystem->filesize($filePath);

        $fileInfo = array(
            'mimeType' => $mimeType,
            'fileSize' => $fileSize,
            'filePath' => $filePath
        );

        return $this->sendImageToBrowserUsingFileInfo($fileInfo);
    }

    protected function getMimeTypeFromFileExtension($fileExtension)
    {
        $fileExtension = mb_strtolower(trim($fileExtension));

        switch ($fileExtension)
        {
            case 'csv': return 'text/csv';
            case 'xls': return 'application/vnd.ms-excel';
            default: return 'unknown';
        }
    }

    protected function sendImageToBrowserUsingFileInfo($fileInfo)
    {
        $response = $this->getResponse();

        $responseHeaders = $response->getHeaders();
        $responseHeaders->addHeaderLine('Content-Type', $fileInfo['mimeType']);

        // Try using optimized method for outputting file:
        if ($this->isApacheXSendFileModuleEnabled())
        {
            $filePathRelativeToWhitelistedXSendFileModuleAbsoluteDirectoryPath = '../../' . $fileInfo['filePath'];
            $responseHeaders->addHeaderLine('X-Sendfile', $filePathRelativeToWhitelistedXSendFileModuleAbsoluteDirectoryPath);

            return $response;
        }

        // Use standard method for outputting file:
        $responseHeaders->addHeaderLine('Content-Length', $fileInfo['fileSize']);
        $response->setContent(file_get_contents($fileInfo['filePath']));

        return $response;
    }

    protected function getUniqueHashForFileNameAndCropCoordsFromRouteParameters()
    {
        $parameters = $this->getParametersForCurrentPageFromRequest($this->getRequest());
        if (!$parameters)
        {
            return false;
        }

        $uniqueHashForFileName = $parameters->key(); // Get name of the first parameter.
        //$fileName = $parameters->current(); // Get value of the first parameter.
        $cropCoordsString = $parameters->crop;
        $cropCoords = array();

        if ($cropCoordsString != '')
        {
            $imageManipulator = $this->getImageManipulator();
            $cropCoords = $imageManipulator->convertRectCoordsStringToArray($cropCoordsString);
        }

        return array($uniqueHashForFileName, $cropCoords);
    }

    protected function responseError404()
    {
        $response = $this->getResponse();
        //$response->getHeaders()->addHeaderLine('Location', $e->getRequest()->getBaseUrl() . '/404');
        $response->setStatusCode(404);

        //return $response;
    }

    protected function isApacheXSendFileModuleEnabled()
    {
        if (function_exists('apache_get_modules'))
        {
            $modules = apache_get_modules();

            return array_search('mod_xsendfile', $modules) !== false;
        }

        return false;
    }
}
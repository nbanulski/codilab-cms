<?php
namespace Site\Controller;

use Zend\View\Model\ViewModel;

class BlockTabsControlController extends BaseController
{
    const BLOCK_TABS_CONTROL_TABLE_NAME = 'block_tabs_control';

    public function addEditTabAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                if ($request->isPost())
                {
                    $pageBlockId = (int)$request->getPost('pageBlockId');
                    $blockManager = $this->getBlockManager();
                    $tabsControlBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                    if ($tabsControlBlock instanceof \Site\Block\TabsControl\TabsControl)
                    {
                        $tabData = new \Site\Custom\FlexibleContainer();
                        $tabCount = $tabsControlBlock->getTabCount();
                        $maxOrder = $tabsControlBlock->getMaxTabsOrder();
                        $tabData->tabOrder = min($maxOrder + 1, 65535);
                        $tabData->tabName .= $this->translate('New tab') . ' (' . ++$tabCount . ')';
                        $tabId = $tabsControlBlock->addOrEditTab($tabData);

                        if ($tabId > 0)
                        {
                            $customStatus = 'TAB_ADDED';
                        }
                        else
                        {
                            $customStatus = 'TAB_ADDING_ERROR';
                        }

                        $jsonResponse->meta->customStatus = $customStatus;
                        $jsonResponse->data = $tabId;
                        $this->response->setContent((string)$jsonResponse);

                        return $this->response;
                    }
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function deleteTabAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $deleted = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $tabId = $request->getPost('tabId');
                $blockManager = $this->getBlockManager();
                $tabsControlBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($tabsControlBlock instanceof \Site\Block\TabsControl\TabsControl)
                {
                    $deleted = $tabsControlBlock->deleteTabById($tabId);
                }

                if ($deleted)
                {
                    $jsonResponse->meta->customStatus = 'TAB_DELETED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'TAB_DELETION_ERROR';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function reorderTabsAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $reorderingSuccessful = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $tabs = $request->getPost('tabs');

                $blockManager = $this->getBlockManager();
                $tabsControlBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($tabsControlBlock instanceof \Site\Block\TabsControl\TabsControl)
                {
                    $reorderingSuccessful = $tabsControlBlock->reorderTabs($tabs);
                }

                if ($reorderingSuccessful)
                {
                    $jsonResponse->meta->customStatus = 'TABS_REORDERED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'TABS_REORDERING_ERROR';
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to reorder tabs', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateTabNameAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $updated = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $tabId = $request->getPost('tabId');
                $tabName = $request->getPost('tabName');

                $blockManager = $this->getBlockManager();
                $tabsControlBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($tabsControlBlock instanceof \Site\Block\TabsControl\TabsControl)
                {
                    $updated = $tabsControlBlock->updateTabName($tabId, $tabName);
                }

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'TAB_NAME_UPDATED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to update tab title', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateTabContentsAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $updated = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $tabId = $request->getPost('tabId');
                $tabContents = $request->getPost('tabContents');

                $blockManager = $this->getBlockManager();
                $tabsControlBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($tabsControlBlock instanceof \Site\Block\TabsControl\TabsControl)
                {
                    $updated = $tabsControlBlock->updateTabContents($tabId, $tabContents);
                }

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'TAB_CONTENTS_UPDATED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to update tab contents', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }
}
<?php
namespace Site\Controller;

use Zend\Mvc\Controller\AbstractController;
use Zend\Http\Request;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Sql\Sql;
use Zend\Mvc\MvcEvent;
use FileSystem\Component\FileSystem;
use Graphics\Component\ImageController;
use Site\Custom\FlexibleContainer;

class UploadManager extends AbstractController
{
    protected $cmsConfig = null;
    protected $serviceLocator = null;
    protected $dbAdapter = null;
    protected $sql = null;
    protected $request = null;
    protected $language = null;
    protected $userLanguage = null;
    protected $identity = null;
    protected $attachmentsManager = null;
    protected $databaseManager = null;
    protected $blockManager = null;
    protected $fileSystem = null;
    protected $imageController = null;

    public function __construct(
        $cmsConfig,
        ServiceLocatorInterface $serviceLocator,
        Sql $sql,
        Request $request,
        \stdClass $language,
        FlexibleContainer $userLanguage,
        $identity,
        FileSystem $fileSystem
    ) {
        $this->cmsConfig = $cmsConfig;
        $this->serviceLocator = $serviceLocator;
        $this->sql = $sql;
        $this->request = $request;
        $this->dbAdapter = $sql->getAdapter();
        $this->language = $language;
        $this->userLanguage = $userLanguage;
        $this->identity = $identity;
        $this->fileSystem = $fileSystem;
        $this->imageController = new ImageController($fileSystem);
    }

    public function onDispatch(MvcEvent $e)
    {
    }

    public function getDbAdapter()
    {
        if (!$this->dbAdapter)
        {
            $serviceLocator = $this->getServiceLocator();
            $this->dbAdapter = $serviceLocator->get('Zend\Db\Adapter\Adapter');
        }

        return $this->dbAdapter;
    }

    public function getDatabaseManager()
    {
        if ($this->databaseManager == null)
        {
            $this->databaseManager = $this->serviceLocator->get('DatabaseManager');
        }

        return $this->databaseManager;
    }

    public function getBlockManager()
    {
        if ($this->blockManager == null)
        {
            $sql = $this->getDatabaseManager()->getSql();
            $this->blockManager = new BlockManager(
                $this->cmsConfig, $this->serviceLocator, $sql, $this->request,
                $this->language, $this->userLanguage, $this->identity
            );
        }

        return $this->blockManager;
    }

    public function setBlockManager(BlockManager $blockManager)
    {
        $this->blockManager = $blockManager;

        return $this;
    }

    public function getAttachmentsManager()
    {
        if ($this->attachmentsManager == null)
        {
            $sql = $this->getDatabaseManager()->getSql();
            $this->attachmentsManager = new AttachmentManager(
                $this->cmsConfig, $this->serviceLocator, $sql, $this->request,
                $this->language, $this->userLanguage, $this->identity, $this->fileSystem
            );
        }

        return $this->attachmentsManager;
    }

    public function getFileSystem()
    {
        return $this->fileSystem;
    }

    /**
     * Upload functions.
     */

    public function handleUploadForSpecificModuleAndBlock(
        $moduleName,
        $blockName,
        $pageBlockId,
        $resizeImageToMaxWidth,
        $resizeImageToMaxHeight,
        $crop,
        $cropMaskCoords
    ) {

        $attachmentsManager = $this->getAttachmentsManager();
        $blockAttachmentsDirectory = $attachmentsManager->getAttachmentsDirectory() .
            'module/' . $moduleName . '/block/' . $blockName .
            '/' . $pageBlockId . '/';

        $movedFiles = $this->moveRecentlyUploadedFiles(
            $blockAttachmentsDirectory,
            $moduleName,
            $blockName,
            $pageBlockId,
            $resizeImageToMaxWidth,
            $resizeImageToMaxHeight,
            $crop,
            $cropMaskCoords
        );
        if (!$movedFiles)
        {
            return false;
        }

        $uniqueFileNameHashes = array();

        $blockAnswer = $this->letTheBlockToDecideWhatToDoWithMovedFiles($pageBlockId, $movedFiles);
        if ($blockAnswer) // If block answer is positive (!= null and != false etc.).
        {
            $uniqueFileNameHashes = array_keys($movedFiles);
        }

        /*$data =
            '$moduleName: ' . $moduleName . "\n" .
            '$blockName: ' . $blockName . "\n" .
            '$pageBlockId: ' . $pageBlockId . "\n" .
            '$crop: ' . $crop . "\n" .
            '$movedFiles: ' . serialize($movedFiles) . "\n" .
            '$uniqueFileNameHashes: ' . serialize($uniqueFileNameHashes) . "\n"
        ;
        file_put_contents('/home/wojtek/Desktop/proimagine-cms.messages.log', $data);*/

        return $uniqueFileNameHashes;
    }

    public function moveRecentlyUploadedFiles(
        $destinationDirectory,
        $moduleName,
        $blockName,
        $pageBlockId,
        $resizeImageToMaxWidth,
        $resizeImageToMaxHeight,
        $crop,
        $cropMaskCoords
    ) {
        if (!$_FILES)
        {
            return false;
        }

        $attachmentManager = $this->getAttachmentsManager();
        $fileSystem = $this->getFileSystem();

        // Create attachment directory recursively if it does not exists:
        $oldUmask = umask(0);
        $fileSystem->mkdir($destinationDirectory, 0777);
        umask($oldUmask);

        $movedFiles = array();
        $movedFilesInfo = array();
        $movedImageFilesToCrop = array();
        $authorId = $this->identity->user_id;

        foreach ($_FILES as $inputName => $fileInfo)
        {
            $temporaryFilePath = $fileInfo['tmp_name'];
            $originalFileName = $fileInfo['name'];
            $mimeType = $fileInfo['type'];
            //$error = $fileInfo['error'];
            //$fileSize = $fileInfo['size'];

            $optionalSalt = $originalFileName;
            $uniqueHashForFileName = $attachmentManager->generateUniqueHashForFileName($optionalSalt);
            $fileExtension = $fileSystem->recognizeExtensionBasedOnSourceFilePathAndMimeType(
                $originalFileName,
                $mimeType
            );
            $destinationFilePath = $destinationDirectory . $uniqueHashForFileName;
            if ($fileExtension != '')
            {
                $destinationFilePath .= '.' . $fileExtension;
            }

            if (
            $this->moveUploadedFileAs(
                $moduleName,
                $blockName,
                $temporaryFilePath,
                $destinationFilePath,
                $resizeImageToMaxWidth,
                $resizeImageToMaxHeight
            )
            )
            {
                $fileSize = $this->getFileSystem()->filesize($destinationFilePath);
                $creationDate = new \Zend\Db\Sql\Expression('NOW()');

                $movedFiles[$uniqueHashForFileName] = $destinationFilePath;
                $movedFilesInfo[] = array(
                    'uniqueHashForFileName' => $uniqueHashForFileName,
                    'temporaryFilePath' => $temporaryFilePath,
                    'destinationFilePath' => $destinationFilePath,
                    'originalFileName' => $originalFileName,
                    'mimeType' => $mimeType,
                    'fileSize' => $fileSize,
                    'fileExtension' => $fileExtension,
                    'creationDate' => $creationDate,
                    'authorId' => $authorId,
                    'moduleName' => $moduleName,
                    'blockName' => $blockName,
                    'pageBlockId' => $pageBlockId
                );
                if ($crop)
                {
                    $movedImageFilesToCrop[$uniqueHashForFileName] = array(
                        'uniqueHashForFileName' => $uniqueHashForFileName,
                        'cropMaskCoords' => $cropMaskCoords
                    );
                }
            }
            else
            {
                $data =
                    'Could not move uploaded file. Parameters: ' . serialize(
                        array(
                            $moduleName,
                            $blockName,
                            $temporaryFilePath,
                            $destinationFilePath,
                            $resizeImageToMaxWidth,
                            $resizeImageToMaxHeight
                        )
                    ) . "\n";
                @file_put_contents('/var/www/upload-manager.log', $data);
                //file_put_contents('/home/wojtek/Desktop/proimagine-cms.messages.log', $data);

                $fileSystem->remove($movedFiles);

                return false;
            }
        }

        $allInsertStatementsSuccessful = $attachmentManager->insertRecordsToAttachmentsTableUsingFilesInfo(
            $movedFilesInfo
        );

        $allCropsSuccessful = true;
        if ($allInsertStatementsSuccessful && $movedImageFilesToCrop)
        {
            foreach ($movedImageFilesToCrop as $uniqueHashForFileName => $info)
            {
                $updatedUniqueHashForFileName =
                    $attachmentManager->cropAttachmentUsingCoordinates(
                        $uniqueHashForFileName,
                        $info['cropMaskCoords']
                    );

                if ($updatedUniqueHashForFileName != '')
                {
                    $movedImageFilesToCrop[$uniqueHashForFileName]['newUniqueHashForFileName'] =
                        $updatedUniqueHashForFileName;
                }
                else
                {
                    $allCropsSuccessful = false;

                    break;
                }
            }
        }

        if (!$allInsertStatementsSuccessful || !$allCropsSuccessful)
        {
            $fileSystem->remove($movedFiles);

            return false;
        }

        $updatedMovedFiles = array();

        foreach ($movedFiles as $uniqueHashForFileName => $destinationFilePath)
        {
            if (array_key_exists($uniqueHashForFileName, $movedImageFilesToCrop))
            {
                $newUniqueHashForFileName = $movedImageFilesToCrop[$uniqueHashForFileName]['newUniqueHashForFileName'];
                $updatedMovedFiles[$newUniqueHashForFileName] = $destinationFilePath;
            }
            else
            {
                $updatedMovedFiles[$uniqueHashForFileName] = $destinationFilePath;
            }
        }

        /*$data =
            '$movedFiles: ' . serialize($movedFiles) . "\n" .
            '$updatedMovedFiles: ' . serialize($updatedMovedFiles) . "\n"
        ;
        file_put_contents('/home/wojtek/Desktop/proimagine-cms.messages.log', $data);*/

        return $updatedMovedFiles;
    }

    public function moveUploadedFileAs(
        $moduleName,
        $blockName,
        $temporaryFileName,
        $destinationFileName,
        $resizeImageToMaxWidth,
        $resizeImageToMaxHeight
    ) {
        if (!is_uploaded_file($temporaryFileName))
        {
            $escapedTemporaryFileName = $this->escapeForHtml($temporaryFileName);

            $data =
                'File "' . $escapedTemporaryFileName . '" is not uploaded!' . "\n";
            @file_put_contents('/var/www/upload-manager.log', $data);

            throw new \Exception('File "' . $escapedTemporaryFileName . '" is not uploaded!');
        }

        $maximumImageWidth = false;
        $maximumImageHeight = false;

        if ($blockName == 'big-banner')
        {
            $maximumImageWidth = 2000;
            $maximumImageHeight = 430;
        }
        else
        {
            if ($resizeImageToMaxWidth > 0)
            {
                $maximumImageWidth = $resizeImageToMaxWidth;
            }

            if ($resizeImageToMaxHeight > 0)
            {
                $maximumImageHeight = $resizeImageToMaxHeight;
            }
        }

        $allOperationsSuccessful = false;

        $isFileMoved = @move_uploaded_file($temporaryFileName, $destinationFileName);
        if ($isFileMoved)
        {
            $isImageFile = $this->imageController->filePathPointsToImage($destinationFileName);
            if ($isImageFile)
            {
                if ($maximumImageWidth && $maximumImageHeight)
                {
                    $imageHasBeenResized = $this->imageController->resizeImageToSpecificDimensionsMaintainingAspectRatio(
                        $destinationFileName,
                        $destinationFileName,
                        $maximumImageWidth,
                        $maximumImageHeight
                    );
                }
                else
                {
                    if ($maximumImageWidth)
                    {
                        $imageHasBeenResized = $this->imageController->resizeImageToSpecificWidth(
                            $destinationFileName,
                            $destinationFileName,
                            $maximumImageWidth
                        );
                    }
                    else
                    {
                        $imageHasBeenResized = $this->imageController->resizeImageToSpecificHeight(
                            $destinationFileName,
                            $destinationFileName,
                            $maximumImageHeight
                        );
                    }
                }

                if (!$imageHasBeenResized)
                {
                    $data =
                        'Image  "' . $destinationFileName . '" cannot be resized!' . "\n";
                    @file_put_contents('/var/www/upload-manager.log', $data);
                }

                $allOperationsSuccessful = $imageHasBeenResized;
            }
            else
            {
                $allOperationsSuccessful = true;
            }
        }

        return $allOperationsSuccessful;
    }

    public function letTheBlockToDecideWhatToDoWithMovedFiles($pageBlockId, $movedFiles)
    {
        $blockManager = $this->getBlockManager();

        $methodName = 'decideWhatToDoWithRecentlyUploadedAndMovedFiles';
        $methodParameters = array(
            $this->request,
            $movedFiles
        );

        $blockMethodResult = null;
        try
        {
            $blockMethodResult = $blockManager->callPageBlockCustomMethod($pageBlockId, $methodName, $methodParameters);
        }
        catch (\Site\Exception\BlockHasNoMethodException $e)
        {
            //if ($atomicOperation)
            //{
            $this->fileSystem->remove($movedFiles);
            //}
        }

        return $blockMethodResult;
    }
}
<?php
namespace Site\Controller;

use Zend\View\Model\ViewModel;

class BlockImageGalleryController extends BaseController
{
    const BLOCK_IMAGE_GALLERY_TABLE_NAME = 'block_images_pile_images';

    public function addEditImageAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageBlockId = (int)$request->getPost('pageBlockId');
                $blockManager = $this->getBlockManager();
                $imageGalleryBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $formData = array();

                if ($imageGalleryBlock instanceof \Site\Block\ImageGallery\ImageGallery)
                {
                    if ($request->isPost())
                    {
                        if ($request->getPost('dataPosted'))
                        {
                            $formData = new \Site\Custom\FlexibleContainer($request->getPost('formData'));
                            $result = $imageGalleryBlock->addOrEditImage($formData);

                            if ($result)
                            {
                                $customStatus = ($formData->imageId > 0) ? 'IMAGE_MODIFIED' : 'IMAGE_ADDED';
                            }
                            else
                            {
                                $customStatus = ($formData->imageId > 0) ? 'IMAGE_MODIFYING_ERROR' : 'IMAGE_ADDING_ERROR';
                            }

                            $jsonResponse->meta->customStatus = $customStatus;
                            $this->response->setContent((string)$jsonResponse);

                            return $this->response;
                        }
                        else
                        {
                            $imageId = $request->getPost('imageId');
                            if ($imageId > 0)
                            {
                                if ($imageGalleryBlock)
                                {
                                    $image = $imageGalleryBlock->getImageById($imageId);
                                    $attachmentId = $image->attachment_id;
                                    $attachment = $this->getUploadManager()->getAttachmentById($attachmentId);
                                    $uniqueHashForFileName = $attachment->hash_for_filename;

                                    $formData = array(
                                        'imageId' => $imageId,
                                        'imageIsPublished' => $image->is_published ? 1 : 0,
                                        'imageLastUpdateAuthorId' => $user->user_id,
                                        'imageTitle' => $image->name,
                                        'imageUniqueHashForFileName' => $uniqueHashForFileName,
                                        'imageOriginalFileName' => $attachment->original_file_name
                                    );
                                }
                            }
                        }
                    }
                }

                $defaultFormData = array(
                    'imageId' => '',
                    'imageIsPublished' => 0,
                    'imageTitle' => '',
                    'imageUniqueHashForFileName' => '',
                    'imageOriginalFileName' => ''
                );
                $formData = new \Site\Custom\FlexibleContainer(array_merge($defaultFormData, $formData));

                $metadata = new \Zend\Db\Metadata\Metadata($this->dbAdapter);
                $blockImageGalleryImagesTableColumnsMetadata = $metadata->getColumns(
                    self::BLOCK_IMAGE_GALLERY_TABLE_NAME
                );
                $maxLengths = array();
                if ($blockImageGalleryImagesTableColumnsMetadata)
                {
                    foreach ($blockImageGalleryImagesTableColumnsMetadata as $column)
                    {
                        $name = $column->getName();

                        $maxLengths[$name] = $column->getCharacterMaximumLength();
                    }
                }

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $addNewImageView = new \Zend\View\Model\ViewModel();
                $addNewImageView->setTemplate('blocks/image-gallery/add-edit-image.phtml');
                $addNewImageView->setVariables(
                    array(
                        'imageParentImagesOptions' => $imageParentImagesOptions,
                        'imageImagesOptions' => $imageImagesOptions,
                        'maxLengths' => $maxLengths,
                        'formData' => $formData
                    )
                );

                $jsonResponse->data = $phpRenderer->render($addNewImageView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function deleteImageAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');
                $imageId = $request->getPost('imageId');

                $deleted = false;

                $imageGalleryBlock = $this->getBlockManager()->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($imageGalleryBlock instanceof \Site\Block\ImageGallery\ImageGallery)
                {
                    $deleted = $imageGalleryBlock->deleteImageById($imageId);
                }

                if ($deleted)
                {
                    $jsonResponse->meta->customStatus = 'IMAGE_DELETED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'IMAGE_DELETION_ERROR';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function reorderImagesAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $reorderingSuccessful = false;

                $pageBlockId = (int)$request->getPost('pageBlockId');
                $images = $request->getPost('images');

                $blockManager = $this->getBlockManager();
                $imageGalleryBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);

                if ($imageGalleryBlock instanceof \Site\Block\ImageGallery\ImageGallery)
                {
                    $reorderingSuccessful = $imageGalleryBlock->reorderImages($images);
                }

                if ($reorderingSuccessful)
                {
                    $jsonResponse->meta->customStatus = 'IMAGES_REORDERED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'IMAGES_REORDERING_ERROR';
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to reorder images', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function toggleImagePublishingAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $updated = false;

                $pageBlockId = (int)$request->getPost('pageBlockId');
                $imageId = (int)$request->getPost('imageId');
                $published = (int)$request->getPost('published') ? 1 : 0;

                $blockManager = $this->getBlockManager();
                $imageGalleryBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($imageGalleryBlock instanceof \Site\Block\ImageGallery\ImageGallery)
                {
                    $updated = $imageGalleryBlock->toggleImagePublished($imageId, $published);
                }

                if (!$updated)
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to update image', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }
}
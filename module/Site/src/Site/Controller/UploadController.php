<?php
namespace Site\Controller;

class UploadController extends BaseController
{
    protected $imageController = null;

    public function indexAction()
    {
        @ini_set('max_input_time', 600); // 10 minutes.
        @ini_set('max_execution_time', 600); // 10 minutes.

        $request = $this->getRequest();
        $jsonResponse = $this->jsonResponse();

        $moduleName = mb_strtolower(trim($request->getPost('moduleName')));
        $blockName = mb_strtolower(trim($request->getPost('blockName')));
        $pageBlockId = (int)$request->getPost('pageBlockId');
        $resizeImageToMaxWidth = (int)$request->getPost('resizeImageToMaxWidth');
        $resizeImageToMaxHeight = (int)$request->getPost('resizeImageToMaxHeight');
        $crop = $request->getPost('crop') ? true : false;
        $cropMaskCoords = $request->getPost('cropMaskCoords');

        $uploadManager = $this->getUploadManager();
        $fileSystem = $uploadManager->getFileSystem();
        $this->imageController = new \Graphics\Component\ImageController($fileSystem);

        /*$data =
            '$moduleName: ' . $moduleName . "\n" .
            '$blockName: ' . $blockName . "\n" .
            '$pageBlockId: ' . $pageBlockId . "\n" .
            '$resizeImageToMaxWidth: ' . $resizeImageToMaxWidth . "\n" .
            '$resizeImageToMaxHeight: ' . $resizeImageToMaxHeight . "\n" .
            '$crop: ' . $crop . "\n" .
            '$cropMaskCoords: ' . $cropMaskCoords . "\n"
        ;
        file_put_contents('/home/wojtek/Desktop/proimagine-cms.messages.log', $data);*/

        $uniqueFileNameHashes = false;

        if ($moduleName != '')
        {
            if ($blockName != '')
            {
                $uniqueFileNameHashes = $uploadManager->handleUploadForSpecificModuleAndBlock(
                    $moduleName, $blockName, $pageBlockId, $resizeImageToMaxWidth, $resizeImageToMaxHeight,
                    $crop, $cropMaskCoords
                );

                $jsonResponse->data = $uniqueFileNameHashes;
            }
        }

        /*$this->getEventManager()->trigger('OnFileUploaded', $this, array(
                'moduleName' => $moduleName,
                'blockName' => $blockName,
                'pageBlockId' => $pageBlockId,
                'resizeImageToMaxWidth' => $resizeImageToMaxWidth,
                'resizeImageToMaxHeight' => $resizeImageToMaxHeight,
                'crop' => $crop,
                'cropMaskCoords' => $cropMaskCoords,
                'uniqueFileNameHashes' => $uniqueFileNameHashes,
            )
        );*/

        /*$this->addEventToSystemHistory(
            'New file uploaded by {{user_login}} into module &quot;' . $moduleName . '&quot, ' .
            'block &quot;' . $blockName . '&quot (id: ' . ($pageBlockId ? : 'null') . ')' .
            ', with resizing to max width: ' . ($resizeImageToMaxWidth ? 'yes, to ' . $resizeImageToMaxWidth : 'no') .
            ', with resizing to max height: ' . ($resizeImageToMaxHeight ? 'yes, to ' . $resizeImageToMaxHeight : 'no') .
            ', with crop mask: ' .
            ($crop ? 'yes (' . $cropMaskCoords . ')' : 'no') . '. Array with unique file name hashes: ' .
            ($uniqueFileNameHashes ? serialize($uniqueFileNameHashes) : 'empty')
        );*/

        /*$data .=
            '$uniqueFileNameHashes: ' . serialize($uniqueFileNameHashes) . "\n";

        @file_put_contents('/var/www/cms-messages.log', $data);*/

        $this->response->setContent((string)$jsonResponse);

        return $this->response;
    }
}
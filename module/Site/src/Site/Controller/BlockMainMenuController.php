<?php
namespace Site\Controller;

use Zend\View\Model\ViewModel;

class BlockMainMenuController extends BaseController {
    public function addEditInfoAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                if ($request->isPost())
                {
                    $pageBlockId = (int)$request->getPost('pageBlockId');
                    $blockManager = $this->getBlockManager();
                    $mainMenuBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                    if ($mainMenuBlock instanceof \Site\Block\MainMenu\MainMenu)
                    {
                        $infoData = new \Site\Custom\FlexibleContainer($request->getPost('infoData'));                        
                        $infoId = $mainMenuBlock->addOrEditInfo($infoData);

                        if ($infoId > 0)
                        {
                            $customStatus = 'INFO_ADDED';
                        }
                        else
                        {
                            $customStatus = 'INFO_ADDING_ERROR';
                        }

                        $jsonResponse->meta->customStatus = $customStatus;
                        $jsonResponse->data = $infoId;
                        $this->response->setContent((string)$jsonResponse);

                        return $this->response;
                    }
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function deleteInfoAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $deleted = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $infoId = $request->getPost('infoId');
                $blockManager = $this->getBlockManager();
                $mainMenuBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($mainMenuBlock instanceof \Site\Block\MainMenu\MainMenu)
                {
                    $deleted = $mainMenuBlock->deleteInfoById($infoId);
                }

                if ($deleted)
                {
                    $jsonResponse->meta->customStatus = 'INFO_DELETED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'INFO_DELETION_ERROR';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }
    
    public function updateInfoTitleAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $updated = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $infoId = $request->getPost('infoId');
                $infoTitle = $request->getPost('infoTitle');

                $blockManager = $this->getBlockManager();
                $mainMenuBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($mainMenuBlock instanceof \Site\Block\MainMenu\MainMenu)
                {
                    $updated = $mainMenuBlock->updateInfoTitle($infoId, $infoTitle);
                }

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'INFO_TITLE_UPDATED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to update the title of the main menu side-panel information', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateInfoContentsAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $updated = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $infoId = $request->getPost('infoId');
                $infoContents = $request->getPost('infoContents');

                $blockManager = $this->getBlockManager();
                $mainMenuBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($mainMenuBlock instanceof \Site\Block\MainMenu\MainMenu)
                {
                    $updated = $mainMenuBlock->updateInfoContents($infoId, $infoContents);
                }

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'INFO_CONTENTS_UPDATED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to update main menu side-panel contents', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }
}

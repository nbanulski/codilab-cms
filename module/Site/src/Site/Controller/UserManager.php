<?php
namespace Site\Controller;

use Zend\Mvc\Controller\AbstractController;
use Zend\Http\Request;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Sql\Sql;
use Zend\Mvc\MvcEvent;
use Site\Custom\FlexibleContainer;

class UserManager extends AbstractController
{
    protected $cmsConfig = null;
    protected $serviceLocator = null;
    protected $dbAdapter = null;
    protected $sql = null;
    protected $request = null;
    protected $language = null;
    protected $userLanguage = null;
    protected $identity = null;
    protected $databaseManager = null;
    protected $mailManager = null;
    protected $allowedPasswordChars =
        'abcdefghijkmnopqrstuvwxyz23456789ABCDEFGHIJKLMNPQRSTUVWXYZ!@#$%^&*()_-+={[}]|:;<,>.?';

    public function __construct(
        $cmsConfig, ServiceLocatorInterface $serviceLocator, Sql $sql, Request $request,
        \stdClass $language, FlexibleContainer $userLanguage, $identity
    )
    {
        $this->cmsConfig = $cmsConfig;
        $this->serviceLocator = $serviceLocator;
        $this->sql = $sql;
        $this->request = $request;
        $this->dbAdapter = $sql->getAdapter();
        $this->language = $language;
        $this->userLanguage = $userLanguage;
        $this->identity = $identity;
    }

    public function onDispatch(MvcEvent $e)
    {

    }

    public function getAllowedPasswordChars()
    {
        return $this->allowedPasswordChars;
    }

    public function getDatabaseManager()
    {
        if ($this->databaseManager == null)
        {
            $this->databaseManager = $this->serviceLocator->get('DatabaseManager');
        }

        return $this->databaseManager;
    }

    public function setDatabaseManager(DatabaseManager $databaseManager)
    {
        $this->databaseManager = $databaseManager;

        return $this;
    }

    public function getMailManager()
    {
        if ($this->mailManager == null)
        {
            $this->mailManager = new MailManager(
                $this->cmsConfig, $this->serviceLocator,
                $this->language, $this->userLanguage, $this->identity
            );
        }

        return $this->mailManager;
    }

    public function setMailManager(MailManager $mailManager)
    {
        $this->mailManager = $mailManager;

        return $this;
    }

    public function getUserById($userId)
    {
        $userId = (int)$userId;

        if ($userId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();
        $sql = $this->sql;

        $select = $sql->select();
        $select
            ->from('users')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('user_id' => $userId));
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getUserByLogin($login)
    {
        $login = trim((string)$login);

        if ($login == '')
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();
        $sql = $this->sql;

        $select = $sql->select();
        $select
            ->from('users')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('login' => $login));
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getUserByEmail($email)
    {
        $email = trim((string)$email);

        if ($email == '')
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();
        $sql = $this->sql;

        $select = $sql->select();
        $select
            ->from('users')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('email' => $email));
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getUserDetailsByUserId($userId)
    {
        $userId = (int)$userId;

        if ($userId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();
        $sql = $this->sql;

        $select = $sql->select();
        $select
            ->from('users_details')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('user_id' => $userId));
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getUserAccountTypeByRole($role)
    {
        $role = trim((string)$role);

        if ($role == '')
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();
        $sql = $this->sql;

        $select = $sql->select();
        $select
            ->from('users_account_types')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('role' => $role));
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getAccountTypes()
    {
        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();
        $sql = $this->sql;

        $select = $sql->select();
        $select
            ->from('users_account_types')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable);
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function toggleAccountActivity($userId, $active)
    {
        $userId = (int)$userId;

        if ($userId <= 0)
        {
            return false;
        }

        if ($userId == $this->identity->user_id)
        {
            throw new \Site\Exception\ChangingCurrentUserAcccountActivityStatusException(
                'User cannot change his own account activity status!'
            );
        }

        $currentStatus = false; // False indicates database error.

        $active = (int)$active ? 1 : 0;

        $dbManager = $this->getDatabaseManager();
        $sql = $this->sql;

        $dbManager->beginTransaction();

        $update = $sql->update('users');
        $update
            ->set(array('active' => $active))
            ->where(array('user_id' => $userId));
        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $user = $this->getUserById($userId);
            if ($user)
            {
                $currentStatus = ($user->active == $active);
            }
        }

        if ($currentStatus !== false)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $currentStatus;
    }

    public function deleteUserById($userId)
    {
        $userId = (int)$userId;

        if ($userId <= 0)
        {
            return false;
        }

        if ($userId == $this->identity->user_id)
        {
            throw new \Site\Exception\DeletingCurrentUserAccountException('User cannot delete his own account!');
        }

        $deleted = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $this->sql;

        $dbManager->beginTransaction();

        $delete = $sql->delete();
        $delete->from('users')->where(array('user_id' => $userId));
        $result = $dbManager->executePreparedStatement($delete);
        if ($result)
        {
            $user = $this->getUserById($userId);
            $deleted = !$user;
        }

        if ($deleted)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $deleted;
    }

    public function createNewUser($userData)
    {
        if (!$userData)
        {
            return false;
        }

        $created = false;
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $userAccountTypeId = isset($userData['userAccountTypeId']) ? (int)trim($userData['userAccountTypeId']) : null;
        $userLogin = isset($userData['userLogin']) ? trim($userData['userLogin']) : null;
        $userPassword = isset($userData['userPassword']) ? trim($userData['userPassword']) : null;

        if (
            ($userLogin == '') ||
            ($userPassword == '') || !$this->isPasswordValid($userPassword)
        )
        {
            return false;
        }

        $userEmail = isset($userData['userEmail']) ? trim($userData['userEmail']) : null;
        $userGender = isset($userData['userGender']) ? (int)trim($userData['userGender']) : 0;
        if (($userGender < 0) || ($userGender > 2))
        {
            $userGender = 0;
        }
        $userName = isset($userData['userName']) ? trim($userData['userName']) : null;
        $userSurname = isset($userData['userSurname']) ? trim($userData['userSurname']) : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');
        $accountCreationDate = new \Zend\Db\Sql\Expression('NOW()');
        $salt = $this->generateRandomString();
        $activationCode = $this->generateRandomString();
        $authorId = (isset($userData['authorId']) && $userData['authorId']) ? (int)trim($userData['authorId']) : null;

        if ($userAccountTypeId <= 0)
        {
            return false;
        }

        $dbManager->beginTransaction();

        $user = $this->getUserByLogin($userLogin);
        if ($user)
        {
            $this->throwUserWithThatLoginAlreadyExistsException();
        }
        $user = $this->getUserByEmail($userEmail);
        if ($user)
        {
            $this->throwUserWithThatEmailAlreadyExistsException();
        }

        $userPasswordHashed = $this->hashUserPasswordUsingSpecificUserSalt($userPassword, $salt);

        $insert = $sql->insert('users');
        $insert
            ->columns(
            array(
                    'active',
                    'type_id',
                    'last_update_author_id',
                    'last_update_date',
                    'account_creation_date',
                    'salt',
                    'activation_code',
                    'password',
                    'login',
                    'email'
                )
            )
            ->values(
                array(
                    'active' => 0,
                    'type_id' => $userAccountTypeId,
                    'last_update_author_id' => $authorId,
                    'last_update_date' => $lastUpdateDate,
                    'account_creation_date' => $accountCreationDate,
                    'salt' => $salt,
                    'activation_code' => $activationCode,
                    'password' => $userPasswordHashed,
                    'login' => $userLogin,
                    'email' => $userEmail
                )
            );
        $userId = $dbManager->executeInsertAndGetLastInsertId($insert);
        if ($userId > 0)
        {
            $insert = $sql->insert('users_details');
            $insert
                ->columns(
                array(
                        'user_id',
                        'gender',
                        'last_update_author_id',
                        'last_update_date',
                        'name',
                        'surname'
                    )
                )
                ->values(
                    array(
                        'user_id' => $userId,
                        'gender' => $userGender,
                        'last_update_author_id' => $authorId,
                        'last_update_date' => $lastUpdateDate,
                        'name' => $userName,
                        'surname' => $userSurname
                    )
                );
            $result = $dbManager->executePreparedStatement($insert);
            if ($result)
            {
                $created = true;
            }
        }

        if ($created)
        {
            $dbManager->commit();

            return $userId;
        }
        else
        {
            $dbManager->rollback();
        }

        return false;
    }

    public function updateExistingUser($userData)
    {
        if (!$userData)
        {
            return false;
        }

        $updated = false;
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $userId = (int)$userData['userId'];
        if ($userId <= 0)
        {
            return false;
        }

        $dbManager->beginTransaction();

        $userAccountTypeId = isset($userData['userAccountTypeId']) ? (int)trim($userData['userAccountTypeId']) : null;
        $userLogin = isset($userData['userLogin']) ? trim($userData['userLogin']) : null;
        $userPassword = isset($userData['userPassword']) ? trim($userData['userPassword']) : null;

        if (
            ($userLogin == '') ||
            ($userPassword != '') && !$this->isPasswordValid($userPassword)
        )
        {
            return false;
        }

        $userEmail = isset($userData['userEmail']) ? trim($userData['userEmail']) : null;
        $userGender = isset($userData['userGender']) ? (int)trim($userData['userGender']) : null;
        if (($userGender < 0) || ($userGender > 2))
        {
            $userGender = 0;
        }
        $userName = isset($userData['userName']) ? trim($userData['userName']) : null;
        $userSurname = isset($userData['userSurname']) ? trim($userData['userSurname']) : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');
        $authorId = (int)$userData['authorId'];

        $user = $this->getUserByLogin($userLogin);
        if ($user && ($user->user_id != $userId))
        {
            $this->throwUserWithThatLoginAlreadyExistsException();
        }
        $user = $this->getUserByEmail($userEmail);
        if ($user && ($user->user_id != $userId))
        {
            $this->throwUserWithThatEmailAlreadyExistsException();
        }

        $user = $this->getUserById($userId);
        if ($user)
        {
            $update = $sql->update('users');
            $update
                ->set(
                    array(
                        'type_id' => $userAccountTypeId,
                        'last_update_author_id' => $authorId,
                        'last_update_date' => $lastUpdateDate,
                        'login' => $userLogin,
                        'email' => $userEmail
                    )
                )
                ->where(array('user_id' => $userId));

            if ($userPassword != '')
            {
                $userPasswordHashed = $this->hashUserPassword($user, $userPassword);
                $update->set(array('password' => $userPasswordHashed), $update::VALUES_MERGE);
            }

            $result = $dbManager->executePreparedStatement($update);
            if ($result)
            {
                $user = $this->getUserById($userId);
                if ($user &&
                    ($user->type_id == $userAccountTypeId) &&
                    ($user->last_update_author_id == $authorId) &&
                    ($user->login == $userLogin) &&
                    (($userPassword == '') || (($userPassword != '') && ($user->password == $userPasswordHashed))) &&
                    ($user->email == $userEmail))
                {
                    $update = $sql->update('users_details');
                    $update
                        ->set(
                            array(
                                'user_id' => $userId,
                                'gender' => $userGender,
                                'last_update_author_id' => $authorId,
                                'last_update_date' => $lastUpdateDate,
                                'name' => $userName,
                                'surname' => $userSurname
                            )
                        )
                        ->where(array('user_id' => $userId));
                    $result = $dbManager->executePreparedStatement($update);
                    if ($result)
                    {
                        $userDetails = $this->getUserDetailsByUserId($userId);
                        if ($userDetails &&
                            ($userDetails->gender == $userGender) &&
                            ($userDetails->name == $userName) &&
                            ($userDetails->surname == $userSurname))
                        {
                            $updated = true;
                        }
                    }
                }
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function generateSetAndReturnNewConfirmationCodeForUser($userId)
    {
        $userId = (int)$userId;

        if ($userId <= 0)
        {
            return false;
        }

        $confirmationCode = $this->generateRandomString(8);

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $update = $sql->update('users');
        $update
            ->set(array('confirmation_code' => $confirmationCode))
            ->where(array('user_id' => $userId));
        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $user = $this->getUserById($userId);
            if ($user->confirmation_code == $confirmationCode)
            {
                $generatedAndSet = true;
            }
        }

        if ($generatedAndSet)
        {
            $dbManager->commit();

            return $confirmationCode;
        }
        else
        {
            $dbManager->rollback();
        }

        return false;
    }

    public function sendAccountActivationCode($loginOrEmail)
    {
        $loginOrEmail = trim((string)$loginOrEmail);

        if ($loginOrEmail == '')
        {
            return false;
        }

        $isEmail = mb_strpos($loginOrEmail, '@') !== false;

        if ($isEmail)
        {
            $user = $this->getUserByEmail($loginOrEmail);
        }
        else
        {
            $user = $this->getUserByLogin($loginOrEmail);
        }

        if ($user)
        {
            $globalConfig = $this->serviceLocator->get('GlobalConfig');
            $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');
            $uri = $this->getRequest()->getUri();
            $basePath = sprintf('%s://%s', $uri->getScheme(), $uri->getHost());
            $activationCode = $user->activation_code;
            if ($activationCode == '')
            {
                return false;
            }

            $isoCode = $globalConfig->cms['default_language_iso_code'];
            $accountActivationUrl = $basePath . '/' . $isoCode . '/user/account-activation/account/' . $user->login . '/code/' . $activationCode;
            $accountActivationUrlEscaped = $renderer->escapeForHtml($accountActivationUrl);

            $htmlContent = 'Witaj,' . "<br />\n<br />\n" .
                'Dziękujemy za rejestrację na stronie www.vistula.edu.pl.' . "<br />\n<br />\n" .
                'Login Twojego konta:' . "<br />\n" .
                $user->login . "<br />\n<br />\n" .
                'Link umożliwiający aktywowanie konta:' . "<br />\n" .
                '<a href="' . $accountActivationUrlEscaped . '">' . $accountActivationUrlEscaped . '</a>';
            $textContent = strip_tags($htmlContent);

            $mailManager = $this->getMailManager();
            $mailManager->setRecipientAddress($user->email)
                ->setSubject('Dziękujemy za rejestrację na portalu Vistula')
                ->setTextContent($textContent)
                ->setHtmlContent($htmlContent);
            $mailManager->send();

            return true;
        }

        return false;
    }

    public function activateUserAccountByUserId($userId)
    {
        $userId = (int)$userId;

        if ($userId <= 0)
        {
            return false;
        }

        $accountActivated = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $update = $sql->update('users');
        $update
            ->set(
                array(
                    'active' => 1,
                    'activation_code' => ''
                )
            )
            ->where(array('user_id' => $userId));
        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $user = $this->getUserById($userId);
            if ($user->active == 1)
            {
                $accountActivated = true;
            }
        }

        if ($accountActivated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $accountActivated;
    }

    public function sendPasswordResetActivationCode($loginOrEmail)
    {
        $loginOrEmail = trim((string)$loginOrEmail);

        if ($loginOrEmail == '')
        {
            return false;
        }

        $isEmail = mb_strpos($loginOrEmail, '@') !== false;

        if ($isEmail)
        {
            $user = $this->getUserByEmail($loginOrEmail);
        }
        else
        {
            $user = $this->getUserByLogin($loginOrEmail);
        }

        if ($user)
        {
            $globalConfig = $this->serviceLocator->get('GlobalConfig');
            $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');
            $uri = $this->getRequest()->getUri();
            $basePath = sprintf('%s://%s', $uri->getScheme(), $uri->getHost());
            $confirmationCode = $this->generateSetAndReturnNewConfirmationCodeForUser($user->user_id);
            if ($confirmationCode == '')
            {
                return false;
            }

            $isoCode = $globalConfig->cms['default_language_iso_code'];
            $newPasswordUrl = $basePath . '/' . $isoCode . '/user/new-password/account/' . $user->login . '/code/' . $confirmationCode;
            $newPasswordUrlEscaped = $renderer->escapeForHtml($newPasswordUrl);

            $htmlContent = 'Link umożliwiający nadanie nowego hasła:' . "<br />\n" .
                '<a href="' . $newPasswordUrlEscaped . '">' . $newPasswordUrlEscaped . '</a>';
            $textContent = strip_tags($htmlContent);

            $mailManager = $this->getMailManager();
            $mailManager
                ->setRecipientAddress($user->email)
                ->setSubject($this->translate('Password reset', 'default', $this->getUserLanguage()->zend2_locale))
                ->setTextContent($textContent)
                ->setHtmlContent($htmlContent)
                ->send()
            ;

            return true;
        }

        return false;
    }

    public function isPasswordValid($password)
    {
        $password = trim((string)$password);

        if ($password == '')
        {
            return false;
        }

        $passwordLength = mb_strlen($password);

        if ($passwordLength > 25)
        {
            return false;
        }

        for ($i = 0; $i < $passwordLength; $i++)
        {
            $char = mb_substr($this->allowedPasswordChars, $i, 1);

            if (mb_strpos($this->allowedPasswordChars, $char) === false)
            {
                return false;
            }
        }

        return true;
    }

    public function hashUserPassword($user, $password)
    {
        $globalConfig = $this->serviceLocator->get('GlobalConfig');
        $globalPasswordSalt = $globalConfig->cms['password_salt'];

        return sha1($globalPasswordSalt . $password . $user->salt);
    }

    public function hashUserPasswordByUserId($userId, $password)
    {
        $user = $this->getUserById($userId);
        if (!$user)
        {
            return false;
        }

        return $this->hashUserPassword($user, $password);
    }

    public function hashUserPasswordUsingSpecificUserSalt($password, $userSalt)
    {
        $globalConfig = $this->serviceLocator->get('GlobalConfig');
        $globalPasswordSalt = $globalConfig->cms['password_salt'];

        return sha1($globalPasswordSalt . $password . $userSalt);
    }

    public function setPasswordForUser($userId, $password)
    {
        $userId = (int)$userId;
        $password = trim((string)$password);

        if (($userId <= 0) || ($password == ''))
        {
            return false;
        }

        $user = $this->getUserById($userId);
        if (!$user)
        {
            return false;
        }

        $passwordSet = false;

        $newPasswordHash = $this->hashUserPassword($user, $password);

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $update = $sql->update('users');
        $update
            ->set(array('password' => $newPasswordHash))
            ->where(array('user_id' => $userId));
        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $user = $this->getUserById($userId);
            if ($user->password == $newPasswordHash)
            {
                $passwordSet = true;
            }
        }

        if ($passwordSet)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $passwordSet;
    }

    public function generateRandomString($characterCount = 8)
    {
        $salt = '';

        // Skip lowercase "L", uppercase "o" and numbers "0" and "1" because of different fonts across multiple
        // operating systems and browsers.
        $allowedCharCount = mb_strlen($this->allowedPasswordChars);
        for ($i = 0; $i < $characterCount; $i++)
        {
            $randomIndex = mt_rand(0, $allowedCharCount - 1);
            $char = mb_substr($this->allowedPasswordChars, $randomIndex, 1);

            $salt .= $char;
        }

        return $salt;
    }

    public function throwUserWithThatLoginAlreadyExistsException()
    {
        $exceptionMessage = 'W systemie istnieje już użytkownik o takim loginie.';

        throw new \Site\Exception\UserWithThatLoginAlreadyExistsException($exceptionMessage);
    }

    public function throwUserWithThatEmailAlreadyExistsException()
    {
        $exceptionMessage = 'W systemie istnieje już użytkownik o takim adresie e-mail.';

        throw new \Site\Exception\UserWithThatEmailAlreadyExistsException($exceptionMessage);
    }
}
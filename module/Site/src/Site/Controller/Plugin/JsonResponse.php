<?php
namespace Site\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class JsonResponse extends AbstractPlugin
{

    public function __invoke()
    {
        return $this->create();
    }

    public function create()
    {
        $timestampInMiliseconds = time() * 1000;
        $sessionExist = ($this->controller->identity() !== null) ? true : false;

        $response = new JsonResponseObjectEncodableToJsonString();
        $response->meta = new \stdClass();
        $response->meta->requestOk = true;
        $response->meta->requestErrorMessage = null;
        $response->meta->requestTimestamp = $timestampInMiliseconds;
        $response->meta->requestStatusCode = 200;
        $response->meta->sessionExist = $sessionExist;
        $response->meta->customStatus = null;
        $response->data = null;

        return $response;
    }
}

class JsonResponseObjectEncodableToJsonString extends \stdClass
{

    public function __toString()
    {
        return json_encode($this, JSON_HEX_APOS | JSON_HEX_QUOT);
    }
}
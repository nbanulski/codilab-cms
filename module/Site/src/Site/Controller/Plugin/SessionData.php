<?php
namespace Site\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Mvc\Controller\Plugin\Service\IdentityFactory;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Site\Component\BasicSessionData;

class SessionData implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceFactory = new IdentityFactory();
        $identity = $serviceFactory->createService($serviceLocator);

        return new IdentityProxy($identity);
    }
}

final class IdentityProxy extends AbstractPlugin
{
    private $identity;

    public function __construct($identity)
    {
        $this->identity = $identity;
    }

    public function __invoke()
    {
        $identity = $this->identity;
        $identityData = $identity();
        $sessionData = new BasicSessionData($identityData);

        return $sessionData;
    }

    public function __call($method, $args)
    {
        return call_user_func_array(array($this->identity, $method), $args);
    }

    public static function __callstatic($method, $args)
    {
        return call_user_func_array(array($this->identity, $method), $args);
    }
}
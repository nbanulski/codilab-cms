<?php
namespace Site\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class EscapeForHtml extends AbstractPlugin
{

    public function __invoke($text)
    {
        return $this->escape($text);
    }

    public function escape($text)
    {
        $text = (string)$text;
        if ($text != '')
        {
            $flags = ENT_COMPAT;
            if (version_compare(PHP_VERSION, '5.4.0') >= 0)
            {
                $flags |= ENT_HTML5;
            }
            else
            {
                $flags |= ENT_HTML401; // Fallback to HTML 4.01.
            }

            $encoding = 'UTF-8';

            $text = htmlspecialchars(trim($text), $flags, $encoding);
        }

        return $text;
    }
}
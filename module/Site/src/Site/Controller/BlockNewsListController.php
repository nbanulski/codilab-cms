<?php
namespace Site\Controller;

class BlockNewsListController extends BaseController
{
    const BLOCK_NEWS_LIST_TABLE_NAME = 'block_news_list';
    const BLOCK_NEWS_LIST_CATEGORIES_TABLE_NAME = 'block_news_list_categories';

    protected $newsListBlock = null;

    /**
     * Getters.
     */

    public function getAddEditCategoryViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');

                $blockManager = $this->getBlockManager();
                $addEditCategoryViewHtml =
                    $blockManager->callPageBlockCustomMethod(
                        $pageBlockId, 'renderAddEditCategory', array($this->request, true)
                    )
                ;
                if ($addEditCategoryViewHtml != '')
                {
                    $jsonResponse->data = $addEditCategoryViewHtml;
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage =
                        $this->translate(
                            'Failed to fetch template', 'default', $this->getUserLanguage()->zend2_locale
                        ) . '.'
                    ;
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getCategoriesManagementViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');

                $blockManager = $this->getBlockManager();
                $categoriesManagementViewHtml =
                    $blockManager->callPageBlockCustomMethod(
                        $pageBlockId, 'renderCategoriesManagement', array($this->request, true)
                    )
                ;
                if ($categoriesManagementViewHtml != '')
                {
                    $jsonResponse->data = $categoriesManagementViewHtml;
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to fetch template', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    protected function getCategories()
    {
        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_NEWS_LIST_CATEGORIES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable);
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    protected function getCategoryCountByContentId($contentId)
    {
        $contentId = (int)$contentId;
        if ($contentId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_NEWS_LIST_CATEGORIES_TABLE_NAME)
            ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')), $prefixColumnsWithTable)
            ->where(array('content_id' => $contentId));
        $resultSet = $dbManager->getHydratedResultSet($select);
        $row = $resultSet->current();
        $count = $row ? $row->count : false;

        return $count;
    }

    /**
     * Logic-functions.
     */

    public function addEditCategoryAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageBlockId = (int)$request->getPost('pageBlockId');
                $blockManager = $this->getBlockManager();
                $this->newsListBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $formData = array();

                if ($request->isPost())
                {
                    if ($request->getPost('dataPosted'))
                    {
                        $customStatus = 'INVALID_DATA';

                        $formData = $request->getPost('formData');
                        if ($formData)
                        {
                            $dataValid = true;

                            $currentCategoryId = isset($formData['categoryId']) ? $formData['categoryId'] : null;
                            $categoryTitle = isset($formData['categoryTitle']) ? trim($formData['categoryTitle']) : null;
                            $isPublished = isset($formData['publishNow']) ? $formData['publishNow'] : null;

                            // TODO: Data validation.

                            if ($dataValid)
                            {
                                $result = $this->createNewOrUpdateExistingCategory($pageBlockId,
                                    array(
                                        'categoryId' => $currentCategoryId,
                                        'categoryTitle' => $categoryTitle,
                                        'isPublished' => $isPublished ? 1 : 0,
                                        'authorId' => $user->user_id
                                    )
                                );
                            }

                            if ($dataValid && $result)
                            {
                                $customStatus =
                                    ($currentCategoryId > 0)
                                        ? 'NEWS_CATEGORY_MODIFIED'
                                        : 'NEWS_CATEGORY_ADDED'
                                ;
                            }
                            else
                            {
                                $customStatus =
                                    ($currentCategoryId > 0)
                                        ? 'NEWS_CATEGORY_MODIFYING_ERROR'
                                        : 'NEWS_CATEGORY_ADDING_ERROR'
                                ;
                            }
                        }

                        $jsonResponse->meta->customStatus = $customStatus;
                        $this->response->setContent((string)$jsonResponse);

                        return $this->response;
                    }
                    else
                    {
                        $categoryId = $request->getPost('categoryId');
                        if ($categoryId > 0)
                        {
                            if ($this->newsListBlock)
                            {
                                $categoryData = $this->newsListBlock->getCategoryById($categoryId);

                                $formData = array(
                                    'categoryId' => $categoryId,
                                    'categoryTitle' => $categoryData->name,
                                    'isPublished' => $categoryData->is_published ? 1 : 0,
                                    'authorId' => $user->user_id,
                                );
                            }
                        }
                    }
                }

                $defaultFormData = array(
                    'categoryId' => '',
                    'categoryTitle' => '',
                    'isPublished' => 0,
                );
                $formData = new \Site\Custom\FlexibleContainer(array_merge($defaultFormData, $formData));

                $metadata = new \Zend\Db\Metadata\Metadata($this->dbAdapter);
                $blockNewsListCategoriesTableColumnsMetadata = $metadata->getColumns(
                    self::BLOCK_NEWS_LIST_CATEGORIES_TABLE_NAME
                );
                $maxLengths = array();
                if ($blockNewsListCategoriesTableColumnsMetadata)
                {
                    foreach ($blockNewsListCategoriesTableColumnsMetadata as $column)
                    {
                        $name = $column->getName();

                        $maxLengths[$name] = $column->getCharacterMaximumLength();
                    }
                }

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $addNewCategoryView = new \Zend\View\Model\ViewModel();
                $addNewCategoryView->setTemplate('blocks/news/add-edit-category.phtml');
                $addNewCategoryView->setVariables(
                    array(
                        'maxLengths' => $maxLengths,
                        'formData' => $formData
                    )
                );

                $jsonResponse->data = $phpRenderer->render($addNewCategoryView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function addEditNewsAction($asGuest = false)
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageBlockId = (int)$request->getPost('pageBlockId');
                $blockManager = $this->getBlockManager();
                $this->newsListBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $formData = array();

                if (
                    $request->isPost() &&
                    ($this->newsListBlock instanceof \Site\Block\NewsList\NewsList)
                )
                {
                    if ($request->getPost('dataPosted'))
                    {
                        $formData = new \Site\Custom\FlexibleContainer($request->getPost('formData'));
                        $result = $this->newsListBlock->addOrEditNews($formData);

                        if ($result)
                        {
                            $customStatus =
                                ($formData->newsId > 0)
                                    ? 'NEWS_MODIFIED'
                                    : 'NEWS_ADDED'
                            ;
                        }
                        else
                        {
                            $customStatus =
                                ($formData->newsId > 0)
                                    ? 'NEWS_MODIFYING_ERROR'
                                    : 'NEWS_ADDING_ERROR'
                            ;
                        }

                        $jsonResponse->meta->customStatus = $customStatus;
                        $this->response->setContent((string)$jsonResponse);

                        return $this->response;
                    }
                    else
                    {
                        $newsId = $request->getPost('newsId');
                        if ($newsId > 0)
                        {
                            $newsListBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                            if ($newsListBlock)
                            {
                                $news = $newsListBlock->getNewsById($newsId);
                                $attachmentId = $news->attachment_id;
                                $attachment = $this->getUploadManager()->getAttachmentById($attachmentId);
                                $uniqueHashForFileName = $attachment->hash_for_filename;

                                $startTime = strtotime($news->news_date);

                                $newsDate = date('Y-m-d', $startTime);
                                $newsStartHour = date('H:i', $startTime);

                                $formData = array(
                                    'newsId' => $newsId,
                                    'newsCategoryId' => $news->category_id,
                                    'newsUniqueHashForFileName' => $uniqueHashForFileName,
                                    'newsTeaser' => $news->teaser,
                                    'newsContents' => $news->contents,
                                    'newsLabel' => $news->label,
                                    'isPublished' => $news->is_published ? 1 : 0,
                                    'newsIsPromoted' => $news->is_promoted ? 1 : 0,
                                    'authorId' => $user->user_id,
                                    'newsDate' => $newsDate,
                                    'newsStartHour' => $newsStartHour,
                                    'newsCleanUrl' => $news->clean_url,
                                    'newsTitle' => $news->name
                                );
                            }
                        }
                    }
                }

                $defaultFormData = array(
                    'newsId' => '',
                    'newsCategoryId' => null,
                    'newsUniqueHashForFileName' => '',
                    'newsTeaser' => '',
                    'newsContents' => '',
                    'newsLabel' => '',
                    'newsDate' => date('Y-m-d'),
                    'newsStartHour' => '',
                    'newsImages' => array(),
                    'newsCleanUrl' => '',
                    'newsTitle' => '',
                    'isPublished' => 0,
                    'newsIsPromoted' => 0
                );
                $formData = new \Site\Custom\FlexibleContainer(array_merge($defaultFormData, $formData));

                $metadata = new \Zend\Db\Metadata\Metadata($this->dbAdapter);
                $blockNewsListTableColumnsMetadata = $metadata->getColumns(
                    self::BLOCK_NEWS_LIST_TABLE_NAME
                );
                $maxLengths = array();
                if ($blockNewsListTableColumnsMetadata)
                {
                    foreach ($blockNewsListTableColumnsMetadata as $column)
                    {
                        $name = $column->getName();

                        $maxLengths[$name] = $column->getCharacterMaximumLength();
                    }
                }

                $newsCategoriesOptions = array();
                $categories = $this->getCategories();
                if ($categories)
                {
                    foreach ($categories as $category)
                    {
                        $newsCategoriesOptions[$category->category_id] = $category->name;
                    }
                }

                $this->addEditView->setVariables(
                    array(
                        'newsCategoriesOptions' => $newsCategoriesOptions
                    )
                );

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $addNewNewsView = new \Zend\View\Model\ViewModel();
                $addNewNewsView->setTemplate('blocks/news/add-edit.phtml');
                $addNewNewsView->setVariables(
                    array(
                        'newsCategoriesOptions' => $newsCategoriesOptions,
                        'maxLengths' => $maxLengths,
                        'formData' => $formData
                    )
                );

                $jsonResponse->data = $phpRenderer->render($addNewNewsView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function createNewCategory($pageBlockId, $categoryData)
    {
        if (($pageBlockId <= 0 ) || !$categoryData)
        {
            return false;
        }

        $authorId = isset($categoryData['authorId']) ? $categoryData['authorId'] : null;
        $isPublished = $categoryData['isPublished'] ? 1 : 0;
        if ($authorId)
        {
            $authorId = (int)$authorId;
        }
        $title = trim($categoryData['categoryTitle']);

        if ($title == '')
        {
            return false;
        }

        $created = false;
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $blockManager = $this->getBlockManager();
        $pageBlockContentId = $blockManager->getPageBlockContentIdByPageBlockId($pageBlockId);

        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager->beginTransaction();

        $insert = $sql->insert(self::BLOCK_NEWS_LIST_CATEGORIES_TABLE_NAME);
        $insert
            ->columns(
                array(
                    'content_id',
                    'is_published',
                    'last_update_author_id',
                    'last_update_date',
                    'name',
                )
            )
            ->values(
                array(
                    'content_id' => $pageBlockContentId,
                    'is_published' => $isPublished,
                    'last_update_author_id' => $authorId,
                    'last_update_date' => $lastUpdateDate,
                    'name' => $title
                )
            );
        $categoryId = $dbManager->executeInsertAndGetLastInsertId($insert);
        if ($categoryId > 0)
        {
            $created = true;
        }

        if ($created)
        {
            $dbManager->commit();

            $this->selectThisCategoryAsDefaultIfThereIsOnlyOne($pageBlockId, $categoryId);
        }
        else
        {
            $dbManager->rollback();
        }

        return $created;
    }

    public function deleteCategoryAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageBlockId = $request->getPost('pageBlockId');
                $categoryId = $request->getPost('categoryId');
                $newsListBlock = $this->getBlockManager()->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $deleted = $newsListBlock->deleteCategoryById($categoryId);

                if ($deleted)
                {
                    $jsonResponse->meta->customStatus = 'NEWS_CATEGORY_DELETED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'NEWS_CATEGORY_DELETION_ERROR';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function deleteNewsAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageBlockId = $request->getPost('pageBlockId');
                $newsId = $request->getPost('newsId');
                $newsListBlock = $this->getBlockManager()->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $deleted = $newsListBlock->deleteNewsById($newsId);

                if ($deleted)
                {
                    $jsonResponse->meta->customStatus = 'NEWS_DELETED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'NEWS_DELETION_ERROR';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function deleteNewsImageAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $deleted = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $newsId = $request->getPost('newsId');
                $imageId = $request->getPost('imageId');

                $newsListBlock = $this->getBlockManager()->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($newsListBlock instanceof \Site\Block\NewsList\NewsList)
                {
                    $deleted = $newsListBlock->deleteNewsImageByNewsIdAndImageId($newsId, $imageId);
                }

                if ($deleted)
                {
                    $jsonResponse->meta->customStatus = 'IMAGE_DELETED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'IMAGE_DELETION_ERROR';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function toggleCategoryPublishingAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $updated = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $categoryId = $request->getPost('categoryId');
                $published = (int)$request->getPost('published') ? 1 : 0;

                $blockManager = $this->getBlockManager();
                $newsListBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($newsListBlock instanceof \Site\Block\NewsList\NewsList)
                {
                    $updated = $newsListBlock->toggleCategoryPublished($categoryId, $published);
                }

                if (!$updated)
                {
                    $jsonResponse->meta->requestErrorMessage =
                        $this->translate(
                            'Failed to update news category', 'default',
                            $this->getUserLanguage()->zend2_locale
                        ) . '.'
                    ;
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function toggleNewsPublishingAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $updated = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $newsId = $request->getPost('newsId');
                $published = (int)$request->getPost('published') ? 1 : 0;

                $blockManager = $this->getBlockManager();
                $newsListBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($newsListBlock instanceof \Site\Block\NewsList\NewsList)
                {
                    $updated = $newsListBlock->toggleNewsPublished($newsId, $published);
                }

                if (!$updated)
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to update news', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateExistingCategory($pageBlockId, $categoryData)
    {
        if (($pageBlockId <= 0 ) || !$categoryData)
        {
            return false;
        }

        $categoryId = (int)$categoryData['categoryId'];
        $isPublished = $categoryData['isPublished'] ? 1 : 0;
        $authorId = isset($categoryData['authorId']) ? (int)$categoryData['authorId'] : null;
        $title = trim($categoryData['categoryTitle']);

        if (($categoryId <= 0) || ($authorId <= 0) || ($title == ''))
        {
            return false;
        }

        $updated = false;
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_NEWS_LIST_CATEGORIES_TABLE_NAME);
        $update
            ->set(
                array(
                    'is_published' => $isPublished,
                    'last_update_author_id' => $authorId,
                    'last_update_date' => $lastUpdateDate,
                    'name' => $title,
                )
            )
            ->where(
                array(
                    'category_id' => $categoryId
                )
            );
        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $category = $this->newsListBlock->getCategoryById($categoryId);
            if ($category &&
                ($category->is_published == $isPublished) &&
                ($category->last_update_author_id == $authorId) &&
                ($category->name == $title))
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    protected function createNewOrUpdateExistingCategory($pageBlockId, $formData)
    {
        $categoryId = isset($formData['categoryId']) ? (int)$formData['categoryId'] : null;

        if ($categoryId > 0)
        {
            return $this->updateExistingCategory($pageBlockId, $formData);
        }
        else
        {
            return $this->createNewCategory($pageBlockId, $formData);
        }
    }

    protected function selectThisCategoryAsDefaultIfThereIsOnlyOne($pageBlockId, $categoryId)
    {
        $pageBlockId = (int)$pageBlockId;
        $categoryId = (int)$categoryId;

        if (($pageBlockId <= 0) || ($categoryId <= 0))
        {
            return false;
        }

        $blockManager = $this->getBlockManager();
        $pageBlockContentId = $blockManager->getPageBlockContentIdByPageBlockId($pageBlockId);

        if ($this->getCategoryCountByContentId($pageBlockContentId) > 0)
        {
            $result = false;

            $newsListBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
            if ($newsListBlock instanceof \Site\Block\NewsList\NewsList)
            {
                $result = $newsListBlock->selectCategoryAsDefault($categoryId);
            }

            return $result;
        }

        return false;
    }
}
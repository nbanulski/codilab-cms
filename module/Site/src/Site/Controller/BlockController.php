<?php
namespace Site\Controller;

class BlockController extends BaseController
{
    public function getBlockListViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $languageCategoryId = $request->getPost('languageCategoryId');
                $phpClassName = $request->getPost('phpClassName');
                $blockNameQuery = $request->getPost('blockNameQuery');

                $blocksWereFiltered = ($languageCategoryId > 0) || ($phpClassName != '') || ($blockNameQuery != '');

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $userLanguage = $this->getUserLanguage();

                list($blockList, $dynamicBlockList) = $this->getBlockManager()->getAllBlocks(
                    $userLanguage->language_id, $languageCategoryId, $phpClassName, $blockNameQuery
                );

                $blockListView = new \Zend\View\Model\ViewModel();
                $blockListView->setTemplate('partial/block-list.phtml');
                $blockListView->setVariables(
                    array(
                        'userLanguage' => $userLanguage,
                        'blockList' => $blockList,
                        'blocksWereFiltered' => $blocksWereFiltered
                    )
                );

                $jsonResponse->data = $phpRenderer->render($blockListView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getDynamicBlockCountAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $blockId = $request->getPost('blockId');

            $blockCount = $this->getBlockManager()->getDynamicBlockCountByDynamicBlockId($blockId);
            if ($blockCount !== false)
            {
                $jsonResponse->data = $blockCount;
            }
            else
            {
                $jsonResponse->meta->requestErrorMessage =
                    $this->translate(
                        'Failed to fetch the number of dynamic block occurrences',
                        'default', $this->getUserLanguage()->zend2_locale
                    ) . '.';
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getDynamicBlockListViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $languageCategoryId = $request->getPost('languageCategoryId');
                $phpClassName = $request->getPost('phpClassName');
                $blockNameQuery = $request->getPost('blockNameQuery');

                $blocksWereFiltered = ($languageCategoryId > 0) || ($phpClassName != '') || ($blockNameQuery != '');

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $userLanguage = $this->getUserLanguage();

                list($blockList, $dynamicBlockList) = $this->getBlockManager()->getAllBlocks(
                    $userLanguage->language_id, $languageCategoryId, $phpClassName, $blockNameQuery
                );

                $dynamicBlockListView = new \Zend\View\Model\ViewModel();
                $dynamicBlockListView->setTemplate('partial/dynamic-block-list.phtml');
                $dynamicBlockListView->setVariables(
                    array(
                        'userLanguage' => $userLanguage,
                        'dynamicBlockList' => $dynamicBlockList,
                        'blocksWereFiltered' => $blocksWereFiltered
                    )
                );

                $jsonResponse->data = $phpRenderer->render($dynamicBlockListView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getEditBlockPropertiesViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageId = $request->getPost('pageId');
                $blockId = $request->getPost('blockId');

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $blockManager = $this->getBlockManager();
                $languageManager = $this->getLanguageManager();
                $languages = $languageManager->getAllLanguages();
                if ($languages)
                {
                    $languages->buffer();
                }

                $currentBlockNames = new \Site\Custom\FlexibleContainer();

                $blockProperties = $blockManager->getAllBlockPropertiesByBlockId($blockId);
                if ($blockProperties)
                {
                    foreach ($blockProperties as $properties)
                    {
                        $currentBlockNames[$properties->language_id] = $properties->name;
                    }
                }

                $languageCategories = array();
                $currentLanguageCategoryId = null;

                if ($blockId > 0)
                {
                    $currentLanguageCategoryId = $blockManager->getBlockLanguageCategoryIdByBlockId($blockId);
                }

                if (!$currentLanguageCategoryId && ($pageId > 0))
                {
                    $currentLanguageCategoryId = $this->getPageManager()->getPageLanguageIdByPageId($pageId);
                }

                $languageCategories[] =
                    '---' . $this->translate(
                        'All categories', 'default', $this->getUserLanguage()->zend2_locale
                    ) . '---';

                if ($languages)
                {
                    foreach ($languages as $language)
                    {
                        $languageCategories[$language->language_id] = $language->english_name;
                    }
                }

                $editBlockNamesView = new \Zend\View\Model\ViewModel();
                $editBlockNamesView->setTemplate('blocks/block-save-as-dynamic-or-update-properties.phtml');
                $editBlockNamesView->setVariables(
                    array(
                        'pageId' => $pageId,
                        'blockId' => $blockId,
                        'userLanguage' => $this->getUserLanguage(),
                        'languages' => $languages,
                        'currentBlockNames' => $currentBlockNames,
                        'currentLanguageCategoryId' => $currentLanguageCategoryId,
                        'languageCategories' => $languageCategories
                    )
                );

                $jsonResponse->data = $phpRenderer->render($editBlockNamesView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function deleteDynamicBlockAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $blockId = $request->getPost('blockId');

                $blockManager = $this->getBlockManager();
                $deleted = $blockManager->deleteDynamicBlock($blockId);
                if ($deleted)
                {
                    $jsonResponse->meta->customStatus = 'DYNAMIC_BLOCK_DELETED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage =
                        $this->translate(
                            'Failed to delete dynamic block', 'default', $this->getUserLanguage()->zend2_locale
                        ) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    /**
     * Inserts block into specific slot on HTML page
     * using drag&drop via jQuery Draggable & Droppable.
     */
    public function insertAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageId = $request->getPost('pageId');
                $blockId = $request->getPost('blockId');
                $slotId = $request->getPost('slotId');
                $order = $request->getPost('order');

                $blockManager = $this->getBlockManager();
                $pageBlockId = $blockManager->insertBlockIntoPage($pageId, $blockId, $slotId, $order);
                if ($pageBlockId > 0)
                {
                    $jsonResponse->data = new \stdClass();
                    $jsonResponse->data->pageBlockId = $pageBlockId;
                    $jsonResponse->meta->customStatus = 'BLOCK_INSERTED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to insert block', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function reorderAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageId = $request->getPost('pageId');
                $slotId = $request->getPost('slotId');
                $pageBlocksOrder = (array)$request->getPost('pageBlocksOrder');

                $pageBlockCount = count($pageBlocksOrder);
                $blockManager = $this->getBlockManager();
                $reorderedCount = $blockManager->reorderBlocksInPageSlot($pageId, $slotId, $pageBlocksOrder);

                if (!$reorderedCount)
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to reorder blocks', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
                else if ($reorderedCount < $pageBlockCount)
                {
                    $jsonResponse->meta->customStatus = 'NOT_ALL_BLOCKS_WAS_REORDERED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'BLOCKS_REORDERED';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function reorderBlocksForAdminToolWndAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $dynamic = $request->getPost('dynamic');
                $blocks = (array)$request->getPost('blocks');

                $blockCount = count($blocks);
                $blockManager = $this->getBlockManager();
                $reorderedCount = $blockManager->reorderBlocksForAdminToolWnd($blocks, $dynamic);

                if (!$reorderedCount)
                {
                    $jsonResponse->meta->requestErrorMessage =
                        $this->translate(
                            'Failed to reorder blocks', 'default', $this->getUserLanguage()->zend2_locale
                        ) . '.';
                }
                else if ($reorderedCount < $blockCount)
                {
                    $jsonResponse->meta->customStatus = 'NOT_ALL_BLOCKS_WAS_REORDERED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'BLOCKS_REORDERED';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function moveToAnotherSlotAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageId = $request->getPost('pageId');
                $pageBlockId = $request->getPost('pageBlockId');
                $sourceSlotId = $request->getPost('sourceSlotId');
                $destinationSlotId = $request->getPost('destinationSlotId');
                $pageBlocksOrder = (array)$request->getPost('pageBlocksOrder');

                $pageBlockCount = count($pageBlocksOrder);
                $blockManager = $this->getBlockManager();
                list($moved, $reorderedCount) = $blockManager->moveBlockToAnotherSlot($pageId, $pageBlockId, $sourceSlotId, $destinationSlotId, $pageBlocksOrder);

                if (!$moved || !$reorderedCount)
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to move block to another slot', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
                else
                {
                    if ($reorderedCount < $pageBlockCount)
                    {
                        $jsonResponse->meta->customStatus = 'NOT_ALL_BLOCKS_WAS_REORDERED';
                    }
                    else
                    {
                        $jsonResponse->meta->customStatus = 'BLOCK_MOVED';
                    }
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    /**
     * This function updates only page block content - it does not touch page block configuration options.
     */
    public function updateAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');
                $blockContent = $request->getPost('blockContent');

                $blockManager = $this->getBlockManager();
                $saved = $blockManager->saveBlockContent($pageBlockId, $blockContent);
                if ($saved)
                {
                    $jsonResponse->meta->customStatus = 'BLOCK_CONTENT_SAVED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to update block content', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    /**
     * This function updates only page block title (not name!) - it does not touch page block configuration options.
     */
    public function updateTitleAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');
                $title = $request->getPost('title');

                $blockManager = $this->getBlockManager();
                $saved = $blockManager->savePageBlockContentsTitle($pageBlockId, $title);
                if ($saved)
                {
                    $jsonResponse->meta->customStatus = 'BLOCK_TITLE_SAVED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to update block title', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getSaveAsDynamicViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $pageId = $request->getPost('pageId');
                $pageBlockId = $request->getPost('pageBlockId');

                $blockId = $this->getBlockManager()->getBlockIdByPageBlockId($pageBlockId);

                $languages = $this->getLanguageManager()->getAllLanguages();
                if ($languages)
                {
                    $languages->buffer();
                }

                $languageCategories = array();
                $currentLanguageCategoryId = null;

                if ($pageId > 0)
                {
                    $currentLanguageCategoryId = $this->getPageManager()->getPageLanguageIdByPageId($pageId);
                }

                if (!$currentLanguageCategoryId)
                {
                    $languageCategories[] =
                        '---' . $this->translate(
                            'Please select', 'default', $this->getUserLanguage()->zend2_locale
                        ) . '---';
                }

                if ($languages)
                {
                    foreach ($languages as $language)
                    {
                        $languageCategories[$language->language_id] = $language->english_name;
                    }
                }

                $renderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');

                $saveAsDynamicView = new \Zend\View\Model\ViewModel();
                $saveAsDynamicView->setTemplate('blocks/block-save-as-dynamic-or-update-properties.phtml');
                $saveAsDynamicView->setVariables(
                    array(
                        'userLanguage' => $this->getUserLanguage(),
                        'pageId' => $pageId,
                        'blockId' => $blockId,
                        'pageBlockId' => $pageBlockId,
                        'languages' => $languages,
                        'currentBlockNames' => false,
                        'currentLanguageCategoryId' => $currentLanguageCategoryId,
                        'languageCategories' => $languageCategories
                    )
                );
                $saveAsDynamicViewHtml = $renderer->render($saveAsDynamicView);

                $jsonResponse->data = $saveAsDynamicViewHtml;
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function saveAsDynamicAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');
                $formData = $request->getPost('formData');
                $formData = new \Site\Custom\FlexibleContainer($formData);

                $allNamesFilled = false;

                $names = new \Site\Custom\FlexibleContainer();
                if ($formData->names)
                {
                    $allNamesFilled = true;
                    foreach ($formData->names as $languageId => $name)
                    {
                        if ($name != '')
                        {
                            $names[$languageId] = $name;
                        }
                        else
                        {
                            $allNamesFilled = false;
                            break;
                        }
                    }
                }

                $languageCategoryId = (int)$formData->languageCategoryId;

                $saved = false;

                if ($allNamesFilled && ($languageCategoryId > 0))
                {
                    $blockManager = $this->getBlockManager();
                    $saved = $blockManager->savePageBlockAsDynamic($pageBlockId, $names, $languageCategoryId);
                }

                if ($saved)
                {
                    $jsonResponse->meta->customStatus = 'BLOCK_SAVED';
                }
                else
                {
                    if (!$allNamesFilled)
                    {
                        $jsonResponse->meta->requestErrorMessage =
                            $this->translate('Could not save block as dynamic because at least one name is empty', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                    }
                    else
                    {
                        $jsonResponse->meta->requestErrorMessage =
                            $this->translate('Failed to save block as dynamic', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                    }
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function togglePublishingAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');

                $blockManager = $this->getBlockManager();
                $status = $blockManager->togglePageBlockPublishingAndReturnStatus($pageBlockId);
                $jsonResponse->meta->customStatus = $status ? 'PUBLISHED' : 'UNPUBLISHED';
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function changeTitleSizeAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $jsonResponse->data = new \stdClass();
                $jsonResponse->data->title = null;
                $jsonResponse->data->titleVisible = null;

                $pageBlockId = $request->getPost('pageBlockId');
                $action = strtolower($request->getPost('action', ''));

                if (($action == 'increase') || ($action == 'decrease'))
                {
                    $blockManager = $this->getBlockManager();
                    $titleSize = $blockManager->changePageBlockTitleSizeAndReturnSize($pageBlockId, $action);
                    $pageBlockContents = $blockManager->getPageBlockContentsByPageBlockId($pageBlockId);
                    if ($pageBlockContents)
                    {
                        $jsonResponse->data->title = $pageBlockContents->title;
                    }

                    $jsonResponse->data->titleSize = $titleSize;
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getManagementViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');

                $blockManager = $this->getBlockManager();
                $managementViewHtml = $blockManager->getPageBlockManagementView($pageBlockId);
                if ($managementViewHtml != '')
                {
                    $jsonResponse->data = $managementViewHtml;
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to fetch template', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getAddEditViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');

                $blockManager = $this->getBlockManager();
                $addEditViewHtml = $blockManager->getPageBlockAddEditView($pageBlockId);
                if ($addEditViewHtml != '')
                {
                    $jsonResponse->data = $addEditViewHtml;
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to fetch template', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getSettingsViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');

                $blockManager = $this->getBlockManager();
                $settingsViewHtml = $blockManager->getPageBlockSettingsView($pageBlockId);
                if ($settingsViewHtml != '')
                {
                    $jsonResponse->data = $settingsViewHtml;
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to fetch template', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function saveSettingsAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');
                $settings = $request->getPost('settings');

                $blockManager = $this->getBlockManager();
                $saved = $blockManager->savePageBlockSettings($pageBlockId, $settings);
                if ($saved)
                {
                    $jsonResponse->meta->customStatus = 'SETTINGS_SAVED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to save block settings', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function deleteAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');

                $deleted = $this->getBlockManager()->deletePageBlock($pageBlockId);
                if ($deleted)
                {
                    $jsonResponse->meta->customStatus = 'BLOCK_DELETED';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function renderBlockAgainAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $pageBlockId = $request->getPost('pageBlockId');
            $additionalData = $request->getPost('additionalData', array());
            $requestUri = trim(strip_tags((string)$request->getPost('requestUri')));

            $request->setUri($requestUri);

            $blockManager = $this->getBlockManager();
            $block = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
            $blockHtmlCode = $block->render($request, true, $additionalData);

            $jsonResponse->data = $blockHtmlCode;
            $jsonResponse->meta->customStatus = 'BLOCK_RENDERED';
            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getImageEditViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user)
            {
                $pageBlockId = $request->getPost('pageBlockId');
                $uniqueHashForFileName = $request->getPost('uniqueHashForFileName');
                $imageAlt = $request->getPost('imageAlt');

                $parentAttachment = null;
                $attachment = null;
                if ($uniqueHashForFileName != '')
                {
                    $attachmentManager = $this->getAttachmentManager();
                    $attachment = $attachmentManager->getAttachmentByUniqueHashForFileName($uniqueHashForFileName);
                    $parentAttachment = $attachment;
                }

                if ($attachment && ($attachment->parent_attachment_id > 0))
                {
                    $parentAttachment = $attachmentManager->getAttachmentById($attachment->parent_attachment_id);
                }

                $renderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');

                $imageEditView = new \Zend\View\Model\ViewModel();
                $imageEditView->setTemplate('blocks/block-image-edit.phtml');
                $imageEditView->setVariables(
                    array(
                        'userLanguage' => $this->getUserLanguage(),
                        'pageBlockId' => $pageBlockId,
                        'parentAttachment' => $parentAttachment,
                        'attachment' => $attachment,
                        'imageAlt' => $imageAlt
                    )
                );
                $imageEditViewHtml = $renderer->render($imageEditView);

                $jsonResponse->data = $imageEditViewHtml;
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function saveImageChangesAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $formData = $request->getPost('formData');
                $formData = new \Site\Custom\FlexibleContainer($formData);
                //$pageBlockId = $formData->pageBlockId;
                $attachmentId = $formData->attachmentId;
                $uniqueHashForFileName = $formData->uniqueHashForFileName;
                $imageCropRectCoordsString = $formData->imageCropRectCoords;
                //$imageAlt = $formData->imageAlt;

                $imageChangesSaved = false;
                $attachmentUpdated = false;

                $imageManipulator = $this->getImageManipulator();
                $imageCropRectCoords = $imageManipulator->convertRectCoordsStringToArray($imageCropRectCoordsString);

                if ($imageCropRectCoords)
                {
                    $parentAttachment = null;
                    $attachment = null;

                    if ($attachmentId > 0)
                    {
                        $attachmentManager = $this->getAttachmentManager();
                        $attachment = $attachmentManager->getAttachmentById($attachmentId);
                        $parentAttachment = $attachment;
                    }
                    else if ($uniqueHashForFileName != '')
                    {
                        $attachmentManager = $this->getAttachmentManager();
                        $attachment = $attachmentManager->getAttachmentByUniqueHashForFileName($uniqueHashForFileName);
                        $attachmentId = $attachment->attachment_id;
                        $parentAttachment = $attachment;
                    }

                    if ($attachment)
                    {
                        if ($attachment->parent_attachment_id > 0)
                        {
                            $parentAttachment = $attachmentManager->getAttachmentById($attachment->parent_attachment_id);
                        }
                        else
                        {
                            $newAttachmentId = $attachmentManager->createAttachmentCopyAndReturnId($attachmentId);
                            if ($newAttachmentId > 0)
                            {
                                $attachment = $attachmentManager->getAttachmentById($newAttachmentId);
                            }
                        }
                    }

                    if ($attachment)
                    {
                        $sourceFilePath = $parentAttachment->file_path;
                        $destinationFilePath = $attachment->file_path;
                        $imageChangesSaved = $imageManipulator->cropImageUsingRectCoordsStringAndSave($sourceFilePath, $imageCropRectCoordsString, $destinationFilePath);

                        if ($imageChangesSaved)
                        {
                            $fileSystem = $this->getFileSystem();

                            $attachment->file_size = $fileSystem->filesize($destinationFilePath);
                            $attachment->image_crop_rect_coords = $imageCropRectCoordsString;
                            $attachmentUpdated = $attachmentManager->updateAttachment($attachment);
                        }
                    }
                }

                if ($imageChangesSaved && $attachmentUpdated)
                {
                    $urlHelper = $this->getServiceLocator()->get('viewhelpermanager')->get('url');
                    $imageUrlParameters = $attachment->hash_for_file_name . ',' . $attachment->original_file_name;
                    $imageUrl = urldecode(
                        $urlHelper(
                            'home/default',
                            array(
                                'controller' => 'get-file', 'action' => 'index', 'parameters' => $imageUrlParameters
                            )
                        )
                    );

                    $jsonResponse->data = new \stdClass();
                    $jsonResponse->data->imageUrl = $imageUrl;
                    $jsonResponse->data->attachmentId = $attachment->attachment_id;
                    $jsonResponse->data->uniqueHashForFileName = $attachment->hash_for_file_name;
                    $jsonResponse->data->imageCropRectCoords = $imageCropRectCoordsString;
                    $jsonResponse->meta->customStatus = 'IMAGE_CHANGES_SAVED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'IMAGE_SAVING_CHANGES_ERROR';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateBlockPropertiesAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $blockId = $request->getPost('blockId');
                $formData = $request->getPost('formData');
                $formData = new \Site\Custom\FlexibleContainer($formData);

                $blockNames = $formData->names->toArray(true);

                $updated = $this->getBlockManager()->updateBlockNamesAndLanguageCategory(
                    $blockId, $blockNames, $formData->languageCategoryId
                );
                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'BLOCK_PROPERTIES_UPDATED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'BLOCK_PROPERTIES_UPDATING_ERROR';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }
}
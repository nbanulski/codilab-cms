<?php
namespace Site\Controller;

class BlockEmployeeDetailsController extends BaseController
{
    public function updateEmployeeNameAndSurnameAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');
                $employeeId = $request->getPost('employeeId');
                $nameAndSurname = $request->getPost('nameAndSurname');

                $updated = $this->updateEmployeeTextData(
                    $pageBlockId,
                    $employeeId,
                    'name_and_surname',
                    $nameAndSurname
                );

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate(
                            'Failed to update employee name and surname', 'default', $this->getUserLanguage()->zend2_locale
                        ) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateEmployeePositionAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');
                $employeeId = $request->getPost('employeeId');
                $position = $request->getPost('position');

                $updated = $this->updateEmployeeTextData(
                    $pageBlockId,
                    $employeeId,
                    'position',
                    $position
                );

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate(
                            'Failed to update employee position', 'default', $this->getUserLanguage()->zend2_locale
                        ) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateEmployeeAboutAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');
                $employeeId = $request->getPost('employeeId');
                $about = $request->getPost('about');

                $updated = $this->updateEmployeeTextData(
                    $pageBlockId,
                    $employeeId,
                    'about',
                    $about
                );

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate(
                            'Failed to update employee description', 'default', $this->getUserLanguage()->zend2_locale
                        ) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateEmployeeScientificCareerAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');
                $employeeId = $request->getPost('employeeId');
                $scientificCareer = $request->getPost('scientificCareer');

                $updated = $this->updateEmployeeTextData(
                    $pageBlockId,
                    $employeeId,
                    'scientific_career',
                    $scientificCareer
                );

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate(
                            'Failed to update employee scientific career', 'default', $this->getUserLanguage()->zend2_locale
                        ) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateEmployeeTheMostImportantPublicationsAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');
                $employeeId = $request->getPost('employeeId');
                $theMostImportantPublications = $request->getPost('theMostImportantPublications');

                $updated = $this->updateEmployeeTextData(
                    $pageBlockId,
                    $employeeId,
                    'most_important_publications',
                    $theMostImportantPublications
                );

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate(
                            'Failed to update information about the most important publications of employee', 'default', $this->getUserLanguage()->zend2_locale
                        ) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateEmployeeContactInfoAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');
                $employeeId = $request->getPost('employeeId');
                $contactInfo = $request->getPost('contactInfo');

                $updated = $this->updateEmployeeTextData(
                    $pageBlockId,
                    $employeeId,
                    'contact_info',
                    $contactInfo
                );

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate(
                            'Failed to update employee contact information', 'default', $this->getUserLanguage()->zend2_locale
                        ) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    protected function updateEmployeeTextData($pageBlockId, $employeeId, $dbColumnName, $text)
    {
        $blockManager = $this->getBlockManager();
        $employeeDetailsBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
        if (!$employeeDetailsBlock instanceof \Site\Block\EmployeeDetails\EmployeeDetails)
        {
            return false;
        }

        $updated = false;

        $dbColumnName = (string)$dbColumnName;
        $text = (string)$text;

        switch ($dbColumnName)
        {
            case 'name_and_surname':
                $updated = $employeeDetailsBlock->updateEmployeeNameAndSurname($employeeId, $text);
                break;
            case 'position':
                $updated = $employeeDetailsBlock->updateEmployeePosition($employeeId, $text);
                break;
            case 'about':
                $updated = $employeeDetailsBlock->updateEmployeeAbout($employeeId, $text);
                break;
            case 'scientific_career':
                $updated = $employeeDetailsBlock->updateEmployeeScientificCareer($employeeId, $text);
                break;
            case 'most_important_publications':
                $updated = $employeeDetailsBlock->updateEmployeeTheMostImportantPublications($employeeId, $text);
                break;
            case 'contact_info':
                $updated = $employeeDetailsBlock->updateEmployeeContactInfo($employeeId, $text);
                break;
        }

        return $updated;
    }
} 
<?php
namespace Site\Controller;

class BlockEventListController extends BaseController
{
    const BLOCK_EVENT_LIST_TABLE_NAME = 'block_event_list';
    const BLOCK_EVENT_LIST_CATEGORIES_TABLE_NAME = 'block_event_list_categories';

    protected $eventListBlock = null;

    /**
     * Getters.
     */

    public function getAddEditCategoryViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');

                $blockManager = $this->getBlockManager();
                $addEditCategoryViewHtml =
                    $blockManager->callPageBlockCustomMethod(
                        $pageBlockId, 'renderAddEditCategory', array($this->request, true)
                    )
                ;
                if ($addEditCategoryViewHtml != '')
                {
                    $jsonResponse->data = $addEditCategoryViewHtml;
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage =
                        $this->translate(
                            'Failed to fetch template', 'default', $this->getUserLanguage()->zend2_locale
                        ) . '.'
                    ;
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getCategoriesManagementViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');

                $blockManager = $this->getBlockManager();
                $categoriesManagementViewHtml =
                    $blockManager->callPageBlockCustomMethod(
                        $pageBlockId, 'renderCategoriesManagement', array($this->request, true)
                    )
                ;
                if ($categoriesManagementViewHtml != '')
                {
                    $jsonResponse->data = $categoriesManagementViewHtml;
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to fetch template', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getSignUpFormViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $blockManager = $this->getBlockManager();

            $pageBlockId = $request->getPost('pageBlockId');
            $contentId = $request->getPost('contentId');

            if ($pageBlockId <= 0)
            {
                $pageBlockId = $blockManager->getPageBlockIdByContentId($contentId);
            }

            $signUpFormViewHtml =
                $blockManager->callPageBlockCustomMethod(
                    $pageBlockId, 'renderSignUpForm', array($request, true)
                );

            if ($signUpFormViewHtml != '')
            {
                $jsonResponse->data = $signUpFormViewHtml;
            }
            else
            {
                $jsonResponse->meta->requestErrorMessage =
                    $this->translate(
                        'Failed to fetch template', 'default', $this->getUserLanguage()->zend2_locale
                    ) . '.'
                ;
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    protected function getCategories()
    {
        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_EVENT_LIST_CATEGORIES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable);
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    protected function getCategoryCountByContentId($contentId)
    {
        $contentId = (int)$contentId;
        if ($contentId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_EVENT_LIST_CATEGORIES_TABLE_NAME)
            ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')), $prefixColumnsWithTable)
            ->where(array('content_id' => $contentId));
        $resultSet = $dbManager->getHydratedResultSet($select);
        $row = $resultSet->current();
        $count = $row ? $row->count : false;

        return $count;
    }

    /**
     * Logic-functions.
     */

    public function addEditCategoryAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageBlockId = (int)$request->getPost('pageBlockId');
                $blockManager = $this->getBlockManager();
                $this->eventListBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $formData = array();

                if ($request->isPost())
                {
                    if ($request->getPost('dataPosted'))
                    {
                        $customStatus = 'INVALID_DATA';

                        $formData = $request->getPost('formData');
                        if ($formData)
                        {
                            $dataValid = true;

                            $currentCategoryId = isset($formData['categoryId']) ? $formData['categoryId'] : null;
                            $categoryTitle = isset($formData['categoryTitle']) ? trim($formData['categoryTitle']) : null;
                            $isPublished = isset($formData['publishNow']) ? $formData['publishNow'] : null;

                            // TODO: Data validation.

                            if ($dataValid)
                            {
                                $result = $this->createNewOrUpdateExistingCategory($pageBlockId,
                                    array(
                                        'categoryId' => $currentCategoryId,
                                        'categoryTitle' => $categoryTitle,
                                        'isPublished' => $isPublished ? 1 : 0,
                                        'authorId' => $user->user_id
                                    )
                                );
                            }

                            if ($dataValid && $result)
                            {
                                $customStatus =
                                    ($currentCategoryId > 0)
                                        ? 'EVENT_CATEGORY_MODIFIED'
                                        : 'EVENT_CATEGORY_ADDED'
                                ;
                            }
                            else
                            {
                                $customStatus =
                                    ($currentCategoryId > 0)
                                        ? 'EVENT_CATEGORY_MODIFYING_ERROR'
                                        : 'EVENT_CATEGORY_ADDING_ERROR'
                                ;
                            }
                        }

                        $jsonResponse->meta->customStatus = $customStatus;
                        $this->response->setContent((string)$jsonResponse);

                        return $this->response;
                    }
                    else
                    {
                        $categoryId = $request->getPost('categoryId');
                        if ($categoryId > 0)
                        {
                            if ($this->eventListBlock)
                            {
                                $categoryData = $this->eventListBlock->getCategoryById($categoryId);

                                $formData = array(
                                    'categoryId' => $categoryId,
                                    'categoryTitle' => $categoryData->name,
                                    'isPublished' => $categoryData->is_published ? 1 : 0,
                                    'authorId' => $user->user_id,
                                );
                            }
                        }
                    }
                }

                $defaultFormData = array(
                    'categoryId' => '',
                    'categoryTitle' => '',
                    'isPublished' => 0,
                );
                $formData = new \Site\Custom\FlexibleContainer(array_merge($defaultFormData, $formData));

                $metadata = new \Zend\Db\Metadata\Metadata($this->dbAdapter);
                $blockEventListCategoriesTableColumnsMetadata = $metadata->getColumns(
                    self::BLOCK_EVENT_LIST_TABLE_NAME
                );
                $maxLengths = array();
                if ($blockEventListCategoriesTableColumnsMetadata)
                {
                    foreach ($blockEventListCategoriesTableColumnsMetadata as $column)
                    {
                        $name = $column->getName();

                        $maxLengths[$name] = $column->getCharacterMaximumLength();
                    }
                }

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $addNewCategoryView = new \Zend\View\Model\ViewModel();
                $addNewCategoryView->setTemplate('blocks/event-list/add-edit-category.phtml');
                $addNewCategoryView->setVariables(
                    array(
                        'maxLengths' => $maxLengths,
                        'formData' => $formData
                    )
                );

                $jsonResponse->data = $phpRenderer->render($addNewCategoryView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function addEditEventAction($asGuest = false)
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageBlockId = (int)$request->getPost('pageBlockId');
                $blockManager = $this->getBlockManager();
                $this->eventListBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $formData = array();

                if (
                    $request->isPost() &&
                    ($this->eventListBlock instanceof \Site\Block\EventList\EventList)
                )
                {
                    if ($request->getPost('dataPosted'))
                    {
                        $formData = new \Site\Custom\FlexibleContainer($request->getPost('formData'));
                        $result = $this->eventListBlock->addOrEditEvent($formData, $asGuest);

                        if ($result)
                        {
                            $customStatus =
                                ($formData->eventId > 0)
                                    ? 'EVENT_MODIFIED'
                                    : 'EVENT_ADDED'
                            ;
                        }
                        else
                        {
                            $customStatus =
                                ($formData->eventId > 0)
                                    ? 'EVENT_MODIFYING_ERROR'
                                    : 'EVENT_ADDING_ERROR'
                            ;
                        }

                        $jsonResponse->meta->customStatus = $customStatus;
                        $this->response->setContent((string)$jsonResponse);

                        return $this->response;
                    }
                    else
                    {
                        $eventId = $request->getPost('eventId');
                        if ($eventId > 0)
                        {
                            $eventListBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                            if ($eventListBlock)
                            {
                                $event = $eventListBlock->getEventById($eventId);
                                $attachmentId = $event->attachment_id;
                                $attachment = $this->getUploadManager()->getAttachmentById($attachmentId);
                                $uniqueHashForFileName = $attachment->hash_for_filename;

                                $startTime = strtotime($event->event_date_start);
                                $endTime = strtotime($event->event_date_end);

                                $eventDayRange = date('Y-m-d', $startTime) . ' - ' . date('Y-m-d', $endTime);
                                $eventStartHour = date('H:i', $startTime);
                                $eventEndHour = date('H:i', $endTime);

                                $formData = array(
                                    'eventId' => $eventId,
                                    'eventCategoryId' => $event->category_id,
                                    'eventIsApproved' => $event->is_approved,
                                    'eventIsSigningUpEnabled' => $event->is_signing_up_enabled,
                                    'eventUniqueHashForFileName' => $uniqueHashForFileName,
                                    'eventTeaser' => $event->teaser,
                                    'eventContents' => $event->contents,
                                    'eventLabel' => $event->label,
                                    'isPublished' => $event->is_published ? 1 : 0,
                                    'eventIsPromoted' => $event->is_promoted ? 1 : 0,
                                    'authorId' => $user->user_id,
                                    'eventCreationDate' => $event->creation_date,
                                    'eventApprovalDate' => $event->approval_date,
                                    'eventLastUpdateAuthorId' => $event->last_update_author_id,
                                    'eventDayRange' => $eventDayRange,
                                    'eventStartHour' => $eventStartHour,
                                    'eventEndHour' => $eventEndHour,
                                    'eventAuthorEmail' => $event->author_email,
                                    'eventCleanUrl' => $event->clean_url,
                                    'eventTitle' => $event->name,
                                    'eventAuthorAdditionalContactData' => $event->additional_contact_data,
                                    'eventAttentions' => $event->attentions
                                );
                            }
                        }
                    }
                }

                $defaultFormData = array(
                    'eventId' => '',
                    'eventCategoryId' => null,
                    'eventIsApproved' => 0,
                    'eventIsSigningUpEnabled' => 0,
                    'eventUniqueHashForFileName' => '',
                    'eventTeaser' => '',
                    'eventContents' => '',
                    'eventLabel' => '',
                    'eventCreationDate' => '',
                    'eventApprovalDate' => '',
                    'eventLastUpdateAuthorId' => '',
                    'eventDayRange' => date('Y-m-d') . ' - ' . date('Y-m-d', strtotime('+7 day')),
                    'eventStartHour' => '',
                    'eventEndHour' => '',
                    'eventAuthorEmail' => '',
                    'eventCleanUrl' => '',
                    'eventTitle' => '',
                    'eventAuthorAdditionalContactData' => '',
                    'eventAttentions' => '',
                    'isPublished' => 0,
                    'eventIsPromoted' => 0
                );
                $formData = new \Site\Custom\FlexibleContainer(array_merge($defaultFormData, $formData));

                $metadata = new \Zend\Db\Metadata\Metadata($this->dbAdapter);
                $tableName = 'block_event_list';
                $blockEventTableColumnsMetadata = $metadata->getColumns($tableName);
                $maxLengths = array();
                if ($blockEventTableColumnsMetadata)
                {
                    foreach ($blockEventTableColumnsMetadata as $column)
                    {
                        $name = $column->getName();

                        $maxLengths[$name] = $column->getCharacterMaximumLength();
                    }
                }

                $eventCategoriesOptions = array();
                $categories = $this->getCategories();
                if ($categories)
                {
                    foreach ($categories as $category)
                    {
                        $eventCategoriesOptions[$category->category_id] = $category->name;
                    }
                }

                $this->addEditView->setVariables(
                    array(
                        'eventCategoriesOptions' => $eventCategoriesOptions
                    )
                );

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $addNewEventView = new \Zend\View\Model\ViewModel();
                $addNewEventView->setTemplate('blocks/event/add-edit.phtml');
                $addNewEventView->setVariables(
                    array(
                        'eventCategoriesOptions' => $eventCategoriesOptions,
                        'maxLengths' => $maxLengths,
                        'formData' => $formData
                    )
                );

                $jsonResponse->data = $phpRenderer->render($addNewEventView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function addEventAsGuestAction()
    {
        return $this->addEditEventAction(true);
    }

    public function createNewCategory($pageBlockId, $categoryData)
    {
        if (($pageBlockId <= 0 ) || !$categoryData)
        {
            return false;
        }

        $authorId = isset($categoryData['authorId']) ? $categoryData['authorId'] : null;
        $isPublished = $categoryData['isPublished'] ? 1 : 0;
        if ($authorId)
        {
            $authorId = (int)$authorId;
        }
        $title = trim($categoryData['categoryTitle']);

        if ($title == '')
        {
            return false;
        }

        $created = false;
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $blockManager = $this->getBlockManager();
        $pageBlockContentId = $blockManager->getPageBlockContentIdByPageBlockId($pageBlockId);

        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager->beginTransaction();

        $insert = $sql->insert(self::BLOCK_EVENT_LIST_CATEGORIES_TABLE_NAME);
        $insert->columns(
            array(
                    'content_id',
                    'is_published',
                    'last_update_author_id',
                    'last_update_date',
                    'name',
                )
            )
            ->values(
                array(
                    'content_id' => $pageBlockContentId,
                    'is_published' => $isPublished,
                    'last_update_author_id' => $authorId,
                    'last_update_date' => $lastUpdateDate,
                    'name' => $title
                )
            );
        $categoryId = $dbManager->executeInsertAndGetLastInsertId($insert);
        if ($categoryId > 0)
        {
            $created = true;
        }

        if ($created)
        {
            $dbManager->commit();

            $this->selectThisCategoryAsDefaultIfThereIsOnlyOne($pageBlockId, $categoryId);
        }
        else
        {
            $dbManager->rollback();
        }

        return $created;
    }

    public function deleteCategoryAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageBlockId = $request->getPost('pageBlockId');
                $categoryId = $request->getPost('categoryId');
                $eventListBlock = $this->getBlockManager()->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $deleted = $eventListBlock->deleteCategoryById($categoryId);

                if ($deleted)
                {
                    $jsonResponse->meta->customStatus = 'EVENT_CATEGORY_DELETED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'EVENT_CATEGORY_DELETION_ERROR';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function deleteEventAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageBlockId = $request->getPost('pageBlockId');
                $eventId = $request->getPost('eventId');
                $eventListBlock = $this->getBlockManager()->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $deleted = $eventListBlock->deleteEventById($eventId);

                if ($deleted)
                {
                    $jsonResponse->meta->customStatus = 'EVENT_DELETED';

                    $this->addEventToSystemHistory(
                        'Event (id: ' . $eventId . ') has been deleted by {{user_login}}.'
                    );
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'EVENT_DELETION_ERROR';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function deleteEventImageAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $deleted = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $eventId = $request->getPost('eventId');
                $imageId = $request->getPost('imageId');

                $eventListBlock = $this->getBlockManager()->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($eventListBlock instanceof \Site\Block\EventList\EventList)
                {
                    $deleted = $eventListBlock->deleteEventImageByEventIdAndImageId($eventId, $imageId);
                }

                if ($deleted)
                {
                    $jsonResponse->meta->customStatus = 'IMAGE_DELETED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'IMAGE_DELETION_ERROR';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function downloadUsersSignedUpForEventAsCsvAction()
    {
        return $this->downloadUsersSignedUpForEventAs('csv');
    }

    public function downloadUsersSignedUpForEventAsXlsAction()
    {
        return $this->downloadUsersSignedUpForEventAs('xls');
    }

    public function reorderEventImagesAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageBlockId = (int)$request->getPost('pageBlockId');
                $eventId = (int)$request->getPost('eventId');
                $images = $request->getPost('images');

                $blockManager = $this->getBlockManager();
                $eventListBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $reorderingSuccessful = $eventListBlock->reorderEventImages($eventId, $images);

                if ($reorderingSuccessful)
                {
                    $jsonResponse->meta->customStatus = 'IMAGES_REORDERED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'IMAGES_REORDERING_ERROR';
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to reorder event images', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function signUpForEventAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $customStatus = 'FAILED_TO_SIGN_UP_FOR_EVENT';

            $pageBlockId = $request->getPost('pageBlockId');
            $subscriptionId = $request->getPost('subscriptionId');
            $contentId = $request->getPost('contentId');
            $formData = $request->getPost('formData');
            $formData = new \Site\Custom\FlexibleContainer($formData);
            $formData->signUpSubscriptionId = $subscriptionId;

            //print_r($request->getPost());
            //print_r($formData);

            if ($pageBlockId <= 0)
            {
                $pageBlockId = $this->getBlockManager()->getPageBlockIdByContentId($contentId);
            }

            $eventListBlock = $this->getBlockManager()->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
            if ($eventListBlock instanceof \Site\Block\EventList\EventList)
            {
                $signedUpForEvent = $eventListBlock->signUpForEvent($formData);
                if ($signedUpForEvent === true)
                {
                    $customStatus = 'USER_ALREADY_SIGNED_UP_FOR_EVENT';
                }
                else if ($signedUpForEvent > 0)
                {
                    $customStatus = 'SUBSCRIPTION_SAVED';
                }
            }

            $jsonResponse->meta->customStatus = $customStatus;

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function toggleCategoryPublishingAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $updated = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $categoryId = $request->getPost('categoryId');
                $published = (int)$request->getPost('published') ? 1 : 0;

                $blockManager = $this->getBlockManager();
                $eventListBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($eventListBlock instanceof \Site\Block\EventList\EventList)
                {
                    $updated = $eventListBlock->toggleCategoryPublished($categoryId, $published);
                }

                if (!$updated)
                {
                    $jsonResponse->meta->requestErrorMessage =
                        $this->translate(
                            'Failed to update event category', 'default',
                            $this->getUserLanguage()->zend2_locale
                        ) . '.'
                    ;
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function toggleEventPublishingAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $updated = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $eventId = $request->getPost('eventId');
                $published = (int)$request->getPost('published') ? 1 : 0;

                $blockManager = $this->getBlockManager();
                $eventListBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($eventListBlock instanceof \Site\Block\EventList\EventList)
                {
                    $updated = $eventListBlock->toggleEventPublished($eventId, $published);
                }

                if (!$updated)
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to publish / unpublish event', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateExistingCategory($pageBlockId, $categoryData)
    {
        if (($pageBlockId <= 0 ) || !$categoryData)
        {
            return false;
        }

        $categoryId = (int)$categoryData['categoryId'];
        $isPublished = $categoryData['isPublished'] ? 1 : 0;
        $authorId = isset($categoryData['authorId']) ? (int)$categoryData['authorId'] : null;
        $title = trim($categoryData['categoryTitle']);

        if (($categoryId <= 0) || ($authorId <= 0) || ($title == ''))
        {
            return false;
        }

        $updated = false;
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_EVENT_LIST_CATEGORIES_TABLE_NAME);
        $update
            ->set(
                array(
                    'is_published' => $isPublished,
                    'last_update_author_id' => $authorId,
                    'last_update_date' => $lastUpdateDate,
                    'name' => $title,
                )
            )
            ->where(
                array(
                    'category_id' => $categoryId
                )
            );
        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $category = $this->eventListBlock->getCategoryById($categoryId);
            if ($category &&
                ($category->is_published == $isPublished) &&
                ($category->last_update_author_id == $authorId) &&
                ($category->name == $title))
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    protected function createNewOrUpdateExistingCategory($pageBlockId, $formData)
    {
        $categoryId = isset($formData['categoryId']) ? (int)$formData['categoryId'] : null;

        if ($categoryId > 0)
        {
            return $this->updateExistingCategory($pageBlockId, $formData);
        }
        else
        {
            return $this->createNewCategory($pageBlockId, $formData);
        }
    }

    protected function downloadUsersSignedUpForEventAs($format = 'csv')
    {
        $user = $this->sessionData();

        if ($user)
        {
            $parameters = $this->getParametersForCurrentPageFromRequest($this->getRequest());
            $contentId = $parameters->cid;
            $eventId = $parameters->eid;

            $pageBlockId = $this->getBlockManager()->getPageBlockIdByContentId($contentId);

            $eventListBlock = $this->getBlockManager()->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
            if ($eventListBlock instanceof \Site\Block\EventList\EventList)
            {
                $fileName = $eventListBlock->exportListOfUsersSignedUpForEventAs($eventId, $format);
                if ($fileName != '')
                {
                    $downloadUrl = '/eng/get-file/temp,' . $fileName;

                    return $this->redirect()->toUrl($downloadUrl);
                }
            }

            return $this->response;
        }

        $response = $this->getResponse();
        $response->setStatusCode(404);

        return $response;
    }

    protected function selectThisCategoryAsDefaultIfThereIsOnlyOne($pageBlockId, $categoryId)
    {
        $pageBlockId = (int)$pageBlockId;
        $categoryId = (int)$categoryId;

        if (($pageBlockId <= 0) || ($categoryId <= 0))
        {
            return false;
        }

        $blockManager = $this->getBlockManager();
        $pageBlockContentId = $blockManager->getPageBlockContentIdByPageBlockId($pageBlockId);

        if ($this->getCategoryCountByContentId($pageBlockContentId) > 0)
        {
            $result = false;

            $eventListBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
            if ($eventListBlock instanceof \Site\Block\EventList\EventList)
            {
                $result = $eventListBlock->selectCategoryAsDefault($categoryId);
            }

            return $result;
        }

        return false;
    }
}
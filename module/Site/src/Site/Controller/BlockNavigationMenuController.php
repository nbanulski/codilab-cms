<?php
namespace Site\Controller;

class BlockNavigationMenuController extends BaseController
{
    const BLOCK_NAVIGATION_MENU_ITEMS_TABLE_NAME = 'block_navigation_menu_items';
    const BLOCK_NAVIGATION_MENU_CATEGORIES_TABLE_NAME = 'block_navigation_menu_categories';

    public function addEditCategoryAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $formData = array();

                if ($request->isPost())
                {
                    $pageBlockId = (int)$request->getPost('pageBlockId');
                    $blockManager = $this->getBlockManager();
                    $this->navigationMenuBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);

                    if ($request->getPost('dataPosted'))
                    {
                        $customStatus = 'INVALID_DATA';

                        $formData = new \Site\Custom\FlexibleContainer($request->getPost('formData'));
                        $result = $this->navigationMenuBlock->addOrEditCategory($formData);

                        if ($result)
                        {
                            $customStatus =
                                ($formData->categoryId > 0) ?
                                    'MENU_CATEGORY_MODIFIED' :
                                    'MENU_CATEGORY_ADDED';
                        }
                        else
                        {
                            $customStatus =
                                ($formData->categoryId > 0) ?
                                    'MENU_CATEGORY_MODIFYING_ERROR' :
                                    'MENU_CATEGORY_ADDING_ERROR';
                        }

                        $jsonResponse->meta->customStatus = $customStatus;
                        $this->response->setContent((string)$jsonResponse);

                        return $this->response;
                    }
                    else
                    {
                        $categoryId = $request->getPost('categoryId');
                        if ($categoryId > 0)
                        {
                            if ($this->navigationMenuBlock)
                            {
                                $categoryData = $this->navigationMenuBlock->getCategoryById($categoryId);

                                $formData = array(
                                    'categoryId' => $categoryId,
                                    'categoryTitle' => $categoryData->name,
                                    'categoryIsPublished' => $categoryData->is_published ? 1 : 0,
                                    'categoryLastUpdateAuthorId' => $user->user_id
                                );
                            }
                        }
                    }
                }

                $defaultFormData = array(
                    'categoryId' => '',
                    'categoryTitle' => '',
                    'categoryIsPublished' => 0
                );
                $formData = new \Site\Custom\FlexibleContainer(array_merge($defaultFormData, $formData));

                $metadata = new \Zend\Db\Metadata\Metadata($this->dbAdapter);
                $blockNavigationMenuCategoriesTableColumnsMetadata = $metadata->getColumns(
                    self::BLOCK_NAVIGATION_MENU_CATEGORIES_TABLE_NAME
                );
                $maxLengths = array();
                if ($blockNavigationMenuCategoriesTableColumnsMetadata)
                {
                    foreach ($blockNavigationMenuCategoriesTableColumnsMetadata as $column)
                    {
                        $name = $column->getName();

                        $maxLengths[$name] = $column->getCharacterMaximumLength();
                    }
                }

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $addNewCategoryView = new \Zend\View\Model\ViewModel();
                $addNewCategoryView->setTemplate('blocks/navigation-menu/add-edit-category.phtml');
                $addNewCategoryView->setVariables(
                    array(
                        'maxLengths' => $maxLengths,
                        'formData' => $formData
                    )
                );

                $jsonResponse->data = $phpRenderer->render($addNewCategoryView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function addEditItemAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageBlockId = (int)$request->getPost('pageBlockId');
                $blockManager = $this->getBlockManager();
                $this->navigationMenuBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $formData = array();

                if ($request->isPost())
                {
                    if (
                        $request->getPost('dataPosted') &&
                        ($this->navigationMenuBlock instanceof \Site\Block\NavigationMenu\NavigationMenu)
                    )
                    {
                        $customStatus = 'INVALID_DATA';

                        $formData = new \Site\Custom\FlexibleContainer($request->getPost('formData'));
                        $result = $this->navigationMenuBlock->addOrEditItem($formData);

                        if ($result)
                        {
                            $customStatus = ($formData->itemId > 0) ? 'MENU_ITEM_MODIFIED' : 'MENU_ITEM_ADDED';
                        }
                        else
                        {
                            $customStatus = ($formData->itemId > 0) ? 'MENU_ITEM_MODIFYING_ERROR' : 'MENU_ITEM_ADDING_ERROR';
                        }

                        $jsonResponse->meta->customStatus = $customStatus;
                        $this->response->setContent((string)$jsonResponse);

                        return $this->response;
                    }
                    else
                    {
                        $itemId = $request->getPost('itemId');
                        if ($itemId > 0)
                        {
                            if ($this->navigationMenuBlock)
                            {
                                $item = $this->navigationMenuBlock->getItemById($itemId);
                                $attachmentId = $item->attachment_id;
                                $attachment = $this->getUploadManager()->getAttachmentById($attachmentId);
                                $uniqueHashForFileName = $attachment->hash_for_filename;

                                $formData = array(
                                    'itemId' => $itemId,
                                    'itemParentId' => $item->parent_id,
                                    'itemCategoryId' => $item->category_id,
                                    'itemPageId' => $item->page_id,
                                    'itemUrlAddressType' => (int)$item->url_address_type,
                                    'itemType' => (int)$item->type,
                                    'itemExternalUrl' => ($item->url_address_type == 1) ? $item->external_url : '',
                                    'itemUniqueHashForFileName' => $uniqueHashForFileName,
                                    'itemOriginalFileName' => $attachment->original_file_name,
                                    'itemTextIcon' => $item->bootstrap_glyphicon,
                                    'itemImageAlt' => $item->image_alt,
                                    'itemTitle' => $item->name,
                                    'itemIsPublished' => $item->is_published ? 1 : 0,
                                    'itemLastUpdateAuthorId' => $user->user_id
                                );
                            }
                        }
                    }
                }

                $defaultFormData = array(
                    'itemId' => '',
                    'itemParentId' => null,
                    'itemCategoryId' => null,
                    'itemPageId' => null,
                    'itemUrlAddressType' => 0,
                    'itemType' => 0,
                    'itemExternalUrl' => '',
                    'itemUniqueHashForFileName' => '',
                    'itemOriginalFileName' => '',
                    'itemTextIcon' => 'asterisk',
                    'itemImageAlt' => '',
                    'itemTitle' => '',
                    'itemIsPublished' => 0
                );
                $formData = new \Site\Custom\FlexibleContainer(array_merge($defaultFormData, $formData));

                $metadata = new \Zend\Db\Metadata\Metadata($this->dbAdapter);
                $blockNavigationMenuItemsTableColumnsMetadata = $metadata->getColumns(
                    self::BLOCK_NAVIGATION_MENU_ITEMS_TABLE_NAME
                );
                $maxLengths = array();
                if ($blockNavigationMenuItemsTableColumnsMetadata)
                {
                    foreach ($blockNavigationMenuItemsTableColumnsMetadata as $column)
                    {
                        $name = $column->getName();

                        $maxLengths[$name] = $column->getCharacterMaximumLength();
                    }
                }

                $itemCategoriesOptions = array();
                $categories = $this->getCategories();
                if ($categories)
                {
                    foreach ($categories as $category)
                    {
                        $itemCategoriesOptions[$category->category_id] = $category->name;
                    }
                }

                $itemParentItemsOptions = array();

                $this->addEditView->setVariables(
                    array(
                        'itemParentItemsOptions' => $itemParentItemsOptions,
                        'itemCategoriesOptions' => $itemCategoriesOptions
                    )
                );

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $addNewItemView = new \Zend\View\Model\ViewModel();
                $addNewItemView->setTemplate('blocks/navigation-menu/add-edit-item.phtml');
                $addNewItemView->setVariables(
                    array(
                        'itemParentItemsOptions' => $itemParentItemsOptions,
                        'itemCategoriesOptions' => $itemCategoriesOptions,
                        'maxLengths' => $maxLengths,
                        'formData' => $formData
                    )
                );

                $jsonResponse->data = $phpRenderer->render($addNewItemView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function deleteCategoryAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageBlockId = $request->getPost('pageBlockId');
                $categoryId = $request->getPost('categoryId');
                $navigationMenuBlock = $this->getBlockManager()->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $deleted = $navigationMenuBlock->deleteCategoryById($categoryId);

                if ($deleted)
                {
                    $jsonResponse->meta->customStatus = 'MENU_CATEGORY_DELETED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'MENU_CATEGORY_DELETION_ERROR';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function deleteItemAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageBlockId = $request->getPost('pageBlockId');
                $itemId = $request->getPost('itemId');

                $deleted = false;

                $navigationMenuBlock = $this->getBlockManager()->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($navigationMenuBlock instanceof \Site\Block\NavigationMenu\NavigationMenu)
                {
                    $deleted = $navigationMenuBlock->deleteItemById($itemId);
                }

                if ($deleted)
                {
                    $jsonResponse->meta->customStatus = 'MENU_ITEM_DELETED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'MENU_ITEM_DELETION_ERROR';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getAddEditCategoryViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');

                $blockManager = $this->getBlockManager();
                $addEditCategoryViewHtml = $blockManager->callPageBlockCustomMethod(
                    $pageBlockId,
                    'renderAddEditCategory',
                    array($this->request, true)
                );
                if ($addEditCategoryViewHtml != '')
                {
                    $jsonResponse->data = $addEditCategoryViewHtml;
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate(
                            'Failed to fetch template', 'default', $this->getUserLanguage()->zend2_locale
                        ) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getCategoriesManagementViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');

                $blockManager = $this->getBlockManager();
                $categoriesManagementViewHtml = $blockManager->callPageBlockCustomMethod(
                    $pageBlockId,
                    'renderCategoriesManagement',
                    array($this->request, true)
                );
                if ($categoriesManagementViewHtml != '')
                {
                    $jsonResponse->data = $categoriesManagementViewHtml;
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate(
                            'Failed to fetch template', 'default', $this->getUserLanguage()->zend2_locale
                        ) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getParentItemsAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageBlockId = (int)$request->getPost('pageBlockId');
                $categoryId = (int)$request->getPost('categoryId');
                $itemId = (int)$request->getPost('itemId');

                $parentItemsOptionsHtml =
                    '<option value="">' .
                    $this->translate('None', 'default', $this->getUserLanguage()->zend2_locale) .
                    '</option>';

                $blockManager = $this->getBlockManager();
                $navigationMenuBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $parentItems = $navigationMenuBlock->getParentItems($categoryId, $itemId);
                if ($parentItems)
                {
                    foreach ($parentItems as $parentItem)
                    {
                        $parentItemsOptionsHtml .=
                            '<option value="' . $parentItem->item_id . '">' .
                            $parentItem->title .
                            '</option>';
                    }
                }

                $jsonResponse->data = $parentItemsOptionsHtml;
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function reorderItemsAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageBlockId = (int)$request->getPost('pageBlockId');
                $parentItemId = $request->getPost('parentItemId');
                $parentItemId = $parentItemId ? (int)$parentItemId : null;
                $items = $request->getPost('items');

                $blockManager = $this->getBlockManager();
                $navigationMenuBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $reorderingSuccessful = $navigationMenuBlock->reorderItems($parentItemId, $items);

                if ($reorderingSuccessful)
                {
                    $jsonResponse->meta->customStatus = 'MENU_ITEMS_REORDERED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'MENU_ITEMS_REORDERING_ERROR';
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to reorder menu items', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function toggleCategoryPublishingAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageBlockId = (int)$request->getPost('pageBlockId');
                $categoryId = (int)$request->getPost('categoryId');
                $published = (int)$request->getPost('published') ? 1 : 0;

                $blockManager = $this->getBlockManager();
                $navigationMenuBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $updated = $navigationMenuBlock->toggleCategoryPublished($categoryId, $published);
                if (!$updated)
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate(
                            'Could not update menu category', 'default', $this->getUserLanguage()->zend2_locale
                        ) . '.';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function toggleItemPublishingAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageBlockId = (int)$request->getPost('pageBlockId');
                $itemId = (int)$request->getPost('itemId');
                $published = (int)$request->getPost('published') ? 1 : 0;

                $blockManager = $this->getBlockManager();
                $navigationMenuBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $updated = $navigationMenuBlock->toggleItemPublished($itemId, $published);
                if (!$updated)
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to update menu item', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    protected function getCategoryCountByContentId($contentId)
    {
        $contentId = (int)$contentId;
        if ($contentId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_NAVIGATION_MENU_CATEGORIES_TABLE_NAME)
            ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')), $prefixColumnsWithTable)
            ->where(array('content_id' => $contentId));
        $resultSet = $dbManager->getHydratedResultSet($select);
        $row = $resultSet->current();
        $count = $row ? $row->count : false;

        return $count;
    }

    protected function getCategories()
    {
        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_NAVIGATION_MENU_CATEGORIES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable);
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    protected function createNewOrUpdateExistingCategory($pageBlockId, $formData)
    {
        $categoryId = isset($formData['categoryId']) ? (int)$formData['categoryId'] : null;

        if ($categoryId > 0)
        {
            return $this->updateExistingCategory($pageBlockId, $formData);
        }
        else
        {
            return $this->createNewCategory($pageBlockId, $formData);
        }
    }

    protected function selectThisCategoryAsDefaultIfThereIsOnlyOne($pageBlockId, $categoryId)
    {
        $pageBlockId = (int)$pageBlockId;
        $categoryId = (int)$categoryId;

        if (($pageBlockId <= 0) || ($categoryId <= 0))
        {
            return false;
        }

        $blockManager = $this->getBlockManager();
        $pageBlockContentId = $blockManager->getPageBlockContentIdByPageBlockId($pageBlockId);

        if ($this->getCategoryCountByContentId($pageBlockContentId) > 0)
        {
            $navigationMenuBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);

            return $navigationMenuBlock->selectCategoryAsDefault($categoryId);
        }

        return false;
    }
}
<?php
namespace Site\Controller;

use Zend\Mvc\Controller\AbstractController;
use Zend\Http\Request;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Sql\Sql;
use Zend\Mvc\MvcEvent;
use Site\Custom\FlexibleContainer;

class BlockManager extends AbstractController
{
    protected $cmsConfig = null;
    protected $serviceLocator = null;
    protected $dbAdapter = null;
    protected $sql = null;
    protected $request = null;
    protected $language = null;
    protected $userLanguage = null;
    protected $identity = null;
    protected $databaseManager = null;
    protected $languageManager = null;
    protected $pageManager = null;
    protected $attachmentManager = null;
    protected $uploadManager = null;
    protected $blocksFetchedFromDb = array();
    protected $blocksPropertiesFetchedFromDb = array();

    public function __construct(
        $cmsConfig, ServiceLocatorInterface $serviceLocator, Sql $sql, Request $request,
        \stdClass $language, FlexibleContainer $userLanguage, $identity
    )
    {
        $this->cmsConfig = $cmsConfig;
        $this->serviceLocator = $serviceLocator;
        $this->sql = $sql;
        $this->request = $request;
        $this->dbAdapter = $sql->getAdapter();
        $this->language = $language;
        $this->userLanguage = $userLanguage;
        $this->identity = $identity;
    }

    public function onDispatch(MvcEvent $e)
    {

    }

    public function getDatabaseManager()
    {
        if ($this->databaseManager == null)
        {
            $this->databaseManager = $this->serviceLocator->get('DatabaseManager');
        }

        return $this->databaseManager;
    }

    public function setDatabaseManager(DatabaseManager $databaseManager)
    {
        $this->databaseManager = $databaseManager;

        return $this;
    }

    public function getLanguageManager()
    {
        if ($this->languageManager == null)
        {
            $this->languageManager = new LanguageManager($this->cmsConfig, $this->serviceLocator, $this->language, $this->identity);
            $this->languageManager->setServiceLocator($this->serviceLocator);
            $this->languageManager->setDatabaseManager($this->getDatabaseManager());
        }

        return $this->languageManager;
    }

    public function getPageManager()
    {
        if ($this->pageManager == null)
        {
            $this->pageManager = new PageManager(
                $this->cmsConfig, $this->serviceLocator,
                $this->language, $this->userLanguage, $this->identity
            );
            $this->pageManager->setServiceLocator($this->serviceLocator);
            $this->pageManager->setDatabaseManager($this->getDatabaseManager());
        }

        return $this->pageManager;
    }

    public function getAttachmentManager()
    {
        if ($this->attachmentManager == null)
        {
            $fileSystem = new \FileSystem\Component\FileSystem();
            $this->attachmentManager = new AttachmentManager(
                $this->cmsConfig, $this->serviceLocator, $this->sql, $this->request,
                $this->language, $this->userLanguage, $this->identity, $fileSystem
            );
            $this->attachmentManager->setServiceLocator($this->serviceLocator);
        }

        return $this->attachmentManager;
    }

    public function getUploadManager()
    {
        if ($this->uploadManager == null)
        {
            $fileSystem = new \FileSystem\Component\FileSystem();
            $this->uploadManager = new UploadManager(
                $this->cmsConfig, $this->serviceLocator, $this->sql, $this->request,
                $this->language, $this->userLanguage, $this->identity, $fileSystem
            );
            $this->uploadManager->setServiceLocator($this->serviceLocator);
        }

        return $this->uploadManager;
    }

    /**
     * Block factory-methods.
     */

    public function createBlockUsingPageBlockIdAndLanguageId($pageBlockId, $specificLanguageId = null)
    {
        $pageBlockId = (int)$pageBlockId;
        $languageId = ($specificLanguageId !== null) ? (int)$specificLanguageId : (int)$this->language->language_id;

        if (($pageBlockId <= 0) || ($languageId <= 0))
        {
            return false;
        }

        $pageBlock = $this->getPageBlockById($pageBlockId);
        if ($pageBlock)
        {
            $block = $this->getBlockById($pageBlock->block_id);
            if ($block)
            {
                return $this->createBlockUsingPhpClassNameBlockInfoAndPageBlockInfo(
                        $block->php_class_name, $block, $pageBlock, $languageId
                );
            }
        }

        return false;
    }

    /**
     * Warning - This function can return also dynamic block's instance if contentId is being used multiple times!
     */
    public function createBlockUsingContentId($contentId, $specificLanguageId = null)
    {
        $contentId = (int)$contentId;
        $languageId = ($specificLanguageId !== null) ? (int)$specificLanguageId : (int)$this->language->language_id;

        if (($contentId <= 0) || ($languageId <= 0))
        {
            return false;
        }

        $pageBlockId = $this->getPageBlockIdByContentId($contentId);

        return $this->createBlockUsingPageBlockIdAndLanguageId($pageBlockId, $languageId);
    }

    public function createBlockUsingPhpClassNameAndBlockData($phpClassName, $blockData)
    {
        if ($blockData)
        {
            $user = $this->identity;

            return $this->createBlock($phpClassName, $blockData, $user);
        }

        return false;
    }

    public function createBlockUsingPhpClassNameBlockInfoAndPageBlockInfo($phpClassName, $block, $pageBlock, $specificLanguageId = null)
    {
        $languageId = ($specificLanguageId !== null) ? (int)$specificLanguageId : (int)$this->language->language_id;

        if (!$block || !$pageBlock || ($languageId <= 0))
        {
            return false;
        }

        $pageBlock = $this->getPageBlockById($pageBlock->page_block_id);
        $blockProperties = $this->getBlockPropertiesByBlockIdAndLanguageId($block->block_id, $languageId);
        $pageBlockContents = $this->getPageBlockContentsByPageBlockId($pageBlock->page_block_id);
        $blockData = $this->makeBlockData($block, $blockProperties, $pageBlock, $pageBlockContents);

        if ($blockData)
        {
            $user = $this->identity;

            return $this->createBlock($phpClassName, $blockData, $user);
        }

        return false;
    }

    public function createBlockUsingPhpClassNameAndAllBlockInfo($phpClassName, $block, $blockProperties, $pageBlock, $pageBlockContents)
    {
        $blockData = $this->makeBlockData($block, $blockProperties, $pageBlock, $pageBlockContents);
        if ($blockData)
        {
            $user = $this->identity;

            return $this->createBlock($phpClassName, $blockData, $user);
        }

        return false;
    }

    /**
     * Block data fetch functions.
     */

    public function getAllBlockPropertiesByBlockId($blockId)
    {
        $blockId = (int)$blockId;
        if ($blockId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $select = $this->sql->select();
        $select
            ->from('blocks_properties')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('block_id' => $blockId));
        $resultSet = $this->getDatabaseManager()->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getAllBlocks($specificLanguageId = null, $languageCategoryId = false, $phpClassName = false, $blockNameQuery = false)
    {
        $languageId = ($specificLanguageId !== null) ? (int)$specificLanguageId : (int)$this->language->language_id;
        if ($languageCategoryId !== false)
        {
            $languageCategoryId = (int)$languageCategoryId ? : null;
        }
        if ($phpClassName !== false)
        {
            $phpClassName = trim($phpClassName);
        }
        if ($blockNameQuery !== false)
        {
            $blockNameQuery = trim($blockNameQuery);
        }

        if (($languageCategoryId !== false) && ($languageCategoryId !== null) && ($languageCategoryId <= 0))
        {
            return false;
        }

        $prefixColumnsWithTable = true;

        $dbManager = $this->getDatabaseManager();

        $select = $this->sql->select();
        $select
            ->from(array('b' => 'blocks'))
            ->columns(array('block_id', 'php_class_name','glyphicon', 'config'), $prefixColumnsWithTable)
            ->join(
                array('bp' => 'blocks_properties'),
                'bp.block_id = b.block_id',
                array('name', 'default_content'),
                $select::JOIN_LEFT
            )
            ->where(array('b.is_enabled' => 1, 'b.is_dynamic' => 0, 'bp.language_id' => $languageId));

        if ($languageCategoryId > 0)
        {
            $select->where->equalTo('b.language_category_id', $languageCategoryId);
        }

        if ($phpClassName != '')
        {
            $select->where->equalTo('b.php_class_name', $phpClassName);
        }

        if ($blockNameQuery != '')
        {
            $select->where->literal('MATCH(bp.name) AGAINST(? IN BOOLEAN MODE)', '*' . $blockNameQuery . '*');
        }

        $select
            ->group('b.block_id')
            ->order('b.order ASC, bp.name');
        $blocksResultSet = $dbManager->getHydratedResultSet($select);

        $select2 = $this->sql->select();
        $select2
            ->from(array('b' => 'blocks'))
            ->columns(array('block_id', 'php_class_name','glyphicon', 'config'), $prefixColumnsWithTable)
            ->join(
                array('bp' => 'blocks_properties'),
                'bp.block_id = b.block_id',
                array('name', 'default_content'),
                $select2::JOIN_LEFT
            )
            ->where(array('b.is_enabled' => 1, 'b.is_dynamic' => 1, 'bp.language_id' => $languageId));

        if ($languageCategoryId > 0)
        {
            $select2->where->equalTo('b.language_category_id', $languageCategoryId);
        }

        if ($phpClassName != '')
        {
            $select2->where->equalTo('b.php_class_name', $phpClassName);
        }

        if ($blockNameQuery != '')
        {
            $select2->where->literal('MATCH(bp.name) AGAINST(? IN BOOLEAN MODE)', '*' . $blockNameQuery . '*');
        }

        $select2
            ->group('b.block_id')
            ->order('b.order ASC, bp.name');
        $dynamicBlocksResultSet = $dbManager->getHydratedResultSet($select2);

        return array($blocksResultSet, $dynamicBlocksResultSet);
    }

    public function getAllSystemBlockNames($specificLanguageId = null)
    {
        $languageId = ($specificLanguageId !== null) ? (int)$specificLanguageId : (int)$this->language->language_id;
        $prefixColumnsWithTable = true;

        $dbManager = $this->getDatabaseManager();

        $select = $this->sql->select();
        $select
            ->from(array('b' => 'blocks'))
            ->columns(array('php_class_name'), $prefixColumnsWithTable)
            ->join(
                array('bp' => 'blocks_properties'),
                'bp.block_id = b.block_id',
                array('name'),
                $select::JOIN_LEFT
            )
            ->where(array('b.is_enabled' => 1, 'b.is_dynamic' => 0, 'bp.language_id' => $languageId))
            ->group('b.block_id')
            ->order('bp.name');
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getBlockById($blockId)
    {
        $blockId = (int)$blockId;
        if ($blockId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $select = $this->sql->select();
        $select
            ->from('blocks')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('block_id' => $blockId));
        $resultSet = $this->getDatabaseManager()->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getBlockIdByPageBlockId($pageBlockId)
    {
        $pageBlockId = (int)$pageBlockId;
        if ($pageBlockId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $select = $this->sql->select();
        $select
            ->from('pages_blocks')
            ->columns(array('block_id'), $prefixColumnsWithTable)
            ->where(array('page_block_id' => $pageBlockId));
        $resultSet = $this->getDatabaseManager()->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();

            return $row ? $row->block_id : false;
        }

        return false;
    }

    public function getBlockLanguageCategoryIdByBlockId($blockId)
    {
        $blockId = (int)$blockId;
        if ($blockId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $select = $this->sql->select();
        $select
            ->from('blocks')
            ->columns(array('language_category_id'), $prefixColumnsWithTable)
            ->where(array('block_id' => $blockId));
        $resultSet = $this->getDatabaseManager()->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();

            return $row ? $row->language_category_id : false;
        }

        return false;
    }

    public function getBlockPropertiesByBlockIdAndLanguageId($blockId, $languageId)
    {
        $key = $blockId . '_' . $languageId;

        if (!isset($this->blocksPropertiesFetchedFromDb[$key]))
        {
            $prefixColumnsWithTable = false;

            $select = $this->sql->select();
            $select->from('blocks_properties')
                ->columns(array('name', 'default_content'), $prefixColumnsWithTable)
                ->where(array('block_id' => (int)$blockId, 'language_id' => (int)$languageId));
            $resultSet = $this->getDatabaseManager()->getHydratedResultSet($select);
            if ($resultSet)
            {
                $blockProperties = $resultSet->current();
            }
            else
            {
                $blockProperties = false;
            }

            $this->blocksPropertiesFetchedFromDb[$key] = $blockProperties;
        }

        return $this->blocksPropertiesFetchedFromDb[$key];
    }

    public function getDynamicBlockCountByDynamicBlockId($blockId)
    {
        $blockId = (int)$blockId;
        if ($blockId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $select = $this->sql->select();
        $select
            ->from('pages_blocks')
            ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')), $prefixColumnsWithTable)
            ->where(array('block_id' => (int)$blockId));
        $resultSet = $this->getDatabaseManager()->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();

            return $row ? $row->count : false;
        }

        return false;
    }

    public function getPageBlockById($pageBlockId)
    {
        $prefixColumnsWithTable = false;

        $select = $this->sql->select();
        $select->from('pages_blocks')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('page_block_id' => (int)$pageBlockId));
        $resultSet = $this->getDatabaseManager()->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getPageBlocksByPageIdBlockIdAndSlotId($pageId, $blockId, $slotId)
    {
        $prefixColumnsWithTable = false;

        $select = $this->sql->select();
        $select->from('pages_blocks')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array(
                'page_id' => (int)$pageId,
                'block_id' => (int)$blockId,
                'slot_number' => (int)$slotId
            ));
        $resultSet = $this->getDatabaseManager()->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getPageBlockContentsAssociation($pageBlockId)
    {
        $prefixColumnsWithTable = false;

        $select = $this->sql->select();
        $select->from('pages_blocks_contents_association')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('page_block_id' => (int)$pageBlockId))
            ->limit(1);
        $resultSet = $this->getDatabaseManager()->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getPageBlockContentIdByPageBlockId($pageBlockId)
    {
        $prefixColumnsWithTable = false;

        $select = $this->sql->select();
        $select->from('pages_blocks_contents_association')
            ->columns(array('content_id'), $prefixColumnsWithTable)
            ->where(array('page_block_id' => (int)$pageBlockId))
            ->limit(1);
        $resultSet = $this->getDatabaseManager()->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();
            if ($row)
            {
                return $row->content_id;
            }
        }

        return false;
    }

    public function getPageBlockContentsByPageBlockId($pageBlockId)
    {
        $pageBlockContentsAssociation = $this->getPageBlockContentsAssociation($pageBlockId);
        if ($pageBlockContentsAssociation)
        {
            $contentId = $pageBlockContentsAssociation->content_id;

            return $this->getPageBlockContentsByContentId($contentId);
        }

        return false;
    }

    public function getPageBlockContentsByContentId($contentId)
    {
        $contentId = (int)$contentId;
        if ($contentId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $select = $this->sql->select();
        $select->from('pages_blocks_contents')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('content_id' => $contentId));
        $resultSet = $this->getDatabaseManager()->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getPageBlockDataByPageBlockId($pageBlockId, $languageId)
    {
        $pageBlock = $this->getPageBlockById($pageBlockId);
        if (!$pageBlock)
        {
            return false;
        }

        $block = $this->getBlockById($pageBlock->block_id);
        $blockProperties = $this->getBlockPropertiesByBlockIdAndLanguageId($pageBlock->block_id, $languageId);
        $pageBlockContents = $this->getPageBlockContentsByPageBlockId($pageBlockId);

        if (!$block || !$blockProperties || !$pageBlockContents)
        {
            return false;
        }

        return $this->makeBlockData($block, $blockProperties, $pageBlock, $pageBlockContents);
    }

    public function getPageBlockIdByContentId($contentId)
    {
        $contentId = (int)$contentId;
        if ($contentId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $select = $this->sql->select();
        $select->from('pages_blocks_contents_association')
        ->columns(array('page_block_id'), $prefixColumnsWithTable)
        ->where(array('content_id' => $contentId))
        ->limit(1);
        $resultSet = $this->getDatabaseManager()->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();
            if ($row)
            {
                return (int)$row->page_block_id;
            }
        }

        return false;
    }

    /**
     * Block logic functions.
     */
    public function getFullBlockNamespaceByPhpClassName($phpClassName)
    {
        if ($phpClassName != '')
        {
            $phpClassName = trim($phpClassName);
            $blockFQN = '\\Site\\Block\\' . $phpClassName . '\\' . $phpClassName;

            return $blockFQN;
        }

        return null;
    }

    public function makeBlockData($block, $blockProperties, $pageBlock, $pageBlockContents)
    {
        if (!$block || !$blockProperties || !$pageBlock || !$pageBlockContents)
        {
            return false;
        }

        $blockContent = array(
            'content_id' => $pageBlockContents->content_id,
            'last_update_author_id' => $pageBlockContents->last_update_author_id,
            'last_update_date' => $pageBlockContents->last_update_date,
            'title' => $pageBlockContents->title,
            'config' => $pageBlockContents->config,
            'content' => $pageBlockContents->content
        );

        $blockData = array(
            'page_id' => $pageBlock->page_id,
            'block_id' => $block->block_id,
            'page_block_id' => $pageBlock->page_block_id,
            'is_system' => $block->is_system,
            'is_dynamic' => $block->is_dynamic,
            'is_parent_block_dynamic' => $pageBlock->is_parent_block_dynamic, // More consistent variable selection.
            'is_published' => $pageBlock->is_published,
            'has_data_in_search_index' => $block->has_data_in_search_index,
            'php_class_name' => $block->php_class_name,
            'slot_number' => $pageBlock->slot_number,
            'order' => $pageBlock->order,
            'language_category_id' => $block->language_category_id,
            'last_update_author_id' => $block->last_update_author_id,
            'last_update_date' => $block->last_update_date,
            'config' => $pageBlock->config,
            'name' => $blockProperties->name,
            'content' => (object)$blockContent
        );

        return (object)$blockData;
    }

    public function deleteDynamicBlock($blockId)
    {
        $blockId = (int)$blockId;
        if ($blockId <= 0)
        {
            return false;
        }

        $dbManager = $this->getDatabaseManager();

        $delete = $this->sql->delete();
        $delete
            ->from('blocks')
            ->where(
                array(
                    'block_id' => $blockId,
                    'is_system' => 0
                )
            );
        $result = $dbManager->executePreparedStatement($delete);
        if ($result)
        {
            return true;
        }

        return false;
    }

    public function insertBlockIntoPage($pageId, $blockId, $slotId, $order, $specificLanguageId = null)
    {
        $pageId = (int)$pageId;
        $blockId = (int)$blockId;
        $slotId = (int)$slotId;
        $order = (int)$order;
        $languageId = ($specificLanguageId !== null) ? (int)$specificLanguageId : (int)$this->language->language_id;

        if (($pageId <= 0) || ($blockId <= 0) || ($slotId <= 0) || ($order < 0) || ($languageId <= 0))
        {
            return false;
        }

        $publishBlocksAfterInsertion = $this->cmsConfig->publish_blocks_after_insertion ? true : false;

        $block = $this->getBlockById($blockId);
        $blockProperties = $this->getBlockPropertiesByBlockIdAndLanguageId($blockId, $languageId);
        $session = $this->identity;
        if ($block && $blockProperties && $session)
        {
            $insertedPageBlockId = false;

            $userId = (int)$session->user_id;

            $dbManager = $this->getDatabaseManager();
            $sql = $this->sql;
            $platform = $sql->getAdapter()->getPlatform();
            $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

            $dbManager->beginTransaction();

            // Reorder existing blocks before inserting new block:
            $orderColumnQuoted = $platform->quoteIdentifier('order');
            $update = $sql
                ->update('pages_blocks')
                ->set(array('order' => new \Zend\Db\Sql\Expression($orderColumnQuoted . ' + 1')))
                ->where($orderColumnQuoted . ' >= ' . $order)
                ->where($orderColumnQuoted . ' < 65535');
            $dbManager->executePreparedStatement($update);

            $insert = $sql
                ->insert('pages_blocks')
                ->columns(
                    array(
                        'page_id',
                        'block_id',
                        'is_parent_block_dynamic',
                        'slot_number',
                        'order',
                        'is_published',
                        'last_update_author_id',
                        'last_update_date',
                        'config'
                    )
                )
                ->values(
                    array(
                        'page_id' => $pageId,
                        'block_id' => $blockId,
                        'is_parent_block_dynamic' => $block->is_dynamic,
                        'slot_number' => $slotId,
                        'order' => $order,
                        'is_published' => $publishBlocksAfterInsertion ? 1 : 0,
                        'last_update_author_id' => $userId,
                        'last_update_date' => $lastUpdateDate,
                        'config' => ''
                    )
                );

            $pageBlockId = $dbManager->executeInsertAndGetLastInsertId($insert);
            if ($pageBlockId > 0)
            {
                if ($block->is_dynamic)
                {
                    $contentId = $block->shared_content_id;
                }
                else
                {
                    $insert = $sql->insert('pages_blocks_contents');
                    $insert
                        ->columns(
                            array(
                                'last_update_author_id',
                                'last_update_date',
                                'title',
                                'config',
                                'content'
                            )
                        )
                        ->values(
                            array(
                                'last_update_author_id' => $userId,
                                'last_update_date' => $lastUpdateDate,
                                'title' => $blockProperties->name,
                                'config' => $block->config,
                                'content' => $blockProperties->default_content
                            )
                        );

                    $contentId = $dbManager->executeInsertAndGetLastInsertId($insert);
                }

                if ($contentId > 0)
                {
                    $insert = $sql->insert('pages_blocks_contents_association');
                    $insert
                        ->columns(
                            array(
                                'page_block_id',
                                'content_id'
                            )
                        )
                        ->values(
                            array(
                                'page_block_id' => $pageBlockId,
                                'content_id' => $contentId
                            )
                        );
                    $associationId = $dbManager->executeInsertAndGetLastInsertId($insert);
                    if ($associationId)
                    {
                        $blockInstance = $this->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                        if ($blockInstance)
                        {
                            try
                            {
                                $result = $this->callPageBlockCustomMethod($pageBlockId, 'onAfterInserting', $slotId);
                            }
                            catch (\Site\Exception\BlockHasNoMethodException $e)
                            {
                                $result = true;
                            }

                            if ($result == true) // Block does not have this method or this method returns "true".
                            {
                                $insertedPageBlockId = $pageBlockId;
                            }
                        }
                    }
                }
            }

            if ($insertedPageBlockId > 0)
            {
                $dbManager->commit();

                return $insertedPageBlockId;
            }

            $dbManager->rollback();
        }

        return false;
    }

    public function reorderBlocksInPageSlot($pageId, $slotId, $pageBlocksOrder)
    {
        $pageId = (int)$pageId;
        $slotId = (int)$slotId;
        $pageBlocksOrder = (array)$pageBlocksOrder;

        if (($pageId <= 0) || ($slotId <= 0) || !$pageBlocksOrder)
        {
            return false;
        }

        $reorderedCount = 0;

        foreach ($pageBlocksOrder as $info)
        {
            $pageBlockId = (int)$info['pageBlockId'];
            $order = (int)$info['order'];

            if ($pageBlockId > 0)
            {
                $update = $this->sql
                    ->update('pages_blocks')
                    ->set(array('order' => $order))
                    ->where(array('page_block_id' => $pageBlockId));
                $result = $this->getDatabaseManager()->executePreparedStatement($update);
                if ($result)
                {
                    $reorderedCount++;
                }
            }
        }

        return $reorderedCount;
    }

    public function reorderBlocksForAdminToolWnd($blocks, $dynamic)
    {
        $blocks = (array)$blocks;
        $dynamic = (bool)$dynamic;

        if (!$blocks)
        {
            return false;
        }

        $reorderedCount = 0;

        foreach ($blocks as $info)
        {
            $blockId = (int)$info['blockId'];
            $order = (int)$info['order'];

            if ($blockId > 0)
            {
                $update = $this->sql
                    ->update('blocks')
                    ->set(array('order' => $order))
                    ->where(array('block_id' => $blockId));
                $result = $this->getDatabaseManager()->executePreparedStatement($update);
                if ($result)
                {
                    $reorderedCount++;
                }
            }
        }

        return $reorderedCount;
    }

    public function moveBlockToAnotherSlot($pageId, $pageBlockId, $sourceSlotId, $destinationSlotId, $pageBlocksOrder)
    {
        $pageId = (int)$pageId;
        $pageBlockId = (int)$pageBlockId;
        $sourceSlotId = (int)$sourceSlotId;
        $destinationSlotId = (int)$destinationSlotId;
        $pageBlocksOrder = (array)$pageBlocksOrder;

        if (($pageId <= 0) || ($pageBlockId <= 0) || ($sourceSlotId <= 0) || ($destinationSlotId <= 0) || !$pageBlocksOrder)
        {
            return false;
        }

        $moved = false;

        $update = $this->sql
            ->update('pages_blocks')
            ->set(array('slot_number' => $destinationSlotId))
            ->where(array('page_block_id' => $pageBlockId));
        $result = $this->getDatabaseManager()->executePreparedStatement($update);
        if ($result)
        {
            $moved = true;

            $reorderedCount = $this->reorderBlocksInPageSlot($pageId, $destinationSlotId, $pageBlocksOrder);
        }

        return array($moved, $reorderedCount);
    }

    public function savePageBlockContentsTitle($pageBlockId, $title)
    {
        $pageBlockId = (int)$pageBlockId;
        $title = (string)$title;

        if (($pageBlockId <= 0) || ($title === null))
        {
            return false;
        }

        $dbManager = $this->getDatabaseManager();
        $sql = $this->sql;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $pageBlockContents = $this->getPageBlockContentsByPageBlockId($pageBlockId);
        if ($pageBlockContents)
        {
            $update = $sql
                ->update('pages_blocks_contents')
                ->set(array('title' => $title, 'last_update_date' => $lastUpdateDate))
                ->where(array('content_id' => $pageBlockContents->content_id));
            $result = $dbManager->executePreparedStatement($update);
            if ($result)
            {
                $pageBlockContents = $this->getPageBlockContentsByPageBlockId($pageBlockId);
                if ($pageBlockContents)
                {
                    return ($pageBlockContents->title === $title);
                }
            }
        }

        return false;
    }

    public function saveBlockContent($pageBlockId, $blockContent, $specificLanguageId = null)
    {
        $pageBlockId = (int)$pageBlockId;
        $blockContent = (string)$blockContent;
        $languageId = ($specificLanguageId !== null) ? (int)$specificLanguageId : (int)$this->language->language_id;

        if (($pageBlockId <= 0) || ($blockContent === null) || ($languageId <= 0))
        {
            return false;
        }

        $pageBlock = $this->getPageBlockById($pageBlockId);
        if ($pageBlock)
        {
            $block = $this->getBlockById($pageBlock->block_id);
            if ($block)
            {
                $phpClassName = $block->php_class_name;
                if ($phpClassName != '')
                {
                    $blockInstance = $this->createBlockUsingPhpClassNameBlockInfoAndPageBlockInfo($phpClassName, $block, $pageBlock, $languageId);
                    if ($blockInstance)
                    {
                        return $blockInstance->saveContent($blockContent);
                    }
                }
            }
        }

        return false;
    }

    public function savePageBlockAsDynamic($pageBlockId, $names, $languageCategoryId = null)
    {
        $pageBlockId = (int)$pageBlockId;
        $languageCategoryId = (int)$languageCategoryId ? : null;

        if (
            ($pageBlockId <= 0) || !$names || (!is_array($names) && (!$names instanceof \Traversable)) ||
            (($languageCategoryId !== null) && ($languageCategoryId <= 0))
        )
        {
            return false;
        }

        $saved = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $this->sql;

        $dbManager->beginTransaction();

        $pageBlock = $this->getPageBlockById($pageBlockId);
        if ($pageBlock)
        {
            $blockId = $pageBlock->block_id;
            $block = $this->getBlockById($blockId);
            $session = $this->identity;

            if ($block && $session)
            {
                $userId = (int)$session->user_id;
                $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

                $pageBlockContents = $this->getPageBlockContentsByPageBlockId($pageBlockId);
                $block = $this->getBlockById($pageBlock->block_id);
                if ($pageBlockContents && $block)
                {
                    $insert = $sql->insert('blocks');
                    $insert
                        ->columns(
                            array(
                                'shared_content_id',
                                'is_system',
                                'is_enabled',
                                'is_dynamic',
                                'has_data_in_search_index',
                                'language_category_id',
                                'last_update_author_id',
                                'last_update_date',
                                'php_class_name',
                                'config'
                            )
                        )
                        ->values(
                            array(
                                'shared_content_id' => $pageBlockContents->content_id,
                                'is_system' => 0,
                                'is_enabled' => 1,
                                'is_dynamic' => 1,
                                'has_data_in_search_index' => $block->has_data_in_search_index,
                                'language_category_id' => $languageCategoryId,
                                'last_update_author_id' => $userId,
                                'last_update_date' => $lastUpdateDate,
                                'php_class_name' => $block->php_class_name,
                                'config' => $pageBlockContents->config
                            )
                        );
                    $dynamicBlockId = $dbManager->executeInsertAndGetLastInsertId($insert);
                    if ($dynamicBlockId > 0)
                    {
                        // Add new name for every language:
                        $namesAdded = false;

                        foreach ($names as $languageId => $name)
                        {
                            $insert = $sql->insert('blocks_properties');
                            $insert
                                ->columns(
                                    array(
                                        'block_id',
                                        'language_id',
                                        'name',
                                        'default_content'
                                    )
                                )
                                ->values(
                                    array(
                                        'block_id' => $dynamicBlockId,
                                        'language_id' => (int)$languageId,
                                        'name' => trim((string)$name),
                                        'default_content' => '' // Dynamic blocks doesn't need content.
                                    )
                                );
                            $dynamicBlockPropertiesId = $dbManager->executeInsertAndGetLastInsertId($insert);
                            if ($dynamicBlockPropertiesId > 0)
                            {
                                $namesAdded = true;
                            }
                            else
                            {
                                $namesAdded = false;

                                break;
                            }
                        }

                        if ($namesAdded)
                        {
                            // Update current block 'is_parent_block_dynamic' field to 1 for indicating that this block
                            // is an instance of dynamic block. Change also it's block_id to it's new (dynamic) parent.
                            $update = $sql->update('pages_blocks');
                            $update
                                ->set(
                                    array(
                                        'block_id' => $dynamicBlockId,
                                        'is_parent_block_dynamic' => '1'
                                    )
                                )
                                ->where(array('page_block_id' => $pageBlockId));
                            $result = $dbManager->executePreparedStatement($update);
                            if ($result)
                            {
                                $pageBlock = $this->getPageBlockById($pageBlockId);
                                if ($pageBlock->is_parent_block_dynamic == 1)
                                {
                                    $saved = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        if ($saved)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $saved;
    }

    public function setDynamicBlockInPageSlotPublished(
        $pageId, $blockId, $slotId, $published = true
    ) // TODO: Use transaction.
    {
        $blockId = (int)$blockId;

        if (!$this->isBlockDynamic($blockId))
        {
            return true; // TODO: Decide if it should be "true" or "false" in this case.
        }

        $page = $this->getPageManager()->getPageById($pageId);
        if (!$page)
        {
            // Page does not exists. Return "true", because it could be deleted a few seconds ago.

            return true;
        }

        $isPublished = $published ? 1 : 0;

        $pageBlocksInPageSlot = $this->getPageBlocksByPageIdBlockIdAndSlotId($pageId, $blockId, $slotId);
        if ($pageBlocksInPageSlot)
        {
            foreach ($pageBlocksInPageSlot as $pageBlock)
            {
                $update = $this->sql->update('pages_blocks');
                $update
                    ->set(array('is_published' => $isPublished))
                    ->where(array('page_block_id' => $pageBlock->page_block_id));
                $result = $this->getDatabaseManager()->executePreparedStatement($update);

                /*if (!$result)
                {
                    return false;
                }*/
            }
        }

        return true;
    }

    public function togglePageBlockPublishingAndReturnStatus($pageBlockId)
    {
        $pageBlockId = (int)$pageBlockId;

        if ($pageBlockId <= 0)
        {
            return false;
        }

        $pageBlock = $this->getPageBlockById($pageBlockId);
        if ($pageBlock)
        {
            $previousStatus = $pageBlock->is_published ? 1 : 0;

            $update = $this->sql->update('pages_blocks');
            $update->set(array('is_published' => !$previousStatus))
                ->where(array('page_block_id' => $pageBlockId));
            $this->getDatabaseManager()->executePreparedStatement($update);

            $pageBlock = $this->getPageBlockById($pageBlockId);
            $currentStatus = $pageBlock->is_published ? 1 : 0;

            if ($currentStatus != $previousStatus)
            {
                // Change was made successful, so return the current status.

                return $currentStatus;
            }
        }

        return false;
    }

    public function updateBlockNamesAndLanguageCategory($blockId, $blockNames, $languageCategoryId = false)
    {
        $blockId = (int)$blockId;
        $blockNames = (array)$blockNames;

        if ($languageCategoryId !== false)
        {
            $languageCategoryId = (int)$languageCategoryId ? : null;
        }

        if (
            ($blockId <= 0) || !$blockNames ||
            (($languageCategoryId !== false) && ($languageCategoryId !== null) && ($languageCategoryId <= 0))
        )
        {
            return false;
        }

        $updated = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $this->sql;

        $dbManager->beginTransaction();

        foreach ($blockNames as $languageId => $name)
        {
            $updated = false;

            $languageId = (int)$languageId;
            $name = trim($name);

            if (($languageId <= 0) || ($name == ''))
            {
                break;
            }

            $update = $sql->update('blocks_properties');
            $update
                ->set(
                    array(
                        'name' => $name
                    )
                )
                ->where(
                    array(
                        'block_id' => $blockId,
                        'language_id' => $languageId
                    )
                );
            $result = $dbManager->executePreparedStatement($update);
            if ($result)
            {
                $properties = $this->getBlockPropertiesByBlockIdAndLanguageId($blockId, $languageId);
                if ($properties && ($properties->name == $name))
                {
                    $updated = true;
                }
            }
        }

        if ($updated && ($languageCategoryId !== false))
        {
            $updated = false;

            $update = $sql->update('blocks');
            $update
                ->set(
                    array(
                        'language_category_id' => $languageCategoryId
                    )
                )
                ->where(
                    array(
                        'block_id' => $blockId
                    )
                );
            $result = $dbManager->executePreparedStatement($update);
            if ($result)
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    /**
     * @param type $pageBlockId
     * @param type $action
     * @return boolean or 0. 0 means that title is hidden.
     */
    public function changePageBlockTitleSizeAndReturnSize($pageBlockId, $action)
    {
        $pageBlockId = (int)$pageBlockId;
        $action = strtolower((string)$action);

        if (($pageBlockId <= 0) || !in_array($action, array('increase', 'decrease')))
        {
            return false;
        }

        $pageBlock = $this->getPageBlockById($pageBlockId);
        if ($pageBlock)
        {
            $pageBlockContent = $this->getPageBlockContentsByPageBlockId($pageBlockId);
            if ($pageBlockContent)
            {
                $settings = $this->convertConfigToFlexibleContainer($pageBlockContent->config);
                if ($settings)
                {
                    $titleSize = (int)trim($settings['titleSize']);
                    if ($action == 'increase')
                    {
                        $newTitleSize = $titleSize - 1;
                    }
                    else
                    {
                        $newTitleSize = $titleSize + 1;
                    }

                    $settings['titleVisible'] = ($newTitleSize > 6) ? 0 : 1;

                    $newTitleSize = min(max($newTitleSize, 1), 6);

                    $settings['titleSize'] = $newTitleSize;

                    $saved = $this->savePageBlockSettings($pageBlockId, $settings->toArray());
                    if ($saved)
                    {
                        return ($settings['titleVisible']) ? $newTitleSize : 0;
                    }
                }
            }
        }

        return false;
    }

    public function getPageBlockManagementView($pageBlockId, $specificLanguageId = null)
    {
        $pageBlockId = (int)$pageBlockId;
        $languageId = ($specificLanguageId !== null) ? (int)$specificLanguageId : (int)$this->language->language_id;

        if (($pageBlockId <= 0) || ($languageId <= 0))
        {
            return false;
        }

        $pageBlock = $this->getPageBlockById($pageBlockId);
        if ($pageBlock)
        {
            $block = $this->getBlockById($pageBlock->block_id);
            if ($block)
            {
                $phpClassName = $block->php_class_name;
                if ($phpClassName != '')
                {
                    $blockInstance = $this->createBlockUsingPhpClassNameBlockInfoAndPageBlockInfo($phpClassName, $block, $pageBlock, $languageId);
                    if ($blockInstance)
                    {
                        return $blockInstance->renderManagement($this->request, true);
                    }
                }
            }
        }

        return false;
    }

    public function getPageBlockAddEditView($pageBlockId, $specificLanguageId = null)
    {
        $pageBlockId = (int)$pageBlockId;
        $languageId = ($specificLanguageId !== null) ? (int)$specificLanguageId : (int)$this->language->language_id;

        if (($pageBlockId <= 0) || ($languageId <= 0))
        {
            return false;
        }

        $pageBlock = $this->getPageBlockById($pageBlockId);
        if ($pageBlock)
        {
            $block = $this->getBlockById($pageBlock->block_id);
            if ($block)
            {
                $phpClassName = $block->php_class_name;
                if ($phpClassName != '')
                {
                    $blockInstance = $this->createBlockUsingPhpClassNameBlockInfoAndPageBlockInfo($phpClassName, $block, $pageBlock, $languageId);
                    if ($blockInstance)
                    {
                        return $blockInstance->renderAddEdit($this->request, true);
                    }
                }
            }
        }

        return false;
    }

    public function getPageBlockSettingsView($pageBlockId, $specificLanguageId = null)
    {
        $pageBlockId = (int)$pageBlockId;
        $languageId = ($specificLanguageId !== null) ? (int)$specificLanguageId : (int)$this->language->language_id;

        if (($pageBlockId <= 0) || ($languageId <= 0))
        {
            return false;
        }

        $pageBlock = $this->getPageBlockById($pageBlockId);
        if ($pageBlock)
        {
            $block = $this->getBlockById($pageBlock->block_id);
            if ($block)
            {
                $phpClassName = $block->php_class_name;
                if ($phpClassName != '')
                {
                    $blockInstance = $this->createBlockUsingPhpClassNameBlockInfoAndPageBlockInfo($phpClassName, $block, $pageBlock, $languageId);
                    if ($blockInstance)
                    {
                        return $blockInstance->renderSettings(true);
                    }
                }
            }
        }

        return false;
    }

    public function getPageBlockSettings($pageBlockId)
    {
        $pageBlockId = (int)$pageBlockId;

        if ($pageBlockId <= 0)
        {
            return false;
        }

        $pageBlockContents = $this->getPageBlockContentsByPageBlockId($pageBlockId);
        if (!$pageBlockContents)
        {
            return false;
        }

        $settings = new FlexibleContainer();
        $xmlConfig = simplexml_load_string(
            $pageBlockContents->config, '\Site\Custom\SimpleXmlElementExtended', LIBXML_NOCDATA
        );
        if ($xmlConfig)
        {
            foreach ($xmlConfig as $name => $value)
            {
                $settings[$name] = (string)$value;
            }
        }

        return $settings;
    }

    public function savePageBlockSettings($pageBlockId, $settings)
    {
        $pageBlockId = (int)$pageBlockId;
        //$settings = (array)$settings; // TODO: Conversion using toIterator() function.

        if (($pageBlockId <= 0) || !$settings)
        {
            return false;
        }

        $saved = false;

        $pageBlock = $this->getPageBlockById($pageBlockId);
        $pageBlockContentId = $this->getPageBlockContentIdByPageBlockId($pageBlockId);
        if ($pageBlock && ($pageBlockContentId !== false))
        {
            $settings = new FlexibleContainer($settings);

            $dbManager = $this->getDatabaseManager();
            $dbManager->beginTransaction();

            if ($pageBlock->is_parent_block_dynamic)
            {
                // Save to pages_blocks only:
                $saved = $this->savePageBlockSettingsToSpecificTable($pageBlockId, $settings, 'pages_blocks');
            }
            else
            {
                // Save to pages_blocks_contents and to pages_blocks:
                $saved = $this->savePageBlockSettingsToSpecificTable($pageBlockId, $settings, 'pages_blocks');
                if ($saved)
                {
                    $contentId = (int)$pageBlockContentId;
                    $saved = $this->savePageBlockSettingsToSpecificTable(
                        $pageBlockId, $settings, 'pages_blocks_contents', $contentId
                    );
                }
            }

            if ($saved && !empty($settings['blockOccurrencesInPages']))
            {
                $pageBlock = $this->getPageBlockById($pageBlockId);
                $saved = $this->saveDynamicBlockOccurrencesInPages(
                    $pageBlock->block_id, $settings['blockOccurrencesInPages']
                );
            }

            if ($saved)
            {
                $dbManager->commit();
            }
            else
            {
                $dbManager->rollback();
            }
        }

        return $saved;
    }

    public function saveDynamicBlockOccurrencesInPages($blockId, $occurrencesInPages)
    {
        $blockId = (int)$blockId;

        if ($blockId <= 0)
        {
            return false;
        }

        $block = $this->getBlockById($blockId);
        if (!$block || !$block->is_dynamic)
        {
            return false;
        }

        // TODO: Find a way to rollback already commited transactions - it is needed for the following foreach-loop.

        foreach ($occurrencesInPages as $pageId => $info)
        {
            $parts = explode('|', $info);
            $slotId = $parts[0];
            $shouldBeAdded = ($parts[1] == '1') ? true : false;

            if ($shouldBeAdded)
            {
                $this->addDynamicBlockToPageSlot($pageId, $blockId, $slotId);
            }
            else
            {
                $this->removeDynamicBlockFromPageSlot($pageId, $blockId, $slotId);
            }
        }

        return true;
    }

    public function addDynamicBlockToPageSlot($pageId, $blockId, $slotId)
    {
        $blockId = (int)$blockId;

        if (!$this->isBlockDynamic($blockId))
        {
            return false;
        }

        $page = $this->getPageManager()->getPageById($pageId);
        if (!$page)
        {
            // Page does not exists. Return "true", because it could be deleted a few seconds ago.

            return true;
        }

        $blockAlreadyExists = $this->getPageManager()->pageContainsDynamicBlockInSlot($pageId, $blockId, $slotId);
        if ($blockAlreadyExists)
        {
            $publishBlocksAfterInsertion = $this->cmsConfig->publish_blocks_after_insertion ? true : false;
            if ($publishBlocksAfterInsertion)
            {
                // Make sure all blocks are published.

                return $this->setDynamicBlockInPageSlotPublished($pageId, $blockId, $slotId, $published = true);
            }

            return true;
        }

        return $this->insertBlockIntoPage($pageId, $blockId, $slotId, 0);
    }

    public function removeDynamicBlockFromPageSlot($pageId, $blockId, $slotId)
    {
        $blockId = (int)$blockId;

        if (!$this->isBlockDynamic($blockId))
        {
            return true; // TODO: Decide if it should be "true" or "false" in this case.
        }

        $page = $this->getPageManager()->getPageById($pageId);
        if (!$page)
        {
            // Page does not exists. Return "true", because it could be deleted a few seconds ago.

            return true;
        }

        $pageBlocksInPageSlot = $this->getPageBlocksByPageIdBlockIdAndSlotId($pageId, $blockId, $slotId);
        if ($pageBlocksInPageSlot)
        {
            foreach ($pageBlocksInPageSlot as $pageBlock)
            {
                $deleted = $this->deletePageBlock($pageBlock->page_block_id);
                if (!$deleted)
                {
                    return false;
                }
            }
        }

        return true;
    }

    public function deletePageBlock($pageBlockId)
    {
        $pageBlockId = (int)$pageBlockId;
        if ($pageBlockId <= 0)
        {
            return false;
        }

        $deleted = false;

        $dbManager = $this->getDatabaseManager();
        $dbManager->beginTransaction();

        $pageBlock = $this->getPageBlockById($pageBlockId);
        if ($pageBlock)
        {
            try
            {
                $this->callPageBlockCustomMethod($pageBlockId, 'onBeforeDeleted');
            }
            catch (\Site\Exception\BlockHasNoMethodException $e){}

            $block = $this->getBlockById($pageBlock->block_id);
            if ($block)
            {
                $pageBlockContentId = $this->getPageBlockContentIdByPageBlockId($pageBlockId);
                if ($pageBlockContentId !== false)
                {
                    $delete = $this->sql->delete();
                    $delete
                        ->from('pages_blocks_contents_association')
                        ->where(
                            array(
                                'page_block_id' => $pageBlockId,
                                'content_id' => (int)$pageBlockContentId
                            )
                        );
                    $result = $dbManager->executePreparedStatement($delete);
                    if ($result)
                    {
                        $delete = $this->sql->delete();
                        $delete
                            ->from('pages_blocks')
                            ->where(array('page_block_id' => $pageBlockId));
                        $result = $dbManager->executePreparedStatement($delete);
                        if ($result)
                        {
                            if ($block->is_dynamic)
                            {
                                $deleted = true;
                            }
                            else // This is static block, so we should also delete it's contents.
                            {
                                $delete = $this->sql->delete();
                                $delete
                                    ->from('pages_blocks_contents')
                                    ->where(array('content_id' => (int)$pageBlockContentId));
                                $result = $dbManager->executePreparedStatement($delete);
                                if ($result)
                                {
                                    $deleted = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        if ($deleted)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $deleted;
    }

    public function callPageBlockCustomMethod($pageBlockId, $methodName, $optionalParameters = array(), $specificLanguageId = null)
    {
        $pageBlockId = (int)$pageBlockId;
        $optionalParameters = $this->toArray($optionalParameters);
        $languageId = ($specificLanguageId !== null) ? (int)$specificLanguageId : (int)$this->language->language_id;

        if (($pageBlockId <= 0) || ($languageId <= 0) || ($methodName == ''))
        {
            return false;
        }

        $pageBlock = $this->getPageBlockById($pageBlockId);
        if ($pageBlock)
        {
            $block = $this->getBlockById($pageBlock->block_id);
            if ($block)
            {
                $phpClassName = $block->php_class_name;
                if ($phpClassName != '')
                {
                    $blockInstance = $this->createBlockUsingPhpClassNameBlockInfoAndPageBlockInfo($phpClassName, $block, $pageBlock, $languageId);
                    if ($blockInstance)
                    {
                        if (!method_exists($blockInstance, $methodName))
                        {
                            throw new \Site\Exception\BlockHasNoMethodException('Block "' . $phpClassName . '" has no method "' . $methodName . '()" !');
                        }

                        return call_user_func_array(array($blockInstance, $methodName), $optionalParameters);
                    }
                }
            }
        }

        return true;
    }

    public function isBlockDynamic($blockId)
    {
        $blockId = (int)$blockId;

        if ($blockId <= 0)
        {
            return false;
        }

        $blockId = $this->getBlockById($blockId);
        if (!$blockId || !$blockId->is_dynamic)
        {
            return false;
        }

        return true;
    }

    protected function convertConfigToSimpleXmlExtended($config)
    {
        $config = (string)$config;

        if ($config == '')
        {
            return false;
        }

        return simplexml_load_string($config, '\Site\Custom\SimpleXmlElementExtended', LIBXML_NOCDATA);
    }

    protected function convertConfigToFlexibleContainer($config)
    {
        $config = (string)$config;

        if ($config == '')
        {
            return false;
        }

        $container = new FlexibleContainer();

        $xmlConfig = $this->convertConfigToSimpleXmlExtended($config);
        if ($xmlConfig)
        {
            foreach ($xmlConfig as $name => $value)
            {
                $container[$name] = (string)$value;
            }
        }

        return $container;
    }

    protected function savePageBlockSettingsToSpecificTable($pageBlockId, $settings, $tableName, $contentId = null)
    {
        $pageBlockId = (int)$pageBlockId;
        //$settings = (array)$settings; // TODO: Conversion using toIterator() function.
        $tableName = (string)$tableName;
        $contentId = (int)$contentId ? : null;

        if (
            ($pageBlockId <= 0) || !$settings ||
            !in_array($tableName, array('pages_blocks', 'pages_blocks_contents')) ||
            (($contentId !== null) && ($contentId <= 0))
        )
        {
            return false;
        }

        $pageBlock = $this->getPageBlockById($pageBlockId);
        if (!$pageBlock)
        {
            return false;
        }

        $saved = false;

        $xmlConfig = new \Site\Custom\SimpleXmlElementExtended('<xml></xml>');
        foreach ($settings as $name => $value)
        {
            if ($name != 'blockOccurrencesInPages') // Occurrences should not be saved to XML.
            {
                $xmlConfig->$name = null;
                $xmlConfig->$name->addCData($value);
            }
        }

        $config = trim($xmlConfig->asXML());
        if (mb_strlen($config) <= 65535) // Maximum length for MySQL TEXT field type is 65535.
        {
            $dbManager = $this->getDatabaseManager();

            $update = $this->sql->update($tableName);
            $update->set(array('config' => $config));

            if ($tableName == 'pages_blocks')
            {
                $update->where(array('page_block_id' => $pageBlockId));
            }
            else
            {
                $update->where(array('content_id' => $contentId));
            }

            $result = $dbManager->executePreparedStatement($update);
            if ($result)
            {
                if ($tableName == 'pages_blocks')
                {
                    $row = $this->getPageBlockById($pageBlockId);
                }
                else
                {
                    $row = $this->getPageBlockContentsByPageBlockId($pageBlockId);
                }

                if ($row && ($row->config == $config))
                {
                    $saved = true;
                }
            }
        }

        return $saved;
    }

    /**
     * Block creation factories.
     */
    protected function createBlock($phpClassName, $blockData, $user)
    {
        $phpClassName = trim((string)$phpClassName);
        $this->throwExceptionOnEmptyBlockClassName($phpClassName);

        $filePath = 'module/Site/src/Site/Block/' . $phpClassName . '/' . $phpClassName . '.php';
        $this->throwExceptionOnBlockClassFileDoesNotExists($filePath);
        $this->throwExceptionOnBlockClassFileIsADirectory($filePath);

        require_once($filePath);
        $blockFQN = $this->getFullBlockNamespaceByPhpClassName($phpClassName);
        $this->throwExceptionOnBlockClassDoesNotExistsInBlockFileException($phpClassName, $blockFQN, $filePath);

        $attachmentManager = $this->getAttachmentManager();
        $languageManager = $this->getLanguageManager();
        $pageManager = $this->getPageManager();
        $uploadManager = $this->getUploadManager();

        $blockInstance = new $blockFQN(
            $this->serviceLocator,
            $languageManager,
            $pageManager,
            $this,
            $attachmentManager,
            $uploadManager,
            $blockData,
            $user,
            $this->language,
            $this->userLanguage
        );

        return $blockInstance;
    }

    /**
     * Exception throw functions.
     */
    protected function throwExceptionOnEmptyBlockClassName($phpClassName)
    {
        if ($phpClassName == '')
        {
            throw new \Exception('Empty block class name provided.');
        }
    }

    protected function throwExceptionOnBlockClassFileDoesNotExists($filePath)
    {
        if (!file_exists($filePath))
        {
            throw new \Exception(getcwd() . 'Block file "' . $filePath . '" does not exists.');
        }
    }

    protected function throwExceptionOnBlockClassFileIsADirectory($filePath)
    {
        if (file_exists($filePath) && !is_file($filePath))
        {
            throw new \Exception('"' . $filePath . '" is a directory, not a file.');
        }
    }

    protected function throwExceptionOnBlockClassDoesNotExistsInBlockFileException($phpClassName, $blockFQN, $filePath)
    {
        if (!class_exists($blockFQN, false))
        {
            throw new \Exception('Class "' . $phpClassName . '" does not exist in file "' . $filePath . '".');
        }
    }

    private function toArray($elements)
    {
        if (is_array($elements))
        {
            return $elements;
        }
        else if ($elements instanceof FlexibleContainer)
        {
            return $elements->toArray();
        }
        else if (is_object($elements))
        {
            return array($elements);
        }

        return (array)$elements;
    }

    private function toIterator($elements)
    {
        if (!$elements instanceof \Traversable)
        {
            $elements = new \ArrayObject(is_array($elements) ? $elements : array($elements));
        }

        return $elements;
    }
}
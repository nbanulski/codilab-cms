<?php
namespace Site\Controller;

class AdminController extends BaseController
{
    public function addEditPageAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $languageManager = $this->getLanguageManager();
                $pageManager = $this->getPageManager();

                $defaultLanguageIsoCode = trim((string)$this->cmsConfig['default_language_iso_code']);
                $defaultLanguage = $languageManager->getLanguageByIsoCode($defaultLanguageIsoCode);
                $formData = array();
                $pageId = null;

                if ($request->isPost())
                {
                    if ($request->getPost('dataPosted'))
                    {
                        $customStatus = null;

                        $formData = $request->getPost('formData');
                        if ($formData)
                        {
                            $currentPageId = isset($formData['pageId']) ? $formData['pageId'] : null;
                            $pageTitle = isset($formData['pageTitle']) ? $formData['pageTitle'] : null;
                            $pageLanguage = isset($formData['pageLanguage']) ? $formData['pageLanguage'] : null;
                            $languageEquivalents = isset($formData['languageEquivalents']) ? $formData['languageEquivalents'] : null;
                            $metaKeywords = isset($formData['metaKeywords']) ? $formData['metaKeywords'] : null;
                            $metaDescription = isset($formData['metaDescription']) ? $formData['metaDescription'] : null;
                            $cleanUrl = isset($formData['cleanUrl']) ? $formData['cleanUrl'] : null;
                            $frequencyOfChanges = isset($formData['frequencyOfChanges']) ? $formData['frequencyOfChanges'] : null;
                            $urlPriority = isset($formData['urlPriority']) ? $formData['urlPriority'] : null;
                            $allowIndexingPage = isset($formData['allowIndexingPage']) ? $formData['allowIndexingPage'] : null;
                            $allowBackingUpPage = isset($formData['allowBackingUpPage']) ? $formData['allowBackingUpPage'] : null;
                            $disallowCachingPage = isset($formData['disallowCachingPage']) ? $formData['disallowCachingPage'] : null;
                            $layoutId = isset($formData['layout']) ? $formData['layout'] : null;
                            $pageParentPageId = isset($formData['pageParentPageId']) ? (int)$formData['pageParentPageId'] : null;
                            $subLayoutId = isset($formData['subLayout']) ? $formData['subLayout'] : null;
                            $isPublished = isset($formData['publishNow']) ? $formData['publishNow'] : null;

                            $result = false;
                            try
                            {
                                $result = $this->createNewOrUpdateExistingPage(array(
                                    'pageId' => $currentPageId,
                                    'pageTitle' => $pageTitle,
                                    'pageLanguage' => $pageLanguage,
                                    'languageEquivalents' => $languageEquivalents,
                                    'metaKeywords' => $metaKeywords,
                                    'metaDescription' => $metaDescription,
                                    'cleanUrl' => $cleanUrl,
                                    'frequencyOfChanges' => $frequencyOfChanges,
                                    'urlPriority' => $urlPriority,
                                    'allowIndexingPage' => $allowIndexingPage,
                                    'allowBackingUpPage' => $allowBackingUpPage,
                                    'disallowCachingPage' => $disallowCachingPage,
                                    'layoutId' => $layoutId,
                                    'pageParentPageId' => $pageParentPageId,
                                    'subLayoutId' => $subLayoutId,
                                    'isPublished' => $isPublished ? 1 : 0,
                                    'authorId' => $user->user_id,
                                ));
                            }
                            catch (\Site\Exception\PageCleanUrlAlreadyInUseException $e)
                            {
                                $jsonResponse->meta->requestErrorMessage = $e->getMessage();
                            }

                            if ($result)
                            {
                                $customStatus = ($currentPageId > 0) ? 'PAGE_MODIFIED' : 'PAGE_ADDED';
                            }
                            else
                            {
                                $customStatus = ($currentPageId > 0) ? 'PAGE_MODIFYING_ERROR' : 'PAGE_ADDING_ERROR';
                            }
                        }

                        $jsonResponse->meta->customStatus = $customStatus;
                        $this->response->setContent((string)$jsonResponse);

                        return $this->response;
                    }
                    else
                    {
                        $pageId = $request->getPost('pageId');
                        if ($pageId > 0)
                        {
                            $page = $pageManager->getPageById($pageId);
                            if ($page)
                            {
                                $languageEquivalents = $pageManager->getPageLanguageEquivalentsByPageId($pageId);
                                $languageEquivalentsArray = array();
                                if ($languageEquivalents)
                                {
                                    foreach ($languageEquivalents as $equivalent)
                                    {
                                        $languageEquivalentsArray[$equivalent->language_id] = $equivalent->associated_page_id;
                                    }
                                }

                                $formData = array(
                                    'pageId' => $page->page_id,
                                    'pageTitle' => $page->title,
                                    'pageLanguage' => $page->language_id,
                                    'languageEquivalents' => $languageEquivalentsArray,
                                    'metaKeywords' => $page->meta_keywords,
                                    'metaDescription' => $page->meta_description,
                                    'cleanUrl' => $page->clean_url,
                                    'frequencyOfChanges' => $page->frequency_of_changes,
                                    'urlPriority' => $page->url_priority,
                                    'allowIndexingPage' => $page->allow_indexing,
                                    'allowBackingUpPage' => $page->allow_backing_up,
                                    'disallowCachingPage' => $page->disallow_caching,
                                    'layout' => $page->layout_id,
                                    'pageParentPageId' => $page->parent_id,
                                    'subLayout' => $page->sub_layout_id,
                                    'publishNow' => $page->is_published,
                                );
                            }
                        }
                    }
                }

                $defaultFormData = array(
                    'pageId' => '',
                    'pageTitle' => '',
                    'pageLanguage' => $defaultLanguage->language_id,
                    'languageEquivalents' => null,
                    'metaKeywords' => '',
                    'metaDescription' => '',
                    'cleanUrl' => '',
                    'frequencyOfChanges' => 'always',
                    'urlPriority' => '0.5',
                    'allowIndexingPage' => '1',
                    'allowBackingUpPage' => '1',
                    'disallowCachingPage' => '0',
                    'layout' => null,
                    'pageParentPageId' => null,
                    'subLayout' => null,
                    'publishNow' => '0',
                );
                $formData = new \Site\Custom\FlexibleContainer(array_merge($defaultFormData, $formData));

                if ($pageId > 0)
                {
                    $languageId = (int)$formData['pageLanguage'];
                }
                else
                {
                    $languageId = $this->language->language_id;
                }

                $metadata = new \Zend\Db\Metadata\Metadata($this->dbAdapter);
                $tableName = 'pages';
                $pagesTableColumnsMetadata = $metadata->getColumns($tableName);
                $maxLengths = array();
                if ($pagesTableColumnsMetadata)
                {
                    foreach ($pagesTableColumnsMetadata as $column)
                    {
                        $name = $column->getName();

                        $maxLengths[$name] = $column->getCharacterMaximumLength();
                    }
                }

                $availableParentPages = $pageManager->getAllAvailableParentPagesByPageIdAndLanguageId(
                    $pageId, $languageId
                );
                $languages = $languageManager->getAllLanguages()->buffer();
                $layouts = $pageManager->getLayouts();
                $subLayouts = $pageManager->getSubLayouts();
                if ($pageId > 0)
                {
                    $availableLanguageEquivalents =
                        $pageManager->getAllAvailablePageLanguageEquivalentsExcludingPageIdAndLanguageId(
                            $pageId, $languageId
                        );
                }
                else
                {
                    /*
                     $availableLanguageEquivalents =
                     $pageManager->getAllAvailablePageLanguageEquivalentsExcludingLanguageId($languageId);
                    */
                    $availableLanguageEquivalents = $pageManager->getAllAvailablePageLanguageEquivalents();
                }

                $defaultIsoCode = '';
                $pageIsoCode = '';
                $languagesOptionsHtml = '';
                if ($languages)
                {
                    foreach ($languages as $language)
                    {
                        if ($defaultIsoCode == '')
                        {
                            $defaultIsoCode = $language->iso_code;
                        }

                        $name =
                            ($language->language_id == $this->language->language_id) ?
                            $language->native_name :
                            $language->english_name;
                        $name = $this->escapeForHtml($name);

                        $languageId = (int)$formData['pageLanguage'];
                        $theSameLanguage = ($language->language_id == $languageId);
                        $selected = $theSameLanguage ? ' selected' : '';

                        if ($theSameLanguage)
                        {
                            $pageIsoCode = $language->iso_code;
                        }

                        $languagesOptionsHtml .= '<option value="' . $language->language_id . '" data-iso-code="' . $language->iso_code . '"' . $selected .'>' . $name . '</option>';
                    }
                }
                if ($pageIsoCode == '')
                {
                    $pageIsoCode = $defaultIsoCode;
                }

                $parentPagesOptions = array('' => '-- Brak --');
                if ($availableParentPages)
                {
                    foreach ($availableParentPages as $parent)
                    {
                        $parentPagesOptions[$parent->page_id] = $parent->title;
                    }
                }

                $languageEquivalentsOptions = array();
                if ($availableLanguageEquivalents)
                {
                    foreach ($availableLanguageEquivalents as $page)
                    {
                        $languageEquivalentsOptions[$page->page_id] = $page->title;
                    }
                }

                $layoutOptions = array();
                if ($layouts)
                {
                    foreach ($layouts as $layout)
                    {
                        $layoutOptions[$layout->layout_id] = $layout->name;
                    }
                }

                $subLayoutOptions = array();
                if ($subLayouts)
                {
                    foreach ($subLayouts as $subLayout)
                    {
                        $subLayoutOptions[$subLayout->sub_layout_id] = $subLayout->name;
                    }
                }

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $addNewPageView = new \Zend\View\Model\ViewModel();
                $addNewPageView->setTemplate('site/admin/add-edit-page.phtml');
                $addNewPageView->setVariables(
                    array(
                        'currentLanguage' => $this->language,
                        'userLanguage' => $this->getUserLanguage(),
                        'maxLengths' => $maxLengths,
                        'languageCount' => $languages->count(),
                        'languages' => $languages,
                        'languagesOptionsHtml' => $languagesOptionsHtml,
                        'languageEquivalentsOptions' => $languageEquivalentsOptions,
                        'layoutOptions' => $layoutOptions,
                        'parentPagesOptions' => $parentPagesOptions,
                        'subLayoutOptions' => $subLayoutOptions,
                        'formData' => $formData,
                        'pageIsoCode' => $pageIsoCode
                    )
                );

                $jsonResponse->data = $phpRenderer->render($addNewPageView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function blocksAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageNumber = (int)$request->getPost('pageNumber') ? : 1;
                $itemCountPerPage = (int)$request->getPost('itemCountPerPage') ? : 10;
                $includePagesFrom = (int)$request->getPost('includePagesFrom') ? : null;
                $pageQuery = trim(strip_tags($request->getPost('pageQuery', '')));
                $blockQuery = trim(strip_tags($request->getPost('blockQuery', '')));

                $userLanguage = $this->getUserLanguage();
                $pageManager = $this->getPageManager();
                $dbManager = $this->getDatabaseManager();

                // Find all system languages:

                $languages = array(
                    '' => '--- ' . $this->translate('All languages', 'default', $userLanguage->zend2_locale) . ' ---'
                );
                $allLanguages = $this->getLanguageManager()->getAllLanguages();
                if ($allLanguages)
                {
                    foreach ($allLanguages as $language)
                    {
                        $languageId = (int)$language->language_id;
                        $languages[$languageId] = $language->english_name;
                    }
                }

                $pageListGroupedByBlock = array();

                if (($pageQuery != '') || (mb_strlen($blockQuery) >= 3))
                {
                    // Find all block ids by block name:

                    $select = $this->sql->select();
                    $select
                        ->from(array('pb' => 'pages_blocks'))
                        ->columns(
                            array(
                                'block_id'
                            )
                        )
                        ->join(
                            array('p' => 'pages'),
                            'p.page_id = pb.page_id',
                            array(),
                            $select::JOIN_LEFT
                        )
                        ->join(
                            array('bp' => 'blocks_properties'),
                            new \Zend\Db\Sql\Expression(
                                'bp.block_id = pb.block_id AND bp.language_id = ?',
                                array($userLanguage->language_id)
                            ),
                            array('block_title' => 'name'),
                            $select::JOIN_LEFT
                        )
                        ->join(
                            array('b' => 'blocks'),
                            'b.block_id = bp.block_id',
                            array('is_dynamic' => 'is_dynamic'),
                            $select::JOIN_LEFT
                        );

                    if ($pageQuery != '')
                    {
                        $select
                            ->where
                            ->nest()
                            ->literal('MATCH(p.title) AGAINST(? IN BOOLEAN MODE)', '*' . $pageQuery . '*')
                            ->or
                            ->literal('MATCH(p.meta_description) AGAINST(? IN BOOLEAN MODE)', '*' . $pageQuery . '*')
                            ->unnest();
                    }

                    if ($blockQuery != '')
                    {
                        $select->where->literal('MATCH(bp.name) AGAINST(? IN BOOLEAN MODE)', '*' . $blockQuery . '*');
                    }

                    if ($includePagesFrom > 0)
                    {
                        $select->where(array('p.language_id' => $includePagesFrom));
                    }

                    $select->group('pb.block_id');
                    //echo $this->sql->getSqlStringForSqlObject($select) . "\n\n";
                    $blocksResultSet = $dbManager->getHydratedResultSet($select);

                    // Find all blocks occurrences by block ids:

                    if ($blocksResultSet)
                    {
                        foreach ($blocksResultSet as $blockInfo)
                        {
                            $select = $this->sql->select();
                            $select
                                ->from(array('pb' => 'pages_blocks'))
                                ->columns(
                                    array(
                                        'page_id', 'block_id'
                                    )
                                )
                                ->join(
                                    array('p' => 'pages'),
                                    'p.page_id = pb.page_id',
                                    array('page_title' => 'title'),
                                    $select::JOIN_LEFT
                                )
                                ->join(
                                    array('l' => 'languages'),
                                    'l.language_id = p.language_id',
                                    array('language_iso_code' => 'iso_code'),
                                    $select::JOIN_LEFT
                                )
                                ->where(
                                    array(
                                        'pb.block_id' => $blockInfo->block_id
                                    )
                                )
                                ->order('p.last_update_date DESC');
                            //echo $this->sql->getSqlStringForSqlObject($select) . "\n\n";
                            $pagesResultSet = $dbManager->getHydratedResultSet($select);
                            if ($pagesResultSet)
                            {
                                foreach ($pagesResultSet as $row)
                                {
                                    $blockId = (int)$row->block_id;

                                    if (!array_key_exists($blockId, $pageListGroupedByBlock))
                                    {
                                        $pageListGroupedByBlock[$blockId] = array(
                                            'blockInfo', 'pageList'
                                        );
                                    }

                                    $pageListGroupedByBlock[$blockId]['blockInfo'] = $blockInfo;

                                    unset($row->block_id);
                                    $row->fullUrl = $pageManager->getFullPageCleanUrlByPageId($row->page_id);
                                    $pageListGroupedByBlock[$blockId]['pageList'][] = $row;
                                }
                            }
                        }
                    }

                    if ($pageListGroupedByBlock)
                    {
                        $pageListGroupedByBlock = new \Site\Custom\FlexibleContainer($pageListGroupedByBlock);
                    }
                }

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $pagesView = new \Zend\View\Model\ViewModel();
                $pagesView->setTemplate('site/admin/search-for-blocks.phtml');
                $pagesView->setVariables(
                    array(
                        'userLanguage' => $this->getUserLanguage(),
                        'languages' => $languages,
                        'pageNumber' => $pageNumber,
                        'itemCountPerPage' => $itemCountPerPage,
                        'includePagesFrom' => $includePagesFrom,
                        'pageListGroupedByBlock' => $pageListGroupedByBlock,
                        'pageQuery' => $pageQuery,
                        'blockQuery' => $blockQuery
                    )
                );

                $jsonResponse->data = $phpRenderer->render($pagesView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function eventListAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageNumber = (int)$request->getPost('pageNumber') ? : 1;
                $itemCountPerPage = (int)$request->getPost('itemCountPerPage') ? : 10;
                //$includeEventsFrom = $request->getPost('includeEventsFrom') ? : 'current-language';
                $searchText = trim(strip_tags($request->getPost('searchText', '')));

                $subQuery = $this->sql->select();
                $subQuery
                    ->from('block_event_list_user_subscriptions')
                    ->columns(array('userCount' => new \Zend\Db\Sql\Expression('COUNT(block_event_list_user_subscriptions.subscription_id)')))
                    ->where('block_event_list_user_subscriptions.event_id = block_event_list.event_id');

                $select = $this->sql->select();
                $select
                    ->from('block_event_list')
                    ->columns(
                        array(
                            'event_id',
                            'content_id',
                            'category_id',
                            'event_date_start',
                            'event_date_end',
                            'clean_url',
                            'title',
                            'user_count' => new \Zend\Db\Sql\Expression('?', array($subQuery))
                        )
                    )
                    ->join(
                        array('belc' => 'block_event_list_categories'),
                        'belc.category_id = block_event_list.category_id',
                        array('category_title' => 'name'),
                        $select::JOIN_LEFT
                    )
                    ->where(
                        array('is_signing_up_enabled' => 1)
                    );

                if ($searchText != '')
                {
                    $select->where->literal('title REGEXP ?', array($searchText));
                }

                /*if ($includePagesFrom == 'current-language')
                {
                    $select->where(array('bel.language_id' => $this->language->language_id));
                }*/

                $select->order('event_date_start DESC');

                //echo $this->sql->getSqlStringForSqlObject($select);

                $translateHelper = $this->serviceLocator->get('viewhelpermanager')->get('translate');
                $resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet(
                    new \Site\Hydrator\EventHydrator(
                        $this->getBlockManager(), $translateHelper, $this->getUserLanguage()
                    ),
                    new \Site\Custom\FlexibleContainer()
                );
                $paginatorAdapter = new \Zend\Paginator\Adapter\DbSelect($select, $this->dbAdapter, $resultSetPrototype);
                $paginator = new \Zend\Paginator\Paginator($paginatorAdapter);
                $paginator->setCurrentPageNumber($pageNumber);
                $paginator->setItemCountPerPage($itemCountPerPage);

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $pagesView = new \Zend\View\Model\ViewModel();
                $pagesView->setTemplate('site/admin/event-list.phtml');
                $pagesView->setVariables(
                    array(
                        'userLanguage' => $this->getUserLanguage(),
                        'pageNumber' => $pageNumber,
                        'itemCountPerPage' => $itemCountPerPage,
                        //'includeEventsFrom' => $includeEventsFrom,
                        'eventsPaginator' => $paginator,
                        'searchText' => $searchText
                    )
                );

                $jsonResponse->data = $phpRenderer->render($pagesView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function usersSignedUpForEventAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageNumber = (int)$request->getPost('pageNumber') ? : 1;
                $itemCountPerPage = (int)$request->getPost('itemCountPerPage') ? : 10;
                $searchText = trim(strip_tags($request->getPost('searchText', '')));
                $additionalData = $request->getPost('additionalData');
                $additionalData = new \Site\Custom\FlexibleContainer($additionalData);
                $eventId = (int)$additionalData->eventId;
                $contentId = (int)$additionalData->contentId;
                $eventTitle = '';

                $dbManager = $this->getDatabaseManager();
                $select = $this->sql->select();
                $select
                    ->from('block_event_list')
                    ->columns(array('title'))
                    ->where(
                        array('event_id' => $eventId, 'content_id' => $contentId)
                    );
                $resultSet = $dbManager->getHydratedResultSet($select);
                if ($resultSet)
                {
                    $row = $resultSet->current();
                    $eventTitle = $row ? $row->title : '';
                }

                $select = $this->sql->select();
                $select
                    ->from('block_event_list_user_subscriptions')
                    ->columns(array($select::SQL_STAR))
                    ->where(
                        array('event_id' => $eventId, 'content_id' => $contentId)
                    );

                if ($searchText != '')
                {
                    $select->where->literal('name_and_surname REGEXP ?', array($searchText));
                }

                $select->order('name_and_surname ASC');

                //$translateHelper = $this->serviceLocator->get('viewhelpermanager')->get('translate');
                $resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet(
                    new \Zend\Stdlib\Hydrator\ObjectProperty(),
                    new \Site\Custom\FlexibleContainer()
                );
                $paginatorAdapter = new \Zend\Paginator\Adapter\DbSelect($select, $this->dbAdapter, $resultSetPrototype);
                $paginator = new \Zend\Paginator\Paginator($paginatorAdapter);
                $paginator->setCurrentPageNumber($pageNumber);
                $paginator->setItemCountPerPage($itemCountPerPage);

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $pagesView = new \Zend\View\Model\ViewModel();
                $pagesView->setTemplate('site/admin/users-signed-up-for-event.phtml');
                $pagesView->setVariables(
                    array(
                        'userLanguage' => $this->getUserLanguage(),
                        'pageNumber' => $pageNumber,
                        'itemCountPerPage' => $itemCountPerPage,
                        'usersPaginator' => $paginator,
                        'searchText' => $searchText,
                        'eventTitle' => $eventTitle,
                        'eventId' => $eventId,
                        'contentId' => $contentId
                    )
                );

                $jsonResponse->data = $phpRenderer->render($pagesView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getPageDeletionConfirmationViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageManager = $this->getPageManager();
                $userLanguage = $this->getUserLanguage();

                $pageId = $request->getPost('pageId');

                $page = $pageManager->getPageById($pageId);
                $navigationMenuBlockList = $pageManager->getListOfNavigationMenuBlocksWherePageExistsByPageId($pageId);

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');

                $pageDeletionConfirmationView = new \Zend\View\Model\ViewModel();
                $pageDeletionConfirmationView->setTemplate('site/admin/page-deletion-confirmation.phtml');
                $pageDeletionConfirmationView->setVariables(
                    array(
                        'userLanguage' => $userLanguage,
                        'page' => $page,
                        'navigationMenuBlockList' => $navigationMenuBlockList
                    )
                );

                $jsonResponse->data = $phpRenderer->render($pageDeletionConfirmationView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getUserSignedUpForEventDeletionConfirmationViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageManager = $this->getPageManager();
                $userLanguage = $this->getUserLanguage();

                $subscriptionId = $request->getPost('subscriptionId');
                $eventId = $request->getPost('eventId');
                $contentId = $request->getPost('contentId');

                $eventListBlock = $this->getBlockManager()->createBlockUsingContentId($contentId);
                if ($eventListBlock instanceof \Site\Block\EventList\EventList)
                {
                    $userSubscription = $eventListBlock->getEventUserSubscriptionDataBySubscriptionIdAndEventId(
                        $subscriptionId, $eventId
                    );
                }

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');

                $pageDeletionConfirmationView = new \Zend\View\Model\ViewModel();
                $pageDeletionConfirmationView->setTemplate(
                    'site/admin/user-signed-up-for-event-deletion-confirmation.phtml'
                );
                $pageDeletionConfirmationView->setVariables(
                    array(
                        'userLanguage' => $userLanguage,
                        'userSubscription' => $userSubscription
                    )
                );

                $jsonResponse->data = $phpRenderer->render($pageDeletionConfirmationView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function deletePageAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageId = $request->getPost('pageId');
                $actionToTakOnMenus = $request->getPost('actionToTakOnMenus');

                $deleted = $this->getPageManager()->deletePageById($pageId, $actionToTakOnMenus);

                if ($deleted)
                {
                    $jsonResponse->meta->customStatus = 'PAGE_DELETED';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function deleteUserSignedUpForEventAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $subscriptionId = $request->getPost('subscriptionId');
                $eventId = $request->getPost('eventId');
                $contentId = $request->getPost('contentId');

                $deleted = false;

                $eventListBlock = $this->getBlockManager()->createBlockUsingContentId($contentId);
                if ($eventListBlock instanceof \Site\Block\EventList\EventList)
                {
                    $deleted = $eventListBlock->deleteUserSignedUpForEvent($subscriptionId, $eventId);
                }

                if ($deleted)
                {
                    $jsonResponse->meta->customStatus = 'USER_DELETED';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function pagesAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageNumber = (int)$request->getPost('pageNumber') ? : 1;
                $itemCountPerPage = (int)$request->getPost('itemCountPerPage') ? : 10;
                $includePagesFrom = $request->getPost('includePagesFrom') ? : 'current-language';
                $searchText = trim(strip_tags($request->getPost('searchText', '')));

                $select = $this->sql->select();
                $select->from(array('p' => 'pages'))
                    ->columns(
                        array(
                            'page_id',
                            'is_published',
                            'views',
                            'last_update_author_id',
                            'last_update_date',
                            'clean_url',
                            'title'
                        )
                    )
                    ->join(
                        array('u' => 'users'), 'u.user_id = p.last_update_author_id', array('last_update_author' => 'login'), $select::JOIN_LEFT
                    )
                    ->join(
                        array('l' => 'languages'), 'l.language_id = p.language_id', array('language_iso_code' => 'iso_code'), $select::JOIN_LEFT
                    );

                if ($searchText != '')
                {
                    $select->where->literal('p.title REGEXP ?', array($searchText));
                }

                if ($includePagesFrom == 'current-language')
                {
                    $select->where(array('p.language_id' => $this->language->language_id));
                }

                $select->order('p.last_update_date DESC');

                $resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet(
                    new \Site\Hydrator\PageHydrator($this->getPageManager()),
                    new \Site\Custom\FlexibleContainer()
                );
                $paginatorAdapter = new \Zend\Paginator\Adapter\DbSelect($select, $this->dbAdapter, $resultSetPrototype);
                $paginator = new \Zend\Paginator\Paginator($paginatorAdapter);
                $paginator->setCurrentPageNumber($pageNumber);
                $paginator->setItemCountPerPage($itemCountPerPage);

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $pagesView = new \Zend\View\Model\ViewModel();
                $pagesView->setTemplate('site/admin/pages.phtml');
                $pagesView->setVariables(
                    array(
                        'userLanguage' => $this->getUserLanguage(),
                        'pageNumber' => $pageNumber,
                        'itemCountPerPage' => $itemCountPerPage,
                        'includePagesFrom' => $includePagesFrom,
                        'pagesPaginator' => $paginator,
                        'searchText' => $searchText
                    )
                );

                $jsonResponse->data = $phpRenderer->render($pagesView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function toggleDesignModeAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $sessionData = $this->sessionData();
            if ($sessionData)
            {
                $sessionData->session->designMode = !$sessionData->session->designMode;

                $auth = $this->serviceLocator->get('AuthService');
                $storage = $auth->getStorage();
                $storage->write($sessionData->toStdClass());

                if ($sessionData->session->designMode)
                {
                    $jsonResponse->meta->customStatus = 'DESIGN_MODE_ENABLED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'DESIGN_MODE_DISABLED';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function togglePagePublishingAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageId = $request->getPost('pageId');
                $published = $request->getPost('published');

                $pageManager = $this->getPageManager();
                $pageManager->togglePagePublishing($pageId, $published);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    protected function createNewOrUpdateExistingPage($formData)
    {
        $pageId = isset($formData['pageId']) ? (int)$formData['pageId'] : null;

        $pageManager = $this->getPageManager();

        if ($pageId > 0)
        {
            return $pageManager->updateExistingPage($formData);
        }
        else
        {
            return $pageManager->createNewPage($formData);
        }
    }
}
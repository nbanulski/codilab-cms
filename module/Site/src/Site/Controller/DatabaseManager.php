<?php
namespace Site\Controller;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\AbstractSql;
use Zend\Db\Sql\Insert;

class DatabaseManager
{
    protected $sql = null;
    protected $connection = null;

    public function __construct(Sql $sql)
    {
        $this->sql = $sql;
    }

    public function getAdapter()
    {
        return $this->sql->getAdapter();
    }

    public function getSql()
    {
        return $this->sql;
    }

    public function getConnection()
    {
        if ($this->connection == null)
        {
            $this->connection = $this->sql->getAdapter()->getDriver()->getConnection();
        }

        return $this->connection;
    }

    public function beginTransaction($forceThrowingExceptionsAboutTransaction = false)
    {
        try
        {
            $this->getConnection()->beginTransaction();
        }
        catch (\PDOException $e)
        {
            if (($e->getMessage() != 'There is already an active transaction') || $forceThrowingExceptionsAboutTransaction)
            {
                throw $e;
            }
        }
    }

    public function commit($forceThrowingExceptionsAboutTransaction = false)
    {
        try
        {
            $this->getConnection()->commit();
        }
        catch (\PDOException $e)
        {
            if (($e->getMessage() != 'There is no active transaction') || $forceThrowingExceptionsAboutTransaction)
            {
                throw $e;
            }
        }
    }

    public function rollback($forceThrowingExceptionsAboutTransaction = false)
    {
        try
        {
            $this->getConnection()->rollback();
        }
        catch (\PDOException $e)
        {
            if (($e->getMessage() != 'There is no active transaction') || $forceThrowingExceptionsAboutTransaction)
            {
                throw $e;
            }
        }
    }

    public function executePreparedStatement(AbstractSql $sqlObject)
    {
        $statement = $this->sql->prepareStatementForSqlObject($sqlObject);
        if ($statement)
        {
            $result = $statement->execute();

            return $result;
        }

        return false;
    }

    public function getHydratedResultSet(AbstractSql $sqlObject)
    {
        $result = $this->executePreparedStatement($sqlObject);
        if ($result)
        {
            $hydratedResultSet = new \Zend\Db\ResultSet\HydratingResultSet(
                new \Zend\Stdlib\Hydrator\ObjectProperty(), new \stdClass()
            );
            $hydratedResultSet->initialize($result);

            return $hydratedResultSet;
        }

        return false;
    }

    public function getLastGeneratedValue()
    {
        return $this->sql->getAdapter()->getDriver()->getLastGeneratedValue();
    }

    public function getSpecificHydratedResultSet(
        AbstractSql $sqlObject, \Zend\Stdlib\Hydrator\HydratorInterface $hydrator, $objectPrototype
    )
    {
        $result = $this->executePreparedStatement($sqlObject);
        if ($result)
        {
            $hydratedResultSet = new \Zend\Db\ResultSet\HydratingResultSet($hydrator, $objectPrototype);
            $hydratedResultSet->initialize($result);

            return $hydratedResultSet;
        }

        return false;
    }

    public function executeInsertAndGetLastInsertId(Insert $insert)
    {
        $lastInsertId = null;

        $statement = $this->sql->prepareStatementForSqlObject($insert);
        if ($statement)
        {
            $result = $statement->execute();
            if ($result)
            {
                $lastInsertId = $this->getLastGeneratedValue();
            }
        }

        return $lastInsertId;
    }

    public function convertIdArrayToString($idArray)
    {
        $parsedIds = array();

        $idArray = (array)$idArray;
        foreach ($idArray as $number)
        {
            $number = (int)$number;
            if ($number > 0)
            {
                $parsedIds[] = $number;
            }
        }

        return join(',', $parsedIds);
    }
}
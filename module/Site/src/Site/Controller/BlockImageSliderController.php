<?php
namespace Site\Controller;

class BlockImageSliderController extends BaseController
{
    const BLOCK_IMAGE_SLIDER_TABLE_NAME = 'block_image_slider';

    public function addEditImageAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageBlockId = (int)$request->getPost('pageBlockId');
                $blockManager = $this->getBlockManager();
                $this->imageSliderBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $formData = array();

                if ($request->isPost())
                {
                    if (
                        $request->getPost('dataPosted') &&
                        ($this->imageSliderBlock instanceof \Site\Block\ImageSlider\ImageSlider)
                    )
                    {
                        $formData = new \Site\Custom\FlexibleContainer($request->getPost('formData'));
                        $result = $this->imageSliderBlock->addOrEditImage($formData);

                        if ($result)
                        {
                            $customStatus = ($formData->imageId > 0) ? 'IMAGE_MODIFIED' : 'IMAGE_ADDED';
                        }
                        else
                        {
                            $customStatus = ($formData->imageId > 0) ? 'IMAGE_MODIFYING_ERROR' : 'IMAGE_ADDING_ERROR';
                        }

                        $jsonResponse->meta->customStatus = $customStatus;
                        $this->response->setContent((string)$jsonResponse);

                        return $this->response;
                    }
                    else
                    {
                        $imageId = $request->getPost('imageId');
                        if ($imageId > 0)
                        {
                            if ($this->imageSliderBlock)
                            {
                                $image = $this->imageSliderBlock->getImageById($imageId);
                                $attachmentId = $image->attachment_id;
                                $attachment = $this->getUploadManager()->getAttachmentById($attachmentId);

                                $formData = array(
                                    'imageId' => $imageId,
                                    'imageContentId' => $image->content_id,
                                    'imageAttachmentId' => $image->attachment_id,
                                    'imagePageId' => $image->page_id,
                                    'imageOrder' => $image->order,
                                    'imageIsPublished' => $image->is_published,
                                    'imageUrlAddressType' => (int)$image->url_address_type,
                                    'imageLastUpdateAuthorId' => $user->user_id,
                                    'imageExternalUrl' => ($image->url_address_type == 1) ? $image->external_url : '',
                                    'imageTitle' => $image->title,
                                    'imageUniqueHashForFileName' => $attachment->hash_for_filename,
                                    'imageOriginalFileName' => $attachment->original_file_name
                                );
                            }
                        }
                    }
                }

                $defaultFormData = array(
                    'imageId' => null,
                    'imageContentId' => null,
                    'imageAttachmentId' => null,
                    'imagePageId' => null,
                    'imageOrder' => 0,
                    'imageIsPublished' => false,
                    'imageExternalUrl' => '',
                    'imageTitle' => '',
                    'imageUniqueHashForFileName' => '',
                    'imageOriginalFileName' => ''
                );
                $formData = new \Site\Custom\FlexibleContainer(array_merge($defaultFormData, $formData));

                $metadata = new \Zend\Db\Metadata\Metadata($this->dbAdapter);
                $blockImageSliderImagesTableColumnsMetadata = $metadata->getColumns(
                    self::BLOCK_IMAGE_SLIDER_TABLE_NAME
                );
                $maxLengths = array();
                if ($blockImageSliderImagesTableColumnsMetadata)
                {
                    foreach ($blockImageSliderImagesTableColumnsMetadata as $column)
                    {
                        $name = $column->getName();

                        $maxLengths[$name] = $column->getCharacterMaximumLength();
                    }
                }

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $addNewImageView = new \Zend\View\Model\ViewModel();
                $addNewImageView->setTemplate('blocks/image-slider/add-edit.phtml');
                $addNewImageView->setVariables(
                    array(
                        'maxLengths' => $maxLengths,
                        'formData' => $formData
                    )
                );

                $jsonResponse->data = $phpRenderer->render($addNewImageView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function deleteImageAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageBlockId = $request->getPost('pageBlockId');
                $imageId = $request->getPost('imageId');

                $deleted = false;

                $imageSliderBlock = $this->getBlockManager()->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($imageSliderBlock instanceof \Site\Block\ImageSlider\ImageSlider)
                {
                    $deleted = $imageSliderBlock->deleteImageById($imageId);
                }

                if ($deleted)
                {
                    $jsonResponse->meta->customStatus = 'IMAGE_DELETED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'IMAGE_DELETION_ERROR';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function reorderImagesAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageBlockId = (int)$request->getPost('pageBlockId');
                $parentImageId = $request->getPost('parentImageId');
                $parentImageId = $parentImageId ? (int)$parentImageId : null;
                $images = $request->getPost('images');

                $blockManager = $this->getBlockManager();
                $imageSliderBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $reorderingSuccessful = $imageSliderBlock->reorderImages($parentImageId, $images);

                if ($reorderingSuccessful)
                {
                    $jsonResponse->meta->customStatus = 'MENU_ITEMS_REORDERED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'MENU_ITEMS_REORDERING_ERROR';
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to reorder images', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function toggleImagePublishingAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageBlockId = (int)$request->getPost('pageBlockId');
                $imageId = (int)$request->getPost('imageId');
                $published = (int)$request->getPost('published') ? 1 : 0;

                $blockManager = $this->getBlockManager();
                $imageSliderBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $updated = $imageSliderBlock->toggleImagePublished($imageId, $published);
                if (!$updated)
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to update image', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }
}
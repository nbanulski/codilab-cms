<?php
namespace Site\Controller;

class BlockEventDetailsController extends BaseController
{
    public function getSignUpFormViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $eventListContentId = $request->getPost('eventListContentId');

            $blockManager = $this->getBlockManager();
            $pageBlockId = $blockManager->getPageBlockIdByContentId($eventListContentId);
            $signUpFormViewHtml =
                $blockManager->callPageBlockCustomMethod(
                    $pageBlockId, 'renderSignUpForm', array($this->request, true)
                );
            if ($signUpFormViewHtml != '')
            {
                $jsonResponse->data = $signUpFormViewHtml;
            }
            else
            {
                $jsonResponse->meta->requestErrorMessage =
                    $this->translate(
                        'Failed to fetch template', 'default', $this->getUserLanguage()->zend2_locale
                    ) . '.'
                ;
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function reorderEventImagesAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $eventId = $request->getPost('eventId');
                $contentId = $request->getPost('contentId');
                $images = $request->getPost('images');

                $reorderingSuccessful = false;

                $blockManager = $this->getBlockManager();
                $eventListBlock = $blockManager->createBlockUsingContentId($contentId);
                if ($eventListBlock instanceof \Site\Block\EventList\EventList)
                {
                    $reorderingSuccessful = $eventListBlock->reorderEventImages($eventId, $images);
                }

                if ($reorderingSuccessful)
                {
                    $jsonResponse->meta->customStatus = 'IMAGES_REORDERED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'IMAGES_REORDERING_ERROR';
                    $jsonResponse->meta->requestErrorMessage =
                        $this->translate(
                            'Failed to reorder event images', 'default',
                            $this->getUserLanguage()->zend2_locale
                        ) . '.'
                    ;
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function signUpForEventAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $customStatus = 'FAILED_TO_SIGN_UP_FOR_EVENT';

            $eventListContentId = $request->getPost('eventListContentId');
            $formData = $request->getPost('formData');
            $formData = new \Site\Custom\FlexibleContainer($formData);

            $eventListBlock = $this->getBlockManager()->createBlockUsingContentId($eventListContentId);
            if ($eventListBlock instanceof \Site\Block\EventList\EventList)
            {
                $signedUpForEvent = $eventListBlock->signUpForEvent($formData);
                if ($signedUpForEvent === true)
                {
                    $customStatus = 'USER_ALREADY_SIGNED_UP_FOR_EVENT';
                }
                else if ($signedUpForEvent > 0)
                {
                    $customStatus = 'SUBSCRIPTION_SAVED';
                }
            }

            $jsonResponse->meta->customStatus = $customStatus;

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateEventTitleAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $updated = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $eventId = $request->getPost('eventId');
                $eventTitle = $request->getPost('eventTitle');

                $blockManager = $this->getBlockManager();
                $eventDetailsBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($eventDetailsBlock instanceof \Site\Block\EventDetails\EventDetails)
                {
                    $updated = $eventDetailsBlock->updateEventTitle($eventId, $eventTitle);
                }

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage =
                        $this->translate(
                            'Failed to update event title', 'default',
                            $this->getUserLanguage()->zend2_locale
                        ) . '.'
                    ;
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateEventGalleryImageTitleAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $updated = false;

                $contentId = $request->getPost('contentId');
                $eventId = $request->getPost('eventId');
                $imageId = $request->getPost('imageId');
                $imageTitle = $request->getPost('imageTitle');

                $blockManager = $this->getBlockManager();
                $eventListBlock = $blockManager->createBlockUsingContentId($contentId);
                if ($eventListBlock instanceof \Site\Block\EventList\EventList)
                {
                    $updated = $eventListBlock->updateExistingGalleryImageTitleByEventIdAndImageId(
                        $eventId, $imageId, $imageTitle
                    );
                }

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage =
                        $this->translate(
                            'Failed to update event image title', 'default',
                            $this->getUserLanguage()->zend2_locale
                        ) . '.'
                    ;
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateEventContentsAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageBlockId = (int)$request->getPost('pageBlockId');
                $eventId = (int)$request->getPost('eventId');
                $eventContents = $request->getPost('eventContents');

                $updated = false;

                $blockManager = $this->getBlockManager();
                $eventDetailsBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($eventDetailsBlock instanceof \Site\Block\EventDetails\EventDetails)
                {
                    $updated = $eventDetailsBlock->updateEventContents($eventId, $eventContents);
                }

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to update event contents', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateEventDateStartAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageBlockId = (int)$request->getPost('pageBlockId');
                $eventId = (int)$request->getPost('eventId');
                $startHourWithMinutes = $request->getPost('startHourWithMinutes');

                $updated = false;

                $blockManager = $this->getBlockManager();
                $eventDetailsBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($eventDetailsBlock instanceof \Site\Block\EventDetails\EventDetails)
                {
                    $updated = $eventDetailsBlock->updateEventDateStartBySettingNewHourAndMinutes(
                        $eventId, $startHourWithMinutes
                    );
                }

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate(
                        'Failed to update event start hour', 'default', $this->getUserLanguage()->zend2_locale
                    ) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateEventAdditionalDataAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageBlockId = (int)$request->getPost('pageBlockId');
                $eventId = (int)$request->getPost('eventId');
                $dataTypeId = (int)$request->getPost('dataTypeId');
                $data = $request->getPost('data');

                $updated = false;

                $blockManager = $this->getBlockManager();
                $eventDetailsBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($eventDetailsBlock instanceof \Site\Block\EventDetails\EventDetails)
                {
                    $updated = $eventDetailsBlock->updateEventAdditionalData($eventId, $dataTypeId, $data);
                }

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to update event additional data', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }
}
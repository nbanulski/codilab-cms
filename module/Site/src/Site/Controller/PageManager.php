<?php
namespace Site\Controller;

use Zend\Mvc\Controller\AbstractController;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Mvc\MvcEvent;
use Site\Custom\FlexibleContainer;

class PageManager extends AbstractController
{
    protected $cmsConfig = null;
    protected $serviceLocator = null;
    protected $language = null;
    protected $userLanguage = null;
    protected $identity = null;
    protected $databaseManager = null;
    protected $blockManager = null;
    protected $languageManager = null;
    protected $defaultLanguage = null;

    public function __construct(
        $cmsConfig, ServiceLocatorInterface $serviceLocator,
        \stdClass $language, FlexibleContainer $userLanguage, $identity
    )
    {
        $this->cmsConfig = $cmsConfig;
        $this->serviceLocator = $serviceLocator;
        $this->language = $language;
        $this->userLanguage = $userLanguage;
        $this->identity = $identity;
    }

    public function onDispatch(MvcEvent $e)
    {
    }

    public function getBlockManager()
    {
        if ($this->blockManager == null)
        {
            $sql = $this->getDatabaseManager()->getSql();
            $this->blockManager = new BlockManager(
                $this->cmsConfig, $this->serviceLocator, $sql, $this->getRequest(),
                $this->language, $this->userLanguage, $this->identity
            );
        }

        return $this->blockManager;
    }

    public function setBlockManager(BlockManager $blockManager)
    {
        $this->blockManager = $blockManager;

        return $this;
    }

    public function getDatabaseManager()
    {
        return $this->databaseManager;
    }

    public function setDatabaseManager(DatabaseManager $databaseManager)
    {
        $this->databaseManager = $databaseManager;

        return $this;
    }

    public function getLanguageManager()
    {
        if ($this->languageManager == null)
        {
            $this->languageManager = new LanguageManager(
                $this->cmsConfig,
                $this->serviceLocator,
                $this->language,
                $this->identity
            );
            $this->languageManager->setDatabaseManager($this->getDatabaseManager());
        }

        return $this->languageManager;
    }

    /**
     * Page data fetch functions.
     */
    public function getMainPageForSpecificLanguage($specificLanguageId = null)
    {
        $languageId = ($specificLanguageId !== null) ? (int)$specificLanguageId : (int)$this->language->language_id;

        if ($languageId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();

        $select = $dbManager->getSql()->select();
        $select
            ->from('pages')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'parent_id' => null,
                    'language_id' => $languageId,
                    'is_published' => 1,
                    'clean_url' => ''
                )
            )
            ->limit(1);
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getPages($specificLanguageId = null, $onlyFromSpecificLanguage = true)
    {
        $languageId = ($specificLanguageId !== null) ? (int)$specificLanguageId : (int)$this->language->language_id;

        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();

        $select = $dbManager->getSql()->select();
        $select
            ->from('pages')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable);

        if ($onlyFromSpecificLanguage && ($languageId > 0))
        {
            $select->where(array('language_id' => $languageId));
        }

        $user = $this->identity;
        if (!$user || ($user->account->role != 'admin'))
        {
            $select->where(array('is_published' => 1));
        }

        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getAllPages()
    {
        return $this->getPages(null, false);
    }

    public function getListOfNavigationMenuBlocksWherePageExistsByPageId($pageId, $specificLanguageId = null)
    {
        $pageId = (int)$pageId;
        $languageId = ($specificLanguageId !== null) ? (int)$specificLanguageId : (int)$this->language->language_id;

        if (($pageId <= 0) || ($languageId <= 0))
        {
            return false;
        }

        $prefixColumnsWithTable = true; // "true" is required in this query!
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(array('bnmi' => 'block_navigation_menu_items'))
            ->columns(array('content_id'), $prefixColumnsWithTable)
            ->join(
                array('pbca' => 'pages_blocks_contents_association'),
                'pbca.content_id = bnmi.content_id',
                array('page_block_id'),
                $select::JOIN_LEFT
            )
            ->join(
                array('pb' => 'pages_blocks'),
                'pb.page_block_id = pbca.page_block_id',
                array('page_id', 'block_id', 'is_parent_block_dynamic'),
                $select::JOIN_LEFT
            )
            ->join(
                array('p' => 'pages'),
                'p.page_id = pb.page_id',
                array('parent_id', 'language_id', 'clean_url', 'title'),
                $select::JOIN_LEFT
            )
            ->join(
                array('bp' => 'blocks_properties'),
                new \Zend\Db\Sql\Expression(
                    'bp.block_id = pb.block_id AND bp.language_id = ?',
                    $languageId
                ),
                array('name'),
                $select::JOIN_LEFT
            )
            ->where(array('bnmi.page_id' => $pageId))
            ->group('bnmi.content_id');
        $resultSet = $dbManager->getSpecificHydratedResultSet(
            $select,
            new \Site\Hydrator\PageHydrator($this),
            new \Site\Custom\FlexibleContainer()
        );

        return $resultSet;
    }

    public function getPageTextBreadcrumbByPageId($pageId)
    {
        $page = $this->getPageById($pageId);
        if (!$page)
        {
            return false;
        }

        $subPage = null;
        if ($page->parent_id > 0)
        {
            $subPage = $page;

            $page = $this->getPageById($page->parent_id);
            if (!$page)
            {
                return false;
            }
        }

        $breadcrumb = $page->title;

        if ($subPage)
        {
            $breadcrumb .= ' > ' . $subPage->title;
        }

        return $breadcrumb;
    }

    public function getPageById($pageId)
    {
        $pageId = (int)$pageId;

        if ($pageId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();

        $select = $dbManager->getSql()->select();
        $select
            ->from('pages')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('page_id' => $pageId));

        $user = $this->identity;
        if (!$user || ($user->account->role != 'admin'))
        {
            $select->where(array('is_published' => 1));
        }

        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getPageByParentIdAndCleanUrl($parentId, $cleanUrl, $specificLanguageId = null)
    {
        $parentId = $parentId ? (int)$parentId : null;
        $cleanUrl = trim((string)$cleanUrl);
        $languageId = ($specificLanguageId !== null) ? (int)$specificLanguageId : (int)$this->language->language_id;

        if ((($parentId !== null) && ($parentId <= 0)) || ($languageId <= 0))
        {
            return false;
        }

        if ($cleanUrl == trim($this->cmsConfig['default_name_for_index_page']))
        {
            $cleanUrl = '';
        }

        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();

        $select = $dbManager->getSql()->select();
        $select
            ->from('pages')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'parent_id' => $parentId,
                    'language_id' => $languageId,
                    'clean_url' => $cleanUrl
                )
            )
            ->limit(1);

        $user = $this->identity;
        if (!$user || ($user->account->role != 'admin'))
        {
            $select->where(array('is_published' => 1));
        }

        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getPageIdByParentPageCleanUrl($parentPageCleanUrl, $specificLanguageId = null)
    {
        $parentPageCleanUrl = trim((string)$parentPageCleanUrl);
        $languageId = ($specificLanguageId !== null) ? (int)$specificLanguageId : (int)$this->language->language_id;

        if ($languageId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();

        $select = $dbManager->getSql()->select();
        $select
            ->from('pages')
            ->columns(array('page_id'), $prefixColumnsWithTable)
            ->where(
                array(
                    'parent_id' => null,
                    'language_id' => $languageId,
                    'clean_url' => $parentPageCleanUrl
                )
            )
            ->limit(1);

        $user = $this->identity;
        if (!$user || ($user->account->role != 'admin'))
        {
            $select->where(array('is_published' => 1));
        }

        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();
            if ($row)
            {
                return $row->page_id;
            }
        }

        return false;
    }

    public function getPageIdByCleanUrls($cleanUrl, $subPageCleanUrl, $specificLanguageId = null)
    {
        $cleanUrl = trim((string)$cleanUrl);
        $subPageCleanUrl = trim((string)$subPageCleanUrl);
        $languageId = ($specificLanguageId !== null) ? (int)$specificLanguageId : (int)$this->language->language_id;

        if ($languageId <= 0)
        {
            return false;
        }

        if ($cleanUrl == trim($this->cmsConfig['default_name_for_index_page']))
        {
            $cleanUrl = '';
            $subPageCleanUrl = '';
        }

        $parentPageId = null;
        if ($cleanUrl != '')
        {
            $parentPageId = $this->getPageIdByParentPageCleanUrl($cleanUrl, $languageId);
        }

        return $this->getPageByParentIdAndCleanUrl($parentPageId, $subPageCleanUrl, $specificLanguageId = null);
    }

    public function getPageLanguageEquivalentByPageIdAndLanguageId($pageId, $languageId)
    {
        $pageId = (int)$pageId;
        $languageId = (int)$languageId;

        if (($pageId <= 0) || ($languageId <= 0))
        {
            return false;
        }

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select()
            ->from('pages_language_equivalents')
            ->where(array('page_id' => $pageId, 'language_id' => $languageId));
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getPageLanguageEquivalentsByPageId($pageId)
    {
        $pageId = (int)$pageId;

        if ($pageId <= 0)
        {
            return false;
        }

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql
            ->select()
            ->from('pages_language_equivalents')
            ->where(array('page_id' => $pageId));
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getPageLanguageIdByPageId($pageId)
    {
        $pageId = (int)$pageId;
        if ($pageId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql
            ->select()
            ->columns(array('language_id'), $prefixColumnsWithTable)
            ->from('pages')
            ->where(array('page_id' => $pageId));
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();

            return $row ? $row->language_id : false;
        }

        return false;
    }

    public function getAllAvailableParentPagesByPageIdAndLanguageId($pageId, $specificLanguageId = null)
    {
        $pageId = (int)$pageId ? : null;
        $languageId = ($specificLanguageId !== null) ? (int)$specificLanguageId : (int)$this->language->language_id;

        if ((($pageId !== null) && ($pageId <= 0)) || ($languageId <= 0))
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select()
            ->columns(array('page_id', 'language_id', 'title'), $prefixColumnsWithTable)
            ->from('pages');

        if ($pageId > 0)
        {
            $select->where->notEqualTo('page_id', $pageId);
        }

        $select->where(
            array(
                'parent_id' => null,
                'language_id' => $languageId
            )
        );

        if ($pageId > 0)
        {
            $select->where->notEqualTo('clean_url', '');
        }

        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getAllAvailablePageLanguageEquivalents()
    {
        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select()
            ->columns(array('page_id', 'language_id', 'title'), $prefixColumnsWithTable)
            ->from('pages');

        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getAllAvailablePageLanguageEquivalentsExcludingLanguageId($languageId)
    {
        $languageId = (int)$languageId;

        if ($languageId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select()
            ->columns(array('page_id', 'language_id', 'title'), $prefixColumnsWithTable)
            ->from('pages')
            ->where('language_id != ' . $languageId);

        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getAllAvailablePageLanguageEquivalentsExcludingPageIdAndLanguageId($pageId, $languageId)
    {
        $pageId = (int)$pageId;
        $languageId = (int)$languageId;

        if (($pageId <= 0) || ($languageId <= 0))
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select()
            ->columns(array('page_id', 'language_id', 'title'), $prefixColumnsWithTable)
            ->from('pages')
            ->where('page_id != ' . $pageId)
            ->where('language_id != ' . $languageId);

        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getPageViews($pageId)
    {
        $pageId = (int)$pageId;

        if ($pageId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select()
            ->from('pages')
            ->columns(array('views'), $prefixColumnsWithTable)
            ->where(array('page_id' => $pageId));
        $resultSet = $dbManager->getHydratedResultSet($select);
        $page = $resultSet->current();
        if ($page)
        {
            return $page->views;
        }

        return false;
    }

    public function getPageBlocksByPageId($pageId)
    {
        $pageId = (int)$pageId;

        if ($pageId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();

        $select = $dbManager->getSql()->select();
        $select
            ->from('pages_blocks')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('page_id' => $pageId))
            ->order('order ASC');

        $user = $this->identity;
        if (!$user || !$user->session->designMode)
        {
            $select->where(array('is_published' => 1));
        }

        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function pageContainsDynamicBlockInSlot($pageId, $blockId, $slotId)
    {
        $pageId = (int)$pageId;
        $blockId = (int)$blockId;
        $slotId = (int)$slotId;

        if (($pageId <= 0) || ($blockId <= 0) || ($slotId <= 0))
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();

        $select = $dbManager->getSql()->select();
        $select
            ->from('pages_blocks')
            ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')), $prefixColumnsWithTable)
            ->where(
                array(
                    'page_id' => $pageId,
                    'block_id' => $blockId,
                    'slot_number' => $slotId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);

        if (!$resultSet)
        {
            throw new \Site\Exception\CmsException(
                'Could not check the number of dynamic block (id: ' . $blockId . ') occurrences ' .
                'in page ' . $pageId . '.'
            );
        }

        $row = $resultSet->current();
        $contains = ($row->count > 0) ? true : false;

        return $contains;
    }

    public function getLayouts()
    {
        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();

        $select = $dbManager->getSql()->select();
        $select
            ->from(array('l' => 'layouts'))
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->join(
                array('lp' => 'layouts_properties'),
                'lp.layout_id = l.layout_id',
                array('name'),
                $select::JOIN_LEFT
            )
            ->where(array('lp.language_id' => $this->language->language_id));
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getSubLayouts()
    {
        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();

        $select = $dbManager->getSql()->select();
        $select
            ->from(array('sl' => 'sub_layouts'))
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->join(
                array('slp' => 'sub_layouts_properties'),
                'slp.sub_layout_id = sl.sub_layout_id',
                array('name'),
                $select::JOIN_LEFT
            )
            ->where(array('slp.language_id' => $this->language->language_id))
            ->order('order ASC');
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getLayoutById($layoutId)
    {
        $layoutId = (int)$layoutId;

        if ($layoutId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();

        $select = $dbManager->getSql()->select();
        $select
            ->from('layouts')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('layout_id' => $layoutId));
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getSubLayoutById($subLayoutId)
    {
        $subLayoutId = (int)$subLayoutId;

        if ($subLayoutId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();

        $select = $dbManager->getSql()->select();
        $select
            ->from('sub_layouts')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('sub_layout_id' => $subLayoutId));
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    /**
     * Page logic functions.
     */
    public function getFullPageCleanUrlByPageId($pageId)
    {
        $page = $this->getPageById($pageId);
        if (!$page)
        {
            return false;
        }

        $subPage = null;
        if ($page->parent_id > 0)
        {
            $subPage = $page;

            $page = $this->getPageById($page->parent_id);
            if (!$page)
            {
                return false;
            }
        }

        $languageManager = $this->getLanguageManager();
        $languageIsoCode = $languageManager->getLanguageIsoCodeByLanguageId($page->language_id);
        $cleanUrl = trim($page->clean_url);

        if ($cleanUrl != '')
        {
            $cleanUrl = '/' . $languageIsoCode . '/page/' . $cleanUrl;

            if ($subPage)
            {
                $subPageCleanUrl = trim($subPage->clean_url);
                $cleanUrl .= '/' . $subPageCleanUrl;
            }
        }
        else
        {
            $cleanUrl = '/' . $languageIsoCode . '/page/' . trim($this->cmsConfig['default_name_for_index_page']);
        }

        return $cleanUrl;
    }

    public function getFullPageCleanUrlByLanguageIsoCodeAndPage(
        $languageIsoCode,
        $page
    ) {
        $languageIsoCode = trim((string)$languageIsoCode);

        if (($languageIsoCode == '') || !$page)
        {
            return false;
        }

        $pageCleanUrl = $page->clean_url;
        $subPageCleanUrl = '';

        if ($page->parent_id > 0)
        {
            $page = $this->getPageById($page->parent_id);
            if (!$page)
            {
                return false;
            }

            $subPageCleanUrl = $pageCleanUrl;
            $pageCleanUrl = $page->clean_url;
        }

        if ($pageCleanUrl == '')
        {
            $pageCleanUrl = trim($this->cmsConfig['default_name_for_index_page']);
        }

        $fullPageCleanUrl = '/' . $languageIsoCode . '/page/' . $pageCleanUrl;

        if ($subPageCleanUrl != '')
        {
            $fullPageCleanUrl .= '/' . $subPageCleanUrl;
        }

        return $fullPageCleanUrl;
    }

    public function makePageBlockArrayUsingPageBlocks($pageBlocks)
    {
        $blockArray = new \Site\Custom\FlexibleContainer();

        if ($pageBlocks)
        {
            $blockManager = $this->getBlockManager();
            $languageId = $this->language->language_id;

            foreach ($pageBlocks as $pageBlock)
            {
                $blockId = (int)$pageBlock->block_id;
                $slotNumber = (int)$pageBlock->slot_number;
                $block = $blockManager->getBlockById($blockId);
                $blockProperties = $blockManager->getBlockPropertiesByBlockIdAndLanguageId($blockId, $languageId);
                $pageBlockContents = $blockManager->getPageBlockContentsByPageBlockId($pageBlock->page_block_id);

                $blockInstance = $blockManager->createBlockUsingPhpClassNameAndAllBlockInfo(
                    $block->php_class_name,
                    $block,
                    $blockProperties,
                    $pageBlock,
                    $pageBlockContents
                );
                if ($blockInstance)
                {
                    $blocksInSlot = $blockArray[$slotNumber];
                    $blocksInSlot[] = $blockInstance;

                    $blockArray[$slotNumber] = $blocksInSlot;
                }
            }
        }

        return $blockArray;
    }

    public function incrementPageViewCounter($pageId)
    {
        $pageId = (int)$pageId;

        if ($pageId <= 0)
        {
            return false;
        }

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $views = $this->getPageViews($pageId);
        if ($views !== false)
        {
            $views++;
            if (($views > 0) && ($views < 2000000000)) // Todo: fix max int size.
            {
                $update = $sql->update('pages');
                $update
                    ->set(array('views' => $views))
                    ->where(array('page_id' => $pageId));
                $result = $dbManager->executePreparedStatement($update);
                if ($result)
                {
                    $views = $this->getPageViews($pageId);

                    return $views;
                }
            }
        }

        return false;
    }

    public function togglePagePublishing($pageId, $published)
    {
        $pageId = (int)$pageId;

        if ($pageId <= 0)
        {
            return false;
        }

        $published = $published ? 1 : 0;

        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $update = $sql->update('pages');
        $update
            ->set(array('is_published' => $published))
            ->where(array('page_id' => $pageId));
        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $select = $sql->select()
                ->from('pages')
                ->columns(array('is_published'), $prefixColumnsWithTable)
                ->where(array('page_id' => $pageId));
            $resultSet = $dbManager->getHydratedResultSet($select);
            $page = $resultSet->current();
            if ($page)
            {
                return $page->is_published;
            }
        }

        return false;
    }

    public function createNewPage($pageData, $specificLanguageId = null)
    {
        if (!$pageData)
        {
            return false;
        }

        $created = false;
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $pageParentPageId = $pageData['pageParentPageId'] ? (int)$pageData['pageParentPageId'] : null;
        $pageLanguage = (int)$pageData['pageLanguage'];
        $allowIndexingPage = $pageData['allowIndexingPage'] ? 1 : 0;
        $allowBackingUpPage = ($allowIndexingPage && $pageData['allowBackingUpPage']) ? 1 : 0;
        $cleanUrl = (string)$pageData['cleanUrl'];
        //$title = $this->escapeForHtml($pageData['pageTitle']);
        //$metaKeywords = $this->escapeForHtml($pageData['metaKeywords']);
        //$metaDescription = $this->escapeForHtml($pageData['metaDescription']);
        $title = $pageData['pageTitle'];
        $metaKeywords = $pageData['metaKeywords'];
        $metaDescription = $pageData['metaDescription'];

        if (($pageParentPageId !== null) && ($pageParentPageId <= 0))
        {
            return false;
        }

        if ($specificLanguageId !== null)
        {
            $languageId = (int)$specificLanguageId;
        }
        else
        {
            if ($pageLanguage > 0)
            {
                $languageId = $pageLanguage;
            }
            else
            {
                $languageId = (int)$this->language->language_id;
            }
        }

        if ($languageId <= 0)
        {
            return false;
        }

        $dbManager->beginTransaction();

        $page = $this->getPageByParentIdAndCleanUrl($pageParentPageId, $cleanUrl, $languageId);
        if ($page)
        {
            $this->throwPageCleanUrlAlreadyInUseException();
        }

        $insert = $sql->insert('pages');
        $insert
            ->columns(
                array(
                    'parent_id',
                    'language_id',
                    'layout_id',
                    'sub_layout_id',
                    'is_published',
                    'views',
                    'url_priority',
                    'allow_indexing',
                    'allow_backing_up',
                    'disallow_caching',
                    'last_update_author_id',
                    'last_update_date',
                    'clean_url',
                    'frequency_of_changes',
                    'title',
                    'meta_keywords',
                    'meta_description'
                )
            )
            ->values(
                array(
                    'parent_id' => $pageParentPageId,
                    'language_id' => $languageId,
                    'layout_id' => (int)$pageData['layoutId'],
                    'sub_layout_id' => (int)$pageData['subLayoutId'],
                    'is_published' => $pageData['isPublished'] ? 1 : 0,
                    'views' => 0,
                    'url_priority' => (float)$pageData['urlPriority'],
                    'allow_indexing' => $allowIndexingPage,
                    'allow_backing_up' => $allowBackingUpPage,
                    'disallow_caching' => $pageData['disallowCachingPage'] ? 1 : 0,
                    'last_update_author_id' => (int)$pageData['authorId'],
                    'last_update_date' => new \Zend\Db\Sql\Expression('NOW()'),
                    'clean_url' => $cleanUrl,
                    'frequency_of_changes' => (string)$pageData['frequencyOfChanges'],
                    'title' => $title,
                    'meta_keywords' => $metaKeywords,
                    'meta_description' => $metaDescription
                )
            );
        $pageId = $dbManager->executeInsertAndGetLastInsertId($insert);
        if ($pageId > 0)
        {
            $equivalentsInserted = false;
            $languageCount = $this->getLanguageManager()->getLanguageCount();
            if ($languageCount !== false) // Language count successfully fetched.
            {
                if ($languageCount > 1) // There are more than one language.
                {
                    $languageEquivalents = $pageData['languageEquivalents'];
                    if ($languageEquivalents)
                    {
                        // Mark operation as failed only when valid page id was found in $languageEquivalents array
                        // AND insert-statement fails. Sometimes array can contain empty values (mistakes in HTML-form),
                        // but this does not mean that we should recognize it as error.
                        $equivalentsInserted = true;

                        foreach ($languageEquivalents as $equivalentLanguageId => $associatedPageId)
                        {
                            $associatedPageId = (int)$associatedPageId;
                            if ($associatedPageId > 0)
                            {
                                if ($equivalentLanguageId == $languageId)
                                {
                                    continue;
                                }

                                $insert = $sql->insert('pages_language_equivalents');
                                $insert
                                    ->columns(
                                        array(
                                            'page_id',
                                            'language_id',
                                            'associated_page_id'
                                        )
                                    )
                                    ->values(
                                        array(
                                            'page_id' => $pageId,
                                            'language_id' => $equivalentLanguageId,
                                            'associated_page_id' => $associatedPageId
                                        )
                                    );
                                $equivalentId = $dbManager->executeInsertAndGetLastInsertId($insert);
                                if ($equivalentId <= 0)
                                {
                                    $equivalentsInserted = false;

                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        // There is only one language, so there is no need to insert equivalents of this page for
                        // other languages.
                        $equivalentsInserted = true;
                    }
                }
            }

            if ($equivalentsInserted)
            {
                $created = true;
            }
        }

        if ($created)
        {
            $dbManager->commit();

            return $pageId;
        }
        else
        {
            $dbManager->rollback();
        }

        return false;
    }

    public function updateExistingPage($pageData)
    {
        if (!$pageData)
        {
            return false;
        }

        $updated = false;
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $pageId = (int)$pageData['pageId'];
        $pageParentPageId = $pageData['pageParentPageId'] ? (int)$pageData['pageParentPageId'] : null;
        $pageLanguageId = (int)$pageData['pageLanguage'];
        $cleanUrl = (string)$pageData['cleanUrl'];

        if ($pageId <= 0)
        {
            return false;
        }

        if (($pageParentPageId !== null) && ($pageParentPageId <= 0))
        {
            return false;
        }

        $dbManager->beginTransaction();

        $page = $this->getPageByParentIdAndCleanUrl($pageParentPageId, $cleanUrl, $pageLanguageId);
        if ($page && ($page->page_id != $pageId))
        {
            $this->throwPageCleanUrlAlreadyInUseException();
        }

        $page = $this->getPageById($pageId);
        if ($page)
        {
            $allowIndexingPage = $pageData['allowIndexingPage'] ? 1 : 0;
            $allowBackingUpPage = ($allowIndexingPage && $pageData['allowBackingUpPage']) ? 1 : 0;
            //$title = $this->escapeForHtml($pageData['pageTitle']);
            //$metaKeywords = $this->escapeForHtml($pageData['metaKeywords']);
            //$metaDescription = $this->escapeForHtml($pageData['metaDescription']);
            $title = $pageData['pageTitle'];
            $metaKeywords = $pageData['metaKeywords'];
            $metaDescription = $pageData['metaDescription'];

            $update = $sql->update('pages');
            $update->set(
                array(
                    'parent_id' => $pageParentPageId,
                    'language_id' => $pageLanguageId,
                    'layout_id' => (int)$pageData['layoutId'],
                    'sub_layout_id' => (int)$pageData['subLayoutId'],
                    'is_published' => $pageData['isPublished'] ? 1 : 0,
                    'views' => 0,
                    'url_priority' => (float)$pageData['urlPriority'],
                    'allow_indexing' => $allowIndexingPage,
                    'allow_backing_up' => $allowBackingUpPage,
                    'disallow_caching' => $pageData['disallowCachingPage'] ? 1 : 0,
                    'last_update_author_id' => (int)$pageData['authorId'],
                    'last_update_date' => new \Zend\Db\Sql\Expression('NOW()'),
                    'clean_url' => $cleanUrl,
                    'frequency_of_changes' => (string)$pageData['frequencyOfChanges'],
                    'title' => $title,
                    'meta_keywords' => $metaKeywords,
                    'meta_description' => $metaDescription
                )
            )
                ->where(array('page_id' => (int)$pageId));
            $result = $dbManager->executePreparedStatement($update);
            if ($result)
            {
                $languageId = (int)$page->language_id;

                $delete = $sql->delete();
                $delete->from('pages_language_equivalents')
                    ->where(array('page_id' => $pageId));
                $result = $dbManager->executePreparedStatement($delete);
                if ($result)
                {
                    $equivalentsInserted = false;
                    $languageCount = $this->getLanguageManager()->getLanguageCount();

                    if ($languageCount !== false) // Language count successfully fetched.
                    {
                        if ($languageCount > 1) // There are more than one language.
                        {
                            $languageEquivalents = $pageData['languageEquivalents'];
                            if ($languageEquivalents)
                            {
                                foreach ($languageEquivalents as $equivalentLanguageId => $associatedPageId)
                                {
                                    if ($equivalentLanguageId == $languageId)
                                    {
                                        continue;
                                    }

                                    $insert = $sql->insert('pages_language_equivalents');
                                    $insert->columns(
                                        array(
                                            'page_id',
                                            'language_id',
                                            'associated_page_id'
                                        )
                                    )
                                        ->values(
                                            array(
                                                'page_id' => $pageId,
                                                'language_id' => $equivalentLanguageId,
                                                'associated_page_id' => $associatedPageId
                                            )
                                        );
                                    $equivalentId = $dbManager->executeInsertAndGetLastInsertId($insert);
                                    if ($equivalentId > 0)
                                    {
                                        $equivalentsInserted = true;
                                    }
                                    else
                                    {
                                        $equivalentsInserted = false;

                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            // There is only one language, so there is no need to insert equivalents of this page for
                            // other languages.
                            $equivalentsInserted = true;
                        }
                    }

                    if ($equivalentsInserted)
                    {
                        $updated = true;
                    }
                }
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function deletePageById($pageId, $actionToTakOnMenus)
    {
        $pageId = (int)$pageId;
        $actionToTakOnMenus = trim((string)$actionToTakOnMenus);

        if (
            ($pageId <= 0) ||
            !in_array(
                $actionToTakOnMenus,
                array('delete_all_items', 'drop_url_in_all_items_and_make_them_unpublished')
            )
        )
        {
            return false;
        }

        $deleted = false;

        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $menusUpdatedSuccessfully = true;
        if ($actionToTakOnMenus == 'drop_url_in_all_items_and_make_them_unpublished')
        {
            $update = $sql->update('block_navigation_menu_items');
            $update
                ->set(
                    array(
                        'page_id' => null,
                        'url_address_type' => 2 // Set URL type to "None".
                    )
                )
                ->where(array('page_id' => $pageId));
            $result = $dbManager->executePreparedStatement($update);
            $menusUpdatedSuccessfully = $result ? true : false;
        }

        if ($menusUpdatedSuccessfully)
        {
            $delete = $sql->delete('pages')->where(array('page_id' => $pageId));
            $result = $dbManager->executePreparedStatement($delete);
            if ($result)
            {
                $select = $sql->select('pages')
                    ->columns(array('page_id'), $prefixColumnsWithTable)
                    ->where(array('page_id' => $pageId));
                $resultSet = $dbManager->getHydratedResultSet($select);
                if ($resultSet)
                {
                    $page = $resultSet->current();
                    if ($page == null)
                    {
                        $deleted = true;
                    }
                }
            }
        }

        if ($deleted)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $deleted;
    }

    /**
     * Other fetch functions.
     */

    /*public function getLanguages($excludeCurrentLanguage = false)
    {
        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();

        $select = $dbManager->getSql()->select();
        $select->from('languages')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->order('english_name ASC');

        if ($excludeCurrentLanguage)
        {
            $select->where('language_id != ' . $this->language->language_id);
        }

        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }*/

    protected function throwPageCleanUrlAlreadyInUseException()
    {
        //$exceptionMessage = $this->translate('You cannot have more than one page with the same clean-URL for that', 'default', $this->getUserLanguage()->zend2_locale);
        $exceptionMessage = 'Nie możesz mieć więcej niż jedną stronę z takim samym adresem URL dla danego języka. ' .
            'Zmień rodzica strony lub wprowadź inny adres URL i spróbuj ponownie.';

        throw new \Site\Exception\PageCleanUrlAlreadyInUseException($exceptionMessage);
    }
}
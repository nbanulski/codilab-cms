<?php
namespace Site\Controller;

class BlockBigBannerController extends BaseController
{
    public function toggleBannerPublishingAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $updated = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $bannerId = $request->getPost('bannerId');
                $published = (int)$request->getPost('published') ? 1 : 0;

                $blockManager = $this->getBlockManager();
                $bigBannerBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($bigBannerBlock instanceof \Site\Block\BigBanner\BigBanner)
                {
                    $updated = $bigBannerBlock->toggleBannerPublished($bannerId, $published);
                }

                if (!$updated)
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to publish / unpublish banner', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function deleteBannerAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) // Check if this is an AJAX request.
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->identity();

            if ($user && $user->session->designMode)
            {
                $deleted = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $bannerId = $request->getPost('bannerId');
                
                $blockManager = $this->getBlockManager();
                $bigBannerBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($bigBannerBlock instanceof \Site\Block\BigBanner\BigBanner)
                {
                    $deleted = $bigBannerBlock->deleteBannerById($bannerId);
                }

                if ($deleted)
                {
                    $jsonResponse->meta->customStatus = 'BANNER_DELETED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'FAILED_BANNER_DELETE';
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to delete banner', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function createBannerAction()
    {
        $request = $this->getRequest();

        if ($request->isXmlHttpRequest()) // Check if this is an AJAX request.
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->identity();

            if ($user && $user->session->designMode)
            {
                $created = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $formData = new \Site\Custom\FlexibleContainer($request->getPost('formData'));

                $blockManager = $this->getBlockManager();
                $bigBannerBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($bigBannerBlock instanceof \Site\Block\BigBanner\BigBanner)
                {
                    $created = $bigBannerBlock->createBanner($formData);
                }

                if ($created)
                {
                    $jsonResponse->meta->customStatus = 'BANNER_SAVED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'BANNER_NOT_SAVED';
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to create banner', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function reorderBannersAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $reorderingSuccessful = false;

                $pageBlockId = (int)$request->getPost('pageBlockId');
                $banners = $request->getPost('banners');

                $blockManager = $this->getBlockManager();
                $bigBannerBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);

                if ($bigBannerBlock instanceof \Site\Block\BigBanner\BigBanner)
                {
                    $reorderingSuccessful = $bigBannerBlock->reorderBanners($banners);
                }

                if ($reorderingSuccessful)
                {
                    $jsonResponse->meta->customStatus = 'BANNERS_REORDERED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'BANNERS_REORDERING_ERROR';
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to reorder banners', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateBannerAction()
    {
        $request = $this->getRequest();

        if ($request->isXmlHttpRequest()) // Check if this is an AJAX request.
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->identity();

            if ($user && $user->session->designMode)
            {
                $updated = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $formData = new \Site\Custom\FlexibleContainer($request->getPost('formData'));

                $blockManager = $this->getBlockManager();
                $bigBannerBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($bigBannerBlock instanceof \Site\Block\BigBanner\BigBanner)
                {
                    $updated = $bigBannerBlock->updateBanner($formData);
                }

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'BANNER_UPDATED';
                }
                else
                {               
                    $jsonResponse->meta->customStatus = 'BANNER_NOT_UPDATED';
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to update banner', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateBannerContentsAction()
    {
        $request = $this->getRequest();

        if ($request->isXmlHttpRequest()) // Check if this is an AJAX request.
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->identity();

            if ($user && $user->session->designMode)
            {
                $updated = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $bannerId = $request->getPost('bannerId');
                $bannerContents = $request->getPost('bannerContents');

                $blockManager = $this->getBlockManager();
                $bigBannerBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($bigBannerBlock instanceof \Site\Block\BigBanner\BigBanner)
                {
                    $updated = $bigBannerBlock->updateBannerContents($bannerId, $bannerContents);
                }

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'BANNER_UPDATED';
                }
                else
                {               
                    $jsonResponse->meta->customStatus = 'BANNER_NOT_UPDATED';
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to update banner contents', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }
            $this->response->setContent((string)$jsonResponse);
        }
        return $this->response;
    }

    public function updateBannerTitleAction()
    {
        $request = $this->getRequest();

        if ($request->isXmlHttpRequest()) // Check if this is an AJAX request.
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->identity();

            if ($user && $user->session->designMode)
            {
                $updated = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $bannerId = $request->getPost('bannerId');
                $bannerTitle = $request->getPost('bannerTitle');

                $blockManager = $this->getBlockManager();
                $bigBannerBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($bigBannerBlock instanceof \Site\Block\BigBanner\BigBanner)
                {
                    $updated = $bigBannerBlock->updateBannerTitle($bannerId, $bannerTitle);
                }

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'BANNER_UPDATED';
                }
                else
                {               
                    $jsonResponse->meta->customStatus = 'BANNER_NOT_UPDATED';
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to update banner title', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }
            $this->response->setContent((string)$jsonResponse);
        }
        return $this->response;
    }
}
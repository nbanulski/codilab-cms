<?php
namespace Site\Controller;

class CodeTestController extends BaseController
{
    public function echoAction()
    {
        echo 'Echo test.';
    }

    public function phpinfoAction()
    {
        phpinfo();

        return $this->getResponse();
    }

    public function vardumpAction()
    {
        $string = 'Some string.';
        var_dump($string);

        return $this->getResponse();
    }

    public function parametersForCurrentPageAction()
    {
        $request = new \Zend\Http\Request();
        $request->setUri('http://proimagine-cms/pol/page/o-uczelni/aktualnosci,spoldzielcze-kasy-oszczednosciowo-kredytow-341,costam,costam2,costam3,costam4');

        $parameters = $this->getParametersForCurrentPageFromRequest($request);

        echo '<br><br><br><br><br>';

        echo '$parameters:';
        var_dump($parameters);

        return $this->getResponse();
    }

    public function parseUrlAction()
    {
        $request = $this->getRequest();

        $parameters = $this->getParametersForCurrentPageFromRequest($request);
        $url = $parameters->key();

        echo '<br><br><br><br><br>';

        echo '$parameters:';
        var_dump($parameters);

        echo '$url:';
        var_dump($url);

        echo 'parse_url() function result:';
        var_dump(parse_url($url));

        var_dump(parse_url('http://proimagine-cms/pol/page/o-uczelni'));
        var_dump(parse_url('http://proimagine-cms/pol/page/o-uczelni/aktualnosci'));
        var_dump(parse_url('http://proimagine-cms/pol/page/o-uczelni/aktualnosci,spoldzielcze-kasy-oszczednosciowo-kredytow-341'));
        var_dump(parse_url('http://proimagine-cms/pol/page/o-uczelni/aktualnosci,spoldzielcze-kasy-oszczednosciowo-kredytow-341#costam'));

        return $this->getResponse();
    }

    public function getAllEventLinksAction()
    {
        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from('block_event_list')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable);
        $resultSet = $dbManager->getHydratedResultSet($select);

        $links = array();
        $linksWithoutEventId = array();

        if ($resultSet)
        {
            foreach ($resultSet as $event)
            {
                $eventCleanUrl = $this->generateCleanUrlFromText($event->title);
                $url = '/pol/page/nauka-i-biznes/konferencje-i-seminaria,' . $eventCleanUrl . '-' . $event->content_id . '-' . $event->event_id;
                $urlWithoutEventId = '/pol/page/nauka-i-biznes/konferencje-i-seminaria,' . $eventCleanUrl . '-' . $event->content_id;
                $link = '<a href="' . $url . '" target="_blank">' . $url . '</a>';
                $linkWithoutEventId = '<a href="' . $urlWithoutEventId . '" target="_blank">' . $urlWithoutEventId . '</a>';

                $links[] = $link;
                $linksWithoutEventId[] = $linkWithoutEventId;
            }
        }

        echo 'Events:' . "<br>\n";
        echo join("<br>\n", $links);
        echo "<br>\n<br>\n" . 'Events (links without event_id):' . "<br>\n";
        echo join("<br>\n", $linksWithoutEventId);

        return $this->getResponse();
    }

    public function getAllNewsLinksAction()
    {
        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from('block_news_list')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable);
        $resultSet = $dbManager->getHydratedResultSet($select);

        $links = array();
        $linksWithoutEventId = array();
        $redirect301Links = array();

        if ($resultSet)
        {
            foreach ($resultSet as $news)
            {
                $newsCleanUrl = $this->generateCleanUrlFromText($news->title);
                $url = '/pol/page/nauka-i-biznes/konferencje-i-seminaria,' . $newsCleanUrl . '-' . $news->content_id . '-' . $news->news_id;
                $urlWithoutEventId = '/pol/page/nauka-i-biznes/konferencje-i-seminaria,' . $newsCleanUrl . '-' . $news->content_id;
                $link = '<a href="' . $url . '" target="_blank">' . $url . '</a>';
                $linkWithoutEventId = '<a href="' . $urlWithoutEventId . '" target="_blank">' . $urlWithoutEventId . '</a>';
                $redirect301 = 'Redirect 301 ' . $urlWithoutEventId . ' ' . $url;

                $links[] = $link;
                $linksWithoutEventId[] = $linkWithoutEventId;
                $redirect301Links[] = $redirect301;
            }
        }

        echo 'News:' . "<br>\n";
        echo join("<br>\n", $links);
        echo "<br>\n<br>\n" . 'News (links without news_id):' . "<br>\n";
        echo join("<br>\n", $linksWithoutEventId);
        echo "<br>\n<br>\n" . '301 redirection rules for .htaccess file:' . "<br>\n";
        echo join("<br>\n", $redirect301Links);

        return $this->getResponse();
    }

    public function transactionAction()
    {
        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $insert = $sql->insert();
        $insert
            ->into('system_history')
            ->columns(
                array(
                    'event_date',
                    'message'
                )
            )
            ->values(
                array(
                    'event_date' => new \Zend\Db\Sql\Expression('NOW()'),
                    'message' => 'test'
                )
            );
        $eventId = $dbManager->executeInsertAndGetLastInsertId($insert);

        if ($eventId > 0)
        {
            $dbManager->beginTransaction();

            $update = $sql->update('system_history');
            $update
                ->set(
                    array(
                        'message' => 'test2'
                    )
                )
                ->where(
                    array(
                        'event_id' => $eventId
                    )
                );
            $result = $dbManager->executePreparedStatement($update);

            $dbManager->commit();
            //$dbManager->rollback();
        }

        //r$dbManager->commit();
        $dbManager->rollback();

        return $this->getResponse();
    }

    public function sendmailAction()
    {
        $sendmailTransport = new \Zend\Mail\Transport\Sendmail();

        try
        {
            $mail = $this->getMailManager();
            $mail
                ->setTransport($sendmailTransport)
                ->setRecipientAddress('wskorodecki@gmail.com')
                ->setSubject('test')
                //->setHtmlContents()
                ->setTextContents('test')
                ->send()
            ;
            echo 'Sent.';
        }
        catch (\Exception $e)
        {
            var_dump($e->getMessage());
        }

        return $this->getResponse();
    }

    public function sendmail2Action()
    {
        var_dump(
            mail(
                'wskorodecki@gmail.com',
                'Test mail',
                'Test mail from your server name',
                '',
                '-f system@vistula.edu.pl'
            )
        );

        return $this->getResponse();
    }

    public function sendmail3Action()
    {
        var_dump(
            mail(
                'wskorodecki@gmail.com',
                'Test mail',
                'Test mail from your server name'
            )
        );

        return $this->getResponse();
    }

    public function smtpmailAction()
    {
        $mail = $this->getMailManager();

        try
        {
            $mail
                ->setRecipientAddress('wskorodecki@gmail.com')
                ->setSubject('test')
                //->setHtmlContents()
                ->setTextContents('test')
                ->send()
            ;
        }
        catch (\Exception $e)
        {
            print_r($e->getMessage());
        }

        var_dump($mail->getTransport()->getOptions());

        return $this->getResponse();
    }

    public function escapeDiacritics($text)
    {
        $text = (string)$text;
        if ($text == '')
        {
            return '';
        }

        $fromReplace = array(
            'ą',
            'ć',
            'ę',
            'ł',
            'ń',
            'ó',
            'ś',
            'ż',
            'ź',
            'Ą',
            'Ć',
            'Ę',
            'Ł',
            'Ń',
            'Ó',
            'Ś',
            'Ż',
            'Ź'
        );
        $toReplace = array(
            'a',
            'c',
            'e',
            'l',
            'n',
            'o',
            's',
            'z',
            'z',
            'A',
            'C',
            'E',
            'L',
            'N',
            'O',
            'S',
            'Z',
            'Z'
        );

        return str_replace($fromReplace, $toReplace, $text);
    }

    public function generateCleanUrlFromText($text)
    {
        $text = trim((string)$text);
        if ($text == '')
        {
            return false;
        }

        $cleanUrl = '';

        $allowedChars = array(
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            'a',
            'b',
            'c',
            'd',
            'e',
            'f',
            'g',
            'h',
            'i',
            'j',
            'k',
            'l',
            'm',
            'n',
            'o',
            'p',
            'q',
            'r',
            's',
            't',
            'u',
            'v',
            'w',
            'x',
            'y',
            'z',
            '-',
            '!',
            '$'
        );

        $source = mb_strtolower($this->escapeDiacritics($text));
        $sourceLength = mb_strlen($source);

        for ($i = 0; $i < $sourceLength; $i++)
        {
            $character = mb_substr($source, $i, 1);

            if (in_array($character, $allowedChars))
            {
                $cleanUrl .= $character;
            }
            else
            {
                $cleanUrl .= '-';
            }
        }

        $cleanUrl = trim(trim($cleanUrl), '-');

        do
        {
            $oldCleanUrl = $cleanUrl;
            $cleanUrl = str_replace('--', '-', $oldCleanUrl);
        }
        while ($cleanUrl != $oldCleanUrl);

        return $cleanUrl;
    }
}
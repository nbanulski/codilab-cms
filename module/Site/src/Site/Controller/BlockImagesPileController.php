<?php
namespace Site\Controller;

class BlockImagesPileController extends BaseController
{
    const BLOCK_IMAGES_PILE_ITEMS_TABLE_NAME = 'block_images_pile_items';
    const BLOCK_IMAGES_PILE_GROUPS_TABLE_NAME = 'block_images_pile_groups';

    public function addEditGroupAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $formData = array();

                if ($request->isPost())
                {
                    $pageBlockId = (int)$request->getPost('pageBlockId');
                    $blockManager = $this->getBlockManager();
                    $imagesPileBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);

                    if ($imagesPileBlock instanceof \Site\Block\ImagesPile\ImagesPile)
                    {
                        if ($request->getPost('dataPosted'))
                        {
                            $formData = new \Site\Custom\FlexibleContainer($request->getPost('formData'));
                            $result = $imagesPileBlock->addOrEditGroup($formData);

                            if ($result)
                            {
                                $customStatus =
                                    ($formData->groupId > 0) ?
                                        'GROUP_MODIFIED' :
                                        'GROUP_ADDED';
                            }
                            else
                            {
                                $customStatus =
                                    ($formData->groupId > 0) ?
                                        'GROUP_MODIFYING_ERROR' :
                                        'GROUP_ADDING_ERROR';
                            }

                            $jsonResponse->meta->customStatus = $customStatus;
                            $this->response->setContent((string)$jsonResponse);

                            return $this->response;
                        }
                        else
                        {
                            $groupId = $request->getPost('groupId');
                            if ($groupId > 0)
                            {
                                if ($imagesPileBlock)
                                {
                                    $groupData = $imagesPileBlock->getGroupById($groupId);

                                    $formData = array(
                                        'groupId' => $groupId,
                                        'groupTitle' => $groupData->name,
                                        'groupIsPublished' => $groupData->is_published ? 1 : 0,
                                        'groupLastUpdateAuthorId' => $user->user_id
                                    );
                                }
                            }
                        }
                    }
                }

                $defaultFormData = array(
                    'groupId' => '',
                    'groupTitle' => '',
                    'groupIsPublished' => 0
                );
                $formData = new \Site\Custom\FlexibleContainer(array_merge($defaultFormData, $formData));

                $metadata = new \Zend\Db\Metadata\Metadata($this->dbAdapter);
                $blockImagesPileGroupsTableColumnsMetadata = $metadata->getColumns(
                    self::BLOCK_IMAGES_PILE_GROUPS_TABLE_NAME
                );
                $maxLengths = array();
                if ($blockImagesPileGroupsTableColumnsMetadata)
                {
                    foreach ($blockImagesPileGroupsTableColumnsMetadata as $column)
                    {
                        $name = $column->getName();

                        $maxLengths[$name] = $column->getCharacterMaximumLength();
                    }
                }

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $addNewGroupView = new \Zend\View\Model\ViewModel();
                $addNewGroupView->setTemplate('blocks/images-pile/add-edit-group.phtml');
                $addNewGroupView->setVariables(
                    array(
                        'maxLengths' => $maxLengths,
                        'formData' => $formData
                    )
                );

                $jsonResponse->data = $phpRenderer->render($addNewGroupView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function addEditItemAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageBlockId = (int)$request->getPost('pageBlockId');
                $blockManager = $this->getBlockManager();
                $imagesPileBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $formData = array();

                if ($imagesPileBlock instanceof \Site\Block\ImagesPile\ImagesPile)
                {
                    if ($request->isPost())
                    {
                        if ($request->getPost('dataPosted'))
                        {
                            $formData = new \Site\Custom\FlexibleContainer($request->getPost('formData'));
                            $result = $imagesPileBlock->addOrEditItem($formData);

                            if ($result)
                            {
                                $customStatus = ($formData->itemId > 0) ? 'ITEM_MODIFIED' : 'ITEM_ADDED';
                            }
                            else
                            {
                                $customStatus = ($formData->itemId > 0) ? 'ITEM_MODIFYING_ERROR' : 'ITEM_ADDING_ERROR';
                            }

                            $jsonResponse->meta->customStatus = $customStatus;
                            $this->response->setContent((string)$jsonResponse);

                            return $this->response;
                        }
                        else
                        {
                            $itemId = $request->getPost('itemId');
                            if ($itemId > 0)
                            {
                                if ($imagesPileBlock)
                                {
                                    $item = $imagesPileBlock->getItemById($itemId);
                                    $attachmentId = $item->attachment_id;
                                    $attachment = $this->getUploadManager()->getAttachmentById($attachmentId);
                                    $uniqueHashForFileName = $attachment->hash_for_filename;

                                    $formData = array(
                                        'itemId' => $itemId,
                                        'itemParentId' => $item->parent_id,
                                        'itemGroupId' => $item->group_id,
                                        'itemPageId' => $item->page_id,
                                        'itemIsPublished' => $item->is_published ? 1 : 0,
                                        'itemUrlAddressType' => (int)$item->url_address_type,
                                        'itemLastUpdateAuthorId' => $user->user_id,
                                        'itemExternalUrl' => ($item->url_address_type == 1) ? $item->external_url : '',
                                        'itemTitle' => $item->name,
                                        'itemUniqueHashForFileName' => $uniqueHashForFileName,
                                        'itemOriginalFileName' => $attachment->original_file_name
                                    );
                                }
                            }
                        }
                    }
                }

                $defaultFormData = array(
                    'itemId' => '',
                    'itemParentId' => null,
                    'itemGroupId' => null,
                    'itemPageId' => null,
                    'itemUrlAddressType' => 0,
                    'itemIsPublished' => 0,
                    'itemExternalUrl' => '',
                    'itemTextIcon' => 'asterisk',
                    'itemTitle' => '',
                    'itemImages' => array(),
                    'itemUniqueHashForFileName' => '',
                    'itemOriginalFileName' => ''
                );
                $formData = new \Site\Custom\FlexibleContainer(array_merge($defaultFormData, $formData));

                $metadata = new \Zend\Db\Metadata\Metadata($this->dbAdapter);
                $blockImagesPileItemsTableColumnsMetadata = $metadata->getColumns(
                    self::BLOCK_IMAGES_PILE_ITEMS_TABLE_NAME
                );
                $maxLengths = array();
                if ($blockImagesPileItemsTableColumnsMetadata)
                {
                    foreach ($blockImagesPileItemsTableColumnsMetadata as $column)
                    {
                        $name = $column->getName();

                        $maxLengths[$name] = $column->getCharacterMaximumLength();
                    }
                }

                $itemGroupsOptions = array();
                $groups = $this->getGroups();
                if ($groups)
                {
                    foreach ($groups as $group)
                    {
                        $itemGroupsOptions[$group->group_id] = $group->name;
                    }
                }

                $itemParentItemsOptions = array();

                $this->addEditView->setVariables(
                    array(
                        'itemParentItemsOptions' => $itemParentItemsOptions,
                        'itemGroupsOptions' => $itemGroupsOptions
                    )
                );

                $phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                $addNewItemView = new \Zend\View\Model\ViewModel();
                $addNewItemView->setTemplate('blocks/images-pile/add-edit-item.phtml');
                $addNewItemView->setVariables(
                    array(
                        'itemParentItemsOptions' => $itemParentItemsOptions,
                        'itemGroupsOptions' => $itemGroupsOptions,
                        'maxLengths' => $maxLengths,
                        'formData' => $formData
                    )
                );

                $jsonResponse->data = $phpRenderer->render($addNewItemView);
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function deleteGroupAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $deleted = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $groupId = $request->getPost('groupId');

                $imagesPileBlock = $this->getBlockManager()->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($imagesPileBlock instanceof \Site\Block\ImagesPile\ImagesPile)
                {
                    $deleted = $imagesPileBlock->deleteGroupById($groupId);
                }

                if ($deleted)
                {
                    $jsonResponse->meta->customStatus = 'GROUP_DELETED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'GROUP_DELETION_ERROR';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function deleteItemAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user)
            {
                $pageBlockId = $request->getPost('pageBlockId');
                $itemId = $request->getPost('itemId');

                $deleted = false;

                $imagesPileBlock = $this->getBlockManager()->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($imagesPileBlock instanceof \Site\Block\ImagesPile\ImagesPile)
                {
                    $deleted = $imagesPileBlock->deleteItemById($itemId);
                }

                if ($deleted)
                {
                    $jsonResponse->meta->customStatus = 'ITEM_DELETED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'ITEM_DELETION_ERROR';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getAddEditGroupViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');

                $blockManager = $this->getBlockManager();
                $addEditGroupViewHtml = $blockManager->callPageBlockCustomMethod(
                    $pageBlockId,
                    'renderAddEditGroup',
                    array($this->request, true)
                );
                if ($addEditGroupViewHtml != '')
                {
                    $jsonResponse->data = $addEditGroupViewHtml;
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate(
                            'Failed to fetch template', 'default', $this->getUserLanguage()->zend2_locale
                        ) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getGroupsManagementViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user && $user->session->designMode)
            {
                $pageBlockId = $request->getPost('pageBlockId');

                $blockManager = $this->getBlockManager();
                $groupsManagementViewHtml = $blockManager->callPageBlockCustomMethod(
                    $pageBlockId,
                    'renderGroupsManagement',
                    array($this->request, true)
                );
                if ($groupsManagementViewHtml != '')
                {
                    $jsonResponse->data = $groupsManagementViewHtml;
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage =
                        $this->translate(
                            'Failed to fetch template', 'default', $this->getUserLanguage()->zend2_locale
                        ) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function reorderGroupsAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $reorderingSuccessful = false;

                $pageBlockId = (int)$request->getPost('pageBlockId');
                $groups = $request->getPost('groups');

                $blockManager = $this->getBlockManager();
                $imagesPileBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);

                if ($imagesPileBlock instanceof \Site\Block\ImagesPile\ImagesPile)
                {
                    $reorderingSuccessful = $imagesPileBlock->reorderGroups($groups);
                }

                if ($reorderingSuccessful)
                {
                    $jsonResponse->meta->customStatus = 'ITEMS_REORDERED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'ITEMS_REORDERING_ERROR';
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to reorder groups', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function reorderItemsInGroupAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $reorderingSuccessful = false;

                $pageBlockId = (int)$request->getPost('pageBlockId');
                $groupId = $request->getPost('groupId');
                $items = $request->getPost('items');

                $blockManager = $this->getBlockManager();
                $imagesPileBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($imagesPileBlock instanceof \Site\Block\ImagesPile\ImagesPile)
                {
                    $reorderingSuccessful = $imagesPileBlock->reorderItemsInGroup($groupId, $items);
                }

                if ($reorderingSuccessful)
                {
                    $jsonResponse->meta->customStatus = 'ITEMS_REORDERED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'ITEMS_REORDERING_ERROR';
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to reorder images in the group', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function toggleGroupPublishingAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $updated = false;

                $pageBlockId = (int)$request->getPost('pageBlockId');
                $groupId = (int)$request->getPost('groupId');
                $published = (int)$request->getPost('published') ? 1 : 0;

                $blockManager = $this->getBlockManager();
                $imagesPileBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($imagesPileBlock instanceof \Site\Block\ImagesPile\ImagesPile)
                {
                    $updated = $imagesPileBlock->toggleGroupPublished($groupId, $published);
                }

                if (!$updated)
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate(
                            'Could not update group', 'default', $this->getUserLanguage()->zend2_locale
                        ) . '.';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function toggleItemPublishingAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $updated = false;

                $pageBlockId = (int)$request->getPost('pageBlockId');
                $itemId = (int)$request->getPost('itemId');
                $published = (int)$request->getPost('published') ? 1 : 0;

                $blockManager = $this->getBlockManager();
                $imagesPileBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($imagesPileBlock instanceof \Site\Block\ImagesPile\ImagesPile)
                {
                    $updated = $imagesPileBlock->toggleItemPublished($itemId, $published);
                }

                if (!$updated)
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to publish / unpublish image', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    protected function getGroupCountByContentId($contentId)
    {
        $contentId = (int)$contentId;
        if ($contentId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_IMAGES_PILE_GROUPS_TABLE_NAME)
            ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')), $prefixColumnsWithTable)
            ->where(array('content_id' => $contentId));
        $resultSet = $dbManager->getHydratedResultSet($select);
        $row = $resultSet->current();
        $count = $row ? $row->count : false;

        return $count;
    }

    protected function getGroups()
    {
        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_IMAGES_PILE_GROUPS_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable);
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    protected function createNewOrUpdateExistingGroup($pageBlockId, $formData)
    {
        $groupId = isset($formData['groupId']) ? (int)$formData['groupId'] : null;

        if ($groupId > 0)
        {
            return $this->updateExistingGroup($pageBlockId, $formData);
        }
        else
        {
            return $this->createNewGroup($pageBlockId, $formData);
        }
    }
}
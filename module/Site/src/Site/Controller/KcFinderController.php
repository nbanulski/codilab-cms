<?php
namespace Site\Controller;

class KcFinderController extends BaseController
{
    protected $imageController = null;
    protected $baseURL = "public/upload/images/";
    
    public function compressAction(){
        $request = $this->getRequest();
        $jsonResponse = $this->jsonResponse();      
        

        $imgName = $request->getPost('name');        
        $newWidth = (int)$request->getPost('width');        
        $newHeight = (int)$request->getPost('height');        
        $outputFile = $request->getPost('output');
        $outputPath = $request->getPost('path');
        
        $imgURL = 'public'.$outputPath.'/'.$imgName;
        $outputURL = 'public'.$outputPath.'/'.$outputFile;
        
        $uploadManager = $this->getUploadManager();
        $fileSystem = $uploadManager->getFileSystem();
        $this->imageController = new \Graphics\Component\ImageController($fileSystem);
        $compressFunction = $this->imageController->resizeImageToSpecificDimensions($imgURL, $outputURL, $newWidth, $newHeight);
        
        if($compressFunction){
            $jsonResponse->customResponse = $compressFunction;
            $jsonResponse->data = 1;
        }else{
            $jsonResponse->customResponse = $compressFunction;
            $jsonResponse->data = 0;
        }
        
        $this->response->setContent((string)$jsonResponse);

        return $this->response;
    }
    public function uploadAction(){
        $request = $this->getRequest();
        $jsonResponse = $this->jsonResponse();

        $moduleName = mb_strtolower(trim($request->getPost('moduleName')));
        $blockName = mb_strtolower(trim($request->getPost('blockName')));
        $pageBlockId = (int)$request->getPost('pageBlockId');

        $uploadManager = $this->getUploadManager();
        $fileSystem = $uploadManager->getFileSystem();
        $this->imageController = new \Graphics\Component\ImageController($fileSystem);

        if ($moduleName != '')
        {
            if ($blockName != '')
            {
                $uniqueFileNameHashes = $uploadManager->handleUploadForSpecificModuleAndBlock($moduleName, $blockName, $pageBlockId);

                $jsonResponse->data = $uniqueFileNameHashes;
            }
        }

        $this->response->setContent((string)$jsonResponse);

        return $this->response;
    }
}
<?php
namespace Site\Controller;

use Zend\Mvc\Controller\AbstractController;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Mvc\MvcEvent;

class LanguageManager extends AbstractController
{
    const LANGUAGES_TABLE_NAME = 'languages';

    protected $cmsConfig = null;
    protected $serviceLocator = null;
    protected $language = null;
    protected $identity = null;
    protected $databaseManager = null;
    protected $defaultLanguage = null;

    public function __construct($cmsConfig, ServiceLocatorInterface $serviceLocator, \stdClass $language, $identity)
    {
        $this->cmsConfig = $cmsConfig;
        $this->serviceLocator = $serviceLocator;
        $this->language = $language;
        $this->identity = $identity;
    }

    public function onDispatch(MvcEvent $e)
    {

    }

    public function getDatabaseManager()
    {
        return $this->databaseManager;
    }

    public function setDatabaseManager(DatabaseManager $databaseManager)
    {
        $this->databaseManager = $databaseManager;

        return $this;
    }

    public function getAllLanguages()
    {
        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();

        $select = $dbManager->getSql()->select();
        $select->from(self::LANGUAGES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable);
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getDefaultLanguage()
    {
        if ($this->defaultLanguage == null)
        {
            $prefixColumnsWithTable = false;
            $dbManager = $this->getDatabaseManager();

            $defaultLanguageIsoCode = $this->cmsConfig['default_language_iso_code'];

            $select = $dbManager->getSql()->select();
            $select
                ->from(self::LANGUAGES_TABLE_NAME)
                ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
                ->where(array('iso_code' => $defaultLanguageIsoCode))
                ->limit(1);
            $resultSet = $dbManager->getHydratedResultSet($select);

            $this->defaultLanguage = $resultSet->current();
        }

        return $this->defaultLanguage;
    }

    public function getLanguageByIsoCode($isoCode)
    {
        $isoCode = strtolower(trim((string)$isoCode));

        if ($isoCode == '')
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();

        $select = $dbManager->getSql()->select();
        $select
            ->from(self::LANGUAGES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('iso_code' => $isoCode));
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getLanguageByLanguageId($languageId)
    {
        $languageId = (int)$languageId;

        if ($languageId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();

        $select = $dbManager->getSql()->select();
        $select
            ->from(self::LANGUAGES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'language_id' => $languageId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getLanguageCount()
    {
        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();

        $select = $dbManager->getSql()->select();
        $select
            ->from(self::LANGUAGES_TABLE_NAME)
            ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')), $prefixColumnsWithTable);
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();
            if ($row)
            {
                return $row->count;
            }
        }

        return false;
    }

    public function getLanguageIsoCodeByLanguageId($languageId)
    {
        $languageId = (int)$languageId;

        if ($languageId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();

        $select = $dbManager->getSql()->select();
        $select
            ->from(self::LANGUAGES_TABLE_NAME)
            ->columns(array('iso_code'), $prefixColumnsWithTable)
            ->where(
                array(
                    'language_id' => $languageId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();
            if ($row)
            {
                return $row->iso_code;
            }
        }

        return false;
    }
} 
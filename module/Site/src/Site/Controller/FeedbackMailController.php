<?php

namespace Site\Controller;

use Zend\Mail;
use MailManager;
class FeedbackMailController extends BaseController{

    public function send(){
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $mailInfo = new \Site\Custom\FlexibleContainer($request->getPost('mailInfo'));

            $validMailInfo = isValidMailInfo($mailInfo);

            if($validMailInfo == false){
                return false;
            }

            $mail = new Mail\Message();
            $mail->setBody($validMailInfo->body);
            $mail->setFrom($validMailInfo->from, 'Vistula CMS Feedback Form');
            $mail->addTo('ahmad.milazi@proimagine.pl', 'Ahmad Milazi');
            $mail->setSubject($validMailInfo->subject);

            $transport = new Mail\Transport\Sendmail();
            $transport->send($mail);

            /*
            $mailManager = $this->getMailManager();
            $mailManager->setRecipientAddress('ahmad.milazi@proimagine.pl')
                ->setSubject($validMailInfo->subject)
                ->setTextContent($validMailInfo->body);

            $mailManager->send();
            */
            if($transport)
            {
                $jsonResponse->meta->customStatus = 'MAIL_SENT';
            }
            else
            {
                $jsonResponse->meta->customStatus = 'MAIL_NOT_SENT';
            }
            $jsonResponse = $this->jsonResponse();
            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }
    public function isValidMailInfo($mailInfo){
        $mailInfo = new \Site\Custom\FlexibleContainer($mailInfo);
        $validInfo = new \Site\Custom\FlexibleContainer();

        //Validate Mail Information

        $validInfo->from = $mailInfo->from;
        if (!filter_var($validInfo->from, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        $validInfo->subject = strip_tags((string)$mailInfo->subject);

        $validInfo->body = strip_tags((string)$mailInfo->body);


        return $mailInfo;
    }

} 
<?php
namespace Site\Controller;

class BlockNewsDetailsController extends BaseController
{
    public function reorderNewsImagesAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $newsId = $request->getPost('newsId');
                $contentId = $request->getPost('contentId');
                $images = $request->getPost('images');

                $reorderingSuccessful = false;

                $blockManager = $this->getBlockManager();
                $newsListBlock = $blockManager->createBlockUsingContentId($contentId);
                if ($newsListBlock instanceof \Site\Block\NewsList\NewsList)
                {
                    $reorderingSuccessful = $newsListBlock->reorderNewsImages($newsId, $images);
                }

                if ($reorderingSuccessful)
                {
                    $jsonResponse->meta->customStatus = 'IMAGES_REORDERED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'IMAGES_REORDERING_ERROR';
                    $jsonResponse->meta->requestErrorMessage =
                        $this->translate(
                            'Failed to reorder news images', 'default',
                            $this->getUserLanguage()->zend2_locale
                        ) . '.'
                    ;
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateNewsTitleAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $updated = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $newsId = $request->getPost('newsId');
                $newsTitle = $request->getPost('newsTitle');
                $currentUrl = $request->getPost('currentUrl');

                $blockManager = $this->getBlockManager();
                $newsDetailsBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($newsDetailsBlock instanceof \Site\Block\NewsDetails\NewsDetails)
                {
                    $updated = $newsDetailsBlock->updateNewsTitle($newsId, $newsTitle, $currentUrl);
                }

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage =
                        $this->translate(
                            'Failed to update news title', 'default',
                            $this->getUserLanguage()->zend2_locale
                        ) . '.'
                    ;
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateNewsGalleryImageTitleAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $updated = false;

                $contentId = $request->getPost('contentId');
                $newsId = $request->getPost('newsId');
                $imageId = $request->getPost('imageId');
                $imageTitle = $request->getPost('imageTitle');

                $blockManager = $this->getBlockManager();
                $newsListBlock = $blockManager->createBlockUsingContentId($contentId);
                if ($newsListBlock instanceof \Site\Block\NewsList\NewsList)
                {
                    $updated = $newsListBlock->updateExistingGalleryImageTitleByNewsIdAndImageId(
                        $newsId, $imageId, $imageTitle
                    );
                }

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage =
                        $this->translate(
                            'Failed to update news image title', 'default',
                            $this->getUserLanguage()->zend2_locale
                        ) . '.'
                    ;
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateNewsContentsAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $updated = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $newsId = $request->getPost('newsId');
                $newsContents = $request->getPost('newsContents');
                $currentUrl = $request->getPost('currentUrl');

                $blockManager = $this->getBlockManager();
                $newsDetailsBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($newsDetailsBlock instanceof \Site\Block\NewsDetails\NewsDetails)
                {
                    $updated = $newsDetailsBlock->updateNewsContents($newsId, $newsContents, $currentUrl);
                }

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage =
                        $this->translate(
                            'Failed to update news contents', 'default',
                            $this->getUserLanguage()->zend2_locale
                        ) . '.'
                    ;
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }
} 
<?php
namespace Site\Controller;

class BlockPostgraduateStudiesController extends BaseController
{
    public function reorderTabsAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $reorderingSuccessful = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $tabs = $request->getPost('tabs');

                $blockManager = $this->getBlockManager();
                $postgraduateStudiesBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($postgraduateStudiesBlock instanceof \Site\Block\PostgraduateStudies\PostgraduateStudies)
                {
                    $reorderingSuccessful = $postgraduateStudiesBlock->reorderTabs($tabs);
                }

                if ($reorderingSuccessful)
                {
                    $jsonResponse->meta->customStatus = 'TABS_REORDERED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'TABS_REORDERING_ERROR';
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to reorder tabs', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateBasicInfoAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $updated = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $infoType = $request->getPost('infoType');
                $contents = $request->getPost('contents');

                $blockManager = $this->getBlockManager();
                $postgraduateStudiesBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($postgraduateStudiesBlock instanceof \Site\Block\PostgraduateStudies\PostgraduateStudies)
                {
                    $updated = $postgraduateStudiesBlock->updateBasicInfo($infoType, $contents);
                }

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'TAB_CONTENTS_UPDATED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage =
                        $this->translate('Failed to update basic information', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateTabContentsAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $updated = false;

                $pageBlockId = $request->getPost('pageBlockId');
                $tabId = $request->getPost('tabId');
                $tabContents = $request->getPost('tabContents');

                $blockManager = $this->getBlockManager();
                $postgraduateStudiesBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($postgraduateStudiesBlock instanceof \Site\Block\PostgraduateStudies\PostgraduateStudies)
                {
                    $updated = $postgraduateStudiesBlock->updateTabContents($tabId, $tabContents);
                }

                if ($updated)
                {
                    $jsonResponse->meta->customStatus = 'TAB_CONTENTS_UPDATED';
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to update tab contents', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }
}
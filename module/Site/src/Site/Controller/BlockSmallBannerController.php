<?php
namespace Site\Controller;

use Zend\View\Model\ViewModel;

class BlockSmallBannerController extends BaseController
{
    public function deleteBannerAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) // Check if this is an AJAX request.
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->identity();

            if ($user && $user->session->designMode)
            {
                $tabId = $request->getPost('bannerId');   
                $pageBlockId = $request->getPost('pageBlockId');
                
                $blockManager = $this->getBlockManager();
                $bannerSmallBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                
                $bannerDeleteStatus = $bannerSmallBlock->deleteBannerById($tabId);                
            }

            if ($bannerDeleteStatus)
            {
                $jsonResponse->meta->customStatus = 'BANNER_DELETED';
            }
            else
            {
                $jsonResponse->meta->customStatus = 'FAILED_BANNER_DELETE';
                $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to delete banner', 'default', $this->getUserLanguage()->zend2_locale) . '.';
            }
            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function createBannerAction()
    {
        $request = $this->getRequest();

        if ($request->isXmlHttpRequest()) // Check if this is an AJAX request.
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->identity();

            if ($user && $user->session->designMode)
            {
                $objectsFromPost = $request->getPost('formData');
                $pageBlockId = $request->getPost('pageBlockId');
                $banner = new \Site\Custom\FlexibleContainer($objectsFromPost);     
                $blockManager = $this->getBlockManager();
                $bannerSmallBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                
                $result = $bannerSmallBlock->createBanner($banner);

                if ($result>0)
                {
                    $jsonResponse->data = $result;
                    $jsonResponse->meta->customStatus = 'BANNER_SAVED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'BANNER_NOT_SAVED';
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to create banner', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function reorderBannersAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $reorderingSuccessful = false;

                $pageBlockId = (int)$request->getPost('pageBlockId');
                $banners = $request->getPost('banners');

                $blockManager = $this->getBlockManager();
                $smallBannerBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);

                if ($smallBannerBlock instanceof \Site\Block\SmallBanner\SmallBanner)
                {
                    $reorderingSuccessful = $smallBannerBlock->reorderBanners($banners);
                }

                if ($reorderingSuccessful)
                {
                    $jsonResponse->meta->customStatus = 'BANNERS_REORDERED';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'BANNERS_REORDERING_ERROR';
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to reorder banners', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateBannerAction()
    {
        $request = $this->getRequest();

        if ($request->isXmlHttpRequest()) // Check if this is an AJAX request.
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->identity();

            if ($user && $user->session->designMode)
            {
                $objectsFromPost = $request->getPost('formData');
                $pageBlockId = $request->getPost('pageBlockId');
                $banner = new \Site\Custom\FlexibleContainer($objectsFromPost);
                $blockManager = $this->getBlockManager();
                $bannerSmallBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);     
                
                $result = $bannerSmallBlock->updateBanner($banner);

                if ($result)
                {
                    $jsonResponse->data = $result;
                    $jsonResponse->meta->customStatus = 'BANNER_UPDATED';
                }
                else
                {               
                    $jsonResponse->meta->customStatus = 'BANNER_NOT_UPDATED';
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to update banner', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function updateBannerTextAction()
    {
        $request = $this->getRequest();

        if ($request->isXmlHttpRequest()) // Check if this is an AJAX request.
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->identity();

            if ($user && $user->session->designMode)
            {
                $objectsFromPost = $request->getPost('banner');
                $pageBlockId = $request->getPost('pageBlockId');
                $banner = new \Site\Custom\FlexibleContainer($objectsFromPost);
                $blockManager = $this->getBlockManager();
                $bannerSmallBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);     
                
                $result = $bannerSmallBlock->updateBannerText($banner);

                if ($result)
                {
                    $jsonResponse->data = $result;
                    $jsonResponse->meta->customStatus = 'BANNER_UPDATED';
                }
                else
                {               
                    $jsonResponse->meta->customStatus = 'BANNER_NOT_UPDATED';
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to update banner contents', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
            }
            $this->response->setContent((string)$jsonResponse);
        }
        return $this->response;
    }

    public function toggleBannerPublishingAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $user = $this->sessionData();
            if ($user && $user->session->designMode)
            {
                $pageBlockId = (int)$request->getPost('pageBlockId');
                $bannerId = (int)$request->getPost('bannerId');
                $published = (int)$request->getPost('published') ? 1 : 0;

                $blockManager = $this->getBlockManager();
                $bannerSmallBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                $updated = $bannerSmallBlock->toggleBannerPublished($bannerId, $published);
                if (!$updated)
                {
                    $jsonResponse->meta->requestErrorMessage = $this->translate('Failed to publish / unpublish banner', 'default', $this->getUserLanguage()->zend2_locale) . '.';
                }
                else
                {
                    $jsonResponse->meta->customStatus = 'UPDATED';
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }
}
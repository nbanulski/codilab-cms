<?php
namespace Site\Controller;

use Zend\View\Model\ViewModel;

class BlockPopupController extends BaseController
{
    public function getTitleAndContentsViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();

            $pageBlockId = $request->getPost('pageBlockId');

            $title = false;
            $contentsViewHtml = false;

            $blockManager = $this->getBlockManager();
            $popupBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
            if ($popupBlock instanceof \Site\Block\Popup\Popup)
            {
                $title = $popupBlock->getTitle();
                $contentsViewHtml = $popupBlock->renderContentsView($this->request);
            }

            if ($contentsViewHtml != '')
            {
                $jsonResponse->data = new \stdClass();
                $jsonResponse->data->title = $title;
                $jsonResponse->data->contents = $contentsViewHtml;
            }
            else
            {
                $jsonResponse->meta->requestErrorMessage =
                    $this->translate(
                        'Failed to fetch template', 'default', $this->getUserLanguage()->zend2_locale
                    ) . '.'
                ;
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function getEditViewAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user)
            {
                $pageBlockId = $request->getPost('pageBlockId');

                $editContentsViewHtml = false;

                $blockManager = $this->getBlockManager();
                $popupBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);
                if ($popupBlock instanceof \Site\Block\Popup\Popup)
                {
                    $editContentsViewHtml = $popupBlock->renderEditView($this->request);
                }

                if ($editContentsViewHtml != '')
                {
                    $jsonResponse->data = new \stdClass();
                    $jsonResponse->data->title =
                        $this->translate(
                            'Edit popup contents', 'default', $this->getUserLanguage()->zend2_locale
                        );
                    $jsonResponse->data->contents = $editContentsViewHtml;
                }
                else
                {
                    $jsonResponse->meta->requestErrorMessage =
                        $this->translate(
                            'Failed to fetch template', 'default', $this->getUserLanguage()->zend2_locale
                        ) . '.'
                    ;
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }

    public function saveContentsAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest())
        {
            $jsonResponse = $this->jsonResponse();
            $user = $this->sessionData();

            if ($user)
            {
                $pageBlockId = $request->getPost('pageBlockId');

                $blockManager = $this->getBlockManager();
                $popupBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($pageBlockId);

                if (
                    $request->isPost() &&
                    ($popupBlock instanceof \Site\Block\Popup\Popup)
                )
                {
                    $formData = new \Site\Custom\FlexibleContainer($request->getPost('formData'));
                    $result = $popupBlock->saveContents($formData);
                    $customStatus = $result ? 'CONTENTS_UPDATED' : 'CONTENTS_SAVING_ERROR';

                    $jsonResponse->meta->customStatus = $customStatus;
                    $this->response->setContent((string)$jsonResponse);

                    return $this->response;
                }
            }

            $this->response->setContent((string)$jsonResponse);
        }

        return $this->response;
    }
}
<?php
namespace Site\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Http\Request;

class BaseController extends AbstractActionController implements EventManagerAwareInterface
{
    public $cmsConfig = null;
    public $serviceLocator = null;
    public $dbAdapter = null;
    public $sql = null;
    public $language = null;
    protected $userLanguage = null;
    protected $attachmentManager = null;
    protected $databaseManager = null;
    protected $blockManager = null;
    protected $fileSystem = null;
    protected $imageManipulator = null;
    protected $languageManager = null;
    protected $mailManager = null;
    protected $pageManager = null;
    protected $systemSettings = null;
    protected $uploadManager = null;
    protected $userManager = null;
    protected $routeParameters = null;
    protected $parametersForCurrentPage = null;

    public function setEventManager(EventManagerInterface $events)
    {
        parent::setEventManager($events);

        //$controller = $this;
    }

    public function getDbAdapter()
    {
        if (!$this->dbAdapter)
        {
            $serviceLocator = $this->getServiceLocator();
            $this->dbAdapter = $serviceLocator->get('Zend\Db\Adapter\Adapter');
        }

        return $this->dbAdapter;
    }

    public function getRefererFromRequest(Request $request)
    {
        return $request->getHeader('Referer');
    }

    public function getRefererPathFromRequest(Request $request)
    {
        $refererObject = $this->getRefererFromRequest($request);
        if ($refererObject)
        {
            return $refererObject->uri()->getPath();
        }

        return '';
    }

    protected function addEventToSystemHistory($message, $variables = false)
    {
        $message = trim((string)$message);

        if ($message == '')
        {
            return false;
        }

        $sessionData = $this->sessionData();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $message = preg_replace_callback(
            '|{{user_login}}|',
            function ($matches) use ($sessionData) {
                if ($sessionData && ($sessionData->offsetExists('user_id')))
                {
                    if ($sessionData->user_id > 0)
                    {
                        return '<strong>' . $sessionData->login . '</strong>';
                    }
                    else
                    {
                        return '<em>guest</em>';
                    }
                }

                return 'unknown user';
            },
            $message
        );

        if ($variables && (is_array($variables) || ($variables instanceof \Traversable)))
        {
            $message = preg_replace_callback(
                '|{{([a-zA-Z0-9_]+)}}|',
                function ($matches) use (&$variables) {
                    $variableName = $matches[1];
                    if (array_key_exists($variableName, $variables))
                    {
                        return $variables[$variableName];
                    }

                    return $matches;
                },
                $message
            );
        }

        $event_date = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager->beginTransaction();

        $insert = $sql->insert('system_history');
        $insert
            ->columns(
                array(
                    'event_date',
                    'message'
                )
            )
            ->values(
                array(
                    'event_date' => $event_date,
                    'message' => $message
                )
            );
        $eventId = $dbManager->executeInsertAndGetLastInsertId($insert);
        if ($eventId > 0)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $eventId;
    }

    protected function getAttachmentManager()
    {
        if ($this->attachmentManager == null)
        {
            $fileSystem = new \FileSystem\Component\FileSystem();
            $this->attachmentManager = new AttachmentManager(
                $this->cmsConfig, $this->serviceLocator, $this->sql, $this->getRequest(),
                $this->language, $this->getUserLanguage(), $this->sessionData(), $fileSystem
            );
            $this->attachmentManager->setServiceLocator($this->serviceLocator);
        }

        return $this->attachmentManager;
    }

    protected function getBlockManager()
    {
        if ($this->blockManager == null)
        {
            $this->blockManager = new BlockManager(
                $this->cmsConfig, $this->serviceLocator, $this->sql, $this->getRequest(),
                $this->language, $this->getUserLanguage(), $this->sessionData()
            );
            $this->blockManager->setServiceLocator($this->serviceLocator);
        }

        if ($this->pageManager != null)
        {
            $this->pageManager->setBlockManager($this->blockManager);
        }

        return $this->blockManager;
    }

    protected function getCurrentRefererPath()
    {
        return $this->getRefererPathFromRequest($this->getRequest());
    }

    protected function getDatabaseManager()
    {
        if ($this->databaseManager == null)
        {
            $this->databaseManager = $this->serviceLocator->get('DatabaseManager');
        }

        return $this->databaseManager;
    }

    protected function getFileSystem()
    {
        if ($this->fileSystem == null)
        {
            $this->fileSystem = new \FileSystem\Component\FileSystem();
        }

        return $this->fileSystem;
    }

    protected function getImageManipulator()
    {
        if ($this->imageManipulator == null)
        {
            $fileSystem = $this->getFileSystem();
            $this->imageManipulator = new \Graphics\Component\ImageController($fileSystem);
        }

        return $this->imageManipulator;
    }

    protected function getLanguageManager()
    {
        if ($this->languageManager == null)
        {
            $this->languageManager = new LanguageManager($this->cmsConfig, $this->serviceLocator, $this->language, $this->sessionData());
            $this->languageManager->setServiceLocator($this->serviceLocator);
            $this->languageManager->setDatabaseManager($this->getDatabaseManager());
        }

        return $this->languageManager;
    }

    protected function getMailManager()
    {
        if ($this->mailManager == null)
        {
            $this->mailManager = new MailManager(
                $this->cmsConfig, $this->serviceLocator, $this->getUserLanguage(), $this->language, $this->sessionData()
            );
            $this->mailManager->setServiceLocator($this->serviceLocator);
        }

        return $this->mailManager;
    }

    protected function getPageManager()
    {
        if ($this->pageManager == null)
        {
            $this->pageManager = new PageManager(
                $this->cmsConfig, $this->serviceLocator,
                $this->language, $this->getUserLanguage(), $this->sessionData()
            );
            $this->pageManager->setServiceLocator($this->serviceLocator);
            $this->pageManager->setDatabaseManager($this->getDatabaseManager());
        }

        if ($this->blockManager != null)
        {
            $this->pageManager->setBlockManager($this->blockManager);
        }

        return $this->pageManager;
    }

    protected function getSystemSettings($cached = true)
    {
        if (($this->systemSettings === null) || !$cached)
        {
            $prefixColumnsWithTable = false;

            $dbManager = $this->getDatabaseManager();
            $sql = $dbManager->getSql();

            $select = $sql->select();
            $select
                ->from('settings')
                ->columns(array($select::SQL_STAR), $prefixColumnsWithTable);
            $resultSet = $dbManager->getHydratedResultSet($select);
            if ($resultSet)
            {
                $this->systemSettings = array();

                foreach ($resultSet as $setting)
                {
                    $name = $setting->name;
                    unset($setting->name);

                    $this->systemSettings[$name] = $setting;
                }

                $this->systemSettings = new \Site\Custom\FlexibleContainer($this->systemSettings);
            }
        }

        return $this->systemSettings;
    }

    protected function getSystemSettingByName($name, $cached = true)
    {
        $name = trim((string)$name);
        if ($name == '')
        {
            return false;
        }

        $this->getSystemSettings($cached);

        if (($this->systemSettings != null) && ($this->systemSettings->offsetExists($name)))
        {
            return $this->systemSettings[$name];
        }

        return false;
    }

    protected function getSystemSettingValueByName($name, $cached = true)
    {
        $setting = $this->getSystemSettingByName($name, $cached);
        if ($setting)
        {
            return trim((string)$setting->value);
        }

        return false;
    }

    protected function getUploadManager()
    {
        if ($this->uploadManager == null)
        {
            $fileSystem = new \FileSystem\Component\FileSystem();
            $this->uploadManager = new UploadManager(
                $this->cmsConfig, $this->serviceLocator, $this->sql, $this->getRequest(),
                $this->language, $this->getUserLanguage(), $this->sessionData(), $fileSystem
            );
            $this->uploadManager->setServiceLocator($this->serviceLocator);
        }

        return $this->uploadManager;
    }

    protected function getUserLanguage()
    {
        if ($this->userLanguage == null)
        {
            $sessionData = $this->sessionData();

            $this->userLanguage = $sessionData->session->userLanguage;
            if (!$this->userLanguage)
            {
                $this->userLanguage = $this->getLanguageManager()->getDefaultLanguage();
            }
        }

        return new \Site\Custom\FlexibleContainer($this->userLanguage);
    }

    protected function getUserManager()
    {
        if ($this->userManager == null)
        {
            $this->userManager = new UserManager(
                $this->cmsConfig, $this->serviceLocator, $this->sql, $this->getRequest(),
                $this->language, $this->getUserLanguage(), $this->sessionData()
            );
            $this->userManager->setServiceLocator($this->serviceLocator);
        }

        return $this->userManager;
    }
    
    protected function getSubPageCleanUrlFromRequest(Request $request)
    {
        return $this->getRouteParametersFromRequest($request)->subPage;
    }

    public function getParametersForCurrentPageFromRequest(Request $request)
    {
        if ($this->parametersForCurrentPage === null)
        {
            $this->parametersForCurrentPage = new \Site\Custom\FlexibleContainer();

            $parametersString = $this->getRouteParametersFromRequest($request)->parameters;
            $parametersParts = explode(',', $parametersString);
            $parameterPartCount = count($parametersParts);
            if ($parameterPartCount > 0)
            {
                $parameterName = null;
                for ($i = 0; $i < $parameterPartCount; $i++)
                {
                    if ($i % 2 == 0)
                    {
                        $parameterName = $parametersParts[$i];
                    }
                    else
                    {
                        $parameterValue = $parametersParts[$i];
                        $this->parametersForCurrentPage[$parameterName] = $parameterValue;
                    }
                }

                if ($i % 2 != 0)
                {
                    // If the number of parameters is odd, it means that the last parameter has no value, so we
                    // must add empty string as it's value.

                    $this->parametersForCurrentPage[$parameterName] = '';
                }
            }
        }

        return $this->parametersForCurrentPage;
    }

    protected function getRouteParametersFromRequest(Request $request)
    {
        if ($this->routeParameters === null)
        {
            $router = $this->serviceLocator->get('Router');
            $routeMatch = $router->match($request);
            $this->routeParameters = new \Site\Custom\FlexibleContainer($routeMatch->getParams());
        }

        return $this->routeParameters;
    }
}
<?php
namespace Site\Model;

class Block
{
    public $blockId;
    public $isSystem;
    public $isEnabled;
    public $isDynamic;
    public $order;
    public $lastUpdateAuthorId;
    public $lastUpdateDate;
    public $phpClassName;
    public $name;
    public $config;

    public function exchangeArray($data)
    {
        $this->blockId = isset($data['blockId']) ? $data['blockId'] : 0;
        $this->isSystem = isset($data['isSystem']) ? $data['isSystem'] : 0;
        $this->isEnabled = isset($data['isEnabled']) ? $data['isEnabled'] : 0;
        $this->isDynamic = isset($data['isDynamic']) ? $data['isDynamic'] : 0;
        $this->order = isset($data['order']) ? $data['order'] : 0;
        $this->lastUpdateAuthorId = isset($data['lastUpdateAuthorId']) ? $data['lastUpdateAuthorId'] : 0;
        $this->lastUpdateDate = isset($data['lastUpdateDate']) ? $data['lastUpdateDate'] : null;
        $this->phpClassName = isset($data['phpClassName']) ? $data['phpClassName'] : '';
        $this->name = isset($data['name']) ? $data['name'] : '';
        $this->config = isset($data['config']) ? $data['config'] : '';
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}
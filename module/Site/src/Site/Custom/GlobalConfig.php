<?php
namespace Site\Custom;

class GlobalConfig
{
    protected $container = null;

    public function __construct($config)
    {
        $this->container = new FlexibleContainer($config);
    }

    public function __get($name)
    {
        return $this->container->$name;
    }
}
<?php
namespace Site\Custom;

use \stdClass;
use \ArrayAccess;
use \Iterator;

class FlexibleContainer extends stdClass implements ArrayAccess, Iterator
{
    private $contents = array();

    public function __construct($array = null)
    {
        $this->processRecursively($this, $array);
    }

    private function processRecursively($container, $array)
    {
        if ($array && (is_array($array) || ($array instanceof \Traversable) || ($array instanceof \stdClass)))
        {
            foreach ($array as $key => $value)
            {
                if ($value && (is_array($value) || ($value instanceof \Traversable)))
                {
                    $newContainer = new FlexibleContainer();
                    $container->offsetSet($key, $newContainer);
                    $this->processRecursively($newContainer, $value);
                }
                else
                {
                    $container->offsetSet($key, $value);
                }
            }
        }
    }

    public function __get($name)
    {
        return $this->offsetExists($name) ? $this->contents[$name] : null;
    }

    public function __set($name, $value)
    {
        $this->contents[$name] = $value;
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset))
        {
            $this->contents[] = $value;
        }
        else
        {
            $this->contents[$offset] = $value;
        }
    }

    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->contents);
    }

    public function offsetUnset($offset)
    {
        unset($this->contents[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->offsetExists($offset) ? $this->contents[$offset] : null;
    }

    public function current()
    {
        return current($this->contents);
    }

    public function key()
    {
        return key($this->contents);
    }

    public function next()
    {
        return next($this->contents);
    }

    public function rewind()
    {
        return reset($this->contents);
    }

    public function valid()
    {
        return key($this->contents) !== null;
    }

    public function toArray($recursively = false)
    {
        $array = array();

        if ($recursively)
        {
            $array = $this->convertToArrayRecursively($this->contents);
        }
        else
        {
            if ($this->contents)
            {
                foreach ($this->contents as $key => $value)
                {
                    $array[$key] = $value;
                }
            }
        }

        return $array;
    }

    public function toStdClass()
    {
        $stdClass = new \stdClass();

        if ($this->contents === null)
        {
            return $stdClass;
        }

        foreach ($this->contents as $property => $value)
        {
            $stdClass->$property = $value;
        }

        return $stdClass;
    }

    public function count()
    {
        return count($this->contents);
    }

    public function merge($arrayOrObject)
    {
        $currentArray = $this->toArray(true);

        if ($arrayOrObject instanceof FlexibleContainer)
        {
            $newArray = $arrayOrObject->toArray(true);
        }
        else
        {
            $newArray = $this->convertToArrayRecursively($arrayOrObject);
        }

        $mergedArray = $this->array_merge_recursive_distinct($currentArray, $newArray);

        $this->contents = array();
        $this->processRecursively($this, $mergedArray);

        return $this;
    }

    private function convertToArrayRecursively($contents)
    {
        $array = array();

        if ($contents)
        {
            foreach ($contents as $key => $value)
            {
                if ($value && (is_array($value) || ($value instanceof \Traversable)))
                {
                    $array[$key] = $this->convertToArrayRecursively($value);
                }
                else
                {
                    $array[$key] = $value;
                }
            }
        }

        return $array;
    }

    /**
     * array_merge_recursive does indeed merge arrays, but it converts values with duplicate
     * keys to arrays rather than overwriting the value in the first array with the duplicate
     * value in the second array, as array_merge does. I.e., with array_merge_recursive,
     * this happens (documented behavior):
     *
     * array_merge_recursive(array('key' => 'org value'), array('key' => 'new value'));
     *     => array('key' => array('org value', 'new value'));
     *
     * array_merge_recursive_distinct does not change the datatypes of the values in the arrays.
     * Matching keys' values in the second array overwrite those in the first array, as is the
     * case with array_merge, i.e.:
     *
     * array_merge_recursive_distinct(array('key' => 'org value'), array('key' => 'new value'));
     *     => array('key' => 'new value');
     *
     * Parameters are passed by reference, though only for performance reasons. They're not
     * altered by this function.
     *
     * @param array $array1
     * @param mixed $array2
     * @author daniel@danielsmedegaardbuus.dk
     * @return array
     */
    private function &array_merge_recursive_distinct(array &$array1, &$array2 = null)
    {
        $merged = $array1;

        if (is_array($array2))
        {
            foreach ($array2 as $key => $val)
            {
                if (is_array($array2[$key]))
                {
                    $merged[$key] =
                        is_array($merged[$key])
                            ? $this->array_merge_recursive_distinct($merged[$key], $array2[$key])
                            : $array2[$key];
                }
                else
                {
                    $merged[$key] = $val;
                }
            }
        }

        return $merged;
    }
}
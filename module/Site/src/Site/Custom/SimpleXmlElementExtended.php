<?php
namespace Site\Custom;

use \SimpleXMLElement;

class SimpleXmlElementExtended extends SimpleXMLElement
{
    public function addCData($cdataText)
    {
        $node = dom_import_simplexml($this);
        $ownerDocument = $node->ownerDocument;
        $node->appendChild($ownerDocument->createCDATASection($cdataText));
    }
}
<?php
namespace Site\Custom;

use \ArrayAccess;

class BlockSettingsContainer implements ArrayAccess
{
    private $settings = array();

    public function offsetSet($offset, $value)
    {
        if (is_null($offset))
        {
            $this->settings[] = $value;
        }
        else
        {
            $this->settings[$offset] = $value;
        }
    }

    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->settings);
    }

    public function offsetUnset($offset)
    {
        unset($this->settings[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->offsetExists($offset) ? $this->settings[$offset] : null;
    }
}

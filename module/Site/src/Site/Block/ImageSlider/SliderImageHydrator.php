<?php
namespace Site\Block\ImageSlider;

use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\I18n\View\Helper\Translate;
use Site\Controller\AttachmentManager;
use Site\Controller\PageManager;

class SliderImageHydrator implements HydratorInterface
{
    protected $translateHelper = null;
    protected $attachmentManager = null;
    protected $imageSlider = null;
    protected $pageManager = null;

    public function __construct(
        AttachmentManager $attachmentManager,
        ImageSlider $imageSlider,
        PageManager $pageManager,
        Translate $translateHelper
    ) {
        $this->attachmentManager = $attachmentManager;
        $this->imageSlider = $imageSlider;
        $this->pageManager = $pageManager;
        $this->translateHelper = $translateHelper;
    }

    public function extract($object)
    {
    }

    public function hydrate(array $data, $object)
    {
        foreach ($data as $property => $value)
        {
            $object[$property] = $value;
        }

        $imageSlider = $this->imageSlider;

        if ($object['url_address_type'] == $imageSlider::IMAGE_URL_ADDRESS_TYPE_INTERNAL)
        {
            $object['pageUrl'] = $this->pageManager->getFullPageCleanUrlByPageId($object['page_id']);
        }
        else
        {
            $object['pageUrl'] = $object['external_url'];
        }

        $object['pageBreadcrumb'] = $this->pageManager->getPageTextBreadcrumbByPageId($object['page_id']);

        $object['thumbUrl'] = '/img/no-image.png';
        if ($object['attachment_id'] > 0)
        {
            $attachment = $this->attachmentManager->getAttachmentById($object['attachment_id']);
            if ($attachment)
            {
                $object['thumbUrl'] =
                    '/eng/get-file/index,' .
                    $attachment->hash_for_file_name .
                    ',' .
                    $attachment->original_file_name;
            }
        }

        return $object;
    }
} 
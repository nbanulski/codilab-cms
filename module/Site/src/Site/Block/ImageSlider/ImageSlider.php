<?php
namespace Site\Block\ImageSlider;

use Site\Block\AbstractBlock;
use Zend\Http\Request;

class ImageSlider extends AbstractBlock
{
    const IMAGE_URL_ADDRESS_TYPE_INTERNAL = 0;
    const IMAGE_URL_ADDRESS_TYPE_EXTERNAL = 1;
    const IMAGE_URL_ADDRESS_TYPE_NONE = 2;

    const BLOCK_IMAGE_SLIDER_TABLE_NAME = 'block_image_slider';

    protected $hasOwnSettings = true;
    protected $hasManagementWindow = true;

    /**
     * Events.
     */

    public function onAfterInserting($slotId)
    {
        return true;
    }

    public function onBeforeDeleted()
    {
        if (!$this->blockData->is_dynamic)
        {
            $this->deleteAllImages();
        }
    }

    /**
     * Render-functions.
     */

    public function childRender(\Zend\Http\Request $request)
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();

        $select = $sql->select();
        $select
            ->from(array('bis' => self::BLOCK_IMAGE_SLIDER_TABLE_NAME))
            ->join(
                array('u' => 'users'),
                'u.user_id = bis.last_update_author_id',
                array('last_update_author' => 'login'),
                $select::JOIN_LEFT
            )
            ->where(
                array(
                    'bis.content_id' => $contentId,
                    'bis.is_published' => 1
                )
            )
            ->order('bis.order ASC, bis.title ASC');
        $result = $dbManager->executePreparedStatement($select);

        $resultSet = new \Zend\Db\ResultSet\HydratingResultSet(
            new SliderImageHydrator(
                $this->attachmentManager, $this, $this->getPageManager(), $this->getTranslateHelper()
            ),
            new \Site\Custom\FlexibleContainer()
        );
        $resultSet->initialize($result)->buffer();

        $this->blockView->setVariables(
            array(
                'images' => $resultSet
            )
        );
    }

    public function childRenderManagement(\Zend\Http\Request $request)
    {
        if (!$this->managementView instanceof \Zend\View\Model\ViewModel)
        {
            return false;
        }

        $dbManager = $this->getDatabaseManager();
        $dbAdapter = $dbManager->getAdapter();
        $sql = $dbManager->getSql();

        $pageNumber = $this->managementView->pageNumber;
        $itemCountPerPage = $this->managementView->itemCountPerPage;
        $contentId = $this->getContentId();

        $select = $sql->select();
        $select->from(array('bis' => self::BLOCK_IMAGE_SLIDER_TABLE_NAME))
        ->join(
            array('u' => 'users'),
            'u.user_id = bis.last_update_author_id',
            array('last_update_author' => 'login'),
            $select::JOIN_LEFT
        )
        ->where(array('bis.content_id' => $contentId))
        ->order('bis.order ASC, bis.title ASC');

        $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        $paginatorAdapter = new \Zend\Paginator\Adapter\DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new \Zend\Paginator\Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($pageNumber);
        $paginator->setItemCountPerPage($itemCountPerPage);

        $this->managementView->setVariables(
            array(
                'imagesPaginator' => $paginator
            )
        );
    }

    public function childRenderAddEdit(\Zend\Http\Request $request)
    {
        if (!$this->addEditView instanceof \Zend\View\Model\ViewModel)
        {
            return false;
        }

        $pageManager = $this->getPageManager();

        $defaultFormData = array(
            'imageId' => null,
            'imageContentId' => null,
            'imageAttachmentId' => null,
            'imageOrder' => 0,
            'imageIsPublished' => false,
            'imageUrlAddressType' => 0,
            'imageExternalUrl' => '',
            'imageTitle' => '',
            'imageImages' => array(
                0 => array(
                    'imageId' => null,
                    'isMain' => true,
                    'attachmentId' => null,
                    'uniqueHashForFileName' => '',
                    'originalFileName' => '',
                )
            ),
            'imageUniqueHashForFileName' => '',
            'imageOriginalFileName' => ''
        );
        $currentImageFormData = array();

        $imageId = (int)$request->getPost('imageId');
        if ($imageId > 0)
        {
            $image = $this->getImageById($imageId);
            if ($image)
            {
                $imageImagesArray = array(
                    0 => array(
                        'imageId' => null,
                        'isMain' => true,
                        'attachmentId' => null,
                        'uniqueHashForFileName' => '',
                        'originalFileName' => '',
                    )
                );

                $attachmentId = $image->attachment_id;
                $attachment = $this->attachmentManager->getAttachmentById($attachmentId);

                $imageImagesArray[0] = new \Site\Custom\FlexibleContainer();
                $imageImagesArray[0]->imageId = false;
                $imageImagesArray[0]->isMain = true;
                $imageImagesArray[0]->attachmentId = $attachmentId;
                $imageImagesArray[0]->uniqueHashForFileName
                    = $attachment ? $attachment->hash_for_file_name : '';
                $imageImagesArray[0]->originalFileName
                    = $attachment ? $attachment->original_file_name : '';

                $currentImageFormData = array(
                    'imageId' => $image->image_id,
                    'imageContentId' => $image->content_id,
                    'imageAttachmentId' => $image->attachment_id,
                    'imageOrder' => $image->order,
                    'imageIsPublished' => $image->is_published,
                    'imageUrlAddressType' => $image->url_address_type,
                    'imageExternalUrl' => $image->external_url,
                    'imageTitle' => $image->title,
                    'imageImages' => $imageImagesArray,
                    'imageUniqueHashForFileName' => $attachment ? $attachment->hash_for_file_name : '',
                    'imageOriginalFileName' => $attachment ? $attachment->original_file_name : ''
                );
            }
        }

        $formData = new \Site\Custom\FlexibleContainer(array_merge($defaultFormData, $currentImageFormData));

        $imageInternalPagesOptions = array();
        $pages = $pageManager->getPages();
        if ($pages)
        {
            foreach ($pages as $page)
            {
                $option = new \stdClass();
                $option->title = $page->title;
                $option->extended_title =
                    $page->title .
                    ' (' . $pageManager->getFullPageCleanUrlByPageId($page->page_id) . ')';
                $option->selected = ($page->page_id == $formData->imagePageId) ? ' selected' : '';

                $imageInternalPagesOptions[$page->page_id] = $option;
            }
        }

        $this->addEditView->setVariables(
            array(
                'imageInternalPagesOptions' => $imageInternalPagesOptions,
                'formData' => $formData
            )
        );
    }

    public function childRenderSettings()
    {
        $this->settingsView->setVariables(
            array(

            )
        );
    }

    /**
     * Getters.
     */

    public function getImageById($imageId)
    {
        $imageId = (int)$imageId;

        if ($imageId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_IMAGE_SLIDER_TABLE_NAME)
        ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
        ->where(
            array(
                'image_id' => (int)$imageId,
                'content_id' => $this->getContentId()
            )
        );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function isImageUrlAddressTypeValid($imageUrlAddressType)
    {
        $imageUrlAddressType = (int)$imageUrlAddressType;

        return ($imageUrlAddressType >= self::IMAGE_URL_ADDRESS_TYPE_INTERNAL) &&
               ($imageUrlAddressType <= self::IMAGE_URL_ADDRESS_TYPE_NONE);
    }

    public function isImageExternalUrlValid($imageExternalUrl)
    {
        $imageExternalUrl = trim(strip_tags((string)$imageExternalUrl));

        return ($imageExternalUrl != '');
    }

    /**
     * Data formatting functions.
     */

    public function formatImageDataAndReturnIfAreValid($imageData)
    {
        $imageData = $this->convertToFlexibleContainer($imageData);
        $imageDataCopy = clone $imageData;

        $imageDataCopy->imageId = (int)$imageData->imageId ? : null;
        $imageDataCopy->imageAttachmentId = (int)$imageData->imageAttachmentId ? : null;
        $imageDataCopy->imagePageId = (int)$imageData->imagePageId ? : null;
        $imageDataCopy->imageOrder = ((int)$imageData->imageOrder > 0) ? : 0;
        $imageDataCopy->imageIsPublished = (int)$imageData->imageIsPublished ? 1 : 0;
        $imageDataCopy->imageUrlAddressType = (int)$imageData->imageUrlAddressType;
        $imageDataCopy->imageExternalUrl = trim((string)$imageData->imageExternalUrl);
        $imageDataCopy->imageTitle = trim((string)$imageData->imageTitle);
        $imageDataCopy->imageImages = $imageData->imageImages;
        if ($imageDataCopy->imageImages instanceof \Site\Custom\FlexibleContainer)
        {
            $imageDataCopy->imageImages = $imageDataCopy->imageImages->toArray(true);
        }

        $imageImages = $imageDataCopy->imageImages;
        $imageImage = $imageImages ? current($imageImages) : null;

        /**
         * Temporary data - from add-form. They will be parsed and deleted.
         */

        $imageDataCopy->imageUniqueHashForFileName = trim((string)$imageData->imageUniqueHashForFileName);

        /**
         * Validating form data.
         */

        if (!$imageImage || ($imageImage['uniqueHashForFileName'] == ''))
        {
            return false;
        }

        if ($imageImage)
        {
            $imageDataCopy->imageUniqueHashForFileName = trim((string)$imageImage['uniqueHashForFileName']);

            // Validate unique hash for file name only if it is not empty!
            if (
                ($imageDataCopy->imageUniqueHashForFileName != '') &&
                !preg_match('/^([a-z0-9]{40})$/i', $imageDataCopy->imageUniqueHashForFileName)
            )
            {
                return false;
            }
        }

        /**
         * Identifying the author.
         */

        $imageDataCopy->imageLastUpdateAuthorId = $this->user->user_id;

        /**
         * Parsing unique hash for file name.
         */

        $imageDataCopy->imageAttachmentId = null;
        if ($imageDataCopy->imageUniqueHashForFileName != '')
        {
            $attachmentManager = $this->getAttachmentManager();
            $attachment = $attachmentManager->getAttachmentByUniqueHashForFileName(
                $imageDataCopy->imageUniqueHashForFileName
            );
            if (!$attachment)
            {
                return false;
            }

            $imageDataCopy->imageAttachmentId = $attachment->attachment_id;
        }

        /**
         * Removing unnecessary data.
         */

        unset($imageDataCopy->imageUniqueHashForFileName);

        /**
         * Adding "http://".
         */

        if (
            ($imageDataCopy->imageExternalUrl != '') &&
            (mb_strpos($imageDataCopy->imageExternalUrl, 'http://') === false) &&
            (mb_strpos($imageDataCopy->imageExternalUrl, 'https://') === false)
        )
        {
            $imageDataCopy->imageExternalUrl = 'http://' . $imageDataCopy->imageExternalUrl;
        }

        $isExternalUrlInvalid =
            ($imageDataCopy->imageUrlAddressType == self::IMAGE_URL_ADDRESS_TYPE_EXTERNAL) &&
            !$this->isImageExternalUrlValid($imageDataCopy->imageExternalUrl);

        if (
            (($imageDataCopy->imageId !== null) && ($imageDataCopy->imageId <= 0)) ||
            (($imageDataCopy->imageAttachmentId !== null) && ($imageDataCopy->imageAttachmentId <= 0)) ||
            (($imageDataCopy->imagePageId !== null) && ($imageDataCopy->imagePageId <= 0)) ||
            !$this->isImageUrlAddressTypeValid($imageDataCopy->imageUrlAddressType) ||
            $isExternalUrlInvalid
        )
        {
            return false;
        }

        return $imageDataCopy;
    }

    /**
     * Logic-functions.
     */

    public function addOrEditImage($imageData)
    {
        $imageDataCopy = $this->formatImageDataAndReturnIfAreValid($imageData);
        if (!$imageDataCopy)
        {
            return false;
        }

        $imageDataCopy->imageLastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        if ($imageData->imageId > 0)
        {
            return $this->updateExistingImage($imageDataCopy);
        }
        else
        {
            return $this->createNewImage($imageDataCopy);
        }
    }

    public function decideWhatToDoWithRecentlyUploadedAndMovedFiles(\Zend\Http\Request $request, $movedFiles)
    {
        // Inform the UploadController and UploadManager than upload was successful and you accept these files:
        return true;
    }

    public function deleteAllImages()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_IMAGE_SLIDER_TABLE_NAME);
        $delete->where(array('content_id' => $this->getContentId()));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;
    }

    public function deleteImageById($imageId)
    {
        $imageId = (int)$imageId;

        if ($imageId <= 0)
        {
            return false;
        }

        $deleted = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();

        $delete = $sql->delete(self::BLOCK_IMAGE_SLIDER_TABLE_NAME);
        $delete->where(
            array(
                'image_id' => $imageId,
                'content_id' => $contentId
            )
        );
        $result = $dbManager->executePreparedStatement($delete);
        if ($result)
        {
            $deleted = true;
        }

        return $deleted;
    }

    public function reorderImages($images)
    {
        $images = (array)$images;

        if (!$images)
        {
            return false;
        }

        $reordered = true;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();
        $order = 0;

        $dbManager->beginTransaction();

        foreach ($images as $imageId)
        {
            $imageId = (int)$imageId;

            if ($imageId > 0)
            {
                $update = $sql->update(self::BLOCK_IMAGE_SLIDER_TABLE_NAME);
                $update->set(array('order' => $order))
                    ->where(
                        array(
                            'image_id' => $imageId,
                            'content_id' => $contentId
                        )
                    );
                $result = $dbManager->executePreparedStatement($update);
                if (!$result)
                {
                    $reordered = false;
                    break;
                }

                $image = $this->getImageById($imageId);
                if ($image->order != $order)
                {
                    $reordered = false;
                    break;
                }

                $order++;
            }
        }

        if ($reordered)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $reordered;
    }

    public function toggleImagePublished($imageId, $published)
    {
        $imageId = (int)$imageId;
        $published = (int)$published ? 1 : 0;

        if ($imageId <= 0)
        {
            return false;
        }

        $updated = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $image = $this->getImageById($imageId);
        if ($image)
        {
            $update = $sql->update(self::BLOCK_IMAGE_SLIDER_TABLE_NAME);
            $update->set(
                array(
                    'is_published' => $published,
                    'content_id' => $this->getContentId()
                )
            )
            ->where(array('image_id' => $imageId));
            $result = $dbManager->executePreparedStatement($update);
            if ($result)
            {
                $image = $this->getImageById($imageId);
                if ($image && ($image->is_published == $published))
                {
                    $updated = true;
                }
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    private function createNewImage($imageData)
    {
        $created = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $insert = $sql->insert(self::BLOCK_IMAGE_SLIDER_TABLE_NAME);
        $insert->columns(
            array(
                'content_id',
                'attachment_id',
                'page_id',
                'order',
                'is_published',
                'url_address_type',
                'last_update_author_id',
                'last_update_date',
                'external_url',
                'title',
            )
        )
        ->values(
            array(
                'content_id' => $contentId,
                'attachment_id' => $imageData->imageAttachmentId,
                'page_id' => $imageData->imagePageId,
                'order' => $imageData->imageOrder,
                'is_published' => $imageData->imageIsPublished,
                'url_address_type' => $imageData->imageUrlAddressType,
                'last_update_author_id' => $imageData->imageLastUpdateAuthorId,
                'last_update_date' => $imageData->imageLastUpdateDate,
                'external_url' => $imageData->imageExternalUrl,
                'title' => $imageData->imageTitle
            )
        );
        $imageId = $dbManager->executeInsertAndGetLastInsertId($insert);
        if ($imageId > 0)
        {
            $created = true;
        }

        if ($created)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $created;
    }

    private function updateExistingImage($imageData)
    {
        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_IMAGE_SLIDER_TABLE_NAME);
        $update->set(
            array(
                'attachment_id' => $imageData->imageAttachmentId,
                'page_id' => $imageData->imagePageId,
                'order' => $imageData->imageOrder,
                'is_published' => $imageData->imageIsPublished,
                'url_address_type' => $imageData->imageUrlAddressType,
                'last_update_author_id' => $imageData->imageLastUpdateAuthorId,
                'last_update_date' => $imageData->imageLastUpdateDate,
                'external_url' => $imageData->imageExternalUrl,
                'title' => $imageData->imageTitle
            )
        )
        ->where(
            array(
                'image_id' => $imageData->imageId,
                'content_id' => $contentId
            )
        );
        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $image = $this->getImageById($imageData->imageId);
            if ($image &&
                ($image->attachment_id == $imageData->imageAttachmentId) &&
                ($image->page_id == $imageData->imagePageId) &&
                ($image->order == $imageData->imageOrder) &&
                ($image->is_published == $imageData->imageIsPublished) &&
                ($image->url_address_type == $imageData->imageUrlAddressType) &&
                ($image->last_update_author_id == $imageData->imageLastUpdateAuthorId) &&
                ($image->external_url == $imageData->imageExternalUrl) &&
                ($image->title == $imageData->imageTitle)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }
}
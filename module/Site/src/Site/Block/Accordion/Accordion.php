<?php
namespace Site\Block\Accordion;

use Site\Block\AbstractBlock;
use Zend\Http\Request;

class Accordion extends AbstractBlock
{
    const BLOCK_ACCORDION_TABLE_NAME = 'block_accordion';

    /**
     * Events.
     */

    public function onBeforeDeleted()
    {
        if (!$this->blockData->is_dynamic)
        {
            // Delete all this block's data from database etc.
            $this->deleteAccordionItems();
            $this->deleteBlockRecordFromSearchIndex();
        }
    }

    public function updateSearchIndex()
    {
        $allPanels = $this->getAllPanels();

        if ($allPanels)
        {
            foreach ($allPanels as $panel)
            {
                $this->updateBlockRecordInSearchIndex(
                    $panel->panel_id, $panel->contents, '', 'Static content', $panel->last_update_date
                );
            }
        }
    }

    /**
     * Render-functions.
     */

    public function childRender(\Zend\Http\Request $request)
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_ACCORDION_TABLE_NAME)
            ->where(array('content_id' => $contentId))
            ->order('order ASC,name ASC');
        $resultSet = $dbManager->getHydratedResultSet($select)->buffer();
        $accordion = $resultSet;

        $this->blockView->setVariables(
            array(
                'accordion' => $accordion,
                'contentId' => $contentId
            )
        );
    }

    public function getAllPanels()
    {
        $prefixColumnsWithTable = false;
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_ACCORDION_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getPanelById($panelId)
    {
        $panelId = (int)$panelId;

        if ($panelId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_ACCORDION_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'panel_id' => $panelId,
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getPanelCount()
    {
        $prefixColumnsWithTable = false;
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_ACCORDION_TABLE_NAME)
            ->columns(
                array(
                    'count' => new \Zend\Db\Sql\Expression('COUNT(*)')
                ),
                $prefixColumnsWithTable
            )
            ->where(
                array(
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();

            return $row->count;
        }

        return false;
    }

    public function getMaxPanelsOrder()
    {
        $prefixColumnsWithTable = false;
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $dbPlatform = $dbManager->getAdapter()->getPlatform();
        $sql = $dbManager->getSql();

        $orderColumnQuoted = $dbPlatform->quoteIdentifier('order');

        $select = $sql->select();
        $select
            ->from(self::BLOCK_ACCORDION_TABLE_NAME)
            ->columns(
                array(
                    'max_order' => new \Zend\Db\Sql\Expression('MAX(' . $orderColumnQuoted . ')')
                ),
                $prefixColumnsWithTable
            )
            ->where(
                array(
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();

            return $row->max_order;
        }

        return false;
    }

    public function formatPanelDataAndReturnIfValid($panelData)
    {
        $panelData = $this->convertToFlexibleContainer($panelData);
        $panelDataCopy = clone $panelData;

        $panelDataCopy->panelId = (int)$panelData->panelId ? : null;
        $panelDataCopy->panelIsPublished = (int)$panelDataCopy->panelIsPublished ? 1 : 0;
        $panelDataCopy->panelOrder = (int)$panelDataCopy->panelOrder;
        if ($panelDataCopy->panelOrder < 0)
        {
            $panelDataCopy->panelOrder = 0;
        }
        $panelDataCopy->panelName = trim((string)$panelData->panelName);
        $panelDataCopy->panelContents = trim((string)$panelData->panelContents);

        /**
         * Temporary data - from add-form. They will be parsed and deleted.
         */

        /**
         * Validating form data.
         */

        /**
         * Identifying the author.
         */
        $panelDataCopy->panelLastUpdateAuthorId = $this->user->user_id;

        /**
         * Removing uneccessary data.
         */

        if (
            (($panelDataCopy->panelId !== null) && ($panelDataCopy->panelId <= 0)) ||
            ($panelDataCopy->panelName == '')
        )
        {
            return false;
        }

        return $panelDataCopy;
    }

    public function addOrEditPanel($panelData)
    {
        $panelDataCopy = $this->formatPanelDataAndReturnIfValid($panelData);
        if (!$panelDataCopy)
        {
            return false;
        }

        $panelDataCopy->panelLastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        if ($panelData->panelId > 0)
        {
            return $this->updateExistingPanel($panelDataCopy);
        }
        else
        {
            return $this->createNewPanel($panelDataCopy);
        }
    }

    private function createNewPanel($panelData)
    {
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $insert = $sql->insert(self::BLOCK_ACCORDION_TABLE_NAME);
        $insert
            ->columns(
                array(
                    'content_id',
                    'is_published',
                    'order',
                    'last_update_author_id',
                    'last_update_date',
                    'name',
                    'contents'
                )
            )
            ->values(
                array(
                    'content_id' => $contentId,
                    'is_published' => $panelData->panelIsPublished,
                    'order' => $panelData->panelOrder,
                    'last_update_author_id' => $panelData->panelLastUpdateAuthorId,
                    'last_update_date' => $panelData->panelLastUpdateDate,
                    'name' => $panelData->panelName,
                    'contents' => $panelData->panelContents
                )
            );
        $panelId = $dbManager->executeInsertAndGetLastInsertId($insert);

        if ($panelId > 0)
        {
            $dbManager->commit();

            return $panelId;
        }

        $dbManager->rollback();

        return false;
    }

    private function updateExistingPanel($panelData)
    {
        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_ACCORDION_TABLE_NAME);
        $update
            ->set(
                array(
                    'is_published' => $panelData->panelIsPublished,
                    'order' => $panelData->panelOrder,
                    'last_update_author_id' => $panelData->panelLastUpdateAuthorId,
                    'last_update_date' => $panelData->panelLastUpdateDate,
                    'name' => $panelData->panelName,
                    'contents' => $panelData->panelContents
                )
            )
            ->where(
                array(
                    'panel_id' => $panelData->panelId,
                    'content_id' => $contentId
                )
            );

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $panel = $this->getPanelById($panelData->panelId);
            if ($panel &&
                ($panel->is_published == $panelData->panelIsPublished) &&
                ($panel->panelOrder == $panelData->panelOrder) &&
                ($panel->last_update_author_id == $panelData->panelLastUpdateAuthorId) &&
                ($panel->name == $panelData->panelName) &&
                ($panel->contents == $panelData->panelContents)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function updatePanelName($panelId, $panelName)
    {
        $panelId = (int)$panelId;
        $panelName = trim(strip_tags((string)$panelName));
        $panelName = str_replace("\r\n", ' ', $panelName);
        $panelName = str_replace(array("\r", "\n"), ' ', $panelName);

        if (($panelId <= 0) || ($panelName == ''))
        {
            return false;
        }

        $updated = false;

        $lastUpdateAuthorId = $this->user->user_id ? (int)$this->user->user_id : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_ACCORDION_TABLE_NAME);
        $update
            ->set(
                array(
                    'last_update_author_id' => $lastUpdateAuthorId,
                    'last_update_date' => $lastUpdateDate,
                    'name' => $panelName
                )
            )
            ->where(array('panel_id' => $panelId));

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $panel = $this->getPanelById($panelId);
            if ($panel &&
                ($panel->last_update_author_id == $lastUpdateAuthorId) &&
                ($panel->name == $panelName)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function updatePanelContents($panelId, $panelContents)
    {
        $panelId = (int)$panelId;
        $panelContents = trim((string)$panelContents);

        if ($panelId <= 0)
        {
            return false;
        }

        $updated = false;

        $lastUpdateAuthorId = $this->user->user_id ? (int)$this->user->user_id : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_ACCORDION_TABLE_NAME);
        $update
            ->set(
                array(
                    'last_update_author_id' => $lastUpdateAuthorId,
                    'last_update_date' => $lastUpdateDate,
                    'contents' => $panelContents
                )
            )
            ->where(array('panel_id' => $panelId));

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $panel = $this->getPanelById($panelId);
            if ($panel &&
                ($panel->last_update_author_id == $lastUpdateAuthorId) &&
                ($panel->contents == $panelContents)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function deletePanelById($panelId)
    {
        $panelId = (int)$panelId;

        if ($panelId <= 0)
        {
            return false;
        }

        $deleted = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_ACCORDION_TABLE_NAME);
        $delete
            ->where(
                array(
                    'panel_id' => $panelId,
                    'content_id' => $this->getContentId()
                )
            );
        $result = $dbManager->executePreparedStatement($delete);
        if ($result)
        {
            $deleted = true;
        }

        return $deleted;
    }

    public function deleteAccordionItems()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_ACCORDION_TABLE_NAME);
        $delete->where(array('content_id' => $this->getContentId()));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;
    }

    public function reorderPanels($panels)
    {
        $panels = (array)$panels;

        if (!$panels)
        {
            return false;
        }

        $reordered = true;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $order = 0;
        $contentId = $this->getContentId();

        $dbManager->beginTransaction();

        foreach ($panels as $panelId)
        {
            $panelId = (int)$panelId;

            if ($panelId > 0)
            {
                $update = $sql->update(self::BLOCK_ACCORDION_TABLE_NAME);
                $update
                    ->set(array('order' => $order))
                    ->where(
                        array(
                            'panel_id' => $panelId,
                            'content_id' => $contentId
                        )
                    );
                $result = $dbManager->executePreparedStatement($update);
                if (!$result)
                {
                    $reordered = false;
                    break;
                }

                $panel = $this->getPanelById($panelId);
                if ($panel->order != $order)
                {
                    $reordered = false;
                    break;
                }

                $order++;
            }
        }

        if ($reordered)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $reordered;
    }
}

<?php
namespace Site\Block\BachelorStudies;

use Site\Block\AbstractBlock;
use Zend\Http\Request;

class BachelorStudies extends AbstractBlock
{
    const BLOCK_BACHELOR_STUDIES_TABLE_NAME = 'block_bachelor_studies';
    const BLOCK_BACHELOR_STUDIES_TABS_TABLE_NAME = 'block_bachelor_studies_tabs';

    protected $hasOwnSettings = true;

    /**
     * Events.
     */

    public function onAfterInserting($slotId)
    {
        $tabsCreated = $this->createDefaultTabsIfTheyAreNotExists();
        if ($tabsCreated)
        {
            $basicInfoCreated = $this->createDefaultBasicInfoIfTheyAreNotExists();
        }

        return ($tabsCreated && $basicInfoCreated);
    }

    public function onBeforeDeleted()
    {
        if (!$this->blockData->is_dynamic)
        {
            // Delete all this block's data from database etc.
            $this->deleteAllTabs();
            $this->deleteAllBasicInfo();
            $this->deleteBlockRecordFromSearchIndex();
        }
    }

    public function updateSearchIndex()
    {
        $allTabs = $this->getAllTabs();

        if ($allTabs)
        {
            foreach ($allTabs as $tab)
            {
                $this->updateBlockRecordInSearchIndex(
                    $tab->tab_id, $tab->contents, '', 'Bachelor studies', $tab->last_update_date
                );
            }
        }
    }

    /**
     * Render-functions.
     */

    public function childRender(\Zend\Http\Request $request)
    {
        $settings = $this->getSettings();
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();
        $basicInfo = $this->getBasicInfo();

        /**
         * Load image info.
         */

        $uniqueHashForFileName = $settings->uniqueHashForFileName;

        $defaultThumbnailUrl = '';
        $thumbUrl = $defaultThumbnailUrl;
        if ($uniqueHashForFileName != '')
        {
            $attachment = $this->attachmentManager->getAttachmentByUniqueHashForFileName($uniqueHashForFileName);
            if ($attachment)
            {
                $thumbUrl =
                    '/eng/get-file/index,' .
                    $attachment->hash_for_file_name .
                    ',' .
                    $attachment->original_file_name;
            }
        }

        /**
         * Tabs.
         */

        $select = $sql->select();
        $select
            ->from(self::BLOCK_BACHELOR_STUDIES_TABS_TABLE_NAME)
            ->where(array('content_id' => $contentId))
            ->order('order ASC,name ASC');
        $resultSet = $dbManager->getHydratedResultSet($select)->buffer();
        $tabs = $resultSet;

        if ($tabs)
        {
            $tabId = $this->blockView->getVariable('tabId');

            $isFirst = true;
            foreach ($tabs as $tab)
            {
                $tab->additional_css_classes = '';
                $tab->is_active = false;

                if ($tabId > 0)
                {
                    if ($tabId == $tab->tab_id)
                    {
                        $tab->additional_css_classes = ' active';
                        $tab->is_active = true;
                    }
                }
                else
                {
                    if ($isFirst)
                    {
                        $tab->additional_css_classes = ' active';
                        $tab->is_active = true;
                    }
                }

                $isFirst = false;
            }
        }

        $this->blockView->setVariables(
            array(
                'basicInfo' => $basicInfo,
                //'glyphicon' => $glyphicon,
                'thumbUrl' => $thumbUrl,
                'tabs' => $tabs
            )
        );
    }

    public function childRenderSettings()
    {
        $settings = $this->getSettings();

        $selectedGlyphicon = $settings->glyphicon;

        $this->settingsView->setVariables(
            array(
                'glyphiconsOptions' => $this->getGlyphiconOptionsHtmlForDropdown($selectedGlyphicon),
            )
        );
    }

    /**
     * Getters.
     */

    public function getGlyphiconOptionsHtmlForDropdown($selectedGlyphicon = '')
    {
        $selectedGlyphicon = trim((string)$selectedGlyphicon);

        $glyphicons = array(
            'vistula-uniF4A1',
            'vistula-uniF4C6',
            'vistula-office',
            'vistula-uniF5E4',
            'vistula-location',
            'vistula-bubbles',
            'vistula-book',
            'vistula-book2',
            'vistula-library',
            'vistula-map',
            'vistula-pen',
            'vistula-globe',
            'vistula-tie',
            'vistula-monitor',
            'vistula-stats',
            'vistula-diamond',
            'vistula-suitcase',
            'vistula-airplane',
            'vistula-turkey'
        );

        sort($glyphicons);

        $options = '';
        foreach ($glyphicons as $icon)
        {
            $selected = ($selectedGlyphicon == $icon) ? ' selected' : '';

            $options .=
                '<option data-subtext="&lt;i class=\'' . $icon . '\'&gt;&lt;/i&gt;"' . $selected . '>' .
                $icon .
                '</option>';
        }

        return $options;
    }

    public function getBasicInfo()
    {
        $prefixColumnsWithTable = false;
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_BACHELOR_STUDIES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('content_id' => $contentId));
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getAllTabs()
    {
        $prefixColumnsWithTable = false;
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_BACHELOR_STUDIES_TABS_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getTabById($tabId)
    {
        $tabId = (int)$tabId;

        if ($tabId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_BACHELOR_STUDIES_TABS_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'tab_id' => $tabId,
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function tabByNameExists($tabName)
    {
        $tabName = trim((string)$tabName);

        if ($tabName == '')
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_BACHELOR_STUDIES_TABS_TABLE_NAME)
            ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $contentId,
                    'name' => $tabName
                )
            )
            ->limit(1);
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();
            if ($row)
            {
                return ($row->count > 0);
            }
        }

        return false;
    }

    /**
     * Data formatting functions.
     */

    /**
     * Logic-functions.
     */

    public function createDefaultBasicInfoIfTheyAreNotExists()
    {
        if ($this->getBasicInfo())
        {
            return true;
        }

        $contentId = $this->getContentId();
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');
        $lastUpdateAuthorId = null;

        $translate = $this->getTranslateHelper();
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $insert = $sql->insert(self::BLOCK_BACHELOR_STUDIES_TABLE_NAME);
        $insert
            ->columns(
                array(
                    'content_id',
                    'last_update_author_id',
                    'last_update_date',
                    'title',
                    'time',
                    'degree_type',
                    'language',
                    'description'
                )
            )
            ->values(
                array(
                    'content_id' => $contentId,
                    'last_update_author_id' => $lastUpdateAuthorId,
                    'last_update_date' => $lastUpdateDate,
                    'title' =>
                        'STUDIA I stopnia, tryb stacjonarny i niestacjonarny' . "\n" .
                        'Absolwenci uzyskują dyplom licencjata ekonomii',
                    'time' => 'Czas trwania',
                    'degree_type' => $translate('Mode') . ': ' . $translate('stationary'),
                    'language' => $translate('Language'),
                    'description' => ''
                )
            );
        $result = $dbManager->executePreparedStatement($insert);
        if ($result)
        {
            $basicInfo = $this->getBasicInfo();
            if ($basicInfo)
            {
                return true;
            }
        }

        return false;
    }

    public function createDefaultTabsIfTheyAreNotExists()
    {
        $translate = $this->getTranslateHelper();

        $dataTypes = array(
            $translate('About the studies'),
            $translate('Strengths'),
            $translate('Program'),
            $translate('Specialties'),
            $translate('Working after graduation'),
            $translate('Recruitment and fees')
        );

        foreach ($dataTypes as $name)
        {
            $tabId = $this->createTabAndReturnId($name);
            if (!$tabId)
            {
                $this->deleteAllTabs();

                return false;
            }
        }

        return true;
    }

    public function createTabAndReturnId($name)
    {
        $contentId = $this->getContentId();
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');
        $lastUpdateAuthorId = null;
        $name = trim((string)$name);

        $typeId = null;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        if (!$this->tabByNameExists($name))
        {
            $insert = $sql->insert(self::BLOCK_BACHELOR_STUDIES_TABS_TABLE_NAME);
            $insert
                ->columns(
                    array(
                        'content_id',
                        'last_update_author_id',
                        'last_update_date',
                        'name',
                        'contents'
                    )
                )
                ->values(
                    array(
                        'content_id' => $contentId,
                        'last_update_author_id' => $lastUpdateAuthorId,
                        'last_update_date' => $lastUpdateDate,
                        'name' => $name,
                        'contents' => ''
                    )
                );
            $tabId = $dbManager->executeInsertAndGetLastInsertId($insert);
            if ($tabId > 0)
            {
                $tab = $this->getTabById($tabId);
                if (
                    !$tab ||
                    ($tab->content_id != $contentId) ||
                    ($tab->last_update_author_id != $lastUpdateAuthorId) ||
                    ($tab->name != $name)
                )
                {
                    $tabId = false;
                }
            }
        }

        if ($tabId > 0)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $tabId;
    }

    public function decideWhatToDoWithRecentlyUploadedAndMovedFiles(\Zend\Http\Request $request, $movedFiles)
    {
        //$postData = $request->getPost();

        // Inform the UploadController and UploadManager than upload was successful and you accept these files:
        return true;
    }

    public function deleteAllBasicInfo()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_BACHELOR_STUDIES_TABLE_NAME);
        $delete->where(array('content_id' => $this->getContentId()));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;
    }

    public function deleteAllTabs()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_BACHELOR_STUDIES_TABS_TABLE_NAME);
        $delete->where(array('content_id' => $this->getContentId()));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;
    }

    public function reorderTabs($tabs)
    {
        $tabs = (array)$tabs;

        if (!$tabs)
        {
            return false;
        }

        $reordered = true;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $order = 0;
        $contentId = $this->getContentId();

        $dbManager->beginTransaction();

        foreach ($tabs as $tabId)
        {
            $tabId = (int)$tabId;

            if ($tabId > 0)
            {
                $update = $sql->update(self::BLOCK_BACHELOR_STUDIES_TABS_TABLE_NAME);
                $update
                    ->set(array('order' => $order))
                    ->where(
                        array(
                            'tab_id' => $tabId,
                            'content_id' => $contentId
                        )
                    );
                $result = $dbManager->executePreparedStatement($update);
                if (!$result)
                {
                    $reordered = false;
                    break;
                }

                $tab = $this->getTabById($tabId);
                if ($tab->order != $order)
                {
                    $reordered = false;
                    break;
                }

                $order++;
            }
        }

        if ($reordered)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $reordered;
    }

    public function updateBasicInfo($infoType, $contents)
    {
        $infoType = mb_strtolower(trim((string)$infoType));
        $contents = trim((string)$contents);

        if (
            !in_array($infoType, array('title', 'time', 'degree_type', 'language', 'description')) ||
            ($contents == '')
        )
        {
            return false;
        }

        $contentId = $this->getContentId();

        $updated = false;

        $lastUpdateAuthorId = $this->user->user_id ? (int)$this->user->user_id : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_BACHELOR_STUDIES_TABLE_NAME);
        $update
            ->set(
                array(
                    'last_update_author_id' => $lastUpdateAuthorId,
                    'last_update_date' => $lastUpdateDate,
                    $infoType => $contents
                )
            )
            ->where(array('content_id' => $contentId));
        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $basicInfo = $this->getBasicInfo();
            if ($basicInfo &&
                ($basicInfo->last_update_author_id == $lastUpdateAuthorId) &&
                ($basicInfo->$infoType == $contents)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function updateTabContents($tabId, $tabContents)
    {
        $tabId = (int)$tabId;
        $tabContents = trim((string)$tabContents);

        if ($tabId <= 0)
        {
            return false;
        }

        $updated = false;

        $lastUpdateAuthorId = $this->user->user_id ? (int)$this->user->user_id : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_BACHELOR_STUDIES_TABS_TABLE_NAME);
        $update
            ->set(
                array(
                    'last_update_author_id' => $lastUpdateAuthorId,
                    'last_update_date' => $lastUpdateDate,
                    'contents' => $tabContents
                )
            )
            ->where(array('tab_id' => $tabId));

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $tab = $this->getTabById($tabId);
            if ($tab &&
                ($tab->last_update_author_id == $lastUpdateAuthorId) &&
                ($tab->contents == $tabContents)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $this->updateBlockRecordInSearchIndex($tabId, $tabContents, '', 'Bachelor studies');

            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }
}
<?php
namespace Site\Block\NavigationMenu;

use Site\Block\AbstractBlock;

class NavigationMenu extends AbstractBlock
{
    const ITEM_TYPE_ONLY_TEXT = 0;
    const ITEM_TYPE_ONLY_ICON = 1;
    const ITEM_TYPE_ONLY_IMAGE = 2;
    const ITEM_TYPE_ICON_AND_TEXT = 3;
    const ITEM_TYPE_IMAGE_AND_TEXT = 4;

    const ITEM_URL_ADDRESS_TYPE_INTERNAL = 0;
    const ITEM_URL_ADDRESS_TYPE_EXTERNAL = 1;
    const ITEM_URL_ADDRESS_TYPE_NONE = 2;

    const BLOCK_NAVIGATION_MENU_CATEGORIES_TABLE_NAME = 'block_navigation_menu_categories';
    const BLOCK_NAVIGATION_MENU_ITEMS_TABLE_NAME = 'block_navigation_menu_items';

    protected $hasOwnSettings = true;
    protected $hasManagementWindow = true;

    public function onAfterInserting($slotId)
    {
        return $this->addDefaultCategoryIfNoCategoryExists();
    }

    public function onBeforeDeleted()
    {
        if (!$this->blockData->is_dynamic)
        {
            $this->deleteAllItems();
            $this->deleteAllCategories();
        }
    }

    /**
     * Render-functions.
     */
    public function childRender(\Zend\Http\Request $request)
    {        
        $settings = $this->getSettings();

        $pageCleanUrl = $this->getRouteParametersFromRequest($request)->action;
        $subPageCleanUrl = $this->getSubPageCleanUrlFromRequest($request);
        
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $metadata = new \Zend\Db\Metadata\Metadata($dbManager->getAdapter());
        $blockNavigationMenuItemsTableColumnsMetadata = $metadata->getColumns(self::BLOCK_NAVIGATION_MENU_ITEMS_TABLE_NAME);
        $maxLengths = array();
        if ($blockNavigationMenuItemsTableColumnsMetadata)
        {
            foreach ($blockNavigationMenuItemsTableColumnsMetadata as $column)
            {
                $name = $column->getName();

                $maxLengths[$name] = $column->getCharacterMaximumLength();
            }
        }

        $contentId = $this->getContentId();

        $select = $sql->select();
        $select->from(array('bnmi' => self::BLOCK_NAVIGATION_MENU_ITEMS_TABLE_NAME))
            ->join(
                array('bnmc' => self::BLOCK_NAVIGATION_MENU_CATEGORIES_TABLE_NAME),
                'bnmc.category_id = bnmi.category_id',
                array('category_title' => 'title'),
                $select::JOIN_LEFT
            )
            ->join(
                array('u' => 'users'),
                'u.user_id = bnmi.last_update_author_id',
                array('last_update_author' => 'login'),
                $select::JOIN_LEFT
            )
            ->where(
                array(
                    'bnmc.category_id' => (int)$settings->categoryId,
                    'bnmc.is_published' => 1,
                    'bnmi.content_id' => $contentId,
                    'bnmi.is_published' => 1
                )
            )
            ->order('bnmi.parent_id ASC, bnmi.order ASC');
        $resultSet = $dbManager->getHydratedResultSet($select)->buffer();
        $itemTree = $this->makeItemTreeFromResultSetAndOptionalPageCleanUrls($resultSet, $pageCleanUrl, $subPageCleanUrl);

        if ($settings->orientation == '')
        {
            $pageBlockId = $this->blockData->page_block_id;
            $errorMessage = 'NavigationMenu block (id: ' . $pageBlockId . ') has no "orientation" variable in ' .
                'it\'s XML configuration in pages_blocks_contents table, so it is impossible to ' .
                'determine which template should be used to render menu items. The variable should ' .
                'contain "horizontal" or "vertical" value which points to "horizontal.phtml" or ' .
                '"vertical.phtml" template file.';
            throw new \Site\Exception\CmsException($errorMessage);
        }

        $this->blockView->setTemplate('blocks/' . $this->blockViewName . '/' . $settings->orientation);
        $this->blockView->setVariables(
            array(
                'maxLengths' => $maxLengths,
                'menuItems' => $itemTree
            )
        );
    }

    public function childRenderManagement(\Zend\Http\Request $request)
    {
        if (!$this->managementView instanceof \Zend\View\Model\ViewModel)
        {
            return false;
        }

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();

        $select = $sql->select();
        $select->from(array('bnmi' => self::BLOCK_NAVIGATION_MENU_ITEMS_TABLE_NAME))
            ->join(
                array('bnmc' => self::BLOCK_NAVIGATION_MENU_CATEGORIES_TABLE_NAME),
                'bnmc.category_id = bnmi.category_id',
                array('category_title' => 'title'),
                $select::JOIN_LEFT
            )
            ->join(
                array('u' => 'users'),
                'u.user_id = bnmi.last_update_author_id',
                array('last_update_author' => 'login'),
                $select::JOIN_LEFT
            )
            ->where(array('bnmi.content_id' => $contentId))
            ->order('bnmi.title ASC');
        $resultSet = $dbManager->getHydratedResultSet($select)->buffer();
        $itemTree = $this->makeItemTreeFromResultSetAndOptionalPageCleanUrls($resultSet, null, null);

        $this->managementView->setVariables(
            array(
                'menuCategoryCount' => $this->getCategoryCount(),
                'menuItemTree' => $itemTree
            )
        );
    }

    public function childRenderAddEdit(\Zend\Http\Request $request)
    {
        if (!$this->addEditView instanceof \Zend\View\Model\ViewModel)
        {
            return false;
        }

        $pageManager = $this->getPageManager();
        $translate = $this->getTranslateHelper();

        $item = null;
        $categoryId = null;
        $itemId = (int)$request->getPost('itemId');
        if ($itemId > 0)
        {
            $item = $this->getItemById($itemId);
            $categoryId = $item->category_id;
        }

        $option = new \stdClass();
        $option->title = '---' . $translate('None', 'default', $this->userLanguage->zend2_locale) . '---';
        $option->extended_title = $option->title;
        $option->selected = '';

        $itemParentItemsOptions = array();
        $itemCategoriesOptions = array();
        $itemInternalPagesOptions = array(
            '' => $option
        );

        $parentItems = $this->getParentItems($categoryId, $itemId);
        if ($parentItems)
        {
            foreach ($parentItems as $parentItem)
            {
                $option = new \stdClass();
                $option->css_classes = 'depth-' . $parentItem->depth;
                $option->title = $parentItem->title;
                $option->selected = ($item && $item->parent_id == $parentItem->item_id) ? ' selected' : '';

                $itemParentItemsOptions[$parentItem->item_id] = $option;
            }
        }

        $categories = $this->getCategories();
        if ($categories)
        {
            foreach ($categories as $category)
            {
                $itemCategoriesOptions[$category->category_id] = $category->title;
            }
        }

        $defaultFormData = array(
            'itemId' => '',
            'itemParentId' => null,
            'itemCategoryId' => null,
            'itemPageId' => null,
            'itemUrlAddressType' => 0,
            'itemType' => 0,
            'itemExternalUrl' => '',
            'itemImages' => array(
                0 => array(
                    'imageId' => null,
                    'isMain' => true,
                    'attachmentId' => null,
                    'uniqueHashForFileName' => '',
                    'originalFileName' => '',
                )
            ),
            'itemUniqueHashForFileName' => '',
            'itemOriginalFileName' => '',
            'itemTextIcon' => '',
            'itemTitle' => '',
            'itemIsPublished' => ''
        );
        $currentItemFormData = array();

        if ($item)
        {
            $itemImagesArray = array(
                0 => array(
                    'imageId' => null,
                    'isMain' => true,
                    'attachmentId' => null,
                    'uniqueHashForFileName' => '',
                    'originalFileName' => '',
                )
            );

            if ($item)
            {
                $attachmentId = $item->attachment_id;
                $attachment = $this->attachmentManager->getAttachmentById($attachmentId);

                $itemImagesArray[0] = new \Site\Custom\FlexibleContainer();
                $itemImagesArray[0]->imageId = false;
                $itemImagesArray[0]->isMain = true;
                $itemImagesArray[0]->attachmentId = $attachmentId;
                $itemImagesArray[0]->uniqueHashForFileName
                    = $attachment ? $attachment->hash_for_file_name : '';
                $itemImagesArray[0]->originalFileName
                    = $attachment ? $attachment->original_file_name : '';
            }

            $currentItemFormData = array(
                'itemId' => $item->item_id,
                'itemParentId' => $item->parent_id,
                'itemCategoryId' => $item->category_id,
                'itemPageId' => $item->page_id,
                'itemUrlAddressType' => $item->url_address_type,
                'itemType' => $item->type,
                'itemExternalUrl' => $item->external_url,
                'itemImages' => $itemImagesArray,
                'itemUniqueHashForFileName' => $attachment ? $attachment->hash_for_file_name : '',
                'itemOriginalFileName' => $attachment ? $attachment->original_file_name : '',
                'itemTextIcon' => $item->bootstrap_glyphicon,
                'itemTitle' => $item->title,
                'itemIsPublished' => $item->is_published
            );
        }

        $formData = new \Site\Custom\FlexibleContainer(array_merge($defaultFormData, $currentItemFormData));

        $pages = $pageManager->getPages();
        if ($pages)
        {
            foreach ($pages as $page)
            {
                $option = new \stdClass();
                $option->title = $page->title;
                $option->extended_title =
                    $page->title .
                    ' (' . $pageManager->getFullPageCleanUrlByPageId($page->page_id) . ')';
                $option->selected = ($page->page_id == $formData->itemPageId) ? ' selected' : '';

                $itemInternalPagesOptions[$page->page_id] = $option;
            }
        }

        $this->addEditView->setVariables(
            array(
                'itemParentItemsOptions' => $itemParentItemsOptions,
                'itemCategoriesOptions' => $itemCategoriesOptions,
                'itemInternalPagesOptions' => $itemInternalPagesOptions,
                'itemGlyphiconsOptions' => $this->getGlyphiconOptionsHtmlForDropdown($formData->itemTextIcon),
                'formData' => $formData
            )
        );
    }

    public function childRenderSettings()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_NAVIGATION_MENU_CATEGORIES_TABLE_NAME)
            ->where(array('content_id' => $this->getContentId()))
            ->order(array('title ASC'));
        $resultSet = $dbManager->getHydratedResultSet($select);

        $availableMenuItemsCategories = array();
        foreach ($resultSet as $category)
        {
            $availableMenuItemsCategories[$category->category_id] = $category->title;
        }

        if (!$availableMenuItemsCategories)
        {
            $availableMenuItemsCategories[''] = '-----';
        }

        $this->settingsView->setVariables(
            array(
                'availableMenuItemsCategories' => $availableMenuItemsCategories,
            )
        );
    }

    public function renderCategoriesManagement(\Zend\Http\Request $request, $return = true)
    {
        $settings = $this->getSettings();

        $pageNumber = (int)$request->getPost('pageNumber') ? : 1;
        $itemCountPerPage = (int)$request->getPost('itemCountPerPage') ? : 10;
        $includePagesFrom = $request->getPost('includePagesFrom') ? : 'current-language';

        $dbManager = $this->getDatabaseManager();
        $dbAdapter = $dbManager->getAdapter();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();

        $select = $sql->select();
        $select->from(array('bnmc' => self::BLOCK_NAVIGATION_MENU_CATEGORIES_TABLE_NAME))
            ->join(
                array('u' => 'users'),
                'u.user_id = bnmc.last_update_author_id',
                array('last_update_author' => 'login'),
                $select::JOIN_LEFT
            )
            ->where(array('bnmc.content_id' => $contentId))
            ->order('bnmc.title ASC');

        $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        $paginatorAdapter = new \Zend\Paginator\Adapter\DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new \Zend\Paginator\Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($pageNumber);
        $paginator->setItemCountPerPage($itemCountPerPage);

        $categoriesManagementView = new \Zend\View\Model\ViewModel();
        $categoriesManagementView->setTemplate('blocks/' . $this->blockViewName . '/categories-management.phtml');
        $categoriesManagementView->setVariables(
            array(
                'userLanguage' => $this->userLanguage,
                'user' => $this->user,
                'designMode' => $this->designMode,
                'blockData' => $this->blockData,
                'hasOwnSettings' => $this->hasOwnSettings,
                'settings' => $settings,
                'pageNumber' => $pageNumber,
                'itemCountPerPage' => $itemCountPerPage,
                'includePagesFrom' => $includePagesFrom,
                'menuCategoriesPaginator' => $paginator
            )
        );
        $categoriesManagementViewHtml = $this->phpRenderer->render($categoriesManagementView);

        if ($return)
        {
            return $categoriesManagementViewHtml;
        }
        else
        {
            echo $categoriesManagementViewHtml;
        }
    }

    public function renderAddEditCategory(\Zend\Http\Request $request, $return = true)
    {
        $settings = $this->getSettings();

        $pageNumber = (int)$request->getPost('pageNumber') ? : 1;
        $itemCountPerPage = (int)$request->getPost('itemCountPerPage') ? : 10;
        $includePagesFrom = $request->getPost('includePagesFrom') ? : 'current-language';

        $defaultFormData = array(
            'categoryId' => '',
            'categoryTitle' => '',
            'categoryIsPublished' => ''
        );
        $currentCategoryFormData = array();

        $categoryId = (int)$request->getPost('categoryId');
        if ($categoryId > 0)
        {
            $category = $this->getCategoryById($categoryId);

            $currentCategoryFormData = array(
                'categoryId' => $category->category_id,
                'categoryTitle' => $category->title,
                'categoryIsPublished' => $category->is_published
            );
        }

        $formData = array_merge($defaultFormData, $currentCategoryFormData);

        $addEditCategoryView = new \Zend\View\Model\ViewModel();
        $addEditCategoryView->setTemplate('blocks/' . $this->blockViewName . '/add-edit-category.phtml');
        $addEditCategoryView->setVariables(
            array(
                'userLanguage' => $this->userLanguage,
                'user' => $this->user,
                'designMode' => $this->designMode,
                'blockData' => $this->blockData,
                'hasOwnSettings' => $this->hasOwnSettings,
                'settings' => $settings,
                'pageNumber' => $pageNumber,
                'itemCountPerPage' => $itemCountPerPage,
                'includePagesFrom' => $includePagesFrom,
                'formData' => $formData
            )
        );
        $addEditCategoryViewHtml = $this->phpRenderer->render($addEditCategoryView);

        if ($return)
        {
            return $addEditCategoryViewHtml;
        }
        else
        {
            echo $addEditCategoryViewHtml;
        }
    }

    /**
     * Getters.
     */

    function getCurrentPageId(\Zend\Http\Request $request)
    {
        $currentPageId = null;
        $currentPageURL = $this->getRouteParametersFromRequest($request)->subPage;
        if(strlen($currentPageURL) < 1){
            $currentPageURL = $this->getRouteParametersFromRequest($request)->action;
        }
        $pageManager = $this->getPageManager();
        $allAvailablePages = $pageManager->getAllPages();
        foreach ($allAvailablePages as $page)
        {
            if($page->clean_url == $currentPageURL)
            {
                $currentPageId = $page->page_id;
            }
        }
        return $currentPageId;
    }

    public function areCategoryDataValid($categoryData)
    {
        if (!$categoryData)
        {
            return false;
        }

        $categoryData = $this->convertToFlexibleContainer($categoryData);

        $categoryId = (int)$categoryData->categoryId;
        $categoryTitle = trim((string)$categoryData->categoryTitle);

        if (
            (($categoryId !== null) && ($categoryId <= 0)) ||
            ($categoryTitle == '')
        )
        {
            return false;
        }

        return true;
    }

    public function areItemDataValid($itemData)
    {
        if (!$itemData)
        {
            return false;
        }

        $itemData = $this->convertToFlexibleContainer($itemData);

        $itemId = $itemData->itemId ? (int)$itemData->itemId : null;
        $itemParentId = $itemData->itemParentId ? (int)$itemData->itemParentId : null;
        $itemCategoryId = (int)$itemData->itemCategoryId;
        $itemPageId = $itemData->itemPageId ? (int)$itemData->itemPageId : null;
        $itemType = (int)$itemData->itemType;
        $itemUrlAddressType = (int)$itemData->itemUrlAddressType;
        $itemExternalUrl = trim((string)$itemData->itemExternalUrl);
        $itemTextIcon = trim((string)$itemData->itemTextIcon);
        $itemTitle = trim((string)$itemData->itemTitle);

        $isExternalUrlInvalid = ($itemUrlAddressType == self::ITEM_URL_ADDRESS_TYPE_EXTERNAL) &&
            !$this->isItemExternalUrlValid($itemExternalUrl);

        if (
            (($itemId !== null) && ($itemId <= 0)) ||
            (($itemParentId !== null) && ($itemParentId <= 0)) ||
            ($itemCategoryId <= 0) ||
            (($itemPageId !== null) && ($itemPageId <= 0)) ||
            !$this->isItemTypeValid($itemType) ||
            ($this->itemContainsIcon($itemType) && ($itemTextIcon == '')) ||
            ($this->itemContainsText($itemType) && ($itemTitle == '')) ||
            !$this->isItemUrlAddressTypeValid($itemUrlAddressType) ||
            $isExternalUrlInvalid
        )
        {
            return false;
        }

        return true;
    }

    public function calculateItemDepthByParentId($parentId)
    {
        $parentId = $parentId ? (int)$parentId : null;
        $depth = 0;

        if ($parentId > 0)
        {
            do
            {
                $item = $this->getItemById($parentId);
                $parentId = $item->parent_id;

                $depth++;
            }
            while ($item && ($parentId > 0));
        }

        return $depth;
    }

    public function getCategories()
    {
        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_NAVIGATION_MENU_CATEGORIES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('content_id' => $this->getContentId()));
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getCategoryById($categoryId)
    {
        $categoryId = (int)$categoryId;

        if ($categoryId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_NAVIGATION_MENU_CATEGORIES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'category_id' => (int)$categoryId,
                    'content_id' => $this->getContentId()
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getCategoryCount()
    {
        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_NAVIGATION_MENU_CATEGORIES_TABLE_NAME)
            ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')), $prefixColumnsWithTable)
            ->where(array('content_id' => $this->getContentId()));
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();
            $count = $row ? $row->count : false;

            return $count;
        }

        return false;
    }

    public function getItemById($itemId)
    {
        $itemId = (int)$itemId;

        if ($itemId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_NAVIGATION_MENU_ITEMS_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'item_id' => $itemId,
                    'content_id' => $this->getContentId()
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getParentItems($categoryId, $itemId = null)
    {
        $categoryId = (int)$categoryId ? : null;
        $itemId = (int)$itemId ? : null;

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_NAVIGATION_MENU_ITEMS_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where->equalTo('content_id', $this->getContentId())
            ->lessThan('depth', 2);

        if ($categoryId > 0)
        {
            $select->where->equalTo('category_id', $categoryId);
        }

        if ($itemId > 0)
        {
            $item = $this->getItemById($itemId);
            if (!$item)
            {
                return false;
            }

            $select->where->notEqualTo('item_id', $itemId);
        }

        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function isItemTypeValid($itemType)
    {
        $itemType = (int)$itemType;

        return ($itemType >= self::ITEM_TYPE_ONLY_TEXT) && ($itemType <= self::ITEM_TYPE_IMAGE_AND_TEXT);
    }

    public function isItemUrlAddressTypeValid($itemUrlAddressType)
    {
        $itemUrlAddressType = (int)$itemUrlAddressType;

        return ($itemUrlAddressType >= self::ITEM_URL_ADDRESS_TYPE_INTERNAL) &&
        ($itemUrlAddressType <= self::ITEM_URL_ADDRESS_TYPE_NONE);
    }

    public function isItemExternalUrlValid($itemExternalUrl)
    {
        $itemExternalUrl = trim(strip_tags((string)$itemExternalUrl));

        return ($itemExternalUrl != '');
    }

    public function itemContainsIcon($itemType)
    {
        $itemType = (int)$itemType;

        return (($itemType == self::ITEM_TYPE_ONLY_ICON) || ($itemType == self::ITEM_TYPE_ICON_AND_TEXT));
    }

    public function itemContainsImage($itemType)
    {
        $itemType = (int)$itemType;

        return (($itemType == self::ITEM_TYPE_ONLY_IMAGE) || ($itemType == self::ITEM_TYPE_IMAGE_AND_TEXT));
    }

    public function itemContainsText($itemType)
    {
        $itemType = (int)$itemType;

        return (($itemType == self::ITEM_TYPE_ONLY_TEXT) ||
            ($itemType == self::ITEM_TYPE_ICON_AND_TEXT) ||
            ($itemType == self::ITEM_TYPE_IMAGE_AND_TEXT));
    }

    /**
     * Data formatting functions.
     */

    public function formatCategoryDataAndReturnIfAreValid($categoryData)
    {
        $categoryData = $this->convertToFlexibleContainer($categoryData);
        $categoryDataCopy = clone $categoryData;

        $categoryDataCopy->categoryId = (int)$categoryData->categoryId ? : null;
        $categoryDataCopy->categoryIsPublished = (int)$categoryData->categoryIsPublished ? 1 : 0;
        $categoryDataCopy->categoryTitle = trim((string)$categoryData->categoryTitle);

        if (
            (($categoryDataCopy->categoryId !== null) && ($categoryDataCopy->categoryId <= 0)) ||
            ($categoryDataCopy->categoryTitle == '')
        )
        {
            return false;
        }

        return $categoryDataCopy;
    }

    public function formatItemDataAndReturnIfAreValid($itemData)
    {
        $itemData = $this->convertToFlexibleContainer($itemData);
        $itemDataCopy = clone $itemData;

        $itemDataCopy->itemId = (int)$itemData->itemId ? : null;
        $itemDataCopy->itemParentId = (int)$itemData->itemParentId ? : null;
        $itemDataCopy->itemDepth = 0; // It will be calculated later.
        $itemDataCopy->itemCategoryId = (int)$itemData->itemCategoryId;
        $itemDataCopy->itemPageId = (int)$itemData->itemPageId ? : null;
        $itemDataCopy->itemOrder = ((int)$itemDataCopy->itemOrder > 0) ? : 0;
        $itemDataCopy->itemIsPublished = (int)$itemDataCopy->itemIsPublished ? 1 : 0;
        $itemDataCopy->itemType = (int)$itemData->itemType;
        $itemDataCopy->itemImages = $itemData->itemImages;
        if ($itemDataCopy->itemImages instanceof \Site\Custom\FlexibleContainer)
        {
            $itemDataCopy->itemImages = $itemDataCopy->itemImages->toArray(true);
        }
        $itemDataCopy->itemUrlAddressType = (int)$itemData->itemUrlAddressType;
        $itemDataCopy->itemExternalUrl = trim((string)$itemData->itemExternalUrl);
        $itemDataCopy->itemTextIcon = mb_strtolower(trim((string)$itemData->itemTextIcon));
        $itemDataCopy->itemTitle = trim((string)$itemData->itemTitle);

        $itemImages = $itemDataCopy->itemImages;
        $itemImage = $itemImages ? current($itemImages) : null;

        /**
         * Temporary data - from add-form. They will be parsed and deleted.
         */

        $itemDataCopy->itemUniqueHashForFileName = trim((string)$itemData->itemUniqueHashForFileName);

        /**
         * Validating form data.
         */

        if (
            // Validate unique hash for file name only if it is not empty!
            ($itemDataCopy->itemUniqueHashForFileName != '') &&
            !preg_match('/^([a-z0-9]{40})$/i', $itemDataCopy->itemUniqueHashForFileName)
        )
        {
            return false;
        }

        /**
         * Identifying the author.
         */

        $itemDataCopy->itemLastUpdateAuthorId = $this->user->user_id;

        /**
         * Calculating item depth.
         */
        
        $itemDataCopy->itemDepth = $this->calculateItemDepthByParentId($itemData->itemParentId);

        /**
         * Parsing unique hash for file name.
         */

        if ($itemDataCopy->itemImages)
        {
            $attachmentManager = $this->getAttachmentManager();
            //$itemImageNumber = 0;
            $itemImages = array();
            foreach ($itemDataCopy->itemImages as $itemImageNumber => $itemImage)
            {
                if ($itemImage['uniqueHashForFileName'] != '')
                {
                    // Validate unique hash for file name:
                    if (!preg_match('/^([a-z0-9]{40})$/i', $itemImage['uniqueHashForFileName']))
                    {
                        return false;
                    }

                    $attachment = $attachmentManager->getAttachmentByUniqueHashForFileName(
                        $itemImage['uniqueHashForFileName']
                    );
                    if (!$attachment)
                    {
                        return false;
                    }

                    $itemImages[$itemImageNumber] = array(
                        'imageId' => $itemImage['imageId'],
                        'imageIsMain' => isset($itemImage['isMain']) ? (int)$itemImage['isMain'] : 0,
                        'imageUniqueHashForFileName' => $itemImage['uniqueHashForFileName'],
                        'imageAttachmentId' => $attachment->attachment_id
                    );

                    $itemDataCopy->itemAttachmentId = $attachment->attachment_id;

                    break;
                }

                //$itemImageNumber++;
            }

            $itemDataCopy->itemImages = $itemImages;
        }

        /**
         * Removing unnecessary data.
         */
        
        unset($itemDataCopy->itemUniqueHashForFileName);

        /**
         * Adding "http://".
         */

        if (
            ($itemDataCopy->itemExternalUrl != '') &&
            (mb_strpos($itemDataCopy->itemExternalUrl, 'http://') === false) &&
            (mb_strpos($itemDataCopy->itemExternalUrl, 'https://') === false)
        )
        {
            $itemDataCopy->itemExternalUrl = 'http://' . $itemDataCopy->itemExternalUrl;
        }

        $isExternalUrlInvalid =
            ($itemDataCopy->itemUrlAddressType == self::ITEM_URL_ADDRESS_TYPE_EXTERNAL) &&
            !$this->isItemExternalUrlValid($itemDataCopy->itemExternalUrl);

        if (
            (($itemDataCopy->itemId !== null) && ($itemDataCopy->itemId <= 0)) ||
            (($itemDataCopy->itemParentId !== null) && ($itemDataCopy->itemParentId <= 0)) ||
            ($itemDataCopy->itemCategoryId <= 0) ||
            (($itemDataCopy->itemPageId !== null) && ($itemDataCopy->itemPageId <= 0)) ||
            !$this->isItemTypeValid($itemDataCopy->itemType) ||
            ($this->itemContainsIcon($itemDataCopy->itemType) && ($itemDataCopy->itemTextIcon == '')) ||
            ($this->itemContainsText($itemDataCopy->itemType) && ($itemDataCopy->itemTitle == '')) ||
            (($itemDataCopy->itemAttachmentId !== null) && ($itemDataCopy->itemAttachmentId <= 0)) ||
            !$this->isItemUrlAddressTypeValid($itemDataCopy->itemUrlAddressType) ||
            $isExternalUrlInvalid
        )
        {
            return false;
        }

        return $itemDataCopy;
    }

    /**
     * Logic-functions.
     */

    public function addDefaultCategoryIfNoCategoryExists()
    {
        $categoryCount = $this->getCategoryCount();
        if ($categoryCount > 0)
        {
            return true;
        }

        if ($categoryCount === false) // It is impossible to get category count because of database error.
        {
            return false;
        }

        $allOperationsWasSuccessful = false;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $translate = $this->getTranslateHelper();

        $insert = $sql->insert(self::BLOCK_NAVIGATION_MENU_CATEGORIES_TABLE_NAME);
        $insert->columns(
            array(
                'content_id',
                'is_published',
                'last_update_author_id',
                'last_update_date',
                'title',
            )
        )
            ->values(
                array(
                    'content_id' => $this->getContentId(),
                    'is_published' => 1,
                    'last_update_author_id' => null,
                    'last_update_date' => $lastUpdateDate,
                    'title' => $translate('Default category', 'default', $this->userLanguage->zend2_locale)
                )
            );
        $categoryId = $dbManager->executeInsertAndGetLastInsertId($insert);
        if ($categoryId > 0)
        {
            $settingsSaved = $this->selectCategoryAsDefault($categoryId);
            if ($settingsSaved)
            {
                $allOperationsWasSuccessful = true;
            }
        }

        if ($allOperationsWasSuccessful)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $allOperationsWasSuccessful;
    }

    public function addOrEditCategory($categoryData)
    {
        $categoryDataCopy = $this->formatCategoryDataAndReturnIfAreValid($categoryData);
        if (!$categoryDataCopy)
        {
            return false;
        }

        $categoryDataCopy->categoryLastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        if ($categoryData->categoryId > 0)
        {
            return $this->updateExistingCategory($categoryDataCopy);
        }
        else
        {
            return $this->createNewCategory($categoryDataCopy);
        }
    }

    public function addOrEditItem($itemData)
    {
        $itemDataCopy = $this->formatItemDataAndReturnIfAreValid($itemData);
        if (!$itemDataCopy)
        {
            return false;
        }

        $itemDataCopy->itemLastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        if ($itemData->itemId > 0)
        {
            return $this->updateExistingItem($itemDataCopy);
        }
        else
        {
            return $this->createNewItem($itemDataCopy);
        }
    }

    public function decideWhatToDoWithRecentlyUploadedAndMovedFiles(\Zend\Http\Request $request, $movedFiles)
    {
        //$postData = $request->getPost();

        // Inform the UploadController and UploadManager than upload was successful and you accept these files:
        return true;
    }

    public function deleteAllCategories()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_NAVIGATION_MENU_CATEGORIES_TABLE_NAME);
        $delete->where(array('content_id' => $this->getContentId()));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;
    }

    public function deleteAllItems()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_NAVIGATION_MENU_ITEMS_TABLE_NAME);
        $delete->where(array('content_id' => $this->getContentId()));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;
    }

    public function deleteCategoryById($categoryId)
    {
        $categoryId = (int)$categoryId;

        if ($categoryId <= 0)
        {
            return false;
        }

        $deleted = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();

        $delete = $sql->delete(self::BLOCK_NAVIGATION_MENU_CATEGORIES_TABLE_NAME);
        $delete
            ->where(
                array(
                    'category_id' => $categoryId,
                    'content_id' => $contentId
                )
            );
        $result = $dbManager->executePreparedStatement($delete);
        if ($result)
        {
            $deleted = true;
        }

        return $deleted;
    }

    public function deleteItemById($itemId)
    {
        $itemId = (int)$itemId;

        if ($itemId <= 0)
        {
            return false;
        }

        $deleted = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();

        $delete = $sql->delete(self::BLOCK_NAVIGATION_MENU_ITEMS_TABLE_NAME);
        $delete
            ->where(
                array(
                    'item_id' => $itemId,
                    'content_id' => $contentId
                )
            );
        $result = $dbManager->executePreparedStatement($delete);
        if ($result)
        {
            $deleted = true;
        }

        return $deleted;
    }

    public function reorderItems($parentItemId, $items)
    {
        $parentItemId = $parentItemId ? (int)$parentItemId : null;
        $items = (array)$items;

        if (!$items)
        {
            return false;
        }

        $reordered = true;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $order = 0;
        $contentId = $this->getContentId();

        $dbManager->beginTransaction();

        foreach ($items as $itemId)
        {
            $itemId = (int)$itemId;

            if ($itemId > 0)
            {
                $update = $sql->update(self::BLOCK_NAVIGATION_MENU_ITEMS_TABLE_NAME);
                $update
                    ->set(array('order' => $order))
                    ->where(
                        array(
                            'item_id' => $itemId,
                            'parent_id' => $parentItemId,
                            'content_id' => $contentId
                        )
                    );
                $result = $dbManager->executePreparedStatement($update);
                if (!$result)
                {
                    $reordered = false;
                    break;
                }

                $item = $this->getItemById($itemId);
                if ($item->order != $order)
                {
                    $reordered = false;
                    break;
                }

                $order++;
            }
        }

        if ($reordered)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $reordered;
    }

    public function selectCategoryAsDefault($categoryId)
    {
        $pageBlockId = $this->blockData->page_block_id;
        $categoryId = (int)$categoryId;

        if ($categoryId <= 0)
        {
            return false;
        }

        $blockManager = $this->getBlockManager();
        $settings = $this->getSettings();
        $settings['categoryId'] = $categoryId;
        $settingsSaved = $blockManager->savePageBlockSettings($pageBlockId, $settings);

        return $settingsSaved;
    }

    public function toggleCategoryPublished($categoryId, $published)
    {
        $categoryId = (int)$categoryId;
        $published = (int)$published ? 1 : 0;

        if ($categoryId <= 0)
        {
            return false;
        }

        $updated = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $category = $this->getCategoryById($categoryId);
        if ($category)
        {
            $update = $sql->update(self::BLOCK_NAVIGATION_MENU_CATEGORIES_TABLE_NAME);
            $update
                ->set(
                    array(
                        'is_published' => $published
                    )
                )
                ->where(
                    array(
                        'category_id' => $categoryId,
                        'content_id' => $this->getContentId()
                    )
                );
            $result = $dbManager->executePreparedStatement($update);
            if ($result)
            {
                $category = $this->getCategoryById($categoryId);
                if ($category && ($category->is_published == $published))
                {
                    $updated = true;
                }
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function toggleItemPublished($itemId, $published)
    {
        $itemId = (int)$itemId;
        $published = (int)$published ? 1 : 0;

        if ($itemId <= 0)
        {
            return false;
        }

        $updated = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $item = $this->getItemById($itemId);
        if ($item)
        {
            $update = $sql->update(self::BLOCK_NAVIGATION_MENU_ITEMS_TABLE_NAME);
            $update
                ->set(
                    array(
                        'is_published' => $published,
                        'content_id' => $this->getContentId()
                    )
                )
                ->where(array('item_id' => $itemId));
            $result = $dbManager->executePreparedStatement($update);
            if ($result)
            {
                $item = $this->getItemById($itemId);
                if ($item && ($item->is_published == $published))
                {
                    $updated = true;
                }
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    protected function getGlyphiconOptionsHtmlForDropdown($selectedGlyphicon = '')
    {
        $fileSystem = $this->getFileSystem();
        $filePath = 'module/Site/data/glyphicons.txt';

        if (!$fileSystem->isFile($filePath))
        {
            return false;
        }

        $glyphicons = null;
        $fileContents = str_replace(array("\r\n"), "\n", @file_get_contents($filePath));
        $fileContents = str_replace(array("\r"), "\n", $fileContents);
        if ($fileContents != '')
        {
            $glyphicons = explode("\n", $fileContents);
        }

        if (!$glyphicons)
        {
            return '';
        }

        sort($glyphicons);

        $options = '';
        foreach ($glyphicons as $icon)
        {
            $selected = ($icon == $selectedGlyphicon) ? ' selected' : '';

            $options .= '<option data-subtext="&lt;i class=\'fa fa-' . $icon . '\'&gt;&lt;/i&gt;"' . $selected . '>' . $icon . '</option>';
        }

        return $options;
    }

    protected function makeItemTreeFromResultSetAndOptionalPageCleanUrls(
        $resultSet, $pageCleanUrl = null, $subPageCleanUrl = null
    )
    {
        $itemTree = new \Site\Custom\FlexibleContainer();

        if (!$resultSet)
        {
            return $itemTree;
        }

        $pageManager = $this->getPageManager();

        if ($pageManager->getPageIdByCleanUrls($pageCleanUrl, $subPageCleanUrl))
        {
            $currentPageId = $pageManager->getPageIdByCleanUrls($pageCleanUrl, $subPageCleanUrl)->page_id;
            if ($subPageCleanUrl == '')
            {
                $currentPageId = $pageManager->getPageIdByParentPageCleanUrl($pageCleanUrl);
            }
        }
        else
        {
            $currentPageId = $pageManager->getPageIdByParentPageCleanUrl($pageCleanUrl);
        }

        $currentPageCleanUrl = $pageCleanUrl;
        if ($subPageCleanUrl != '')
        {
            $currentPageCleanUrl = $subPageCleanUrl;
        }

        // We need three copies of items, because multiple traversing on the same object will not work as expected.
        // We have to convert object into array and create three copies for traversing on different arrays instead on
        // one object.
        $items = array();
        foreach ($resultSet as $item)
        {
            $items[] = $item;
        }
        $items2 = $items;
        $items3 = $items;
        // $resultSet is not needed anymore.
        unset($resultSet);

        $atLeastOneElementIsUncollapsed = false;

        foreach ($items as $item)
        {
            if (!$item->parent_id)
            {
                $thereExistsAnActiveItem = false;
                $itemId = (int)$item->item_id;
                $item->image_url = '';

                $attachment = $this->attachmentManager->getAttachmentById($item->attachment_id);
                if ($attachment)
                {
                    $item->image_url = '/eng/get-file/index,' . $attachment->hash_for_file_name . ',' . $attachment->original_file_name;
                }

                if ($item->url_address_type == self::ITEM_URL_ADDRESS_TYPE_INTERNAL)
                {
                    $item->full_url = $pageManager->getFullPageCleanUrlByPageId($item->page_id);
                }
                else
                {
                    $item->full_url = $item->external_url;
                }

                // Second depth:
                $depthOneActiveClassNotFound= true;
                $childItems = new \Site\Custom\FlexibleContainer();
                foreach ($items2 as $itemFromDepth1)
                {
                    $itemFromDepth1Id = (int)$itemFromDepth1->item_id;
                    $itemFromDepth1ParentId = (int)$itemFromDepth1->parent_id;

                    if ($itemFromDepth1ParentId == $itemId)
                    {
                        $itemFromDepth1->image_url = '';

                        $attachment = $this->attachmentManager->getAttachmentById($itemFromDepth1->attachment_id);
                        if ($attachment)
                        {
                            $itemFromDepth1->image_url = '/eng/get-file/index,' . $attachment->hash_for_file_name . ',' . $attachment->original_file_name;
                        }

                        if ($itemFromDepth1->url_address_type == self::ITEM_URL_ADDRESS_TYPE_INTERNAL)
                        {
                            $itemFromDepth1->full_url = $pageManager->getFullPageCleanUrlByPageId(
                                $itemFromDepth1->page_id
                            );
                        }
                        else
                        {
                            $itemFromDepth1->full_url = $itemFromDepth1->external_url;
                        }

                        // Third (last) depth:
                        $itemFromDepth1Items = new \Site\Custom\FlexibleContainer();
                        foreach ($items3 as $itemFromDepth2)
                        {
                            $itemFromDepth2Id = (int)$itemFromDepth2->item_id;
                            $itemFromDepth2ParentId = (int)$itemFromDepth2->parent_id;

                            if ($itemFromDepth2ParentId == $itemFromDepth1Id)
                            {
                                $itemFromDepth2->image_url = '';

                                $attachment = $this->attachmentManager->getAttachmentById($itemFromDepth2->attachment_id);
                                if ($attachment)
                                {
                                    $itemFromDepth2->image_url = '/eng/get-file/index,' . $attachment->hash_for_file_name . ',' . $attachment->original_file_name;
                                }

                                if ($itemFromDepth2->url_address_type == self::ITEM_URL_ADDRESS_TYPE_INTERNAL)
                                {
                                    $itemFromDepth2->full_url = $pageManager->getFullPageCleanUrlByPageId(
                                        $itemFromDepth2->page_id
                                    );
                                }
                                else
                                {
                                    $itemFromDepth2->full_url = $itemFromDepth2->external_url;
                                }

                                $itemFromDepth2->additionalCssClasses = '';
                                $itemFromDepth2->isUncollapsed = false;
                                if (
                                    (
                                        ($itemFromDepth2->url_address_type == self::ITEM_URL_ADDRESS_TYPE_INTERNAL) &&
                                        ($itemFromDepth2->page_id == $currentPageId)
                                    ) ||
                                    (
                                        ($itemFromDepth2->url_address_type == self::ITEM_URL_ADDRESS_TYPE_EXTERNAL) &&
                                        ($itemFromDepth2->external_url == $currentPageCleanUrl)
                                    )
                                )
                                {
                                    $itemFromDepth2->additionalCssClasses = ' active';
                                    $itemFromDepth2->isUncollapsed = true;
                                    $thereExistsAnActiveItem = true;
                                    $atLeastOneElementIsUncollapsed = true;
                                }

                                $itemFromDepth1Items[$itemFromDepth2Id] = $itemFromDepth2;
                            }
                        }
                        $itemFromDepth1->additionalCssClasses = '';
                        $itemFromDepth1->isUncollapsed = false;
                        if (
                            (
                                ($itemFromDepth1->url_address_type == self::ITEM_URL_ADDRESS_TYPE_INTERNAL) &&
                                ($itemFromDepth1->page_id == $currentPageId)
                            ) ||
                            (
                                ($itemFromDepth1->url_address_type == self::ITEM_URL_ADDRESS_TYPE_EXTERNAL) &&
                                ($itemFromDepth1->external_url == $currentPageCleanUrl)
                              ||
                                 $thereExistsAnActiveItem && $depthOneActiveClassNotFound
                            )
                        )
                        {

                            $itemFromDepth1->additionalCssClasses = ' active';
                            $itemFromDepth1->isUncollapsed = true;
                            $atLeastOneElementIsUncollapsed = true;
                            $thereExistsAnActiveItem = true;

                            $depthOneActiveClassNotFound = false;
                        }

                        if ($itemFromDepth1Items->count() > 0)
                        {
                            $itemFromDepth1->additionalCssClasses .= ' has-childs';
                        }

                        $childItems[$itemFromDepth1Id] = array(
                            'data' => $itemFromDepth1,
                            'childItems' => $itemFromDepth1Items
                        );
                    }
                }

                $item->additionalCssClasses = '';
                $item->isUncollapsed = false;
                if (
                    (
                        ($item->url_address_type == self::ITEM_URL_ADDRESS_TYPE_INTERNAL) &&
                        ($item->page_id == $currentPageId)
                    ) ||
                    (
                        ($item->url_address_type == self::ITEM_URL_ADDRESS_TYPE_EXTERNAL) &&
                        ($item->external_url == $currentPageCleanUrl)
                    )
                    || $thereExistsAnActiveItem
                )
                {
                    $item->additionalCssClasses = ' active';

                    $item->isUncollapsed = true;
                    $atLeastOneElementIsUncollapsed = true;
                }

                if ($childItems->count() > 0)
                {
                    $item->additionalCssClasses .= ' has-childs';
                }

                $itemTree[$itemId] = array(
                    'data' => $item,
                    'childItems' => $childItems
                );
            }
        }

        if (!$atLeastOneElementIsUncollapsed)
        {
            $firstItem = current($items);
            if ($firstItem)
            {
                $firstItem->isUncollapsed = true;
            }
        }

        return $itemTree;
    }

    private function createNewCategory($categoryData)
    {
        $created = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $insert = $sql->insert(self::BLOCK_NAVIGATION_MENU_CATEGORIES_TABLE_NAME);
        $insert
            ->columns(
                array(
                    'content_id',
                    'is_published',
                    'last_update_author_id',
                    'last_update_date',
                    'title'
                )
            )
            ->values(
                array(
                    'content_id' => $contentId,
                    'is_published' => $categoryData->categoryIsPublished,
                    'last_update_author_id' => $categoryData->categoryLastUpdateAuthorId,
                    'last_update_date' => $categoryData->categoryLastUpdateDate,
                    'title' => $categoryData->categoryTitle
                )
            );
        $categoryId = $dbManager->executeInsertAndGetLastInsertId($insert);
        if ($categoryId > 0)
        {
            $created = true;
        }

        if ($created)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $created;
    }

    private function updateExistingCategory($categoryData)
    {
        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_NAVIGATION_MENU_CATEGORIES_TABLE_NAME);
        $update->set(
            array(
                'is_published' => $categoryData->categoryIsPublished,
                'last_update_author_id' => $categoryData->categoryLastUpdateAuthorId,
                'last_update_date' => $categoryData->categoryLastUpdateDate,
                'title' => $categoryData->categoryTitle
            )
        )
            ->where(
                array(
                    'category_id' => $categoryData->categoryId,
                    'content_id' => $contentId
                )
            );
        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $category = $this->getCategoryById($categoryData->categoryId);
            if ($category &&
                ($category->is_published == $categoryData->categoryIsPublished) &&
                ($category->last_update_author_id == $categoryData->categoryLastUpdateAuthorId) &&
                ($category->title == $categoryData->categoryTitle)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    private function createNewItem($itemData)
    {
        $created = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $insert = $sql->insert(self::BLOCK_NAVIGATION_MENU_ITEMS_TABLE_NAME);
        $insert
            ->columns(
                array(
                    'parent_id',
                    'depth',
                    'content_id',
                    'category_id',
                    'page_id',
                    'is_published',
                    'url_address_type',
                    'type',
                    'attachment_id',
                    'last_update_author_id',
                    'last_update_date',
                    'bootstrap_glyphicon',
                    'image_alt',
                    'external_url',
                    'title',
                )
            )
            ->values(
                array(
                    'parent_id' => $itemData->itemParentId,
                    'depth' => $itemData->itemDepth,
                    'content_id' => $contentId,
                    'category_id' => $itemData->itemCategoryId,
                    'page_id' => $itemData->itemPageId,
                    'is_published' => $itemData->itemIsPublished,
                    'url_address_type' => $itemData->itemUrlAddressType,
                    'type' => $itemData->itemType,
                    'attachment_id' => $itemData->itemAttachmentId,
                    'last_update_author_id' => $itemData->itemLastUpdateAuthorId,
                    'last_update_date' => $itemData->itemLastUpdateDate,
                    'bootstrap_glyphicon' => $itemData->itemTextIcon,
                    'image_alt' => '',
                    'external_url' => $itemData->itemExternalUrl,
                    'title' => $itemData->itemTitle
                )
            );
        $itemId = $dbManager->executeInsertAndGetLastInsertId($insert);
        if ($itemId > 0)
        {
            $created = true;
        }

        if ($created)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $created;
    }

    private function updateExistingItem($itemData)
    {
        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_NAVIGATION_MENU_ITEMS_TABLE_NAME);
        $update
            ->set(
                array(
                    'parent_id' => $itemData->itemParentId,
                    'depth' => $itemData->itemDepth,
                    'category_id' => $itemData->itemCategoryId,
                    'page_id' => $itemData->itemPageId,
                    'is_published' => $itemData->itemIsPublished,
                    'url_address_type' => $itemData->itemUrlAddressType,
                    'type' => $itemData->itemType,
                    'attachment_id' => $itemData->itemAttachmentId,
                    'last_update_author_id' => $itemData->itemLastUpdateAuthorId,
                    'last_update_date' => $itemData->itemLastUpdateDate,
                    'bootstrap_glyphicon' => $itemData->itemTextIcon,
                    'image_alt' => '',
                    'external_url' => $itemData->itemExternalUrl,
                    'title' => $itemData->itemTitle
                )
            )
            ->where(
                array(
                    'item_id' => $itemData->itemId,
                    'content_id' => $contentId
                )
            );

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $item = $this->getItemById($itemData->itemId);
            if ($item &&
                ($item->parent_id == $itemData->itemParentId) &&
                ($item->depth == $itemData->itemDepth) &&
                ($item->category_id == $itemData->itemCategoryId) &&
                ($item->page_id == $itemData->itemPageId) &&
                ($item->is_published == $itemData->itemIsPublished) &&
                ($item->url_address_type == $itemData->itemUrlAddressType) &&
                ($item->type == $itemData->itemType) &&
                ($item->attachment_id == $itemData->itemAttachmentId) &&
                ($item->last_update_author_id == $itemData->itemLastUpdateAuthorId) &&
                ($item->bootstrap_glyphicon == $itemData->itemTextIcon) &&
                ($item->image_alt == '') &&
                ($item->external_url == $itemData->itemExternalUrl) &&
                ($item->title == $itemData->itemTitle)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }
}
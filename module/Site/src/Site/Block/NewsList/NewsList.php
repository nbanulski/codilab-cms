<?php
namespace Site\Block\NewsList;

use Site\Block\AbstractBlock;
use Zend\Http\Request;

class NewsList extends AbstractBlock
{
    const BLOCK_NEWS_LIST_TABLE_NAME = 'block_news_list';
    const BLOCK_NEWS_LIST_CATEGORIES_TABLE_NAME = 'block_news_list_categories';
    const BLOCK_NEWS_LIST_NEWS_IMAGES_TABLE_NAME = 'block_news_list_news_images';

    protected $hasOwnSettings = true;
    protected $hasManagementWindow = true;

    /**
     * News.
     */

    public function onAfterInserting($slotId)
    {
        return $this->setDefaultDetailsPageIfNoOneWasSetYet();
    }

    public function onBeforeDeleted()
    {
        if (!$this->blockData->is_dynamic)
        {
            $this->deleteAllImages();
            $this->deleteAllNews();
            $this->deleteAllCategories();
            $this->deleteBlockRecordFromSearchIndex();
        }
    }

    public function updateSearchIndex()
    {
        $allNews = $this->getAllNews();

        if ($allNews)
        {
            $contentId = $this->getContentId();

            foreach ($allNews as $news)
            {
                $newsCleanUrl = $this->generateCleanUrlFromNewsTitle($news->title);
                $elementUrlParametersString =
                    $newsCleanUrl . '-' . $contentId . '-' . $news->news_id;

                $this->updateBlockRecordInSearchIndex(
                    $news->news_id, $news->contents, $elementUrlParametersString, '_search_category_news',
                    $news->news_date
                );
            }
        }
    }

    /**
     * Render-functions.
     */

    public function childRender(Request $request)
    {
        $settings = $this->getSettings();
        list($currentNewsCleanUrl, $currentNewsContentId) = $this->getNewsCleanUrlAndContentIdFromRequest($request);

        $newsListShouldBeRendered = true;
        $shouldBeHiddenIfCurrentUrlLeadsToNewsDetails = $settings->hideIfCurrentUrlLeadsToNewsDetails ? true : false;

        $currentNewsId = null;
        if ($shouldBeHiddenIfCurrentUrlLeadsToNewsDetails)
        {
            // Data needed for displaying news's details are correct:
            if (($currentNewsCleanUrl != '') && ($currentNewsContentId > 0))
            {
                $news = $this->getNewsByNewsCleanUrlAndContentId($currentNewsCleanUrl, $currentNewsContentId);
                if ($news) // News exists.
                {
                    $currentNewsId = $news->news_id;
                    $newsListShouldBeRendered = false;
                }
            }
        }

        $this->blockView->setVariables(
            array(
                'currentNewsId' => $currentNewsId,
                'currentNewsCleanUrl' => $currentNewsCleanUrl,
                'currentNewsContentId' => $currentNewsContentId,
                'currentNewsDataAreValid' => (($currentNewsCleanUrl != '') && ($currentNewsContentId > 0))
            )
        );

        if ($newsListShouldBeRendered)
        {
            $this->renderNewsList($request);
        }
        else
        {
            $this->hideNewsList();
        }
    }

    public function childRenderAddEdit(Request $request)
    {
        if (!$this->addEditView instanceof \Zend\View\Model\ViewModel)
        {
            return false;
        }

        $newsCategoriesOptions = array();

        $categories = $this->getCategories();
        if ($categories)
        {
            foreach ($categories as $category)
            {
                $newsCategoriesOptions[$category->category_id] = $category->name;
            }
        }

        $defaultFormData = array(
            'newsId' => '',
            'newsCategoryId' => '',
            'newsCleanUrl' => '',
            'newsTitle' => '',
            'newsLabel' => '',
            'newsDate' => date('Y-m-d'),
            'newsStartHour' => '',
            'newsImages' => array(
                0 => array(
                    'imageId' => null,
                    'isMain' => true,
                    'attachmentId' => null,
                    'uniqueHashForFileName' => '',
                    'originalFileName' => '',
                )
            ),
            'newsUniqueHashForFileName' => '',
            'newsOriginalFileName' => '',
            'newsTeaser' => '',
            'newsContents' => '',
            'newsIsPublished' => 0,
            'newsIsPromoted' => 0
        );
        $currentNewsFormData = array();

        $newsId = (int)$request->getPost('newsId');
        if ($newsId > 0)
        {
            $newsImagesArray = array(
                0 => array(
                    'imageId' => null,
                    'isMain' => true,
                    'attachmentId' => null,
                    'uniqueHashForFileName' => '',
                    'originalFileName' => '',
                )
            );

            $news = $this->getNewsById($newsId);
            $newsImages = $this->getNewsImagesByNewsId($newsId);
            if ($newsImages)
            {
                $imageNumber = 0;
                foreach ($newsImages as $newsImage)
                {
                    $attachmentId = $newsImage->attachment_id;
                    $attachment = $this->attachmentManager->getAttachmentById($attachmentId);

                    $newsImagesArray[$imageNumber] = new \Site\Custom\FlexibleContainer();
                    $newsImagesArray[$imageNumber]->imageId = $newsImage->image_id;
                    $newsImagesArray[$imageNumber]->isMain = $newsImage->is_main ? true : false;
                    $newsImagesArray[$imageNumber]->attachmentId = $attachmentId;
                    $newsImagesArray[$imageNumber]->uniqueHashForFileName
                        = $attachment ? $attachment->hash_for_file_name : '';
                    $newsImagesArray[$imageNumber]->originalFileName
                        = $attachment ? $attachment->original_file_name : '';

                    $imageNumber++;
                }
            }

            $currentYear = date('Y');
            $startTime = strtotime(str_replace('0000-00-00 00:00:00', $currentYear . '-01-01 00:00:00', $news->news_date));

            $newsDate = date('Y-m-d', $startTime);
            $newsStartHour = date('H:i', $startTime);

            $currentNewsFormData = array(
                'newsId' => $news->news_id,
                'newsCategoryId' => $news->category_id,
                'newsCleanUrl' => $news->clean_url,
                'newsTitle' => $news->title,
                'newsLabel' => $news->label,
                'newsDate' => $newsDate,
                'newsStartHour' => $newsStartHour,
                'newsImages' => $newsImagesArray,
                'newsTeaser' => $news->teaser,
                'newsContents' => $news->contents,
                'newsIsPublished' => $news->is_published,
                'newsIsPromoted' => $news->is_promoted
            );
        }

        $formData = array_merge($defaultFormData, $currentNewsFormData);

        $this->addEditView->setVariables(
            array(
                'newsCategoriesOptions' => $newsCategoriesOptions,
                'formData' => $formData
            )
        );
    }

    public function childRenderManagement(Request $request)
    {
        if (!$this->managementView instanceof \Zend\View\Model\ViewModel)
        {
            return false;
        }

        $pageNumber = $this->managementView->pageNumber;
        $itemCountPerPage = $this->managementView->itemCountPerPage;
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $dbAdapter = $dbManager->getAdapter();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(array('bnl' => self::BLOCK_NEWS_LIST_TABLE_NAME))
            ->join(
                array('bnlc' => self::BLOCK_NEWS_LIST_CATEGORIES_TABLE_NAME),
                'bnlc.category_id = bnl.category_id',
                array('category_name' => 'name'),
                $select::JOIN_LEFT
            )
            ->join(
                array('u' => 'users'),
                'u.user_id = bnl.last_update_author_id',
                array('last_update_author' => 'login'),
                $select::JOIN_LEFT
            )
            ->where(
                array(
                    'bnl.content_id' => $contentId,
                    'bnlc.content_id' => $contentId
                )
            )
            ->order('bnl.is_promoted DESC, bnl.news_date ASC');

        $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        $paginatorAdapter = new \Zend\Paginator\Adapter\DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new \Zend\Paginator\Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($pageNumber);
        $paginator->setItemCountPerPage($itemCountPerPage);

        $this->managementView->setVariables(
            array(
                'newsCategoryCount' => $this->getCategoryCount(),
                'newsPaginator' => $paginator
            )
        );
    }

    public function childRenderSettings()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        /**
         * Language id.
         */
        $select = $sql->select();
        $select
            ->columns(array('language_id'))
            ->from('pages')
            ->where(array('page_id' => $this->blockData->page_id));
        $resultSet = $dbManager->getHydratedResultSet($select);
        $page = $resultSet->current();

        $languageId = (int)$page->language_id;

        /**
         * Available pages.
         */
        $select = $sql->select();
        $select
            ->from('pages')
            ->where(array('language_id' => $languageId))
            ->order(array('title ASC'));
        $resultSet = $dbManager->getHydratedResultSet($select);

        $availablePages = array(
            '' => '------'
        );
        foreach ($resultSet as $page)
        {
            $availablePages[$page->page_id] = $page->title;
        }

        $this->settingsView->setVariables(
            array(
                'availablePages' => $availablePages,
            )
        );
    }

    public function hideNewsList()
    {
        $this->setRenderingEnabled(false);
    }

    public function renderNewsList(Request $request)
    {
        $settings = $this->getSettings();

        $pageNumber = (int)$this->blockView->pageNumber ? : 1;
        $itemCountPerPage = (int)$settings->itemCountPerPage ? : 7;
        $contentId = $this->getContentId();

        $pageManager = $this->getPageManager();
        $dbManager = $this->getDatabaseManager();
        $dbAdapter = $dbManager->getAdapter();
        $sql = $dbManager->getSql();

        $metadata = new \Zend\Db\Metadata\Metadata($dbManager->getAdapter());
        $blockNewsTableColumnsMetadata = $metadata->getColumns(self::BLOCK_NEWS_LIST_TABLE_NAME);
        $maxLengths = array();
        if ($blockNewsTableColumnsMetadata)
        {
            foreach ($blockNewsTableColumnsMetadata as $column)
            {
                $name = $column->getName();

                $maxLengths[$name] = $column->getCharacterMaximumLength();
            }
        }

        $newsDetailsPageUrl = '';
        $detailsPageId = (int)$settings->detailsPageId;
        if ($detailsPageId > 0)
        {
            $newsDetailsPageUrl = $pageManager->getFullPageCleanUrlByPageId($detailsPageId);
        }

        $newsCategoriesOptions = array();
        $categories = $this->getCategories();
        if ($categories)
        {
            foreach ($categories as $category)
            {
                $newsCategoriesOptions[$category->category_id] = $category->name;
            }
        }

        $select = $sql->select();
        $select->from(array('bnl' => self::BLOCK_NEWS_LIST_TABLE_NAME))
            ->join(
                array('bnlc' => self::BLOCK_NEWS_LIST_CATEGORIES_TABLE_NAME),
                'bnlc.category_id = bnl.category_id',
                array('category_name' => 'name'),
                $select::JOIN_LEFT
            )
            ->join(
                array('u' => 'users'),
                'u.user_id = bnl.last_update_author_id',
                array('last_update_author' => 'login'),
                $select::JOIN_LEFT
            )
            ->where(
                array(
                    'bnl.content_id' => $contentId,
                    'bnl.is_published' => 1,
                    'bnlc.content_id' => $contentId,
                    'bnlc.is_published' => 1
                )
            );
        $select->order('bnl.is_promoted DESC, bnl.news_date ASC');

        $resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet(
            new NewsHydrator($this->getTranslateHelper(), $this->attachmentManager, $this),
            new \Site\Custom\FlexibleContainer()
        );
        $paginatorAdapter = new \Zend\Paginator\Adapter\DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new \Zend\Paginator\Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($pageNumber);
        $paginator->setItemCountPerPage($itemCountPerPage);

        $this->blockView->setVariables(
            array(
                'maxLengths' => $maxLengths,
                'newsDetailsPageUrl' => $newsDetailsPageUrl,
                'newsCategoriesOptions' => $newsCategoriesOptions,
                'newsPaginator' => $paginator
            )
        );
    }

    public function renderAddEditCategory(Request $request, $return = true)
    {
        $settings = $this->getSettings();

        $pageNumber = (int)$request->getPost('pageNumber') ? : 1;
        $itemCountPerPage = (int)$request->getPost('itemCountPerPage') ? : 10;
        $includePagesFrom = $request->getPost('includePagesFrom') ? : 'current-language';

        $defaultFormData = array(
            'categoryId' => '',
            'categoryTitle' => '',
            'publishNow' => ''
        );
        $currentCategoryFormData = array();

        $categoryId = (int)$request->getPost('categoryId');
        if ($categoryId > 0)
        {
            $category = $this->getCategoryById($categoryId);

            $currentCategoryFormData = array(
                'categoryId' => $category->category_id,
                'categoryTitle' => $category->name,
                'publishNow' => $category->is_published
            );
        }

        $formData = array_merge($defaultFormData, $currentCategoryFormData);

        $addEditCategoryView = new \Zend\View\Model\ViewModel();
        $addEditCategoryView->setTemplate('blocks/' . $this->blockViewName . '/add-edit-category.phtml');
        $addEditCategoryView->setVariables(
            array(
                'user' => $this->user,
                'userLanguage' => $this->userLanguage,
                'designMode' => $this->designMode,
                'blockData' => $this->blockData,
                'hasOwnSettings' => $this->hasOwnSettings,
                'settings' => $settings,
                'pageNumber' => $pageNumber,
                'itemCountPerPage' => $itemCountPerPage,
                'includePagesFrom' => $includePagesFrom,
                'formData' => $formData
            )
        );
        $addEditCategoryViewHtml = $this->phpRenderer->render($addEditCategoryView);

        if ($return)
        {
            return $addEditCategoryViewHtml;
        }
        else
        {
            echo $addEditCategoryViewHtml;
        }
    }

    public function renderCategoriesManagement(Request $request, $return = true)
    {
        $settings = $this->getSettings();

        $pageNumber = (int)$request->getPost('pageNumber') ? : 1;
        $itemCountPerPage = (int)$request->getPost('itemCountPerPage') ? : 10;
        $includePagesFrom = $request->getPost('includePagesFrom') ? : 'current-language';

        $dbManager = $this->getDatabaseManager();
        $dbAdapter = $dbManager->getAdapter();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();

        $select = $sql->select();
        $select
            ->from(array('bec' => self::BLOCK_NEWS_LIST_CATEGORIES_TABLE_NAME))
            ->join(
                array('u' => 'users'),
                'u.user_id = bec.last_update_author_id',
                array('last_update_author' => 'login'),
                $select::JOIN_LEFT
            )
            ->where(array('bec.content_id' => $contentId))
            ->order('bec.name ASC');

        $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        $paginatorAdapter = new \Zend\Paginator\Adapter\DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new \Zend\Paginator\Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($pageNumber);
        $paginator->setItemCountPerPage($itemCountPerPage);

        $categoriesManagementView = new \Zend\View\Model\ViewModel();
        $categoriesManagementView->setTemplate('blocks/' . $this->blockViewName . '/categories-management.phtml');
        $categoriesManagementView->setVariables(
            array(
                'user' => $this->user,
                'userLanguage' => $this->userLanguage,
                'designMode' => $this->designMode,
                'blockData' => $this->blockData,
                'hasOwnSettings' => $this->hasOwnSettings,
                'settings' => $settings,
                'pageNumber' => $pageNumber,
                'itemCountPerPage' => $itemCountPerPage,
                'includePagesFrom' => $includePagesFrom,
                'newsCategoriesPaginator' => $paginator
            )
        );
        $categoriesManagementViewHtml = $this->phpRenderer->render($categoriesManagementView);

        if ($return)
        {
            return $categoriesManagementViewHtml;
        }
        else
        {
            echo $categoriesManagementViewHtml;
        }
    }

    /**
     * Getters.
     */

    public function getCategories()
    {
        $contentId = $this->getContentId();

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_NEWS_LIST_CATEGORIES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getCategoryById($categoryId)
    {
        $categoryId = (int)$categoryId;

        if ($categoryId <= 0)
        {
            return false;
        }

        $contentId = $this->getContentId();

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_NEWS_LIST_CATEGORIES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'category_id' => (int)$categoryId,
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getCategoryCount()
    {
        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_NEWS_LIST_CATEGORIES_TABLE_NAME)
            ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')), $prefixColumnsWithTable)
            ->where(array('content_id' => $this->getContentId()));
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();
            $count = $row ? $row->count : false;

            return $count;
        }

        return false;
    }

    public function getAllNews()
    {
        $contentId = $this->getContentId();

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_NEWS_LIST_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getNewsById($newsId)
    {
        $newsId = (int)$newsId;

        if ($newsId <= 0)
        {
            return false;
        }

        $contentId = $this->getContentId();

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_NEWS_LIST_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'news_id' => $newsId,
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getNewsByNewsCleanUrlAndContentId($newsCleanUrl, $contentId)
    {
        $newsCleanUrl = trim((string)$newsCleanUrl);
        $contentId = (int)$contentId;

        if (($newsCleanUrl == '') || ($contentId < 0))
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_NEWS_LIST_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $contentId,
                    'clean_url' => $newsCleanUrl
                )
            );

        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return  $resultSet->current();
        }

        return false;
    }

    public function getNewsCleanUrlAndContentIdFromNewsCleanUrlWithContentId($newsCleanUrlWithContentId)
    {
        $parts = explode('-', $newsCleanUrlWithContentId);

        $contentId = array_pop($parts);
        $newsCleanUrl = join('-', $parts);

        return array($newsCleanUrl, $contentId);
    }

    public function getNewsCleanUrlAndContentIdFromRequest(Request $request)
    {
        $parameters = $this->getParametersForCurrentPageFromRequest($request);
        $newsCleanUrlWithContentId = $parameters->key(); // Get name of the first parameter.

        return $this->getNewsCleanUrlAndContentIdFromNewsCleanUrlWithContentId($newsCleanUrlWithContentId);
    }

    public function getNewsImageByNewsIdAndImageId($newsId, $imageId)
    {
        $newsId = (int)$newsId;
        $imageId = (int)$imageId;

        if (($newsId <= 0) || ($imageId <= 0))
        {
            return false;
        }

        $contentId = $this->getContentId();

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_NEWS_LIST_NEWS_IMAGES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'image_id' => $imageId,
                    'news_id' => $newsId,
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getNewsImagesByNewsId($newsId, $excludeMainImage = false)
    {
        $newsId = (int)$newsId;

        if ($newsId <= 0)
        {
            return false;
        }

        $contentId = $this->getContentId();

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_NEWS_LIST_NEWS_IMAGES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'news_id' => $newsId,
                    'content_id' => $contentId
                )
            )
            ->order('order ASC');

        if ($excludeMainImage)
        {
            $select->where->equalTo('is_main', 0);
        }

        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet) // TODO: Use Hydrator instead of this whole section here.
        {
            $attachmentManager = $this->getAttachmentManager();
            $resultSet = $resultSet->buffer();

            foreach ($resultSet as $image)
            {
                $imageUrl = '';

                $attachment = $attachmentManager->getAttachmentById($image->attachment_id);
                if ($attachment)
                {
                    $imageUrl =
                        '/eng/get-file/index,' .
                        $attachment->hash_for_file_name .
                        ',' .
                        $attachment->original_file_name;
                }

                $image->imageUrl = $imageUrl;
            }
        }

        return $resultSet;
    }

    public function getNewsMainImageByNewsId($newsId)
    {
        $newsId = (int)$newsId;

        if ($newsId <= 0)
        {
            return false;
        }

        $contentId = $this->getContentId();

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_NEWS_LIST_NEWS_IMAGES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'news_id' => $newsId,
                    'content_id' => $contentId,
                    'is_main' => 1
                )
            )
            ->limit(1);
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function newsListShouldBeRenderedForCurrentNewsCleanUrlAndContentId(
        $currentNewsCleanUrl, $currentNewsContentId
    )
    {
        $settings = $this->getSettings();
        $shouldBeHiddenIfCurrentUrlLeadsToNewsDetails = $settings->hideIfCurrentUrlLeadsToNewsDetails ? true : false;

        return
            !$shouldBeHiddenIfCurrentUrlLeadsToNewsDetails || // Hiding is disabled...
            // ... or data needed for displaying news's details are incorrect:
            (($currentNewsCleanUrl == '') || ($currentNewsContentId <= 0));
    }

    /**
     * Data formatting functions.
     */

    public function generateCleanUrlFromNewsTitle($newsTitle)
    {
        return $this->generateCleanUrlFromText($newsTitle);
    }

    public function formatNewsDataAndReturnIfAreValid($newsData)
    {
        $newsData = $this->convertToFlexibleContainer($newsData);
        $newsDataCopy = clone $newsData;

        $newsDataCopy->newsId = (int)$newsData->newsId ? : null;
        $newsDataCopy->newsCategoryId = (int)$newsData->newsCategoryId;
        $newsDataCopy->newsAttachmentId = (int)$newsDataCopy->newsAttachmentId ? : null;
        $newsDataCopy->newsIsPublished = (int)$newsDataCopy->newsIsPublished ? 1 : 0;
        $newsDataCopy->newsIsPromoted = (int)$newsDataCopy->newsIsPromoted ? 1 : 0;
        $newsDataCopy->newsDate = trim((string)$newsData->newsDate);
        $newsDataCopy->newsCleanUrl = trim((string)$newsData->newsCleanUrl);
        $newsDataCopy->newsTitle = trim((string)$newsData->newsTitle);
        $newsDataCopy->newsLabel = trim((string)$newsData->newsLabel);
        $newsDataCopy->newsImages = $newsData->newsImages;

        if ($newsDataCopy->newsImages instanceof \Site\Custom\FlexibleContainer)
        {
            $newsDataCopy->newsImages = $newsDataCopy->newsImages->toArray(true);
        }
        $newsDataCopy->newsImagesOrder = $newsData->newsImagesOrder;

        $newsDataCopy->newsTeaser = trim((string)$newsData->newsTeaser);
        $newsDataCopy->newsContents = trim((string)$newsData->newsContents);
        $newsDataCopy->attachmentsToDelete = $newsData->attachmentsToDelete;
        if ($newsDataCopy->attachmentsToDelete instanceof \Site\Custom\FlexibleContainer)
        {
            $newsDataCopy->attachmentsToDelete = $newsDataCopy->attachmentsToDelete->toArray(true);
        }

        $newsImages = $newsDataCopy->newsImages;
        $newsImage = $newsImages ? current($newsImages) : null;

        /**
         * Temporary data - from add-form. They will be parsed and deleted.
         */

        $newsDataCopy->newsStartHour = trim((string)$newsData->newsStartHour);

        /**
         * Validating form data.
         */

        if (!preg_match('/^20([0-9]{2})\-[0-1]{1}[0-9]{1}\-[0-3]{1}[0-9]{1}$/i', $newsDataCopy->newsDate) ||
            !preg_match('/^([0-9]{1})[0-9]{1}\:([0-5]{1})[0-9]{1}$/i', $newsDataCopy->newsStartHour) ||
            !preg_match('/^([a-z0-9]{1,})([a-z0-9\-]+)$/i', $newsDataCopy->newsCleanUrl)
        )
        {
            return false;
        }

        /**
         * Identifying the author.
         */

        $newsDataCopy->newsLastUpdateAuthorId = $this->user->user_id;

        /**
         * Parsing dates.
         */

        $newsDataCopy->newsDate = str_replace(
                '-',
                ':',
                mb_substr($newsDataCopy->newsDate, 0, 10)
            ) . ' ' . $newsDataCopy->newsStartHour . ':00';

        /**
         * Parsing unique hash for file name.
         */

        if ($newsDataCopy->newsImages)
        {
            $attachmentManager = $this->getAttachmentManager();
            //$newsImageNumber = 0;
            $newsImages = array();
            foreach ($newsDataCopy->newsImages as $newsImageNumber => $newsImage)
            {
                if ($newsImage['uniqueHashForFileName'] != '')
                {
                    // Validate unique hash for file name:
                    if (!preg_match('/^([a-z0-9]{40})$/i', $newsImage['uniqueHashForFileName']))
                    {
                        return false;
                    }

                    $attachment = $attachmentManager->getAttachmentByUniqueHashForFileName(
                        $newsImage['uniqueHashForFileName']
                    );
                    if (!$attachment)
                    {
                        return false;
                    }

                    $newsImages[$newsImageNumber] = array(
                        'imageId' => $newsImage['imageId'],
                        'imageIsMain' => isset($newsImage['isMain']) ? (int)$newsImage['isMain'] : 0,
                        'imageUniqueHashForFileName' => $newsImage['uniqueHashForFileName'],
                        'imageAttachmentId' => $attachment->attachment_id
                    );
                }

                //$newsImageNumber++;
            }

            $newsDataCopy->newsImages = $newsImages;
        }

        /**
         * Removing unnecessary data.
         */

        unset($newsDataCopy->newsStartHour);
        unset($newsDataCopy->newsUniqueHashForFileName);

        if (
            (($newsDataCopy->newsId !== null) && ($newsDataCopy->newsId <= 0)) ||
            ($newsDataCopy->newsCategoryId <= 0) ||
            ($newsDataCopy->newsCleanUrl == '') ||
            ($newsDataCopy->newsTitle == '') ||
            ($newsDataCopy->newsTeaser == '') ||
            ($newsDataCopy->newsContents == '')
        )
        {
            return false;
        }

        return $newsDataCopy;
    }

    public function formatNewsImageDataAndReturnIfAreValid($imageData)
    {
        $imageData = $this->convertToFlexibleContainer($imageData);
        $imageDataCopy = clone $imageData;

        $imageDataCopy->imageId = (int)$imageData->imageId ? : null;
        $imageDataCopy->imageIsMain = (int)$imageDataCopy->imageIsMain ? 1 : 0;
        $imageDataCopy->imageAttachmentId = (int)$imageDataCopy->imageUniqueHashForFileName ? : null;

        /**
         * Temporary data - from add-form. They will be parsed and deleted.
         */

        $imageDataCopy->imageUniqueHashForFileName = trim((string)$imageDataCopy->imageUniqueHashForFileName);

        /**
         * Identifying the author.
         */

        $imageDataCopy->imageLastUpdateAuthorId = $this->user->user_id;

        /**
         * Parsing unique hash for file name.
         */

        if ($imageDataCopy->imageUniqueHashForFileName != '')
        {
            // Validate unique hash for file name:
            if (!preg_match('/^([a-z0-9]{40})$/i', $imageDataCopy->imageUniqueHashForFileName))
            {
                return false;
            }

            $attachment = $this->getAttachmentManager()->getAttachmentByUniqueHashForFileName(
                $imageDataCopy->imageUniqueHashForFileName
            );
            if (!$attachment)
            {
                return false;
            }

            $imageDataCopy->imageAttachmentId = $attachment->attachment_id;
        }

        /**
         * Removing unnecessary data.
         */

        unset($imageDataCopy->imageUniqueHashForFileName);

        if (($imageDataCopy->imageId !== null) && ($imageDataCopy->imageId <= 0))
        {
            return false;
        }

        return $imageDataCopy;
    }

    /**
     * Logic-functions.
     */

    public function addDefaultCategoryIfNoCategoryExists()
    {
        $categoryCount = $this->getCategoryCount();
        if ($categoryCount > 0)
        {
            return true;
        }

        if ($categoryCount === false) // It is impossible to get category count because of database error.
        {
            return false;
        }

        $allOperationsWasSuccessful = false;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $translate = $this->getTranslateHelper();

        $insert = $sql->insert(self::BLOCK_NEWS_LIST_CATEGORIES_TABLE_NAME);
        $insert
            ->columns(
                array(
                    'content_id',
                    'is_published',
                    'last_update_author_id',
                    'last_update_date',
                    'name',
                )
            )
            ->values(
                array(
                    'content_id' => $this->getContentId(),
                    'is_published' => 1,
                    'last_update_author_id' => null,
                    'last_update_date' => $lastUpdateDate,
                    'name' => $translate('Default category', 'default', $this->userLanguage->zend2_locale)
                )
            );
        $categoryId = $dbManager->executeInsertAndGetLastInsertId($insert);
        if ($categoryId > 0)
        {
            $settingsSaved = $this->selectCategoryAsDefault($categoryId);
            if ($settingsSaved)
            {
                $allOperationsWasSuccessful = true;
            }
        }

        if ($allOperationsWasSuccessful)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $allOperationsWasSuccessful;
    }

    public function addOrEditNews($newsData)
    {
        $newsDataCopy = $this->formatNewsDataAndReturnIfAreValid($newsData);
        if (!$newsDataCopy)
        {
            return false;
        }

        $newsDataCopy->newsLastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        if ($newsData->newsId > 0)
        {
            return $this->updateExistingNews($newsDataCopy);
        }
        else
        {
            return $this->createNewNews($newsDataCopy);
        }
    }

    public function addOrEditNewsImage($newsId, $imageData)
    {
        $newsId = (int)$newsId;
        if ($newsId <= 0)
        {
            return false;
        }

        $imageDataCopy = $this->formatNewsImageDataAndReturnIfAreValid($imageData);
        if (!$imageDataCopy)
        {
            return false;
        }

        $imageDataCopy->imageLastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        if ($imageDataCopy->imageId > 0)
        {
            return $this->updateExistingNewsImage($newsId, $imageDataCopy);
        }
        else
        {
            return $this->createNewNewsImage($newsId, $imageDataCopy);
        }
    }

    public function createNewOrUpdateExistingNews($formData)
    {
        $newsId = isset($formData['newsId']) ? (int)$formData['newsId'] : null;

        if ($newsId > 0)
        {
            return $this->updateExistingNews($formData);
        }
        else
        {
            return $this->createNewNews($formData);
        }
    }

    public function decideWhatToDoWithRecentlyUploadedAndMovedFiles(\Zend\Http\Request $request, $movedFiles)
    {
        //$postData = $request->getPost();

        // Inform the UploadController and UploadManager than upload was successful and you accept these files:
        return true;
    }

    public function deleteAllCategories()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_NEWS_LIST_CATEGORIES_TABLE_NAME);
        $delete->where(array('content_id' => $this->getContentId()));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;
    }

    public function deleteAllImages()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_NEWS_LIST_NEWS_IMAGES_TABLE_NAME);
        $delete->where(array('content_id' => $this->getContentId()));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;
    }

    public function deleteAllNews()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_NEWS_LIST_TABLE_NAME);
        $delete->where(array('content_id' => $this->getContentId()));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;
    }

    public function deleteCategoryById($categoryId)
    {
        $categoryId = (int)$categoryId;

        if ($categoryId <= 0)
        {
            return false;
        }

        $deleted = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_NEWS_LIST_CATEGORIES_TABLE_NAME);
        $delete
            ->where(
                array(
                    'category_id' => $categoryId,
                    'content_id' => $this->getContentId()
                )
            );
        $result = $dbManager->executePreparedStatement($delete);
        if ($result)
        {
            $deleted = true;
        }

        return $deleted;
    }

    public function deleteNewsById($newsId)
    {
        $newsId = (int)$newsId;

        if ($newsId <= 0)
        {
            return false;
        }

        $deleted = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_NEWS_LIST_TABLE_NAME);
        $delete
            ->where(
                array(
                    'news_id' => $newsId,
                    'content_id' => $contentId
                )
            );
        $result = $dbManager->executePreparedStatement($delete);
        if ($result)
        {
            $deleted = true;
        }

        return $deleted;
    }

    public function deleteNewsImageByNewsIdAndImageId($newsId, $imageId)
    {
        $newsId = (int)$newsId;
        $imageId = (int)$imageId;

        if (($newsId <= 0) || ($imageId <= 0))
        {
            return false;
        }

        $deleted = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_NEWS_LIST_NEWS_IMAGES_TABLE_NAME);
        $delete
            ->where(
                array(
                    'image_id' => $imageId,
                    'news_id' => $newsId,
                    'content_id' => $contentId
                )
            );
        $result = $dbManager->executePreparedStatement($delete);
        if ($result)
        {
            $deleted = true;
        }

        return $deleted;
    }

    public function deleteNewsImagesFromOutsideTheImageIdSet($newsId, $imageIds)
    {
        $newsId = (int)$newsId;
        $imageIds = $this->convertToFlexibleContainer($imageIds);

        if ($newsId <= 0)
        {
            return false;
        }

        $deleted = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_NEWS_LIST_NEWS_IMAGES_TABLE_NAME);
        $delete
            ->where(
                array(
                    'news_id' => $newsId,
                    'content_id' => $contentId
                )
            );

        if ($imageIds->count())
        {
            $delete->where->addPredicate(
                new \Zend\Db\Sql\Predicate\NotIn('image_id', $imageIds->toArray())
            );
        }

        $result = $dbManager->executePreparedStatement($delete);
        if ($result)
        {
            $deleted = true;
        }

        return $deleted;
    }

    public function reorderNewsImages($newsId, $images)
    {
        $newsId = (int)$newsId;
        $images = (array)$images;

        if (($newsId <= 0) || !$images)
        {
            return false;
        }

        $reordered = true;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $order = 0;
        $contentId = $this->getContentId();

        $dbManager->beginTransaction();

        foreach ($images as $imageId)
        {
            $imageId = (int)$imageId;

            if ($imageId > 0)
            {
                $update = $sql->update(self::BLOCK_NEWS_LIST_NEWS_IMAGES_TABLE_NAME);
                $update
                    ->set(array('order' => $order))
                    ->where(
                        array(
                            'image_id' => $imageId,
                            'news_id' => $newsId,
                            'content_id' => $contentId
                        )
                    );
                $result = $dbManager->executePreparedStatement($update);
                if (!$result)
                {
                    $reordered = false;
                    break;
                }

                $image = $this->getNewsImageByNewsIdAndImageId($newsId, $imageId);
                if ($image->order != $order)
                {
                    $reordered = false;
                    break;
                }

                $order++;
            }
        }

        if ($reordered)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $reordered;
    }

    public function selectCategoryAsDefault($categoryId)
    {
        $pageBlockId = $this->blockData->page_block_id;
        $categoryId = (int)$categoryId;

        if ($categoryId <= 0)
        {
            return false;
        }

        $blockManager = $this->getBlockManager();
        $settings = $this->getSettings();
        $settings['categoryId'] = $categoryId;
        $settingsSaved = $blockManager->savePageBlockSettings($pageBlockId, $settings);

        return $settingsSaved;
    }

    public function setDefaultDetailsPageIfNoOneWasSetYet()
    {
        $settings = $this->getSettings();

        if ($settings->detailsPageId > 0)
        {
            return true;
        }

        $pageManager = $this->getPageManager();
        $mainPage = $pageManager->getMainPageForSpecificLanguage();
        if (!$mainPage)
        {
            return false;
        }

        $settings->detailsPageId = $mainPage->page_id; // TODO: Should use other page if no main page currently exists.
        $blockManager = $this->getBlockManager();

        return $blockManager->savePageBlockSettings($this->blockData->page_block_id, $settings);
    }

    public function setNewsImageAsMainByNewsIdAndImageId($newsId, $imageId)
    {
        $newsId = (int)$newsId;
        $imageId = (int)$imageId;

        if (($newsId <= 0) || ($imageId <= 0))
        {
            return false;
        }

        $image = $this->getNewsImageByNewsIdAndImageId($newsId, $imageId);
        if (!$image)
        {
            return false;
        }

        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_NEWS_LIST_NEWS_IMAGES_TABLE_NAME);
        $update
            ->set(
                array(
                    'is_main' => 1
                )
            )
            ->where(
                array(
                    'news_id' => $newsId,
                    'content_id' => $contentId
                )
            )
            ->where->notEqualTo('image_id', $imageId);

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $update = $sql->update(self::BLOCK_NEWS_LIST_NEWS_IMAGES_TABLE_NAME);
            $update
                ->set(
                    array(
                        'is_main' => 0
                    )
                )
                ->where(
                    array(
                        'image_id' => $imageId,
                        'news_id' => $newsId,
                        'content_id' => $contentId
                    )
                );
            $result = $dbManager->executePreparedStatement($update);
            if ($result)
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function toggleCategoryPublished($categoryId, $published)
    {
        $categoryId = (int)$categoryId;
        $published = (int)$published ? 1 : 0;

        if ($categoryId <= 0)
        {
            return false;
        }

        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $category = $this->getCategoryById($categoryId);
        if ($category)
        {
            $update = $sql->update(self::BLOCK_NEWS_LIST_CATEGORIES_TABLE_NAME);
            $update
                ->set(
                    array(
                        'is_published' => $published
                    )
                )
                ->where(
                    array(
                        'category_id' => $categoryId,
                        'content_id' => $contentId
                    )
                );
            $result = $dbManager->executePreparedStatement($update);
            if ($result)
            {
                $category = $this->getCategoryById($categoryId);
                if ($category && ($category->is_published == $published))
                {
                    $updated = true;
                }
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function toggleNewsPublished($newsId, $published)
    {
        $newsId = (int)$newsId;
        $published = (int)$published ? 1 : 0;

        if ($newsId <= 0)
        {
            return false;
        }

        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $news = $this->getNewsById($newsId);
        if ($news)
        {
            $update = $sql->update(self::BLOCK_NEWS_LIST_TABLE_NAME);
            $update
                ->set(
                    array(
                        'is_published' => $published
                    )
                )
                ->where(
                    array(
                        'news_id' => $newsId,
                        'content_id' => $contentId
                    )
                );
            $result = $dbManager->executePreparedStatement($update);
            if ($result)
            {
                $news = $this->getNewsById($newsId);
                if ($news && ($news->is_published == $published))
                {
                    $updated = true;
                }
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    private function createNewNews($newsData)
    {
        $created = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $insert = $sql->insert(self::BLOCK_NEWS_LIST_TABLE_NAME);
        $insert
            ->columns(
                array(
                    'content_id',
                    'category_id',
                    'is_published',
                    'is_promoted',
                    //'attachment_id',
                    'last_update_author_id',
                    'last_update_date',
                    'news_date',
                    'clean_url',
                    'title',
                    'label',
                    'teaser',
                    'contents'
                )
            )
            ->values(
                array(
                    'content_id' => $contentId,
                    'category_id' => $newsData->newsCategoryId,
                    'is_published' => $newsData->newsIsPublished,
                    'is_promoted' => $newsData->newsIsPromoted,
                    //'attachment_id' => $newsData->newsAttachmentId,
                    'last_update_author_id' => $newsData->newsLastUpdateAuthorId,
                    'last_update_date' => $newsData->newsLastUpdateDate,
                    'news_date' => $newsData->newsDate,
                    'clean_url' => $newsData->newsCleanUrl,
                    'title' => $newsData->newsTitle,
                    'label' => $newsData->newsLabel,
                    'teaser' => $newsData->newsTeaser,
                    'contents' => $newsData->newsContents
                )
            );
        $newsId = $dbManager->executeInsertAndGetLastInsertId($insert);
        if ($newsId > 0)
        {
            $created = true;
        }

        if ($created)
        {
            $created = false;
            $newsImagesOk = true;

            // Array with all image hash and id, necessary to set proper images order after they are saved in database:
            $existingImages = array();

            if ($newsData->newsImages)
            {
                $firstImageId = null;
                $atLeastOneImageWasSetAsMain = false;
                foreach ($newsData->newsImages as $imageData)
                {
                    $isMain = $imageData['imageIsMain'];
                    $attachmentId = $imageData['imageAttachmentId'];
                    $uniqueHashForFileName = $imageData['imageUniqueHashForFileName'];

                    if ($attachmentId)
                    {
                        $imageId = (int)$this->addOrEditNewsImage($newsId, $imageData);
                        if ($imageId > 0)
                        {
                            if ($firstImageId === null)
                            {
                                $firstImageId = $imageId;
                            }

                            if ($isMain)
                            {
                                $atLeastOneImageWasSetAsMain = true;
                            }

                            $existingImages[$uniqueHashForFileName] = $imageId;
                        }
                        else
                        {
                            $newsImagesOk = false;

                            break;
                        }
                    }
                }

                $reordered = false;
                if ($newsImagesOk)
                {
                    $imagesOrder = array();
                    foreach ($newsData->newsImagesOrder as $uniqueHashForFileName)
                    {
                        $imageId =
                            isset($existingImages[$uniqueHashForFileName])
                                ? (int)$existingImages[$uniqueHashForFileName]
                                : null;
                        if ($imageId > 0)
                        {
                            $imagesOrder[] = $imageId;
                        }
                    }

                    $reordered = $this->reorderNewsImages($newsId, $imagesOrder);
                }

                if (!$atLeastOneImageWasSetAsMain && $reordered)
                {
                    if (!$this->setNewsImageAsMainByNewsIdAndImageId($newsId, $firstImageId))
                    {
                        $newsImagesOk = false;
                    }
                }
            }

            $created = $newsImagesOk;
        }

        if ($created)
        {


            $newsCleanUrl = $this->generateCleanUrlFromNewsTitle($newsData->newsTitle);
            $elementUrlParametersString =
                $newsCleanUrl . '-' . $contentId . '-' . $newsId;

            $this->updateBlockRecordInSearchIndex(
                $newsId, $newsData->newsContents, $elementUrlParametersString, '_search_category_news',
                $newsData->newsDate
            );

            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        $this->processAttachmentsToDelete($newsId, $newsData->attachmentsToDelete);

        return $created;
    }

    private function createNewNewsImage($newsId, $imageData)
    {
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $insert = $sql->insert(self::BLOCK_NEWS_LIST_NEWS_IMAGES_TABLE_NAME);
        $insert
            ->columns(
                array(
                    'news_id',
                    'content_id',
                    'attachment_id',
                    'is_main',
                    'last_update_author_id',
                    'last_update_date'
                )
            )
            ->values(
                array(
                    'news_id' => $newsId,
                    'content_id' => $contentId,
                    'attachment_id' => $imageData->imageAttachmentId,
                    'is_main' => $imageData->imageIsMain,
                    'last_update_author_id' => $imageData->imageLastUpdateAuthorId,
                    'last_update_date' => $imageData->imageLastUpdateDate
                )
            );
        $imageId = $dbManager->executeInsertAndGetLastInsertId($insert);
        if ($imageId > 0)
        {
            $dbManager->commit();

            return $imageId;
        }

        $dbManager->rollback();

        return false;
    }

    private function updateExistingNews($newsData)
    {
        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_NEWS_LIST_TABLE_NAME);
        $update
            ->set(
                array(
                    'category_id' => $newsData->newsCategoryId,
                    'is_published' => $newsData->newsIsPublished,
                    'is_promoted' => $newsData->newsIsPromoted,
                    //'attachment_id' => $newsData->newsAttachmentId,
                    'last_update_author_id' => $newsData->newsLastUpdateAuthorId,
                    'last_update_date' => $newsData->newsLastUpdateDate,
                    'news_date' => $newsData->newsDate,
                    'clean_url' => $newsData->newsCleanUrl,
                    'title' => $newsData->newsTitle,
                    'label' => $newsData->newsLabel,
                    'teaser' => $newsData->newsTeaser,
                    'contents' => $newsData->newsContents
                )
            )
            ->where(
                array(
                    'news_id' => $newsData->newsId,
                    'content_id' => $contentId
                )
            );

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $news = $this->getNewsById($newsData->newsId);
            if ($news &&
                ($news->category_id == $newsData->newsCategoryId) &&
                ($news->is_published == $newsData->newsIsPublished) &&
                ($news->is_promoted == $newsData->newsIsPromoted) &&
                //($news->attachment_id == $newsData->newsAttachmentId) &&
                ($news->last_update_author_id == $newsData->newsLastUpdateAuthorId) &&
                ($news->clean_url == $newsData->newsCleanUrl) &&
                ($news->title == $newsData->newsTitle) &&
                ($news->label == $newsData->newsLabel) &&
                ($news->teaser == $newsData->newsTeaser) &&
                ($news->contents == $newsData->newsContents)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $updated = false;

            $newsImagesOk = true;

            // Array with all image hash and id, necessary to remove images from database which were removed by user
            // from add-edit dialog window and to set proper images order after they are saved in database:
            $existingImages = array();

            if ($newsData->newsImages)
            {
                $firstImageId = null;
                $atLeastOneImageWasSetAsMain = false;
                foreach ($newsData->newsImages as $imageData)
                {
                    $imageId = $imageData['imageId'];
                    $isMain = $imageData['imageIsMain'] == 1;
                    $uniqueHashForFileName = $imageData['imageUniqueHashForFileName'];

                    $result = $this->addOrEditNewsImage($news->news_id, $imageData);
                    if ($result)
                    {
                        if (!is_bool($result))
                        {
                            $imageId = (int)$result;
                        }

                        if ($firstImageId === null)
                        {
                            $firstImageId = $imageId;
                        }

                        if ($isMain)
                        {
                            $atLeastOneImageWasSetAsMain = true;
                        }

                        $existingImages[$uniqueHashForFileName] = $imageId;
                    }
                    else
                    {
                        $newsImagesOk = false;

                        break;
                    }
                }

                $reordered = false;
                if ($newsImagesOk)
                {
                    $imagesOrder = array();
                    foreach ($newsData->newsImagesOrder as $uniqueHashForFileName)
                    {
                        $imageId =
                            isset($existingImages[$uniqueHashForFileName])
                                ? (int)$existingImages[$uniqueHashForFileName]
                                : null;
                        if ($imageId > 0)
                        {
                            $imagesOrder[] = $imageId;
                        }
                    }

                    $reordered = $this->reorderNewsImages($news->news_id, $imagesOrder);
                }

                if (!$atLeastOneImageWasSetAsMain && $reordered)
                {
                    if (!$this->setNewsImageAsMainByNewsIdAndImageId($news->news_id, $firstImageId))
                    {
                        $newsImagesOk = false;
                    }
                }
            }

            if ($newsImagesOk)
            {
                $deleted = $this->deleteNewsImagesFromOutsideTheImageIdSet($news->news_id, $existingImages);
                if (!$deleted)
                {
                    $newsImagesOk = false;
                }
            }

            if ($newsImagesOk)
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $newsCleanUrl = $this->generateCleanUrlFromNewsTitle($newsData->newsTitle);
            $elementUrlParametersString =
                $newsCleanUrl . '-' . $contentId . '-' . $newsData->newsId;

            $this->updateBlockRecordInSearchIndex(
                $newsData->newsId, $newsData->newsContents, $elementUrlParametersString, '_search_category_news',
                $newsData->newsDate
            );

            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        $this->processAttachmentsToDelete($newsData->newsId, $newsData->attachmentsToDelete);

        return $updated;
    }

    private function updateExistingNewsImage($newsId, $imageData)
    {
        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_NEWS_LIST_NEWS_IMAGES_TABLE_NAME);
        $update
            ->set(
                array(
                    'attachment_id' => $imageData->imageAttachmentId,
                    'is_main' => $imageData->imageIsMain,
                    'last_update_author_id' => $imageData->imageLastUpdateAuthorId,
                    'last_update_date' => $imageData->imageLastUpdateDate
                )
            )
            ->where(
                array(
                    'image_id' => $imageData->imageId,
                    'news_id' => $newsId,
                    'content_id' => $contentId
                )
            );

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $image = $this->getNewsImageByNewsIdAndImageId($newsId, $imageData->imageId);
            if ($image &&
                ($image->attachment_id == $imageData->imageAttachmentId) &&
                ($image->is_main == $imageData->imageIsMain)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function updateExistingGalleryImageTitleByNewsIdAndImageId($newsId, $imageId, $imageTitle)
    {
        $newsId = (int)$newsId;
        $imageId = (int)$imageId;
        $imageTitle = trim((string)$imageTitle);

        if (($newsId <= 0) || ($imageId <= 0))
        {
            return false;
        }

        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_NEWS_LIST_NEWS_IMAGES_TABLE_NAME);
        $update
            ->set(
                array(
                    'title' => $imageTitle
                )
            )
            ->where(
                array(
                    'image_id' => $imageId,
                    'content_id' => $contentId,
                    'news_id' => $newsId
                )
            );
        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $image = $this->getNewsImageByNewsIdAndImageId($newsId, $imageId);
            if ($image && ($image->title == $imageTitle))
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    private function processAttachmentsToDelete($newsId, $attachmentsToDelete)
    {
        $attachmentManager = $this->getAttachmentManager();

        $deleteSuccessful = $attachmentManager->deleteAttachmentsByUniqueHashForFileName($attachmentsToDelete);
        if ($deleteSuccessful)
        {
            /*if ($newsId > 0)
            {
                $update = $sql->update(self::BLOCK_NEWS_LIST_TABLE_NAME);
                $update
                    ->set(
                        array(
                            'attachment_id' => null
                        )
                    )
                    ->where(
                        array(
                            'news_id' => $newsData->newsId,
                            'content_id' => $contentId
                        )
                    );
            }*/
        }

        return $deleteSuccessful;
    }
}
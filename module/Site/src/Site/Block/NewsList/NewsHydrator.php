<?php
namespace Site\Block\NewsList;

use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\I18n\View\Helper\Translate;
use Site\Controller\AttachmentManager;

class NewsHydrator implements HydratorInterface
{
    protected $translateHelper = null;
    protected $attachmentManager = null;
    protected $newsList = null;

    public function __construct(
        Translate $translateHelper, AttachmentManager $attachmentManager, NewsList $newsList
    )
    {
        $this->translateHelper = $translateHelper;
        $this->attachmentManager = $attachmentManager;
        $this->newsList = $newsList;
    }

    public function extract($object) {}

    public function hydrate(array $data, $object)
    {
        foreach ($data as $property => $value)
        {
            $object[$property] = $value;
        }

        $translate = $this->translateHelper;

        $newsDateUnixTimestamp = strtotime($object['news_date']);
        $currentDateUnixTimestamp = time();

        // We should include only days - without hour, minutes, and seconds:
        $newsDateDays = strtotime(date('Y-m-d 00:00:00', $newsDateUnixTimestamp));
        $currentDateDays = strtotime(date('Y-m-d 00:00:00', $currentDateUnixTimestamp));

        $dateDiff = $currentDateDays - $newsDateDays;
        $daysAgo = (int)($dateDiff / 86400); // 60 * 60 * 24.

        if ($daysAgo < -1)
        {
            $daysAgoStr = abs($daysAgo) . ' ' . $translate('days');
        }
        else if ($daysAgo == -1)
        {
            $daysAgoStr = '1 ' . $translate('day');
        }
        else if ($daysAgo == 0)
        {
            $daysAgoStr = $translate('Today');
        }
        else if ($daysAgo == 1)
        {
            $daysAgoStr = $translate('Yesterday');
        }
        else
        {
            $daysAgoStr = $daysAgo . ' ' . $translate('days ago');
        }

        $date = date('Y-m-d', $newsDateUnixTimestamp);
        $timeStart = date('H:i', $newsDateUnixTimestamp);

        $object['daysAgo'] = $daysAgo;
        $object['daysAgoStr'] = $daysAgoStr;
        $object['date'] = $date;
        $object['time_start'] = $timeStart;

        $object['mainImageUrl'] = '';
        $mainImage = $this->newsList->getNewsMainImageByNewsId($object['news_id']);
        if ($mainImage && ($mainImage->attachment_id > 0))
        {
            $attachment = $this->attachmentManager->getAttachmentById($mainImage->attachment_id);
            if ($attachment)
            {
                $object['mainImageUrl'] =
                    '/eng/get-file/index,' .
                    $attachment->hash_for_file_name .
                    ',' .
                    $attachment->original_file_name;
            }
        }

        return $object;
    }
} 
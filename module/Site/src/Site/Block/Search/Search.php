<?php
namespace Site\Block\Search;

use Site\Block\AbstractBlock;
use Zend\Http\Request;

class Search extends AbstractBlock
{
    protected $hasOwnSettings = true;

    public function childRender(Request $request)
    {
        $searchResultsUrl = '';

        $settings = $this->getSettings();
        $searchResultsPageId = (int)$settings->searchResultsPageId ? : false;
        if ($searchResultsPageId > 0)
        {
            $searchResultsUrl = $this->getPageManager()->getFullPageCleanUrlByPageId($searchResultsPageId);
        }

        $this->blockView->setVariables(
            array(
                'searchResultsUrl' => $searchResultsUrl
            )
        );
    }

    public function childRenderSettings()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->columns(array('language_id'))
            ->from('pages')
            ->where(array('page_id' => $this->blockData->page_id));
        $resultSet = $dbManager->getHydratedResultSet($select);
        $page = $resultSet->current();

        $languageId = (int)$page->language_id;

        $select = $sql->select();
        $select
            ->from('pages')
            ->where(array('language_id' => $languageId))
            ->order(array('title ASC'));
        $resultSet = $dbManager->getHydratedResultSet($select);

        $availablePages = array(
            '' => '------'
        );
        foreach ($resultSet as $page)
        {
            $availablePages[$page->page_id] = $page->title;
        }

        $this->settingsView->setVariables(
            array(
                'availablePages' => $availablePages,
            )
        );
    }
}
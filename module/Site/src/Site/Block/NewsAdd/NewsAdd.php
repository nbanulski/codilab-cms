<?php
namespace Site\Block\NewsAdd;

use Site\Block\AbstractBlock;
use Zend\Http\Request;

class NewsAdd extends AbstractBlock
{
    const NEWS_LIST_TABLE_NAME = 'block_news_list';
    const NEWS_LIST_CATEGORIES_TABLE_NAME = 'block_news_list_categories';

    protected $hasOwnSettings = true;

    public function childRender(Request $request)
    {
        $dbManager = $this->getDatabaseManager();

        $metadata = new \Zend\Db\Metadata\Metadata($dbManager->getAdapter());
        $blockNewsTableColumnsMetadata = $metadata->getColumns(self::NEWS_LIST_TABLE_NAME);
        $maxLengths = array();
        if ($blockNewsTableColumnsMetadata)
        {
            foreach ($blockNewsTableColumnsMetadata as $column)
            {
                $name = $column->getName();

                $maxLengths[$name] = $column->getCharacterMaximumLength();
            }
        }

        $newsCategoriesOptions = array();
        $categories = $this->getCategories();
        if ($categories)
        {
            foreach ($categories as $category)
            {
                $newsCategoriesOptions[$category->category_id] = $category->name;
            }
        }

        $this->blockView->setVariables(
            array(
                'maxLengths' => $maxLengths,
                'newsCategoriesOptions' => $newsCategoriesOptions
            )
        );
    }

    public function childRenderSettings()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(array('pb' => 'pages_blocks'))
            ->join(
                array('b' => 'blocks'),
                'b.block_id = pb.block_id',
                array(),
                $select::JOIN_LEFT
            )
            ->join(
                array('bp' => 'blocks_properties'),
                'bp.block_id = b.block_id',
                array('block_translated_name' => 'name'),
                $select::JOIN_LEFT
            )
            ->where(
                array(
                    'b.php_class_name' => 'NewsList',
                    'bp.language_id' => $this->language->language_id
                )
            )
            ->order(array('block_translated_name ASC'));
        $resultSet = $dbManager->getHydratedResultSet($select);

        $availableNewsListBlocks = array();
        foreach ($resultSet as $pageBlock)
        {
            $pageBlockId = $pageBlock->page_block_id;
            $availableNewsListBlocks[$pageBlockId] = '[id: ' . $pageBlockId . '] ' . $pageBlock->block_translated_name;
        }

        if (!$availableNewsListBlocks)
        {
            $availableNewsListBlocks[''] = '-----';
        }

        $this->settingsView->setVariables(
            array(
                'availableNewsListBlocks' => $availableNewsListBlocks,
            )
        );
    }

    public function getCategories()
    {
        $settings = $this->getSettings();

        $pageBlockId = (int)$settings->pageBlockId;
        if ($pageBlockId <= 0)
        {
            return false;
        }

        $blockManager = $this->getBlockManager();
        $contentId = $blockManager->getPageBlockContentIdByPageBlockId($pageBlockId);
        if ($contentId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::NEWS_LIST_CATEGORIES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }
}
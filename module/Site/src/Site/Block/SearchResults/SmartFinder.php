<?php
namespace Site\Block\SearchResults;

class SmartFinder
{
    protected $isHighlightingWordsEnabled = true;
    protected $finderCollection = array();

    /**
     * Getters.
     */

    public function getHighlightingWordsEnabled()
    {
        return $this->isHighlightingWordsEnabled;
    }

    public function setHighlightingWordsEnabled($enabled = true)
    {
        $this->isHighlightingWordsEnabled = (bool)$enabled;

        return $this;
    }

    public function getFinderCount()
    {
        return count($this->finderCollection);
    }

    public function finderExistsInCollection(AbstractFinder $finderInstance)
    {
        return in_array($finderInstance, $this->finderCollection);
    }

    /**
     * Collection management methods.
     */

    public function addFinder(AbstractFinder $finderInstance)
    {
        if (!$this->finderExistsInCollection($finderInstance))
        {
            $this->finderCollection[] = $finderInstance;
        }

        return $this;
    }

    public function removeAllFinders()
    {
        $this->finderCollection = array();

        return $this;
    }

    public function removeFinderByIndex($index)
    {
        $index = (int)$index;

        if (array_key_exists($index, $this->finderCollection))
        {
            unset($this->finderCollection[$index]);

            $this->finderCollection = array_values($this->finderCollection);
        }

        return $this;
    }

    public function removeFinderByInstance(AbstractFinder $finderInstance)
    {
        $key = $this->findFinderInstanceAndReturnCollectionKey($finderInstance);
        if ($key !== false)
        {
            unset($this->finderCollection[$key]);
        }

        return $this;
    }

    /**
     * Search methods.
     */

    public function search($query, $languageId = null)
    {
        $totalResults = array();

        foreach ($this->finderCollection as $finder)
        {
            $finderResults = $finder->search($query, $languageId);
            if ($finderResults)
            {
                $resultsArray = $this->convertFinderResultsToArray($finderResults);
                if ($resultsArray)
                {
                    //$totalResults[] = $resultsArray;
                    $totalResults = array_merge($totalResults, $resultsArray);
                }
            }
        }

        if ($this->isHighlightingWordsEnabled)
        {
            $this->highlightFoundWordsInSearchResultsArray($totalResults, $query);
        }

        /*$resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet(
            new \Zend\ArraySerializableHydrator(),
            new \Site\Custom\FlexibleContainer()
        );*/

        $paginatorAdapter = new \Zend\Paginator\Adapter\ArrayAdapter($totalResults);
        $paginator = new \Zend\Paginator\Paginator($paginatorAdapter);
        //$paginator->setCurrentPageNumber($pageNumber);
        $paginator->setItemCountPerPage(10);

        return $paginator;
    }

    /**
     * Private methods.
     */

    protected function highlightFoundWordsInSearchResultsArray(&$totalResults, &$query)
    {
        foreach ($totalResults as $index => $result)
        {
            $totalResults[$index]['short_description'] =
                $this->highlightFoundWords($result['short_description'], $query);
        }
    }

    protected function highlightFoundWords($textToSearchIn, $words)
    {
        preg_match_all('~\w+~i', $words, $matches);
        if (!$matches)
        {
            return $textToSearchIn;
        }

        $pattern = '~\\b(' . implode('|', $matches[0]) . ')\\b~i';

        return preg_replace($pattern, '<span class="highlighted">$0</span>', $textToSearchIn);
    }

    private function convertFinderResultsToArray($finderResults)
    {
        if (!$finderResults)
        {
            return array();
        }

        $resultsArray = array();

        foreach ($finderResults as $result)
        {
            if ($result instanceof \Site\Custom\FlexibleContainer)
            {
                $resultsArray[] = $result->toArray();
            }
            else
            {
                $resultsArray[] = $result;
            }
        }

        return $resultsArray;
    }

    private function findFinderInstanceAndReturnCollectionKey(AbstractFinder $finderInstance)
    {
        return array_search($finderInstance, $this->finderCollection);
    }
} 
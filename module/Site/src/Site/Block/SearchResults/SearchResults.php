<?php
namespace Site\Block\SearchResults;

use Site\Block\AbstractBlock;
use Zend\Http\Request;

class SearchResults extends AbstractBlock
{
    protected $hasOwnSettings = true;

    /**
     * Render-functions.
     */

    public function childRender(Request $request)
    {
        $settings = $this->getSettings();

        $query = $request->getQuery('query');
        if ($query == null)
        {
            $query = $this->blockView->query;
        }

        $currentPageNumber = (int)$this->blockView->pageNumber ? : 1;
        $itemCountPerPage = (int)$settings->itemCountPerPage ? : 7;

        $cutText = $this->getCutTextHelper();
        $dbManager = $this->getDatabaseManager();
        $pageManager = $this->getPageManager();

        $smartFinder = new SmartFinder();
        $smartFinder->setHighlightingWordsEnabled($settings->highlightFoundWords);
        $smartFinder->addFinder(new PageFinder($cutText, $dbManager, $pageManager));
        $smartFinder->addFinder(new BlockContentFinder($cutText, $dbManager, $pageManager));
        $searchResultsPaginator = $smartFinder->search($query);
        $searchResultsPaginator->setCurrentPageNumber($currentPageNumber);
        $searchResultsPaginator->setItemCountPerPage($itemCountPerPage);

        $searchResultCount = $searchResultsPaginator ? $searchResultsPaginator->count() : 0;

        $this->blockView->setVariables(
            array(
                'query' => $query,
                'searchResultCount' => $searchResultCount,
                'searchResultsPaginator' => $searchResultsPaginator
            )
        );
    }
}
<?php
namespace Site\Block\SearchResults;

class PageFinder extends AbstractFinder
{
    public function search($query, $languageId = null)
    {
        return $this->searchInPagesTable($query, $languageId);
    }

    protected function searchInPagesTable($query, $languageId = null)
    {
        $query = trim(strip_tags((string)$query));
        $languageId = $languageId ? (int)$languageId : null;

        $prefixColumnsWithTable = false;

        $cutText = $this->getCutTextHelper();
        $dbManager = $this->getDatabaseManager();
        $dbAdapter = $dbManager->getAdapter();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->columns(
                array(
                    'page_id',
                    'date' => 'last_update_date',
                    'clean_url',
                    'page_title' => 'title',
                    'short_description' => new \Zend\Db\Sql\Expression('LEFT(meta_description,120)'),
                    'category' => new \Zend\Db\Sql\Expression('?', 'Page')
                ),
                $prefixColumnsWithTable
            )
            ->from(array('p' => 'pages'))
            /*->join(
                array('l' => 'languages'),
                'l.language_id = p.language_id',
                array('iso_code' => 'iso_code'),
                $select::JOIN_LEFT
            )*/
            /*->where(
                array(
                    'p.is_published' => 1,
                )
            )*/
            ->group('p.page_id')
            ->order('p.last_update_date DESC');

        if ($languageId > 0)
        {
            $select->where->equalTo('p.language_id', $languageId);
        }

        //$select->where->like('p.title', '%' . $query . '%');
        //$select->where->like('p.meta_description', '%' . $query . '%');
        //$select->where->literal('p.title REGEXP ?', array($query));
        //$select->where->literal('p.meta_description REGEXP ?', array($query));

        $select
            ->where
            ->nest()
                ->literal('MATCH(p.title) AGAINST(? IN BOOLEAN MODE)', $query)
                ->or
                ->literal('MATCH(p.meta_description) AGAINST(? IN BOOLEAN MODE)', $query)
            ->unnest();

        //var_dump($sql->getSqlStringForSqlObject($select));

        //$resultSet = $dbManager->getHydratedResultSet($select);
        /*$paginator = array();

        $result = $dbManager->executePreparedStatement($select);
        if ($result)
        {
            $resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet(
                new PageFinderHydrator($cutText, $this->getPageManager()),
                new \Site\Custom\FlexibleContainer()
            );

            $paginatorAdapter = new \Zend\Paginator\Adapter\DbSelect($select, $dbAdapter, $resultSetPrototype);
            $paginator = new \Zend\Paginator\Paginator($paginatorAdapter);
            //$paginator->setCurrentPageNumber($pageNumber);
            $paginator->setItemCountPerPage(10);
        }

        return $paginator;*/

        $resultSet = array();

        $result = $dbManager->executePreparedStatement($select);
        if ($result)
        {
            $resultSet = new \Zend\Db\ResultSet\HydratingResultSet(
                new PageFinderHydrator($cutText, $this->getPageManager()),
                new \Site\Custom\FlexibleContainer()
            );
            $resultSet->initialize($result);
        }

        return $resultSet;
    }
} 
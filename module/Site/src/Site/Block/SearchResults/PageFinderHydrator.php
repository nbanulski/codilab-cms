<?php
namespace Site\Block\SearchResults;

use Zend\Stdlib\Hydrator\HydratorInterface;
use Site\View\Helper\CutText;
use Site\Controller\PageManager;

class PageFinderHydrator implements HydratorInterface
{
    protected $cutText = null;
    protected $pageManager = null;

    public function __construct(
        CutText $cutText,
        PageManager $pageManager
    )
    {
        $this->cutText = $cutText;
        $this->pageManager = $pageManager;
    }

    public function extract($object) {}

    public function hydrate(array $data, $object)
    {
        foreach ($data as $property => $value)
        {
            $object[$property] = $value;
        }

        $cutText = $this->cutText;
        $append = ' ...';
        $appendOnlyIfTextIsNotEmpty = true;

        if ($object['short_description'] != '')
        {
            $object['short_description'] = $cutText(
                $object['short_description'], 250, $append, $appendOnlyIfTextIsNotEmpty
            );
        }

        $object['pageUrl'] = $this->pageManager->getFullPageCleanUrlByPageId($object['page_id']);
        $object['pageBreadcrumb'] = $this->pageManager->getPageTextBreadcrumbByPageId($object['page_id']);

        unset($object['page_id']);

        return $object;
    }
} 
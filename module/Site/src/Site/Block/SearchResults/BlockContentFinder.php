<?php
namespace Site\Block\SearchResults;

class BlockContentFinder extends AbstractFinder
{
    public function search($query, $languageId = null)
    {
        return $this->searchInPagesTable($query, $languageId);
    }

    protected function searchInPagesTable($query, $languageId = null)
    {
        $query = trim(strip_tags((string)$query));
        $languageId = $languageId ? (int)$languageId : null;

        $prefixColumnsWithTable = false;

        $cutText = $this->getCutTextHelper();
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        /*

        $select = $sql->select();
        $select
            ->columns(
                array(
                    'short_description' => 'content',
                    'category' => new \Zend\Db\Sql\Expression('?', 'Block')
                ),
                $prefixColumnsWithTable
            )
            ->from(array('pbc' => 'pages_blocks_contents'))
            ->join(
                array('pbca' => 'pages_blocks_contents_association'),
                'pbca.content_id = pbc.content_id',
                array(),
                $select::JOIN_LEFT
            )
            ->join(
                array('pb' => 'pages_blocks'),
                'pb.page_block_id = pbca.page_block_id',
                array(),
                $select::JOIN_LEFT
            )
            ->join(
                array('b' => 'blocks'),
                'b.block_id = pb.block_id',
                array(
                    'php_class_name'
                ),
                $select::JOIN_LEFT
            )
            ->join(
                array('p' => 'pages'),
                'p.page_id = pb.page_id',
                array(
                    'page_id',
                    'date' => 'last_update_date',
                    'clean_url' => 'clean_url',
                    'page_title' => 'title'
                ),
                $select::JOIN_LEFT
            )
            /*->where(
                array(
                    'p.is_published' => 1,
                )
            )
            // Exclude the same content on one page:
            ->group(
                array(
                    'pb.page_id',
                    'pbc.content_id'
                )
            )
            ->order('p.last_update_date DESC');

        if ($languageId > 0)
        {
            $select->where->equalTo('p.language_id', $languageId);
        }

        //$select->where->like('p.title', '%' . $query . '%');
        $select->where->literal('pbc.content REGEXP ?', array($query));

        */

        $select = $sql->select();
        $select
            ->columns(
                array(
                    'page_id',
                    'content_id',
                    'block_element_id',
                    'date',
                    'page_title',
                    'category' => 'translatable_search_result_category',
                    'block_element_url_parameters',
                    'block_element_contents',
                    'block_contents'/*,
                    'score' => new \Zend\Db\Sql\Expression('MATCH(block_element_contents) AGAINST(?)', $query)*/
                ),
                $prefixColumnsWithTable
            )
            ->from('search_index')
            ->where
                ->nest()
                    ->literal('MATCH(page_title) AGAINST(? IN BOOLEAN MODE)', $query)
                    ->or
                    ->literal('MATCH(meta_description) AGAINST(? IN BOOLEAN MODE)', $query)
                    ->or
                    ->literal('MATCH(block_element_contents) AGAINST(? IN BOOLEAN MODE)', $query)
                    ->or
                    ->literal('MATCH(block_contents) AGAINST(? IN BOOLEAN MODE)', $query)
                ->unnest();

        //echo $sql->getSqlStringForSqlObject($select);

        $resultSet = array();

        $result = $dbManager->executePreparedStatement($select);
        if ($result)
        {
            $resultSet = new \Zend\Db\ResultSet\HydratingResultSet(
                new BlockContentFinderHydrator($cutText, $this->getPageManager()),
                new \Site\Custom\FlexibleContainer()
            );
            $resultSet->initialize($result);
        }

        return $resultSet;
    }
} 
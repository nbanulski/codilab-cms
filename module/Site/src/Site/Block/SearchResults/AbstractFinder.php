<?php
namespace Site\Block\SearchResults;

use Site\Controller\AttachmentManager;
use Site\Controller\BlockManager;
use Site\Controller\DatabaseManager;
use Site\Controller\PageManager;
use Site\Custom\FlexibleContainer;
use Site\View\Helper\CutText;
use Zend\View\Renderer\PhpRenderer;

class AbstractFinder
{
    /*protected $blockSettings = null;
    protected $attachmentManager = null;
    protected $blockManager = null;*/
    protected $cutTextHelper = null;
    protected $databaseManager = null;
    protected $pageManager = null;
    //protected $fileSystem = null;

    public function __construct(
        /*FlexibleContainer $blockSettings,
        AttachmentManager $attachmentManager,
        BlockManager $blockManager,*/
        CutText $cutTextHelper,
        DatabaseManager $databaseManager,
        PageManager $pageManager/*,
        $fileSystem,
        $blockData,
        $user,
        $language,
        PhpRenderer $phpRenderer*/
    )
    {
        /*$this->blockSettings = $blockSettings;
        $this->attachmentManager = $attachmentManager;
        $this->blockManager = $blockManager;*/
        $this->cutTextHelper = $cutTextHelper;
        $this->databaseManager = $databaseManager;
        $this->pageManager = $pageManager;
        /*$this->fileSystem = $fileSystem;
        $this->blockData = $blockData;
        $this->user = $user;
        $this->language = $language;
        $this->phpRenderer = $phpRenderer;
        $this->designMode = $this->user && $this->user->session->designMode;*/
    }

    /*public function search($query, $languageId = null)
    {
        $query = trim(strip_tags((string)$query));
        $languageId = $languageId ? (int)$languageId : null;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(array('p' => 'pages'))
            ->join(
                array('l' => 'languages'),
                'l.language_id = p.language_id',
                array('iso_code' => 'iso_code'),
                $select::JOIN_LEFT
            )
            ->where(
                array(
                    'p.title' => $query,
                    'p.is_published' => 1,
                )
            )
            ->order('p.last_update_date DESC, p.title ASC');

        if ($languageId > 0)
        {
            $select->where->equalTo('p.language_id', $languageId);
        }

        $resultSet = $dbManager->getHydratedResultSet($select);
    }*/

    public function getAttachmentManager()
    {
        return $this->attachmentManager;
    }

    public function getBlockManager()
    {
        return $this->blockManager;
    }

    public function getCutTextHelper()
    {
        return $this->cutTextHelper;
    }

    public function getDatabaseManager()
    {
        return $this->databaseManager;
    }

    public function getFileSystem()
    {
        if ($this->fileSystem == null)
        {
            $this->fileSystem = new \FileSystem\Component\FileSystem();
        }

        return $this->fileSystem;
    }

    public function getPageManager()
    {
        return $this->pageManager;
    }
} 
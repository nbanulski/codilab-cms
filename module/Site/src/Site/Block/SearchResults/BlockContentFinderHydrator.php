<?php
namespace Site\Block\SearchResults;

use Zend\Stdlib\Hydrator\HydratorInterface;
use Site\View\Helper\CutText;
use Site\Controller\PageManager;

class BlockContentFinderHydrator implements HydratorInterface
{
    protected $cutText = null;
    protected $pageManager = null;
    /*protected $translatableBlockNames = array(
        'EventList' => 'Event',
        'FutureEventList' => 'Future event',
        'OldEventList' => 'Old event',
        'Text' => 'Static content',
        '' => '',
        '' => '',
        '' => ''
    );*/

    public function __construct(
        CutText $cutText,
        PageManager $pageManager
    )
    {
        $this->cutText = $cutText;
        $this->pageManager = $pageManager;
    }

    public function extract($object) {}

    public function hydrate(array $data, $object)
    {
        foreach ($data as $property => $value)
        {
            $object[$property] = $value;
        }

        $cutText = $this->cutText;
        $append = ' ...';
        $appendOnlyIfTextIsNotEmpty = true;

        if ($object['block_element_contents'] != '')
        {
            $shortDescription = $cutText($object['block_element_contents'], 250, $append, $appendOnlyIfTextIsNotEmpty);
        }
        else
        {
            $shortDescription = $cutText($object['block_contents'], 250, $append, $appendOnlyIfTextIsNotEmpty);
        }

        $object['short_description'] = $shortDescription;

        $object['pageUrl'] = $this->pageManager->getFullPageCleanUrlByPageId($object['page_id']);

        if ($object['block_element_url_parameters'] != '')
        {
            $object['pageUrl'] .= ',' . $object['block_element_url_parameters'];
        }

        $object['pageBreadcrumb'] = $this->pageManager->getPageTextBreadcrumbByPageId($object['page_id']);

        /*$translatableBlockName = $this->convertPhpClassNameToTranslatableBlockName($object['php_class_name']);
        if ($translatableBlockName != '')
        {
            $object['category'] = $translatableBlockName;
        }*/

        unset($object['page_id']);
        unset($object['block_element_contents']);
        unset($object['block_contents']);

        //var_dump($object);

        // Sometimes phrase can exists in HTML tag (<img> tag for example), it is being stripped later
        // (in CutText helper) by strip_tags() and trim() functions, so short description becomes empty.
        // Afterwards our result is false and we must notify search-results.phtml template to exclude it
        //from displaying. That is the reason why you have to set null here.
        if ($object['short_description'] == '')
        {
            $object = null;
        }

        return $object;
    }

    private function convertPhpClassNameToTranslatableBlockName($phpClassName)
    {
        if (array_key_exists($phpClassName, $this->translatableBlockNames))
        {
            return $this->translatableBlockNames[$phpClassName];
        }

        return false;
    }
} 
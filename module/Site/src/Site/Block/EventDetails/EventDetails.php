<?php
namespace Site\Block\EventDetails;

use Site\Block\AbstractBlock;
use Zend\Http\Request;

class EventDetails extends AbstractBlock
{
    const BLOCK_EVENT_LIST_TABLE_NAME = 'block_event_list';
    const BLOCK_EVENT_LIST_CATEGORIES_TABLE_NAME = 'block_event_list_categories';
    const BLOCK_EVENT_LIST_ADDITIONAL_DATA_TYPES_TABLE_NAME = 'block_event_list_additional_data_types';
    const BLOCK_EVENT_LIST_ADDITIONAL_DATA_TABLE_NAME = 'block_event_list_additional_data';

    protected $hasOwnSettings = true;
    protected $hasManagementWindow = false;

    /**
     * Render-functions.
     */

    public function childRender(Request $request)
    {
        $parameters = $this->getParametersForCurrentPageFromRequest($request);
        $eventCleanUrlWithContentId = $parameters->key(); // Get name of the first parameter.

        list($currentEventCleanUrl, $currentEventContentId) =
            $this->getEventCleanUrlAndContentIdFromEventCleanUrlWithContentId($eventCleanUrlWithContentId);

        $this->blockView->setVariables(
            array(
                'currentEventCleanUrl' => $currentEventCleanUrl,
                'currentEventContentId' => $currentEventContentId,
                'currentEventDataAreValid' => (($currentEventCleanUrl != '') && ($currentEventContentId > 0))
            )
        );

        if (
            $this->eventDetailsShouldBeRenderedForCurrentEventCleanUrlAndContentId(
                $currentEventCleanUrl,
                $currentEventContentId
            )
        )
        {
            $this->renderEventDetails(
                $request,
                $eventCleanUrlWithContentId,
                $currentEventCleanUrl,
                $currentEventContentId
            );
        }
        else
        {
            $this->hideEventDetails();
        }
    }

    public function hideEventDetails()
    {
        $this->setRenderingEnabled(false);
    }

    public function renderEventDetails(
        Request $request,
        $eventCleanUrlWithContentId,
        $currentEventCleanUrl,
        $currentEventContentId
    )
    {
        $eventDetails = $this->getFullEventInfoByEventCleanUrlAndContentId(
            $currentEventCleanUrl, $currentEventContentId
        );

        $dataForEmailsAboutUserSignedUpForEventWasGiven = false;

        if ($currentEventContentId > 0)
        {
            /**
             * Check if e-mails data was given in block settings.
             */
            $blockManager = $this->getBlockManager();
            $eventListPageBlockId = $blockManager->getPageBlockIdByContentId($currentEventContentId);
            $eventListSettings = $blockManager->getPageBlockSettings($eventListPageBlockId);

            $dataForEmailsAboutUserSignedUpForEventWasGiven =
                $eventListSettings &&
                ($eventListSettings->subjectForMailForUserAboutUserSignedUpForEvent != '') &&
                ($eventListSettings->contentsForMailForUserAboutUserSignedUpForEvent != '') &&
                ($eventListSettings->subjectForMailForAdminAboutUserSignedUpForEvent != '') &&
                ($eventListSettings->contentsForMailForAdminAboutUserSignedUpForEvent != '')
            ;
        }

        $this->blockView->setVariables(
            array(
                'eventCleanUrlWithContentId' => $eventCleanUrlWithContentId,
                'eventListContentId' => $currentEventContentId,
                'eventDetails' => $eventDetails,
                'dataForEmailsAboutUserSignedUpForEventWasGiven' => $dataForEmailsAboutUserSignedUpForEventWasGiven
            )
        );
    }

    /**
     * Getters.
     */

    public function getAdditionalDataByTypeIdAndEventId($typeId, $eventId)
    {
        $typeId = (int)$typeId;
        $eventId = (int)$eventId;

        if (($typeId <= 0) || ($eventId <= 0))
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_EVENT_LIST_ADDITIONAL_DATA_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'type_id' => $typeId,
                    'event_id' => $eventId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getEventById($eventId)
    {
        $eventId = (int)$eventId;

        if ($eventId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_EVENT_LIST_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('event_id' => $eventId));

        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getEventByEventCleanUrlAndContentId($eventCleanUrl, $contentId)
    {
        $eventCleanUrl = trim((string)$eventCleanUrl);
        $contentId = (int)$contentId;

        if (($eventCleanUrl == '') || ($contentId < 0))
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_EVENT_LIST_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $contentId,
                    'clean_url' => $eventCleanUrl
                )
            );

        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return  $resultSet->current();
        }

        return false;
    }

    public function getFullEventInfoByEventCleanUrlAndContentId($eventCleanUrl, $contentId)
    {
        $eventCleanUrl = mb_strtolower(trim((string)$eventCleanUrl));
        $contentId = (int)$contentId;

        if (($eventCleanUrl == '') || ($contentId <= 0))
        {
            return false;
        }

        $fullInfo = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(array('be' => self::BLOCK_EVENT_LIST_TABLE_NAME))
            ->join(
                array('bec' => self::BLOCK_EVENT_LIST_CATEGORIES_TABLE_NAME),
                'bec.category_id = be.category_id',
                array('category_name' => 'name'),
                $select::JOIN_LEFT
            )
            ->join(
                array('u' => 'users'),
                'u.user_id = be.last_update_author_id',
                array('last_update_author' => 'login'),
                $select::JOIN_LEFT
            )
            ->where(
                array(
                    'be.content_id' => $contentId,
                    'be.clean_url' => $eventCleanUrl,
                    'bec.content_id' => $contentId
                )
            );
        $select->order('be.event_date_start ASC');
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $fullInfo = $resultSet->current();
        }

        if ($fullInfo)
        {
            /**
             * Images.
             */

            $mainImage = false;
            $mainImageUrl = '';
            $images = new \Site\Custom\FlexibleContainer();

            $blockManager = $this->getBlockManager();
            $eventListBlock = $blockManager->createBlockUsingContentId($contentId);
            if ($eventListBlock instanceof \Site\Block\EventList\EventList)
            {
                $mainImage = $eventListBlock->getEventMainImageByEventId($fullInfo->event_id);
                if ($mainImage)
                {
                    $attachment = $this->attachmentManager->getAttachmentById($mainImage->attachment_id);
                    if ($attachment)
                    {
                        $mainImageUrl =
                            '/eng/get-file/index,' .
                            $attachment->hash_for_file_name .
                            ',' .
                            $attachment->original_file_name;
                    }
                }

                $excludeMainImage = true;
                $images = $eventListBlock->getEventImagesByEventId($fullInfo->event_id, $excludeMainImage);
            }

            /**
             * Additional data.
             */

            $additionalData = new \Site\Custom\FlexibleContainer();

            $onClause = new \Zend\Db\Sql\Expression(
                'belad.type_id = beladt.type_id AND belad.event_id = ? AND belad.content_id = ?',
                array(
                    $fullInfo->event_id,
                    $contentId
                )
            );

            $select = $sql->select();
            $select
                ->columns(
                    array(
                        'type_id',
                        'name'
                    )
                )
                ->from(array('beladt' => self::BLOCK_EVENT_LIST_ADDITIONAL_DATA_TYPES_TABLE_NAME))
                ->join(
                    array('belad' => self::BLOCK_EVENT_LIST_ADDITIONAL_DATA_TABLE_NAME),
                    $onClause,
                    array(
                        'data_id',
                        'last_update_author_id',
                        'last_update_date',
                        'contents'
                    ),
                    'LEFT OUTER'
                )
                ->where(
                    array(
                        'beladt.content_id' => $contentId
                    )
                );

            $resultSet = $dbManager->getHydratedResultSet($select);
            if ($resultSet)
            {
                $additionalData = new \Site\Custom\FlexibleContainer($resultSet->toArray());
            }

            $fullInfo->dateStart = date('Y-m-d H:i', strtotime($fullInfo->event_date_start));
            $fullInfo->hourStart = date('H:i', strtotime($fullInfo->event_date_start));
            $fullInfo->hourEnd = date('H:i', strtotime($fullInfo->event_date_end));
            $fullInfo->mainImage = $mainImage;
            $fullInfo->mainImageUrl = $mainImageUrl;
            $fullInfo->images = $images;
            $fullInfo->additionalData = $additionalData;
        }


        return $fullInfo;
    }

    public function eventDetailsShouldBeRenderedForCurrentEventCleanUrlAndContentId(
        $currentEventCleanUrl, $currentEventContentId
    )
    {
        $settings = $this->getSettings();
        $shouldBeHiddenIfCurrentUrlDoesNotLeadsToEventDetails =
            $settings->hideIfCurrentUrlDoesNotLeadsToEventDetails ? true : false;

        $shouldBeRendered =
            !$shouldBeHiddenIfCurrentUrlDoesNotLeadsToEventDetails || // Hiding is disabled...
            // ... or data needed for displaying event's details are correct:
            (($currentEventCleanUrl != '') && ($currentEventContentId > 0));

        if ($shouldBeRendered)
        {
            // Check also if given data really points to event details, not to - for example - event details:

            $event = $this->getEventByEventCleanUrlAndContentId($currentEventCleanUrl, $currentEventContentId);
            if (!$event)
            {
                return false;
            }
        }

        return $shouldBeRendered;
    }

    /**
     * Data formatting functions.
     */

    public function formatHourWithMinutesString($hourWithMinutesString)
    {
        $hourWithMinutesString = trim((string)$hourWithMinutesString);

        if ($hourWithMinutesString == '')
        {
            return '';
        }

        $parts = explode(':', $hourWithMinutesString);
        if (count($parts) < 2)
        {
            return '';
        }

        $hour = $parts[0];
        $minutes = $parts[1];

        if (
            (($hour >= 0) && ($hour <= 23)) &&
            (($minutes >= 0) && ($minutes <= 59))
        )
        {
            return str_pad($hour, 2, '0', STR_PAD_LEFT) . ':' . str_pad($minutes, 2, '0', STR_PAD_LEFT);
        }

        return '';
    }

    /**
     * Logic functions.
     */

    public function updateEventTitle($eventId, $eventTitle)
    {
        $eventId = (int)$eventId;
        $eventTitle = trim(strip_tags((string)$eventTitle));
        $eventTitle = str_replace("\r\n", ' ', $eventTitle);
        $eventTitle = str_replace(array("\r", "\n"), ' ', $eventTitle);

        if (($eventId <= 0) || ($eventTitle == ''))
        {
            return false;
        }

        $updated = false;

        $lastUpdateAuthorId = $this->user->user_id ? (int)$this->user->user_id : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_EVENT_LIST_TABLE_NAME);
        $update
            ->set(
                array(
                    'last_update_author_id' => $lastUpdateAuthorId,
                    'last_update_date' => $lastUpdateDate,
                    'title' => $eventTitle
                )
            )
            ->where(array('event_id' => $eventId));

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $event = $this->getEventById($eventId);
            if (
                $event &&
                ($event->last_update_author_id == $lastUpdateAuthorId) &&
                ($event->title == $eventTitle)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function updateEventContents($eventId, $eventContents)
    {
        $eventId = (int)$eventId;
        $eventContents = trim((string)$eventContents);

        if (($eventId <= 0) || ($eventContents == ''))
        {
            return false;
        }

        $updated = false;

        $lastUpdateAuthorId = $this->user->user_id ? (int)$this->user->user_id : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_EVENT_LIST_TABLE_NAME);
        $update
            ->set(
                array(
                    'last_update_author_id' => $lastUpdateAuthorId,
                    'last_update_date' => $lastUpdateDate,
                    'contents' => $eventContents
                )
            )
            ->where(array('event_id' => $eventId));

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $event = $this->getEventById($eventId);
            if (
                $event &&
                ($event->last_update_author_id == $lastUpdateAuthorId) &&
                ($event->contents == $eventContents)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function updateEventAdditionalData($eventId, $dataTypeId, $contents)
    {
        $eventId = (int)$eventId;
        $dataTypeId = (int)$dataTypeId;
        $contents = trim((string)$contents);

        if (($eventId <= 0) || ($dataTypeId <= 0))
        {
            return false;
        }

        $event = $this->getEventById($eventId);
        if (!$event)
        {
            return false;
        }

        $updated = false;

        $lastUpdateAuthorId = $this->user->user_id ? (int)$this->user->user_id : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $data = $this->getAdditionalDataByTypeIdAndEventId($dataTypeId, $eventId);
        if ($data)
        {
            $update = $sql->update(self::BLOCK_EVENT_LIST_ADDITIONAL_DATA_TABLE_NAME);
            $update
                ->set(
                    array(
                        'last_update_author_id' => $lastUpdateAuthorId,
                        'last_update_date' => $lastUpdateDate,
                        'contents' => $contents
                    )
                )
                ->where(
                    array(
                        'type_id' => $dataTypeId,
                        'event_id' => $eventId
                    )
                );
            $result = $dbManager->executePreparedStatement($update);
        }
        else
        {
            $insert = $sql->insert(self::BLOCK_EVENT_LIST_ADDITIONAL_DATA_TABLE_NAME);
            $insert
                ->columns(
                    array(
                        'type_id',
                        'event_id',
                        'content_id',
                        'last_update_author_id',
                        'last_update_date',
                        'contents'
                    )
                )
                ->values(
                    array(
                        'type_id' => $dataTypeId,
                        'event_id' => $eventId,
                        'content_id' => $event->content_id,
                        'last_update_author_id' => $lastUpdateAuthorId,
                        'last_update_date' => $lastUpdateDate,
                        'contents' => $contents
                    )
                );
            $result = $dbManager->executePreparedStatement($insert);
        }

        if ($result)
        {
            $data = $this->getAdditionalDataByTypeIdAndEventId($dataTypeId, $eventId);
            if ($data &&
                ($data->last_update_author_id == $lastUpdateAuthorId) &&
                ($data->contents == $contents)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function updateEventDateStartBySettingNewHourAndMinutes($eventId, $startHourWithMinutes)
    {
        $eventId = (int)$eventId;
        $startHourWithMinutes = trim((string)$startHourWithMinutes);
        $startHourWithMinutes = $this->formatHourWithMinutesString($startHourWithMinutes);

        if (($eventId <= 0) || ($startHourWithMinutes == ''))
        {
            return false;
        }

        $event = $this->getEventById($eventId);
        if (!$event)
        {
            return false;
        }

        $lastUpdateAuthorId = $this->user->user_id ? (int)$this->user->user_id : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $eventDateStart = $event->event_date_start;
        $eventNewDateStart = date('Y-m-d', strtotime($eventDateStart)) . ' ' . $startHourWithMinutes . ':00';

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $update = $sql->update(self::BLOCK_EVENT_LIST_TABLE_NAME);
        $update
            ->set(
            array(
                    'last_update_author_id' => $lastUpdateAuthorId,
                    'last_update_date' => $lastUpdateDate,
                    'event_date_start' => $eventNewDateStart
                )
            )
            ->where(
                array(
                    'event_id' => $eventId
                )
            );
        $result = $dbManager->executePreparedStatement($update);

        if ($result)
        {
            return true;
        }

        return false;
    }

    /**
     * Other functions.
     */

    public function getEventCleanUrlAndContentIdFromEventCleanUrlWithContentId($eventCleanUrlWithContentId)
    {
        $eventCleanUrl = $eventCleanUrlWithContentId;
        $contentId = null;

        $characterPosition = mb_strrpos($eventCleanUrlWithContentId, '-');
        if ($characterPosition !== false)
        {
            $totalLength = mb_strlen($eventCleanUrlWithContentId);
            $eventCleanUrl = mb_substr($eventCleanUrlWithContentId, 0, $characterPosition);
            $contentId = mb_substr($eventCleanUrlWithContentId, -($totalLength - $characterPosition - 1));
        }

        return array($eventCleanUrl, $contentId);
    }
}
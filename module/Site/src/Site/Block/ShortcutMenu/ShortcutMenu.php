<?php
namespace Site\Block\ShortcutMenu;

use Site\Block\NavigationMenu\NavigationMenu;

class ShortcutMenu extends NavigationMenu
{
    public function init()
    {
        $className = basename(str_replace('\\', '/', get_class($this)));
        $filter = new \Zend\Filter\Word\CamelCaseToDash();
        $this->blockViewName = mb_strtolower($filter->filter($className));
    }
} 
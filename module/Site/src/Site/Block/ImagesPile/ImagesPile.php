<?php
namespace Site\Block\ImagesPile;

use Site\Block\AbstractBlock;
use Zend\Http\Request;

class ImagesPile extends AbstractBlock
{
    const ITEM_URL_ADDRESS_TYPE_INTERNAL = 0;
    const ITEM_URL_ADDRESS_TYPE_EXTERNAL = 1;
    const ITEM_URL_ADDRESS_TYPE_NONE = 2;

    const BLOCK_IMAGES_PILE_GROUPS_TABLE_NAME = 'block_images_pile_groups';
    const BLOCK_IMAGES_PILE_ITEMS_TABLE_NAME = 'block_images_pile_items';

    protected $hasOwnSettings = true;
    protected $hasManagementWindow = true;

    public static $availableAnimationTypes = array(
        'linear',
        'ease',
        'ease-in',
        'ease-out',
        'ease-in-out'
    );

    /**
     * Events.
     */

    public function onAfterInserting($slotId)
    {
        return true;
    }

    public function onBeforeDeleted()
    {
        if (!$this->blockData->is_dynamic)
        {
            $this->deleteAllItems();
            $this->deleteAllGroups();
        }
    }

    /**
     * Render-functions.
     */

    public function childRender(\Zend\Http\Request $request)
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();

        /**
         * Groups:
         */

        $select = $sql->select();
        $select->from(array('bipi' => self::BLOCK_IMAGES_PILE_ITEMS_TABLE_NAME))
            ->join(
                array('bipg' => self::BLOCK_IMAGES_PILE_GROUPS_TABLE_NAME),
                'bipg.group_id = bipi.group_id',
                array('group_title' => 'title'),
                $select::JOIN_LEFT
            )
            ->join(
                array('u' => 'users'),
                'u.user_id = bipg.last_update_author_id',
                array('last_update_author' => 'login'),
                $select::JOIN_LEFT
            )
            ->where(
                array(
                    'bipg.content_id' => $contentId,
                    'bipg.is_published' => 1,
                    'bipi.content_id' => $contentId,
                    'bipi.is_published' => 1
                )
            )
            ->order('bipg.order ASC, bipg.title ASC, bipi.order ASC, bipi.title ASC');
        $result = $dbManager->executePreparedStatement($select);
        $resultSet = new \Zend\Db\ResultSet\HydratingResultSet(
            new ImageHydrator($this->getTranslateHelper(), $this->attachmentManager, $this->getPageManager(), $this),
            new \Site\Custom\FlexibleContainer()
        );
        $resultSet->initialize($result);

        $jsSettings = $this->makeJavaScriptObjectFromImagesPileSettings();

        $this->blockView->setVariables(
            array(
                'items' => $resultSet,
                'jsSettings' => $jsSettings
            )
        );
    }

    public function childRenderManagement(\Zend\Http\Request $request)
    {
        if (!$this->managementView instanceof \Zend\View\Model\ViewModel)
        {
            return false;
        }

        $dbManager = $this->getDatabaseManager();
        $dbAdapter = $dbManager->getAdapter();
        $sql = $dbManager->getSql();

        $pageNumber = $this->managementView->pageNumber;
        $itemCountPerPage = $this->managementView->itemCountPerPage;
        $contentId = $this->getContentId();

        $select = $sql->select();
        $select->from(array('bipi' => self::BLOCK_IMAGES_PILE_ITEMS_TABLE_NAME))
            ->join(
                array('bipg' => self::BLOCK_IMAGES_PILE_GROUPS_TABLE_NAME),
                'bipg.group_id = bipi.group_id AND bipg.content_id = bipi.content_id',
                array('group_title' => 'title'),
                $select::JOIN_LEFT
            )
            ->join(
                array('u' => 'users'),
                'u.user_id = bipi.last_update_author_id',
                array('last_update_author' => 'login'),
                $select::JOIN_LEFT
            )
            ->where(
                array(
                    'bipi.content_id' => $contentId
                )
            )
            ->order('bipg.order ASC, bipg.title, bipi.order ASC, bipi.title ASC');
        $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        $paginatorAdapter = new \Zend\Paginator\Adapter\DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new \Zend\Paginator\Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($pageNumber);
        $paginator->setItemCountPerPage($itemCountPerPage);

        $this->managementView->setVariables(
            array(
                'itemsGroupCount' => $this->getGroupCount(),
                'itemsPaginator' => $paginator
            )
        );
    }

    public function childRenderAddEdit(\Zend\Http\Request $request)
    {
        if (!$this->addEditView instanceof \Zend\View\Model\ViewModel)
        {
            return false;
        }

        $pageManager = $this->getPageManager();

        $item = null;
        $itemId = (int)$request->getPost('itemId');
        if ($itemId > 0)
        {
            $item = $this->getItemById($itemId);
        }

        $defaultFormData = array(
            'itemId' => null,
            'itemContentId' => null,
            'itemGroupId' => null,
            'itemPageId' => null,
            'itemAttachmentId' => null,
            'itemOrder' => 0,
            'itemIsPublished' => false,
            'itemUrlAddressType' => 0,
            'itemExternalUrl' => '',
            'itemTitle' => '',
            'itemImages' => array(
                0 => array(
                    'imageId' => null,
                    'isMain' => true,
                    'attachmentId' => null,
                    'uniqueHashForFileName' => '',
                    'originalFileName' => '',
                )
            ),
            'itemUniqueHashForFileName' => '',
            'itemOriginalFileName' => ''
        );
        $currentImageFormData = array();

        if ($item)
        {
            $itemImagesArray = array(
                0 => array(
                    'imageId' => null,
                    'isMain' => true,
                    'attachmentId' => null,
                    'uniqueHashForFileName' => '',
                    'originalFileName' => '',
                )
            );

            $attachmentId = $item->attachment_id;
            $attachment = $this->attachmentManager->getAttachmentById($attachmentId);

            $itemImagesArray[0] = new \Site\Custom\FlexibleContainer();
            $itemImagesArray[0]->imageId = false;
            $itemImagesArray[0]->isMain = true;
            $itemImagesArray[0]->attachmentId = $attachmentId;
            $itemImagesArray[0]->uniqueHashForFileName
                = $attachment ? $attachment->hash_for_file_name : '';
            $itemImagesArray[0]->originalFileName
                = $attachment ? $attachment->original_file_name : '';

            $currentImageFormData = array(
                'itemId' => $item->item_id,
                'itemContentId' => $item->content_id,
                'itemGroupId' => $item->group_id,
                'itemPageId' => $item->page_id,
                'itemAttachmentId' => $item->attachment_id,
                'itemOrder' => $item->order,
                'itemIsPublished' => $item->is_published,
                'itemUrlAddressType' => $item->url_address_type,
                'itemExternalUrl' => $item->external_url,
                'itemTitle' => $item->title,
                'itemImages' => $itemImagesArray,
                'itemUniqueHashForFileName' => $attachment ? $attachment->hash_for_file_name : '',
                'itemOriginalFileName' => $attachment ? $attachment->original_file_name : ''
            );
        }

        $formData = new \Site\Custom\FlexibleContainer(array_merge($defaultFormData, $currentImageFormData));

        $itemGroupsOptions = array();
        $groups = $this->getGroups();
        if ($groups)
        {
            foreach ($groups as $group)
            {
                $itemGroupsOptions[$group->group_id] = $group->title;
            }
        }

        $itemInternalPagesOptions = array();
        $pages = $pageManager->getPages();
        if ($pages)
        {
            foreach ($pages as $page)
            {
                $option = new \stdClass();
                $option->title = $page->title;
                $option->extended_title =
                    $page->title .
                    ' (' . $pageManager->getFullPageCleanUrlByPageId($page->page_id) . ')';
                $option->selected = ($page->page_id == $formData->itemPageId) ? ' selected' : '';

                $itemInternalPagesOptions[$page->page_id] = $option;
            }
        }

        $this->addEditView->setVariables(
            array(
                'itemGroupsOptions' => $itemGroupsOptions,
                'itemInternalPagesOptions' => $itemInternalPagesOptions,
                'formData' => $formData
            )
        );
    }

    public function childRenderSettings()
    {
        $this->settingsView->setVariables(
            array()
        );
    }

    public function renderGroupsManagement(\Zend\Http\Request $request, $return = true)
    {
        $settings = $this->getSettings();

        $pageNumber = (int)$request->getPost('pageNumber') ? : 1;
        $itemCountPerPage = (int)$request->getPost('itemCountPerPage') ? : 10;
        $includePagesFrom = $request->getPost('includePagesFrom') ? : 'current-language';

        $dbManager = $this->getDatabaseManager();
        $dbAdapter = $dbManager->getAdapter();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();

        $select = $sql->select();
        $select->from(array('bipg' => self::BLOCK_IMAGES_PILE_GROUPS_TABLE_NAME))
            ->join(
                array('u' => 'users'),
                'u.user_id = bipg.last_update_author_id',
                array('last_update_author' => 'login'),
                $select::JOIN_LEFT
            )
            ->where(array('bipg.content_id' => $contentId))
            ->order('bipg.order ASC, bipg.title ASC');
        $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        $paginatorAdapter = new \Zend\Paginator\Adapter\DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new \Zend\Paginator\Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($pageNumber);
        $paginator->setItemCountPerPage($itemCountPerPage);

        $groupsManagementView = new \Zend\View\Model\ViewModel();
        $groupsManagementView->setTemplate('blocks/' . $this->blockViewName . '/groups-management.phtml');
        $groupsManagementView->setVariables(
            array(
                'userLanguage' => $this->userLanguage,
                'user' => $this->user,
                'designMode' => $this->designMode,
                'blockData' => $this->blockData,
                'hasOwnSettings' => $this->hasOwnSettings,
                'settings' => $settings,
                'pageNumber' => $pageNumber,
                'itemCountPerPage' => $itemCountPerPage,
                'includePagesFrom' => $includePagesFrom,
                'itemGroupsPaginator' => $paginator
            )
        );
        $groupsManagementViewHtml = $this->phpRenderer->render($groupsManagementView);

        if ($return)
        {
            return $groupsManagementViewHtml;
        }
        else
        {
            echo $groupsManagementViewHtml;
        }
    }

    public function renderAddEditGroup(\Zend\Http\Request $request, $return = true)
    {
        $settings = $this->getSettings();

        $pageNumber = (int)$request->getPost('pageNumber') ? : 1;
        $itemCountPerPage = (int)$request->getPost('itemCountPerPage') ? : 10;
        $includePagesFrom = $request->getPost('includePagesFrom') ? : 'current-language';

        $defaultFormData = array(
            'groupId' => '',
            'groupTitle' => '',
            'groupIsPublished' => ''
        );
        $currentGroupFormData = array();

        $groupId = (int)$request->getPost('groupId');
        if ($groupId > 0)
        {
            $group = $this->getGroupById($groupId);

            $currentGroupFormData = array(
                'groupId' => $group->group_id,
                'groupTitle' => $group->title,
                'groupIsPublished' => $group->is_published
            );
        }

        $formData = array_merge($defaultFormData, $currentGroupFormData);

        $addEditGroupView = new \Zend\View\Model\ViewModel();
        $addEditGroupView->setTemplate('blocks/' . $this->blockViewName . '/add-edit-group.phtml');
        $addEditGroupView->setVariables(
            array(
                'userLanguage' => $this->userLanguage,
                'user' => $this->user,
                'designMode' => $this->designMode,
                'blockData' => $this->blockData,
                'hasOwnSettings' => $this->hasOwnSettings,
                'settings' => $settings,
                'pageNumber' => $pageNumber,
                'itemCountPerPage' => $itemCountPerPage,
                'includePagesFrom' => $includePagesFrom,
                'formData' => $formData
            )
        );
        $addEditGroupViewHtml = $this->phpRenderer->render($addEditGroupView);

        if ($return)
        {
            return $addEditGroupViewHtml;
        }
        else
        {
            echo $addEditGroupViewHtml;
        }
    }

    /**
     * Getters.
     */

    public function getGroups()
    {
        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_IMAGES_PILE_GROUPS_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('content_id' => $this->getContentId()));
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getGroupById($groupId)
    {
        $groupId = (int)$groupId;

        if ($groupId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_IMAGES_PILE_GROUPS_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'group_id' => (int)$groupId,
                    'content_id' => $this->getContentId()
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getGroupCount()
    {
        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_IMAGES_PILE_GROUPS_TABLE_NAME)
            ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')), $prefixColumnsWithTable)
            ->where(array('content_id' => $this->getContentId()));
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();
            $count = $row ? $row->count : false;

            return $count;
        }

        return false;
    }

    public function getItemById($itemId)
    {
        $itemId = (int)$itemId;

        if ($itemId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_IMAGES_PILE_ITEMS_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'item_id' => (int)$itemId,
                    'content_id' => $this->getContentId()
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function isItemExternalUrlValid($itemExternalUrl)
    {
        $itemExternalUrl = trim(strip_tags((string)$itemExternalUrl));

        return ($itemExternalUrl != '');
    }

    public function isItemUrlAddressTypeValid($itemUrlAddressType)
    {
        $itemUrlAddressType = (int)$itemUrlAddressType;

        return
            ($itemUrlAddressType >= self::ITEM_URL_ADDRESS_TYPE_INTERNAL) &&
            ($itemUrlAddressType <= self::ITEM_URL_ADDRESS_TYPE_NONE);
    }

    /**
     * Data formatting functions.
     */

    public function formatCss3AnimationType($animationType, $default = 'linear')
    {
        $animationType = mb_strtolower(trim((string)$animationType));

        if (!in_array($animationType, self::$availableAnimationTypes))
        {
            return $default;
        }

        return $animationType;
    }

    public function formatGroupDataAndReturnIfAreValid($groupData)
    {
        $groupData = $this->convertToFlexibleContainer($groupData);
        $groupDataCopy = clone $groupData;

        $groupDataCopy->groupId = (int)$groupData->groupId ? : null;
        $groupDataCopy->groupIsPublished = (int)$groupData->groupIsPublished ? 1 : 0;
        $groupDataCopy->groupTitle = trim((string)$groupData->groupTitle);

        /**
         * Identifying the author.
         */
        $groupDataCopy->groupLastUpdateAuthorId = $this->user->user_id;

        if (
            (($groupDataCopy->groupId !== null) && ($groupDataCopy->groupId <= 0)) ||
            ($groupDataCopy->groupTitle == '')
        )
        {
            return false;
        }

        return $groupDataCopy;
    }

    public function formatItemDataAndReturnIfAreValid($itemData)
    {
        $itemData = $this->convertToFlexibleContainer($itemData);
        $itemDataCopy = clone $itemData;

        $itemDataCopy->itemId = (int)$itemData->itemId ? : null;
        $itemDataCopy->itemGroupId = (int)$itemData->itemGroupId;
        $itemDataCopy->itemPageId = (int)$itemData->itemPageId ? : null;
        $itemDataCopy->itemAttachmentId = (int)$itemData->itemAttachmentId ? : null;
        $itemDataCopy->itemOrder = ((int)$itemDataCopy->itemOrder > 0) ? : 0;
        $itemDataCopy->itemIsPublished = (int)$itemDataCopy->itemIsPublished ? 1 : 0;
        $itemDataCopy->itemUrlAddressType = (int)$itemData->itemUrlAddressType;
        $itemDataCopy->itemExternalUrl = trim((string)$itemData->itemExternalUrl);
        $itemDataCopy->itemTitle = trim((string)$itemData->itemTitle);
        $itemDataCopy->itemImages = $itemData->itemImages;
        if ($itemDataCopy->itemImages instanceof \Site\Custom\FlexibleContainer)
        {
            $itemDataCopy->itemImages = $itemDataCopy->itemImages->toArray(true);
        }

        $itemImages = $itemDataCopy->itemImages;
        $itemImage = $itemImages ? current($itemImages) : null;

        /**
         * Validating form data.
         */

        if ($itemImage)
        {
            $itemDataCopy->itemUniqueHashForFileName = trim((string)$itemImage['uniqueHashForFileName']);

            // Validate unique hash for file name only if it is not empty!
            if (
                ($itemDataCopy->itemUniqueHashForFileName != '') &&
                !preg_match('/^([a-z0-9]{40})$/i', $itemDataCopy->itemUniqueHashForFileName)
            )
            {
                return false;
            }
        }

        /**
         * Identifying the author.
         */

        $itemDataCopy->itemLastUpdateAuthorId = $this->user->user_id;

        /**
         * Parsing unique hash for file name.
         */

        if ($itemDataCopy->itemImages)
        {
            $attachmentManager = $this->getAttachmentManager();
            $itemImages = array();
            foreach ($itemDataCopy->itemImages as $itemImageNumber => $itemImage)
            {
                if ($itemImage['uniqueHashForFileName'] != '')
                {
                    // Validate unique hash for file name:
                    if (!preg_match('/^([a-z0-9]{40})$/i', $itemImage['uniqueHashForFileName']))
                    {
                        return false;
                    }

                    $attachment = $attachmentManager->getAttachmentByUniqueHashForFileName(
                        $itemImage['uniqueHashForFileName']
                    );
                    if (!$attachment)
                    {
                        return false;
                    }

                    $itemImages[$itemImageNumber] = array(
                        'imageId' => $itemImage['imageId'],
                        'imageIsMain' => isset($itemImage['isMain']) ? (int)$itemImage['isMain'] : 0,
                        'imageUniqueHashForFileName' => $itemImage['uniqueHashForFileName'],
                        'imageAttachmentId' => $attachment->attachment_id
                    );

                    $itemDataCopy->itemAttachmentId = $attachment->attachment_id;

                    break;
                }
            }

            $itemDataCopy->itemImages = $itemImages;
        }

        /**
         * Removing unnecessary data.
         */

        unset($itemDataCopy->itemUniqueHashForFileName);

        /**
         * Adding "http://".
         */

        if (
            ($itemDataCopy->itemExternalUrl != '') &&
            (mb_strpos($itemDataCopy->itemExternalUrl, 'http://') === false) &&
            (mb_strpos($itemDataCopy->itemExternalUrl, 'https://') === false)
        )
        {
            $itemDataCopy->itemExternalUrl = 'http://' . $itemDataCopy->itemExternalUrl;
        }

        $isExternalUrlInvalid =
            ($itemDataCopy->itemUrlAddressType == self::ITEM_URL_ADDRESS_TYPE_EXTERNAL) &&
            !$this->isItemExternalUrlValid($itemDataCopy->itemExternalUrl);

        if (
            (($itemDataCopy->itemId !== null) && ($itemDataCopy->itemId <= 0)) ||
            ($itemDataCopy->itemGroupId <= 0) ||
            (($itemDataCopy->itemPageId !== null) && ($itemDataCopy->itemPageId <= 0)) ||
            (($itemDataCopy->itemAttachmentId !== null) && ($itemDataCopy->itemAttachmentId <= 0)) ||
            !$this->isItemUrlAddressTypeValid($itemDataCopy->itemUrlAddressType) ||
            $isExternalUrlInvalid
        )
        {
            return false;
        }

        return $itemDataCopy;
    }

    public function makeJavaScriptObjectFromImagesPileSettings()
    {
        $settings = $this->getSettings();

        $defaultAnimationType = 'ease-in-out';

        $gutter = ($settings->gutter !== null) ? max(0, (int)$settings->gutter) : 40;
        $pileAngles = ($settings->pileAngles !== null) ? max(0, (int)$settings->pileAngles) : 2;

        $pileAnimationOpenSpeed =
            ($settings->pileAnimationOpenSpeed !== null) ? max(0, (int)$settings->pileAnimationOpenSpeed) : 400;
        $pileAnimationOpenEasing =
            ($settings->pileAnimationOpenEasing !== null)
                ? $this->formatCss3AnimationType($settings->pileAnimationOpenEasing, $defaultAnimationType)
                : $defaultAnimationType;
        $pileAnimationCloseSpeed =
            ($settings->pileAnimationCloseSpeed !== null) ? max(0, (int)$settings->pileAnimationCloseSpeed) : 400;
        $pileAnimationCloseEasing =
            ($settings->pileAnimationCloseEasing !== null)
                ? $this->formatCss3AnimationType($settings->pileAnimationCloseEasing, $defaultAnimationType)
                : $defaultAnimationType;

        $otherPileAnimationOpenSpeed =
            ($settings->otherPileAnimationOpenSpeed !== null)
                ? max(0, (int)$settings->otherPileAnimationOpenSpeed)
                : 400;
        $otherPileAnimationOpenEasing =
            ($settings->otherPileAnimationOpenEasing !== null)
                ? $this->formatCss3AnimationType($settings->otherPileAnimationOpenEasing, $defaultAnimationType)
                : $defaultAnimationType;
        $otherPileAnimationCloseSpeed =
            ($settings->otherPileAnimationCloseSpeed !== null)
                ? max(0, (int)$settings->otherPileAnimationCloseSpeed)
                : 400;
        $otherPileAnimationCloseEasing =
            ($settings->otherPileAnimationCloseEasing !== null)
                ? $this->formatCss3AnimationType($settings->otherPileAnimationCloseEasing, $defaultAnimationType)
                : $defaultAnimationType;

        $delay = ($settings->delay !== null) ? (int)$settings->delay : 0;
        $randomAngle = ($settings->randomAngle !== null) ? ($settings->randomAngle ? 'true' : 'false') : 'false';

        $jsObjectString = '
            {
                gutter:' . $gutter . ',
                pileAngles:' . $pileAngles . ',
                pileAnimation:{
                    openSpeed:' . $pileAnimationOpenSpeed . ',
                    openEasing:\'' . $pileAnimationOpenEasing . '\',
                    closeSpeed:' . $pileAnimationCloseSpeed . ',
                    closeEasing:\'' . $pileAnimationCloseEasing . '\'
                },
                otherPileAnimation:{
                    openSpeed:' . $otherPileAnimationOpenSpeed . ',
                    openEasing:\'' . $otherPileAnimationOpenEasing . '\',
                    closeSpeed:' . $otherPileAnimationCloseSpeed . ',
                    closeEasing:\'' . $otherPileAnimationCloseEasing . '\'
                },
                delay:' . $delay . ',
                randomAngle:' . $randomAngle . '
            }
        ';

        return $jsObjectString;
    }

    /**
     * Logic-functions.
     */

    public function addDefaultGroupIfNoGroupExists()
    {
        $groupCount = $this->getGroupCount();
        if ($groupCount > 0)
        {
            return true;
        }

        if ($groupCount === false) // It is impossible to get group count because of database error.
        {
            return false;
        }

        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $translate = $this->getTranslateHelper();
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $insert = $sql->insert(self::BLOCK_IMAGES_PILE_GROUPS_TABLE_NAME);
        $insert
            ->columns(
                array(
                    'content_id',
                    'is_published',
                    'last_update_author_id',
                    'last_update_date',
                    'title',
                )
            )
            ->values(
                array(
                    'content_id' => $this->getContentId(),
                    'is_published' => 1,
                    'last_update_author_id' => null,
                    'last_update_date' => $lastUpdateDate,
                    'title' => $translate('Default group')
                )
            );
        $groupId = $dbManager->executeInsertAndGetLastInsertId($insert);
        if ($groupId > 0)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $groupId;
    }

    public function addOrEditGroup($groupData)
    {
        $groupDataCopy = $this->formatGroupDataAndReturnIfAreValid($groupData);
        if (!$groupDataCopy)
        {
            return false;
        }

        $groupDataCopy->groupLastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        if ($groupData->groupId > 0)
        {
            return $this->updateExistingGroup($groupDataCopy);
        }
        else
        {
            return $this->createNewGroup($groupDataCopy);
        }
    }

    public function addOrEditItem($itemData)
    {
        $itemDataCopy = $this->formatItemDataAndReturnIfAreValid($itemData);
        if (!$itemDataCopy)
        {
            return false;
        }

        $itemDataCopy->itemLastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        if ($itemData->itemId > 0)
        {
            return $this->updateExistingItem($itemDataCopy);
        }
        else
        {
            return $this->createNewItem($itemDataCopy);
        }
    }

    public function decideWhatToDoWithRecentlyUploadedAndMovedFiles(\Zend\Http\Request $request, $movedFiles)
    {
        //$postData = $request->getPost();

        // Inform the UploadController and UploadManager than upload was successful and you accept these files:
        return true;
    }

    public function deleteAllGroups()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_IMAGES_PILE_GROUPS_TABLE_NAME);
        $delete->where(array('content_id' => $this->getContentId()));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;
    }

    public function deleteAllItems()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_IMAGES_PILE_ITEMS_TABLE_NAME);
        $delete->where(array('content_id' => $this->getContentId()));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;
    }

    public function deleteItemById($itemId)
    {
        $itemId = (int)$itemId;
        if ($itemId <= 0)
        {
            return false;
        }

        $deleted = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();

        $delete = $sql->delete(self::BLOCK_IMAGES_PILE_ITEMS_TABLE_NAME);
        $delete->where(
            array(
                'item_id' => $itemId,
                'content_id' => $contentId
            )
        );
        $result = $dbManager->executePreparedStatement($delete);
        if ($result)
        {
            $deleted = true;
        }

        return $deleted;
    }

    public function reorderGroups($groups)
    {
        $groups = (array)$groups;
        if (!$groups)
        {
            return false;
        }

        $reordered = true;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();
        $order = 0;

        $dbManager->beginTransaction();

        foreach ($groups as $groupId)
        {
            $groupId = (int)$groupId;

            if ($groupId > 0)
            {
                $update = $sql->update(self::BLOCK_IMAGES_PILE_GROUPS_TABLE_NAME);
                $update
                    ->set(array('order' => $order))
                    ->where(
                        array(
                            'group_id' => $groupId,
                            'content_id' => $contentId
                        )
                    );
                $result = $dbManager->executePreparedStatement($update);
                if (!$result)
                {
                    $reordered = false;
                    break;
                }

                $group = $this->getGroupById($groupId);
                if ($group->order != $order)
                {
                    $reordered = false;
                    break;
                }

                $order++;
            }
        }

        if ($reordered)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $reordered;
    }

    public function reorderItemsInGroup($groupId, $items)
    {
        $groupId = (int)$groupId;
        $items = (array)$items;

        if (($groupId <= 0) || !$items)
        {
            return false;
        }

        $reordered = true;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();
        $order = 0;

        $dbManager->beginTransaction();

        foreach ($items as $itemId)
        {
            $itemId = (int)$itemId;

            if ($itemId > 0)
            {
                $update = $sql->update(self::BLOCK_IMAGES_PILE_ITEMS_TABLE_NAME);
                $update
                    ->set(array('order' => $order))
                    ->where(
                        array(
                            'item_id' => $itemId,
                            'content_id' => $contentId,
                            'group_id' => $groupId
                        )
                    );
                $result = $dbManager->executePreparedStatement($update);
                if (!$result)
                {
                    $reordered = false;
                    break;
                }

                $item = $this->getItemById($itemId);
                if ($item->order != $order)
                {
                    $reordered = false;
                    break;
                }

                $order++;
            }
        }

        if ($reordered)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $reordered;
    }

    public function toggleGroupPublished($groupId, $published)
    {
        $groupId = (int)$groupId;
        $published = (int)$published ? 1 : 0;

        if ($groupId <= 0)
        {
            return false;
        }

        $updated = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $group = $this->getGroupById($groupId);
        if ($group)
        {
            $update = $sql->update(self::BLOCK_IMAGES_PILE_GROUPS_TABLE_NAME);
            $update
                ->set(
                    array(
                        'is_published' => $published
                    )
                )
                ->where(
                    array(
                        'group_id' => $groupId,
                        'content_id' => $this->getContentId()
                    )
                );
            $result = $dbManager->executePreparedStatement($update);
            if ($result)
            {
                $group = $this->getGroupById($groupId);
                if ($group && ($group->is_published == $published))
                {
                    $updated = true;
                }
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function toggleItemPublished($itemId, $published)
    {
        $itemId = (int)$itemId;
        $published = (int)$published ? 1 : 0;

        if ($itemId <= 0)
        {
            return false;
        }

        $updated = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $item = $this->getItemById($itemId);
        if ($item)
        {
            $update = $sql->update(self::BLOCK_IMAGES_PILE_ITEMS_TABLE_NAME);
            $update
                ->set(
                    array(
                        'is_published' => $published,
                        'content_id' => $this->getContentId()
                    )
                )
                ->where(array('item_id' => $itemId));
            $result = $dbManager->executePreparedStatement($update);
            if ($result)
            {
                $item = $this->getItemById($itemId);
                if ($item && ($item->is_published == $published))
                {
                    $updated = true;
                }
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    private function createNewGroup($groupData)
    {
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $insert = $sql->insert(self::BLOCK_IMAGES_PILE_GROUPS_TABLE_NAME);
        $insert
            ->columns(
                array(
                    'content_id',
                    'is_published',
                    'last_update_author_id',
                    'last_update_date',
                    'title'
                )
            )
            ->values(
                array(
                    'content_id' => $contentId,
                    'is_published' => $groupData->groupIsPublished,
                    'last_update_author_id' => $groupData->groupLastUpdateAuthorId,
                    'last_update_date' => $groupData->groupLastUpdateDate,
                    'title' => $groupData->groupTitle
                )
            );
        $groupId = $dbManager->executeInsertAndGetLastInsertId($insert);

        if ($groupId > 0)
        {
            $dbManager->commit();

            return $groupId;
        }

        $dbManager->rollback();

        return false;
    }

    private function updateExistingGroup($groupData)
    {
        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_IMAGES_PILE_GROUPS_TABLE_NAME);
        $update
            ->set(
                array(
                    'is_published' => $groupData->groupIsPublished,
                    'last_update_author_id' => $groupData->groupLastUpdateAuthorId,
                    'last_update_date' => $groupData->groupLastUpdateDate,
                    'title' => $groupData->groupTitle
                )
            )
            ->where(
                array(
                    'group_id' => $groupData->groupId,
                    'content_id' => $contentId
                )
            );
        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $group = $this->getGroupById($groupData->groupId);
            if ($group &&
                ($group->is_published == $groupData->groupIsPublished) &&
                ($group->last_update_author_id == $groupData->groupLastUpdateAuthorId) &&
                ($group->title == $groupData->groupTitle)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    private function createNewItem($itemData)
    {
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $insert = $sql->insert(self::BLOCK_IMAGES_PILE_ITEMS_TABLE_NAME);
        $insert
            ->columns(
                array(
                    'content_id',
                    'group_id',
                    'page_id',
                    'attachment_id',
                    'is_published',
                    'url_address_type',
                    'last_update_author_id',
                    'last_update_date',
                    'external_url',
                    'title',
                )
            )
            ->values(
                array(
                    'content_id' => $contentId,
                    'group_id' => $itemData->itemGroupId,
                    'page_id' => $itemData->itemPageId,
                    'attachment_id' => $itemData->itemAttachmentId,
                    'is_published' => $itemData->itemIsPublished,
                    'url_address_type' => $itemData->itemUrlAddressType,
                    'last_update_author_id' => $itemData->itemLastUpdateAuthorId,
                    'last_update_date' => $itemData->itemLastUpdateDate,
                    'external_url' => $itemData->itemExternalUrl,
                    'title' => $itemData->itemTitle
                )
            );
        $itemId = $dbManager->executeInsertAndGetLastInsertId($insert);
        if ($itemId > 0)
        {
            $dbManager->commit();

            return $itemId;
        }

        $dbManager->rollback();

        return false;
    }

    private function updateExistingItem($itemData)
    {
        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_IMAGES_PILE_ITEMS_TABLE_NAME);
        $update
            ->set(
                array(
                    'group_id' => $itemData->itemGroupId,
                    'page_id' => $itemData->itemPageId,
                    'attachment_id' => $itemData->itemAttachmentId,
                    'is_published' => $itemData->itemIsPublished,
                    'url_address_type' => $itemData->itemUrlAddressType,
                    'last_update_author_id' => $itemData->itemLastUpdateAuthorId,
                    'last_update_date' => $itemData->itemLastUpdateDate,
                    'external_url' => $itemData->itemExternalUrl,
                    'title' => $itemData->itemTitle
                )
            )
            ->where(
                array(
                    'item_id' => $itemData->itemId,
                    'content_id' => $contentId
                )
            );

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $item = $this->getItemById($itemData->itemId);
            if ($item &&
                ($item->group_id == $itemData->itemGroupId) &&
                ($item->page_id == $itemData->itemPageId) &&
                ($item->attachment_id == $itemData->itemAttachmentId) &&
                ($item->is_published == $itemData->itemIsPublished) &&
                ($item->url_address_type == $itemData->itemUrlAddressType) &&
                ($item->last_update_author_id == $itemData->itemLastUpdateAuthorId) &&
                ($item->external_url == $itemData->itemExternalUrl) &&
                ($item->title == $itemData->itemTitle)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }
}
<?php
namespace Site\Block\ImagesPile;

use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\I18n\View\Helper\Translate;
use Site\Controller\AttachmentManager;
use Site\Controller\PageManager;

class ImageHydrator implements HydratorInterface
{
    protected $attachmentManager = null;
    protected $imagesPile = null;
    protected $pageManager = null;
    protected $translateHelper = null;

    public function __construct(
        Translate $translateHelper, AttachmentManager $attachmentManager, PageManager $pageManager,
        ImagesPile $imagesPile
    )
    {
        $this->attachmentManager = $attachmentManager;
        $this->imagesPile = $imagesPile;
        $this->pageManager = $pageManager;
        $this->translateHelper = $translateHelper;
    }

    public function extract($object) {}

    public function hydrate(array $data, $object)
    {
        foreach ($data as $property => $value)
        {
            $object[$property] = $value;
        }

        $imagesPile = $this->imagesPile;
        $translate = $this->translateHelper;

        if ($object['url_address_type'] == $imagesPile::ITEM_URL_ADDRESS_TYPE_INTERNAL)
        {
            $object['pageUrl'] = $this->pageManager->getFullPageCleanUrlByPageId($object['page_id']);
        }
        else
        {
            $object['pageUrl'] = $object['external_url'];
        }

        $object['pageBreadcrumb'] = $this->pageManager->getPageTextBreadcrumbByPageId($object['page_id']);

        $defaultThumbnailUrl = '/img/no-image.png';
        $object['thumbUrl'] = $defaultThumbnailUrl;
        if ($object['attachment_id'] > 0)
        {
            $attachment = $this->attachmentManager->getAttachmentById($object['attachment_id']);
            if ($attachment)
            {
                $object['thumbUrl'] =
                    '/eng/get-file/index,' .
                    $attachment->hash_for_file_name .
                    ',' .
                    $attachment->original_file_name;
            }
        }

        $object['thumbAlt'] = $translate('No thumbnail');
        if ($object['thumbUrl'] != $defaultThumbnailUrl)
        {
            $object['thumbAlt'] = $object['title'];
            if ($object['thumbAlt'] == '')
            {
                $object['thumbAlt'] = 'Thumbnail';
            }
        }

        return $object;
    }
}
<?php
namespace Site\Block\SmallBanner;

use Zend\Stdlib\Hydrator\HydratorInterface;
use Site\Controller\AttachmentManager;

class SmallBannerHydrator implements HydratorInterface
{
    protected $attachmentManager = null;

    public function __construct(AttachmentManager $attachmentManager)
    {
        $this->attachmentManager = $attachmentManager;
    }

    public function extract($object) {}

    public function hydrate(array $data, $object)
    {
        foreach ($data as $property => $value)
        {
            $object[$property] = $value;
        }

        $attachment = $this->attachmentManager->getAttachmentById($object->attachment_id);
        if ($attachment)
        {
            $object['image_url'] =
                '/eng/get-file/index,' .
                $attachment->hash_for_file_name .
                ',' .
                $attachment->original_file_name;
        }
        else
        {
            $object['image_url'] = '';
        }

        return $object;
    }
} 
<?php
namespace Site\Block\SmallBanner;

use Site\Block\AbstractBlock;
use Zend\Http\Request;

class SmallBanner extends AbstractBlock
{
    const BLOCK_SMALL_BANNER_TABLE_NAME = 'block_banner_small';

    protected $hasManagementWindow = true;
    protected $hasOwnSettings = true;

    /**
     * Events.
     */

    public function onBeforeDeleted()
    {
        if (!$this->blockData->is_dynamic)
        {
            // Delete all this block's data from database etc.
            $this->deleteAllBanners();
        }
    }

    /**
     * Render-functions.
     */

    public function childRender()
    {
        $dbManager = $this->getDatabaseManager();
        $dbAdapter = $dbManager->getAdapter();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();
        if (!$contentId)
        {
            throw new \Site\Exception\CmsException('SmallBannerBlock (id: ' . $this->blockData->page_block_id . ') has no associated content.');
        }

        $select = $sql->select();
        $select
            ->from(self::BLOCK_SMALL_BANNER_TABLE_NAME)
            ->where(
                array(
                    'content_id' => $contentId,
                    'is_published' => 1
                )
            )
            ->order('order ASC');

        $resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet(
            new SmallBannerHydrator($this->attachmentManager),
            new \Site\Custom\FlexibleContainer()
        );

        $resultSetPrototype->buffer();
        $paginatorAdapter = new \Zend\Paginator\Adapter\DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new \Zend\Paginator\Paginator($paginatorAdapter);
        $countOnlyPublishBanners = true;
        $numBanners = $this->getAssociatedBannerCount($countOnlyPublishBanners);
        $this->blockView->setVariables(
            array(

                'contentId' => $contentId,
                'smallBannerPaginator' => $paginator,
                'numBanners' => $numBanners
            )
        );
    }

    public function childRenderAddEdit(Request $request)
    {
        if (!$this->addEditView instanceof \Zend\View\Model\ViewModel)
        {
            return false;
        }

        $contentId = $this->getContentId();

        $defaultFormData = array(
            'bannerId' => '',
            'bannerAttachment' =>array(
                '0' => array(
                    'imageId' => null,
                    'isMain' => true,
                    'attachmentId' => null,
                    'uniqueHashForFileName' => '',
                    'originalFileName' => '',
                )
            ),
            'bannerTitle' => '',
            'bannerContents' => '',
            'bannerIsPublished' => 0,
            'contentId' => $contentId
        );
        $currentBannerFormData = array();

        $bannerId = (int)$request->getPost('bannerId');
        if ($bannerId > 0)
        {
            $banner = $this->getBannerById($bannerId);
            $attachmentId = $banner->attachment_id;
            $attachment = $this->attachmentManager->getAttachmentById($attachmentId);

            $currentBannerFormData = array(
                'bannerId' => $banner->banner_id,
                'bannerAttachment' =>array(
                    '0' => array(
                        'imageId' => false,
                        'isMain' => true,
                        'attachmentId' => $banner->attachment_id,
                        'uniqueHashForFileName' => $attachment ? $attachment->hash_for_file_name : '',
                        'originalFileName' => $attachment ? $attachment->original_file_name : '',
                    )
                ) ,
                'bannerTitle' => $banner->title,
                'bannerContents' => $banner->contents,
                'bannerIsPublished' => $banner->is_published,
                'contentId' => $contentId
            );
        }

        $formData = array_merge($defaultFormData, $currentBannerFormData);
        $numBanners = $this->getAssociatedBannerCount();
        $this->addEditView->setVariables(
            array(
                'formData' => $formData,
                'maxLengths' => 350,
                'numBanners' => $numBanners
            )
        );
    }

    public function childRenderManagement(Request $request)
    {
        if (!$this->managementView instanceof \Zend\View\Model\ViewModel)
        {
            return false;
        }

        $pageNumber = $this->managementView->pageNumber;
        $itemCountPerPage = $this->managementView->itemCountPerPage;
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $dbAdapter = $dbManager->getAdapter();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(array('bbs' => self::BLOCK_SMALL_BANNER_TABLE_NAME))
            ->join(
                array('u' => 'users'),
                'u.user_id = bbs.last_update_author_id',
                array('last_update_author' => 'login'),
                $select::JOIN_LEFT
            )
            ->where(array('bbs.content_id' => $contentId))
            ->order('bbs.order ASC');
        $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        $paginatorAdapter = new \Zend\Paginator\Adapter\DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new \Zend\Paginator\Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($pageNumber);
        $paginator->setItemCountPerPage($itemCountPerPage);

        $this->managementView->setVariables(
            array(
                'smallBannerPaginator' => $paginator
            )
        );
    }

    /**
     * Getters.
     */

    public function getAssociatedBannerCount($onlyPublished = false)
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $contentId = $this->getContentId();
        if ($onlyPublished)
        {
            $select = $sql->select();
            $select
                ->from(self::BLOCK_SMALL_BANNER_TABLE_NAME)
                ->where(
                    array(
                        'content_id' => $contentId,
                        'is_published' => 1
                    )
                )
                ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')));
            $resultSet = $dbManager->getHydratedResultSet($select);
        }
        else
        {
            $select = $sql->select();
            $select
                ->from(self::BLOCK_SMALL_BANNER_TABLE_NAME)
                ->where(
                    array(
                        'content_id' => $contentId,
                    )
                )
                ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')));
            $resultSet = $dbManager->getHydratedResultSet($select);
        }
        $numOfBannersInDatabaseTable = $resultSet->current();
        if (!$numOfBannersInDatabaseTable)
        {
            throw new \Site\Exception\CmsException('Database error: Could not count number of elements in block-small-banner table.');
        }

        return $numOfBannersInDatabaseTable->count;
    }

    public function getBannerById($bannerId)
    {
        $bannerId = (int)$bannerId;

        if ($bannerId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_SMALL_BANNER_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('banner_id' => $bannerId));
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet->current();
    }

    /**
     * Data formatting functions.
     */

    public function validateBannerData($bannerObject)
    {
        $banner = new \Site\Custom\FlexibleContainer($bannerObject);
        $bannerCopy = clone $banner;

        $bannerCopy->bannerId = (int)$banner->bannerId ? : null;
        $bannerCopy->bannerIsPublished = (int)$bannerCopy->bannerIsPublished ? 1 : 0;
        $bannerCopy->bannerOrder = (int)$bannerCopy->bannerOrder;
        if ($bannerCopy->bannerOrder < 0)
        {
            $bannerCopy->bannerOrder = 0;
        }

        $bannerCopy->bannerTitle = trim((string)$banner->bannerTitle);
        $bannerCopy->bannerContents = trim((string)$banner->bannerContents);

        /**
         * Temporary data - from add-form. They will be parsed and deleted.
         */

        /**
         * Validating form data.
         */

        /**
         * Identifying the author.
         */
        $bannerCopy->bannerLastUpdateAuthorId = $this->user->user_id;

        /**
         * Removing unnecessary data.
         */

        if (
            (($bannerCopy->bannerId !== null) && ($bannerCopy->bannerId <= 0)) ||
            ($bannerCopy->bannerTitle == '')
        )
        {
            return false;
        }

        return $bannerCopy;
    }

    /**
     * Logic-functions.
     */

    public function createBanner($bannerObject)
    {
        $banner = $this->validateBannerData($bannerObject);
        if (!$banner)
        {
            return false;
        }
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $contentId = $this->getContentId();
        $attachmentId = '';
        $isPublished = $banner['bannerIsPublished'] ? 1 : 0;
        $title = $banner['bannerTitle'];
        $contents = $banner['bannerContents'];
        $uniqueHash = $banner['bannerAttachment'][0]['uniqueHashForFileName'];
        $authorId = $this->user->user_id;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        if ($uniqueHash != '')
        {
            $attachmentManager = $this->getAttachmentManager();
            $attachment = $attachmentManager->getAttachmentByUniqueHashForFileName($uniqueHash);
            if (!$attachment)
            {
                return false;
            }

            $attachmentId = $attachment->attachment_id;
        }
        else
        {
            return false;
        }
        $insert = $sql->insert(self::BLOCK_SMALL_BANNER_TABLE_NAME);
        $insert
            ->columns(
                array(
                    'content_id',
                    'is_published',
                    'attachment_id',
                    'last_update_author_id',
                    'last_update_date',
                    'title',
                    'contents'
                )
            )
            ->values(
                array(
                    'content_id' => $contentId,
                    'is_published' => $isPublished,
                    'attachment_id' => $attachmentId,
                    'last_update_author_id' => $authorId,
                    'last_update_date' => $lastUpdateDate,
                    'title' => $title,
                    'contents' => $contents
                )
            );
        $result = $dbManager->executeInsertAndGetLastInsertId($insert);

        return $result;
    }

    public function decideWhatToDoWithRecentlyUploadedAndMovedFiles(\Zend\Http\Request $request, $movedFiles)
    {
        $postData = $request->getPost();

        // Inform the UploadController and UploadManager than upload was successful and you accept these files:
        return true;
    }

    public function deleteAllBanners()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $contentId = $this->getContentId();

        $delete = $sql->delete(self::BLOCK_SMALL_BANNER_TABLE_NAME);
        $delete->where(array('content_id' => $contentId));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;
    }

    public function deleteBannerById($bannerId)
    {
        $bannerId = (int)$bannerId;

        if ($bannerId <= 0)
        {
            return false;
        }

        $deleted = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $delete = $sql->delete();
        $delete->from(self::BLOCK_SMALL_BANNER_TABLE_NAME)->where(array('banner_id' => $bannerId));
        $result = $dbManager->executePreparedStatement($delete);
        if ($result)
        {
            $banner = $this->getBannerById($bannerId);
            $deleted = !$banner;
        }

        if ($deleted)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $deleted;
    }

    public function reorderBanners($banners)
    {
        $banners = (array)$banners;
        if (!$banners)
        {
            return false;
        }

        $reordered = true;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();
        $order = 0;

        $dbManager->beginTransaction();

        foreach ($banners as $bannerId)
        {
            $bannerId = (int)$bannerId;

            if ($bannerId > 0)
            {
                $update = $sql->update(self::BLOCK_SMALL_BANNER_TABLE_NAME);
                $update
                    ->set(array('order' => $order))
                    ->where(
                        array(
                            'banner_id' => $bannerId,
                            'content_id' => $contentId
                        )
                    );
                $result = $dbManager->executePreparedStatement($update);
                if (!$result)
                {
                    $reordered = false;
                    break;
                }

                $banner = $this->getBannerById($bannerId);
                if ($banner->order != $order)
                {
                    $reordered = false;
                    break;
                }

                $order++;
            }
        }

        if ($reordered)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $reordered;
    }

    public function toggleBannerPublished($bannerId, $published)
    {
        $bannerId = (int)$bannerId;
        $published = (int)$published ? 1 : 0;

        if ($bannerId <= 0)
        {
            return false;
        }

        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $banner = $this->getBannerById($bannerId);
        if ($banner)
        {
            $update = $sql->update(self::BLOCK_SMALL_BANNER_TABLE_NAME);
            $update
                ->set(
                    array(
                        'is_published' => $published
                    )
                )
                ->where(
                    array(
                        'banner_id' => $bannerId,
                        'content_id' => $contentId
                    )
                );
            $result = $dbManager->executePreparedStatement($update);
            if ($result)
            {
                $banner = $this->getBannerById($bannerId);
                if ($banner && ($banner->is_published == $published))
                {
                    $updated = true;
                }
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function updateBanner($bannerObject)
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $banner = $this->validateBannerData($bannerObject);
        if (!$banner)
        {
            return false;
        }

        $bannerId = $banner['smallBannerId'];
        $attachment_id = '';
        $isPublished = $banner['bannerIsPublished'];
        $title = $banner['bannerTitle'];
        $contents = $banner['bannerContents'];
        $contentId = $this->getContentId();
        $uniqueHash = $banner['bannerAttachment'][0]['uniqueHashForFileName'];
        $authorId = $this->user->user_id;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');
        if ($uniqueHash != '')
        {
            $attachmentManager = $this->getAttachmentManager();
            $attachment = $attachmentManager->getAttachmentByUniqueHashForFileName($uniqueHash);
            if (!$attachment)
            {
                return false;
            }

            $attachment_id = $attachment->attachment_id;
        }
        else
        {
            return false;
        }

        $updateStatus = false;
        $update = $sql->update(self::BLOCK_SMALL_BANNER_TABLE_NAME)
            ->set(
                array(
                    'is_published' => $isPublished,
                    'attachment_id' => $attachment_id,
                    'last_update_author_id' => $authorId,
                    'last_update_date' => $lastUpdateDate,
                    'title' => $title,
                    'contents' => $contents
                )
            )
            ->where(
                array(
                    'banner_id' => $bannerId,
                    'content_id' => $contentId
                )
            );
        $result = $dbManager->executePreparedStatement($update);

        if ($result)
        {
            $updateStatus = true;
        }

        return $updateStatus;
    }

    public function updateBannerText($bannerObject)
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $banner = new \Site\Custom\FlexibleContainer($bannerObject);
        $bannerId = $banner['smallBannerId'];
        $contents = $banner['bannerContents'];
        $contentId = $this->getContentId();

        $authorId = $this->user->user_id;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $updateStatus = false;
        $update = $sql->update(self::BLOCK_SMALL_BANNER_TABLE_NAME)
            ->set(
                array(
                    'last_update_author_id' => $authorId,
                    'last_update_date' => $lastUpdateDate,
                    'contents' => $contents
                )
            )
            ->where(
                array(
                    'banner_id' => $bannerId,
                    'content_id' => $contentId
                )
            );
        $result = $dbManager->executePreparedStatement($update);

        if ($result)
        {
            $updateStatus = true;
        }

        return $updateStatus;
    }
}
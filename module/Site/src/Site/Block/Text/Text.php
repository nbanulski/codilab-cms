<?php
namespace Site\Block\Text;

use Site\Block\AbstractBlock;

class Text extends AbstractBlock
{
    /**
     * Events.
     */

    public function onBeforeDeleted()
    {
        if (!$this->blockData->is_dynamic)
        {
            $this->deleteBlockRecordFromSearchIndex();
        }
    }

    public function updateSearchIndex()
    {
        $blockData = $this->getBlockData();

        $this->updateBlockRecordInSearchIndex(
            null, '', '', 'Static content', false, $blockData->content->content
        );
    }

    /**
     * Render-functions.
     */

    public function childRender(\Zend\Http\Request $request)
    {
        if ($this->blockData->content->content == '')
        {
            $blockManager = $this->getBlockManager();
            $block = $blockManager->getBlockPropertiesByBlockIdAndLanguageId(
                $this->blockData->block_id, $this->language->language_id
            );

            if ($block)
            {
                $this->blockData->content->content = $block->default_content;
            }
        }
    }
}
<?php
namespace Site\Block\TabsControl;

use Site\Block\AbstractBlock;
use Zend\Http\Request;

class TabsControl extends AbstractBlock
{
    const BLOCK_TABS_CONTROL_TABLE_NAME = 'block_tabs_control';

    /**
     * Events.
     */

    public function onBeforeDeleted()
    {
        if (!$this->blockData->is_dynamic)
        {
            // Delete all this block's data from database etc.
            $this->deleteAllTabs();
            $this->deleteBlockRecordFromSearchIndex();
        }
    }

    public function updateSearchIndex()
    {
        $allTabs = $this->getAllTabs();

        if ($allTabs)
        {
            foreach ($allTabs as $tab)
            {
                $eventCleanUrl = $this->generateCleanUrlFromTabTitle($tab->name);
                $elementUrlParametersString = $eventCleanUrl . '-' . $tab->tab_id;

                $this->updateBlockRecordInSearchIndex(
                    $tab->tab_id, $tab->contents, $elementUrlParametersString, 'Static content', $tab->last_update_date
                );
            }
        }
    }

    /**
     * Render-functions.
     */

    public function childRender(\Zend\Http\Request $request)
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();

        $parameters = $this->getParametersForCurrentPageFromRequest($request);
        $tabCleanUrlWithTabId = $parameters->key();
        list($tabCleanUrl, $currentTabId) =
            $this->getTabCleanUrlAndTabIdFromTabCleanUrlWithTabId($tabCleanUrlWithTabId);

        $select = $sql->select();
        $select
            ->from(self::BLOCK_TABS_CONTROL_TABLE_NAME)
            ->where(array('content_id' => $contentId))
            ->order('order ASC,name ASC');
        $resultSet = $dbManager->getHydratedResultSet($select)->buffer();
        $tabs = $resultSet;

        if ($tabs)
        {
            $tabId = $this->blockView->getVariable('tabId');
            if (!$tabId)
            {
                $tabId = $currentTabId;
            }

            $isFirst = true;
            foreach ($tabs as $tab)
            {
                $tab->additional_css_classes = '';
                $tab->is_active = false;

                if (
                    ($isFirst && !$tabId) ||
                    (($tabId > 0) && ($tabId == $tab->tab_id))
                )
                {
                    $tab->additional_css_classes = ' active';
                    $tab->is_active = true;
                }

                $isFirst = false;
            }
        }

        $this->blockView->setVariables(
            array(
                'tabs' => $tabs
            )
        );
    }

    /**
     * Getters.
     */

    public function getAllTabs()
    {
        $prefixColumnsWithTable = false;
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_TABS_CONTROL_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getMaxTabsOrder()
    {
        $prefixColumnsWithTable = false;
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $dbPlatform = $dbManager->getAdapter()->getPlatform();
        $sql = $dbManager->getSql();

        $orderColumnQuoted = $dbPlatform->quoteIdentifier('order');

        $select = $sql->select();
        $select
            ->from(self::BLOCK_TABS_CONTROL_TABLE_NAME)
            ->columns(
                array(
                    'max_order' => new \Zend\Db\Sql\Expression('MAX(' . $orderColumnQuoted . ')')
                ),
                $prefixColumnsWithTable
            )
            ->where(
                array(
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();

            return $row->max_order;
        }

        return false;
    }

    public function getTabById($tabId)
    {
        $tabId = (int)$tabId;

        if ($tabId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_TABS_CONTROL_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'tab_id' => $tabId,
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getTabCount()
    {
        $prefixColumnsWithTable = false;
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_TABS_CONTROL_TABLE_NAME)
            ->columns(
                array(
                    'count' => new \Zend\Db\Sql\Expression('COUNT(*)')
                ),
                $prefixColumnsWithTable
            )
            ->where(
                array(
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();

            return $row->count;
        }

        return false;
    }

    public function getTabCleanUrlAndTabIdFromTabCleanUrlWithTabId($tabCleanUrlWithTabId)
    {
        $parts = explode('-', $tabCleanUrlWithTabId);

        $contentId = array_pop($parts);
        $tabCleanUrl = join('-', $parts);

        return array($tabCleanUrl, $contentId);
    }

    /**
     * Data formatting functions.
     */

    public function generateCleanUrlFromTabTitle($tabTitle)
    {
        return $this->generateCleanUrlFromText($tabTitle);
    }

    public function formatTabDataAndReturnIfAreValid($tabData)
    {
        $tabData = $this->convertToFlexibleContainer($tabData);
        $tabDataCopy = clone $tabData;

        $tabDataCopy->tabId = (int)$tabData->tabId ? : null;
        $tabDataCopy->tabIsPublished = (int)$tabDataCopy->tabIsPublished ? 1 : 0;
        $tabDataCopy->tabOrder = (int)$tabDataCopy->tabOrder;
        if ($tabDataCopy->tabOrder < 0)
        {
            $tabDataCopy->tabOrder = 0;
        }
        $tabDataCopy->tabName = trim((string)$tabData->tabName);
        $tabDataCopy->tabContents = trim((string)$tabData->tabContents);

        /**
         * Temporary data - from add-form. They will be parsed and deleted.
         */

        /**
         * Validating form data.
         */

        /**
         * Identifying the author.
         */
        $tabDataCopy->tabLastUpdateAuthorId = $this->user->user_id;

        /**
         * Removing unnecessary data.
         */

        if (
            (($tabDataCopy->tabId !== null) && ($tabDataCopy->tabId <= 0)) ||
            ($tabDataCopy->tabName == '')
        )
        {
            return false;
        }

        return $tabDataCopy;
    }

    /**
     * Logic-functions.
     */

    public function addOrEditTab($tabData)
    {
        $tabDataCopy = $this->formatTabDataAndReturnIfAreValid($tabData);
        if (!$tabDataCopy)
        {
            return false;
        }

        $tabDataCopy->tabLastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        if ($tabData->tabId > 0)
        {
            return $this->updateExistingTab($tabDataCopy);
        }
        else
        {
            return $this->createNewTab($tabDataCopy);
        }
    }

    public function deleteAllTabs()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_TABS_CONTROL_TABLE_NAME);
        $delete->where(array('content_id' => $this->getContentId()));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;
    }

    public function deleteTabById($tabId)
    {
        $tabId = (int)$tabId;

        if ($tabId <= 0)
        {
            return false;
        }

        $deleted = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_TABS_CONTROL_TABLE_NAME);
        $delete
            ->where(
            array(
                'tab_id' => $tabId,
                'content_id' => $this->getContentId()
            )
        );
        $result = $dbManager->executePreparedStatement($delete);
        if ($result)
        {
            $deleted = true;
        }

        return $deleted;
    }

    public function reorderTabs($tabs)
    {
        $tabs = (array)$tabs;

        if (!$tabs)
        {
            return false;
        }

        $reordered = true;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $order = 0;
        $contentId = $this->getContentId();

        $dbManager->beginTransaction();

        foreach ($tabs as $tabId)
        {
            $tabId = (int)$tabId;

            if ($tabId > 0)
            {
                $update = $sql->update(self::BLOCK_TABS_CONTROL_TABLE_NAME);
                $update
                    ->set(array('order' => $order))
                    ->where(
                        array(
                            'tab_id' => $tabId,
                            'content_id' => $contentId
                        )
                    );
                $result = $dbManager->executePreparedStatement($update);
                if (!$result)
                {
                    $reordered = false;
                    break;
                }

                $tab = $this->getTabById($tabId);
                if ($tab->order != $order)
                {
                    $reordered = false;
                    break;
                }

                $order++;
            }
        }

        if ($reordered)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $reordered;
    }

    public function updateTabName($tabId, $tabName)
    {
        $tabId = (int)$tabId;
        $tabName = trim(strip_tags((string)$tabName));
        $tabName = str_replace("\r\n", ' ', $tabName);
        $tabName = str_replace(array("\r", "\n"), ' ', $tabName);

        if (($tabId <= 0) || ($tabName == ''))
        {
            return false;
        }

        $updated = false;

        $lastUpdateAuthorId = $this->user->user_id ? (int)$this->user->user_id : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_TABS_CONTROL_TABLE_NAME);
        $update
            ->set(
            array(
                'last_update_author_id' => $lastUpdateAuthorId,
                'last_update_date' => $lastUpdateDate,
                'name' => $tabName
            )
        )
            ->where(array('tab_id' => $tabId));

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $tab = $this->getTabById($tabId);
            if ($tab &&
                ($tab->last_update_author_id == $lastUpdateAuthorId) &&
                ($tab->name == $tabName)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function updateTabContents($tabId, $tabContents)
    {
        $tabId = (int)$tabId;
        $tabContents = trim((string)$tabContents);

        if ($tabId <= 0)
        {
            return false;
        }

        $updated = false;

        $lastUpdateAuthorId = $this->user->user_id ? (int)$this->user->user_id : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_TABS_CONTROL_TABLE_NAME);
        $update
            ->set(
            array(
                'last_update_author_id' => $lastUpdateAuthorId,
                'last_update_date' => $lastUpdateDate,
                'contents' => $tabContents
            )
        )
            ->where(array('tab_id' => $tabId));

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $tab = $this->getTabById($tabId);
            if ($tab &&
                ($tab->last_update_author_id == $lastUpdateAuthorId) &&
                ($tab->contents == $tabContents)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $eventCleanUrl = $this->generateCleanUrlFromTabTitle($tab->name);
            $elementUrlParametersString = $eventCleanUrl . '-' . $tabId;

            $this->updateBlockRecordInSearchIndex($tabId, $tabContents, $elementUrlParametersString, 'Static content');

            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    private function createNewTab($tabData)
    {
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $insert = $sql->insert(self::BLOCK_TABS_CONTROL_TABLE_NAME);
        $insert
            ->columns(
                array(
                    'content_id',
                    'is_published',
                    'order',
                    'last_update_author_id',
                    'last_update_date',
                    'name',
                    'contents'
                )
            )
            ->values(
                array(
                    'content_id' => $contentId,
                    'is_published' => $tabData->tabIsPublished,
                    'order' => $tabData->tabOrder,
                    'last_update_author_id' => $tabData->tabLastUpdateAuthorId,
                    'last_update_date' => $tabData->tabLastUpdateDate,
                    'name' => $tabData->tabName,
                    'contents' => $tabData->tabContents
                )
            );
        $tabId = $dbManager->executeInsertAndGetLastInsertId($insert);

        if ($tabId > 0)
        {
            $eventCleanUrl = $this->generateCleanUrlFromTabTitle($tabData->tabName);
            $elementUrlParametersString = $eventCleanUrl . '-' . $tabId;

            $this->updateBlockRecordInSearchIndex(
                $tabId, $tabData->tabContents, $elementUrlParametersString, 'Static content'
            );

            $dbManager->commit();

            return $tabId;
        }

        $dbManager->rollback();

        return false;
    }

    private function updateExistingTab($tabData)
    {
        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_TABS_CONTROL_TABLE_NAME);
        $update
            ->set(
            array(
                'is_published' => $tabData->tabIsPublished,
                'order' => $tabData->tabOrder,
                'last_update_author_id' => $tabData->tabLastUpdateAuthorId,
                'last_update_date' => $tabData->tabLastUpdateDate,
                'name' => $tabData->tabName,
                'contents' => $tabData->tabContents
            )
        )
            ->where(
                array(
                    'tab_id' => $tabData->tabId,
                    'content_id' => $contentId
                )
            );

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $tab = $this->getTabById($tabData->tabId);
            if ($tab &&
                ($tab->is_published == $tabData->tabIsPublished) &&
                ($tab->tabOrder == $tabData->tabOrder) &&
                ($tab->last_update_author_id == $tabData->tabLastUpdateAuthorId) &&
                ($tab->name == $tabData->tabName) &&
                ($tab->contents == $tabData->tabContents)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $eventCleanUrl = $this->generateCleanUrlFromTabTitle($tabData->tabName);
            $elementUrlParametersString = $eventCleanUrl . '-' . $tabData->tabId;

            $this->updateBlockRecordInSearchIndex(
                $tabData->tabId, $tabData->tabContents, $elementUrlParametersString, 'Static content'
            );

            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }
}
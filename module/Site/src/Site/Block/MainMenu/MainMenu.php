<?php
namespace Site\Block\MainMenu;

use Site\Block\NavigationMenu\NavigationMenu;
use Zend\Http\Request;

class MainMenu extends NavigationMenu
{
    const BLOCK_MAIN_MENU_INFORMATION_DATABASE_TABLE = "block_main_menu_information";
    
    public function onBeforeDeleted()
    {
        parent::onBeforeDeleted();
        
        if (!$this->blockData->is_dynamic)
        {           
            $this->deleteAllInfo();
        }
    }
    
    public function init()
    {
        $className = basename(str_replace('\\', '/', get_class($this)));
        $filter = new \Zend\Filter\Word\CamelCaseToDash();
        $this->blockViewName = mb_strtolower($filter->filter($className));
    }

    public function childRender(Request $request)
    {
        $settings = $this->getSettings();

        $pageCleanUrl = $this->getRouteParametersFromRequest($request)->action;
        $subPageCleanUrl = $this->getSubPageCleanUrlFromRequest($request);

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $metadata = new \Zend\Db\Metadata\Metadata($dbManager->getAdapter());
        $blockNavigationMenuItemsTableColumnsMetadata = $metadata->getColumns(
            self::BLOCK_NAVIGATION_MENU_ITEMS_TABLE_NAME
        );
        $maxLengths = array();
        if ($blockNavigationMenuItemsTableColumnsMetadata)
        {
            foreach ($blockNavigationMenuItemsTableColumnsMetadata as $column)
            {
                $name = $column->getName();

                $maxLengths[$name] = $column->getCharacterMaximumLength();
            }
        }

        $contentId = $this->getContentId();

        $select = $sql->select();
        $select
            ->from(array('bnmi' => self::BLOCK_NAVIGATION_MENU_ITEMS_TABLE_NAME))
            ->join(
                array('bnmc' => self::BLOCK_NAVIGATION_MENU_CATEGORIES_TABLE_NAME),
                'bnmc.category_id = bnmi.category_id',
                array('category_title' => 'title'),
                $select::JOIN_LEFT
            )
            ->join(
                array('u' => 'users'),
                'u.user_id = bnmi.last_update_author_id',
                array('last_update_author' => 'login'),
                $select::JOIN_LEFT
            )
            ->where(
                array(
                    'bnmc.category_id' => (int)$settings->categoryId,
                    'bnmc.is_published' => 1,
                    'bnmi.content_id' => $contentId,
                    'bnmi.is_published' => 1
                )
            )
            ->order('bnmi.parent_id ASC, bnmi.order ASC');
        $resultSet = $dbManager->getHydratedResultSet($select)->buffer();

        // We need three copies of items, because multiple traversing on the same object will not work as expected.
        // We have to convert object into array and create three copies for traversing on different arrays instead on
        // one object.
        $items = array();
        foreach ($resultSet as $item)
        {
            $items[] = $item;
        }
        $items2 = $items;
        $items3 = $items;

        // $resultSet is not needed anymore.
        unset($resultSet);

        $pageManager = $this->getPageManager();
        if($pageManager->getPageIdByCleanUrls($pageCleanUrl, $subPageCleanUrl)){
            $currentPageId = $pageManager->getPageIdByCleanUrls($pageCleanUrl, $subPageCleanUrl)->page_id;
            if($subPageCleanUrl == '')
            {
                $currentPageId = $pageManager->getPageIdByParentPageCleanUrl($pageCleanUrl);
            }
        }else{
            $currentPageId = $pageManager->getPageIdByParentPageCleanUrl($pageCleanUrl);
        }
        $currentPageCleanUrl = $pageCleanUrl;
        if ($subPageCleanUrl != '')
        {
            $currentPageCleanUrl = $subPageCleanUrl;
        }

        $itemTree = new \Site\Custom\FlexibleContainer();
        foreach ($items as $item)
        {
            if (!$item->parent_id)
            {
                $itemId = (int)$item->item_id;
                $thereExistsAnActiveItem = false;

                $attachment = $this->attachmentManager->getAttachmentById($item->attachment_id);

                if ($attachment)
                {
                    $item->image_url = '/eng/get-file/index/,' . $attachment->hash_for_file_name . ',' . $attachment->original_file_name;
                }

                if ($item->url_address_type == self::ITEM_URL_ADDRESS_TYPE_INTERNAL)
                {
                    $item->full_url = $pageManager->getFullPageCleanUrlByPageId($item->page_id);
                }
                else
                {
                    $item->full_url = $item->external_url;
                }

                $item->additionalCssClasses = '';

                //Side Information Panel
                $sideInformation = $this->getInfoByItemId($itemId);
                if($sideInformation)
                {
                    $item->sideInformation = $sideInformation;
                }
                else
                {                      
                    $sideInfoData = $this->generateSideInformationForItem($itemId);
                    $sideInfoData->infoTitle = $item->title;
                    $sideInfoDataId =  $this->createNewInfoAndReturnId($sideInfoData); 
                    $sideInformation = $this->getInfoById($sideInfoDataId);
                    $item->sideInformation = $sideInformation;
                }

                // Second depth:
                $depthOneActiveClassNotFound= true;
                $childItems = new \Site\Custom\FlexibleContainer();
                foreach ($items2 as $itemFromDepth1)
                {
                    $itemFromDepth1Id = (int)$itemFromDepth1->item_id;
                    $itemFromDepth1ParentId = (int)$itemFromDepth1->parent_id;

                    if ($itemFromDepth1ParentId == $itemId)
                    {
                        if ($itemFromDepth1->url_address_type == self::ITEM_URL_ADDRESS_TYPE_INTERNAL)
                        {
                            $itemFromDepth1->full_url = $pageManager->getFullPageCleanUrlByPageId(
                                $itemFromDepth1->page_id
                            );
                        }
                        else
                        {
                            $itemFromDepth1->full_url = $itemFromDepth1->external_url;
                        }

                        $itemFromDepth1->additionalCssClasses = '';

                        // Third (last) depth:
                        $itemFromDepth1Items = new \Site\Custom\FlexibleContainer();
                        foreach ($items3 as $itemFromDepth2)
                        {
                            $itemFromDepth2Id = (int)$itemFromDepth2->item_id;
                            $itemFromDepth2ParentId = (int)$itemFromDepth2->parent_id;

                            if ($itemFromDepth2ParentId == $itemFromDepth1Id)
                            {
                                if ($itemFromDepth2->url_address_type == self::ITEM_URL_ADDRESS_TYPE_INTERNAL)
                                {
                                    $itemFromDepth2->full_url = $pageManager->getFullPageCleanUrlByPageId(
                                        $itemFromDepth2->page_id
                                    );
                                }
                                else
                                {
                                    $itemFromDepth2->full_url = $itemFromDepth2->external_url;
                                }

                                $itemFromDepth2->additionalCssClasses = '';
                                if (
                                    (
                                        ($itemFromDepth2->url_address_type == self::ITEM_URL_ADDRESS_TYPE_INTERNAL) &&
                                        ($itemFromDepth2->page_id == $currentPageId)
                                    ) ||
                                    (
                                        ($itemFromDepth2->url_address_type == self::ITEM_URL_ADDRESS_TYPE_EXTERNAL) &&
                                        ($itemFromDepth2->external_url == $currentPageCleanUrl)
                                    )
                                )
                                {
                                    $thereExistsAnActiveItem = true;
                                    $itemFromDepth2->additionalCssClasses = ' active';
                                }

                                $itemFromDepth1Items[$itemFromDepth2Id] = $itemFromDepth2;
                            }
                        }

                        if (
                            (
                                ($itemFromDepth1->url_address_type == self::ITEM_URL_ADDRESS_TYPE_INTERNAL) &&
                                ($itemFromDepth1->page_id == $currentPageId)
                            ) ||
                            (
                                ($itemFromDepth1->url_address_type == self::ITEM_URL_ADDRESS_TYPE_EXTERNAL) &&
                                ($itemFromDepth1->external_url == $currentPageCleanUrl)
                            )
                            ||
                            $thereExistsAnActiveItem && $depthOneActiveClassNotFound
                        )
                        {
                            $itemFromDepth1->additionalCssClasses = ' active';
                            $depthOneActiveClassNotFound = false;
                            $thereExistsAnActiveItem = true;
                        }

                        if ($itemFromDepth1Items->count() > 0)
                        {
                            $itemFromDepth1->additionalCssClasses .= ' has-childs';
                        }

                        $childItems[$itemFromDepth1Id] = array(
                            'data' => $itemFromDepth1,
                            'childItems' => $itemFromDepth1Items
                        );
                    }
                }
                if (
                    (
                        ($item->url_address_type == self::ITEM_URL_ADDRESS_TYPE_INTERNAL) &&
                        ($item->page_id == $currentPageId)
                    ) ||
                    (
                        ($item->url_address_type == self::ITEM_URL_ADDRESS_TYPE_EXTERNAL) &&
                        ($item->external_url == $currentPageCleanUrl)
                    )
                    || $thereExistsAnActiveItem
                )
                {
                    $item->additionalCssClasses = ' active';
                }

                $itemTree[$itemId] = array(
                    'data' => $item,
                    'childItems' => $childItems
                );
            }
        }

        if ($settings->orientation == '')
        {
            $pageBlockId = $this->blockData->page_block_id;
            $errorMessage = 'NavigationMenu block (id: ' . $pageBlockId . ') has no "orientation" variable in ' .
                'it\'s XML configuration in pages_blocks_contents table, so it is impossible to ' .
                'determine which template should be used to render menu items. The variable should ' .
                'contain "horizontal" or "vertical" value which points to "horizontal.phtml" or ' .
                '"vertical.phtml" template file.';
            throw new \Site\Exception\CmsException($errorMessage);
        }

        $this->blockView->setTemplate('blocks/' . $this->blockViewName . '/main-menu');
        $this->blockView->setVariables(array(
                'maxLengths' => $maxLengths,
                'menuItems' => $itemTree                
            )
        );
    }
    
    public function getInfoByItemId($itemId)
    {
        $itemId = (int)$itemId;

        if ($itemId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_MAIN_MENU_INFORMATION_DATABASE_TABLE)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'item_id' => $itemId,
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }
    
    public function getInfoById($infoId)
    {
        $infoId = (int)$infoId;

        if ($infoId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_MAIN_MENU_INFORMATION_DATABASE_TABLE)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'info_id' => $infoId,
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }
    
    public function formatInfoDataAndReturnIfValid($infoData)
    {
        $infoData = $this->convertToFlexibleContainer($infoData);
        $infoDataCopy = clone $infoData;

        $infoDataCopy->infoId = (int)$infoData->infoId ? : null;
        $infoDataCopy->itemId = (int)$infoData->itemId ? : null;        
        $infoDataCopy->infoTitle = trim((string)$infoData->infoTitle);
        $infoDataCopy->infoContents = trim((string)$infoData->infoContents);

        /**
         * Temporary data - from add-form. They will be parsed and deleted.
         */

        /**
         * Validating form data.
         */

        /**
         * Identifying the author.
         */

        $infoDataCopy->infoLastUpdateAuthorId = $this->user->user_id;

        /**
         * Removing unnecessary data.
         */

        if (
            (($infoDataCopy->infoId !== null) && ($infoDataCopy->infoId <= 0)) ||
            ($infoDataCopy->infoName == '')
        )
        {
            return false;
        }

        return $infoDataCopy;
    }
    
    public function addOrEditInfo($infoData)
    {
        $infoDataCopy = $this->formatinfoDataAndReturnIfValid($infoData);
        if (!$infoDataCopy)
        {
            return false;
        }

        $infoDataCopy->infoLastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        if ($infoData->infolId > 0)
        {
            return $this->updateExistingInfo($infoDataCopy);
        }
        else
        {
            return $this->createNewInfoAndReturnId($infoDataCopy);
        }
    }
    
    private function generateSideInformationForItem($itemId)
    {
        $translate = $this->getTranslateHelper();

        $infoData = new \Site\Custom\FlexibleContainer();
        $infoData->infoId = null;
        $infoData->itemId = (int)$itemId ? : null;        
        $infoData->infoTitle = $translate('Title', 'default', $this->userLanguage->zend2_locale);
        $infoData->infoContents = $translate('Click to edit', 'default', $this->userLanguage->zend2_locale);
        $infoData->infoLastUpdateAuthorId = $this->user->user_id;
        $infoData->infoLastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        return $infoData;
    }
    
    private function createNewInfoAndReturnId($infoData)
    {
        $contentId = $this->getContentId();
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $insert = $sql->insert(self::BLOCK_MAIN_MENU_INFORMATION_DATABASE_TABLE);
        $insert
            ->columns(
                array(
                    'item_id',
                    'content_id',
                    'last_update_author_id',
                    'last_update_date',
                    'title',
                    'contents'
                )
            )
            ->values(
                array(
                    'item_id' => $infoData->itemId,
                    'content_id' => $contentId,
                    'last_update_author_id' => $infoData->infoLastUpdateAuthorId,
                    'last_update_date' => $infoData->infoLastUpdateDate,
                    'title' => $infoData->infoTitle,
                    'contents' => $infoData->infoContents
                )
            );
        $infoId = $dbManager->executeInsertAndGetLastInsertId($insert);

        if ($infoId > 0)
        {
            $dbManager->commit();

            return $infoId;
        }

        $dbManager->rollback();

        return false;
    }

    private function updateExistingInfo($infoData)
    {
        $updated = false;
        $contentId = $this->getContentId();
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_MAIN_MENU_INFORMATION_DATABASE_TABLE);
        $update
            ->set(
                array(
                    'last_update_author_id' => $infoData->infoLastUpdateAuthorId,
                    'last_update_date' => $infoData->infoLastUpdateDate,
                    'title' => $infoData->infoTitle,
                    'contents' => $infoData->infoContents
                )
            )
            ->where(
                array(
                    'info_id' => $infoData->infoId,
                    'item_id' => $infoData->itemId,
                    'content_id' => $contentId
                )
            );

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $info = $this->getTabById($infoData->infoId);
            if ($info &&
                ($info->is_published == $infoData->infoIsPublished) &&                
                ($info->last_update_author_id == $infoData->infoLastUpdateAuthorId) &&
                ($info->title == $infoData->infoTitle) &&
                ($info->contents == $infoData->infoContents)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }
    
    public function updateInfoTitle($infoId, $infoTitle)
    {
        $infoId = (int)$infoId;
        $infoTitle = trim(strip_tags((string)$infoTitle));
        $infoTitle = str_replace("\r\n", ' ', $infoTitle);
        $infoTitle = str_replace(array("\r", "\n"), ' ', $infoTitle);

        if (($infoId <= 0) || ($infoTitle == ''))
        {
            return false;
        }

        $updated = false;

        $lastUpdateAuthorId = $this->user->user_id ? (int)$this->user->user_id : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_MAIN_MENU_INFORMATION_DATABASE_TABLE);
        $update
            ->set(
                array(
                    'last_update_author_id' => $lastUpdateAuthorId,
                    'last_update_date' => $lastUpdateDate,
                    'title' => $infoTitle
                )
            )
            ->where(array('info_id' => $infoId));

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $info = $this->getInfoById($infoId);
            if ($info &&
                ($info->last_update_author_id == $lastUpdateAuthorId) &&
                ($info->title == $infoTitle)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function updateInfoContents($infoId, $infoContents)
    {
        $infoId = (int)$infoId;
        $infoContents = trim((string)$infoContents);

        if ($infoId <= 0)
        {
            return false;
        }

        $updated = false;

        $lastUpdateAuthorId = $this->user->user_id ? (int)$this->user->user_id : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_MAIN_MENU_INFORMATION_DATABASE_TABLE);
        $update
            ->set(
                array(
                    'last_update_author_id' => $lastUpdateAuthorId,
                    'last_update_date' => $lastUpdateDate,
                    'contents' => $infoContents
                )
            )
            ->where(array('info_id' => $infoId));

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $info = $this->getInfoById($infoId);
            if ($info &&
                ($info->last_update_author_id == $lastUpdateAuthorId) &&
                ($info->contents == $infoContents)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }
    
    public function deleteInfoById($infoId)
    {
        $infoId = (int)$infoId;

        if ($infoId <= 0)
        {
            return false;
        }

        $deleted = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_MAIN_MENU_INFORMATION_DATABASE_TABLE);
        $delete
            ->where(
                array(
                    'info_id' => $infoId,
                    'content_id' => $this->getContentId()
                )
            );
        $result = $dbManager->executePreparedStatement($delete);
        if ($result)
        {
            $deleted = true;
        }

        return $deleted;
    }
    
    public function deleteAllInfo()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_MAIN_MENU_INFORMATION_DATABASE_TABLE);
        $delete->where(array('content_id' => $this->getContentId()));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;        
    }
}
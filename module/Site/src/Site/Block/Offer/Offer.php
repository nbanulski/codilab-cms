<?php
namespace Site\Block\Offer;

use Site\Block\AbstractBlock;
use Zend\Http\Request;

class Offer extends AbstractBlock
{
    const BLOCK_OFFER_DEGREES_TABLE_NAME = 'block_offer_degrees';
    const BLOCK_OFFER_MAJORS_TABLE_NAME = 'block_offer_majors';

    protected $hasManagementWindow = true;

    /**
     * Events.
     */

    public function onAfterInserting($slotId)
    {
        return $this->addDefaultDegreesIfNoDegreeExists();
    }

    public function onBeforeDeleted()
    {
        $this->deleteAllMajors();
        $this->deleteAllDegrees();
    }

    /**
     * Render-functions.
     */
    public function childRender(\Zend\Http\Request $request)
    {
        $settings = $this->getSettings();

        $dbManager = $this->getDatabaseManager();
        $dbAdapter = $dbManager->getAdapter();
        $sql = $dbManager->getSql();

        $metadata = new \Zend\Db\Metadata\Metadata($dbManager->getAdapter());
        $majorsTableColumnsMetadata = $metadata->getColumns(self::BLOCK_OFFER_MAJORS_TABLE_NAME);
        $maxLengths = array();
        if ($majorsTableColumnsMetadata)
        {
            foreach ($majorsTableColumnsMetadata as $column)
            {
                $name = $column->getName();

                $maxLengths[$name] = $column->getCharacterMaximumLength();
            }
        }

        $contentId = $this->getContentId();
        $degrees = $this->getDegrees()->buffer();

        $select = $sql->select();
        $select->from(array('bom' => self::BLOCK_OFFER_MAJORS_TABLE_NAME))
            ->join(
                array('bod' => self::BLOCK_OFFER_DEGREES_TABLE_NAME),
                'bod.degree_id = bom.degree_id',
                array('degree_title' => 'title'),
                $select::JOIN_LEFT
            )
            ->join(
                array('u' => 'users'),
                'u.user_id = bom.last_update_author_id',
                array('last_update_author' => 'login'),
                $select::JOIN_LEFT
            )
            ->where(
                array(
                    'bod.is_published' => 1,
                    'bom.content_id' => $contentId,
                    'bom.is_published' => 1
                )
            )
            ->order('bom.order ASC, bom.title ASC');
        $resultSet = $dbManager->getHydratedResultSet($select);

        $this->blockView->setTemplate('blocks/' . $this->blockViewName . '/' . $this->blockViewName);
        $this->blockView->setVariables(
            array(
                'maxLengths' => $maxLengths,
                'degrees' => $degrees,
                'majors' => $resultSet
            )
        );
    }

    public function childRenderManagement(\Zend\Http\Request $request)
    {
        if (!$this->managementView instanceof \Zend\View\Model\ViewModel)
        {
            return false;
        }

        $pageNumber = $this->managementView->pageNumber;
        $itemCountPerPage = $this->managementView->itemCountPerPage;

        $dbManager = $this->getDatabaseManager();
        $dbAdapter = $dbManager->getAdapter();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();

        $select = $sql->select();
        $select->from(array('bom' => self::BLOCK_OFFER_MAJORS_TABLE_NAME))
            ->join(
                array('bod' => self::BLOCK_OFFER_DEGREES_TABLE_NAME),
                'bod.degree_id = bom.degree_id',
                array('degree_title' => 'title'),
                $select::JOIN_LEFT
            )
            ->join(
                array('u' => 'users'),
                'u.user_id = bom.last_update_author_id',
                array('last_update_author' => 'login'),
                $select::JOIN_LEFT
            )
            ->where(array('bom.content_id' => $contentId))
            ->order('bom.order ASC, bom.title ASC');
        $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        $paginatorAdapter = new \Zend\Paginator\Adapter\DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new \Zend\Paginator\Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($pageNumber);
        $paginator->setItemCountPerPage($itemCountPerPage);

        $this->managementView->setVariables(
            array(
                'degreeCount' => $this->getDegreeCount(),
                'majorsPaginator' => $paginator
            )
        );
    }

    public function childRenderAddEdit(\Zend\Http\Request $request)
    {
        if (!$this->addEditView instanceof \Zend\View\Model\ViewModel)
        {
            return false;
        }

        $pageManager = $this->getPageManager();

        $degreeId = null;
        $major = null;
        $majorId = (int)$request->getPost('majorId');
        if ($majorId > 0)
        {
            $major = $this->getMajorById($majorId);
            $degreeId = $major->degree_id;
        }

        $majorDegreesOptions = array();

        $degrees = $this->getDegrees();
        if ($degrees)
        {
            foreach ($degrees as $degree)
            {
                $majorDegreesOptions[$degree->degree_id] = $degree->title;
            }
        }

        $defaultFormData = array(
            'majorId' => null,
            'majorDegreeId' => null,
            'majorIsPublished' => 0,
            'majorIsStationary' => 1,
            'majorIsRemote' => 0,
            'majorTextIcon' => '',
            'majorTitle' => ''
        );
        $currentMajorFormData = array();

        if ($major)
        {
            $currentMajorFormData = array(
                'majorId' => $major->major_id,
                'majorDegreeId' => $major->degree_id,
                'majorIsPublished' => $major->is_published,
                'majorIsStationary' => $major->is_stationary,
                'majorIsRemote' => $major->is_remote,
                'majorTextIcon' => $major->bootstrap_glyphicon,
                'majorTitle' => $major->title
            );
        }

        $formData = new \Site\Custom\FlexibleContainer(array_merge($defaultFormData, $currentMajorFormData));

        $majorGlyphiconsOptions = $this->getGlyphiconOptionsHtmlForDropdown($formData->majorTextIcon);

        $this->addEditView->setVariables(
            array(
                'majorDegreesOptions' => $majorDegreesOptions,
                'majorGlyphiconsOptions' => $majorGlyphiconsOptions,
                'formData' => $formData
            )
        );
    }

    public function renderDegreesManagement(\Zend\Http\Request $request, $return = true)
    {
        $settings = $this->getSettings();

        $pageNumber = (int)$request->getPost('pageNumber') ? : 1;
        $itemCountPerPage = (int)$request->getPost('itemCountPerPage') ? : 10;
        $includePagesFrom = $request->getPost('includePagesFrom') ? : 'current-language';

        $dbManager = $this->getDatabaseManager();
        $dbAdapter = $dbManager->getAdapter();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();

        $select = $sql->select();
        $select->from(array('bod' => self::BLOCK_OFFER_DEGREES_TABLE_NAME))
            ->join(
                array('u' => 'users'),
                'u.user_id = bod.last_update_author_id',
                array('last_update_author' => 'login'),
                $select::JOIN_LEFT
            )
            ->where(array('bod.content_id' => $contentId))
            ->order('bod.title ASC');

        $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        $paginatorAdapter = new \Zend\Paginator\Adapter\DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new \Zend\Paginator\Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($pageNumber);
        $paginator->setItemCountPerPage($itemCountPerPage);

        $degreesManagementView = new \Zend\View\Model\ViewModel();
        $degreesManagementView->setTemplate('blocks/' . $this->blockViewName . '/degrees-management.phtml');
        $degreesManagementView->setVariables(
            array(
                'user' => $this->user,
                'designMode' => $this->designMode,
                'blockData' => $this->blockData,
                'hasOwnSettings' => $this->hasOwnSettings,
                'settings' => $settings,
                'pageNumber' => $pageNumber,
                'itemCountPerPage' => $itemCountPerPage,
                'includePagesFrom' => $includePagesFrom,
                'degreesPaginator' => $paginator
            )
        );
        $degreesManagementViewHtml = $this->phpRenderer->render($degreesManagementView);

        if ($return)
        {
            return $degreesManagementViewHtml;
        }
        else
        {
            echo $degreesManagementViewHtml;
        }
    }

    public function renderAddEditDegree(\Zend\Http\Request $request, $return = true)
    {
        $settings = $this->getSettings();

        $pageNumber = (int)$request->getPost('pageNumber') ? : 1;
        $itemCountPerPage = (int)$request->getPost('itemCountPerPage') ? : 10;
        $includePagesFrom = $request->getPost('includePagesFrom') ? : 'current-language';

        $defaultFormData = array(
            'degreeId' => '',
            'degreeTitle' => '',
            'degreeType' => '',
            'degreeIsPublished' => ''
        );
        $currentDegreeFormData = array();

        $degreeId = (int)$request->getPost('degreeId');
        if ($degreeId > 0)
        {
            $degree = $this->getDegreeById($degreeId);

            $currentDegreeFormData = array(
                'degreeId' => $degree->degree_id,
                'degreeTitle' => $degree->title,
                'degreeType' => $degree->type,
                'degreeIsPublished' => $degree->is_published
            );
        }

        $formData = array_merge($defaultFormData, $currentDegreeFormData);

        $addEditDegreeView = new \Zend\View\Model\ViewModel();
        $addEditDegreeView->setTemplate('blocks/' . $this->blockViewName . '/add-edit-degree.phtml');
        $addEditDegreeView->setVariables(
            array(
                'user' => $this->user,
                'designMode' => $this->designMode,
                'blockData' => $this->blockData,
                'hasOwnSettings' => $this->hasOwnSettings,
                'settings' => $settings,
                'pageNumber' => $pageNumber,
                'itemCountPerPage' => $itemCountPerPage,
                'includePagesFrom' => $includePagesFrom,
                'formData' => $formData
            )
        );
        $addEditDegreeViewHtml = $this->phpRenderer->render($addEditDegreeView);

        if ($return)
        {
            return $addEditDegreeViewHtml;
        }
        else
        {
            echo $addEditDegreeViewHtml;
        }
    }

    /**
     * Getters.
     */

    public function getDegrees()
    {
        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_OFFER_DEGREES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('content_id' => $this->getContentId()));
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getDegreeById($degreeId)
    {
        $degreeId = (int)$degreeId;

        if ($degreeId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();

        $select = $sql->select();
        $select->from(self::BLOCK_OFFER_DEGREES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'degree_id' => $degreeId,
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getDegreeCount()
    {
        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_OFFER_DEGREES_TABLE_NAME)
            ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')), $prefixColumnsWithTable)
            ->where(array('content_id' => $this->getContentId()));
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();
            $count = $row ? $row->count : false;

            return $count;
        }

        return false;
    }

    public function getMajorById($majorId)
    {
        $majorId = (int)$majorId;

        if ($majorId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_OFFER_MAJORS_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'major_id' => $majorId,
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    /**
     * Data formatting functions.
     */

    public function formatDegreeDataAndReturnIfAreValid($degreeData)
    {
        $degreeData = $this->convertToFlexibleContainer($degreeData);
        $degreeDataCopy = clone $degreeData;

        $degreeDataCopy->degreeId = (int)$degreeData->degreeId ? : null;
        $degreeDataCopy->degreeIsPublished = (int)$degreeData->degreeIsPublished ? 1 : 0;
        $degreeDataCopy->degreeType = (int)$degreeData->degreeType;
        $degreeDataCopy->degreeTitle = trim((string)$degreeData->degreeTitle);

        if (
            (($degreeDataCopy->degreeId !== null) && ($degreeDataCopy->degreeId <= 0)) ||
            !in_array($degreeDataCopy->degreeType, array(0, 1, 2, 3)) ||
            ($degreeDataCopy->degreeTitle == '')
        )
        {
            return false;
        }

        return $degreeDataCopy;
    }

    public function formatMajorDataAndReturnIfAreValid($majorData)
    {
        $majorData = $this->convertToFlexibleContainer($majorData);
        $majorDataCopy = clone $majorData;

        $majorDataCopy->majorId = (int)$majorData->majorId ? : null;
        $majorDataCopy->majorIsPublished = (int)$majorData->majorIsPublished ? 1 : 0;
        $majorDataCopy->majorIsStationary = (int)$majorData->majorIsStationary ? 1 : 0;
        $majorDataCopy->majorIsRemote = (int)$majorData->majorIsRemote ? 1 : 0;
        $majorDataCopy->majorDegreeId = (int)$majorData->majorDegreeId;
        $majorDataCopy->majorTextIcon = mb_strtolower(trim((string)$majorData->majorTextIcon));
        $majorDataCopy->majorTitle = trim((string)$majorData->majorTitle);

        /**
         * Identifying the author.
         */
        $majorDataCopy->majorLastUpdateAuthorId = $this->user->user_id;

        /**
         * Validating form data.
         */
        if (
            (($majorDataCopy->majorId !== null) && ($majorDataCopy->majorId <= 0)) ||
            (($majorDataCopy->majorIsStationary == 0) && ($majorDataCopy->majorIsRemote == 0)) ||
            ($majorDataCopy->majorDegreeId <= 0) ||
            ($majorDataCopy->majorTextIcon == '') ||
            ($majorDataCopy->majorTitle == '')
        )
        {
            return false;
        }

        return $majorDataCopy;
    }

    /**
     * Logic-functions.
     */

    public function addDefaultDegreesIfNoDegreeExists()
    {
        $degreeCount = $this->getDegreeCount();
        if ($degreeCount > 0)
        {
            return true;
        }

        if ($degreeCount === false) // It is impossible to get degree count because of database error.
        {
            return false;
        }

        $translate = $this->getTranslateHelper();
        $defaultDegrees = array(
            array('title' => $translate('Licencjackie'), 'type' => 3),
            array('title' => $translate('Magisterskie'), 'type' => 3),
            array('title' => $translate('Inżynierskie'), 'type' => 3),
            array('title' => $translate('Doktoranckie'), 'type' => 0)
        );

        $allOperationsWasSuccessful = true;

        $contentId = $this->getContentId();
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        foreach ($defaultDegrees as $degree)
        {
            $insert = $sql->insert(self::BLOCK_OFFER_DEGREES_TABLE_NAME);
            $insert->columns(
                array(
                    'content_id',
                    'is_published',
                    'type',
                    'last_update_author_id',
                    'last_update_date',
                    'title',
                )
            )
                ->values(
                    array(
                        'content_id' => $contentId,
                        'is_published' => 1,
                        'type' => $degree['type'],
                        'last_update_author_id' => null,
                        'last_update_date' => $lastUpdateDate,
                        'title' => $degree['title']
                    )
                );
            $degreeId = $dbManager->executeInsertAndGetLastInsertId($insert);
            if ($degreeId <= 0)
            {
                $allOperationsWasSuccessful = false;

                break;
            }
        }

        if ($allOperationsWasSuccessful)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $allOperationsWasSuccessful;
    }

    public function addOrEditDegree($degreeData)
    {
        $degreeDataCopy = $this->formatDegreeDataAndReturnIfAreValid($degreeData);
        if (!$degreeDataCopy)
        {
            return false;
        }

        $degreeDataCopy->degreeLastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        if ($degreeData->degreeId > 0)
        {
            return $this->updateExistingDegree($degreeDataCopy);
        }
        else
        {
            return $this->createNewDegree($degreeDataCopy);
        }
    }

    public function addOrEditMajor($majorData)
    {
        $majorDataCopy = $this->formatMajorDataAndReturnIfAreValid($majorData);
        if (!$majorDataCopy)
        {
            return false;
        }

        $majorDataCopy->majorLastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        if ($majorData->majorId > 0)
        {
            return $this->updateExistingMajor($majorDataCopy);
        }
        else
        {
            return $this->createNewMajor($majorDataCopy);
        }
    }

    public function deleteAllDegrees()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_OFFER_DEGREES_TABLE_NAME);
        $delete->where(array('content_id' => $this->getContentId()));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;
    }

    public function deleteAllMajors()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_OFFER_MAJORS_TABLE_NAME);
        $delete->where(array('content_id' => $this->getContentId()));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;
    }

    public function deleteDegreeById($degreeId)
    {
        $degreeId = (int)$degreeId;

        if ($degreeId <= 0)
        {
            return false;
        }

        $deleted = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();

        $delete = $sql->delete(self::BLOCK_OFFER_DEGREES_TABLE_NAME);
        $delete->where(
            array(
                'degree_id' => $degreeId,
                'content_id' => $contentId
            )
        );
        $result = $dbManager->executePreparedStatement($delete);
        if ($result)
        {
            $deleted = true;
        }

        return $deleted;
    }

    public function deleteMajorById($majorId)
    {
        $majorId = (int)$majorId;

        if ($majorId <= 0)
        {
            return false;
        }

        $deleted = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();

        $delete = $sql->delete(self::BLOCK_OFFER_MAJORS_TABLE_NAME);
        $delete->where(
            array(
                'major_id' => $majorId,
                'content_id' => $contentId
            )
        );
        $result = $dbManager->executePreparedStatement($delete);
        if ($result)
        {
            $deleted = true;
        }

        return $deleted;
    }

    public function reorderDegrees($degrees)
    {
        $degrees = (array)$degrees;

        if (!$degrees)
        {
            return false;
        }

        $reordered = true;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $order = 0;
        $contentId = $this->getContentId();

        $dbManager->beginTransaction();

        foreach ($degrees as $degreeId)
        {
            $degreeId = (int)$degreeId;

            if ($degreeId > 0)
            {
                $update = $sql->update(self::BLOCK_OFFER_DEGREES_TABLE_NAME);
                $update->set(array('order' => $order))
                    ->where(
                        array(
                            'degree_id' => $degreeId,
                            'content_id' => $contentId
                        )
                    );
                $result = $dbManager->executePreparedStatement($update);
                if (!$result)
                {
                    $reordered = false;
                    break;
                }

                $degree = $this->getDegreeById($degreeId);
                if ($degree->order != $order)
                {
                    $reordered = false;
                    break;
                }

                $order++;
            }
        }

        if ($reordered)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $reordered;
    }

    public function reorderMajors($majors)
    {
        $majors = (array)$majors;

        if (!$majors)
        {
            return false;
        }

        $reordered = true;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $order = 0;
        $contentId = $this->getContentId();

        $dbManager->beginTransaction();

        foreach ($majors as $majorId)
        {
            $majorId = (int)$majorId;

            if ($majorId > 0)
            {
                $update = $sql->update(self::BLOCK_OFFER_MAJORS_TABLE_NAME);
                $update->set(array('order' => $order))
                    ->where(
                        array(
                            'major_id' => $majorId,
                            'content_id' => $contentId
                        )
                    );
                $result = $dbManager->executePreparedStatement($update);
                if (!$result)
                {
                    $reordered = false;
                    break;
                }

                $major = $this->getDegreeById($majorId);
                if ($major->order != $order)
                {
                    $reordered = false;
                    break;
                }

                $order++;
            }
        }

        if ($reordered)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $reordered;
    }

    public function toggleDegreePublished($degreeId, $published)
    {
        $degreeId = (int)$degreeId;
        $published = (int)$published ? 1 : 0;

        if ($degreeId <= 0)
        {
            return false;
        }

        $updated = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $degree = $this->getDegreeById($degreeId);
        if ($degree)
        {
            $update = $sql->update(self::BLOCK_OFFER_DEGREES_TABLE_NAME);
            $update->set(
                array(
                    'is_published' => $published,
                    'content_id' => $this->getContentId()
                )
            )
                ->where(array('degree_id' => $degreeId));
            $result = $dbManager->executePreparedStatement($update);
            if ($result)
            {
                $degree = $this->getDegreeById($degreeId);
                if ($degree && ($degree->is_published == $published))
                {
                    $updated = true;
                }
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function toggleMajorPublished($majorId, $published)
    {
        $majorId = (int)$majorId;
        $published = (int)$published ? 1 : 0;

        if ($majorId <= 0)
        {
            return false;
        }

        $updated = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $major = $this->getMajorById($majorId);
        if ($major)
        {
            $update = $sql->update(self::BLOCK_OFFER_MAJORS_TABLE_NAME);
            $update->set(
                array(
                    'is_published' => $published
                )
            )
                ->where(
                    array(
                        'major_id' => $majorId,
                        'content_id' => $this->getContentId()
                    )
                );
            $result = $dbManager->executePreparedStatement($update);
            if ($result)
            {
                $major = $this->getMajorById($majorId);
                if ($major && ($major->is_published == $published))
                {
                    $updated = true;
                }
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    protected function getGlyphiconOptionsHtmlForDropdown($selectedGlyphicon = '')
    {
        $fileSystem = $this->getFileSystem();
        $filePath = 'module/Site/data/glyphicons.txt';

        if (!$fileSystem->isFile($filePath))
        {
            return false;
        }

        $glyphicons = null;
        $fileContents = str_replace(array("\r\n"), "\n", @file_get_contents($filePath));
        $fileContents = str_replace(array("\r"), "\n", $fileContents);
        if ($fileContents != '')
        {
            $glyphicons = explode("\n", $fileContents);
        }

        if (!$glyphicons)
        {
            return '';
        }

        sort($glyphicons);

        $options = '';
        foreach ($glyphicons as $icon)
        {
            $selected = (($selectedGlyphicon != '') && ($icon == $selectedGlyphicon)) ? ' selected' : '';

            $options .= '<option data-subtext="&lt;i class=\'glyphicon glyphicon-' . $icon . '\'&gt;&lt;/i&gt;"' . $selected . '>' . $icon . '</option>';
        }

        return $options;
    }

    private function createNewDegree($degreeData)
    {
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $insert = $sql->insert(self::BLOCK_OFFER_DEGREES_TABLE_NAME);
        $insert->columns(
            array(
                'content_id',
                'is_published',
                'type',
                'last_update_author_id',
                'last_update_date',
                'title'
            )
        )
            ->values(
                array(
                    'content_id' => $contentId,
                    'is_published' => $degreeData->degreeIsPublished,
                    'type' => $degreeData->degreeType,
                    'last_update_author_id' => $degreeData->degreeLastUpdateAuthorId,
                    'last_update_date' => $degreeData->degreeLastUpdateDate,
                    'title' => $degreeData->degreeTitle
                )
            );
        $degreeId = $dbManager->executeInsertAndGetLastInsertId($insert);

        if ($degreeId > 0)
        {
            $dbManager->commit();

            return $degreeId;
        }

        $dbManager->rollback();

        return false;
    }

    private function updateExistingDegree($degreeData)
    {
        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_OFFER_DEGREES_TABLE_NAME);
        $update->set(
            array(
                'is_published' => $degreeData->degreeIsPublished,
                'type' => $degreeData->degreeType,
                'last_update_author_id' => $degreeData->degreeLastUpdateAuthorId,
                'last_update_date' => $degreeData->degreeLastUpdateDate,
                'title' => $degreeData->degreeTitle
            )
        )
            ->where(
                array(
                    'degree_id' => $degreeData->degreeId,
                    'content_id' => $contentId
                )
            );
        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $degree = $this->getDegreeById($degreeData->degreeId);
            if ($degree &&
                ($degree->is_published == $degreeData->degreeIsPublished) &&
                ($degree->type == $degreeData->degreeType) &&
                ($degree->last_update_author_id == $degreeData->degreeLastUpdateAuthorId) &&
                ($degree->title == $degreeData->degreeTitle)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    private function createNewMajor($majorData)
    {
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $insert = $sql->insert(self::BLOCK_OFFER_MAJORS_TABLE_NAME);
        $insert->columns(
            array(
                'content_id',
                'degree_id',
                'is_published',
                'is_stationary',
                'is_remote',
                'last_update_author_id',
                'last_update_date',
                'bootstrap_glyphicon',
                'title'
            )
        )
            ->values(
                array(
                    'content_id' => $contentId,
                    'degree_id' => $majorData->majorDegreeId,
                    'is_published' => $majorData->majorIsPublished,
                    'is_stationary' => $majorData->majorIsStationary,
                    'is_remote' => $majorData->majorIsRemote,
                    'last_update_author_id' => $majorData->majorLastUpdateAuthorId,
                    'last_update_date' => $majorData->majorLastUpdateDate,
                    'bootstrap_glyphicon' => $majorData->majorTextIcon,
                    'title' => $majorData->majorTitle
                )
            );
        $majorId = $dbManager->executeInsertAndGetLastInsertId($insert);

        if ($majorId > 0)
        {
            $dbManager->commit();

            return $majorId;
        }

        $dbManager->rollback();

        return false;
    }

    private function updateExistingMajor($majorData)
    {
        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_OFFER_MAJORS_TABLE_NAME);
        $update->set(
            array(
                'degree_id' => $majorData->majorDegreeId,
                'is_published' => $majorData->majorIsPublished,
                'is_stationary' => $majorData->majorIsStationary,
                'is_remote' => $majorData->majorIsRemote,
                'last_update_author_id' => $majorData->majorLastUpdateAuthorId,
                'last_update_date' => $majorData->majorLastUpdateDate,
                'bootstrap_glyphicon' => $majorData->majorTextIcon,
                'title' => $majorData->majorTitle
            )
        )
            ->where(
                array(
                    'major_id' => $majorData->majorId,
                    'content_id' => $contentId
                )
            );

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $major = $this->getMajorById($majorData->majorId);
            if ($major &&
                ($major->degree_id == $majorData->majorDegreeId) &&
                ($major->is_published == $majorData->majorIsPublished) &&
                ($major->is_stationary == $majorData->majorIsStationary) &&
                ($major->is_remote == $majorData->majorIsRemote) &&
                ($major->last_update_author_id == $majorData->majorLastUpdateAuthorId) &&
                ($major->bootstrap_glyphicon == $majorData->majorTextIcon) &&
                ($major->title == $majorData->majorTitle)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }
}
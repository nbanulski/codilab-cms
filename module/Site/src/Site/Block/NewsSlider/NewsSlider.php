<?php
namespace Site\Block\NewsSlider;

use Site\Block\AbstractBlock;
use Zend\Http\Request;

class NewsSlider extends AbstractBlock
{
    const BLOCK_NEWS_LIST_TABLE_NAME = 'block_news_list';
    const BLOCK_NEWS_LIST_CATEGORIES_TABLE_NAME = 'block_news_list_categories';

    protected $hasOwnSettings = true;

    public function childRender(Request $request)
    {
        $blockManager = $this->getBlockManager();
        $pageManager = $this->getPageManager();
        $dbManager = $this->getDatabaseManager();
        $dbAdapter = $dbManager->getAdapter();
        $sql = $dbManager->getSql();

        $settings = $this->getSettings();
        $pageNumber = (int)$this->blockView->pageNumber ? : 1;
        $totalItemCount = (int)$settings->totalItemCount ? : 10;
        $totalItemCount = max(4, $totalItemCount);
        $newsListPageBlockId = (int)$settings->newsListPageBlockId;
        $newsListCategoryId = (int)$settings->newsListCategoryId;

        $newsListPageUrl = '';
        $newsDetailsPageUrl = '';
        $newsCategoriesOptions = array();
        $newsList = null;

        $contentId = $blockManager->getPageBlockContentIdByPageBlockId($newsListPageBlockId);
        if ($contentId <= 0) // If content id could not be found.
        {
            $newsListPageBlockId = null; // Block does not exists.
        }

        $newsListBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($newsListPageBlockId);
        if ($newsListBlock instanceof \Site\Block\NewsList\NewsList)
        {
            $newsListData = $blockManager->getPageBlockById($newsListPageBlockId);
            $newsListSettings = $newsListBlock->getSettings();

            if ($newsListData)
            {
                $newsListPageUrl = $pageManager->getFullPageCleanUrlByPageId($newsListData->page_id);
            }

            if ($newsListSettings instanceof \Site\Custom\FlexibleContainer)
            {
                $detailsPageId = (int)$newsListSettings->detailsPageId;
                if ($detailsPageId > 0)
                {
                    $newsDetailsPageUrl = $pageManager->getFullPageCleanUrlByPageId($detailsPageId);
                }
            }

            if ($contentId > 0)
            {
                $categories = $this->getCategoriesByContentId($contentId);
                if ($categories)
                {
                    foreach ($categories as $category)
                    {
                        $newsCategoriesOptions[$category->category_id] = $category->name;
                    }
                }

                $select = $sql->select();
                $select
                    ->from(array('bnl' => self::BLOCK_NEWS_LIST_TABLE_NAME))
                    ->join(
                        array('bnlc' => self::BLOCK_NEWS_LIST_CATEGORIES_TABLE_NAME),
                        'bnlc.category_id = bnl.category_id',
                        array('category_name' => 'name'),
                        $select::JOIN_LEFT
                    )
                    ->join(
                        array('u' => 'users'),
                        'u.user_id = bnl.last_update_author_id',
                        array('last_update_author' => 'login'),
                        $select::JOIN_LEFT
                    )
                    ->where(
                        array(
                            'bnl.content_id' => $contentId,
                            'bnl.is_published' => 1,
                            'bnlc.content_id' => $contentId,
                            'bnlc.is_published' => 1
                        )
                    )
                    ->limit($totalItemCount);

                if ($newsListCategoryId > 0)
                {
                    $select->where->equalTo('bnl.category_id', $newsListCategoryId);
                }

                $select->order('bnl.is_promoted DESC, bnl.news_date ASC');

                $newsList = $dbManager->getSpecificHydratedResultSet(
                    $select,
                    new \Site\Block\NewsList\NewsHydrator(
                        $this->getTranslateHelper(), $this->attachmentManager, $newsListBlock
                    ),
                    new \Site\Custom\FlexibleContainer()
                );
            }
        }

        $this->blockView->setVariables(
            array(
                'newsListPageBlockId' => $newsListPageBlockId,
                'newsListPageUrl' => $newsListPageUrl,
                'newsDetailsPageUrl' => $newsDetailsPageUrl,
                'newsCategoriesOptions' => $newsCategoriesOptions,
                'newsList' => $newsList
            )
        );
    }

    public function childRenderSettings()
    {
        $settings = $this->getSettings();
        $blockManager = $this->getBlockManager();
        $translate = $this->getTranslateHelper();
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $newsListPageBlockId = (int)$settings->newsListPageBlockId;

        $select = $sql->select();
        $select
            ->from(array('pb' => 'pages_blocks'))
            ->join(
                array('b' => 'blocks'),
                'b.block_id = pb.block_id',
                array(),
                $select::JOIN_LEFT
            )
            ->join(
                array('bp' => 'blocks_properties'),
                'bp.block_id = b.block_id',
                array('block_translated_name' => 'name'),
                $select::JOIN_LEFT
            )
            ->join(
                array('p' => 'pages'),
                'p.page_id = pb.page_id',
                array('page_title' => 'title'),
                $select::JOIN_LEFT
            )
            ->join(
                array('p2' => 'pages'),
                'p2.page_id = p.parent_id',
                array('parent_page_title' => 'title'),
                'LEFT OUTER'
            )
            ->where(
                array(
                    'pb.is_parent_block_dynamic' => 0,
                    'b.php_class_name' => 'NewsList',
                    'bp.language_id' => $this->language->language_id
                )
            )
            ->order(array('block_translated_name ASC'));
        $resultSet = $dbManager->getHydratedResultSet($select);

        $select = $sql->select();
        $select->from(array('pb' => 'pages_blocks'))
            ->join(
                array('b' => 'blocks'),
                'b.block_id = pb.block_id',
                array(),
                $select::JOIN_LEFT
            )
            ->join(
                array('bp' => 'blocks_properties'),
                'bp.block_id = b.block_id',
                array('block_translated_name' => 'name'),
                $select::JOIN_LEFT
            )
            ->join(
                array('p' => 'pages'),
                'p.page_id = pb.page_id',
                array('page_title' => 'title'),
                $select::JOIN_LEFT
            )
            ->join(
                array('p2' => 'pages'),
                'p2.page_id = p.parent_id',
                array('parent_page_title' => 'title'),
                'LEFT OUTER'
            )
            ->where(
                array(
                    'pb.is_parent_block_dynamic' => 1,
                    'b.php_class_name' => 'NewsList',
                    'bp.language_id' => $this->language->language_id
                )
            )
            //->group('b.block_id')
            ->order(array('block_translated_name ASC'));
        $resultSet2 = $dbManager->getHydratedResultSet($select);

        $array1 = $resultSet ? $resultSet->toArray() : array();
        $array2 = $resultSet2 ? $resultSet2->toArray() : array();
        unset($resultSet);
        unset($resultSet2);
        $rowsFromBothResultSets = array_merge($array1, $array2);
        unset($array1);
        unset($array2);

        /**
         * Available NewsList blocks.
         */

        $availableNewsListBlocks = array();
        foreach ($rowsFromBothResultSets as $pageBlock)
        {
            $pageStr = $pageBlock['page_title'];
            if ($pageBlock['parent_page_title'] != '')
            {
                $pageStr = $pageBlock['parent_page_title'] . ' > ' . $pageStr;
            }

            if ($pageBlock['is_parent_block_dynamic'])
            {
                $blockTypeStr = ' (' . $translate('dynamic block', 'default', $this->userLanguage->zend2_locale) . ')';
            }
            else
            {
                $blockTypeStr = ' (' . $translate('standard block', 'default', $this->userLanguage->zend2_locale) . ')';
            }

            $pageBlockId = $pageBlock['page_block_id'];
            $availableNewsListBlocks[$pageBlockId] =
                '[id: ' . $pageBlockId . '] ' .
                $translate('Page', 'default', $this->userLanguage->zend2_locale) . ': ' .
                $pageStr . ' | ' .
                $translate('Block', 'default', $this->userLanguage->zend2_locale) . ': ' .
                $pageBlock['block_translated_name'] .
                $blockTypeStr;
        }

        if (!$availableNewsListBlocks)
        {
            $availableNewsListBlocks[''] = '-----';
        }

        /**
         * Available categories.
         */

        $availableNewsListCategories = array(
            '0' => '-- ' . $translate('All categories', 'default', $this->userLanguage->zend2_locale) . ' --'
        );
        $newsListBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($newsListPageBlockId);
        if ($newsListBlock instanceof \Site\Block\NewsList\NewsList)
        {
            $categories = $newsListBlock->getCategories();
            if ($categories)
            {
                foreach ($categories as $category)
                {
                    $availableNewsListCategories[$category->category_id] = $category->name;
                }
            }
        }

        $this->settingsView->setVariables(
            array(
                'availableNewsListBlocks' => $availableNewsListBlocks,
                'availableNewsListCategories' => $availableNewsListCategories
            )
        );
    }

    /**
     * Getters.
     */

    public function getCategoriesByContentId($contentId)
    {
        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_NEWS_LIST_CATEGORIES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }
}
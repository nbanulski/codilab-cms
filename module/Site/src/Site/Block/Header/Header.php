<?php
namespace Site\Block\Header;

use Site\Block\AbstractBlock;

class Header extends AbstractBlock
{
    protected $hasOwnSettings = true;

    public function childRenderSettings()
    {
        $settings = $this->getSettings();

        $tagType = trim($settings['tagType']);
        $predefinedTag = mb_strtolower(trim($settings['predefinedTag']));
        $customTag = mb_strtolower(trim($settings['customTag']));

        $this->settingsView->setVariables(array(
            'tagType' => $tagType,
            'predefinedTag' => $predefinedTag,
            'customTag' => $customTag,
        ));
    }
}
<?php
namespace Site\Block\Employees;

use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\I18n\View\Helper\Translate;
use Site\Controller\AttachmentManager;

class EmployeeHydrator implements HydratorInterface
{
    protected $attachmentManager = null;
    protected $blockEmployees = null;
    protected $pageManager = null;
    protected $translateHelper = null;
    protected $detailsPageUrl = '';

    public function __construct(
        AttachmentManager $attachmentManager,
        Employees $blockEmployees,
        Translate $translateHelper,
        $detailsPageUrl
    ) {
        $this->attachmentManager = $attachmentManager;
        $this->blockEmployees = $blockEmployees;
        $this->translateHelper = $translateHelper;
        $this->detailsPageUrl = $detailsPageUrl;
    }

    public function extract($object)
    {
    }

    public function hydrate(array $data, $object)
    {
        foreach ($data as $property => $value)
        {
            $object[$property] = $value;
        }

        $object['thumbUrl'] = '/img/no-image.png';
        if ($object['attachment_id'] > 0)
        {
            $attachment = $this->attachmentManager->getAttachmentById($object['attachment_id']);
            if ($attachment)
            {
                $object['thumbUrl'] =
                    '/eng/get-file/index,' .
                    $attachment->hash_for_file_name .
                    ',' .
                    $attachment->original_file_name;
            }
        }

        $object['cleanUrl'] = $this->blockEmployees->generateCleanUrlFromEmployeeNameAndSurname(
            $object['name_and_surname']
        );

        $object['detailsPageUrl'] = '#';
        if ($this->detailsPageUrl != '')
        {
            $object['detailsPageUrl'] =
                $this->detailsPageUrl . ',' .
                $object['cleanUrl'] . '-' .
                $object['content_id'] . '-' .
                $object['employee_id'];
        }

        return $object;
    }
} 
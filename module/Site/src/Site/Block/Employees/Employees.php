<?php
namespace Site\Block\Employees;

use Site\Block\AbstractBlock;
use Zend\Http\Request;

class Employees extends AbstractBlock
{
    const BLOCK_EMPLOYEES_DEGREES_TABLE_NAME = 'block_employees_degrees';
    const BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME = 'block_employees_employees';

    protected $hasOwnSettings = true;
    protected $hasManagementWindow = true;

    protected $alphabet = 'AĄBCĆDEĘFGHIJKLŁMNŃOÓPQRSŚTUWXYZŹŻ';
    protected $employeeDetailsPageUrl = '';

    /**
     * Events.
     */

    public function onBeforeDeleted()
    {
        if (!$this->blockData->is_dynamic)
        {
            $this->deleteAllEmployees();
            $this->deleteAllDegrees();
            $this->deleteBlockRecordFromSearchIndex();
        }
    }

    public function updateSearchIndex()
    {
        $allEmployees = $this->getAllEmployees();

        if ($allEmployees)
        {
            $contentId = $this->getContentId();

            foreach ($allEmployees as $employee)
            {
                $employeeCleanUrl = $this->generateCleanUrlFromEmployeeNameAndSurname($employee->name_and_surname);
                $elementUrlParametersString =
                    $employeeCleanUrl . '-' . $contentId . '-' . $employee->employee_id;

                $this->updateBlockRecordInSearchIndex(
                    $employee->employee_id, $employee->about, $elementUrlParametersString, 'About employee',
                    $employee->last_update_date
                );
            }
        }
    }

    /**
     * Render-functions.
     */

    public function childRender(Request $request)
    {
        $settings = $this->getSettings();
        list(
            $currentEmployeeCleanUrl,
            $currentEmployeeContentId,
            $currentEmployeeId
            ) = $this->getEmployeePartialDataFromRequest($request);

        $employeesBlockShouldBeRendered = true;
        $employeesBlockShouldBeHiddenIfCurrentUrlLeadsToEmployeeDetails =
            $settings->hideIfCurrentUrlLeadsToEmployeeDetails ? true : false;

        if ($employeesBlockShouldBeHiddenIfCurrentUrlLeadsToEmployeeDetails)
        {
            // Data needed for displaying employee's details are correct:
            if (($currentEmployeeContentId > 0) && ($currentEmployeeId > 0))
            {
                $employee = $this->getEmployeeByIdAndContentId(
                    $currentEmployeeId,
                    $currentEmployeeContentId
                );
                if ($employee) // Employee exists.
                {
                    $employeesBlockShouldBeRendered = false;
                }
            }
        }

        $this->blockView->setVariables(
            array(
                'currentEmployeeId' => $currentEmployeeId,
                'currentEmployeeCleanUrl' => $currentEmployeeCleanUrl,
                'currentEmployeeContentId' => $currentEmployeeContentId,
                'currentEmployeeDataAreValid' => (($currentEmployeeCleanUrl != '') && ($currentEmployeeContentId > 0)),
                'employeesBlockShouldBeRendered' => $employeesBlockShouldBeRendered
            )
        );

        if ($employeesBlockShouldBeRendered)
        {
            $this->renderEmployees($request);
        }
        else
        {
            $this->hideEmployees();
        }
    }

    public function childRenderManagement(\Zend\Http\Request $request)
    {
        if (!$this->managementView instanceof \Zend\View\Model\ViewModel)
        {
            return false;
        }

        $dbManager = $this->getDatabaseManager();
        $dbAdapter = $dbManager->getAdapter();
        $sql = $dbManager->getSql();

        $pageNumber = $this->managementView->pageNumber;
        $itemCountPerPage = $this->managementView->itemCountPerPage;
        $contentId = $this->getContentId();

        $select = $sql->select();
        $select
            ->from(array('bee' => self::BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME))
            ->join(
                array('bed' => self::BLOCK_EMPLOYEES_DEGREES_TABLE_NAME),
                'bed.degree_id = bee.degree_id AND bed.content_id = bee.content_id',
                array('degree_title' => 'title'),
                $select::JOIN_LEFT
            )
            ->join(
                array('u' => 'users'),
                'u.user_id = bee.last_update_author_id',
                array('last_update_author' => 'login'),
                $select::JOIN_LEFT
            )
            ->where(
                array(
                    'bee.content_id' => $contentId
                )
            )
            ->order('bed.title, bee.name_and_surname ASC');
        $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        $paginatorAdapter = new \Zend\Paginator\Adapter\DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new \Zend\Paginator\Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($pageNumber);
        $paginator->setItemCountPerPage($itemCountPerPage);

        $this->managementView->setVariables(
            array(
                'employeesDegreeCount' => $this->getDegreeCount(),
                'employeesPaginator' => $paginator
            )
        );
    }

    public function childRenderAddEdit(\Zend\Http\Request $request)
    {
        if (!$this->addEditView instanceof \Zend\View\Model\ViewModel)
        {
            return false;
        }

        $pageManager = $this->getPageManager();

        $employee = null;
        $employeeId = (int)$request->getPost('employeeId');
        if ($employeeId > 0)
        {
            $employee = $this->getEmployeeById($employeeId);
        }

        $defaultFormData = array(
            'employeeId' => null,
            'employeeContentId' => null,
            'employeeDegreeId' => null,
            'employeeIsPublished' => false,
            'employeeNameAndSurname' => '',
            'employeePosition' => '',
            'employeeAbout' => '',
            'employeeScientificCareer' => '',
            'employeeTheMostImportantPublications' => '',
            'employeeContactInfo' => '',
            'employeeImage' => array(
                0 => array(
                    'imageId' => null,
                    'isMain' => true,
                    'attachmentId' => null,
                    'uniqueHashForFileName' => '',
                    'originalFileName' => '',
                )
            )
        );
        $currentImageFormData = array();

        if ($employee)
        {
            $attachmentId = $employee->attachment_id;
            $attachment = $this->attachmentManager->getAttachmentById($attachmentId);

            $currentImageFormData = array(
                'employeeId' => $employee->employee_id,
                'employeeContentId' => $employee->content_id,
                'employeeDegreeId' => $employee->degree_id,
                'employeeIsPublished' => $employee->is_published,
                'employeeNameAndSurname' => $employee->name_and_surname,
                'employeePosition' => $employee->position,
                'employeeAbout' => $employee->about,
                'employeeScientificCareer' => $employee->scientific_career,
                'employeeTheMostImportantPublications' => $employee->most_important_publications,
                'employeeContactInfo' => $employee->contact_info,
                'employeeImage' => array(
                    0 => array(
                        'imageId' => null,
                        'isMain' => true,
                        'attachmentId' => $employee->attachment_id,
                        'uniqueHashForFileName' => $attachment ? $attachment->hash_for_file_name : '',
                        'originalFileName' => $attachment ? $attachment->original_file_name : '',
                    )
                )
            );
        }

        $formData = new \Site\Custom\FlexibleContainer(array_merge($defaultFormData, $currentImageFormData));

        $employeeDegreesOptions = array();
        $degrees = $this->getDegrees();
        if ($degrees)
        {
            foreach ($degrees as $degree)
            {
                $employeeDegreesOptions[$degree->degree_id] = $degree->title;
            }
        }

        $employeeInternalPagesOptions = array();
        $pages = $pageManager->getPages();
        if ($pages)
        {
            foreach ($pages as $page)
            {
                $option = new \stdClass();
                $option->title = $page->title;
                $option->extended_title =
                    $page->title .
                    ' (' . $pageManager->getFullPageCleanUrlByPageId($page->page_id) . ')';
                $option->selected = ($page->page_id == $formData->employeePageId) ? ' selected' : '';

                $employeeInternalPagesOptions[$page->page_id] = $option;
            }
        }

        $this->addEditView->setVariables(
            array(
                'employeeDegreesOptions' => $employeeDegreesOptions,
                'employeeInternalPagesOptions' => $employeeInternalPagesOptions,
                'formData' => $formData
            )
        );
    }

    public function childRenderSettings()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        /**
         * Language id.
         */

        $select = $sql->select();
        $select->columns(array('language_id'))
            ->from('pages')
            ->where(array('page_id' => $this->blockData->page_id));
        $resultSet = $dbManager->getHydratedResultSet($select);
        $page = $resultSet->current();

        $languageId = (int)$page->language_id;

        /**
         * Available pages.
         */

        $select = $sql->select();
        $select->from('pages')
            ->where(array('language_id' => $languageId))
            ->order(array('title ASC'));
        $resultSet = $dbManager->getHydratedResultSet($select);

        $availablePages = array(
            '' => '------'
        );
        foreach ($resultSet as $page)
        {
            $availablePages[$page->page_id] = $page->title;
        }

        $this->settingsView->setVariables(
            array(
                'availablePages' => $availablePages,
            )
        );
    }

    public function hideEmployees()
    {
        $this->setRenderingEnabled(false);
    }

    public function renderEmployees(\Zend\Http\Request $request)
    {
        $settings = $this->getSettings();
        $pageManager = $this->getPageManager();

        /**
         * Post data.
         */

        $alphabetLetter = $request->getPost('alphabetLetter', '');
        $nameOrSurname = $request->getPost('nameOrSurname', '');
        $degreeId = $request->getPost('degreeId');

        /**
         * Getting id of page on which the employee's details should be displayed.
         */

        $employeeDetailsPageUrl = $this->getLastRecognizedEmployeeDetailsPageUrl();

        /**
         * Find employees based on POST data:
         */

        $employees = $this->findEmployees($alphabetLetter, $nameOrSurname, $degreeId);
        $employeesDividedByFirstLetter = $this->divideFoundEmployeesByFirstLetter($employees);
        $employeesDividedByDegree = $this->divideFoundEmployeesByDegree($employees);
        //$employeesDividedByEmployeePosition = $this->divideFoundEmployeesByEmployeePosition($employees);
        unset($employees);

        /**
         * Make alphabet based on first letter of found column's name:
         */

        $alphabet = array();
        $alphabetLetterCount = mb_strlen($this->alphabet);
        for ($i = 0; $i < $alphabetLetterCount; $i++)
        {
            $letter = mb_strtoupper(mb_substr($this->alphabet, $i, 1));
            $enabled = false;

            if ($employeesDividedByFirstLetter && array_key_exists($letter, $employeesDividedByFirstLetter))
            {
                $enabled = true;
            }

            $alphabet[] = array(
                'letter' => $letter,
                'css_classes' => $enabled ? 'enabled' : ''
            );
        }

        /**
         * Employee positions for Dropdown.
         */

        $employeeDegrees = $this->getEmployeeDegreesForDropdown();
        //$employeesPositions = $this->getEmployeesPositionsForDropdown();

        $this->blockView->setVariables(
            array(
                'alphabet' => $alphabet,
                'alphabetLetterCount' => $alphabetLetterCount,
                //'employees' => $employees,
                'employeesDividedByFirstLetter' => $employeesDividedByFirstLetter,
                'employeesDividedByDegree' => $employeesDividedByDegree,
                //'employeesDividedByEmployeePosition' => $employeesDividedByEmployeePosition,
                'employeeDegrees' => $employeeDegrees,
                //'employeesPositions' => $employeesPositions,
                'employeeDetailsPageUrl' => $employeeDetailsPageUrl
            )
        );
    }

    public function renderDegreesManagement(\Zend\Http\Request $request, $return = true)
    {
        $settings = $this->getSettings();

        $pageNumber = (int)$request->getPost('pageNumber') ? : 1;
        $itemCountPerPage = (int)$request->getPost('itemCountPerPage') ? : 10;
        $includePagesFrom = $request->getPost('includePagesFrom') ? : 'current-language';

        $dbManager = $this->getDatabaseManager();
        $dbAdapter = $dbManager->getAdapter();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();

        $select = $sql->select();
        $select->from(array('bed' => self::BLOCK_EMPLOYEES_DEGREES_TABLE_NAME))
            ->join(
                array('u' => 'users'),
                'u.user_id = bed.last_update_author_id',
                array('last_update_author' => 'login'),
                $select::JOIN_LEFT
            )
            ->where(array('bed.content_id' => $contentId))
            ->order('bed.title ASC');

        $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        $paginatorAdapter = new \Zend\Paginator\Adapter\DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new \Zend\Paginator\Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($pageNumber);
        $paginator->setItemCountPerPage($itemCountPerPage);

        $degreesManagementView = new \Zend\View\Model\ViewModel();
        $degreesManagementView->setTemplate('blocks/' . $this->blockViewName . '/degrees-management.phtml');
        $degreesManagementView->setVariables(
            array(
                'userLanguage' => $this->userLanguage,
                'user' => $this->user,
                'designMode' => $this->designMode,
                'blockData' => $this->blockData,
                'hasOwnSettings' => $this->hasOwnSettings,
                'settings' => $settings,
                'pageNumber' => $pageNumber,
                'itemCountPerPage' => $itemCountPerPage,
                'includePagesFrom' => $includePagesFrom,
                'employeeDegreesPaginator' => $paginator
            )
        );
        $degreesManagementViewHtml = $this->phpRenderer->render($degreesManagementView);

        if ($return)
        {
            return $degreesManagementViewHtml;
        }
        else
        {
            echo $degreesManagementViewHtml;
        }
    }

    public function renderAddEditDegree(\Zend\Http\Request $request, $return = true)
    {
        $settings = $this->getSettings();

        $pageNumber = (int)$request->getPost('pageNumber') ? : 1;
        $employeeCountPerPage = (int)$request->getPost('employeeCountPerPage') ? : 10;
        $includePagesFrom = $request->getPost('includePagesFrom') ? : 'current-language';

        $defaultFormData = array(
            'degreeId' => '',
            'degreeTitle' => '',
            'degreeIsPublished' => ''
        );
        $currentDegreeFormData = array();

        $degreeId = (int)$request->getPost('degreeId');
        if ($degreeId > 0)
        {
            $degree = $this->getDegreeById($degreeId);

            $currentDegreeFormData = array(
                'degreeId' => $degree->degree_id,
                'degreeTitle' => $degree->title,
                'degreeIsPublished' => $degree->is_published
            );
        }

        $formData = array_merge($defaultFormData, $currentDegreeFormData);

        $addEditDegreeView = new \Zend\View\Model\ViewModel();
        $addEditDegreeView->setTemplate('blocks/' . $this->blockViewName . '/add-edit-degree.phtml');
        $addEditDegreeView->setVariables(
            array(
                'userLanguage' => $this->userLanguage,
                'user' => $this->user,
                'designMode' => $this->designMode,
                'blockData' => $this->blockData,
                'hasOwnSettings' => $this->hasOwnSettings,
                'settings' => $settings,
                'pageNumber' => $pageNumber,
                'employeeCountPerPage' => $employeeCountPerPage,
                'includePagesFrom' => $includePagesFrom,
                'formData' => $formData
            )
        );
        $addEditDegreeViewHtml = $this->phpRenderer->render($addEditDegreeView);

        if ($return)
        {
            return $addEditDegreeViewHtml;
        }
        else
        {
            echo $addEditDegreeViewHtml;
        }
    }

    /**
     * Getters.
     */

    public function convertFoundEmployeesToArray($employees)
    {
        if (!$employees)
        {
            return false;
        }

        $employeeArray = array();

        foreach ($employees as $employee)
        {
            $employeeArray[] = $employee->toArray();
        }

        return $employeeArray;
    }

    public function divideFoundEmployeesByDegree($employees)
    {
        if (!$employees)
        {
            return false;
        }

        $columns = array();

        foreach ($employees as $employee)
        {
            $degreeTitle = $employee->degree_title;

            if (!array_key_exists($degreeTitle, $columns))
            {
                $columns[$degreeTitle] = '';
            }

            $columns[$degreeTitle][] = $employee->toArray();
        }

        ksort($columns);

        return $columns;
    }

    public function divideFoundEmployeesByEmployeePosition($employees)
    {
        if (!$employees)
        {
            return false;
        }

        $columns = array();

        foreach ($employees as $employee)
        {
            $position = $employee->position;

            if (!array_key_exists($position, $columns))
            {
                $columns[$position] = '';
            }

            $columns[$position][] = $employee->toArray();
        }

        ksort($columns);

        return $columns;
    }

    public function divideFoundEmployeesByFirstLetter($employees)
    {
        if (!$employees)
        {
            return false;
        }

        $columns = array();

        foreach ($employees as $employee)
        {
            $firstLetter = mb_strtoupper(mb_substr($employee->name_and_surname, 0, 1));

            if (!array_key_exists($firstLetter, $columns))
            {
                $columns[$firstLetter] = '';
            }

            $columns[$firstLetter][] = $employee->toArray();
        }

        ksort($columns);

        return $columns;
    }

    public function findEmployees($alphabetLetter = '', $nameAndSurname = '', $degreeId = null)
    {
        $alphabetLetter = mb_strtoupper(trim((string)$alphabetLetter));
        $nameAndSurname = trim((string)$nameAndSurname);
        $degreeId = (int)$degreeId ? : null;

        if (
            // Letter not found in alphabet:
            (($alphabetLetter != '') && (mb_strpos($this->alphabet, $alphabetLetter)) === false) ||
            (($degreeId !== null) && ($degreeId <= 0))
        )
        {
            return false;
        }

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(array('bee' => self::BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME))
            ->join(
                array('bed' => self::BLOCK_EMPLOYEES_DEGREES_TABLE_NAME),
                'bed.degree_id = bee.degree_id',
                array('degree_title' => 'title'),
                $select::JOIN_LEFT
            )
            ->join(
                array('u' => 'users'),
                'u.user_id = bee.last_update_author_id',
                array('last_update_author' => 'login'),
                $select::JOIN_LEFT
            )
            ->where(
                array(
                    'bee.content_id' => $contentId,
                    'bee.is_published' => 1,
                    'bed.content_id' => $contentId,
                    'bed.is_published' => 1
                )
            );

        if ($alphabetLetter != '')
        {
            $select->where->like('bee.name_and_surname', $alphabetLetter . '%');
        }
        else
        {
            if ($degreeId != '')
            {
                $select->where->equalTo('bee.degree_id', $degreeId);
            }

            if ($nameAndSurname != '')
            {
                $select->where->like('bee.name_and_surname', '%' . $nameAndSurname . '%');
            }
        }

        $select->order('bee.name_and_surname ASC');

        $result = $dbManager->executePreparedStatement($select);
        if ($result)
        {
            $employeeDetailsPageUrl = $this->getLastRecognizedEmployeeDetailsPageUrl();

            $resultSet = new \Zend\Db\ResultSet\HydratingResultSet(
                new EmployeeHydrator(
                    $this->attachmentManager,
                    $this,
                    $this->getTranslateHelper(),
                    $employeeDetailsPageUrl
                ),
                new \Site\Custom\FlexibleContainer()
            );
            $resultSet->initialize($result)->buffer();

            return $resultSet;
        }

        return false;
    }

    public function getAllEmployees()
    {
        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $this->getContentId()
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getLastRecognizedEmployeeDetailsPageUrl()
    {
        if ($this->employeeDetailsPageUrl == '')
        {
            $settings = $this->getSettings();
            $pageManager = $this->getPageManager();

            $detailsPageId = (int)$settings->detailsPageId;
            if ($detailsPageId > 0)
            {
                $this->employeeDetailsPageUrl = $pageManager->getFullPageCleanUrlByPageId($detailsPageId);
            }
        }

        return $this->employeeDetailsPageUrl;
    }

    public function getDegrees()
    {
        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_EMPLOYEES_DEGREES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('content_id' => $this->getContentId()));
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getDegreeById($degreeId)
    {
        $degreeId = (int)$degreeId;

        if ($degreeId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_EMPLOYEES_DEGREES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'degree_id' => (int)$degreeId,
                    'content_id' => $this->getContentId()
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getDegreeCount()
    {
        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_EMPLOYEES_DEGREES_TABLE_NAME)
            ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')), $prefixColumnsWithTable)
            ->where(array('content_id' => $this->getContentId()));
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();
            $count = $row ? $row->count : false;

            return $count;
        }

        return false;
    }

    public function getEmployeeById($employeeId)
    {
        $employeeId = (int)$employeeId;

        if ($employeeId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'employee_id' => $employeeId,
                    'content_id' => $this->getContentId()
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getEmployeeByIdAndContentId($employeeId, $contentId)
    {
        $employeeId = (int)$employeeId;
        $contentId = (int)$contentId;

        if (($employeeId <= 0) || ($contentId <= 0))
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'employee_id' => $employeeId,
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getEmployeePartialDataFromEmployeePartialDataString($employeePartialDataString)
    {
        $parts = explode('-', $employeePartialDataString);

        $employeeId = (int)array_pop($parts);
        $contentId = (int)array_pop($parts);
        $employeeCleanUrl = join('-', $parts);

        return array($employeeCleanUrl, $contentId, $employeeId);
    }

    public function getEmployeePartialDataFromRequest(Request $request)
    {
        $parameters = $this->getParametersForCurrentPageFromRequest($request);
        $employeePartialDataString = $parameters->key(); // Get name of the first parameter.

        return $this->getEmployeePartialDataFromEmployeePartialDataString($employeePartialDataString);
    }

    public function getEmployeeDegreesForDropdown()
    {
        $translate = $this->getTranslateHelper();

        $employeeDegrees = array(
            '' => '-- ' . $translate('All employees positions', 'default', $this->userLanguage->zend2_locale) . ' --'
        );

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->quantifier('DISTINCT')
            ->from(self::BLOCK_EMPLOYEES_DEGREES_TABLE_NAME)
            ->columns(array('degree_id', 'title'), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $this->getContentId()
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            foreach ($resultSet as $row)
            {
                $employeeDegrees[$row->degree_id] = $row->title;
            }
        }

        return $employeeDegrees;
    }

    public function getEmployeesPositionsForDropdown()
    {
        $translate = $this->getTranslateHelper();

        $employeesPositions = array(
            '' => '-- ' . $translate('All employees positions', 'default', $this->userLanguage->zend2_locale) . ' --'
        );

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->quantifier('DISTINCT')
            ->from(self::BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME)
            ->columns(array('position'), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $this->getContentId()
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            foreach ($resultSet as $row)
            {
                $employeesPositions[] = $row->position;
            }
        }

        return $employeesPositions;
    }

    /**
     * Data formatting functions.
     */

    public function generateCleanUrlFromEmployeeNameAndSurname($nameAndSurname)
    {
        return $this->generateCleanUrlFromText($nameAndSurname);
    }

    public function formatDegreeDataAndReturnIfAreValid($degreeData)
    {
        $degreeData = $this->convertToFlexibleContainer($degreeData);
        $degreeDataCopy = clone $degreeData;

        $degreeDataCopy->degreeId = (int)$degreeData->degreeId ? : null;
        $degreeDataCopy->degreeIsPublished = (int)$degreeData->degreeIsPublished ? 1 : 0;
        $degreeDataCopy->degreeTitle = trim((string)$degreeData->degreeTitle);

        /**
         * Identifying the author.
         */
        $degreeDataCopy->degreeLastUpdateAuthorId = $this->user->user_id;

        if (
            (($degreeDataCopy->degreeId !== null) && ($degreeDataCopy->degreeId <= 0)) ||
            ($degreeDataCopy->degreeTitle == '')
        )
        {
            return false;
        }

        return $degreeDataCopy;
    }

    public function formatEmployeeDataAndReturnIfAreValid($employeeData)
    {
        $employeeData = $this->convertToFlexibleContainer($employeeData);
        $employeeDataCopy = clone $employeeData;

        $employeeDataCopy->employeeId = (int)$employeeData->employeeId ? : null;
        $employeeDataCopy->employeeDegreeId = (int)$employeeData->employeeDegreeId;
        $employeeDataCopy->employeeAttachmentId = (int)$employeeData->employeeAttachmentId ? : null;
        $employeeDataCopy->employeeIsPublished = (int)$employeeDataCopy->employeeIsPublished ? 1 : 0;
        $employeeDataCopy->employeeNameAndSurname = trim((string)$employeeData->employeeNameAndSurname);
        $employeeDataCopy->employeePosition = trim((string)$employeeData->employeePosition);
        $employeeDataCopy->employeeAbout = trim((string)$employeeData->employeeAbout);
        $employeeDataCopy->employeeScientificCareer = trim((string)$employeeData->employeeScientificCareer);
        $employeeDataCopy->employeeTheMostImportantPublications =
            trim((string)$employeeData->employeeTheMostImportantPublications);
        $employeeDataCopy->employeeContactInfo = trim((string)$employeeData->employeeContactInfo);

        /**
         * Temporary data - from add-form. They will be parsed and deleted.
         */
        $employeeDataCopy->employeeUniqueHashForFileName = trim((string)$employeeData->employeeImage[0]['uniqueHashForFileName']);

        /**
         * Validating form data.
         */
        if (
            // Validate unique hash for file name only if it is not empty!
            ($employeeDataCopy->employeeUniqueHashForFileName != '') &&
            !preg_match('/^([a-z0-9]{40})$/i', $employeeDataCopy->employeeUniqueHashForFileName)
        )
        {
            return false;
        }

        /**
         * Identifying the author.
         */
        $employeeDataCopy->employeeLastUpdateAuthorId = $this->user->user_id;

        /**
         * Parsing unique hash for file name.
         */
        $employeeDataCopy->employeeAttachmentId = null;
        if ($employeeDataCopy->employeeUniqueHashForFileName != '')
        {
            $attachmentManager = $this->getAttachmentManager();
            $attachment = $attachmentManager->getAttachmentByUniqueHashForFileName(
                $employeeDataCopy->employeeUniqueHashForFileName
            );
            if (!$attachment)
            {
                return false;
            }

            $employeeDataCopy->employeeAttachmentId = $attachment->attachment_id;
        }

        /**
         * Removing unnecessary data.
         */
        unset($employeeDataCopy->employeeUniqueHashForFileName);

        if (
            (($employeeDataCopy->employeeId !== null) && ($employeeDataCopy->employeeId <= 0)) ||
            ($employeeDataCopy->employeeDegreeId <= 0) ||
            (($employeeDataCopy->employeeAttachmentId !== null) && ($employeeDataCopy->employeeAttachmentId <= 0))
        )
        {
            return false;
        }

        return $employeeDataCopy;
    }

    /**
     * Logic-functions.
     */

    public function addOrEditDegree($degreeData)
    {
        $degreeDataCopy = $this->formatDegreeDataAndReturnIfAreValid($degreeData);
        if (!$degreeDataCopy)
        {
            return false;
        }

        $degreeDataCopy->degreeLastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        if ($degreeData->degreeId > 0)
        {
            return $this->updateExistingDegree($degreeDataCopy);
        }
        else
        {
            return $this->createNewDegree($degreeDataCopy);
        }
    }

    public function addOrEditEmployee($employeeData)
    {
        $employeeDataCopy = $this->formatEmployeeDataAndReturnIfAreValid($employeeData);
        if (!$employeeDataCopy)
        {
            return false;
        }

        $employeeDataCopy->employeeLastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        if ($employeeData->employeeId > 0)
        {
            return $this->updateExistingEmployee($employeeDataCopy);
        }
        else
        {
            return $this->createNewEmployee($employeeDataCopy);
        }
    }

    public function decideWhatToDoWithRecentlyUploadedAndMovedFiles(\Zend\Http\Request $request, $movedFiles)
    {
        //$postData = $request->getPost();

        // Inform the UploadController and UploadManager than upload was successful and you accept these files:
        return true;
    }

    public function deleteAllDegrees()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_EMPLOYEES_DEGREES_TABLE_NAME);
        $delete->where(array('content_id' => $this->getContentId()));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;
    }

    public function deleteAllEmployees()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME);
        $delete->where(array('content_id' => $this->getContentId()));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;
    }

    public function toggleDegreePublished($degreeId, $published)
    {
        $degreeId = (int)$degreeId;
        $published = (int)$published ? 1 : 0;

        if ($degreeId <= 0)
        {
            return false;
        }

        $updated = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $degree = $this->getDegreeById($degreeId);
        if ($degree)
        {
            $update = $sql->update(self::BLOCK_EMPLOYEES_DEGREES_TABLE_NAME);
            $update
                ->set(
                    array(
                        'is_published' => $published
                    )
                )
                ->where(
                    array(
                        'degree_id' => $degreeId,
                        'content_id' => $this->getContentId()
                    )
                );
            $result = $dbManager->executePreparedStatement($update);
            if ($result)
            {
                $degree = $this->getDegreeById($degreeId);
                if ($degree && ($degree->is_published == $published))
                {
                    $updated = true;
                }
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function toggleEmployeePublished($employeeId, $published)
    {
        $employeeId = (int)$employeeId;
        $published = (int)$published ? 1 : 0;

        if ($employeeId <= 0)
        {
            return false;
        }

        $updated = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $employee = $this->getEmployeeById($employeeId);
        if ($employee)
        {
            $update = $sql->update(self::BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME);
            $update
                ->set(
                    array(
                        'is_published' => $published,
                        'content_id' => $this->getContentId()
                    )
                )
                ->where(array('employee_id' => $employeeId));
            $result = $dbManager->executePreparedStatement($update);
            if ($result)
            {
                $employee = $this->getEmployeeById($employeeId);
                if ($employee && ($employee->is_published == $published))
                {
                    $updated = true;
                }
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    private function createNewDegree($degreeData)
    {
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $insert = $sql->insert(self::BLOCK_EMPLOYEES_DEGREES_TABLE_NAME);
        $insert
            ->columns(
                array(
                    'content_id',
                    'is_published',
                    'last_update_author_id',
                    'last_update_date',
                    'title'
                )
            )
            ->values(
                array(
                    'content_id' => $contentId,
                    'is_published' => $degreeData->degreeIsPublished,
                    'last_update_author_id' => $degreeData->degreeLastUpdateAuthorId,
                    'last_update_date' => $degreeData->degreeLastUpdateDate,
                    'title' => $degreeData->degreeTitle
                )
            );
        $degreeId = $dbManager->executeInsertAndGetLastInsertId($insert);

        if ($degreeId > 0)
        {
            $dbManager->commit();

            return $degreeId;
        }

        $dbManager->rollback();

        return false;
    }

    private function updateExistingDegree($degreeData)
    {
        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_EMPLOYEES_DEGREES_TABLE_NAME);
        $update
            ->set(
                array(
                    'is_published' => $degreeData->degreeIsPublished,
                    'last_update_author_id' => $degreeData->degreeLastUpdateAuthorId,
                    'last_update_date' => $degreeData->degreeLastUpdateDate,
                    'title' => $degreeData->degreeTitle
                )
            )
            ->where(
                array(
                    'degree_id' => $degreeData->degreeId,
                    'content_id' => $contentId
                )
            );
        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $degree = $this->getDegreeById($degreeData->degreeId);
            if ($degree &&
                ($degree->is_published == $degreeData->degreeIsPublished) &&
                ($degree->last_update_author_id == $degreeData->degreeLastUpdateAuthorId) &&
                ($degree->title == $degreeData->degreeTitle)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    private function createNewEmployee($employeeData)
    {
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $insert = $sql->insert(self::BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME);
        $insert
            ->columns(
                array(
                    'content_id',
                    'degree_id',
                    'attachment_id',
                    'is_published',
                    'last_update_author_id',
                    'last_update_date',
                    'name_and_surname',
                    'position',
                    'about',
                    'scientific_career',
                    'most_important_publications',
                    'contact_info'
                )
            )
            ->values(
                array(
                    'content_id' => $contentId,
                    'degree_id' => $employeeData->employeeDegreeId,
                    'attachment_id' => $employeeData->employeeAttachmentId,
                    'is_published' => $employeeData->employeeIsPublished,
                    'last_update_author_id' => $employeeData->employeeLastUpdateAuthorId,
                    'last_update_date' => $employeeData->employeeLastUpdateDate,
                    'name_and_surname' => $employeeData->employeeNameAndSurname,
                    'position' => $employeeData->employeePosition,
                    'about' => $employeeData->employeeAbout,
                    'scientific_career' => $employeeData->employeeScientificCareer,
                    'most_important_publications' => $employeeData->employeeTheMostImportantPublications,
                    'contact_info' => $employeeData->employeeContactInfo
                )
            );
        $employeeId = $dbManager->executeInsertAndGetLastInsertId($insert);
        if ($employeeId > 0)
        {
            $employeeCleanUrl = $this->generateCleanUrlFromEmployeeNameAndSurname(
                $employeeData->employeeNameAndSurname
            );
            $elementUrlParametersString = $employeeCleanUrl . '-' . $contentId . '-' . $employeeId;

            $this->updateBlockRecordInSearchIndex(
                $employeeId, $employeeData->employeeAbout, $elementUrlParametersString, 'About employee'
            );

            $dbManager->commit();

            return $employeeId;
        }

        $dbManager->rollback();

        return false;
    }

    private function updateExistingEmployee($employeeData)
    {
        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME);
        $update
            ->set(
                array(
                    'degree_id' => $employeeData->employeeDegreeId,
                    'attachment_id' => $employeeData->employeeAttachmentId,
                    'is_published' => $employeeData->employeeIsPublished,
                    'last_update_author_id' => $employeeData->employeeLastUpdateAuthorId,
                    'last_update_date' => $employeeData->employeeLastUpdateDate,
                    'name_and_surname' => $employeeData->employeeNameAndSurname,
                    'position' => $employeeData->employeePosition,
                    'about' => $employeeData->employeeAbout,
                    'scientific_career' => $employeeData->employeeScientificCareer,
                    'most_important_publications' => $employeeData->employeeTheMostImportantPublications
                )
            )
            ->where(
                array(
                    'employee_id' => $employeeData->employeeId,
                    'content_id' => $contentId
                )
            );

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $employee = $this->getEmployeeById($employeeData->employeeId);
            if ($employee &&
                ($employee->degree_id == $employeeData->employeeDegreeId) &&
                ($employee->attachment_id == $employeeData->employeeAttachmentId) &&
                ($employee->is_published == $employeeData->employeeIsPublished) &&
                ($employee->last_update_author_id == $employeeData->employeeLastUpdateAuthorId) &&
                ($employee->name_and_surname == $employeeData->employeeNameAndSurname) &&
                ($employee->position == $employeeData->employeePosition) &&
                ($employee->about == $employeeData->employeeAbout) &&
                ($employee->scientific_career == $employeeData->employeeScientificCareer) &&
                ($employee->most_important_publications == $employeeData->employeeTheMostImportantPublications) &&
                ($employee->contact_info == $employeeData->employeeContactInfo)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $employeeCleanUrl = $this->generateCleanUrlFromEmployeeNameAndSurname(
                $employeeData->employeeNameAndSurname
            );
            $elementUrlParametersString = $employeeCleanUrl . '-' . $contentId . '-' . $employeeData->employeeId;

            $this->updateBlockRecordInSearchIndex(
                $employeeData->employeeId, $employeeData->employeeAbout, $elementUrlParametersString, 'About employee'
            );

            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }
}
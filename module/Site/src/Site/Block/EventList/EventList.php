<?php
namespace Site\Block\EventList;

use Site\Block\AbstractBlock;
use Zend\Http\Request;

class EventList extends AbstractBlock
{
    const BLOCK_EVENT_LIST_TABLE_NAME = 'block_event_list';
    const BLOCK_EVENT_LIST_CATEGORIES_TABLE_NAME = 'block_event_list_categories';
    const BLOCK_EVENT_LIST_ADDITIONAL_DATA_TYPES_TABLE_NAME = 'block_event_list_additional_data_types';
    const BLOCK_EVENT_LIST_ADDITIONAL_DATA_TABLE_NAME = 'block_event_list_additional_data';
    const BLOCK_EVENT_LIST_EVENT_IMAGES_TABLE_NAME = 'block_event_list_event_images';
    const BLOCK_EVENT_LIST_USER_SUBSCRIPTIONS_TABLE_NAME = 'block_event_list_user_subscriptions';

    protected $hasOwnSettings = true;
    protected $hasManagementWindow = true;

    /**
     * Events.
     */

    public function onAfterInserting($slotId)
    {
        return $this->createDefaultAdditionalDataTypes();
    }

    public function onBeforeDeleted()
    {
        if (!$this->blockData->is_dynamic)
        {
            $this->deleteAllAdditionalData();
            $this->deleteAllAdditionalDataTypes();
            $this->deleteAllImages();
            $this->deleteAllEvents();
            $this->deleteAllCategories();
            $this->deleteBlockRecordFromSearchIndex();
        }
    }

    public function updateSearchIndex()
    {
        $allEvents = $this->getAllEvents();

        if ($allEvents)
        {
            $contentId = $this->getContentId();

            foreach ($allEvents as $event)
            {
                $eventCleanUrl = $this->generateCleanUrlFromEventTitle($event->title);
                $elementUrlParametersString =
                    $eventCleanUrl . '-' . $contentId . '-' . $event->event_id;

                $this->updateBlockRecordInSearchIndex(
                    $event->event_id, $event->contents, $elementUrlParametersString, 'Event',
                    $event->event_date_start
                );
            }
        }
    }

    /**
     * Render-functions.
     */

    public function childRender(Request $request)
    {
        $settings = $this->getSettings();
        list($currentEventCleanUrl, $currentEventContentId) = $this->getEventCleanUrlAndContentIdFromRequest($request);

        $eventListShouldBeRendered = true;
        $shouldBeHiddenIfCurrentUrlLeadsToEventDetails = $settings->hideIfCurrentUrlLeadsToEventDetails ? true : false;

        $currentEventId = null;
        if ($shouldBeHiddenIfCurrentUrlLeadsToEventDetails)
        {
            // Data needed for displaying event's details are correct:
            if (($currentEventCleanUrl != '') && ($currentEventContentId > 0))
            {
                $event = $this->getEventByEventCleanUrlAndContentId($currentEventCleanUrl, $currentEventContentId);
                if ($event) // Event exists.
                {
                    $currentEventId = $event->event_id;
                    $eventListShouldBeRendered = false;
                }
            }
        }

        $this->blockView->setVariables(
            array(
                'currentEventId' => $currentEventId,
                'currentEventCleanUrl' => $currentEventCleanUrl,
                'currentEventContentId' => $currentEventContentId,
                'currentEventDataAreValid' => (($currentEventCleanUrl != '') && ($currentEventContentId > 0))
            )
        );

        if ($eventListShouldBeRendered)
        {
            $this->renderEventList($request);
        }
        else
        {
            $this->hideEventList();
        }
    }

    public function childRenderAddEdit(Request $request)
    {
        if (!$this->addEditView instanceof \Zend\View\Model\ViewModel)
        {
            return false;
        }

        $settings = $this->getSettings();

        $eventCategoriesOptions = array();

        $categories = $this->getCategories();
        if ($categories)
        {
            foreach ($categories as $category)
            {
                $eventCategoriesOptions[$category->category_id] = $category->name;
            }
        }

        $defaultFormData = array(
            'eventId' => '',
            'eventCategoryId' => '',
            'eventIsApproved' => 0,
            'eventIsSigningUpEnabled' => 0,
            'eventAuthorEmail' => '',
            'eventCleanUrl' => '',
            'eventTitle' => '',
            'eventLabel' => '',
            'eventCreationDate' => '',
            'eventApprovalDate' => '',
            'eventLastUpdateAuthorId' => '',
            'eventDayRange' => date('Y-m-d') . ' - ' . date('Y-m-d', strtotime('+7 day')),
            'eventStartHour' => '',
            'eventEndHour' => '',
            'eventImages' => array(
                '0' => array(
                    'imageId' => null,
                    'isMain' => true,
                    'attachmentId' => null,
                    'uniqueHashForFileName' => '',
                    'originalFileName' => '',
                )
            ),
            'eventTeaser' => '',
            'eventContents' => '',
            'eventAuthorAdditionalContactData' => '',
            'eventAttentions' => '',
            'eventAdditionalData' => new \Site\Custom\FlexibleContainer(),
            'eventContactPersonEmail' => '',
            'eventContactPersonPhone' => '',
            'eventIsPublished' => 0,
            'eventIsPromoted' => 0
        );
        $currentEventFormData = array();

        $eventId = (int)$request->getPost('eventId');
        if ($eventId > 0)
        {
            $eventImagesArray = array(
                '0' => array(
                    'imageId' => null,
                    'isMain' => true,
                    'attachmentId' => null,
                    'uniqueHashForFileName' => '',
                    'originalFileName' => '',
                )
            );

            $event = $this->getEventById($eventId);
            $eventImages = $this->getEventImagesByEventId($eventId);
            if ($eventImages)
            {
                $imageNumber = 0;
                foreach ($eventImages as $eventImage)
                {
                    $attachmentId = $eventImage->attachment_id;
                    $attachment = $this->attachmentManager->getAttachmentById($attachmentId);

                    $eventImagesArray[$imageNumber] = new \Site\Custom\FlexibleContainer();
                    $eventImagesArray[$imageNumber]->imageId = $eventImage->image_id;
                    $eventImagesArray[$imageNumber]->isMain = $eventImage->is_main ? true : false;
                    $eventImagesArray[$imageNumber]->attachmentId = $attachmentId;
                    $eventImagesArray[$imageNumber]->uniqueHashForFileName
                        = $attachment ? $attachment->hash_for_file_name : '';
                    $eventImagesArray[$imageNumber]->originalFileName
                        = $attachment ? $attachment->original_file_name : '';

                    $imageNumber++;
                }
            }

            $currentYear = date('Y');
            $startTime = strtotime(str_replace('0000-00-00 00:00:00', $currentYear . '-01-01 00:00:00', $event->event_date_start));
            $endTime = strtotime(str_replace('0000-00-00 00:00:00', $currentYear . '-01-01 00:00:00', $event->event_date_end));

            $eventDayRange = date('Y-m-d', $startTime) . ' - ' . date('Y-m-d', $endTime);
            $eventStartHour = date('H:i', $startTime);
            $eventEndHour = date('H:i', $endTime);

            $additionalData = $this->getAdditionalDataByEventId($eventId);

            $currentEventFormData = array(
                'eventId' => $event->event_id,
                'eventCategoryId' => $event->category_id,
                'eventIsApproved' => $event->is_approved,
                'eventIsSigningUpEnabled' => $event->is_signing_up_enabled,
                'eventAuthorEmail' => $event->author_email,
                'eventCleanUrl' => $event->clean_url,
                'eventTitle' => $event->title,
                'eventLabel' => $event->label,
                'eventCreationDate' => $event->creation_date,
                'eventApprovalDate' => $event->approval_date,
                'eventLastUpdateAuthorId' => $event->last_update_author_id,
                'eventDayRange' => $eventDayRange,
                'eventStartHour' => $eventStartHour,
                'eventEndHour' => $eventEndHour,
                'eventImages' => $eventImagesArray,
                'eventTeaser' => $event->teaser,
                'eventContents' => $event->contents,
                'eventAuthorAdditionalContactData' => $event->additional_contact_data,
                'eventAttentions' => $event->attentions,
                'eventAdditionalData' => $additionalData,
                'eventContactPersonEmail' => $event->contact_person_email,
                'eventContactPersonPhone' => $event->contact_person_phone,
                'eventIsPublished' => $event->is_published,
                'eventIsPromoted' => $event->is_promoted
            );
        }

        $formData = array_merge($defaultFormData, $currentEventFormData);

        $defaultSubjectForMailAboutEventApproval = (string)$settings->defaultSubjectForMailAboutEventApproval;
        $defaultContentsForMailAboutEventApproval = (string)$settings->defaultContentsForMailAboutEventApproval;

        // If no email or phone was set to this event - use the default from settings:
        if ($formData['eventContactPersonEmail'] == '')
        {
            $formData['eventContactPersonEmail'] = (string)$settings->contactPersonEmail;
        }
        if ($formData['eventContactPersonPhone'] == '')
        {
            $formData['eventContactPersonPhone'] = (string)$settings->contactPersonPhone;
        }

        $this->addEditView->setVariables(
            array(
                'eventCategoriesOptions' => $eventCategoriesOptions,
                'formData' => $formData,
                'defaultSubjectForMailAboutEventApproval' => $defaultSubjectForMailAboutEventApproval,
                'defaultContentsForMailAboutEventApproval' => $defaultContentsForMailAboutEventApproval
            )
        );
    }

    public function childRenderManagement(Request $request)
    {
        if (!$this->managementView instanceof \Zend\View\Model\ViewModel)
        {
            return false;
        }

        $pageNumber = $this->managementView->pageNumber;
        $itemCountPerPage = $this->managementView->itemCountPerPage;
        $filterEvents = $this->managementView->additionalData->filterEvents;
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $dbAdapter = $dbManager->getAdapter();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(array('bel' => self::BLOCK_EVENT_LIST_TABLE_NAME))
            ->join(
                array('belc' => self::BLOCK_EVENT_LIST_CATEGORIES_TABLE_NAME),
                'belc.category_id = bel.category_id',
                array('category_name' => 'name'),
                $select::JOIN_LEFT
            )
            ->join(
                array('u' => 'users'),
                'u.user_id = bel.last_update_author_id',
                array('last_update_author' => 'login'),
                $select::JOIN_LEFT
            )
            ->where(
                array(
                    'bel.content_id' => $contentId,
                    'belc.content_id' => $contentId
                )
            );

        if ($filterEvents == 'only-approved')
        {
            $select->where->equalTo('bel.is_approved', 1);
        }
        else if ($filterEvents == 'only-not-approved')
        {
            $select->where->equalTo('bel.is_approved', 0);
        }

        $select->order('bel.is_promoted DESC, bel.event_date_end DESC, bel.event_date_start DESC');

        //echo $sql->getSqlStringForSqlObject($select);

        $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        $paginatorAdapter = new \Zend\Paginator\Adapter\DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new \Zend\Paginator\Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($pageNumber);
        $paginator->setItemCountPerPage($itemCountPerPage);

        $this->managementView->setVariables(
            array(
                'filterEvents' => $filterEvents,
                'eventCategoryCount' => $this->getCategoryCount(),
                'eventPaginator' => $paginator
            )
        );
    }

    public function childRenderSettings()
    {
        $translate = $this->getTranslateHelper();
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        /**
         * Available categories.
         */
        $availableCategories = array(
            '0' => '-- ' . $translate('All categories', 'default', $this->userLanguage->zend2_locale) . ' --'
        );
        $categories = $this->getCategories();
        if ($categories)
        {
            foreach ($categories as $category)
            {
                $availableCategories[$category->category_id] = $category->name;
            }
        }

        /**
         * Language id.
         */
        $select = $sql->select();
        $select
            ->columns(array('language_id'))
            ->from('pages')
            ->where(array('page_id' => $this->blockData->page_id));
        $resultSet = $dbManager->getHydratedResultSet($select);
        $page = $resultSet->current();

        $languageId = (int)$page->language_id;

        /**
         * Available pages.
         */
        $select = $sql->select();
        $select
            ->from('pages')
            ->where(array('language_id' => $languageId))
            ->order(array('title ASC'));
        $resultSet = $dbManager->getHydratedResultSet($select);

        $availablePages = array(
            '' => '------'
        );
        foreach ($resultSet as $page)
        {
            $availablePages[$page->page_id] = $page->title;
        }

        $this->settingsView->setVariables(
            array(
                'availableCategories' => $availableCategories,
                'availablePages' => $availablePages,
            )
        );
    }

    public function hideEventList()
    {
        $this->setRenderingEnabled(false);
    }

    public function renderEventList(Request $request)
    {
        $settings = $this->getSettings();

        $pageNumber = (int)$this->blockView->pageNumber ? : 1;
        $itemCountPerPage = (int)$settings->itemCountPerPage ? : 7;
        $contentId = $this->getContentId();
        $categoryId = (int)$settings->categoryId;

        $pageManager = $this->getPageManager();
        $dbManager = $this->getDatabaseManager();
        $dbAdapter = $dbManager->getAdapter();
        $sql = $dbManager->getSql();

        $eventDetailsPageUrl = '';
        $detailsPageId = (int)$settings->detailsPageId;
        if ($detailsPageId > 0)
        {
            $eventDetailsPageUrl = $pageManager->getFullPageCleanUrlByPageId($detailsPageId);
        }

        $eventCategoriesOptions = array();
        $eventCategoryCount = $this->getCategoryCount();
        if ($eventCategoryCount > 0)
        {
            $categories = $this->getCategories();
            if ($categories)
            {
                foreach ($categories as $category)
                {
                    $eventCategoriesOptions[$category->category_id] = $category->name;
                }
            }
        }

        $select = $sql->select();
        $select
            ->from(array('bel' => self::BLOCK_EVENT_LIST_TABLE_NAME))
            ->join(
                array('belc' => self::BLOCK_EVENT_LIST_CATEGORIES_TABLE_NAME),
                'belc.category_id = bel.category_id',
                array('category_name' => 'name'),
                $select::JOIN_LEFT
            )
            ->join(
                array('u' => 'users'),
                'u.user_id = bel.last_update_author_id',
                array('last_update_author' => 'login'),
                $select::JOIN_LEFT
            )
            ->where(
                array(
                    'bel.content_id' => $contentId,
                    'bel.is_published' => 1,
                    'belc.content_id' => $contentId,
                    'belc.is_published' => 1
                )
            );

        if ($categoryId > 0)
        {
            $select->where->equalTo('bel.category_id', $categoryId);
        }

        $select->order('bel.is_promoted DESC, bel.event_date_end DESC, bel.event_date_start DESC');

        $resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet(
            new EventHydrator($this->getTranslateHelper(), $this->attachmentManager, $this),
            new \Site\Custom\FlexibleContainer()
        );
        $paginatorAdapter = new \Zend\Paginator\Adapter\DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new \Zend\Paginator\Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($pageNumber);
        $paginator->setItemCountPerPage($itemCountPerPage);

        /**
         * Check if e-mails data was given in block settings.
         */
        $dataForEmailsAboutUserSignedUpForEventWasGiven =
            ($settings->subjectForMailForUserAboutUserSignedUpForEvent != '') &&
            ($settings->contentsForMailForUserAboutUserSignedUpForEvent != '') &&
            ($settings->subjectForMailForAdminAboutUserSignedUpForEvent != '') &&
            ($settings->contentsForMailForAdminAboutUserSignedUpForEvent != '')
        ;

        $this->blockView->setVariables(
            array(
                'eventDetailsPageUrl' => $eventDetailsPageUrl,
                'eventCategoryCount' => $eventCategoryCount,
                'eventCategoriesOptions' => $eventCategoriesOptions,
                'eventPaginator' => $paginator,
                'dataForEmailsAboutUserSignedUpForEventWasGiven' => $dataForEmailsAboutUserSignedUpForEventWasGiven
            )
        );
    }

    public function renderAddEditCategory(Request $request, $return = true)
    {
        $settings = $this->getSettings();

        $pageNumber = (int)$request->getPost('pageNumber') ? : 1;
        $itemCountPerPage = (int)$request->getPost('itemCountPerPage') ? : 10;
        $includePagesFrom = $request->getPost('includePagesFrom') ? : 'current-language';

        $defaultFormData = array(
            'categoryId' => '',
            'categoryTitle' => '',
            'publishNow' => ''
        );
        $currentCategoryFormData = array();

        $categoryId = (int)$request->getPost('categoryId');
        if ($categoryId > 0)
        {
            $category = $this->getCategoryById($categoryId);

            $currentCategoryFormData = array(
                'categoryId' => $category->category_id,
                'categoryTitle' => $category->name,
                'publishNow' => $category->is_published
            );
        }

        $formData = array_merge($defaultFormData, $currentCategoryFormData);

        $addEditCategoryView = new \Zend\View\Model\ViewModel();
        $addEditCategoryView->setTemplate('blocks/' . $this->blockViewName . '/add-edit-category.phtml');
        $addEditCategoryView->setVariables(
            array(
                'user' => $this->user,
                'userLanguage' => $this->userLanguage,
                'designMode' => $this->designMode,
                'blockData' => $this->blockData,
                'hasOwnSettings' => $this->hasOwnSettings,
                'settings' => $settings,
                'pageNumber' => $pageNumber,
                'itemCountPerPage' => $itemCountPerPage,
                'includePagesFrom' => $includePagesFrom,
                'formData' => $formData
            )
        );
        $addEditCategoryViewHtml = $this->phpRenderer->render($addEditCategoryView);

        if ($return)
        {
            return $addEditCategoryViewHtml;
        }
        else
        {
            echo $addEditCategoryViewHtml;
        }
    }

    public function renderCategoriesManagement(Request $request, $return = true)
    {
        $settings = $this->getSettings();

        $pageNumber = (int)$request->getPost('pageNumber') ? : 1;
        $itemCountPerPage = (int)$request->getPost('itemCountPerPage') ? : 10;
        $includePagesFrom = $request->getPost('includePagesFrom') ? : 'current-language';

        $dbManager = $this->getDatabaseManager();
        $dbAdapter = $dbManager->getAdapter();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();

        $select = $sql->select();
        $select
            ->from(array('bec' => self::BLOCK_EVENT_LIST_CATEGORIES_TABLE_NAME))
            ->join(
                array('u' => 'users'),
                'u.user_id = bec.last_update_author_id',
                array('last_update_author' => 'login'),
                $select::JOIN_LEFT
            )
            ->where(array('bec.content_id' => $contentId))
            ->order('bec.name ASC');

        $resultSetPrototype = new \Zend\Db\ResultSet\ResultSet();
        $paginatorAdapter = new \Zend\Paginator\Adapter\DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new \Zend\Paginator\Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($pageNumber);
        $paginator->setItemCountPerPage($itemCountPerPage);

        $categoriesManagementView = new \Zend\View\Model\ViewModel();
        $categoriesManagementView->setTemplate('blocks/' . $this->blockViewName . '/categories-management.phtml');
        $categoriesManagementView->setVariables(
            array(
                'user' => $this->user,
                'userLanguage' => $this->userLanguage,
                'designMode' => $this->designMode,
                'blockData' => $this->blockData,
                'hasOwnSettings' => $this->hasOwnSettings,
                'settings' => $settings,
                'pageNumber' => $pageNumber,
                'itemCountPerPage' => $itemCountPerPage,
                'includePagesFrom' => $includePagesFrom,
                'eventCategoriesPaginator' => $paginator
            )
        );
        $categoriesManagementViewHtml = $this->phpRenderer->render($categoriesManagementView);

        if ($return)
        {
            return $categoriesManagementViewHtml;
        }
        else
        {
            echo $categoriesManagementViewHtml;
        }
    }

    public function renderSignUpForm(Request $request, $return = true)
    {
        $eventId = $request->getPost('eventId');
        $subscriptionId = $request->getPost('subscriptionId');

        if ($subscriptionId > 0)
        {
            $subscriptionData = $this->getEventUserSubscriptionDataBySubscriptionIdAndEventId($subscriptionId, $eventId);
            if ($subscriptionData)
            {
                $formData = new \Site\Custom\FlexibleContainer();
                $formData->signUpSubscriptionId = $subscriptionData->subscription_id;
                $formData->signUpEventId = $subscriptionData->event_id;
                $formData->signUpAcademicTitle = $subscriptionData->academic_title;
                $formData->signUpNameAndSurname = $subscriptionData->name_and_surname;
                $formData->signUpGender = ($subscriptionData->gender == 0) ? 'male' : 'female';
                $formData->signUpEmail = $subscriptionData->email;
                $formData->signUpPhone = $subscriptionData->phone;
                $formData->signUpType = ($subscriptionData->type_of_request == 0) ? 'private_person' : 'institution';
                $formData->signUpInstitutionName = $subscriptionData->institution_name;
                $formData->signUpInvoiceData = $subscriptionData->invoice_data;
                $formData->signUpAttentions = $subscriptionData->attentions;
                $formData->signUpNewsletterAgreement = $subscriptionData->newsletter_agreement;
                $formData->signUpDataProcessingAgreement = 1;
            }
        }
        else
        {
            $formData = $request->getPost('formData');
            $formData = new \Site\Custom\FlexibleContainer($formData);
        }

        $signUpFormView = new \Zend\View\Model\ViewModel();
        $signUpFormView->setTemplate('blocks/' . $this->blockViewName . '/sign-up-form.phtml');
        $signUpFormView->setVariables(
            array(
                'userLanguage' => $this->userLanguage,
                'user' => $this->user,
                'designMode' => $this->designMode,
                'blockData' => $this->blockData,
                'hasOwnSettings' => $this->hasOwnSettings,
                'subscriptionId' => $subscriptionId,
                'eventId' => $eventId,
                'formData' => $formData
            )
        );
        $signUpFormViewHtml = $this->phpRenderer->render($signUpFormView);

        if ($return)
        {
            return $signUpFormViewHtml;
        }
        else
        {
            echo $signUpFormViewHtml;
        }
    }

    /**
     * Getters.
     */

    public function getAdditionalDataTypeById($typeId)
    {
        $typeId = (int)$typeId;

        if ($typeId <= 0)
        {
            return false;
        }

        $contentId = $this->getContentId();

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_EVENT_LIST_ADDITIONAL_DATA_TYPES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'type_id' => (int)$typeId,
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getAdditionalDataTypeByName($additionalDataName)
    {
        $additionalDataName = trim((string)$additionalDataName);

        if ($additionalDataName == '')
        {
            return false;
        }

        $contentId = $this->getContentId();

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_EVENT_LIST_ADDITIONAL_DATA_TYPES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $contentId,
                    'name' => $additionalDataName
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getAdditionalDataByEventId($eventId)
    {
        $eventId = (int)$eventId;

        if ($eventId <= 0)
        {
            return false;
        }

        $additionalData = new \Site\Custom\FlexibleContainer();

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $onClause = new \Zend\Db\Sql\Expression(
            'belad.type_id = beladt.type_id AND belad.event_id = ? AND belad.content_id = ?',
            array(
                $eventId,
                $contentId
            )
        );

        $select = $sql->select();
        $select
            ->columns(
                array(
                    'type_id',
                    'name'
                )
            )
            ->from(array('beladt' => self::BLOCK_EVENT_LIST_ADDITIONAL_DATA_TYPES_TABLE_NAME))
            ->join(
                array('belad' => self::BLOCK_EVENT_LIST_ADDITIONAL_DATA_TABLE_NAME),
                $onClause,
                array(
                    'data_id',
                    'last_update_author_id',
                    'last_update_date',
                    'contents'
                ),
                'LEFT OUTER'
            )
            ->where(
                array(
                    'beladt.content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $additionalData = new \Site\Custom\FlexibleContainer($resultSet->toArray());
        }

        return $additionalData;
    }

    public function getAdditionalDataByEventIdAndAdditionalDataId($eventId, $additionalDataId)
    {
        $eventId = (int)$eventId;
        $additionalDataId = trim((string)$additionalDataId);

        if (($eventId <= 0) || ($additionalDataId < 0))
        {
            return false;
        }

        $contentId = $this->getContentId();

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_EVENT_LIST_ADDITIONAL_DATA_TABLE_NAME)
        ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
        ->where(
            array(
                'data_id' => $additionalDataId,
                'event_id' => $eventId,
                'content_id' => $contentId
            )
        );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getAdditionalDataByEventIdAndDataTypeId($eventId, $dataTypeId)
    {
        $eventId = (int)$eventId;
        $dataTypeId = (int)$dataTypeId;

        if (($eventId <= 0) || ($dataTypeId < 0))
        {
            return false;
        }

        $contentId = $this->getContentId();

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(array('elad' => self::BLOCK_EVENT_LIST_ADDITIONAL_DATA_TABLE_NAME))
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->join(
                array('eladt' => self::BLOCK_EVENT_LIST_ADDITIONAL_DATA_TYPES_TABLE_NAME),
                'eladt.type_id = elad.type_id',
                array(), $select::JOIN_LEFT
            )
            ->where(
                array(
                    'eladt.type_id' => $dataTypeId,
                    'eladt.content_id' => $contentId,
                    'elad.event_id' => $eventId,
                    'elad.content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getAdditionalDataByEventIdAndDataTypeName($eventId, $dataTypeName)
    {
        $eventId = (int)$eventId;
        $dataTypeName = trim((string)$dataTypeName);

        if (($eventId <= 0) || ($dataTypeName == ''))
        {
            return false;
        }

        $contentId = $this->getContentId();

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(array('elad' => self::BLOCK_EVENT_LIST_ADDITIONAL_DATA_TABLE_NAME))
        ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
        ->join(
            array('eladt' => self::BLOCK_EVENT_LIST_ADDITIONAL_DATA_TYPES_TABLE_NAME),
            'eladt.type_id = elad.type_id',
            array(), $select::JOIN_LEFT
        )
        ->where(
            array(
                'eladt.content_id' => $contentId,
                'eladt.name' => $dataTypeName,
                'elad.event_id' => $eventId,
                'elad.content_id' => $contentId
            )
        );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getAllEvents()
    {
        $contentId = $this->getContentId();

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_EVENT_LIST_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getCategories()
    {
        $contentId = $this->getContentId();

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_EVENT_LIST_CATEGORIES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getCategoryById($categoryId)
    {
        $categoryId = (int)$categoryId;

        if ($categoryId <= 0)
        {
            return false;
        }

        $contentId = $this->getContentId();

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_EVENT_LIST_CATEGORIES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'category_id' => (int)$categoryId,
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getCategoryCount()
    {
        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_EVENT_LIST_CATEGORIES_TABLE_NAME)
            ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')), $prefixColumnsWithTable)
            ->where(array('content_id' => $this->getContentId()));
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();
            $count = $row ? $row->count : false;

            return $count;
        }

        return false;
    }

    public function getDefaultNewsletterData()
    {
        $config = array(
            'adapter' => 'Zend\Http\Client\Adapter\Curl',
            'curloptions' => array(CURLOPT_FOLLOWLOCATION => true)
        );
        $url = 'http://www.vistula.edu.pl/newsletter/index.php?option=com_acymailing&ctrl=sub&task=nslist&ajax=1';
        $client = new \Zend\Http\Client($url, $config);
        $response = $client->send();
        $json = $response->getBody();
        $newslettersData = \Zend\Json\Json::decode($json, \Zend\Json\Json::TYPE_OBJECT);
        if (!$newslettersData)
        {
            return false;
        }

        $defaultNewsletterData = false;
        foreach ($newslettersData as $index => $data)
        {
            if ($data->name == 'Grupa Uczelni Vistula')
            {
                $defaultNewsletterData = $data;

                break;
            }
        }

        return $defaultNewsletterData;
    }

    public function getEventById($eventId, $specificColumns = false)
    {
        $eventId = (int)$eventId;
        $specificColumns = $specificColumns ? (array)$specificColumns : array();

        if ($eventId <= 0)
        {
            return false;
        }

        $columns = $specificColumns ? $specificColumns : array(\Zend\Db\Sql\Select::SQL_STAR);
        $contentId = $this->getContentId();

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_EVENT_LIST_TABLE_NAME)
            ->columns($columns, $prefixColumnsWithTable)
            ->where(
                array(
                    'event_id' => $eventId,
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getEventByEventCleanUrlAndContentId($eventCleanUrl, $contentId)
    {
        $eventCleanUrl = trim((string)$eventCleanUrl);
        $contentId = (int)$contentId;

        if (($eventCleanUrl == '') || ($contentId < 0))
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_EVENT_LIST_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $contentId,
                    'clean_url' => $eventCleanUrl
                )
            );

        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return  $resultSet->current();
        }

        return false;
    }

    public function getEventCleanUrlAndContentIdFromEventCleanUrlWithContentId($eventCleanUrlWithContentId)
    {
        $parts = explode('-', $eventCleanUrlWithContentId);

        $contentId = array_pop($parts);
        $eventCleanUrl = join('-', $parts);

        return array($eventCleanUrl, $contentId);
    }

    public function getEventCleanUrlAndContentIdFromRequest(Request $request)
    {
        $parameters = $this->getParametersForCurrentPageFromRequest($request);
        $eventCleanUrlWithContentId = $parameters->key(); // Get name of the first parameter.

        return $this->getEventCleanUrlAndContentIdFromEventCleanUrlWithContentId($eventCleanUrlWithContentId);
    }

    public function getEventContactPersonData($eventId)
    {
        $contactPersonData = new \Site\Custom\FlexibleContainer();
        $contactPersonData->email = '';
        $contactPersonData->phone = '';

        $event = $this->getEventById($eventId, array('contact_person_email', 'contact_person_phone'));
        if ($event)
        {
            $contactPersonData->email = $event->contact_person_email;
            $contactPersonData->phone = $event->contact_person_phone;
        }

        $settings = $this->getSettings();

        if ($contactPersonData->email == '')
        {
            $contactPersonData->email = (string)$settings->contactPersonEmail;
        }

        if ($contactPersonData->phone == '')
        {
            $contactPersonData->phone = (string)$settings->contactPersonPhone;
        }

        return $contactPersonData;
    }

    public function getEventImageByEventIdAndImageId($eventId, $imageId)
    {
        $eventId = (int)$eventId;
        $imageId = (int)$imageId;

        if (($eventId <= 0) || ($imageId <= 0))
        {
            return false;
        }

        $contentId = $this->getContentId();

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_EVENT_LIST_EVENT_IMAGES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'image_id' => $imageId,
                    'event_id' => $eventId,
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getEventImagesByEventId($eventId, $excludeMainImage = false)
    {
        $eventId = (int)$eventId;

        if ($eventId <= 0)
        {
            return false;
        }

        $contentId = $this->getContentId();

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_EVENT_LIST_EVENT_IMAGES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'event_id' => $eventId,
                    'content_id' => $contentId
                )
            )
            ->order('order ASC');

        if ($excludeMainImage)
        {
            $select->where->equalTo('is_main', 0);
        }

        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet) // TODO: Use Hydrator instead of this whole section here.
        {
            $attachmentManager = $this->getAttachmentManager();
            $resultSet = $resultSet->buffer();

            foreach ($resultSet as $image)
            {
                $imageUrl = '';

                $attachment = $attachmentManager->getAttachmentById($image->attachment_id);
                if ($attachment)
                {
                    $imageUrl =
                        '/eng/get-file/index,' .
                        $attachment->hash_for_file_name .
                        ',' .
                        $attachment->original_file_name;
                }

                $image->imageUrl = $imageUrl;
            }
        }

        return $resultSet;
    }

    public function getEventMainImageByEventId($eventId)
    {
        $eventId = (int)$eventId;

        if ($eventId <= 0)
        {
            return false;
        }

        $contentId = $this->getContentId();

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_EVENT_LIST_EVENT_IMAGES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'event_id' => $eventId,
                    'content_id' => $contentId,
                    'is_main' => 1
                )
            )
            ->limit(1);
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getEventUserSubscriptionDataByEventIdAndUserEmail($eventId, $email)
    {
        $eventId = (int)$eventId;
        $email = trim((string)$email);

        $validator = new \Zend\Validator\EmailAddress();

        if (($eventId <= 0) || !$validator->isValid($email))
        {
            return false;
        }

        $contentId = $this->getContentId();

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_EVENT_LIST_USER_SUBSCRIPTIONS_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'event_id' => $eventId,
                    'content_id' => $contentId,
                    'email' => $email
                )
            )
            ->limit(1);
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getEventUserSubscriptionDataBySubscriptionIdAndEventId($subscriptionId, $eventId)
    {
        $subscriptionId = (int)$subscriptionId;
        $eventId = (int)$eventId;

        if (($subscriptionId <= 0) || ($eventId <= 0))
        {
            return false;
        }

        $contentId = $this->getContentId();

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_EVENT_LIST_USER_SUBSCRIPTIONS_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'subscription_id' => $subscriptionId,
                    'event_id' => $eventId,
                    'content_id' => $contentId
                )
            )
            ->limit(1);
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function eventAdditionalDataTypeNameExists($dataTypeName)
    {
        $contentId = $this->getContentId();
        $dataTypeName = trim((string)$dataTypeName);

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_EVENT_LIST_ADDITIONAL_DATA_TYPES_TABLE_NAME)
            ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $contentId,
                    'name' => $dataTypeName
                )
            )
            ->limit(1);
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();
            if ($row)
            {
                return ($row->count > 0);
            }
        }

        return false;
    }

    /**
     * Data formatting functions.
     */

    public function generateCleanUrlFromEventTitle($eventTitle)
    {
        return $this->generateCleanUrlFromText($eventTitle);
    }

    public function formatEventDataAndReturnIfAreValid($eventData, $asGuest = false)
    {
        $settings = $this->getSettings();
        $loggedAsModerator = $this->user && ($this->user->account->role == 'moderator');
        $loggedAsAdmin = $this->user && ($this->user->account->role == 'admin');

        $eventData = $this->convertToFlexibleContainer($eventData);
        $eventDataCopy = clone $eventData;

        $eventDataCopy->eventId = (int)$eventData->eventId ? : null;
        $eventDataCopy->eventCategoryId = (int)$eventData->eventCategoryId;
        $eventDataCopy->eventIsPublished = (int)$eventDataCopy->eventIsPublished ? 1 : 0;
        $eventDataCopy->eventIsPromoted = (int)$eventDataCopy->eventIsPromoted ? 1 : 0;
        $eventDataCopy->eventIsApproved = (int)$eventDataCopy->eventIsApproved ? 1 : 0;
        $eventDataCopy->eventIsSigningUpEnabled = (int)$eventDataCopy->eventIsSigningUpEnabled ? 1 : 0;
        $eventDataCopy->eventIsBeingApprovedNow = (int)$eventDataCopy->eventIsBeingApprovedNow ? 1 : 0;
        $eventDataCopy->eventAddWithoutNecessaryToApprove =
            (int)$eventDataCopy->eventAddWithoutNecessaryToApprove ? 1 : 0;
        $eventDataCopy->eventCreationDate = '';
        $eventDataCopy->eventApprovalDate = null;
        $eventDataCopy->eventDateStart = trim((string)$eventData->eventDateStart);
        $eventDataCopy->eventDateEnd = trim((string)$eventData->eventDateEnd);
        $eventDataCopy->eventAuthorEmail = trim((string)$eventData->eventAuthorEmail);
        $eventDataCopy->eventCleanUrl = trim((string)$eventData->eventCleanUrl);
        $eventDataCopy->eventTitle = trim((string)$eventData->eventTitle);
        $eventDataCopy->eventLabel = trim((string)$eventData->eventLabel);
        $eventDataCopy->eventTeaser = trim((string)$eventData->eventTeaser);
        $eventDataCopy->eventContents = trim((string)$eventData->eventContents);

        if (
            ($asGuest && ($loggedAsModerator || $loggedAsAdmin)) ||
            ($asGuest && $settings->allowAddingImagesByGuests) ||
            !$asGuest
        )
        {
            $eventDataCopy->eventImages = $eventData->eventImages;
            if ($eventDataCopy->eventImages instanceof \Site\Custom\FlexibleContainer)
            {
                $eventDataCopy->eventImages = $eventDataCopy->eventImages->toArray(true);
            }
        }

        $eventDataCopy->eventImagesOrder = $eventData->eventImagesOrder;
        $eventDataCopy->eventAdditionalData = $eventData->eventAdditionalData;
        if ($eventDataCopy->eventAdditionalData instanceof \Site\Custom\FlexibleContainer)
        {
            $eventDataCopy->eventAdditionalData = $eventDataCopy->eventAdditionalData->toArray(true);
        }
        $eventDataCopy->eventAuthorEmail = trim((string)$eventData->eventAuthorEmail);
        $eventDataCopy->eventAuthorAdditionalContactData = trim((string)$eventData->eventAuthorAdditionalContactData);
        $eventDataCopy->eventAttentions = trim((string)$eventData->eventAttentions);
        $eventDataCopy->eventContactPersonEmail = trim((string)$eventData->eventContactPersonEmail);
        $eventDataCopy->eventContactPersonPhone = trim((string)$eventData->eventContactPersonPhone);

        if ($asGuest && ($eventDataCopy->eventAuthorEmail == ''))
        {
            return false;
        }

        /**
         * Temporary data - from add-form. They will be parsed and deleted.
         */

        $eventDataCopy->eventDayRange = trim((string)$eventData->eventDayRange);
        $eventDataCopy->eventStartHour = trim((string)$eventData->eventStartHour);
        $eventDataCopy->eventEndHour = trim((string)$eventData->eventEndHour);
        $eventDataCopy->eventUniqueHashForFileName = trim((string)$eventData->eventUniqueHashForFileName);

        /**
         * Generate event clean URL if was not given.
         */

        if ($eventDataCopy->eventCleanUrl == '')
        {
            $eventDataCopy->eventCleanUrl = $this->generateCleanUrlFromEventTitle($eventDataCopy->eventTitle);
        }

        /**
         * Check if event require approval.
         */

        if ($eventDataCopy->eventAddWithoutNecessaryToApprove || $eventDataCopy->eventIsBeingApprovedNow)
        {
            $eventDataCopy->eventIsApproved = 1;
            $eventDataCopy->eventApprovalDate = new \Zend\Db\Sql\Expression('NOW()');
        }
        else
        {
            if ($asGuest)
            {
                if (
                    ($this->user->user_id <= 0) ||
                    !$this->user->account ||
                    (
                        ($this->user->account->role != 'moderator') &&
                        ($this->user->account->role != 'admin')
                    )
                )
                {
                    // There is no information about user account or he is not a moderator nor admin, so approval is
                    // is necessary.

                    $eventDataCopy->eventIsApproved = 0;
                    $eventDataCopy->eventIsBeingApprovedNow = false;
                }
            }

            // If event was not yet approved:
            /*if (($eventDataCopy->eventIsApproved == 0) && ($eventDataCopy->eventApprovalDate == null))
            {
                $eventDataCopy->eventApprovalDate = new \Zend\Db\Sql\Expression('NOW()');
                $eventDataCopy->eventIsBeingApprovedNow = true;
            }*/
        }

        /**
         * Validating form data.
         */

        if (!preg_match('/^20([0-9]{2})\-[0-1]{1}[0-9]{1}\-[0-3]{1}[0-9]{1} \- 20([0-9]{2})\-[0-1]{1}[0-9]{1}\-[0-3]{1}[0-9]{1}$/i', $eventDataCopy->eventDayRange) ||
            !preg_match('/^([0-9]{1})[0-9]{1}\:([0-5]{1})[0-9]{1}$/i', $eventDataCopy->eventStartHour) ||
            !preg_match('/^([0-9]{1})[0-9]{1}\:([0-5]{1})[0-9]{1}$/i', $eventDataCopy->eventEndHour) ||
            !preg_match('/^([a-z0-9]{1,})([a-z0-9\-]+)$/i', $eventDataCopy->eventCleanUrl)
        )
        {
            return false;
        }

        /**
         * Identifying the author.
         */

        $eventDataCopy->eventLastUpdateAuthorId = $this->user->user_id;

        /**
         * Parsing dates.
         */

        $eventDataCopy->eventDateStart = str_replace(
                '-',
                ':',
                mb_substr($eventDataCopy->eventDayRange, 0, 10)
            ) . ' ' . $eventDataCopy->eventStartHour . ':00';
        $eventDataCopy->eventDateEnd = str_replace(
                '-',
                ':',
                mb_substr($eventDataCopy->eventDayRange, 13, 10)
            ) . ' ' . $eventDataCopy->eventEndHour . ':00';

        /**
         * Parsing unique hash for file name.
         */

        if ($eventDataCopy->eventImages)
        {
            $attachmentManager = $this->getAttachmentManager();
            //$eventImageNumber = 0;
            $eventImages = array();
            foreach ($eventDataCopy->eventImages as $eventImageNumber => $eventImage)
            {
                if ($eventImage['uniqueHashForFileName'] != '')
                {
                    // Validate unique hash for file name:
                    if (!preg_match('/^([a-z0-9]{40})$/i', $eventImage['uniqueHashForFileName']))
                    {
                        return false;
                    }

                    $attachment = $attachmentManager->getAttachmentByUniqueHashForFileName(
                        $eventImage['uniqueHashForFileName']
                    );
                    if (!$attachment)
                    {
                        return false;
                    }

                    $eventImages[$eventImageNumber] = array(
                        'imageId' => $eventImage['imageId'],
                        'imageIsMain' => isset($eventImage['isMain']) ? (int)$eventImage['isMain'] : 0,
                        'imageUniqueHashForFileName' => $eventImage['uniqueHashForFileName'],
                        'imageAttachmentId' => $attachment->attachment_id
                    );
                }

                //$eventImageNumber++;
            }

            $eventDataCopy->eventImages = $eventImages;
        }

        /**
         * Parsing additional data.
         */

        if ($eventDataCopy->eventAdditionalData)
        {
            $eventAdditionalData = array();
            foreach ($eventDataCopy->eventAdditionalData as $additionalDataTypeId => $additionalDataContents)
            {
                $eventAdditionalData[$additionalDataTypeId] = trim($additionalDataContents);
            }

            $eventDataCopy->eventAdditionalData = $eventAdditionalData;
        }

        if ($asGuest)
        {
            $eventDataCopy->eventContents = trim(strip_tags($eventDataCopy->eventContents));
        }

        /**
         * Removing unnecessary data.
         */

        unset($eventDataCopy->eventDayRange);
        unset($eventDataCopy->eventStartHour);
        unset($eventDataCopy->eventEndHour);
        unset($eventDataCopy->hour);
        unset($eventDataCopy->minute);

        if (
            (($eventDataCopy->eventId !== null) && ($eventDataCopy->eventId <= 0)) ||
            ($eventDataCopy->eventCategoryId <= 0) ||
            ($eventDataCopy->eventCleanUrl == '') ||
            ($eventDataCopy->eventTitle == '') ||
            ($eventDataCopy->eventTeaser == '') ||
            ($eventDataCopy->eventContents == '')
        )
        {
            return false;
        }

        return $eventDataCopy;
    }
    
    public function formatEventImageDataAndReturnIfAreValid($imageData)
    {
        $imageData = $this->convertToFlexibleContainer($imageData);
        $imageDataCopy = clone $imageData;

        $imageDataCopy->imageId = (int)$imageData->imageId ? : null;
        $imageDataCopy->imageIsMain = (int)$imageDataCopy->imageIsMain ? 1 : 0;
        $imageDataCopy->imageAttachmentId = (int)$imageDataCopy->imageUniqueHashForFileName ? : null;

        /**
         * Temporary data - from add-form. They will be parsed and deleted.
         */

        $imageDataCopy->imageUniqueHashForFileName = trim((string)$imageDataCopy->imageUniqueHashForFileName);

        /**
         * Identifying the author.
         */

        $imageDataCopy->imageLastUpdateAuthorId = $this->user->user_id;

        /**
         * Parsing unique hash for file name.
         */

        if ($imageDataCopy->imageUniqueHashForFileName != '')
        {
            // Validate unique hash for file name:
            if (!preg_match('/^([a-z0-9]{40})$/i', $imageDataCopy->imageUniqueHashForFileName))
            {
                return false;
            }

            $attachment = $this->getAttachmentManager()->getAttachmentByUniqueHashForFileName(
                $imageDataCopy->imageUniqueHashForFileName
            );
            if (!$attachment)
            {
                return false;
            }

            $imageDataCopy->imageAttachmentId = $attachment->attachment_id;
        }

        /**
         * Removing unnecessary data.
         */

        unset($imageDataCopy->imageUniqueHashForFileName);

        if (($imageDataCopy->imageId !== null) && ($imageDataCopy->imageId <= 0))
        {
            return false;
        }

        return $imageDataCopy;
    }

    public function formatSignUpFormData($formData)
    {
        $formData = $this->convertToFlexibleContainer($formData);
        $formDataCopy = clone $formData;

        $formDataCopy->signUpSubscriptionId = (int)$formData->signUpSubscriptionId ? : null;
        $formDataCopy->signUpEventId = (int)$formData->signUpEventId ? : null;
        $formDataCopy->signUpAcademicTitle = trim($formData->signUpAcademicTitle);
        $formDataCopy->signUpNameAndSurname = trim($formData->signUpNameAndSurname);
        $formDataCopy->signUpGender = ($formData->signUpGender == 'male') ? 0 : 1;
        $formDataCopy->signUpEmail = trim($formData->signUpEmail);
        $formDataCopy->signUpPhone = trim($formData->signUpPhone);
        $formDataCopy->signUpType = ($formData->signUpType == 'private_person') ? 0 : 1;
        $formDataCopy->signUpAttentions = trim($formData->signUpAttentions);
        $formDataCopy->signUpNewsletterAgreement = ($formData->signUpNewsletterAgreement == 1) ? 1 : 0;
        $formDataCopy->signUpDataProcessingAgreement = ($formData->signUpDataProcessingAgreement == 1) ? 1 : 0;
        $formDataCopy->signUpSubscriptionDate = new \Zend\Db\Sql\Expression('NOW()');
        $formDataCopy->signUpInstitutionName = trim($formData->signUpInstitutionName);
        $formDataCopy->signUpInvoiceData = trim($formData->signUpInvoiceData);

        if (($formDataCopy->signUpEventId <= 0) || !$formDataCopy->signUpDataProcessingAgreement)
        {
            return false;
        }

        $validator = new \Zend\Validator\EmailAddress();
        if (!$validator->isValid($formDataCopy->signUpEmail))
        {
            return false;
        }

        if (
            ($formDataCopy->signUpPhone !== '') &&
            !preg_match('/([\+0-9]{9,15})/', str_replace(' ', '', $formDataCopy->signUpPhone))
        )
        {
            return false;
        }

        if (($formDataCopy->signUpType == 1) && ($formDataCopy->signUpInstitutionName == ''))
        {
            return false;
        }

        return $formDataCopy;
    }

    /**
     * Logic-functions.
     */

    public function addDefaultCategoryIfNoCategoryExists()
    {
        $categoryCount = $this->getCategoryCount();
        if ($categoryCount > 0)
        {
            return true;
        }

        if ($categoryCount === false) // It is impossible to get category count because of database error.
        {
            return false;
        }

        $allOperationsWasSuccessful = false;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $translate = $this->getTranslateHelper();

        $insert = $sql->insert(self::BLOCK_EVENT_LIST_CATEGORIES_TABLE_NAME);
        $insert
            ->columns(
                array(
                    'content_id',
                    'is_published',
                    'last_update_author_id',
                    'last_update_date',
                    'name',
                )
            )
            ->values(
                array(
                    'content_id' => $this->getContentId(),
                    'is_published' => 1,
                    'last_update_author_id' => null,
                    'last_update_date' => $lastUpdateDate,
                    'name' => $translate('Default category', 'default', $this->userLanguage->zend2_locale)
                )
            );
        $categoryId = $dbManager->executeInsertAndGetLastInsertId($insert);
        if ($categoryId > 0)
        {
            $settingsSaved = $this->selectCategoryAsDefault($categoryId);
            if ($settingsSaved)
            {
                $allOperationsWasSuccessful = true;
            }
        }

        if ($allOperationsWasSuccessful)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $allOperationsWasSuccessful;
    }

    public function addOrEditAdditionalData($eventId, $additionalDataTypeId, $additionalDataContents)
    {
        $eventId = (int)$eventId;
        $additionalDataTypeId = (int)$additionalDataTypeId;
        $additionalDataContents = trim((string)$additionalDataContents);

        if (($eventId <= 0) || ($additionalDataTypeId < 0))
        {
            return false;
        }

        $additionalDataType = $this->getAdditionalDataTypeById($additionalDataTypeId);
        if (!$additionalDataType)
        {
            // We cannot add contents for data type which does not exist!

            return false;
        }

        $additionalData = $this->getAdditionalDataByEventIdAndDataTypeId($eventId, $additionalDataTypeId);
        if ($additionalData)
        {
            // $result will be "true" or "false":
            $result = $this->updateExistingAdditionalDataByEventIdAdditionalDataId(
                $eventId, $additionalData->data_id, $additionalDataContents
            );
        }
        else
        {
            // $result will have the id of inserted record or "false":
            $result = $this->createNewAdditionalDataAndReturnId(
                $eventId, $additionalDataType->type_id, $additionalDataContents
            );
        }

        return $result;
    }

    public function addOrEditEvent($eventData, $asGuest = false)
    {
        $eventDataCopy = $this->formatEventDataAndReturnIfAreValid($eventData, $asGuest);
        if (!$eventDataCopy)
        {
            return false;
        }

        $eventDataCopy->eventLastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        if ($eventData->eventId > 0)
        {
            return $this->updateExistingEvent($eventDataCopy);
        }
        else
        {
            return $this->createNewEvent($eventDataCopy);
        }
    }

    public function addOrEditEventImage($eventId, $imageData)
    {
        $eventId = (int)$eventId;
        if ($eventId <= 0)
        {
            return false;
        }

        $imageDataCopy = $this->formatEventImageDataAndReturnIfAreValid($imageData);
        if (!$imageDataCopy)
        {
            return false;
        }

        $imageDataCopy->imageLastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        if ($imageDataCopy->imageId > 0)
        {
            return $this->updateExistingEventImage($eventId, $imageDataCopy);
        }
        else
        {
            return $this->createNewEventImage($eventId, $imageDataCopy);
        }
    }

    public function addUserToDefaultNewsletter($userData)
    {
        $userDataAsObject = $this->convertToFlexibleContainer($userData);
        if (
            ($userDataAsObject->name == '') || ($userDataAsObject->email == '')
        )
        {
            return false;
        }

        // Create a clean array, because $userData should contain only the variables supported
        // by the current AcyMailing configuration:
        $userData = array(
            'name' => $userDataAsObject->name,
            'email' => $userDataAsObject->email,
            'attentions' => $userDataAsObject->attentions
        );

        // Get data of the default newsletter:
        $defaultNewsletterData = $this->getDefaultNewsletterData();
        if (!$defaultNewsletterData)
        {
            return false;
        }

        $url = 'http://www.vistula.edu.pl/newsletter/index.php';
        $request = new \Zend\Http\Request();
        $request->getHeaders()->addHeaders([
            'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'
        ]);
        $request->setUri($url);
        $request->setMethod($request::METHOD_POST);
        $request->getPost()->set('option', 'com_acymailing');
        $request->getPost()->set('ctrl', 'sub');
        $request->getPost()->set('task', 'optin');
        $request->getPost()->set('hiddenlists', $defaultNewsletterData->listid);
        $request->getPost()->set('user', $userData);
        $request->getPost()->set('ajax', 1);

        $client = new \Zend\Http\Client();
        $response = $client->dispatch($request);;
        $json = $response->getBody();
        $responseObject = \Zend\Json\Json::decode($json, \Zend\Json\Json::TYPE_OBJECT);
        if ($responseObject->code == 3) // Success.
        {
            return true;
        }

        return false;
    }

    public function createAdditionalDataTypeAndReturnId($name, $shouldBePublished = false)
    {
        $contentId = $this->getContentId();
        $isPublished = $shouldBePublished ? 1 : 0;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');
        $lastUpdateAuthorId = null;
        $name = trim((string)$name);

        if (!$this->eventAdditionalDataTypeNameExists($name))
        {
            $dbManager = $this->getDatabaseManager();
            $sql = $dbManager->getSql();

            $dbManager->beginTransaction();

            $insert = $sql->insert(self::BLOCK_EVENT_LIST_ADDITIONAL_DATA_TYPES_TABLE_NAME);
            $insert
                ->columns(
                    array(
                        'content_id',
                        'is_published',
                        'last_update_author_id',
                        'last_update_date',
                        'name'
                    )
                )
                ->values(
                    array(
                        'content_id' => $contentId,
                        'is_published' => $isPublished,
                        'last_update_author_id' => $lastUpdateAuthorId,
                        'last_update_date' => $lastUpdateDate,
                        'name' => $name
                    )
                );
            $typeId = $dbManager->executeInsertAndGetLastInsertId($insert);
            if ($typeId > 0)
            {
                $type = $this->getAdditionalDataTypeById($typeId);
                if (
                    !$type ||
                    ($type->content_id != $contentId) ||
                    ($type->is_published != $isPublished) ||
                    ($type->last_update_author_id != $lastUpdateAuthorId) ||
                    ($type->name != $name)
                )
                {
                    $typeId = false;
                }
            }

            if ($typeId > 0)
            {
                $dbManager->commit();
            }
            else
            {
                $dbManager->rollback();
            }

            return $typeId;
        }

        return true;
    }

    public function createDefaultAdditionalDataTypes()
    {
        $shouldBePublished = true;

        $translate = $this->getTranslateHelper();

        $dataTypes = array(
            $translate('Where'),
            $translate('Introduction'),
            $translate('Description'),
            $translate('Contact'),
            $translate('Additional information')
        );

        foreach ($dataTypes as $name)
        {
            $typeId = $this->createAdditionalDataTypeAndReturnId($name, $shouldBePublished);
            if (!$typeId)
            {
                $this->deleteAllAdditionalDataTypes();

                return false;
            }
        }

        return true;
    }

    public function createNewOrUpdateExistingEvent($formData)
    {
        $eventId = isset($formData['eventId']) ? (int)$formData['eventId'] : null;

        if ($eventId > 0)
        {
            return $this->updateExistingEvent($formData);
        }
        else
        {
            return $this->createNewEvent($formData);
        }
    }

    public function decideWhatToDoWithRecentlyUploadedAndMovedFiles(\Zend\Http\Request $request, $movedFiles)
    {
        //$postData = $request->getPost();

        // Inform the UploadController and UploadManager than upload was successful and you accept these files:
        return true;
    }

    public function deleteAllAdditionalDataTypes()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_EVENT_LIST_ADDITIONAL_DATA_TYPES_TABLE_NAME);
        $delete->where(array('content_id' => $this->getContentId()));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;
    }

    public function deleteAllAdditionalData()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_EVENT_LIST_ADDITIONAL_DATA_TABLE_NAME);
        $delete->where(array('content_id' => $this->getContentId()));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;
    }

    public function deleteAllCategories()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_EVENT_LIST_CATEGORIES_TABLE_NAME);
        $delete->where(array('content_id' => $this->getContentId()));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;
    }

    public function deleteAllImages()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_EVENT_LIST_EVENT_IMAGES_TABLE_NAME);
        $delete->where(array('content_id' => $this->getContentId()));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;
    }

    public function deleteAllEvents()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_EVENT_LIST_TABLE_NAME);
        $delete->where(array('content_id' => $this->getContentId()));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;
    }

    public function deleteCategoryById($categoryId)
    {
        $categoryId = (int)$categoryId;

        if ($categoryId <= 0)
        {
            return false;
        }

        $deleted = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_EVENT_LIST_CATEGORIES_TABLE_NAME);
        $delete
            ->where(
                array(
                    'category_id' => $categoryId,
                    'content_id' => $contentId
                )
            );
        $result = $dbManager->executePreparedStatement($delete);
        if ($result)
        {
            $deleted = true;
        }

        return $deleted;
    }

    public function deleteEventById($eventId)
    {
        $eventId = (int)$eventId;

        if ($eventId <= 0)
        {
            return false;
        }

        $deleted = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_EVENT_LIST_TABLE_NAME);
        $delete
            ->where(
                array(
                    'event_id' => $eventId,
                    'content_id' => $contentId
                )
            );
        $result = $dbManager->executePreparedStatement($delete);
        if ($result)
        {
            $deleted = true;
        }

        return $deleted;
    }

    public function deleteEventImageByEventIdAndImageId($eventId, $imageId)
    {
        $eventId = (int)$eventId;
        $imageId = (int)$imageId;

        if (($eventId <= 0) || ($imageId <= 0))
        {
            return false;
        }

        $deleted = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_EVENT_LIST_EVENT_IMAGES_TABLE_NAME);
        $delete
            ->where(
                array(
                    'image_id' => $imageId,
                    'event_id' => $eventId,
                    'content_id' => $contentId
                )
            );
        $result = $dbManager->executePreparedStatement($delete);
        if ($result)
        {
            $deleted = true;
        }

        return $deleted;
    }

    public function deleteEventImagesFromOutsideTheImageIdSet($eventId, $imageIds)
    {
        $eventId = (int)$eventId;
        $imageIds = $this->convertToFlexibleContainer($imageIds);

        if ($eventId <= 0)
        {
            return false;
        }

        $deleted = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_EVENT_LIST_EVENT_IMAGES_TABLE_NAME);
        $delete
            ->where(
                array(
                    'event_id' => $eventId,
                    'content_id' => $contentId
                )
            );

        if ($imageIds->count())
        {
            $delete->where->addPredicate(
                new \Zend\Db\Sql\Predicate\NotIn('image_id', $imageIds->toArray())
            );
        }

        $result = $dbManager->executePreparedStatement($delete);
        if ($result)
        {
            $deleted = true;
        }

        return $deleted;
    }

    public function deleteUserSignedUpForEvent($subscriptionId, $eventId)
    {
        $subscriptionId = (int)$subscriptionId;
        $eventId = (int)$eventId;

        if (($subscriptionId <= 0) || ($eventId <= 0))
        {
            return false;
        }

        $deleted = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_EVENT_LIST_USER_SUBSCRIPTIONS_TABLE_NAME);
        $delete
            ->where(
                array(
                    'subscription_id' => $subscriptionId,
                    'event_id' => $eventId,
                    'content_id' => $contentId
                )
            );
        $result = $dbManager->executePreparedStatement($delete);
        if ($result)
        {
            $deleted = true;
        }

        return $deleted;
    }

    public function exportListOfUsersSignedUpForEventAs($eventId, $format = 'csv', $includeHeaders = true)
    {
        $eventId = (int)$eventId;
        if ($eventId <= 0)
        {
            return false;
        }

        $contentId = $this->getContentId();
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $prefixColumnsWithTable = false;

        $select = $sql->select();
        $select
            ->from(self::BLOCK_EVENT_LIST_USER_SUBSCRIPTIONS_TABLE_NAME)
            ->columns(
                array(
                    'academic_title',
                    'name_and_surname',
                    'gender',
                    'type_of_request',
                    'institution_name',
                    'invoice_data',
                    'email',
                    'phone',
                    'attentions',
                    'newsletter_agreement',
                    'subscription_date'
                ), $prefixColumnsWithTable
            )
            ->where(
                array(
                    'event_id' => $eventId,
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getSpecificHydratedResultSet(
            $select, new \Zend\Stdlib\Hydrator\ArraySerializable(), new \ArrayObject
        );
        if ($resultSet)
        {
            $translate = $this->getTranslateHelper();
            $users = $resultSet->toArray();

            require_once('vendor/phpoffice/phpexcel/Classes/PHPExcel.php');
            $objPHPExcel = new \PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);
            $worksheet = $objPHPExcel->getActiveSheet();

            $rowIndex = 1;

            if ($includeHeaders)
            {
                $worksheet
                    ->setCellValue('A1', $translate('Academic title', 'default', $this->userLanguage->zend2_locale))
                    ->setCellValue('B1', $translate('Name and surname', 'default', $this->userLanguage->zend2_locale))
                    ->setCellValue('C1', $translate('Gender', 'default', $this->userLanguage->zend2_locale))
                    ->setCellValue('D1', $translate('Type of request', 'default', $this->userLanguage->zend2_locale))
                    ->setCellValue('E1', $translate('Institution', 'default', $this->userLanguage->zend2_locale))
                    ->setCellValue('F1', $translate('Invoice data', 'default', $this->userLanguage->zend2_locale))
                    ->setCellValue('G1', $translate('E-Mail', 'default', $this->userLanguage->zend2_locale))
                    ->setCellValue('H1', $translate('Phone', 'default', $this->userLanguage->zend2_locale))
                    ->setCellValue('I1', $translate('Attentions', 'default', $this->userLanguage->zend2_locale))
                    ->setCellValue('J1', $translate('Newsletter', 'default', $this->userLanguage->zend2_locale))
                    ->setCellValue('K1', $translate('Subscription date', 'default', $this->userLanguage->zend2_locale))
                ;
                $rowIndex++;
            }

            foreach ($users as $columns)
            {
                $gender = ($columns['gender']) == 0 ? 'Male' : 'Female';
                $typeOfRequest = ($columns['type_of_request']) == 0 ? 'Private person' : 'Institution';
                $newsletterAgreement = ($columns['newsletter_agreement']) == 0 ? 'No' : 'Yes';

                $columns['gender'] = $translate(
                    $gender,
                    'default',
                    $this->userLanguage->zend2_locale
                );
                $columns['type_of_request'] = $translate(
                    $typeOfRequest,
                    'default',
                    $this->userLanguage->zend2_locale
                );
                $columns['newsletter_agreement'] = $newsletterAgreement;

                $columnIndex = 0;
                foreach ($columns as $data)
                {
                    $worksheet->setCellValueByColumnAndRow($columnIndex, $rowIndex, $data);
                    $columnIndex++;
                }

                $rowIndex++;
            }

            $format = mb_strtolower(trim($format));
            $fileName = date('Y-m-d_H-m-s_') . 'users_' . $eventId . '.' . $format;

            $objPHPExcel->getActiveSheet()->setTitle('Simple');
            $objPHPExcel->setActiveSheetIndex(0);
            if ($format == 'csv')
            {
                $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
                $objWriter->setDelimiter("\t");
            }
            else
            {
                $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            }
            $objWriter->save('data/temp/' . $fileName);

            return $fileName;
        }

        return false;
    }

    public function reorderEventImages($eventId, $images)
    {
        $eventId = (int)$eventId;
        $images = (array)$images;

        if (($eventId <= 0) || !$images)
        {
            return false;
        }

        $reordered = true;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $order = 0;
        $contentId = $this->getContentId();

        $dbManager->beginTransaction();

        foreach ($images as $imageId)
        {
            $imageId = (int)$imageId;

            if ($imageId > 0)
            {
                $update = $sql->update(self::BLOCK_EVENT_LIST_EVENT_IMAGES_TABLE_NAME);
                $update
                    ->set(array('order' => $order))
                    ->where(
                        array(
                            'image_id' => $imageId,
                            'event_id' => $eventId,
                            'content_id' => $contentId
                        )
                    );
                $result = $dbManager->executePreparedStatement($update);
                if (!$result)
                {
                    $reordered = false;
                    break;
                }

                $image = $this->getEventImageByEventIdAndImageId($eventId, $imageId);
                if ($image->order != $order)
                {
                    $reordered = false;
                    break;
                }

                $order++;
            }
        }

        if ($reordered)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $reordered;
    }

    public function selectCategoryAsDefault($categoryId)
    {
        $pageBlockId = $this->blockData->page_block_id;
        $categoryId = (int)$categoryId;

        if ($categoryId <= 0)
        {
            return false;
        }

        $blockManager = $this->getBlockManager();
        $settings = $this->getSettings();
        $settings['categoryId'] = $categoryId;
        $settingsSaved = $blockManager->savePageBlockSettings($pageBlockId, $settings);

        return $settingsSaved;
    }

    public function setDefaultDetailsPageIfNoOneWasSetYet()
    {
        $settings = $this->getSettings();

        if ($settings->detailsPageId > 0)
        {
            return true;
        }

        $pageManager = $this->getPageManager();
        $mainPage = $pageManager->getMainPageForSpecificLanguage();
        if (!$mainPage)
        {
            return false;
        }

        $settings->detailsPageId = $mainPage->page_id; // TODO: Should use other page if no main page currently exists.
        $blockManager = $this->getBlockManager();

        return $blockManager->savePageBlockSettings($this->blockData->page_block_id, $settings);
    }

    public function setEventImageAsMainByEventIdAndImageId($eventId, $imageId)
    {
        $eventId = (int)$eventId;
        $imageId = (int)$imageId;

        if (($eventId <= 0) || ($imageId <= 0))
        {
            return false;
        }

        $image = $this->getEventImageByEventIdAndImageId($eventId, $imageId);
        if (!$image)
        {
            return false;
        }

        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_EVENT_LIST_EVENT_IMAGES_TABLE_NAME);
        $update
            ->set(
                array(
                    'is_main' => 1
                )
            )
            ->where(
                array(
                    'event_id' => $eventId,
                    'content_id' => $contentId
                )
            )
            ->where->notEqualTo('image_id', $imageId);

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $update = $sql->update(self::BLOCK_EVENT_LIST_EVENT_IMAGES_TABLE_NAME);
            $update
                ->set(
                    array(
                        'is_main' => 0
                    )
                )
                ->where(
                    array(
                        'image_id' => $imageId,
                        'event_id' => $eventId,
                        'content_id' => $contentId
                    )
                );
            $result = $dbManager->executePreparedStatement($update);
            if ($result)
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function signUpForEvent($formData)
    {
        $formData = $this->formatSignUpFormData($formData);
        if (!$formData)
        {
            return false;
        }

        /**
         * When signUpSubscriptionId is given it means that admin it is now being modified by admin so we should not
         * check if user is already signed up for this event.
         */
        if (!$formData->signUpSubscriptionId)
        {
            $signUpData = $this->getEventUserSubscriptionDataByEventIdAndUserEmail(
                $formData->signUpEventId, $formData->signUpEmail
            );
            if ($signUpData)
            {
                return true; // User was already signed up for this event.
            }
        }

        /**
         * Check if the event exists and its start or end date are greater than current - if not - return false.
         */
        $currentUnixTimestamp = time();
        $event = $this->getEventById($formData->signUpEventId);
        if (!$event || (strtotime($event->event_date_end) <= $currentUnixTimestamp))
        {
            return false;
        }

        $subscriptionSaved = false;
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        if ($formData->signUpSubscriptionId > 0)
        {
            $update = $sql->update(self::BLOCK_EVENT_LIST_USER_SUBSCRIPTIONS_TABLE_NAME);
            $update
                ->set(
                    array(
                        'gender' => $formData->signUpGender,
                        'type_of_request' => $formData->signUpType,
                        'newsletter_agreement' => $formData->signUpNewsletterAgreement,
                        'subscription_date' => $formData->signUpSubscriptionDate,
                        'academic_title' => $formData->signUpAcademicTitle,
                        'name_and_surname' => $formData->signUpNameAndSurname,
                        'email' => $formData->signUpEmail,
                        'phone' => $formData->signUpPhone,
                        'institution_name' => $formData->signUpInstitutionName,
                        'invoice_data' => $formData->signUpInvoiceData,
                        'attentions' => $formData->signUpAttentions
                    )
                )
                ->where(
                    array(
                        'subscription_id' => $formData->signUpSubscriptionId,
                        'content_id' => $contentId,
                        'event_id' => $formData->signUpEventId,
                    )
                );
            $result = $dbManager->executePreparedStatement($update);
            if ($result)
            {
                $subscriptionSaved = true;
            }

            $subscriptionId = $formData->signUpSubscriptionId;
        }
        else
        {
            $insert = $sql->insert(self::BLOCK_EVENT_LIST_USER_SUBSCRIPTIONS_TABLE_NAME);
            $insert
                ->columns(
                    array(
                        'content_id',
                        'event_id',
                        'gender',
                        'type_of_request',
                        'newsletter_agreement',
                        'subscription_date',
                        'academic_title',
                        'name_and_surname',
                        'email',
                        'phone',
                        'institution_name',
                        'invoice_data',
                        'attentions'
                    )
                )
                ->values(
                    array(
                        'content_id' => $contentId,
                        'event_id' => $formData->signUpEventId,
                        'gender' => $formData->signUpGender,
                        'type_of_request' => $formData->signUpType,
                        'newsletter_agreement' => $formData->signUpNewsletterAgreement,
                        'subscription_date' => $formData->signUpSubscriptionDate,
                        'academic_title' => $formData->signUpAcademicTitle,
                        'name_and_surname' => $formData->signUpNameAndSurname,
                        'email' => $formData->signUpEmail,
                        'phone' => $formData->signUpPhone,
                        'institution_name' => $formData->signUpInstitutionName,
                        'invoice_data' => $formData->signUpInvoiceData,
                        'attentions' => $formData->signUpAttentions
                    )
                );
            $subscriptionId = $dbManager->executeInsertAndGetLastInsertId($insert);
            if ($subscriptionId > 0)
            {
                $subscriptionSaved = true;
            }
        }

        if ($subscriptionSaved)
        {
            $subscriptionSaved = false;
            $subscriptionData = $this->getEventUserSubscriptionDataBySubscriptionIdAndEventId(
                $subscriptionId, $formData->signUpEventId
            );
            if (
                $subscriptionData &&
                ($subscriptionData->event_id == $formData->signUpEventId) &&
                ($subscriptionData->gender == $formData->signUpGender) &&
                ($subscriptionData->type_of_request == $formData->signUpType) &&
                ($subscriptionData->academic_title == $formData->signUpAcademicTitle) &&
                ($subscriptionData->name_and_surname == $formData->signUpNameAndSurname) &&
                ($subscriptionData->email == $formData->signUpEmail) &&
                ($subscriptionData->phone == $formData->signUpPhone) &&
                ($subscriptionData->institution_name == $formData->signUpInstitutionName) &&
                ($subscriptionData->invoice_data == $formData->signUpInvoiceData) &&
                ($subscriptionData->attentions == $formData->signUpAttentions)
            )
            {
                $subscriptionSaved = true;
            }
        }

        //echo '$subscriptionSaved: '; print_r($subscriptionSaved); echo ',';

        // Add user to newsletter only if he wants it.
        $addingUserToNewsletterSuccessful = true;
        if ($subscriptionSaved && ($formData->signUpNewsletterAgreement == 1))
        {
            $userData = new \Site\Custom\FlexibleContainer();
            $userData->name = $formData->signUpNameAndSurname;
            $userData->email = $formData->signUpEmail;
            $userData->attentions = $formData->signUpAttentions;

            $addingUserToNewsletterSuccessful = $this->addUserToDefaultNewsletter($userData);
        }

        //echo '$addingUserToNewsletterSuccessful: '; print_r($addingUserToNewsletterSuccessful); echo ',';

        /**
         * When signUpSubscriptionId is given it means that admin it is now being modified by admin so we should not
         * send these e-mails again.
         */
        $mailSentSuccessfully = true;
        if (!$formData->signUpSubscriptionId)
        {
            $mailSentSuccessfully = false;
            if ($subscriptionSaved && $addingUserToNewsletterSuccessful)
            {
                $mailSentSuccessfully = $this->sendMailAboutUserSignedUpForEvent($formData);
            }

            if ($subscriptionSaved && $mailSentSuccessfully)
            {
                $adminEmailAddresses = $this->getSystemSettingValueByName(
                    'emails_to_notify_about_important_actions_in_system'
                );
                $adminEmailAddresses = explode("\n", $adminEmailAddresses);
                if ($adminEmailAddresses) // Send only if at least one email was given.
                {
                    $mailSentSuccessfully = $this->sendMailAboutUserSignedUpForEvent($formData, $adminEmailAddresses);
                }
            }
        }

        //echo '$mailSentSuccessfully: '; print_r($mailSentSuccessfully); echo ',';

        if ($subscriptionSaved && $addingUserToNewsletterSuccessful && $mailSentSuccessfully)
        {
            $dbManager->commit();

            return $subscriptionId;
        }

        $dbManager->rollback();

        return false;
    }

    public function toggleCategoryPublished($categoryId, $published)
    {
        $categoryId = (int)$categoryId;
        $published = (int)$published ? 1 : 0;

        if ($categoryId <= 0)
        {
            return false;
        }

        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $category = $this->getCategoryById($categoryId);
        if ($category)
        {
            $update = $sql->update(self::BLOCK_EVENT_LIST_CATEGORIES_TABLE_NAME);
            $update
                ->set(
                    array(
                        'is_published' => $published
                    )
                )
                ->where(
                    array(
                        'category_id' => $categoryId,
                        'content_id' => $contentId
                    )
                );
            $result = $dbManager->executePreparedStatement($update);
            if ($result)
            {
                $category = $this->getCategoryById($categoryId);
                if ($category && ($category->is_published == $published))
                {
                    $updated = true;
                }
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function toggleEventPublished($eventId, $published)
    {
        $eventId = (int)$eventId;
        $published = (int)$published ? 1 : 0;

        if ($eventId <= 0)
        {
            return false;
        }

        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $event = $this->getEventById($eventId);
        if ($event)
        {
            $update = $sql->update(self::BLOCK_EVENT_LIST_TABLE_NAME);
            $update
                ->set(
                    array(
                        'is_published' => $published
                    )
                )
                ->where(
                    array(
                        'event_id' => $eventId,
                        'content_id' => $contentId
                    )
                );
            $result = $dbManager->executePreparedStatement($update);
            if ($result)
            {
                $event = $this->getEventById($eventId);
                if ($event && ($event->is_published == $published))
                {
                    $updated = true;
                }
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    protected function cleanCsvRowData(&$str)
    {
        $str = preg_replace("/\t/", " ", $str);
        $str = preg_replace("/\r?\n/", "", $str);
        if (strstr($str, '"'))
        {
            $str = '"' . str_replace('"', '""', $str) . '"';
        }
    }

    private function createNewAdditionalDataAndReturnId(
        $eventId, $additionalDataTypeId, $additionalDataContents
    )
    {
        $contentId = $this->getContentId();
        $lastUpdateAuthorId = $this->user->user_id ? (int)$this->user->user_id : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $insert = $sql->insert(self::BLOCK_EVENT_LIST_ADDITIONAL_DATA_TABLE_NAME);
        $insert
            ->columns(
                array(
                    'type_id',
                    'event_id',
                    'content_id',
                    'content_id',
                    'last_update_author_id',
                    'last_update_date',
                    'contents'
                )
            )
            ->values(
                array(
                    'type_id' => $additionalDataTypeId,
                    'event_id' => $eventId,
                    'content_id' => $contentId,
                    'last_update_author_id' => $lastUpdateAuthorId,
                    'last_update_date' => $lastUpdateDate,
                    'contents' => $additionalDataContents
                )
            );
        $additionalDataId = $dbManager->executeInsertAndGetLastInsertId($insert);
        if ($additionalDataId > 0)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $additionalDataId;
    }

    private function createNewEvent($eventData)
    {
        $created = false;

        $contentId = $this->getContentId();
        $eventCreationDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();
        $event = false;

        $columns = array(
            'content_id',
            'category_id',
            'is_published',
            'is_promoted',
            //'is_approved',
            'is_signing_up_enabled',
            //'attachment_id',
            'last_update_author_id',
            'last_update_date',
            'creation_date',
            //'approval_date',
            'event_date_start',
            'event_date_end',
            'author_email',
            'contact_person_email',
            'contact_person_phone',
            'clean_url',
            'title',
            'label',
            'teaser',
            'contents',
            'additional_contact_data',
            'attentions'
        );
        $values = array(
            'content_id' => $contentId,
            'category_id' => $eventData->eventCategoryId,
            'is_published' => $eventData->eventIsPublished,
            'is_promoted' => $eventData->eventIsPromoted,
            //'is_approved' => $eventData->eventIsApproved,
            'is_signing_up_enabled' => $eventData->eventIsSigningUpEnabled,
            //'attachment_id' => $eventData->eventAttachmentId,
            'last_update_author_id' => $eventData->eventLastUpdateAuthorId,
            'last_update_date' => $eventData->eventLastUpdateDate,
            'creation_date' => $eventCreationDate,
            //'approval_date' => $eventData->eventApprovalDate,
            'event_date_start' => $eventData->eventDateStart,
            'event_date_end' => $eventData->eventDateEnd,
            'author_email' => $eventData->eventAuthorEmail,
            'contact_person_email' => $eventData->eventContactPersonEmail,
            'contact_person_phone' => $eventData->eventContactPersonPhone,
            'clean_url' => $eventData->eventCleanUrl,
            'title' => $eventData->eventTitle,
            'label' => $eventData->eventLabel,
            'teaser' => $eventData->eventTeaser,
            'contents' => $eventData->eventContents,
            'additional_contact_data' => $eventData->eventAuthorAdditionalContactData,
            'attentions' => $eventData->eventAttentions
        );

        if ($eventData->eventAddWithoutNecessaryToApprove)
        {
            $columns[] = 'is_approved';
            $values['is_approved'] = 1;
        }

        $insert = $sql->insert(self::BLOCK_EVENT_LIST_TABLE_NAME);
        $insert
            ->columns($columns)
            ->values($values);

        $eventId = $dbManager->executeInsertAndGetLastInsertId($insert);
        if ($eventId > 0)
        {
            $event = $this->getEventById($eventId);
            if ($event &&
                ($event->content_id == $contentId) &&
                ($event->category_id == $eventData->eventCategoryId) &&
                ($event->is_published == $eventData->eventIsPublished) &&
                ($event->is_promoted == $eventData->eventIsPromoted) &&
                //($event->is_approved == $eventData->eventIsApproved) &&
                ($event->is_signing_up_enabled == $eventData->eventIsSigningUpEnabled) &&
                //($event->attachment_id == $eventData->eventAttachmentId) &&
                ($event->last_update_author_id == $eventData->eventLastUpdateAuthorId) &&
                ($event->author_email == $eventData->eventAuthorEmail) &&
                ($event->contact_person_email == $eventData->eventContactPersonEmail) &&
                ($event->contact_person_phone == $eventData->eventContactPersonPhone) &&
                ($event->clean_url == $eventData->eventCleanUrl) &&
                ($event->title == $eventData->eventTitle) &&
                ($event->label == $eventData->eventLabel) &&
                ($event->teaser == $eventData->eventTeaser) &&
                ($event->contents == $eventData->eventContents) &&
                ($event->additional_contact_data == $eventData->eventAuthorAdditionalContactData) &&
                ($event->attentions == $eventData->eventAttentions)
            )
            {
                $created = true;
            }
        }

        if ($created)
        {
            $created = false;
            $eventImagesOk = true;

            // Array with all image hash and id, necessary to set proper images order after they are saved in database:
            $existingImages = array();

            if ($eventData->eventImages)
            {
                $firstImageId = null;
                $atLeastOneImageWasSetAsMain = false;
                foreach ($eventData->eventImages as $imageData)
                {
                    $isMain = $imageData['imageIsMain'];
                    $attachmentId = $imageData['imageAttachmentId'];
                    $uniqueHashForFileName = $imageData['imageUniqueHashForFileName'];

                    if ($attachmentId)
                    {
                        $imageId = (int)$this->addOrEditEventImage($eventId, $imageData);
                        if ($imageId > 0)
                        {
                            if ($firstImageId === null)
                            {
                                $firstImageId = $imageId;
                            }

                            if ($isMain)
                            {
                                $atLeastOneImageWasSetAsMain = true;
                            }

                            $existingImages[$uniqueHashForFileName] = $imageId;
                        }
                        else
                        {
                            $eventImagesOk = false;

                            break;
                        }
                    }
                }

                $reordered = false;
                if ($eventImagesOk)
                {
                    $imagesOrder = array();
                    foreach ($eventData->eventImagesOrder as $uniqueHashForFileName)
                    {
                        $imageId =
                            isset($existingImages[$uniqueHashForFileName])
                                ? (int)$existingImages[$uniqueHashForFileName]
                                : null;
                        if ($imageId > 0)
                        {
                            $imagesOrder[] = $imageId;
                        }
                    }

                    $reordered = $this->reorderEventImages($eventId, $imagesOrder);
                }

                if (!$atLeastOneImageWasSetAsMain && $reordered)
                {
                    if (!$this->setEventImageAsMainByEventIdAndImageId($eventId, $firstImageId))
                    {
                        $eventImagesOk = false;
                    }
                }
            }

            $created = $eventImagesOk;

            if ($created && $eventData->eventAdditionalData)
            {
                $additionalDataOk = true;

                foreach ($eventData->eventAdditionalData as $dataTypeId => $dataContents)
                {
                    $dataUpdated = $this->addOrEditAdditionalData($eventId, $dataTypeId, $dataContents);
                    if (!$dataUpdated)
                    {
                        $additionalDataOk = false;
                        break;
                    }
                }

                $created = $additionalDataOk;
            }
        }

        if ($created)
        {
            $eventCleanUrl = $this->generateCleanUrlFromEventTitle($eventData->eventTitle);
            $elementUrlParametersString =
                $eventCleanUrl . '-' . $contentId . '-' . $eventId;

            /*$this->addEventToSystemHistory(
                'Event (id: ' . $eventId . ') has been created by {{userLogin}} ({{authorEmail}}).', array(
                    'authorEmail' => $eventData->eventAuthorEmail
                )
            );*/

            $this->updateBlockRecordInSearchIndex(
                $eventId, $eventData->eventContents, $elementUrlParametersString, 'Event',
                $eventData->eventDateStart
            );

            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        if ($created && $event && !$eventData->eventIsApproved)
        {
            // Send mail about newly created not approved event to administrators.

            $settings = $this->getSettings();
            $cmsConfig = $this->getCmsConfig();
            $escapeForHtml = $this->getTranslateHelper();

            /*var_dump(
                $eventData->eventAuthorAdditionalContactData, $eventData->eventAttentions, $eventData->eventAuthorEmail,
                $eventData->eventTitle
            );*/

            $eventAuthorAdditionalContactData = $escapeForHtml($eventData->eventAuthorAdditionalContactData);
            $eventAttentions = $escapeForHtml($eventData->eventAttentions);
            $eventAuthorEmail = $escapeForHtml($eventData->eventAuthorEmail);
            $eventTitle = $escapeForHtml($eventData->eventTitle);

            //var_dump($eventAdditionalContactData, $eventAttentions, $eventAuthorEmail, $eventTitle);

            $emails = $this->getSystemSettingValueByName('emails_to_notify_about_important_actions_in_system');
            $mailRecipientAddresses = explode("\n", (string)$emails);
            $mailSenderTitle = $cmsConfig->system_email_sender_title;
            $mailSubject = $this->getSystemSettingValueByName('default_subject_for_mail_about_newly_added_event');
            $mailContents = $this->getSystemSettingValueByName('default_contents_for_mail_about_newly_added_event');

            $mailContents =
                str_replace(
                    array(
                        '{{attentions}}',
                        '{{author_additional_contact_data}}',
                        '{{author_email}}',
                        '{{event_title}}',
                        '{{event_datetime}}',
                        "\n"
                    ),
                    array(
                        $eventAttentions,
                        $eventAuthorAdditionalContactData,
                        '<a href="mailto:' . $eventAuthorEmail . '">' . $eventAuthorEmail . '</a>',
                        $eventTitle,
                        '<em>' . $event->creation_date . '</em>',
                        "<br>\n"
                    ),
                    $mailContents
                );

            //var_dump($mailRecipientAddresses, $mailSenderTitle, $mailSubject, $mailContents);

            // Send only if all neccesarry data are provided:
            if (
                $mailRecipientAddresses &&
                ($mailSenderTitle != '') &&
                ($mailSubject != '') &&
                ($mailContents != '')
            )
            {
                $mail = $this->getMailManager();
                $mail
                    ->setSubject($mailSubject)
                    ->setHtmlContents($mailContents)
                    ->setTextContents(strip_tags($mailContents))
                ;

                foreach ($mailRecipientAddresses as $mailRecipientAddress)
                {
                    $mailRecipientAddress = trim((string)$mailRecipientAddress);

                    if ($mailRecipientAddress != '')
                    {
                        try
                        {
                            $mail
                                ->setRecipientAddress($mailRecipientAddress)
                                ->send()
                            ;
                        }
                        catch (\Exception $e)
                        {
                            //var_dump($e->getMessage());
                        }
                    }
                }
            }
        }

        return $created;
    }

    private function createNewEventImage($eventId, $imageData)
    {
        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $insert = $sql->insert(self::BLOCK_EVENT_LIST_EVENT_IMAGES_TABLE_NAME);
        $insert
            ->columns(
                array(
                    'event_id',
                    'content_id',
                    'attachment_id',
                    'is_main',
                    'last_update_author_id',
                    'last_update_date'
                )
            )
            ->values(
                array(
                    'event_id' => $eventId,
                    'content_id' => $contentId,
                    'attachment_id' => $imageData->imageAttachmentId,
                    'is_main' => $imageData->imageIsMain,
                    'last_update_author_id' => $imageData->imageLastUpdateAuthorId,
                    'last_update_date' => $imageData->imageLastUpdateDate
                )
            );
        $imageId = $dbManager->executeInsertAndGetLastInsertId($insert);
        if ($imageId > 0)
        {
            $dbManager->commit();

            return $imageId;
        }

        $dbManager->rollback();

        return false;
    }

    private function sendMailAboutUserSignedUpForEvent($signUpFormData, $adminEmailAddresses = false)
    {
        $event = $this->getEventById($signUpFormData->signUpEventId);
        if (!$event)
        {
            return false;
        }

        // Determine if this email should be send to user or admin based on email address:
        $mailToAdmin = true;
        $recipientEmailAddresses = $this->convertToFlexibleContainer($adminEmailAddresses);
        if (!$recipientEmailAddresses->count())
        {
            $recipientEmailAddresses[] = $signUpFormData->signUpEmail; // Send to user instead of admin.
            $mailToAdmin = false;
        }

        $settings = $this->getSettings();
        if ($mailToAdmin)
        {
            $mailSubject = $settings->subjectForMailForAdminAboutUserSignedUpForEvent;
            $mailContents = $settings->contentsForMailForAdminAboutUserSignedUpForEvent;
        }
        else
        {
            $mailSubject = $settings->subjectForMailForUserAboutUserSignedUpForEvent;
            $mailContents = $settings->contentsForMailForUserAboutUserSignedUpForEvent;
        }

        $cmsConfig = $this->getCmsConfig();
        $escapeForHtml = $this->getEscapeForHtmlHelper();
        $translate = $this->getTranslateHelper();
        $contentId = $this->getContentId();

        $eventAdditionalDataTypeName = $translate('Where');

        $eventStartDate = date('Y-m-d', strtotime($event->event_date_start));
        $eventStartHour = date('H:m', strtotime($event->event_date_start));
        $eventEndDate = date('Y-m-d', strtotime($event->event_date_end));
        $eventEndHour = date('H:m', strtotime($event->event_date_end));

        $eventAdditionalWhereData = $this->getAdditionalDataByEventIdAndDataTypeName(
            $signUpFormData->signUpEventId, $eventAdditionalDataTypeName
        );
        $eventPlace = $eventAdditionalWhereData ? $eventAdditionalWhereData->contents : '';

        // Get link to the details page:
        $eventDetailsPageUrl = '';
        $detailsPageId = (int)$this->getSettings()->detailsPageId;
        if ($detailsPageId > 0)
        {
            $eventDetailsPageUrl = $this->getPageManager()->getFullPageCleanUrlByPageId($detailsPageId);
        }

        // Get the link to current page if no details page was specified:
        if ($eventDetailsPageUrl == '')
        {
            $pageManager = $this->getPageManager();
            $eventDetailsPageUrl = $pageManager->getFullPageCleanUrlByPageId($this->blockData->page_id);
        }

        // Generate a link to the event list page:
        $eventListUrl = 'http://' . $cmsConfig->domain_name . $eventDetailsPageUrl;
        $eventListLink = '<a href="' . $eventListUrl . '" target="_blank">' . $eventListUrl . '</a>';

        // Generate a link to event:
        $eventUrl = $escapeForHtml(
            $eventListUrl . ',' .
            $this->generateCleanUrlFromEventTitle($event->title) . '-' . $contentId
        );
        $eventLinkStart = '<a href="' . $eventUrl . '" target="_blank">';
        $eventLinkEnd = '</a>';
        $eventLink = $eventLinkStart . $eventUrl . $eventLinkEnd;

        // User may contact with the following person if he wants to ask about something:
        $contactPersonData = $this->getEventContactPersonData($signUpFormData->signUpEventId);

        // Replace any variable contained in message contents to proper value:
        $mailContents = str_replace(
            array(
                '{{user_academic_title}}',
                '{{user_name_and_surname}}',
                '{{user_email}}',
                '{{user_phone}}',
                '{{user_attentions}}',
                '{{user_institution_name}}',
                '{{user_invoice_data}}',
                '{{event_title}}',
                '{{event_start_date}}',
                '{{event_start_hour}}',
                '{{event_end_date}}',
                '{{event_end_hour}}',
                '{{event_place}}',
                '{{event_url}}',
                '{{event_link_start}}',
                '{{event_link_end}}',
                '{{event_link}}',
                '{{event_list_link}}',
                '{{event_creation_datetime}}',
                '{{contact_email}}',
                '{{contact_phone}}'
            ),
            array(
                $signUpFormData->signUpAcademicTitle,
                $signUpFormData->signUpNameAndSurname,
                $signUpFormData->signUpEmail,
                $signUpFormData->signUpPhone,
                $signUpFormData->signUpAttentions,
                $signUpFormData->signUpInstitutionName,
                $signUpFormData->signUpInvoiceData,
                $event->title,
                $eventStartDate,
                $eventStartHour,
                $eventEndDate,
                $eventEndHour,
                $eventPlace,
                $eventUrl,
                $eventLinkStart,
                $eventLinkEnd,
                $eventLink,
                $eventListLink,
                $event->creation_date,
                $contactPersonData->email,
                $contactPersonData->phone
            ),
            $mailContents
        );
        $plainMailContents = strip_tags($mailContents);

        if (($mailSubject == '') || ($mailContents == '') || ($plainMailContents == ''))
        {
            return false;
        }

        $mailSentSuccessfully = false;
        try
        {
            $mail = $this->getMailManager();

            foreach ($recipientEmailAddresses as $email)
            {
                $mail
                    ->setSubject($mailSubject)
                    ->setHtmlContents($mailContents)
                    ->setTextContents($plainMailContents)
                    ->setRecipientAddress($email)
                    ->send()
                ;
            }

            $mailSentSuccessfully = true;
        }
        catch (\Exception $e)
        {

        }

        return $mailSentSuccessfully;
    }

    private function updateExistingAdditionalDataByEventIdAdditionalDataId(
        $eventId, $additionalDataId, $additionalDataContents
    )
    {
        $updated = false;

        $contentId = $this->getContentId();
        $lastUpdateAuthorId = $this->user->user_id ? (int)$this->user->user_id : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_EVENT_LIST_ADDITIONAL_DATA_TABLE_NAME);
        $update
            ->set(
                array(
                    'last_update_author_id' => $lastUpdateAuthorId,
                    'last_update_date' => $lastUpdateDate,
                    'contents' => $additionalDataContents
                )
            )
            ->where(
                array(
                    'data_id' => $additionalDataId,
                    'event_id' => $eventId,
                    'content_id' => $contentId
                )
            );
        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $additionalData = $this->getAdditionalDataByEventIdAndAdditionalDataId($eventId, $additionalDataId);
            if (
                $additionalData &&
                ($additionalData->last_update_author_id == $lastUpdateAuthorId) &&
                ($additionalData->contents == $additionalDataContents)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    private function updateExistingEvent($eventData)
    {
        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();
        $event = null;

        $set = array(
            'category_id' => $eventData->eventCategoryId,
            'is_published' => $eventData->eventIsPublished,
            'is_promoted' => $eventData->eventIsPromoted,
            //'is_approved' => $eventData->eventIsApproved,
            'is_signing_up_enabled' => $eventData->eventIsSigningUpEnabled,
            //'attachment_id' => $eventData->eventAttachmentId,
            'last_update_author_id' => $eventData->eventLastUpdateAuthorId,
            'last_update_date' => $eventData->eventLastUpdateDate,
            //'approval_date' => $eventData->eventApprovalDate,
            'event_date_start' => $eventData->eventDateStart,
            'event_date_end' => $eventData->eventDateEnd,
            'contact_person_email' => $eventData->eventContactPersonEmail,
            'contact_person_phone' => $eventData->eventContactPersonPhone,
            'clean_url' => $eventData->eventCleanUrl,
            'title' => $eventData->eventTitle,
            'label' => $eventData->eventLabel,
            'teaser' => $eventData->eventTeaser,
            'contents' => $eventData->eventContents,
            'additional_contact_data' => $eventData->eventAuthorAdditionalContactData,
            'attentions' => $eventData->eventAttentions
        );

        if ($eventData->eventIsBeingApprovedNow)
        {
            $set['is_approved'] = $eventData->eventIsApproved;
            $set['approval_date'] = $eventData->eventApprovalDate;
        }

        $update = $sql->update(self::BLOCK_EVENT_LIST_TABLE_NAME);
        $update
            ->set($set)
            ->where(
                array(
                    'event_id' => $eventData->eventId,
                    'content_id' => $contentId
                )
            );

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $event = $this->getEventById($eventData->eventId);
            if ($event &&
                ($event->category_id == $eventData->eventCategoryId) &&
                ($event->is_published == $eventData->eventIsPublished) &&
                ($event->is_promoted == $eventData->eventIsPromoted) &&
                //($event->is_approved == $eventData->eventIsApproved) &&
                ($event->is_signing_up_enabled == $eventData->eventIsSigningUpEnabled) &&
                //($event->attachment_id == $eventData->eventAttachmentId) &&
                ($event->last_update_author_id == $eventData->eventLastUpdateAuthorId) &&
                ($event->clean_url == $eventData->eventCleanUrl) &&
                ($event->contact_person_email == $eventData->eventContactPersonEmail) &&
                ($event->contact_person_phone == $eventData->eventContactPersonPhone) &&
                ($event->title == $eventData->eventTitle) &&
                ($event->label == $eventData->eventLabel) &&
                ($event->teaser == $eventData->eventTeaser) &&
                ($event->contents == $eventData->eventContents) &&
                ($event->additional_contact_data == $eventData->eventAuthorAdditionalContactData) &&
                ($event->attentions == $eventData->eventAttentions)
            )
            {
                $eventImagesOk = true;

                // Array with all image hash and id, necessary to remove images from database which were removed by user
                // from add-edit dialog window and to set proper images order after they are saved in database:
                $existingImages = array();

                if ($eventData->eventImages)
                {
                    $firstImageId = null;
                    $atLeastOneImageWasSetAsMain = false;
                    foreach ($eventData->eventImages as $imageData)
                    {
                        $imageId = $imageData['imageId'];
                        $isMain = $imageData['imageIsMain'] == 1;
                        $uniqueHashForFileName = $imageData['imageUniqueHashForFileName'];

                        $result = $this->addOrEditEventImage($event->event_id, $imageData);
                        if ($result)
                        {
                            if (!is_bool($result))
                            {
                                $imageId = (int)$result;
                            }

                            if ($firstImageId === null)
                            {
                                $firstImageId = $imageId;
                            }

                            if ($isMain)
                            {
                                $atLeastOneImageWasSetAsMain = true;
                            }

                            $existingImages[$uniqueHashForFileName] = $imageId;
                        }
                        else
                        {
                            $eventImagesOk = false;

                            break;
                        }
                    }

                    $reordered = false;
                    if ($eventImagesOk)
                    {
                        $imagesOrder = array();
                        foreach ($eventData->eventImagesOrder as $uniqueHashForFileName)
                        {
                            $imageId =
                                isset($existingImages[$uniqueHashForFileName])
                                    ? (int)$existingImages[$uniqueHashForFileName]
                                    : null;
                            if ($imageId > 0)
                            {
                                $imagesOrder[] = $imageId;
                            }
                        }

                        $reordered = $this->reorderEventImages($event->event_id, $imagesOrder);
                    }

                    if (!$atLeastOneImageWasSetAsMain && $reordered)
                    {
                        if (!$this->setEventImageAsMainByEventIdAndImageId($event->event_id, $firstImageId))
                        {
                            $eventImagesOk = false;
                        }
                    }
                }

                if ($eventImagesOk)
                {
                    $deleted = $this->deleteEventImagesFromOutsideTheImageIdSet($event->event_id, $existingImages);
                    if (!$deleted)
                    {
                        $eventImagesOk = false;
                    }
                }

                $updated = $eventImagesOk;

                if ($updated && $eventData->eventAdditionalData)
                {
                    $additionalDataOk = true;

                    foreach ($eventData->eventAdditionalData as $dataTypeId => $dataContents)
                    {
                        $dataUpdated = $this->addOrEditAdditionalData($event->event_id, $dataTypeId, $dataContents);
                        if (!$dataUpdated)
                        {
                            $additionalDataOk = false;
                            break;
                        }
                    }

                    $updated = $additionalDataOk;
                }
            }
        }

        if ($updated && $event && $eventData->eventIsBeingApprovedNow)
        {
            $updated = false;

            $eventDetailsPageUrl = '';
            $detailsPageId = (int)$this->getSettings()->detailsPageId;
            if ($detailsPageId > 0)
            {
                $eventDetailsPageUrl = $this->getPageManager()->getFullPageCleanUrlByPageId($detailsPageId);
            }

            $cmsConfig = $this->getCmsConfig();
            $languageIsoCode = $this->language->iso_code;
            $escapeForHtml = $this->getEscapeForHtmlHelper();

            $eventUrl = $escapeForHtml(
                'http://' . $cmsConfig->domain_name . $eventDetailsPageUrl . ',' .
                $this->generateCleanUrlFromEventTitle($eventData->eventTitle) . '-' . $contentId
            );

            $eventTitle = $escapeForHtml($eventData->eventTitle);

            // Send mail about newly approved event:
            $mailSubject = $eventData->eventDefaultSubjectForMailAboutEventApproval;
            $mailContents = $eventData->eventDefaultContentsForMailAboutEventApproval;
            $mailContents =
                str_replace(
                    array(
                        '{{event_link}}',
                        '{{event_datetime}}',
                        "\n"
                    ),
                    array(
                        '<a href="' . $eventUrl . '">' . $eventTitle . '</a>',
                        '<em>' . $event->creation_date . '</em>',
                        "<br>\n"
                    ),
                    $mailContents
                );

            //var_dump($mailSubject, $mailContents, $eventData->eventAuthorEmail);

            if (($mailSubject != '') && ($mailContents != '') && ($eventData->eventAuthorEmail != ''))
            {
                try
                {
                    $mail = $this->getMailManager();
                    $mail
                        ->setRecipientAddress($eventData->eventAuthorEmail)
                        ->setSubject($mailSubject)
                        ->setHtmlContents($mailContents)
                        ->setTextContents(strip_tags($mailContents))
                        ->send()
                    ;

                    $updated = true;
                }
                catch (\Exception $e)
                {
                    // var_dump($e->getMessage());
                }
            }
        }

        if ($updated)
        {
            $eventCleanUrl = $this->generateCleanUrlFromEventTitle($eventData->eventTitle);
            $elementUrlParametersString =
                $eventCleanUrl . '-' . $contentId . '-' . $eventData->eventId;

            /*$this->addEventToSystemHistory(
                'Event (id: ' . $eventId . ') has been updated by {{user_login}}.'
            );*/

            $this->updateBlockRecordInSearchIndex(
                $eventData->eventId, $eventData->eventContents, $elementUrlParametersString, 'Event',
                $eventData->eventDateStart
            );

            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    private function updateExistingEventImage($eventId, $imageData)
    {
        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_EVENT_LIST_EVENT_IMAGES_TABLE_NAME);
        $update
            ->set(
                array(
                    'attachment_id' => $imageData->imageAttachmentId,
                    'is_main' => $imageData->imageIsMain,
                    'last_update_author_id' => $imageData->imageLastUpdateAuthorId,
                    'last_update_date' => $imageData->imageLastUpdateDate
                )
            )
            ->where(
                array(
                    'image_id' => $imageData->imageId,
                    'event_id' => $eventId,
                    'content_id' => $contentId
                )
            );

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $image = $this->getEventImageByEventIdAndImageId($eventId, $imageData->imageId);
            if ($image &&
                ($image->attachment_id == $imageData->imageAttachmentId) &&
                ($image->is_main == $imageData->imageIsMain)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }
    
    public function updateExistingGalleryImageTitleByEventIdAndImageId($eventId, $imageId, $imageTitle)
    {
        $eventId = (int)$eventId;
        $imageId = (int)$imageId;
        $imageTitle = trim((string)$imageTitle);

        if (($eventId <= 0) || ($imageId <= 0))
        {
            return false;
        }

        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_EVENT_LIST_EVENT_IMAGES_TABLE_NAME);
        $update
            ->set(
                array(
                    'title' => $imageTitle
                )
            )
            ->where(
                array(
                    'image_id' => $imageId,
                    'content_id' => $contentId,
                    'event_id' => $eventId
                )
            );
        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $image = $this->getEventImageByEventIdAndImageId($eventId, $imageId);
            if ($image && ($image->title == $imageTitle))
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }
}
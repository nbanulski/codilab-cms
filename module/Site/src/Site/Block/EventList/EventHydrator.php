<?php
namespace Site\Block\EventList;

use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\I18n\View\Helper\Translate;
use Site\Controller\AttachmentManager;

class EventHydrator implements HydratorInterface
{
    protected $translateHelper = null;
    protected $attachmentManager = null;
    protected $eventList = null;

    public function __construct(
        Translate $translateHelper, AttachmentManager $attachmentManager, EventList $eventList
    )
    {
        $this->translateHelper = $translateHelper;
        $this->attachmentManager = $attachmentManager;
        $this->eventList = $eventList;
    }

    public function extract($object) {}

    public function hydrate(array $data, $object)
    {
        foreach ($data as $property => $value)
        {
            $object[$property] = $value;
        }

        $translate = $this->translateHelper;
        $dateStartUnixTimestamp = strtotime($object['event_date_start']);
        $currentUnixTimestamp = time();

        $dateStart_monthName = $translate(date('F', $dateStartUnixTimestamp));
        if ($dateStart_monthName == 'May')
        {
            $dateStart_monthName = $translate('_month_may');
        }
        $dateStart_monthShortName = mb_substr($dateStart_monthName, 0, 3);
        $dateStart_day = date('d', $dateStartUnixTimestamp);
        $dateDiff = $currentUnixTimestamp - $dateStartUnixTimestamp;
        $daysLeft = abs(floor($dateDiff / 86400)); // 60 * 60 * 24.

        if ($daysLeft == 0)
        {
            $daysLeftStr = $translate('Today');
        }
        else if ($daysLeft == 1)
        {
            $daysLeftStr = $translate('Tomorrow');
        }
        else
        {
            $daysLeftStr = $daysLeft . ' ' . $translate('days');
        }

        $dateStart = date('Y-m-d', $dateStartUnixTimestamp);
        $timeStart = date('H:i', $dateStartUnixTimestamp);

        $object['start_date_month_short_name'] = $dateStart_monthShortName;
        $object['start_date_day'] = $dateStart_day;
        $object['days_left_str'] = $daysLeftStr;
        $object['date_start'] = $dateStart;
        $object['time_start'] = $timeStart;

        $object['mainImageUrl'] = '';
        $mainImage = $this->eventList->getEventMainImageByEventId($object['event_id']);
        if ($mainImage && ($mainImage->attachment_id > 0))
        {
            $attachment = $this->attachmentManager->getAttachmentById($mainImage->attachment_id);
            if ($attachment)
            {
                $object['mainImageUrl'] =
                    '/eng/get-file/index,' .
                    $attachment->hash_for_file_name .
                    ',' .
                    $attachment->original_file_name;
            }
        }

        $dataTypeName = $translate('Where');
        $object['where'] = $this->eventList->getAdditionalDataByEventIdAndDataTypeName(
            $object['event_id'], $dataTypeName
        );

        if ($object['where'] && ($object['where']->contents != ''))
        {
            $object['where']->contents = trim(strip_tags($object['where']->contents));
        }

        return $object;
    }
} 
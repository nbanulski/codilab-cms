<?php
namespace Site\Block\EmployeeDetails;

use Site\Block\AbstractBlock;
use Zend\Http\Request;

class EmployeeDetails extends AbstractBlock
{
    const BLOCK_EMPLOYEES_DEGREES_TABLE_NAME = 'block_employees_degrees';
    const BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME = 'block_employees_employees';

    protected $hasOwnSettings = true;
    protected $hasManagementWindow = false;

    /**
     * Render-functions.
     */

    public function childRender(Request $request)
    {
        $parameters = $this->getParametersForCurrentPageFromRequest($request);
        $employeePartialDataString = $parameters->key(); // Get name of the first parameter.

        list(
            $currentEmployeeCleanUrl,
            $currentEmployeeContentId,
            $currentEmployeeId
        ) = $this->getEmployeePartialDataFromEmployeePartialDataString($employeePartialDataString);

        $this->blockView->setVariables(
            array(
                'currentEmployeeCleanUrl' => $currentEmployeeCleanUrl,
                'currentEmployeeContentId' => $currentEmployeeContentId,
                'currentEmployeeDataAreValid' => (($currentEmployeeCleanUrl != '') && ($currentEmployeeContentId > 0))
            )
        );

        if (
            $this->employeeDetailsShouldBeRenderedForCurrentEmployeeIdAndContentId(
                $currentEmployeeId, $currentEmployeeContentId
            )
        )
        {
            $this->renderEmployeeDetails(
                $request,
                $employeePartialDataString,
                $currentEmployeeContentId,
                $currentEmployeeId
            );
        }
        else
        {
            $this->hideEmployeeDetails();
        }
    }

    public function hideEmployeeDetails()
    {
        $this->setRenderingEnabled(false);
    }

    public function renderEmployeeDetails(
        Request $request,
        $employeePartialDataString,
        $currentEmployeeContentId,
        $currentEmployeeId
    )
    {
        $employeeDetails = $this->getFullEmployeeInfoByEmployeeIdAndContentId(
            $currentEmployeeId, $currentEmployeeContentId
        );

        $this->blockView->setVariables(
            array(
                'employeePartialDataString' => $employeePartialDataString,
                'employeesContentId' => $currentEmployeeContentId,
                'employeeId' => $currentEmployeeId,
                'employeeDetails' => $employeeDetails
            )
        );
    }

    /**
     * Getters.
     */

    public function getEmployeeById($employeeId)
    {
        $employeeId = (int)$employeeId;

        if ($employeeId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('employee_id' => $employeeId));

        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getEmployeeByIdAndContentId($employeeId, $contentId)
    {
        $employeeId = (int)$employeeId;
        $contentId = (int)$contentId;

        if (($employeeId <= 0) || ($contentId <= 0))
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'employee_id' => $employeeId,
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getEmployeePartialDataFromEmployeePartialDataString($employeePartialDataString)
    {
        $parts = explode('-', $employeePartialDataString);

        $employeeId = array_pop($parts);
        $contentId = array_pop($parts);
        $employeeCleanUrl = join('-', $parts);

        return array($employeeCleanUrl, $contentId, $employeeId);
    }

    public function getEmployeeByEmployeeCleanUrlAndContentId($employeeCleanUrl, $contentId)
    {
        $employeeCleanUrl = trim((string)$employeeCleanUrl);
        $contentId = (int)$contentId;

        if (($employeeCleanUrl == '') || ($contentId < 0))
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $contentId,
                    'clean_url' => $employeeCleanUrl
                )
            );

        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return  $resultSet->current();
        }

        return false;
    }

    public function getFullEmployeeInfoByEmployeeIdAndContentId($employeeId, $contentId)
    {
        $employeeId = (int)$employeeId;
        $contentId = (int)$contentId;

        if (($employeeId <= 0) || ($contentId <= 0))
        {
            return false;
        }

        $fullInfo = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(array('bee' => self::BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME))
            ->join(
                array('bed' => self::BLOCK_EMPLOYEES_DEGREES_TABLE_NAME),
                'bed.degree_id = bee.degree_id',
                array('degree_title' => 'title'),
                $select::JOIN_LEFT
            )
            ->join(
                array('u' => 'users'),
                'u.user_id = bee.last_update_author_id',
                array('last_update_author' => 'login'),
                $select::JOIN_LEFT
            )
            ->where(
                array(
                    'bee.employee_id' => $employeeId,
                    'bee.content_id' => $contentId,
                    'bed.content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $fullInfo = $resultSet->current();
        }

        if ($fullInfo)
        {
            $thumbUrl = '/img/no-image.png';

            $blockManager = $this->getBlockManager();
            $employeesBlock = $blockManager->createBlockUsingContentId($contentId);
            if ($employeesBlock instanceof \Site\Block\Employees\Employees)
            {
                if ($fullInfo->attachment_id > 0)
                {
                    $attachment = $this->attachmentManager->getAttachmentById($fullInfo->attachment_id);
                    if ($attachment)
                    {
                        $thumbUrl =
                            '/eng/get-file/index,' .
                            $attachment->hash_for_file_name .
                            ',' .
                            $attachment->original_file_name;
                    }
                }
            }

            $fullInfo->thumbUrl = $thumbUrl;
        }


        return $fullInfo;
    }

    public function employeeDetailsShouldBeRenderedForCurrentEmployeeIdAndContentId(
        $currentEmployeeId, $currentEmployeeContentId
    )
    {
        $settings = $this->getSettings();
        $shouldBeHiddenIfCurrentUrlDoesNotLeadsToEmployeeDetails =
            $settings->hideIfCurrentUrlDoesNotLeadsToEmployeeDetails ? true : false;

        $shouldBeRendered =
            !$shouldBeHiddenIfCurrentUrlDoesNotLeadsToEmployeeDetails || // Hiding is disabled...
            // ... or data needed for displaying employee's details are correct:
            (($currentEmployeeId > 0) && ($currentEmployeeContentId > 0));

        if ($shouldBeRendered)
        {
            // Check also if given data really points to employee details, not to - for example - employee details:

            $employee = $this->getEmployeeByIdAndContentId($currentEmployeeId, $currentEmployeeContentId);
            if (!$employee)
            {
                return false;
            }
        }

        return $shouldBeRendered;
    }

    /**
     * Data formatting functions.
     */

    public function generateCleanUrlFromEmployeeNameAndSurname($nameAndSurname)
    {
        return $this->generateCleanUrlFromText($nameAndSurname);
    }

    /**
     * Logic functions.
     */

    public function setEmployeeDegreeId($employeeId, $degreeId)
    {
        $employeeId = (int)$employeeId;
        $degreeId = (int)$degreeId;

        if (($employeeId <= 0) || ($degreeId <= 0))
        {
            return false;
        }

        $updated = false;

        $lastUpdateAuthorId = $this->user->user_id ? (int)$this->user->user_id : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME);
        $update
            ->set(
                array(
                    'degree_id' => $degreeId,
                    'last_update_author_id' => $lastUpdateAuthorId,
                    'last_update_date' => $lastUpdateDate
                )
            )
            ->where(array('employee_id' => $employeeId));

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $employee = $this->getEmployeeById($employeeId);
            if ($employee &&
                ($employee->degree_id == $degreeId) &&
                ($employee->last_update_author_id == $lastUpdateAuthorId)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function updateEmployeeNameAndSurname($employeeId, $nameAndSurname)
    {
        $employeeId = (int)$employeeId;
        $nameAndSurname = trim(strip_tags((string)$nameAndSurname));
        $nameAndSurname = str_replace("\r\n", ' ', $nameAndSurname);
        $nameAndSurname = str_replace(array("\r", "\n"), ' ', $nameAndSurname);

        if (($employeeId <= 0) || ($nameAndSurname == ''))
        {
            return false;
        }

        $updated = false;

        $lastUpdateAuthorId = $this->user->user_id ? (int)$this->user->user_id : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME);
        $update
            ->set(
                array(
                    'last_update_author_id' => $lastUpdateAuthorId,
                    'last_update_date' => $lastUpdateDate,
                    'name_and_surname' => $nameAndSurname
                )
            )
            ->where(array('employee_id' => $employeeId));

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $employee = $this->getEmployeeById($employeeId);
            if ($employee &&
                ($employee->last_update_author_id == $lastUpdateAuthorId) &&
                ($employee->name_and_surname == $nameAndSurname)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function updateEmployeePosition($employeeId, $position)
    {
        $employeeId = (int)$employeeId;
        $position = trim(strip_tags((string)$position));
        $position = str_replace("\r\n", ' ', $position);
        $position = str_replace(array("\r", "\n"), ' ', $position);

        if (($employeeId <= 0) || ($position == ''))
        {
            return false;
        }

        $updated = false;

        $lastUpdateAuthorId = $this->user->user_id ? (int)$this->user->user_id : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME);
        $update
            ->set(
                array(
                    'last_update_author_id' => $lastUpdateAuthorId,
                    'last_update_date' => $lastUpdateDate,
                    'position' => $position
                )
            )
            ->where(array('employee_id' => $employeeId));

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $employee = $this->getEmployeeById($employeeId);
            if ($employee &&
                ($employee->last_update_author_id == $lastUpdateAuthorId) &&
                ($employee->position == $position)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function updateEmployeeAbout($employeeId, $about)
    {
        $employeeId = (int)$employeeId;
        $about = trim((string)$about);

        if (($employeeId <= 0) || ($about == ''))
        {
            return false;
        }

        $updated = false;

        $lastUpdateAuthorId = $this->user->user_id ? (int)$this->user->user_id : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME);
        $update
            ->set(
                array(
                    'last_update_author_id' => $lastUpdateAuthorId,
                    'last_update_date' => $lastUpdateDate,
                    'about' => $about
                )
            )
            ->where(array('employee_id' => $employeeId));

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $employee = $this->getEmployeeById($employeeId);
            if ($employee &&
                ($employee->last_update_author_id == $lastUpdateAuthorId) &&
                ($employee->about == $about)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $employeeCleanUrl = $this->generateCleanUrlFromEmployeeNameAndSurname($employee->name_and_surname);
            $elementUrlParametersString =
                $employeeCleanUrl . '-' . $employee->content_id . '-' . $employee->employee_id;

            $this->updateBlockRecordInSearchIndex($employeeId, $about, $elementUrlParametersString, 'About employee');

            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function updateEmployeeScientificCareer($employeeId, $scientificCareer)
    {
        $employeeId = (int)$employeeId;
        $scientificCareer = trim((string)$scientificCareer);

        if (($employeeId <= 0) || ($scientificCareer == ''))
        {
            return false;
        }

        $updated = false;

        $lastUpdateAuthorId = $this->user->user_id ? (int)$this->user->user_id : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME);
        $update
            ->set(
            array(
                'last_update_author_id' => $lastUpdateAuthorId,
                'last_update_date' => $lastUpdateDate,
                'scientific_career' => $scientificCareer
            )
        )
            ->where(array('employee_id' => $employeeId));

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $employee = $this->getEmployeeById($employeeId);
            if ($employee &&
                ($employee->last_update_author_id == $lastUpdateAuthorId) &&
                ($employee->scientific_career == $scientificCareer)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function updateEmployeeTheMostImportantPublications($employeeId, $theMostImportantPublications)
    {
        $employeeId = (int)$employeeId;
        $theMostImportantPublications = trim((string)$theMostImportantPublications);

        if (($employeeId <= 0) || ($theMostImportantPublications == ''))
        {
            return false;
        }

        $updated = false;

        $lastUpdateAuthorId = $this->user->user_id ? (int)$this->user->user_id : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME);
        $update
            ->set(
            array(
                'last_update_author_id' => $lastUpdateAuthorId,
                'last_update_date' => $lastUpdateDate,
                'most_important_publications' => $theMostImportantPublications
            )
        )
            ->where(array('employee_id' => $employeeId));

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $employee = $this->getEmployeeById($employeeId);
            if ($employee &&
                ($employee->last_update_author_id == $lastUpdateAuthorId) &&
                ($employee->most_important_publications == $theMostImportantPublications)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function updateEmployeeContactInfo($employeeId, $contactInfo)
    {
        $employeeId = (int)$employeeId;
        $contactInfo = trim((string)$contactInfo);

        if (($employeeId <= 0) || ($contactInfo == ''))
        {
            return false;
        }

        $updated = false;

        $lastUpdateAuthorId = $this->user->user_id ? (int)$this->user->user_id : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_EMPLOYEES_EMPLOYEES_TABLE_NAME);
        $update
            ->set(
                array(
                    'last_update_author_id' => $lastUpdateAuthorId,
                    'last_update_date' => $lastUpdateDate,
                    'contact_info' => $contactInfo
                )
            )
            ->where(array('employee_id' => $employeeId));

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $employee = $this->getEmployeeById($employeeId);
            if ($employee &&
                ($employee->last_update_author_id == $lastUpdateAuthorId) &&
                ($employee->contact_info == $contactInfo)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }
} 
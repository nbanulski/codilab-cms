<?php
namespace Site\Block\Popup;

use Site\Block\AbstractBlock;
use Zend\Http\Request;

class Popup extends AbstractBlock
{
    const BLOCK_POPUP = 'block_popup';

    /**
     * Events.
     */

    public function onAfterInserting($slotId)
    {
        return $this->createEmptyContents();
    }

    public function onBeforeDeleted()
    {
        if (!$this->blockData->is_dynamic)
        {
            $this->deleteContents();
        }
    }

    /**
     * Render-functions.
     */

    public function childRender($request)
    {
        $cookieName = 'popup_' . $this->getContentId();
        $cookies = $request->getCookie();
        $popupCookieExists = property_exists($cookies, $cookieName);

        $firstDisplayAfterCreating = (bool)$this->blockView->getVariable('firstDisplayAfterCreating');
        $doNotDisplayPopupNow = (bool)$this->blockView->getVariable('doNotDisplayPopupNow');

        $popupTitle = '';
        $popupContents = '';
        $popupData = $this->getPopupData();
        if ($popupData)
        {
            $popupTitle = $popupData->title;
            $popupContents = $popupData->contents;
        }

        $this->blockView->setVariables(
            array(
                'popupCookieExists' => $popupCookieExists,
                'firstDisplayAfterCreating' => $firstDisplayAfterCreating,
                'doNotDisplayPopupNow' => $doNotDisplayPopupNow,
                'popupTitle' => $popupTitle,
                'popupContents' => $popupContents
            )
        );
    }

    public function renderContentsView(Request $request)
    {
        $contents = $this->getContents();

        $editContentsView = new \Zend\View\Model\ViewModel();
        $editContentsView->setTemplate('blocks/' . $this->blockViewName . '/contents.phtml');
        $editContentsView->setVariables(
            array(
                'user' => $this->user,
                'language' => $this->language,
                'userLanguage' => $this->userLanguage,
                'designMode' => $this->designMode,
                'blockData' => $this->blockData,
                'contents' => $contents
            )
        );

        return $this->phpRenderer->render($editContentsView);
    }

    public function renderEditView(Request $request)
    {
        $defaultFormData = array(
            'popupTitle' => '',
            'popupContents' => ''
        );
        $currentFormData = array();

        $popupData = $this->getPopupData();
        if ($popupData)
        {
            $currentFormData = array(
                'popupTitle' => $popupData->title,
                'popupContents' => $popupData->contents
            );
        }

        $formData = array_merge($defaultFormData, $currentFormData);

        $editContentsView = new \Zend\View\Model\ViewModel();
        $editContentsView->setTemplate('blocks/' . $this->blockViewName . '/edit.phtml');
        $editContentsView->setVariables(
            array(
                'user' => $this->user,
                'language' => $this->language,
                'userLanguage' => $this->userLanguage,
                'designMode' => $this->designMode,
                'blockData' => $this->blockData,
                'formData' => $formData
            )
        );

        return $this->phpRenderer->render($editContentsView);
    }

    /**
     * Getters.
     */

    public function getContents()
    {
        $contentId = $this->getContentId();

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_POPUP)
            ->columns(array('contents'), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();

            return $row ? $row->contents : false;
        }

        return false;
    }

    public function getTitle()
    {
        $contentId = $this->getContentId();

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_POPUP)
            ->columns(array('title'), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();

            return $row ? $row->title : false;
        }

        return false;
    }

    public function getPopupData()
    {
        $contentId = $this->getContentId();

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_POPUP)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    /**
     * Data formatting functions.
     */

    public function formatPopupDataAndReturnIfAreValid($popupData)
    {
        $popupData = $this->convertToFlexibleContainer($popupData);
        $popupDataCopy = clone $popupData;

        $popupDataCopy->popupTitle = trim((string)$popupData->popupTitle);
        $popupDataCopy->popupContents = trim((string)$popupData->popupContents);

        /**
         * Identifying the author.
         */

        $popupDataCopy->popupLastUpdateAuthorId = $this->user->user_id;

        if (($popupDataCopy->popupTitle == '') || ($popupDataCopy->popupContents == ''))
        {
            return false;
        }

        return $popupDataCopy;
    }

    /**
     * Logic-functions.
     */

    public function createEmptyContents()
    {
        $contentId = $this->getContentId();
        $lastUpdateAuthorId = null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $created = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $insert = $sql->insert(self::BLOCK_POPUP);
        $insert
            ->columns(
                array(
                    'content_id',
                    'last_update_author_id',
                    'last_update_date',
                    'title',
                    'contents'
                )
            )
            ->values(
                array(
                    'content_id' => $contentId,
                    'last_update_author_id' => $lastUpdateAuthorId,
                    'last_update_date' => $lastUpdateDate,
                    'title' => '',
                    'contents' => ''
                )
            );
        $result = $dbManager->executePreparedStatement($insert);
        if ($result)
        {
            $popup = $this->getPopupData();
            if (
                $popup &&
                ($popup->content_id == $contentId) &&
                ($popup->last_update_author_id == $lastUpdateAuthorId) &&
                ($popup->title == '') &&
                ($popup->contents == '')
            )
            {
                $created = true;
            }
        }

        if ($created)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $created;
    }

    public function deleteContents()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete(self::BLOCK_POPUP);
        $delete->where(array('content_id' => $this->getContentId()));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;
    }

    public function saveContents($popupData)
    {
        $popupDataCopy = $this->formatPopupDataAndReturnIfAreValid($popupData);
        if (!$popupDataCopy)
        {
            return false;
        }

        $popupDataCopy->popupLastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();
        $popup = null;

        $update = $sql->update(self::BLOCK_POPUP);
        $update
            ->set(
                array(
                    'last_update_author_id' => $popupData->popupLastUpdateAuthorId,
                    'last_update_date' => $popupData->popupLastUpdateDate,
                    'title' => $popupData->popupTitle,
                    'contents' => $popupData->popupContents,
                )
            )
            ->where(
                array(
                    'content_id' => $contentId
                )
            );

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $popup = $this->getPopupData();
            if (
                $popup &&
                ($popup->last_update_author_id == $popupData->popupLastUpdateAuthorId) &&
                ($popup->title == $popupData->popupTitle) &&
                ($popup->contents == $popupData->popupContents)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }
} 
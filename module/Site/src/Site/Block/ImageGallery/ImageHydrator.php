<?php
namespace Site\Block\ImageGallery;

use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\I18n\View\Helper\Translate;
use Site\Controller\AttachmentManager;
use Site\Controller\PageManager;

class ImageHydrator implements HydratorInterface
{
    protected $attachmentManager = null;
    protected $imageGallery = null;
    protected $pageManager = null;
    protected $translateHelper = null;

    public function __construct(
        Translate $translateHelper, AttachmentManager $attachmentManager, ImageGallery $imageGallery
    )
    {
        $this->attachmentManager = $attachmentManager;
        $this->imageGallery = $imageGallery;
        $this->translateHelper = $translateHelper;
    }

    public function extract($object) {}

    public function hydrate(array $data, $object)
    {
        foreach ($data as $property => $value)
        {
            $object[$property] = $value;
        }

        $imageGallery = $this->imageGallery;
        $translate = $this->translateHelper;

        $noImageUrl = '/img/no-image.png';
        $object['thumbUrl'] = $noImageUrl;
        $object['imageUrl'] = $noImageUrl;
        if ($object['attachment_id'] > 0)
        {
            $attachment = $this->attachmentManager->getAttachmentById($object['attachment_id']);
            if ($attachment)
            {
                $object['thumbUrl'] =
                    '/eng/get-file/index,' .
                    $attachment->hash_for_file_name .
                    ',' .
                    $attachment->original_file_name;

                $parentAttachment = $this->attachmentManager->getAttachmentById($attachment->parent_attachment_id);
                if ($parentAttachment)
                {
                    $object['imageUrl'] =
                        '/eng/get-file/index,' .
                        $parentAttachment->hash_for_file_name .
                        ',' .
                        $parentAttachment->original_file_name;
                }
            }
        }

        $object['thumbAlt'] = $translate('No thumbnail');
        if ($object['thumbUrl'] != $noImageUrl)
        {
            $object['thumbAlt'] = $object['title'];
            if ($object['thumbAlt'] == '')
            {
                $object['thumbAlt'] = $translate('Thumbnail');
            }
        }

        return $object;
    }
}
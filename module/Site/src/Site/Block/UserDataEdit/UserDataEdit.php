<?php
namespace Site\Block\UserDataEdit;

use Site\Block\AbstractBlock;

class UserDataEdit extends AbstractBlock
{
    protected $hasManagementWindow = false;
    protected $userData = array();
}
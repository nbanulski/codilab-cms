<?php
namespace Site\Block\NewsDetails;

use Site\Block\AbstractBlock;
use Zend\Http\Request;

class NewsDetails extends AbstractBlock
{
    const BLOCK_NEWS_LIST_TABLE_NAME = 'block_news_list';
    const BLOCK_NEWS_LIST_CATEGORIES_TABLE_NAME = 'block_news_list_categories';

    protected $hasOwnSettings = true;
    protected $hasManagementWindow = false;

    /**
     * Render-functions.
     */

    public function childRender(Request $request)
    {
        $parameters = $this->getParametersForCurrentPageFromRequest($request);
        $newsCleanUrlWithContentId = $parameters->key(); // Get name of the first parameter.

        list($currentNewsCleanUrl, $currentNewsContentId) =
            $this->getNewsCleanUrlAndContentIdFromNewsCleanUrlWithContentId($newsCleanUrlWithContentId);

        $this->blockView->setVariables(
            array(
                'currentNewsCleanUrl' => $currentNewsCleanUrl,
                'currentNewsContentId' => $currentNewsContentId,
                'currentNewsDataAreValid' => (($currentNewsCleanUrl != '') && ($currentNewsContentId > 0))
            )
        );

        if (
        $this->newsDetailsShouldBeRenderedForCurrentNewsCleanUrlAndContentId(
            $currentNewsCleanUrl,
            $currentNewsContentId
        )
        )
        {
            $this->renderNewsDetails(
                $request,
                $newsCleanUrlWithContentId,
                $currentNewsCleanUrl,
                $currentNewsContentId
            );
        }
        else
        {
            $this->hideNewsDetails();
        }
    }

    public function hideNewsDetails()
    {
        $this->setRenderingEnabled(false);
    }

    public function renderNewsDetails(
        Request $request,
        $newsCleanUrlWithContentId,
        $currentNewsCleanUrl,
        $currentNewsContentId
    )
    {
        $newsDetails = $this->getFullNewsInfoByNewsCleanUrlAndContentId(
            $currentNewsCleanUrl, $currentNewsContentId
        );

        $this->blockView->setVariables(
            array(
                'newsCleanUrlWithContentId' => $newsCleanUrlWithContentId,
                'newsListContentId' => $currentNewsContentId,
                'newsDetails' => $newsDetails
            )
        );
    }

    /**
     * Getters.
     */

    public function getNewsById($newsId)
    {
        $newsId = (int)$newsId;

        if ($newsId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_NEWS_LIST_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('news_id' => $newsId));

        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getNewsByNewsCleanUrlAndContentId($newsCleanUrl, $contentId)
    {
        $newsCleanUrl = trim((string)$newsCleanUrl);
        $contentId = (int)$contentId;

        if (($newsCleanUrl == '') || ($contentId < 0))
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_NEWS_LIST_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $contentId,
                    'clean_url' => $newsCleanUrl
                )
            );

        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function getFullNewsInfoByNewsCleanUrlAndContentId($newsCleanUrl, $contentId)
    {
        $newsCleanUrl = mb_strtolower(trim((string)$newsCleanUrl));
        $contentId = (int)$contentId;

        if (($newsCleanUrl == '') || ($contentId <= 0))
        {
            return false;
        }

        $fullInfo = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(array('bnl' => self::BLOCK_NEWS_LIST_TABLE_NAME))
            ->join(
                array('bnlc' => self::BLOCK_NEWS_LIST_CATEGORIES_TABLE_NAME),
                'bnlc.category_id = bnl.category_id',
                array('category_name' => 'name'),
                $select::JOIN_LEFT
            )
            ->join(
                array('u' => 'users'),
                'u.user_id = bnl.last_update_author_id',
                array('last_update_author' => 'login'),
                $select::JOIN_LEFT
            )
            ->where(
                array(
                    'bnl.content_id' => $contentId,
                    'bnl.clean_url' => $newsCleanUrl,
                    'bnlc.content_id' => $contentId
                )
            );
        $select->order('bnl.news_date ASC');
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $fullInfo = $resultSet->current();
        }

        if ($fullInfo)
        {
            /**
             * Images.
             */

            $mainImage = false;
            $mainImageUrl = '';
            $images = new \Site\Custom\FlexibleContainer();

            $blockManager = $this->getBlockManager();
            $newsListBlock = $blockManager->createBlockUsingContentId($contentId);
            if ($newsListBlock instanceof \Site\Block\NewsList\NewsList)
            {
                $mainImage = $newsListBlock->getNewsMainImageByNewsId($fullInfo->news_id);
                if ($mainImage)
                {
                    $attachment = $this->attachmentManager->getAttachmentById($mainImage->attachment_id);
                    if ($attachment)
                    {
                        $mainImageUrl =
                            '/eng/get-file/index,' .
                            $attachment->hash_for_file_name .
                            ',' .
                            $attachment->original_file_name;
                    }
                }

                $excludeMainImage = true;
                $images = $newsListBlock->getNewsImagesByNewsId($fullInfo->news_id, $excludeMainImage);
            }

            $fullInfo->date = date('Y-m-d H:i', strtotime($fullInfo->news_date));
            $fullInfo->mainImage = $mainImage;
            $fullInfo->mainImageUrl = $mainImageUrl;
            $fullInfo->images = $images;
        }

        return $fullInfo;
    }

    public function newsDetailsShouldBeRenderedForCurrentNewsCleanUrlAndContentId(
        $currentNewsCleanUrl,
        $currentNewsContentId
    ) {
        $settings = $this->getSettings();
        $shouldBeHiddenIfCurrentUrlDoesNotLeadsToNewsDetails =
            $settings->hideIfCurrentUrlDoesNotLeadsToNewsDetails ? true : false;

        $shouldBeRendered =
            !$shouldBeHiddenIfCurrentUrlDoesNotLeadsToNewsDetails || // Hiding is disabled...
            // ... or data needed for displaying news's details are correct:
            (($currentNewsCleanUrl != '') && ($currentNewsContentId > 0));

        if ($shouldBeRendered)
        {
            // Check also if given data really points to news details, not to - for example - event's details:

            $news = $this->getNewsByNewsCleanUrlAndContentId($currentNewsCleanUrl, $currentNewsContentId);
            if (!$news)
            {
                return false;
            }
        }

        return $shouldBeRendered;
    }

    /**
     * Logic functions.
     */

    public function updateNewsTitle($newsId, $newsTitle, $currentUrl)
    {
        $newsId = (int)$newsId;
        $newsTitle = trim(strip_tags((string)$newsTitle));
        $newsTitle = str_replace("\r\n", ' ', $newsTitle);
        $newsTitle = str_replace(array("\r", "\n"), ' ', $newsTitle);

        if (($newsId <= 0) || ($newsTitle == ''))
        {
            return false;
        }

        $updated = false;

        $lastUpdateAuthorId = $this->user->user_id ? (int)$this->user->user_id : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_NEWS_LIST_TABLE_NAME);
        $update
            ->set(
            array(
                'last_update_author_id' => $lastUpdateAuthorId,
                'last_update_date' => $lastUpdateDate,
                'title' => $newsTitle
            )
        )
            ->where(array('news_id' => $newsId));

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $news = $this->getNewsById($newsId);
            if (
                $news &&
                ($news->last_update_author_id == $lastUpdateAuthorId) &&
                ($news->title == $newsTitle)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function updateNewsContents($newsId, $newsContents, $currentUrl)
    {
        $newsId = (int)$newsId;
        $newsContents = trim((string)$newsContents);

        if (($newsId <= 0) || ($newsContents == ''))
        {
            return false;
        }

        $updated = false;

        $lastUpdateAuthorId = $this->user->user_id ? (int)$this->user->user_id : null;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_NEWS_LIST_TABLE_NAME);
        $update
            ->set(
                array(
                    'last_update_author_id' => $lastUpdateAuthorId,
                    'last_update_date' => $lastUpdateDate,
                    'contents' => $newsContents
                )
            )
            ->where(array('news_id' => $newsId));

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $news = $this->getNewsById($newsId);
            if (
                $news &&
                ($news->last_update_author_id == $lastUpdateAuthorId) &&
                ($news->contents == $newsContents)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    /**
     * Other functions.
     */

    public function getNewsCleanUrlAndContentIdFromNewsCleanUrlWithContentId($newsCleanUrlWithContentId)
    {
        $newsCleanUrl = $newsCleanUrlWithContentId;
        $contentId = null;

        $characterPosition = mb_strrpos($newsCleanUrlWithContentId, '-');
        if ($characterPosition !== false)
        {
            $totalLength = mb_strlen($newsCleanUrlWithContentId);
            $newsCleanUrl = mb_substr($newsCleanUrlWithContentId, 0, $characterPosition);
            $contentId = mb_substr($newsCleanUrlWithContentId, -($totalLength - $characterPosition - 1));
        }

        return array($newsCleanUrl, $contentId);
    }
}
<?php
namespace Site\Block\OldEventList;

use Site\Block\AbstractBlock;
use Zend\Http\Request;

class OldEventList extends AbstractBlock
{
    const BLOCK_EVENT_LIST_TABLE_NAME = 'block_event_list';
    const BLOCK_EVENT_LIST_CATEGORIES_TABLE_NAME = 'block_event_list_categories';

    protected $hasOwnSettings = true;
    protected $hasManagementWindow = true;

    /**
     * Render-functions.
     */

    public function childRender(Request $request)
    {
        $settings = $this->getSettings();
        list($currentEventCleanUrl, $currentEventContentId) = $this->getEventCleanUrlAndContentIdFromRequest($request);

        $oldEventListShouldBeRendered = true;
        $shouldBeHiddenIfCurrentUrlLeadsToEventDetails = $settings->hideIfCurrentUrlLeadsToEventDetails ? true : false;

        $currentEventId = null;
        if ($shouldBeHiddenIfCurrentUrlLeadsToEventDetails)
        {
            // Data needed for displaying event's details are correct:
            if (($currentEventCleanUrl != '') && ($currentEventContentId > 0))
            {
                $event = $this->getEventByEventCleanUrlAndContentId($currentEventCleanUrl, $currentEventContentId);
                if ($event) // Event exists.
                {
                    $currentEventId = $event->event_id;
                    $oldEventListShouldBeRendered = false;
                }
            }
        }

        $this->blockView->setVariables(
            array(
                'currentEventId' => $currentEventId,
                'currentEventCleanUrl' => $currentEventCleanUrl,
                'currentEventContentId' => $currentEventContentId,
                'currentEventDataAreValid' => (($currentEventCleanUrl != '') && ($currentEventContentId > 0))
            )
        );

        if ($oldEventListShouldBeRendered)
        {
            $this->renderOldEventList($request);
        }
        else
        {
            $this->hideOldEventList();
        }
    }

    public function childRenderSettings()
    {
        $settings = $this->getSettings();
        $blockManager = $this->getBlockManager();
        $translate = $this->getTranslateHelper();
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $eventListPageBlockId = (int)$settings->eventListPageBlockId;

        $select = $sql->select();
        $select
            ->from(array('pb' => 'pages_blocks'))
            ->join(
                array('b' => 'blocks'),
                'b.block_id = pb.block_id',
                array(),
                $select::JOIN_LEFT
            )
            ->join(
                array('bp' => 'blocks_properties'),
                'bp.block_id = b.block_id',
                array('block_translated_name' => 'name'),
                $select::JOIN_LEFT
            )
            ->join(
                array('p' => 'pages'),
                'p.page_id = pb.page_id',
                array('page_title' => 'title'),
                $select::JOIN_LEFT
            )
            ->join(
                array('p2' => 'pages'),
                'p2.page_id = p.parent_id',
                array('parent_page_title' => 'title'),
                'LEFT OUTER'
            )
            ->where(
                array(
                    'pb.is_parent_block_dynamic' => 0,
                    'b.php_class_name' => 'EventList',
                    'bp.language_id' => $this->language->language_id
                )
            )
            ->order(array('block_translated_name ASC'));
        $resultSet = $dbManager->getHydratedResultSet($select);

        $select = $sql->select();
        $select->from(array('pb' => 'pages_blocks'))
            ->join(
                array('b' => 'blocks'),
                'b.block_id = pb.block_id',
                array(),
                $select::JOIN_LEFT
            )
            ->join(
                array('bp' => 'blocks_properties'),
                'bp.block_id = b.block_id',
                array('block_translated_name' => 'name'),
                $select::JOIN_LEFT
            )
            ->join(
                array('p' => 'pages'),
                'p.page_id = pb.page_id',
                array('page_title' => 'title'),
                $select::JOIN_LEFT
            )
            ->join(
                array('p2' => 'pages'),
                'p2.page_id = p.parent_id',
                array('parent_page_title' => 'title'),
                'LEFT OUTER'
            )
            ->where(
                array(
                    'pb.is_parent_block_dynamic' => 1,
                    'b.php_class_name' => 'EventList',
                    'bp.language_id' => $this->language->language_id
                )
            )
            //->group('b.block_id')
            ->order(array('block_translated_name ASC'));
        $resultSet2 = $dbManager->getHydratedResultSet($select);

        $array1 = $resultSet ? $resultSet->toArray() : array();
        $array2 = $resultSet2 ? $resultSet2->toArray() : array();
        unset($resultSet);
        unset($resultSet2);
        $rowsFromBothResultSets = array_merge($array1, $array2);
        unset($array1);
        unset($array2);

        /**
         * Available EventList blocks.
         */
        $availableEventListBlocks = array();
        foreach ($rowsFromBothResultSets as $pageBlock)
        {
            $pageStr = $pageBlock['page_title'];
            if ($pageBlock['parent_page_title'] != '')
            {
                $pageStr = $pageBlock['parent_page_title'] . ' &gt; ' . $pageStr;
            }

            if ($pageBlock['is_parent_block_dynamic'])
            {
                $blockTypeStr = ' (' . $translate('dynamic block', 'default', $this->userLanguage->zend2_locale) . ')';
            }
            else
            {
                $blockTypeStr = ' (' . $translate('standard block', 'default', $this->userLanguage->zend2_locale) . ')';
            }

            $pageBlockId = $pageBlock['page_block_id'];
            $availableEventListBlocks[$pageBlockId] =
                '[id: ' . $pageBlockId . '] ' .
                $translate('Page', 'default', $this->userLanguage->zend2_locale) . ': ' .
                $pageStr . ' | ' .
                $translate('Block', 'default', $this->userLanguage->zend2_locale) . ': ' .
                $pageBlock['block_translated_name'] .
                $blockTypeStr;
        }

        if (!$availableEventListBlocks)
        {
            $availableEventListBlocks[''] = '-----';
        }

        /**
         * Available categories.
         */
        $availableEventListCategories = array(
            '0' => '-- ' . $translate('All categories', 'default', $this->userLanguage->zend2_locale) . ' --'
        );
        $eventListBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($eventListPageBlockId);
        if ($eventListBlock instanceof \Site\Block\EventList\EventList)
        {
            $categories = $eventListBlock->getCategories();
            if ($categories)
            {
                foreach ($categories as $category)
                {
                    $availableEventListCategories[$category->category_id] = $category->name;
                }
            }
        }

        $this->settingsView->setVariables(
            array(
                'availableEventListBlocks' => $availableEventListBlocks,
                'availableEventListCategories' => $availableEventListCategories
            )
        );
    }

    public function hideOldEventList()
    {
        $this->setRenderingEnabled(false);
    }

    public function renderOldEventList(Request $request)
    {
        $blockManager = $this->getBlockManager();
        $pageManager = $this->getPageManager();
        $dbManager = $this->getDatabaseManager();
        $dbAdapter = $dbManager->getAdapter();
        $sql = $dbManager->getSql();

        $settings = $this->getSettings();
        $pageNumber = (int)$this->blockView->pageNumber ? : 1;
        $itemCountPerPage = (int)$settings->itemCountPerPage ? : 7;
        $eventListPageBlockId = (int)$settings->eventListPageBlockId;
        $eventListCategoryId = (int)$settings->eventListCategoryId;

        $eventDetailsPageUrl = '';
        $eventCategoriesOptions = array();
        $paginator = null;

        // Hide management icon when no EventList id was set, because we don't know what block should being managed:
        if ($eventListPageBlockId <= 0)
        {
            $this->hasManagementWindow = false;
            if ($this->frameView)
            {
                $this->frameView->setVariable('hasManagementWindow', false);
            }
        }

        $contentId = $blockManager->getPageBlockContentIdByPageBlockId($eventListPageBlockId);
        if ($contentId <= 0) // If content id could not be found.
        {
            $eventListPageBlockId = null; // Block does not exists.
        }

        $eventListBlock = $blockManager->createBlockUsingPageBlockIdAndLanguageId($eventListPageBlockId);
        if ($eventListBlock instanceof \Site\Block\EventList\EventList)
        {
            //$eventListSettings = $blockManager->getPageBlockSettings($eventListPageBlockId);
            $eventListSettings = $eventListBlock->getSettings();

            if ($eventListCategoryId <= 0)
            {
                $eventListCategoryId = (int)$eventListSettings->eventListCategoryId;
            }

            if ($eventListSettings instanceof \Site\Custom\FlexibleContainer)
            {
                $detailsPageId = (int)$eventListSettings->detailsPageId;
                if ($detailsPageId > 0)
                {
                    $eventDetailsPageUrl = $pageManager->getFullPageCleanUrlByPageId($detailsPageId);
                }
            }

            $categories = $this->getCategoriesByContentId($contentId);
            if ($categories)
            {
                foreach ($categories as $category)
                {
                    $eventCategoriesOptions[$category->category_id] = $category->name;
                }
            }

            $select = $sql->select();
            $select->from(array('bel' => self::BLOCK_EVENT_LIST_TABLE_NAME))
                ->join(
                    array('belc' => self::BLOCK_EVENT_LIST_CATEGORIES_TABLE_NAME),
                    'belc.category_id = bel.category_id',
                    array('category_name' => 'name'),
                    $select::JOIN_LEFT
                )
                ->join(
                    array('u' => 'users'),
                    'u.user_id = bel.last_update_author_id',
                    array('last_update_author' => 'login'),
                    $select::JOIN_LEFT
                )
                ->where(
                    array(
                        'bel.content_id' => $contentId,
                        'bel.is_published' => 1,
                        'belc.content_id' => $contentId,
                        'belc.is_published' => 1
                    )
                )
                ->where->lessThan('bel.event_date_end', new \Zend\Db\Sql\Expression('NOW()'));

            if ($eventListCategoryId > 0)
            {
                $select->where->equalTo('bel.category_id', $eventListCategoryId);
            }

            $select->order('bel.is_promoted DESC, bel.event_date_end DESC, bel.event_date_start DESC');

            $resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet(
                new \Site\Block\EventList\EventHydrator(
                    $this->getTranslateHelper(), $this->attachmentManager, $eventListBlock
                ),
                new \Site\Custom\FlexibleContainer()
            );
            $paginatorAdapter = new \Zend\Paginator\Adapter\DbSelect($select, $dbAdapter, $resultSetPrototype);
            $paginator = new \Zend\Paginator\Paginator($paginatorAdapter);
            $paginator->setCurrentPageNumber($pageNumber);
            $paginator->setItemCountPerPage($itemCountPerPage);
        }

        $this->blockView->setVariables(
            array(
                //'maxLengths' => $maxLengths,
                'eventListPageBlockId' => $eventListPageBlockId,
                'eventDetailsPageUrl' => $eventDetailsPageUrl,
                'eventCategoriesOptions' => $eventCategoriesOptions,
                'eventPaginator' => $paginator
            )
        );
    }

    /**
     * Getters.
     */

    public function getCategoriesByContentId($contentId)
    {
        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_EVENT_LIST_CATEGORIES_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);

        return $resultSet;
    }

    public function getEventByEventCleanUrlAndContentId($eventCleanUrl, $contentId)
    {
        $eventCleanUrl = trim((string)$eventCleanUrl);
        $contentId = (int)$contentId;

        if (($eventCleanUrl == '') || ($contentId < 0))
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select->from(self::BLOCK_EVENT_LIST_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'content_id' => $contentId,
                    'clean_url' => $eventCleanUrl
                )
            );

        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return  $resultSet->current();
        }

        return false;
    }

    public function getEventCleanUrlAndContentIdFromEventCleanUrlWithContentId($eventCleanUrlWithContentId)
    {
        $eventCleanUrl = $eventCleanUrlWithContentId;
        $contentId = null;

        $characterPosition = mb_strrpos($eventCleanUrlWithContentId, '-');
        if ($characterPosition !== false)
        {
            $totalLength = mb_strlen($eventCleanUrlWithContentId);
            $eventCleanUrl = mb_substr($eventCleanUrlWithContentId, 0, $characterPosition);
            $contentId = mb_substr($eventCleanUrlWithContentId, -($totalLength - $characterPosition - 1));
        }

        return array($eventCleanUrl, $contentId);
    }

    public function getEventCleanUrlAndContentIdFromRequest(Request $request)
    {
        $parameters = $this->getParametersForCurrentPageFromRequest($request);
        $eventCleanUrlWithContentId = $parameters->key(); // Get name of the first parameter.

        return $this->getEventCleanUrlAndContentIdFromEventCleanUrlWithContentId($eventCleanUrlWithContentId);
    }
}
<?php
namespace Site\Block;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Http\Request;
use Site\Controller\AttachmentManager;
use Site\Controller\BlockManager;
use Site\Controller\LanguageManager;
use Site\Controller\PageManager;
use Site\Controller\UploadManager;
use Site\Custom\FlexibleContainer;

abstract class AbstractBlock
{
    protected $serviceLocator = null;
    protected $attachmentManager = null;
    protected $blockManager = null;
    protected $cmsConfig = null;
    protected $cutTextHelperInstance = null;
    protected $databaseManager = null;
    protected $escapeForHtmlHelperInstance = null;
    protected $fileSystem = null;
    protected $imageManipulator = null;
    protected $languageManager = null;
    protected $mailManager = null;
    protected $pageManager = null;
    protected $uploadManager = null;
    protected $translateHelperInstance = null;
    protected $routeParameters = null;
    protected $parametersForCurrentPage = null;
    protected $contentId = null;
    protected $settings = null;
    protected $systemSettings = null;
    protected $overwrittenSettings = null;
    protected $blockData = null;
    protected $user = null;
    protected $language = null;
    protected $userLanguage = null;
    protected $phpRenderer = null;
    protected $designMode = false;
    protected $blockViewName = '';
    protected $frameView = null;
    protected $blockView = null;
    protected $windowView = null;
    protected $settingsView = null;
    protected $dynamicBlockSettingsView = null;
    protected $managementView = null;
    protected $addEditView = null;
    protected $viewToRender = null;
    protected $renderWillBeReturned = false;
    protected $renderingEnabled = true;
    protected $hasOwnSettings = false;
    protected $hasOwnDynamicBlockSettings = false;
    protected $hasManagementWindow = false;

    public function __construct(
        ServiceLocatorInterface $serviceLocator,
        LanguageManager $languageManager,
        PageManager $pageManager,
        BlockManager $blockManager,
        AttachmentManager $attachmentManager,
        UploadManager $uploadManager,
        $blockData,
        $user,
        $language,
        FlexibleContainer $userLanguage
    )
    {
        $this->serviceLocator = $serviceLocator;
        $this->languageManager = $languageManager;
        $this->pageManager = $pageManager;
        $this->blockManager = $blockManager;
        $this->attachmentManager = $attachmentManager;
        $this->uploadManager = $uploadManager;
        $this->blockData = $blockData;
        $this->user = $user;
        $this->language = $language;
        $this->userLanguage = $userLanguage;
        $this->phpRenderer = $this->serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
        $this->designMode = $this->user->session->designMode;

        $className = basename(str_replace('\\', '/', get_class($this)));
        $filter = new \Zend\Filter\Word\CamelCaseToDash();
        $this->blockViewName = mb_strtolower($filter->filter($className));

        $this->callBlockMethodIfItExists('init');
    }

    public function getBlockData()
    {
        return $this->blockData;
    }

    public function isRenderingEnabled()
    {
        return $this->renderingEnabled || $this->designMode;
    }

    public function setRenderingEnabled($enabled = true)
    {
        $enabled = $enabled ? true : false;
        $this->renderingEnabled = $enabled;

        return ($this->isRenderingEnabled() == $enabled); // Return info about the change was applied or not.
    }

    public function render(Request $request, $return = false, $additionalBlockViewData = array())
    {
        $settings = $this->getSettings();

        $this->blockView = new \Zend\View\Model\ViewModel();
        $this->blockView->setTemplate('blocks/' . $this->blockViewName . '/' . $this->blockViewName . '.phtml');
        $this->blockView->setVariables(
            array(
                'user' => $this->user,
                'language' => $this->language,
                'userLanguage' => $this->userLanguage,
                'designMode' => $this->designMode,
                'blockData' => $this->blockData,
                'hasOwnSettings' => $this->hasOwnSettings,
                'hasManagementWindow' => $this->hasManagementWindow,
                'settings' => $settings
            )
        );
        $this->blockView->setVariables($additionalBlockViewData);
        $this->viewToRender = $this->blockView;

        $blockNameForCss = 'block-' . $this->convertPhpClassNameToHyphenNotation($this->blockData->php_class_name);

        $this->frameView = null;
        //if ($this->designMode)
        //{
        $this->frameView = new \Zend\View\Model\ViewModel();
        $this->frameView->setTemplate('blocks/frame.phtml');
        $this->frameView->setVariables(
            array(
                'user' => $this->user,
                'language' => $this->language,
                'userLanguage' => $this->userLanguage,
                'designMode' => $this->designMode,
                'blockData' => $this->blockData,
                'blockContent' => '',
                'blockNameForCss' => $blockNameForCss,
                'hasOwnSettings' => $this->hasOwnSettings,
                'hasManagementWindow' => $this->hasManagementWindow,
                'settings' => $settings
            )
        );

        $this->viewToRender = $this->frameView;
        //}

        $this->renderWillBeReturned = $return;

        // Call render function from child of this class:
        $this->callBlockMethodIfItExists('childRender', $request);

        $blockHtml = '';
        if ($this->isRenderingEnabled())
        {
            // Render views:
            if ($this->frameView != null)
            {
                $blockContent = '';
                if ($this->blockView instanceof \Zend\View\Model\ModelInterface)
                {
                    $blockContent = $this->phpRenderer->render($this->blockView);
                }

                $this->frameView->setVariables(
                    array(
                        'blockContent' => $blockContent
                    )
                );
            }

            if ($this->viewToRender instanceof \Zend\View\Model\ModelInterface)
            {
                $blockHtml = $this->phpRenderer->render($this->viewToRender);
            }
        }

        if (!$return)
        {
            echo $blockHtml;
        }
        else
        {
            return $blockHtml;
        }
    }

    public function renderManagement(Request $request, $return = false)
    {
        if (!$this->designMode)
        {
            return false;
        }

        $settings = $this->getSettings();
        $translate = $this->getTranslateHelper();

        $this->managementView = null;
        $managementWndHtml = $translate(
            'This block does not have management.phtml file to include inside this window',
            'default', $this->userLanguage->zend2_locale
        );

        // If management view specific for this block exists then render it and append.
        $pageBlockManagementViewFilePath = 'blocks/' . $this->blockViewName . '/management.phtml';
        if (is_file('module/Site/view/' . $pageBlockManagementViewFilePath))
        {
            $pageNumber = (int)$request->getPost('pageNumber') ? : 1;
            $itemCountPerPage = (int)$request->getPost('itemCountPerPage') ? : 10;
            $includePagesFrom = $request->getPost('includePagesFrom') ? : 'current-language';
            $additionalData = $request->getPost('additionalData');
            $additionalData = new FlexibleContainer($additionalData);

            $this->managementView = new \Zend\View\Model\ViewModel();
            $this->managementView->setTemplate($pageBlockManagementViewFilePath);
            $this->managementView->setVariables(
                array(
                    'user' => $this->user,
                    'language' => $this->language,
                    'userLanguage' => $this->userLanguage,
                    'designMode' => $this->designMode,
                    'blockData' => $this->blockData,
                    'hasOwnSettings' => $this->hasOwnSettings,
                    'settings' => $settings,
                    'pageNumber' => $pageNumber,
                    'itemCountPerPage' => $itemCountPerPage,
                    'includePagesFrom' => $includePagesFrom,
                    'additionalData' => $additionalData
                )
            );
        }

        $this->windowView = null;

        // Call render function from child of this class:
        //$this->childRenderManagement($request);
        $this->callBlockMethodIfItExists('childRenderManagement', $request);

        if ($this->isRenderingEnabled())
        {
            // Render views:
            if ($this->managementView instanceof \Zend\View\Model\ModelInterface)
            {
                $managementWndHtml = $this->phpRenderer->render($this->managementView);
            }
        }

        if (!$return)
        {
            echo $managementWndHtml;
        }
        else
        {
            return $managementWndHtml;
        }
    }

    public function renderAddEdit(Request $request, $return = false)
    {
        if (!$this->designMode)
        {
            return false;
        }

        $settings = $this->getSettings();

        $this->addEditView = null;
        $addEditWndHtml = 'This block does not have add-edit.phtml file to include inside this window!';

        // If add-edit view specific for this block exists then render it and append.
        $pageBlockAddEditViewFilePath = 'blocks/' . $this->blockViewName . '/add-edit.phtml';
        if (is_file('module/Site/view/' . $pageBlockAddEditViewFilePath))
        {
            $pageNumber = (int)$request->getPost('pageNumber') ? : 1;
            $itemCountPerPage = (int)$request->getPost('itemCountPerPage') ? : 10;
            $includePagesFrom = $request->getPost('includePagesFrom') ? : 'current-language';

            $this->addEditView = new \Zend\View\Model\ViewModel();
            $this->addEditView->setTemplate($pageBlockAddEditViewFilePath);
            $this->addEditView->setVariables(
                array(
                    'user' => $this->user,
                    'language' => $this->language,
                    'userLanguage' => $this->userLanguage,
                    'designMode' => $this->designMode,
                    'blockData' => $this->blockData,
                    'hasOwnSettings' => $this->hasOwnSettings,
                    'settings' => $settings,
                    'pageNumber' => $pageNumber,
                    'itemCountPerPage' => $itemCountPerPage,
                    'includePagesFrom' => $includePagesFrom
                )
            );
        }

        $this->windowView = null;

        // Call render function from child of this class:
        //$this->childRenderAddEdit($request);
        $this->callBlockMethodIfItExists('childRenderAddEdit', $request);

        if ($this->isRenderingEnabled())
        {
            // Render views:
            if ($this->addEditView instanceof \Zend\View\Model\ModelInterface)
            {
                $addEditWndHtml = $this->phpRenderer->render($this->addEditView);
            }
        }

        if (!$return)
        {
            echo $addEditWndHtml;
        }
        else
        {
            return $addEditWndHtml;
        }
    }

    public function renderSettings($return = false)
    {
        if (!$this->designMode)
        {
            return false;
        }

        $settings = $this->getSettings();
        $basicVariables = new FlexibleContainer(
            array(
                'user' => $this->user,
                'language' => $this->language,
                'userLanguage' => $this->userLanguage,
                'designMode' => $this->designMode,
                'blockData' => $this->blockData,
                'hasOwnSettings' => $this->hasOwnSettings,
                'hasManagementWindow' => $this->hasManagementWindow,
                'settings' => $settings
            )
        );
        if ($this->blockData->is_dynamic)
        {
            $blockOccurrencesInPages = $this->getBlockOccurrencesInPages();
        }

        // Render default settings for all type of blocks.
        $sharedSettingsView = new \Zend\View\Model\ViewModel();
        $sharedSettingsView->setTemplate('blocks/block-settings.phtml');
        $sharedSettingsView->setVariables($basicVariables);
        $sharedSettingsViewHtml = $this->phpRenderer->render($sharedSettingsView);

        // If this is a dynamic block, then render default settings for dynamic blocks.
        $dynamicBlockSharedSettingsView = null;
        $dynamicBlockSharedSettingsViewHtml = '';
        if ($this->blockData->is_dynamic)
        {
            $dynamicBlockSharedSettingsView = new \Zend\View\Model\ViewModel();
            $dynamicBlockSharedSettingsView->setTemplate('blocks/dynamic-block-settings.phtml');
            $dynamicBlockSharedSettingsView->setVariables($basicVariables);
            $dynamicBlockSharedSettingsView->setVariables(
                array(
                    'blockOccurrencesInPages' => $blockOccurrencesInPages
                )
            );
            $dynamicBlockSharedSettingsViewHtml = $this->phpRenderer->render($dynamicBlockSharedSettingsView);
        }

        $this->settingsView = null;
        $this->dynamicBlockSettingsView = null;

        // If settings view specific for this block exists then render it and append.
        $blockSettingsViewFilePath = 'blocks/' . $this->blockViewName . '/settings.phtml';
        if ($this->hasOwnSettings && is_file('module/Site/view/' . $blockSettingsViewFilePath))
        {
            $this->settingsView = new \Zend\View\Model\ViewModel();
            $this->settingsView->setTemplate($blockSettingsViewFilePath);
            $this->settingsView->setVariables($basicVariables);
        }

        // If this is a dynamic block and settings view specific for this block exists then render it and append.
        if ($this->blockData->is_dynamic)
        {
            $dynamicBlockSettingsViewFilePath = 'blocks/' . $this->blockViewName . '/dynamic-block-settings.phtml';
            if ($this->hasOwnDynamicBlockSettings && is_file('module/Site/view/' . $dynamicBlockSettingsViewFilePath))
            {
                $this->dynamicBlockSettingsView = new \Zend\View\Model\ViewModel();
                $this->dynamicBlockSettingsView->setTemplate($dynamicBlockSettingsViewFilePath);
                $this->dynamicBlockSettingsView->setVariables($basicVariables);
                $this->dynamicBlockSettingsView->setVariables(
                    array(
                        'blockOccurrencesInPages' => $blockOccurrencesInPages
                    )
                );
            }
        }

        $this->windowView = new \Zend\View\Model\ViewModel();
        $this->windowView->setTemplate('blocks/window.phtml');
        $this->windowView->setVariables(
            array(
                'blockData' => $this->blockData
            )
        );

        // Call render functions from child of this class:
        $this->callBlockMethodIfItExists('childRenderSettings');
        $this->callBlockMethodIfItExists('childRenderDynamicBlockSettings');

        // Window view contents HTML:
        $windowViewContentsHtml = $sharedSettingsViewHtml;
        if ($this->settingsView instanceof \Zend\View\Model\ModelInterface)
        {
            $windowViewContentsHtml .= $this->phpRenderer->render($this->settingsView);
        }
        $windowViewContentsHtml .= $dynamicBlockSharedSettingsViewHtml;
        if ($this->dynamicBlockSettingsView instanceof \Zend\View\Model\ModelInterface)
        {
            $windowViewContentsHtml .= $this->phpRenderer->render($this->dynamicBlockSettingsView);
        }

        $this->windowView->setVariables(
            array(
                'contents' => $windowViewContentsHtml
            )
        );

        $settingsWndHtml = $this->phpRenderer->render($this->windowView);

        if (!$return)
        {
            echo $settingsWndHtml;
        }
        else
        {
            return $settingsWndHtml;
        }
    }

    public function saveContent($externalContent = null)
    {
        $content = ($externalContent !== null) ? $externalContent : $this->blockData->content->content;

        if ($content !== null) // Empty content should be '', but not === null.
        {
            $pageBlockId = (int)$this->blockData->page_block_id;
            $contentId = $this->blockManager->getPageBlockContentIdByPageBlockId($pageBlockId);
            if ($contentId !== false)
            {
                $databaseManager = $this->getDatabaseManager();
                if ($databaseManager)
                {
                    $saved = false;

                    $databaseManager->beginTransaction();

                    $update = $databaseManager->getSql()->update('pages_blocks_contents');
                    $update
                        ->set(array('content' => (string)$content))
                        ->where(array('content_id' => (int)$contentId));
                    $result = $databaseManager->executePreparedStatement($update);
                    if ($result)
                    {
                        $pageBlockContents = $this->blockManager->getPageBlockContentsByPageBlockId($pageBlockId);

                        $saved = ($pageBlockContents->content === $content);
                    }

                    if ($saved)
                    {
                        $this->updateBlockRecordInSearchIndex(
                            null, '', 'Static content', false, $content
                        );

                        $databaseManager->commit();
                    }
                    else
                    {
                        $databaseManager->rollback();
                    }

                    return $saved;
                }
            }
        }

        return false;
    }

    protected function addEventToSystemHistory($message, $variables = false)
    {
        $message = trim((string)$message);

        if ($message == '')
        {
            return false;
        }

        $sessionData = $this->user;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $message = preg_replace_callback(
            '|{{userLogin}}|',
            function ($matches) use ($sessionData) {
                if ($sessionData && ($sessionData->offsetExists('user_id')))
                {
                    if ($sessionData->user_id > 0)
                    {
                        return '<strong>' . $sessionData->login . '</strong>';
                    }
                    else
                    {
                        return '<em>guest</em>';
                    }
                }

                return 'unknown user';
            },
            $message
        );

        if ($variables && (is_array($variables) || ($variables instanceof \Traversable)))
        {
            $message = preg_replace_callback(
                '|{{([a-zA-Z0-9_]+)}}|',
                function ($matches) use (&$variables) {
                    $variableName = $matches[1];
                    if (array_key_exists($variableName, $variables))
                    {
                        return $variables[$variableName];
                    }

                    return $matches;
                },
                $message
            );
        }

        $event_date = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager->beginTransaction();

        $insert = $sql->insert('system_history');
        $insert
            ->columns(
                array(
                    'event_date',
                    'message'
                )
            )
            ->values(
                array(
                    'event_date' => $event_date,
                    'message' => $message
                )
            );
        $eventId = $dbManager->executeInsertAndGetLastInsertId($insert);
        if ($eventId > 0)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $eventId;
    }

    protected function deleteBlockRecordFromSearchIndex($elementId = null)
    {
        $elementId = (int)$elementId ? : null;
        if (($elementId !== null) && ($elementId <= 0))
        {
            return false;
        }

        $contentId = $this->getContentId();
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $delete = $sql->delete();
        $delete
            ->from('search_index')
            ->where(
                array(
                    'content_id' => $contentId,
                    'block_element_id' => $elementId,
                )
            );

        $result = $dbManager->executePreparedStatement($delete);

        return $result ? true : false;
    }

    protected function updateBlockRecordInSearchIndex(
        $elementId, $elementContents, $elementUrlParametersString, $translatableSearchResultCategory,
        $date = false, $contents = false, $otherBlockData = false
    )
    {
        $elementId = (int)$elementId ? : null;
        $elementContents = trim((string)$elementContents);
        $elementUrlParametersString = trim((string)$elementUrlParametersString);
        $translatableSearchResultCategory = trim(strip_tags((string)$translatableSearchResultCategory));

        if (
            (($elementId !== null) && ($elementId <= 0)) ||
            (($elementId !== null) && ($elementContents == '')) ||
            ($translatableSearchResultCategory == '') ||
            (($elementId === null) && ($contents == ''))
        )
        {
            return false;
        }

        if (!$otherBlockData)
        {
            $blockData = $this->getBlockData();
        }
        else
        {
            $blockData = $otherBlockData;
        }

        $dbManager = $this->getDatabaseManager();
        $dbManager->beginTransaction();

        if ($elementId > 0)
        {
            $contentId = $blockData->content->content_id;

            $resultId = $this->getSearchIndexRecordIdByBlockElementIdAndContentId($elementId, $contentId);
            if ($resultId)
            {
                $result = $this->updateRecordInSearchIndex(
                    $resultId, $elementContents, $elementUrlParametersString, $translatableSearchResultCategory,
                    $date, $contents
                );
            }
            else
            {
                $result = $this->insertRecordIntoSearchIndex(
                    $elementId, $elementContents, $elementUrlParametersString, $translatableSearchResultCategory,
                    $date, $contents
                );
            }
        }
        else
        {
            $blockData = $this->getBlockData();

            $resultId = $this->getSearchIndexRecordIdByPageIdAndContentId(
                $blockData->page_id, $blockData->content->content_id
            );
            if ($resultId)
            {
                $result = $this->updateRecordInSearchIndex(
                    $resultId, $elementContents, $elementUrlParametersString, $translatableSearchResultCategory,
                    $date, $contents
                );
            }
            else
            {
                $result = $this->insertRecordIntoSearchIndex(
                    $elementId, $elementContents, $elementUrlParametersString, $translatableSearchResultCategory,
                    $date, $contents
                );
            }
        }

        if ($result)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $result;
    }

    public function escapeDiacritics($text)
    {
        $text = (string)$text;
        if ($text == '')
        {
            return '';
        }

        $fromReplace = array(
            'ą',
            'ć',
            'ę',
            'ł',
            'ń',
            'ó',
            'ś',
            'ż',
            'ź',
            'Ą',
            'Ć',
            'Ę',
            'Ł',
            'Ń',
            'Ó',
            'Ś',
            'Ż',
            'Ź'
        );
        $toReplace = array(
            'a',
            'c',
            'e',
            'l',
            'n',
            'o',
            's',
            'z',
            'z',
            'A',
            'C',
            'E',
            'L',
            'N',
            'O',
            'S',
            'Z',
            'Z'
        );

        return str_replace($fromReplace, $toReplace, $text);
    }

    public function generateCleanUrlFromText($text)
    {
        $text = trim((string)$text);
        if ($text == '')
        {
            return false;
        }

        $cleanUrl = '';

        $allowedChars = array(
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            'a',
            'b',
            'c',
            'd',
            'e',
            'f',
            'g',
            'h',
            'i',
            'j',
            'k',
            'l',
            'm',
            'n',
            'o',
            'p',
            'q',
            'r',
            's',
            't',
            'u',
            'v',
            'w',
            'x',
            'y',
            'z',
            '-',
            '!',
            '$'
        );

        $source = mb_strtolower($this->escapeDiacritics($text));
        $sourceLength = mb_strlen($source);

        for ($i = 0; $i < $sourceLength; $i++)
        {
            $character = mb_substr($source, $i, 1);

            if (in_array($character, $allowedChars))
            {
                $cleanUrl .= $character;
            }
            else
            {
                $cleanUrl .= '-';
            }
        }

        $cleanUrl = trim(trim($cleanUrl), '-');

        do
        {
            $oldCleanUrl = $cleanUrl;
            $cleanUrl = str_replace('--', '-', $oldCleanUrl);
        }
        while ($cleanUrl != $oldCleanUrl);

        return $cleanUrl;
    }

    protected function getContentId()
    {
        if ($this->contentId == null)
        {
            $blockManager = $this->getBlockManager();
            $contentId = $blockManager->getPageBlockContentIdByPageBlockId($this->blockData->page_block_id);
            if (!$contentId)
            {
                throw new \Site\Exception\CmsException(__CLASS__ . ' block (id: ' . $this->blockData->page_block_id . ') has not associated contents.');
            }

            $this->contentId = $contentId;
        }

        return $this->contentId;
    }

    /**
     * This method returns block's settings.
     * If it is a dynamic block then these settings should be recognized as shared settings (between multiple
     * block instances).
     * @return null|FlexibleContainer
     */
    protected function getNotSharedSettings()
    {
        if (!$this->settings)
        {
            $this->settings = new FlexibleContainer();
            $xmlString = trim($this->blockData->content->config);
            $xmlConfig = simplexml_load_string($xmlString, '\Site\Custom\SimpleXmlElementExtended', LIBXML_NOCDATA);
            if ($xmlConfig)
            {
                foreach ($xmlConfig as $name => $value)
                {
                    $this->settings[$name] = (string)$value;
                }
            }
        }

        return $this->settings;
    }

    /**
     * This method returns block's overwritten settings (specific to it's instance, not shared).
     * If the block is not dynamic then returned value will be exactly the same as from getSettings() method.
     * @return null|FlexibleContainer
     */
    protected function getSettings()
    {
        $notSharedSettings = $this->getNotSharedSettings();

        if (!$this->blockData->is_dynamic)
        {
            return $notSharedSettings;
        }

        if (!$this->overwrittenSettings)
        {
            $this->overwrittenSettings = new FlexibleContainer();
            $xmlString = trim($this->blockData->config);
            $xmlConfig = simplexml_load_string($xmlString, '\Site\Custom\SimpleXmlElementExtended', LIBXML_NOCDATA);
            if ($xmlConfig)
            {
                foreach ($xmlConfig as $name => $value)
                {
                    $this->overwrittenSettings[$name] = (string)$value;
                }
            }

            $this->overwrittenSettings = new FlexibleContainer(array_merge(
                $notSharedSettings->toArray(), $this->overwrittenSettings->toArray()
            ));
        }

        return $this->overwrittenSettings;
    }

    protected function getSystemSettings()
    {
        if ($this->systemSettings === null)
        {
            $prefixColumnsWithTable = false;

            $dbManager = $this->getDatabaseManager();
            $sql = $dbManager->getSql();

            $select = $sql->select();
            $select
                ->from('settings')
                ->columns(array($select::SQL_STAR), $prefixColumnsWithTable);
            $resultSet = $dbManager->getHydratedResultSet($select);
            if ($resultSet)
            {
                $this->systemSettings = array();

                foreach ($resultSet as $setting)
                {
                    $name = $setting->name;
                    unset($setting->name);

                    $this->systemSettings[$name] = $setting;
                }

                $this->systemSettings = new FlexibleContainer($this->systemSettings);
            }
        }

        return $this->systemSettings;
    }

    protected function getSystemSettingByName($name)
    {
        $name = trim((string)$name);
        if ($name == '')
        {
            return false;
        }

        if ($this->systemSettings === null)
        {
            $this->getSystemSettings();
        }

        if (($this->systemSettings != null) && ($this->systemSettings->offsetExists($name)))
        {
            return $this->systemSettings[$name];
        }

        return false;
    }

    protected function getSystemSettingValueByName($name)
    {
        $setting = $this->getSystemSettingByName($name);
        if ($setting)
        {
            return $setting->value;
        }

        return false;
    }

    protected function getAttachmentManager()
    {
        return $this->attachmentManager;
    }

    protected function getBlockManager()
    {
        return $this->blockManager;
    }

    protected function getCmsConfig()
    {
        if ($this->cmsConfig === null)
        {
            $config = $this->serviceLocator->get('config');
            if (array_key_exists('cms', $config))
            {
                $this->cmsConfig = new FlexibleContainer($config['cms']);
            }
        }

        return $this->cmsConfig;
    }

    protected function getCutTextHelper()
    {
        if ($this->cutTextHelperInstance == null)
        {
            $this->cutTextHelperInstance = $this->serviceLocator->get('viewhelpermanager')->get('cutText');
        }

        return $this->cutTextHelperInstance;
    }

    protected function getDatabaseManager()
    {
        if ($this->databaseManager == null)
        {
            $this->databaseManager = $this->serviceLocator->get('DatabaseManager');
        }

        return $this->databaseManager;
    }

    protected function getEscapeForHtmlHelper()
    {
        if ($this->escapeForHtmlHelperInstance == null)
        {
            $this->escapeForHtmlHelperInstance = $this->serviceLocator->get('viewhelpermanager')->get('escapeForHtml');
        }

        return $this->escapeForHtmlHelperInstance;
    }

    protected function getFileSystem()
    {
        if ($this->fileSystem == null)
        {
            $this->fileSystem = new \FileSystem\Component\FileSystem();
        }

        return $this->fileSystem;
    }

    protected function getImageManipulator()
    {
        if ($this->imageManipulator == null)
        {
            $fileSystem = $this->getFileSystem();
            $this->imageManipulator = new \Graphics\Component\ImageController($fileSystem);
        }

        return $this->imageManipulator;
    }

    protected function getLanguageManager()
    {
        return $this->languageManager;
    }

    protected function getMailManager()
    {
        if ($this->mailManager == null)
        {
            $this->mailManager = new \Site\Controller\MailManager(
                $this->getCmsConfig(), $this->serviceLocator,
                $this->language, $this->userLanguage, $this->user
            );
        }

        return clone $this->mailManager;
    }

    protected function getPageManager()
    {
        return $this->pageManager;
    }

    protected function getRefererFromRequest(Request $request)
    {
        return $request->getHeader('Referer');
    }

    protected function getRefererPathFromRequest(Request $request)
    {
        $refererObject = $this->getRefererFromRequest($request);
        if ($refererObject)
        {
            return $refererObject->uri()->getPath();
        }

        return '';
    }

    protected function getTranslateHelper()
    {
        if ($this->translateHelperInstance == null)
        {
            $this->translateHelperInstance = $this->serviceLocator->get('viewhelpermanager')->get('translate');
        }

        return $this->translateHelperInstance;
    }

    protected function getSubPageCleanUrlFromRequest(Request $request)
    {
        return $this->getRouteParametersFromRequest($request)->subPage;
    }

    protected function getParametersForCurrentPageFromRequest(Request $request)
    {
        if ($this->parametersForCurrentPage === null)
        {
            $this->parametersForCurrentPage = new FlexibleContainer();

            $parametersString = $this->getRouteParametersFromRequest($request)->parameters;
            $parametersParts = explode(',', $parametersString);
            $parameterPartCount = count($parametersParts);
            if ($parameterPartCount > 0)
            {
                $parameterName = null;
                for ($i = 0; $i < $parameterPartCount; $i++)
                {
                    if ($i % 2 == 0)
                    {
                        $parameterName = $parametersParts[$i];
                    }
                    else
                    {
                        $parameterValue = $parametersParts[$i];
                        $this->parametersForCurrentPage[$parameterName] = $parameterValue;
                    }
                }

                if ($i % 2 != 0)
                {
                    // If the number of parameters is odd, it means that the last parameter has no value, so we
                    // must add empty string as it's value.

                    $this->parametersForCurrentPage[$parameterName] = '';
                }
            }
        }

        return $this->parametersForCurrentPage;
    }

    protected function getUrlInfoPartsFromUrl($url)
    {
        $url = trim($url);
        if ($url == '')
        {
            return false;
        }

        $urlInfoParts = parse_url($url);
        if ($urlInfoParts === false)
        {
            return false;
        }

        $urlInfoParts = new FlexibleContainer($urlInfoParts);

        return $urlInfoParts;
    }

    protected function getUrlPathFromUrlInfoParts($urlInfoParts)
    {
        if (!$urlInfoParts || !is_object($urlInfoParts))
        {
            return false;
        }

        return $urlInfoParts->path;
    }

    protected function getUrlPathFromUrl($url)
    {
        $urlInfoParts = $this->getUrlInfoPartsFromUrl($url);
        if (!$urlInfoParts)
        {
            return false;
        }

        return $urlInfoParts->path;
    }

    protected function getParametersForCurrentPageFromUrl($url)
    {
        $urlPath = $this->getUrlPathFromUrl($url);

        $parametersForCurrentPage = new FlexibleContainer();

        $parts = explode(',', $urlPath);
        if ($parts)
        {
            array_shift($parts); // Remove full page clean URL from array.
            $partCount = count($parts);

            $parameterName = null;
            for ($i = 0; $i < $partCount; $i++)
            {
                if ($i % 2 == 0)
                {
                    $parameterName = $parts[$i];
                }
                else
                {
                    $parameterValue = $parts[$i];
                    $parametersForCurrentPage[$parameterName] = $parameterValue;
                }
            }

            if ($i % 2 != 0)
            {
                // If the number of parameters is odd, it means that the last parameter has no value, so we
                // must add empty string as it's value.

                $parametersForCurrentPage[$parameterName] = '';
            }
        }

        return $parametersForCurrentPage;
    }

    protected function getRouteParametersFromRequest(Request $request)
    {
        if ($this->routeParameters === null)
        {
            $router = $this->serviceLocator->get('Router');
            $routeMatch = $router->match($request);
            $this->routeParameters = new FlexibleContainer($routeMatch->getParams());
        }

        return $this->routeParameters;
    }

    protected function getBlockOccurrencesInPages()
    {
        $occurrences = new FlexibleContainer();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->quantifier('DISTINCT')
            ->from(array('p' => 'pages'))
            ->join(
                array('pb' => 'pages_blocks'),
                'pb.page_id = p.page_id',
                array(
                    'block_id' => 'block_id',
                    'slot_number' => 'slot_number'
                ), 'LEFT OUTER'
            )
            ->order('p.title ASC');
        $resultSet = $dbManager->getHydratedResultSet($select);

        if ($resultSet)
        {
            $languageManager = $this->getLanguageManager();
            $pageManager = $this->getPageManager();

            foreach ($resultSet as $row)
            {
                $isBlockAddedToSlotOnThisPage = ($row->block_id == $this->blockData->block_id) &&
                                                ($row->slot_number == $this->blockData->slot_number);

                if (!isset($occurrences[$row->page_id]))
                {
                    $page = $pageManager->getPageById($row->page_id);
                    $language = $languageManager->getLanguageByLanguageId($page->language_id);

                    $pageUrl = $pageManager->getFullPageCleanUrlByLanguageIsoCodeAndPage(
                        $language->iso_code, $page
                    );

                    $info = new \stdClass();
                    $info->page_language = $language->english_name;
                    $info->page_title = $row->title;
                    $info->page_url = $pageUrl;
                    $info->is_block_added_to_slot_on_this_page = $isBlockAddedToSlotOnThisPage;
                    $info->slot_number = $this->blockData->slot_number;

                    $occurrences[$row->page_id] = $info;
                }
                else if ($isBlockAddedToSlotOnThisPage)
                {
                    // Change value of this variable only from false -> true.
                    // Do not change this in the opposite way.

                    $occurrences[$row->page_id]->is_block_added_to_slot_on_this_page = true;
                }
            }
        }

        return $occurrences;
    }

    protected function convertPhpClassNameToHyphenNotation($phpClassName)
    {
        $phpClassName = trim((string)$phpClassName);

        if ($phpClassName == '')
        {
            return '';
        }

        return ltrim(mb_strtolower(preg_replace('/[A-Z]/', '-$0', $phpClassName)), '-');
    }

    protected function convertToFlexibleContainer($arrayOrObject)
    {
        if (!$arrayOrObject instanceof FlexibleContainer)
        {
            return new FlexibleContainer($arrayOrObject);
        }

        return $arrayOrObject;
    }

    private function callBlockMethodIfItExists($methodName, $parameters = array())
    {
        if (($methodName == '') || !method_exists($this, $methodName))
        {
            return false;
        }

        //return call_user_func_array(array(&$this, $methodName), $parameters);
        return $this->$methodName($parameters);
    }

    private function getSearchIndexRecordByResultId($resultId)
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from('search_index')
            ->where(
                array(
                    'result_id' => $resultId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    private function getSearchIndexRecordIdByBlockElementIdAndContentId($elementId, $contentId)
    {
        $elementId = (int)$elementId;
        $contentId = (int)$contentId;

        if (($elementId <= 0) && ($contentId <= 0))
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->columns(array('result_id'), $prefixColumnsWithTable)
            ->from('search_index')
            ->where(
                array(
                    'content_id' => $contentId,
                    'block_element_id' => $elementId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();

            return $row ? $row->result_id : false;
        }

        return false;
    }

    private function getSearchIndexRecordIdByPageIdAndContentId($pageId, $contentId)
    {
        $pageId = (int)$pageId;
        $contentId = (int)$contentId;

        if (($pageId <= 0) && ($contentId <= 0))
        {
            return false;
        }

        $prefixColumnsWithTable = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->columns(array('result_id'), $prefixColumnsWithTable)
            ->from('search_index')
            ->where(
                array(
                    'page_id' => $pageId,
                    'content_id' => $contentId
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            $row = $resultSet->current();

            return $row ? $row->result_id : false;
        }

        return false;
    }

    private function insertRecordIntoSearchIndex(
        $elementId, $elementContents, $elementUrlParametersString, $translatableSearchResultCategory, $date, $contents
    )
    {
        $blockData = $this->getBlockData();
        $contentId = $this->getContentId();
        $contents = trim((string)$contents);

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $pageTitle = '';
        $pageMetaDescription = '';
        $page = $this->pageManager->getPageById($blockData->page_id);
        if ($page)
        {
            $pageTitle = $page->title;
            $pageMetaDescription = $page->meta_description;
        }
        unset($page);

        $insert = $sql->insert('search_index');

        if (!$date || ($date == '0000-00-00 00:00:00'))
        {
            $date = null;
        }

        if ($date !== null)
        {
            $insert
                ->columns(
                    array(
                        'page_id',
                        'language_id',
                        'content_id',
                        'block_element_id',
                        'date',
                        'translatable_search_result_category',
                        'block_element_url_parameters',
                        'page_title',
                        'meta_description',
                        'block_element_contents',
                        'block_contents'
                    )
                )
                ->values(
                    array(
                        'page_id' => $blockData->page_id,
                        'language_id' => $this->language->language_id,
                        'content_id' => $contentId,
                        'block_element_id' => $elementId,
                        'date' => $date,
                        'translatable_search_result_category' => $translatableSearchResultCategory,
                        'block_element_url_parameters' => $elementUrlParametersString,
                        'page_title' => $pageTitle,
                        'meta_description' => $pageMetaDescription,
                        'block_element_contents' => $elementContents,
                        'block_contents' => $contents
                    )
                );
        }
        else
        {
            $insert
                ->columns(
                    array(
                        'page_id',
                        'language_id',
                        'content_id',
                        'block_element_id',
                        'translatable_search_result_category',
                        'block_element_url_parameters',
                        'page_title',
                        'meta_description',
                        'block_element_contents',
                        'block_contents'
                    )
                )
                ->values(
                    array(
                        'page_id' => $blockData->page_id,
                        'language_id' => $this->language->language_id,
                        'content_id' => $contentId,
                        'block_element_id' => $elementId,
                        'translatable_search_result_category' => $translatableSearchResultCategory,
                        'block_element_url_parameters' => $elementUrlParametersString,
                        'page_title' => $pageTitle,
                        'meta_description' => $pageMetaDescription,
                        'block_element_contents' => $elementContents,
                        'block_contents' => $contents
                    )
                );
        }

        $resultId = $dbManager->executeInsertAndGetLastInsertId($insert);
        $inserted = ($resultId > 0);

        return $inserted;
    }

    private function updateRecordInSearchIndex(
        $resultId, $elementContents, $elementUrlParametersString, $translatableSearchResultCategory, $date, $contents
    )
    {
        $blockData = $this->getBlockData();
        $contentId = $this->getContentId();

        $updated = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $pageTitle = null;
        $pageMetaDescription = null;
        $page = $this->pageManager->getPageById($blockData->page_id);
        if ($page)
        {
            $pageTitle = $page->title;
            $pageMetaDescription = $page->meta_description;
        }
        unset($page);

        $update = $sql->update('search_index');

        if ($date !== false)
        {
            if (!$date || ($date == '0000-00-00 00:00:00'))
            {
                $date = null;
            }

            $update->set(array('date' => $date));
        }

        $update
            ->set(
                array(
                    'page_id' => $blockData->page_id,
                    'language_id' => $this->language->language_id,
                    'content_id' => $contentId,
                    'translatable_search_result_category' => $translatableSearchResultCategory,
                    'block_element_url_parameters' => $elementUrlParametersString,
                    'page_title' => $pageTitle,
                    'meta_description' => $pageMetaDescription,
                    'block_element_contents' => $elementContents
                )
            );

        if ($contents !== false)
        {
            $contents = trim((string)$contents);
            $update->set(array('block_contents' => $contents));
        }

        $update->where(array('result_id' => $resultId));

        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $record = $this->getSearchIndexRecordByResultId($resultId);
            if (
                $record &&
                ($record->block_element_contents == $elementContents)
            )
            {
                $updated = true;
            }
        }

        return $updated;
    }
}
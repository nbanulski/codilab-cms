<?php
namespace Site\Block\BigBanner;

use Zend\Stdlib\Hydrator\HydratorInterface;
use Site\Controller\AttachmentManager;
use Site\Controller\PageManager;

class BigBannerHydrator implements HydratorInterface
{
    protected $attachmentManager = null;
    protected $bigBanner = null;
    protected $pageManager = null;

    public function __construct(
        AttachmentManager $attachmentManager,
        BigBanner $bigBanner,
        PageManager $pageManager
    )
    {
        $this->attachmentManager = $attachmentManager;
        $this->bigBanner = $bigBanner;
        $this->pageManager = $pageManager;
    }

    public function extract($object) {}

    public function hydrate(array $data, $object)
    {
        foreach ($data as $property => $value)
        {
            $object[$property] = $value;
        }

        $bigBanner = $this->bigBanner;

        if ($object['url_address_type'] == $bigBanner::BANNER_URL_ADDRESS_TYPE_INTERNAL)
        {
            $object['pageUrl'] = $this->pageManager->getFullPageCleanUrlByPageId($object['page_id']);
        }
        else
        {
            $object['pageUrl'] = $object['external_url'];
        }

        $object['pageBreadcrumb'] = $this->pageManager->getPageTextBreadcrumbByPageId($object['page_id']);

        $attachment = $this->attachmentManager->getAttachmentById($object->attachment_id);
        if ($attachment)
        {
            $object['imageUrl'] =
                '/eng/get-file/index,' .
                $attachment->hash_for_file_name .
                ',' .
                $attachment->original_file_name;
        }
        else
        {
            $object['imageUrl'] = '';
        }

        return $object;
    }
} 
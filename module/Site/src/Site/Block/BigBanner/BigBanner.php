<?php
namespace Site\Block\BigBanner;

use Site\Block\AbstractBlock;
use Zend\Http\Request;

class BigBanner extends AbstractBlock
{
    const BANNER_URL_ADDRESS_TYPE_INTERNAL = 0;
    const BANNER_URL_ADDRESS_TYPE_EXTERNAL = 1;
    const BANNER_URL_ADDRESS_TYPE_NONE = 2;

    const BLOCK_BIG_BANNER_TABLE_NAME = 'block_big_banner';

    protected $hasManagementWindow = true;
    protected $hasOwnSettings = true;
    protected $banners = array();

    /**
     * Events.
     */

    public function onBeforeDeleted()
    {
        if (!$this->blockData->is_dynamic)
        {
            // Delete all this block's data from database etc.
            $this->deleteAllBigBannerData();
        }
    }

    /**
     * Render-functions.
     */

    public function childRender()
    {
        $getOnlyPublishedBanners = true;
        $bufferResultSetPrototype = true;
        
        $contentId = $this->getContentId();
        $paginator = $this->getPaginatorOfBannersToRender($getOnlyPublishedBanners,$bufferResultSetPrototype);
        $numberOfBanners = $this->getCountOfAllAssociatedBanners($getOnlyPublishedBanners);
        
        $this->blockView->setVariables(
            array(
                'contentId' => $contentId,
                'numBanners' => $numberOfBanners,
                'bigBannerPaginator' => $paginator
            )
        );
    }

    public function childRenderManagement(Request $request)
    {
        if (!$this->managementView instanceof \Zend\View\Model\ViewModel)
        {
            return false;
        }

        $currentPageNumber = $this->managementView->pageNumber;
        $itemCountPerPage = $this->managementView->itemCountPerPage;        
        $paginator = $this->getPaginatorOfBannersToRender();  
        $paginator->setCurrentPageNumber($currentPageNumber);
        $paginator->setItemCountPerPage($itemCountPerPage);

        $numberOfBanners = $this->getCountOfAllAssociatedBanners();

        $this->managementView->setVariables(
            array(
                'numBanners' => $numberOfBanners,
                'bigBannerPaginator' => $paginator
            )
        );
    }

    public function childRenderAddEdit(Request $request)
    {
        if (!$this->addEditView instanceof \Zend\View\Model\ViewModel)
        {
            return false;
        }

        $pageManager = $this->getPageManager();

        $defaultFormData = array(
            'bannerId' => '',
            'bannerPageId' => null,
            'bannerIsPublished' => 0,
            'bannerIsTitleVisible' => 0,
            'bannerIsContentsVisible' => 0,
            'bannerUrlAddressType' => 0,
            'bannerExternalUrl' => '',
            'bannerTitle' => '',
            'bannerContents' => '',
            'bannerImages' => array(
                0 => array(
                    'imageId' => null,
                    'isMain' => true,
                    'attachmentId' => null,
                    'uniqueHashForFileName' => '',
                    'originalFileName' => '',
                )
            ),
            'bannerUniqueHashForFileName' => '',
            'bannerOriginalFileName' => ''
        );
        $currentBannerFormData = array();

        $bannerId = (int)$request->getPost('bannerId');
        if ($bannerId > 0)
        {
            $banner = $this->getBannerById($bannerId);
            if ($banner)
            {
                $bannerImagesArray = array(
                    0 => array(
                        'imageId' => null,
                        'isMain' => true,
                        'attachmentId' => null,
                        'uniqueHashForFileName' => '',
                        'originalFileName' => '',
                    )
                );

                $attachmentId = $banner->attachment_id;
                $attachment = $this->attachmentManager->getAttachmentById($attachmentId);

                $bannerImagesArray[0] = new \Site\Custom\FlexibleContainer();
                $bannerImagesArray[0]->imageId = false;
                $bannerImagesArray[0]->isMain = true;
                $bannerImagesArray[0]->attachmentId = $attachmentId;
                $bannerImagesArray[0]->uniqueHashForFileName
                    = $attachment ? $attachment->hash_for_file_name : '';
                $bannerImagesArray[0]->originalFileName
                    = $attachment ? $attachment->original_file_name : '';

                $currentBannerFormData = array(
                    'bannerId' => $banner->banner_id,
                    'bannerPageId' => $banner->page_id,
                    'bannerUrlAddressType' => $banner->url_address_type,
                    'bannerExternalUrl' => $banner->external_url,
                    'bannerIsPublished' => $banner->is_published,
                    'bannerIsTitleVisible' => $banner->is_title_visible,
                    'bannerIsContentsVisible' => $banner->is_contents_visible,
                    'bannerTitle' => $banner->title,
                    'bannerContents' => $banner->contents,
                    'bannerImages' => $bannerImagesArray,
                    'bannerUniqueHashForFileName' => $attachment ? $attachment->hash_for_file_name : '',
                    'bannerOriginalFileName' => $attachment ? $attachment->original_file_name : ''
                );
            }
        }

        $formData = new \Site\Custom\FlexibleContainer(array_merge($defaultFormData, $currentBannerFormData));
        $numberOfBanners = $this->getCountOfAllAssociatedBanners();

        $bannerInternalPagesOptions = array();
        $pages = $pageManager->getPages();
        if ($pages)
        {
            foreach ($pages as $page)
            {
                $option = new \stdClass();
                $option->title = $page->title;
                $option->extended_title =
                    $page->title .
                    ' (' . $pageManager->getFullPageCleanUrlByPageId($page->page_id) . ')';
                $option->selected = ($page->page_id == $formData->bannerPageId) ? ' selected' : '';

                $bannerInternalPagesOptions[$page->page_id] = $option;
            }
        }

        $this->addEditView->setVariables(
            array(
                'bannerInternalPagesOptions' => $bannerInternalPagesOptions,
                'formData' => $formData,
                'maxLengths' => 350,
                'numBanners' => $numberOfBanners
            )
        );
    }

    /**
     * Getters.
     */

    public function getPaginatorOfBannersToRender($selectOnlyPublished = false,$bufferResultSetPrototype = false)
    {
        $dbManager = $this->getDatabaseManager();
        $dbAdapter = $dbManager->getAdapter();
        $sql = $dbManager->getSql();
        $select = $sql->select();

        $contentId = $this->getContentId();
        if (!$contentId)
        {
            throw new \Site\Exception\CmsException('BigBannerBlock (id: ' . $this->blockData->page_block_id . ') has no associated content.');
        }
        
        if ($selectOnlyPublished)
        {
            $select
                ->from(array('bbb' => self::BLOCK_BIG_BANNER_TABLE_NAME))
                ->join(
                    array('u' => 'users'),
                    'u.user_id = bbb.last_update_author_id',
                    array('last_update_author' => 'login'),
                    $select::JOIN_LEFT
                )
                ->where(
                    array(
                        'bbb.content_id' => $contentId,
                        'bbb.is_published' => 1
                    )
                )
                ->order('bbb.order ASC');
        }
        else
        {
            $select
                ->from(array('bbb' => self::BLOCK_BIG_BANNER_TABLE_NAME))
                ->join(
                    array('u' => 'users'),
                    'u.user_id = bbb.last_update_author_id',
                    array('last_update_author' => 'login'),
                    $select::JOIN_LEFT
                )
                ->where(
                    array(
                        'bbb.content_id' => $contentId,                        
                    )
                )
                ->order('bbb.order ASC');
        }

        $resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet(
            new BigBannerHydrator($this->attachmentManager, $this, $this->getPageManager()),
            new \Site\Custom\FlexibleContainer()
        );
        if($bufferResultSetPrototype){
            $resultSetPrototype->buffer();
        }
        $paginationAdapter = new \Zend\Paginator\Adapter\DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new \Zend\Paginator\Paginator($paginationAdapter);

        return $paginator;

    }

    public function getCountOfAllAssociatedBanners($onlyPublished = false)
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $contentId = $this->getContentId();

        if ($onlyPublished)
        {
            $select = $sql->select();
            $select
                ->from(self::BLOCK_BIG_BANNER_TABLE_NAME)
                ->where(
                    array(
                        'content_id' => $contentId,
                        'is_published' => 1
                    )
                )
                ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')));
            $resultSet = $dbManager->getHydratedResultSet($select);
        }
        else
        {
            $select = $sql->select();
            $select
                ->from(self::BLOCK_BIG_BANNER_TABLE_NAME)
                ->where(
                    array(
                        'content_id' => $contentId,
                    )
                )
                ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')));
            $resultSet = $dbManager->getHydratedResultSet($select);
        }

        if ($resultSet)
        {
            $row = $resultSet->current();
            $count = $row ? $row->count : false;

            return $count;
        }

        return false;
    }

    public function getBannerById($bannerId)
    {
        $bannerId = (int)$bannerId;
        if ($bannerId <= 0)
        {
            return false;
        }

        $prefixColumnsWithTable = false;
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $select = $sql->select();
        $select
            ->from(self::BLOCK_BIG_BANNER_TABLE_NAME)
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(
                array(
                    'banner_id' => $bannerId,
                    'content_id' => $this->getContentId()
                )
            );
        $resultSet = $dbManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    public function isExternalUrlValid($bannerExternalUrl)
    {
        $bannerExternalUrl = trim(strip_tags((string)$bannerExternalUrl));

        return ($bannerExternalUrl != '');
    }

    /**
     * Data formatting functions.
     */

    public function validateBannerData($bannerData)
    {
        $bannerData = new \Site\Custom\FlexibleContainer($bannerData);
        $bannerDataCopy = clone $bannerData;

        $bannerDataCopy->bannerId = (int)$bannerData->bannerId ? : null;
        $bannerDataCopy->bannerPageId = (int)$bannerData->bannerPageId ? : null;
        $bannerDataCopy->bannerAttachmentId = (int)$bannerData->bannerAttachmentId ? : null;
        $bannerDataCopy->bannerIsPublished = (int)$bannerData->bannerIsPublished ? 1 : 0;
        $bannerDataCopy->bannerIsTitleVisible = (int)$bannerData->bannerIsTitleVisible ? 1 : 0;
        $bannerDataCopy->bannerIsContentsVisible = (int)$bannerData->bannerIsContentsVisible ? 1 : 0;
        $bannerDataCopy->bannerOrder = (int)$bannerData->bannerOrder;
        if ($bannerDataCopy->bannerOrder < 0)
        {
            $bannerDataCopy->bannerOrder = 0;
        }
        $bannerDataCopy->bannerUrlAddressType = (int)$bannerData->bannerUrlAddressType;
        $bannerDataCopy->bannerExternalUrl = trim((string)$bannerData->bannerExternalUrl);
        $bannerDataCopy->bannerTitle = trim(strip_tags((string)$bannerData->bannerTitle));
        $bannerDataCopy->bannerContents = trim((string)$bannerData->bannerContents);
        $bannerDataCopy->bannerImages = $bannerData->bannerImages;
        if ($bannerDataCopy->bannerImages instanceof \Site\Custom\FlexibleContainer)
        {
            $bannerDataCopy->bannerImages = $bannerDataCopy->bannerImages->toArray(true);
        }

        $bannerImages = $bannerDataCopy->bannerImages;
        $banerImage = $bannerImages ? current($bannerImages) : null;

        /**
         * Temporary data - from add-form. They will be parsed and deleted.
         */

        $bannerDataCopy->bannerUniqueHashForFileName = trim((string)$bannerData->bannerUniqueHashForFileName);

        /**
         * Validating form data.
         */

        if (!$banerImage || ($banerImage['uniqueHashForFileName'] == ''))
        {
            return false;
        }

        if ($banerImage)
        {
            $bannerDataCopy->bannerUniqueHashForFileName = trim((string)$banerImage['uniqueHashForFileName']);

            // Validate unique hash for file name only if it is not empty!
            if (
                ($bannerDataCopy->bannerUniqueHashForFileName != '') &&
                !preg_match('/^([a-z0-9]{40})$/i', $bannerDataCopy->bannerUniqueHashForFileName)
            )
            {
                return false;
            }
        }

        /**
         * Identifying the author.
         */

        $bannerDataCopy->bannerLastUpdateAuthorId = $this->user->user_id;

        /**
         * Parsing unique hash for file name.
         */

        if ($bannerDataCopy->bannerUniqueHashForFileName != '')
        {
            $attachmentManager = $this->getAttachmentManager();
            $attachment = $attachmentManager->getAttachmentByUniqueHashForFileName(
                $bannerDataCopy->bannerUniqueHashForFileName
            );
            if (!$attachment)
            {
                return false;
            }

            $bannerDataCopy->bannerAttachmentId = $attachment->attachment_id;
        }

        /**
         * Removing unnecessary data.
         */

        unset($bannerDataCopy->bannerUniqueHashForFileName);

        /**
         * Adding "http://".
         */

        if (
            ($bannerDataCopy->bannerExternalUrl != '') &&
            (mb_strpos($bannerDataCopy->bannerExternalUrl, 'http://') === false) &&
            (mb_strpos($bannerDataCopy->bannerExternalUrl, 'https://') === false)
        )
        {
            $bannerDataCopy->bannerExternalUrl = 'http://' . $bannerDataCopy->bannerExternalUrl;
        }

        $isExternalUrlInvalid =
            ($bannerDataCopy->bannerUrlAddressType == self::BANNER_URL_ADDRESS_TYPE_EXTERNAL) &&
            !$this->isExternalUrlValid($bannerDataCopy->bannerExternalUrl);

        if (
            (($bannerDataCopy->bannerId !== null) && ($bannerDataCopy->bannerId <= 0)) ||
            ($bannerDataCopy->bannerTitle == '') ||
            $isExternalUrlInvalid
        )
        {
            return false;
        }

        return $bannerDataCopy;
    }

    /**
     * Logic-functions.
     */

    public function createBanner($bannerData)
    {
        $bannerDataCopy = $this->validateBannerData($bannerData);
        if (!$bannerDataCopy)
        {
            return false;
        }

        $created = false;

        $contentId = $this->getContentId();
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $insert = $sql->insert(self::BLOCK_BIG_BANNER_TABLE_NAME);
        $insert
            ->columns(
                array(
                    'content_id',
                    'page_id',
                    'attachment_id',
                    'is_published',
                    'is_title_visible',
                    'is_contents_visible',
                    'url_address_type',
                    'last_update_author_id',
                    'last_update_date',
                    'external_url',
                    'title',
                    'contents'
                )
            )
            ->values(
                array(
                    'content_id' => $contentId,
                    'page_id' => $bannerDataCopy->bannerPageId,
                    'attachment_id' => $bannerDataCopy->bannerAttachmentId,
                    'is_published' => $bannerDataCopy->bannerIsPublished,
                    'is_title_visible' => $bannerDataCopy->bannerIsTitleVisible,
                    'is_contents_visible' => $bannerDataCopy->bannerIsContentsVisible,
                    'url_address_type' => $bannerDataCopy->bannerUrlAddressType,
                    'last_update_author_id' => $bannerDataCopy->bannerLastUpdateAuthorId,
                    'last_update_date' => $lastUpdateDate,
                    'external_url' => $bannerDataCopy->bannerExternalUrl,
                    'title' => $bannerDataCopy->bannerTitle,
                    'contents' => $bannerDataCopy->bannerContents
                )
            );
        $bannerId = $dbManager->executeInsertAndGetLastInsertId($insert);
        if ($bannerId > 0)
        {
            $created = true;
        }

        if ($created)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $created;
    }

    public function decideWhatToDoWithRecentlyUploadedAndMovedFiles(\Zend\Http\Request $request, $movedFiles)
    {
        $postData = $request->getPost();

        // Inform the UploadController and UploadManager than upload was successful and you accept these files:
        return true;
    }

    public function deleteAllBigBannerData()
    {
        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $contentId = $this->getContentId();

        $delete = $sql->delete(self::BLOCK_BIG_BANNER_TABLE_NAME);
        $delete->where(array('content_id' => $contentId));
        $result = $dbManager->executePreparedStatement($delete);

        return $result;
    }

    public function deleteBannerById($bannerId)
    {
        $bannerId = (int)$bannerId;
        if ($bannerId <= 0)
        {
            return false;
        }

        $deleted = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $delete = $sql->delete();

        $dbManager->beginTransaction();
        $delete
            ->from(self::BLOCK_BIG_BANNER_TABLE_NAME)
            ->where(array('banner_id' => $bannerId));
        $result = $dbManager->executePreparedStatement($delete);
        if ($result)
        {
            $banner = $this->getBannerById($bannerId);
            $deleted = !$banner;
        }

        if ($deleted)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $deleted;
    }

    public function reorderBanners($banners)
    {
        $banners = (array)$banners;
        if (!$banners)
        {
            return false;
        }

        $reordered = true;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $contentId = $this->getContentId();
        $order = 0;

        $dbManager->beginTransaction();

        foreach ($banners as $bannerId)
        {
            $bannerId = (int)$bannerId;

            if ($bannerId > 0)
            {
                $update = $sql->update(self::BLOCK_BIG_BANNER_TABLE_NAME);
                $update
                    ->set(array('order' => $order))
                    ->where(
                        array(
                            'banner_id' => $bannerId,
                            'content_id' => $contentId
                        )
                    );
                $result = $dbManager->executePreparedStatement($update);
                if (!$result)
                {
                    $reordered = false;
                    break;
                }

                $banner = $this->getBannerById($bannerId);
                if ($banner->order != $order)
                {
                    $reordered = false;
                    break;
                }

                $order++;
            }
        }

        if ($reordered)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $reordered;
    }

    public function toggleBannerPublished($bannerId, $published)
    {
        $bannerId = (int)$bannerId;
        $published = (int)$published ? 1 : 0;

        if ($bannerId <= 0)
        {
            return false;
        }

        $updated = false;

        $contentId = $this->getContentId();

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();

        $dbManager->beginTransaction();

        $banner = $this->getBannerById($bannerId);
        if ($banner)
        {
            $update = $sql->update(self::BLOCK_BIG_BANNER_TABLE_NAME);
            $update
                ->set(
                    array(
                        'is_published' => $published
                    )
                )
                ->where(
                    array(
                        'banner_id' => $bannerId,
                        'content_id' => $contentId
                    )
                );
            $result = $dbManager->executePreparedStatement($update);
            if ($result)
            {
                $banner = $this->getBannerById($bannerId);
                if ($banner && ($banner->is_published == $published))
                {
                    $updated = true;
                }
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function updateBanner($bannerData)
    {
        $bannerDataCopy = $this->validateBannerData($bannerData);
        if (!$bannerDataCopy)
        {
            return false;
        }

        $contentId = $this->getContentId();
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $updated = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_BIG_BANNER_TABLE_NAME)
            ->set(
                array(
                    'page_id' => $bannerDataCopy->bannerPageId,
                    'attachment_id' => $bannerDataCopy->bannerAttachmentId,
                    'is_published' => $bannerDataCopy->bannerIsPublished,
                    'is_title_visible' => $bannerDataCopy->bannerIsTitleVisible,
                    'is_contents_visible' => $bannerDataCopy->bannerIsContentsVisible,
                    'url_address_type' => $bannerDataCopy->bannerUrlAddressType,
                    'last_update_author_id' => $bannerDataCopy->bannerLastUpdateAuthorId,
                    'last_update_date' => $lastUpdateDate,
                    'external_url' => $bannerDataCopy->bannerExternalUrl,
                    'title' => $bannerDataCopy->bannerTitle,
                    'contents' => $bannerDataCopy->bannerContents
                )
            )
            ->where(
                array(
                    'banner_id' => $bannerDataCopy->bannerId,
                    'content_id' => $contentId
                )
            );
        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $banner = $this->getBannerById($bannerDataCopy->bannerId);
            if (
                $banner &&
                ($banner->page_id == $bannerDataCopy->bannerPageId) &&
                ($banner->attachment_id == $bannerDataCopy->bannerAttachmentId) &&
                ($banner->is_published == $bannerDataCopy->bannerIsPublished) &&
                ($banner->url_address_type == $bannerDataCopy->bannerUrlAddressType) &&
                ($banner->last_update_author_id == $bannerDataCopy->bannerLastUpdateAuthorId) &&
                ($banner->external_url == $bannerDataCopy->bannerExternalUrl) &&
                ($banner->title == $bannerDataCopy->bannerTitle) &&
                ($banner->contents == $bannerDataCopy->bannerContents)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function updateBannerContents($bannerId, $bannerContents)
    {
        $bannerId = (int)$bannerId;
        $bannerContents = trim((string)$bannerContents);

        if (($bannerId <= 0) || ($bannerContents == ''))
        {
            return false;
        }

        $contentId = $this->getContentId();
        $lastUpdateAuthorId = $this->user->user_id;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $updated = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_BIG_BANNER_TABLE_NAME)
            ->set(
                array(
                    'last_update_author_id' => $lastUpdateAuthorId,
                    'last_update_date' => $lastUpdateDate,
                    'contents' => $bannerContents
                )
            )
            ->where(
                array(
                    'banner_id' => $bannerId,
                    'content_id' => $contentId
                )
            );
        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $banner = $this->getBannerById($bannerId);
            if (
                $banner &&
                ($banner->last_update_author_id == $lastUpdateAuthorId) &&
                ($banner->contents == $bannerContents)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }

    public function updateBannerTitle($bannerId, $bannerTitle)
    {
        $bannerId = (int)$bannerId;
        $bannerTitle = trim(strip_tags((string)$bannerTitle));

        if (($bannerId <= 0) || ($bannerTitle == ''))
        {
            return false;
        }

        $contentId = $this->getContentId();
        $lastUpdateAuthorId = $this->user->user_id;
        $lastUpdateDate = new \Zend\Db\Sql\Expression('NOW()');

        $updated = false;

        $dbManager = $this->getDatabaseManager();
        $sql = $dbManager->getSql();
        $dbManager->beginTransaction();

        $update = $sql->update(self::BLOCK_BIG_BANNER_TABLE_NAME)
            ->set(
                array(
                    'last_update_author_id' => $lastUpdateAuthorId,
                    'last_update_date' => $lastUpdateDate,
                    'title' => $bannerTitle
                )
            )
            ->where(
                array(
                    'banner_id' => $bannerId,
                    'content_id' => $contentId
                )
            );
        $result = $dbManager->executePreparedStatement($update);
        if ($result)
        {
            $banner = $this->getBannerById($bannerId);
            if (
                $banner &&
                ($banner->last_update_author_id == $lastUpdateAuthorId) &&
                ($banner->title == $bannerTitle)
            )
            {
                $updated = true;
            }
        }

        if ($updated)
        {
            $dbManager->commit();
        }
        else
        {
            $dbManager->rollback();
        }

        return $updated;
    }
}
<?php
namespace Site\Service;

use Zend\Log\LoggerInterface;

class ErrorHandling
{
    protected $logger;

    function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    function logException(\Exception $e)
    {
        $this->logger->err(
            $e . "\n" .
            'Current script owner: ' . get_current_user() . "\n" .
            'Current working directory: ' . getcwd() .
            "\n\n"
        );
    }
}
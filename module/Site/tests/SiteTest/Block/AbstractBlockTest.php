<?php
/**
 * proimagine-cms
 * Developer: Dzmitry Vensko
 * Date: 17.06.14 11:51
 */
namespace SiteTest\Block;

use Zend\Db\Adapter\Driver\StatementInterface;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\Adapter\Driver\ConnectionInterface;
use Zend\Db\Adapter\Driver\DriverInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\View\Renderer\PhpRenderer;
use Zend\ServiceManager\ServiceManager;
use Site\Controller\DatabaseManager;
use Site\Block\AbstractBlock;
use Site\Custom\FlexibleContainer;
use Site\Component\BasicSessionData;

class AbstractBlockTest extends \PHPUnit_Framework_TestCase
{
    protected $instance, $block, $serviceManager, $mockSql, $mockDriver, $mockDatabaseManager, $mockLanguageManager, $mockUserLanguage, $mockLanguage, $mockPageManager, $request, $fileSystem, $mockBlockManager, $mockAttachmentManager, $mockUploadManager, $mockUser, $mockBlockData, $phpRenderer, $mockRouter;

    protected $mockResult;

    private $mock;

    public static $loremIpsum;

    public function setUp()
    {
        $sm = $this->serviceManager = new ServiceManager();

        $this->phpRenderer = $this->getMock(PhpRenderer::class);
        $sm->setService('\Zend\View\Renderer\PhpRenderer', $this->phpRenderer);

        $this->mockResult = $this->getMock(\Zend\Db\Adapter\Driver\Pdo\Result::class);

        $mockStatement = $this->getMock(StatementInterface::class);

        $mockStatement->expects($this->any())
            ->method('execute')
            ->willReturn($this->mockResult);

        $mockConnection = $this->getMock(ConnectionInterface::class);

        $this->mockDriver = $this->getMock(DriverInterface::class);
        $this->mockDriver->expects($this->any())
            ->method('createStatement')
            ->willReturn($mockStatement);
        $this->mockDriver->expects($this->any())
            ->method('getConnection')
            ->willReturn($mockConnection);

        $mockAdapter = $this->getMock(Adapter::class, null, [ $this->mockDriver ]);

        $this->mockSql = new Sql($mockAdapter);
        $sm->setService('Sql', $this->mockSql);

        $this->mockDatabaseManager = $this->getMock(DatabaseManager::class, null, [ $this->mockSql ]);

        $sm->setService('DatabaseManager', $this->mockDatabaseManager);

        $this->mockBlockData = (object)[
            'page_id' => $this->getRandomNumber(),
            'block_id' => $this->getRandomNumber(),
            'page_block_id' => $this->getRandomNumber(),
            'is_system' => 1,
            'is_dynamic' => 0,
            'is_parent_block_dynamic' => 1, // More consistent variable selection.
            'is_published' => 1,
            'has_data_in_search_index' => 0,
            'php_class_name' => 'Text',
            'slot_number' => $this->getRandomNumber(),
            'order' => $this->getRandomNumber(),
            'language_category_id' => null,
            'last_update_author_id' => null,
            'last_update_date' => date('Y-m-d H:i:s', $this->getRandomNumber(time() - 3600*24*365*20, time())),
            'config' => [],
            'name' => $this->generateString(),
            'content' => (object)[
                    'content' => $this->getGeneratedText(),
                    'config' => '<?xml version="1.0"?><a></a>'
                ]
        ];

        $this->request = $this->getMock(\Zend\Http\Request::class);

        $this->fileSystem = $this->getMock(\FileSystem\Component\FileSystem::class);

        $this->mockLanguage = $this->getMock(\stdClass::class);

        $this->mockUserLanguage = $this->getMock(\Site\Custom\FlexibleContainer::class);

        $this->mockLanguageManager = $this->getMockBuilder(\Site\Controller\LanguageManager::class)
            ->setConstructorArgs([
                [],
                $this->serviceManager,
                $this->mockLanguage,
                null
            ])
            ->getMock();

        $this->mockPageManager = $this->getMockBuilder(\Site\Controller\PageManager::class)
            ->setConstructorArgs([
                [],
                $this->serviceManager,
                $this->mockLanguage,
                $this->mockUserLanguage,
                null
            ])
            ->getMock();

        $this->mockBlockManager = $this->getMockBuilder(\Site\Controller\BlockManager::class)
            ->setConstructorArgs([
                [],
                $this->serviceManager,
                $this->mockSql,
                $this->request,
                $this->mockLanguage,
                $this->mockUserLanguage,
                null
            ])
            ->getMock();

        $this->mockBlockManager->expects($this->any())
            ->method('getPageBlockContentsByPageBlockId')
            ->willReturn((object)['content' => $this->getGeneratedText()]);

        $this->mockBlockManager->expects($this->any())
            ->method('getPageBlockContentIdByPageBlockId')
            ->willReturn($this->getRandomNumber());

        $this->mockAttachmentManager = $this->getMockBuilder(\Site\Controller\AttachmentManager::class)
            ->setConstructorArgs([
                [],
                $this->serviceManager,
                $this->mockSql,
                $this->request,
                $this->mockLanguage,
                $this->mockUserLanguage,
                null,
                $this->fileSystem
            ])
            ->getMock();

        $this->mockUploadManager = $this->getMockBuilder(\Site\Controller\UploadManager::class)
            ->setConstructorArgs([
                [],
                $this->serviceManager,
                $this->mockSql,
                $this->request,
                $this->mockLanguage,
                $this->mockUserLanguage,
                null,
                $this->fileSystem
            ])
            ->getMock();

        $this->mockUser = new FlexibleContainer();
        $this->mockUser->user_id = $this->getRandomPositiveNumber();

        $this->mockUser->session = new BasicSessionData();

        $this->mockRouter = $this->getMockForAbstractClass(\Zend\Mvc\Router\Http\RouteInterface::class);
        $sm->setService('Router', $this->mockRouter);

        $this->mockViewHelperManager = $this->getMockBuilder(\Zend\View\HelperPluginManager::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->mockViewHelperManager->expects($this->any())
            ->method('get')
            ->willReturn(function(){ return func_get_arg(0); });
        $sm->setService('ViewHelperManager', $this->mockViewHelperManager);
    }

    public function tearDown()
    {
        $this->serviceManager = new ServiceManager();
    }

    public function getArgs()
    {
        return [
            $this->serviceManager,
            $this->mockLanguageManager,
            $this->mockPageManager,
            $this->mockBlockManager,
            $this->mockAttachmentManager,
            $this->mockUploadManager,
            $this->mockBlockData,
            $this->mockUser,
            'eng',
            $this->mockUserLanguage
        ];
    }

    /**
     * @return AbstractBlock
     */
    private function getStub()
    {
        if (!$this->mock) {
            $this->mock = $this->getMockBuilder(AbstractBlock::class)
                ->setConstructorArgs($this->getArgs())
                ->getMockForAbstractClass();
        }

        return $this->mock;
    }

    public function testBlockDataType()
    {
        $this->assertInstanceOf('stdClass', $this->getStub()
            ->getBlockData());
    }

    public function testInitialRenderingMode()
    {
        $this->assertTrue($this->getStub()
            ->isRenderingEnabled());
    }

    public function testChangeOfRenderingMode()
    {
        $this->getStub()
            ->setRenderingEnabled(true);

        $this->assertTrue($this->getStub()
            ->isRenderingEnabled());

        $this->getStub()
            ->setRenderingEnabled(false);

        $this->assertFalse($this->getStub()
            ->isRenderingEnabled());
    }

    public function testSaveContentWithoutArguments()
    {
        $this->assertTrue($this->getStub()
            ->saveContent());
    }

    public function testSaveContent()
    {
        $this->assertTrue($this->getStub()
            ->saveContent($this->getGeneratedText()));
    }

    /**
     * @dataProvider provideRandomPositiveNumber
     */
    public function testDeleteBlockRecordFromSearchIndex($elementId)
    {
        $this->assertTrue(
            $this->getMethodReflection('deleteBlockRecordFromSearchIndex')->invokeArgs($this->getStub(),[ $elementId ])
        );
    }

    public function testUpdateBlockRecordInSearchIndex()
    {
        $r = $this->getMethodReflection('updateBlockRecordInSearchIndex');

        $this->assertFalse($r->invokeArgs($this->getStub(), [ -$this->getRandomNumber(), '', '', '' ]));
    }

    public function testGetContentId()
    {
        $this->assertInternalType('int', $this->getMethodReflection('getContentId')->invoke($this->getStub()));
    }

    /**
     * @depends testGetNotSharedSettings
     */
    public function testGetSettings()
    {
        $r = $this->getMethodReflection('getSettings');

        $this->assertInstanceOf(\Site\Custom\FlexibleContainer::class, $r->invoke($this->getStub()));
    }

    /**
     * @depends testEscapeDiacritics
     */
    public function testGenerateCleanUrlFromText()
    {
        $this->assertFalse($this->getStub()->generateCleanUrlFromText('   '));

        $input = "\n".'po--niedziałek  ';

        $this->assertEquals('po-niedzialek', $this->getStub()->generateCleanUrlFromText($input));
    }

    public function testEscapeDiacritics()
    {
        $this->assertEquals('', $this->getStub()->generateCleanUrlFromText(''));

        $fromReplace = array(
            'ą',
            'ć',
            'ę',
            'ł',
            'ń',
            'ó',
            'ś',
            'ż',
            'ź',
            'Ą',
            'Ć',
            'Ę',
            'Ł',
            'Ń',
            'Ó',
            'Ś',
            'Ż',
            'Ź'
        );

        $toReplace = array(
            'a',
            'c',
            'e',
            'l',
            'n',
            'o',
            's',
            'z',
            'z',
            'A',
            'C',
            'E',
            'L',
            'N',
            'O',
            'S',
            'Z',
            'Z'
        );

        $resultString = $inputString = '';
        $n = sizeof($fromReplace)-1;

        for ($i=0; $i < $this->getRandomNumber(10,50); $i++) {
            $index = $this->getRandomNumber(0,$n);
            $inputString .= $fromReplace[$index];
            $resultString .= $toReplace[$index];
        }

        $this->assertEquals($resultString, $this->getStub()->escapeDiacritics($inputString));
    }

    public function testGetNotSharedSettings()
    {
        $r = $this->getMethodReflection('getNotSharedSettings');

        $this->assertInstanceOf(\Site\Custom\FlexibleContainer::class, $r->invoke($this->getStub()));
    }

    /**
     * @depends testGetRouteParametersFromRequest
     */
    public function testGetParametersForCurrentPageFromRequest()
    {
        $mockRequest = $this->getMock(\Zend\Http\Request::class);

        $params = [
            'parameters' => 'param1,value1,param2,value2'
        ];

        $mockMatch = $this->getMock(\Zend\Mvc\Router\Http\RouteMatch::class, null, [ $params ]);
        $mockMatch->expects($this->any())
            ->method('getParams')
            ->willReturn($params);

        $this->mockRouter->expects($this->once())
            ->method('match')
            ->willReturn($mockMatch);

        $r = $this->getMethodReflection('getParametersForCurrentPageFromRequest');
        $result = $r->invokeArgs($this->getStub(), [ $mockRequest ]);

        $this->assertInternalType('object', $result);

        $this->assertObjectHasAttribute('param1', $result);
        $this->assertObjectHasAttribute('param2', $result);
        $this->assertAttributeEquals('value1', 'param1', $result);
        $this->assertAttributeEquals('value2', 'param2', $result);
    }

    public function testGetRouteParametersFromRequest()
    {
        $mockRequest = $this->getMock(\Zend\Http\Request::class);

        $params = [
            'parameters' => 'param1,value1,param2,value2'
        ];

        $mockMatch = $this->getMock(\Zend\Mvc\Router\RouteMatch::class, null, [ $params ]);
        $mockMatch->expects($this->any())
            ->method('getParams')
            ->willReturn($params);

        $this->mockRouter->expects($this->any())
            ->method('match')
            ->willReturn($mockMatch);

        $r = $this->getMethodReflection('getRouteParametersFromRequest');
        $result = $r->invokeArgs($this->getStub(), [ $mockRequest ]);

        $this->assertInternalType('object', $result);
        $this->assertEquals('param1,value1,param2,value2', $result->parameters);
    }

    public function testConvertToFlexibleContainer()
    {
        $r = $this->getMethodReflection('convertToFlexibleContainer');

        $this->assertInstanceOf(\Site\Custom\FlexibleContainer::class, $r->invokeArgs($this->getStub(), [ [] ]));
        $this->assertInstanceOf(\Site\Custom\FlexibleContainer::class, $r->invokeArgs($this->getStub(), [ new \ArrayObject() ]));
    }

    protected function getMethodReflection($name)
    {
        $class = new \ReflectionObject($this->getStub());
        $method = $class->getMethod($name);
        $method->setAccessible(true);
        return $method;
    }

    public function provideRandomPositiveNumber()
    {
        return [
            [ $this->getRandomPositiveNumber() ]
        ];
    }

    public function getRandomPositiveNumber()
    {
        return $this->getRandomNumber(1);
    }

    public function getRandomNumber($min = null, $max = null)
    {
        return mt_rand($min === null ? 0 : $min, $max ?: mt_getrandmax());
    }

    public function getGeneratedText()
    {
        if (empty(self::$loremIpsum)) {
            self::$loremIpsum = file_get_contents('http://loripsum.net/api');
        }

        return self::$loremIpsum;
    }

    public function generateString($max = 255)
    {
        return sha1(microtime());
    }

    protected function isPdoResult($result)
    {
        $this->assertInstanceOf(\Zend\Db\Adapter\Driver\Pdo\Result::class, $result);
    }

    protected function isHydratingResultSet($result)
    {
        $this->assertInstanceOf(\Zend\Db\ResultSet\HydratingResultSet::class, $result);
    }
}

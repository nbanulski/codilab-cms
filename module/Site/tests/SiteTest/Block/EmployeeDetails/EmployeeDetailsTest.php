<?php

namespace SiteTest\Block\EmployeeDetails;

use Site\Block\EmployeeDetails\EmployeeDetails;

/**
 * @coversDefaultClass \Site\Block\EmployeeDetails\EmployeeDetails
 */
class EmployeeDetailsTest extends \SiteTest\Block\BlockTestCase
{
    /**
     * @var EmployeeDetails
     */
    protected $block;

    protected $blockClassName = EmployeeDetails::class;

    /**
     * @depends testChangeOfRenderingMode
     */
    public function testHideEmployeeDetails()
    {
        $this->assertTrue(true);
    }

    /**
     * @dataProvider provideRandomPositiveNumber
     */
    public function testGetEmployeeById($employeeId)
    {
        $this->assertFalse($this->block->getEmployeeById(0));
        $this->assertFalse($this->block->getEmployeeById(-$employeeId));

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'employee_id' => $employeeId
            ]);

        $this->assertObjectHasAttribute('employee_id', $this->block->getEmployeeById($employeeId));
    }

    public function getEmployeeByIdAndContentId()
    {
        $employeeId = $this->getRandomPositiveNumber();
        $contentId = $this->getRandomPositiveNumber();

        $this->assertFalse($this->block->getEmployeeByIdAndContentId(0, $contentId));
        $this->assertFalse($this->block->getEmployeeByIdAndContentId($employeeId, 0));

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'employee_id' => $employeeId,
                'content_id' => $contentId,
            ]);

        $result = $this->block->getEmployeeByIdAndContentId($employeeId, $contentId);

        $this->assertInternalType('object', $result);

        $this->assertObjectHasAttribute('employee_id', $result);
        $this->assertObjectHasAttribute('content_id', $result);

        $this->assertEquals($employeeId, $result->employee_id);
        $this->assertEquals($contentId, $result->content_id);
    }

    public function testGetEmployeePartialDataFromEmployeePartialDataString()
    {
        $employeeId = $this->getRandomPositiveNumber();
        $contentId = $this->getRandomPositiveNumber();
        $randomString = $this->generateString();

        $input = $randomString.'-some-random-string-'.$contentId.'-'.$employeeId;

        $this->assertEquals(
            [ $randomString.'-some-random-string', $contentId, $employeeId ],
            $this->block->getEmployeePartialDataFromEmployeePartialDataString($input)
        );
    }

    public function testGetEmployeeByEmployeeCleanUrlAndContentId()
    {
        $this->assertFalse(
            $this->block->getEmployeeByEmployeeCleanUrlAndContentId(
                $this->generateString(),
                -$this->getRandomPositiveNumber()
            )
        );

        $this->assertFalse(
            $this->block->getEmployeeByEmployeeCleanUrlAndContentId(
                '',
                $this->getRandomPositiveNumber()
            )
        );

        $contentId = $this->getRandomPositiveNumber();

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'content_id' => $contentId,
            ]);

        $result = $this->block->getEmployeeByEmployeeCleanUrlAndContentId($this->generateString(), $contentId);

        $this->assertInternalType('object', $result);
        $this->assertObjectHasAttribute('content_id', $result);
        $this->assertEquals($contentId, $result->content_id);
    }

    public function testGetFullEmployeeInfoByEmployeeIdAndContentId()
    {
        $employeeId = $this->getRandomPositiveNumber();
        $contentId = $this->getRandomPositiveNumber();
        $attachmentId = $this->getRandomPositiveNumber();

        $hashForFileName = $this->generateString();
        $originalFileName = $this->generateString();

        $this->assertFalse(
            $this->block->getFullEmployeeInfoByEmployeeIdAndContentId($employeeId, -$contentId)
        );

        $this->assertFalse(
            $this->block->getFullEmployeeInfoByEmployeeIdAndContentId(0, $contentId)
        );

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'employee_id' => $employeeId,
                'content_id' => $contentId,
                'attachment_id' => $attachmentId,
            ]);

        $r = new \ReflectionClass(\Site\Block\Employees\Employees::class);

        $this->mockBlockManager->expects($this->once())
            ->method('createBlockUsingContentId')
            ->willReturn($r->newInstanceWithoutConstructor());

        $this->mockAttachmentManager->expects($this->once())
            ->method('getAttachmentById')
            ->willReturn((object)[
                'hash_for_file_name' => $hashForFileName,
                'original_file_name' => $originalFileName,
            ]);

        $result = $this->block->getFullEmployeeInfoByEmployeeIdAndContentId($employeeId, $contentId);

        $this->assertInternalType('object', $result);

        $this->assertObjectHasAttribute('employee_id', $result);
        $this->assertObjectHasAttribute('content_id', $result);
        $this->assertObjectHasAttribute('attachment_id', $result);
        $this->assertObjectHasAttribute('thumbUrl', $result);

        $this->assertEquals($employeeId, $result->employee_id);
        $this->assertEquals($contentId, $result->content_id);
        $this->assertEquals($attachmentId, $result->attachment_id);
        $this->assertEquals('/eng/get-file/index,'.$hashForFileName.','.$originalFileName, $result->thumbUrl);
    }

    /**
     * @depends testGetSettings
     * @depends getEmployeeByIdAndContentId
     */
    public function testEmployeeDetailsShouldBeRenderedForCurrentEmployeeIdAndContentId()
    {
        $employeeId = $this->getRandomPositiveNumber();
        $contentId = $this->getRandomPositiveNumber();

        $this->assertTrue($this->block->employeeDetailsShouldBeRenderedForCurrentEmployeeIdAndContentId($employeeId, $contentId));
    }

    /**
     * @depends testGenerateCleanUrlFromText
     * @depends testEscapeDiacritics
     */
    public function testGenerateCleanUrlFromEmployeeNameAndSurname()
    {
        $this->assertTrue(true);
    }

    /**
     * @depends testGetEmployeeById
     */
    public function testSetEmployeeDegreeId()
    {
        $employeeId = $this->getRandomPositiveNumber();
        $degreeId = $this->getRandomPositiveNumber();

        $this->assertFalse($this->block->setEmployeeDegreeId($employeeId, -$degreeId));
        $this->assertFalse($this->block->setEmployeeDegreeId(0, $degreeId));

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'degree_id' => $degreeId,
                'last_update_author_id' => $this->mockUser->user_id,
            ]);

        $this->assertTrue($this->block->setEmployeeDegreeId($employeeId, $degreeId));
    }

    /**
     * @depends testGetEmployeeById
     */
    public function testUpdateEmployeeNameAndSurname()
    {
        $employeeId = $this->getRandomPositiveNumber();
        $name = $this->generateString().' '.$this->generateString();
        $nameNonFiltered = '<b>'.$name.'</b>';

        $this->assertFalse($this->block->updateEmployeeNameAndSurname($employeeId, ''));
        $this->assertFalse($this->block->updateEmployeeNameAndSurname(0, $name));

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'name_and_surname' => $name,
                'last_update_author_id' => $this->mockUser->user_id,
            ]);

        $this->assertTrue($this->block->updateEmployeeNameAndSurname($employeeId, $nameNonFiltered));
    }

    /**
     * @depends testGetEmployeeById
     */
    public function testUpdateEmployeePosition()
    {
        $employeeId = $this->getRandomPositiveNumber();
        $position = $this->generateString();

        $this->assertFalse($this->block->updateEmployeeNameAndSurname($employeeId, ''));
        $this->assertFalse($this->block->updateEmployeeNameAndSurname(0, $position));

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'position' => $position,
                'last_update_author_id' => $this->mockUser->user_id,
            ]);

        $this->assertTrue($this->block->updateEmployeePosition($employeeId, $position));
    }

    /**
     * @depends testGetEmployeeById
     * @depends testGenerateCleanUrlFromEmployeeNameAndSurname
     * @depends testUpdateBlockRecordInSearchIndex
     */
    public function testUpdateEmployeeAbout()
    {
        $employeeId = $this->getRandomPositiveNumber();
        $about = $this->getGeneratedText();

        $this->assertFalse($this->block->updateEmployeeNameAndSurname($employeeId, ''));
        $this->assertFalse($this->block->updateEmployeeNameAndSurname(0, $about));

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'employee_id' => $employeeId,
                'about' => $about,
                'last_update_author_id' => $this->mockUser->user_id,
                'name_and_surname' => $this->generateString(),
                'content_id' => $this->getRandomPositiveNumber(),
            ]);

        $this->assertTrue($this->block->updateEmployeeNameAndSurname($employeeId, $about));
    }

    /**
     * @depends testGetEmployeeById
     */
    public function testUpdateEmployeeScientificCareer()
    {
        $employeeId = $this->getRandomPositiveNumber();
        $info = $this->getGeneratedText();

        $this->assertFalse($this->block->updateEmployeeScientificCareer($employeeId, ''));
        $this->assertFalse($this->block->updateEmployeeScientificCareer(0, $info));

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'scientific_career' => trim($info),
                'last_update_author_id' => $this->mockUser->user_id,
            ]);

        $this->assertTrue($this->block->updateEmployeeScientificCareer($employeeId, $info));
    }

    /**
     * @depends testGetEmployeeById
     */
    public function testUpdateEmployeeTheMostImportantPublications()
    {
        $employeeId = $this->getRandomPositiveNumber();
        $info = $this->getGeneratedText();

        $this->assertFalse($this->block->updateEmployeeTheMostImportantPublications($employeeId, ''));
        $this->assertFalse($this->block->updateEmployeeTheMostImportantPublications(0, $info));

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'most_important_publications' => trim($info),
                'last_update_author_id' => $this->mockUser->user_id,
            ]);

        $this->assertTrue($this->block->updateEmployeeTheMostImportantPublications($employeeId, $info));
    }

    /**
     * @depends testGetEmployeeById
     */
    public function testUpdateEmployeeContactInfo()
    {
        $employeeId = $this->getRandomPositiveNumber();
        $info = $this->getGeneratedText();

        $this->assertFalse($this->block->updateEmployeeContactInfo($employeeId, ''));
        $this->assertFalse($this->block->updateEmployeeContactInfo(0, $info));

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'contact_info' => trim($info),
                'last_update_author_id' => $this->mockUser->user_id,
            ]);

        $this->assertTrue($this->block->updateEmployeeContactInfo($employeeId, $info));
    }
}


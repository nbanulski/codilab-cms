<?php
/**
 * proimagine-cms
 * Developer: Dzmitry Vensko
 * Date: 24.06.14 11:54
 */
namespace SiteTest\Block\Text;

use Site\Block\Text\Text;

/**
 * @coversDefaultClass \Site\Block\Text\Text
 */
class TextTest extends \SiteTest\Block\BlockTestCase
{
    /**
     * @var Text
     */
    protected $block;

    protected $blockClassName = Text::class;

    public function testHasDynamicAttribute()
    {
        $this->hasDynamicAttribute();
    }

    /**
     * @depends testHasDynamicAttribute
     * @depends deleteBlockRecordFromSearchIndex
     */
    public function testOnBeforeDeleted()
    {
        $this->assertTrue(true);
    }

    /**
     * @depends testBlockDataType
     * @depends testUpdateBlockRecordInSearchIndex
     */
    public function testUpdateSearchIndex()
    {
        $this->assertTrue(true);
    }

    public function testChildRender()
    {
        $testText = $this->getGeneratedText();

        $this->mockBlockManager->expects($this->any())
            ->method('getBlockPropertiesByBlockIdAndLanguageId')
            ->willReturn((object)[ 'default_content' => $testText ]);

        $this->block->childRender($this->getMock(\Zend\Http\Request::class));

        $this->assertEquals($testText, $this->block->getBlockData()->content->content);
    }
}

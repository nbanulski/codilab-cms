<?php
/**
 * proimagine-cms
 * Developer: Dzmitry Vensko
 * Date: 23.06.14 13:13
 */

namespace SiteTest\Block\BachelorStudies;

use Site\Block\BachelorStudies\BachelorStudies;

/**
 * @coversDefaultClass \Site\Block\BachelorStudies\BachelorStudies
 */
class BachelorStudiesTest extends \SiteTest\Block\BlockTestCase
{
    /**
     * @var BachelorStudies
     */
    protected $block;

    protected $blockClassName = BachelorStudies::class;

    /**
     * @depends testHasDynamicAttribute
     * @depends testDeleteAllTabs
     * @depends testDeleteAllBasicInfo
     * @depends testDeleteBlockRecordFromSearchIndex
     */
    public function testOnBeforeDeleted()
    {
        $this->assertTrue(true);
    }

    public function testHasDynamicAttribute()
    {
        $this->hasDynamicAttribute();
    }

    public function testGetBasicInfo()
    {
        $testString = $this->generateString();

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'test_content' => $testString
            ]);

        $result = $this->block->getBasicInfo();

        $this->assertObjectHasAttribute('test_content', $result);

        if (isset($result->test_content)) {
            $this->assertEquals($result->test_content, $testString);
        }
    }

    public function testGetAllTabs()
    {
        $this->isHydratingResultSet($this->block->getAllTabs());
    }

    /**
     * @dataProvider provideRandomPositiveNumber
     * @depends testGetContentId
     */
    public function testGetTabById($id)
    {
        $this->assertFalse($this->block->getTabById(0));
        $this->assertFalse($this->block->getTabById(-$id));

        $result = $this->block->getTabById($id);

        $this->assertObjectHasAttribute('tab_id', $result);

        if (isset($result->tab_id)) {
            $this->assertEquals($id, $result->tab_id);
        }
    }

    /**
     * @depends testGetContentId
     */
    public function testTabByNameExists()
    {
        $name = $this->generateString();

        $this->assertFalse($this->block->tabByNameExists(''));
        $this->assertFalse($this->block->tabByNameExists(0));
        $this->assertFalse($this->block->tabByNameExists(null));

        $testNumber = $this->getRandomPositiveNumber();

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'count' => $testNumber
            ]);

        $this->assertTrue($this->block->tabByNameExists($name));
    }

    /**
     * @depends testGetContentId
     * @depends testTabByNameExists
     */
    public function testCreateTabAndReturnId()
    {
//        $insertId = $this->getRandomPositiveNumber();
//
//        $this->mockDriver->expects($this->any())
//            ->method('getLastGeneratedValue')
//            ->willReturn($insertId);
    }

    /**
     * @depends testGetAllTabs
     * @depends testUpdateBlockRecordInSearchIndex
     */
    public function testUpdateSearchIndex()
    {
        $this->assertTrue(true);
    }

    public function testDeleteAllTabs()
    {
        $this->isPdoResult($this->block->deleteAllTabs());
    }

    public function testDeleteAllBasicInfo()
    {
        $this->isPdoResult($this->block->deleteAllTabs());
    }


}

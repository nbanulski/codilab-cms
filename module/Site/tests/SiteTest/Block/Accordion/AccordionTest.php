<?php
/**
 * proimagine-cms
 * Developer: Dzmitry Vensko
 * Date: 16.06.14 10:35
 */

namespace SiteTest\Block\Accordion;

use Site\Block\Accordion\Accordion;

/**
 * @coversDefaultClass \Site\Block\Accordion\Accordion
 */
class AccordionTest extends \SiteTest\Block\BlockTestCase
{
    /**
     * @var Accordion
     */
    protected $block;

    protected $blockClassName = Accordion::class;

    public function testHasDynamicAttribute()
    {
        $this->hasDynamicAttribute();
    }

    /**
     * @depends testHasDynamicAttribute
     * @depends testDeleteAccordionItems
     * @depends testDeleteBlockRecordFromSearchIndex
     */
    public function testOnBeforeDeleted()
    {
        $this->assertTrue(true);
    }

    /**
     * @depends testGetAllPanels
     * @depends testUpdateBlockRecordInSearchIndex
     */
    public function testUpdateSearchIndex()
    {
        $this->assertTrue(true);
    }

    public function testChildRender()
    {
        $propertyExists = property_exists(get_class($this->block), 'blockView');
        $this->assertTrue($propertyExists);

        if ($propertyExists) {
            $this->assertInstanceOf(\ReflectionProperty::class, $r = (new \ReflectionObject($this->block))->getProperty('blockView'));
            $r->setAccessible(true);
            $viewModel = $r->getValue($this->block);

            $isViewModelInstance = ($viewModel instanceof \Zend\View\Model\ViewModel);
            $this->assertTrue($isViewModelInstance);

            if ($isViewModelInstance) {
                $this->block->childRender($this->request);

                $this->assertArrayHasKey('contentId', $viewModel);
                $this->assertInternalType('int', $viewModel['contentId']);

                $this->assertArrayHasKey('accordion', $viewModel);
                $this->assertInternalType('array', $viewModel['accordion']);
            }
        }
    }

    public function testGetAllPanels()
    {
        $this->isHydratingResultSet($this->block->getAllPanels());
    }

    /**
     * @dataProvider provideRandomPositiveNumber
     */
    public function testGetPanelById($panelId)
    {
        $this->assertFalse($this->block->getPanelById(0));
        $this->assertFalse($this->block->getPanelById(-$panelId));

        $contentId = $this->getRandomPositiveNumber();

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'panel_id' => $panelId,
                'content_id' => $contentId
            ]);

        $result = $this->block->getPanelById($panelId);

        $this->assertEquals($result->panel_id, $panelId);
        $this->assertEquals($result->content_id, $contentId);
    }

    public function testGetPanelCount()
    {
        $testNumber = $this->getRandomNumber();

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'count' => $testNumber
            ]);

        $this->assertEquals($this->block->getPanelCount(), $testNumber);
    }

    public function testGetMaxPanelsOrder()
    {
        $testNumber = $this->getRandomNumber();

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'max_order' => $testNumber
            ]);

        $this->assertEquals($this->block->getMaxPanelsOrder(), $testNumber);
    }

    /**
     * @dataProvider provideRawPanelData
     */
    public function testFormatPanelDataAndReturnIfValid($panel)
    {
        $this->assertFalse($this->block->formatPanelDataAndReturnIfValid([]));

        $testPanel = $panel;
        $testPanel['panelId'] = -$this->getRandomPositiveNumber();
        $this->assertFalse($this->block->formatPanelDataAndReturnIfValid($testPanel));

        $testPanel = $panel;
        $testPanel['panelName'] = '';
        $this->assertFalse($this->block->formatPanelDataAndReturnIfValid($testPanel));

        $testPanel = $panel;
        $testPanel['panelId'] = 0;
        $this->assertInstanceOf(\Site\Custom\FlexibleContainer::class, $this->block->formatPanelDataAndReturnIfValid($testPanel));

        $this->assertInstanceOf(\Site\Custom\FlexibleContainer::class, $this->block->formatPanelDataAndReturnIfValid($panel));
    }

    /**
     * @dataProvider provideRawPanelData
     * @depends testFormatPanelDataAndReturnIfValid
     * @depends testUpdateExistingPanel
     * @depends testCreateNewPanel
     */
    public function testAddOrEditPanel($panel)
    {
        $this->assertTrue(true);
    }

    public function testUpdateExistingPanel()
    {
        $fakeValue = $this->getRandomPositiveNumber();

        $panelData = (object)[
            'panelId' => $fakeValue,
            'panelIsPublished' => $fakeValue,
            'panelOrder' => $fakeValue,
            'panelLastUpdateAuthorId' => $fakeValue,
            'panelLastUpdateDate' => $fakeValue,
            'panelName' => $fakeValue,
            'panelContents' => $fakeValue,
        ];

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'is_published' => $panelData->panelIsPublished,
                'panelOrder' => $panelData->panelOrder,
                'last_update_author_id' => $panelData->panelLastUpdateAuthorId,
                'name' => $panelData->panelName,
                'contents' => $panelData->panelContents,
            ]);

        $r = $this->getMethodReflection('updateExistingPanel');

        $this->assertTrue($r->invokeArgs($this->block, [ $panelData ]));
    }

    /**
     * @dataProvider provideRawPanelData
     * @depends testGetPanelById
     */
    public function testCreateNewPanel($panel)
    {
        $insertId = $fakeValue = $this->getRandomPositiveNumber();

        $panelData = (object)[
            'panelIsPublished' => $fakeValue,
            'panelOrder' => $fakeValue,
            'panelLastUpdateAuthorId' => $fakeValue,
            'panelLastUpdateDate' => $fakeValue,
            'panelName' => $fakeValue,
            'panelContents' => $fakeValue,
        ];

        $r = $this->getMethodReflection('createNewPanel');

        $this->mockDriver->expects($this->any())
            ->method('getLastGeneratedValue')
            ->willReturn($insertId);

        $this->assertEquals($insertId, $r->invokeArgs($this->block, [ $panelData ]));
    }

    public function testDeleteAccordionItems()
    {
        $this->isPdoResult($this->block->deleteAccordionItems());
    }

    /**
     * @dataProvider provideRandomPositiveNumber
     */
    public function testDeletePanelById($panelId)
    {
        $this->assertTrue($this->block->deletePanelById($panelId));
    }

    /**
     * @dataProvider provideRandomPositiveNumber
     */
    public function testDeletePanelByIncorrectId($panelId)
    {
        $this->assertFalse($this->block->deletePanelById(-$panelId));
    }

    /**
     * @expectedException \PHPUnit_Framework_Error_Warning
     */
    public function testDeletePanelByNullId()
    {
        $this->assertFalse($this->block->deletePanelById());
    }

    public function testDeletePanelByEmptyString()
    {
        $this->assertFalse($this->block->deletePanelById(''));
    }

    public function testReorderPanels()
    {
        $this->assertFalse($this->block->reorderPanels([]));
    }

    public function provideRawPanelData()
    {
        return [
            [
                [
                    'panelId' => $this->getRandomPositiveNumber(),
                    'panelName' => $this->generateString(),
                    'panelContents' => $this->getGeneratedText()
                ]
            ]
        ];
    }
}

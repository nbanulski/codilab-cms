<?php
/**
 * proimagine-cms
 * Developer: Dzmitry Vensko
 * Date: 24.06.14 13:38
 */
namespace SiteTest\Block;

class BlockTestCase extends AbstractBlockTest
{
    /**
     * @var object
     */
    protected $block;

    /**
     * @var string
     */
    protected $blockClassName = null;

    public function setUp()
    {
        parent::setUp();

        if ($this->blockClassName !== null) {
            $r = new \ReflectionClass($this->blockClassName);
            $this->block = $r->newInstanceArgs($this->getArgs());
        }
    }

    protected function hasDynamicAttribute()
    {
        $this->assertObjectHasAttribute('is_dynamic', $this->block->getBlockData());
    }

    protected function getMethodReflection($name)
    {
        $class = new \ReflectionObject($this->block);
        $method = $class->getMethod($name);
        $method->setAccessible(true);
        return $method;
    }
} 

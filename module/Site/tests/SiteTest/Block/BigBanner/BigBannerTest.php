<?php

namespace SiteTest\Block\BigBanner;

use Site\Block\BigBanner\BigBanner;

/**
 * @coversDefaultClass \Site\Block\BigBanner\BigBanner
 */
class BigBannerTest extends \SiteTest\Block\BlockTestCase
{
    /**
     * @var BigBanner
     */
    protected $block;

    protected $blockClassName = BigBanner::class;

    public function testHasDynamicAttribute()
    {
        $this->hasDynamicAttribute();
    }

    /**
     * @depends testHasDynamicAttribute
     * @depends testGetContentId
     * @depends testDeleteAllBigBannerData
     */
    public function testOnBeforeDeleted()
    {
        $this->assertTrue(true);
    }

    public function testDeleteAllBigBannerData()
    {
        $this->isPdoResult($this->block->deleteAllBigBannerData());
    }

    /**
     * @depends testGetContentId
     */
    public function testGetPaginatorOfBannersToRender()
    {
        $this->assertInstanceOf(\Zend\Paginator\Paginator::class, $this->block->getPaginatorOfBannersToRender());
    }

    /**
     * @depends testGetContentId
     */
    public function testGetCountOfAllAssociatedBanners()
    {
        $testNumber = $this->getRandomNumber();

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'count' => $testNumber
            ]);

        $this->assertEquals($this->block->getCountOfAllAssociatedBanners(), $testNumber);

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([]);

        $this->assertFalse($this->block->getCountOfAllAssociatedBanners());
    }

    /**
     * @dataProvider provideRandomPositiveNumber
     */
    public function testGetBannerById($id)
    {
        $this->assertFalse($this->block->getBannerById(-$id));

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'banner_id' => $id
            ]);

        $result = $this->block->getBannerById($id);

        $this->assertInternalType('object', $result);

        if (is_object($result)) {
            $this->assertEquals($result->banner_id, $id);
        }
    }

    public function testIsExternalUrlValid()
    {
        $this->assertFalse($this->block->isExternalUrlValid(0));
        $this->assertFalse($this->block->isExternalUrlValid($this->getRandomNumber()));
        $this->assertFalse($this->block->isExternalUrlValid('some non-url string'));
        $this->assertFalse($this->block->isExternalUrlValid('http://'));
        $this->assertFalse($this->block->isExternalUrlValid('http://example.com'));

        $this->assertTrue($this->block->isExternalUrlValid('http://example.com/block_uri'));
    }

    /**
     * @dataProvider provideRawBannerData
     */
    public function testValidateBannerData($bannerData)
    {
        $testAttachmentId = $this->getRandomPositiveNumber();

        $this->mockAttachmentManager->expects($this->any())
            ->method('getAttachmentByUniqueHashForFileName')
            ->willReturn((object)[ 'attachment_id' => $testAttachmentId ]);

        $this->assertInstanceOf(\Site\Custom\FlexibleContainer::class, $this->block->validateBannerData($bannerData));

        $bannerDataTest = $bannerData;
        $bannerDataTest['bannerId'] = 0;
        $this->assertInstanceOf(\Site\Custom\FlexibleContainer::class, $this->block->validateBannerData($bannerData));

        $bannerDataTest = $bannerData;
        $bannerDataTest['bannerImages'] = [];
        $this->assertFalse($this->block->validateBannerData($bannerDataTest));

        $bannerDataTest = $bannerData;
        $bannerDataTest['bannerImages'] = [ [ 'uniqueHashForFileName' => '' ] ];
        $this->assertFalse($this->block->validateBannerData($bannerDataTest));

        $bannerDataTest = $bannerData;
        $bannerDataTest['bannerImages'] = [ [ 'uniqueHashForFileName' => 'some incorrect hash' ] ];
        $this->assertFalse($this->block->validateBannerData($bannerDataTest));

        $bannerDataTest = $bannerData;
        $bannerDataTest['bannerId'] = -$this->getRandomPositiveNumber();
        $this->assertFalse($this->block->validateBannerData($bannerDataTest));

        $bannerDataTest = $bannerData;
        $bannerDataTest['bannerTitle'] = '';
        $this->assertFalse($this->block->validateBannerData($bannerDataTest));
    }

    /**
     * @dataProvider provideRawBannerData
     * @depends testGetContentId
     * @depends testValidateBannerData
     */
    public function testCreateBanner($bannerData)
    {
        $insertId = $this->getRandomPositiveNumber();

        $this->mockDriver->expects($this->once())
            ->method('getLastGeneratedValue')
            ->willReturn($insertId);

        $this->assertGreaterThan(0, $this->block->createBanner($bannerData));
    }

    /**
     * @dataProvider provideRandomPositiveNumber
     * @depends testGetBannerById
     */
    public function testDeleteBannerById($bannerId)
    {
        $this->assertFalse($this->block->deleteBannerById(0));
        $this->assertFalse($this->block->deleteBannerById(-$bannerId));

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn(false);

        $this->assertTrue($this->block->deleteBannerById($bannerId));
    }

    /**
     * @depends testGetContentId
     * @depends testGetBannerById
     */
    public function testReorderBanners()
    {
        $banners = [];
        for ($i=0; $i < $this->getRandomNumber(1,20); $i++) {
            $banners[] = $this->getRandomNumber(1,1000);
        }

        $this->assertTrue($this->block->reorderBanners($banners));
    }

    /**
     * @dataProvider provideRandomPositiveNumber
     * @depends testGetBannerById
     */
    public function testToggleBannerPublished($bannerId)
    {
        $testPublished = $this->getRandomNumber(0,1);

        $this->assertFalse($this->block->toggleBannerPublished(-$bannerId, $testPublished));

        $this->mockResult->expects($this->exactly(2))
            ->method('current')
            ->willReturn([
                'is_published' => $testPublished
            ]);

        $this->assertTrue($this->block->toggleBannerPublished($bannerId, $testPublished));
    }

    /**
     * @dataProvider provideRawBannerData
     * @depends testGetContentId
     * @depends testValidateBannerData
     * @depends testGetBannerById
     */
    public function testUpdateBanner($bannerData)
    {
        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'page_id' => $bannerData['bannerPageId'],
                'attachment_id' => $bannerData['bannerAttachmentId'],
                'is_published' => $bannerData['bannerIsPublished'],
                'url_address_type' => $bannerData['bannerUrlAddressType'],
                'last_update_author_id' => $bannerData['bannerLastUpdateAuthorId'],
                'external_url' => $bannerData['bannerExternalUrl'],
                'title' => $bannerData['bannerTitle'],
                'contents' => $bannerData['bannerContents'],
            ]);

        $this->assertTrue($this->block->updateBanner($bannerData));
    }

    /**
     * @depends testGetContentId
     * @depends testGetBannerById
     */
    public function testUpdateBannerContents()
    {
        $testBannerId = $this->getRandomPositiveNumber();
        $testContents = $this->getGeneratedText();

        $this->assertFalse($this->block->updateBannerContents(-$testBannerId, $testContents));
        $this->assertFalse($this->block->updateBannerContents(0, $testContents));

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'last_update_author_id' => $this->mockUser->user_id,
                'contents' => trim((string)$testContents),
            ]);

        $this->assertTrue($this->block->updateBannerContents($testBannerId, $testContents));
    }

    /**
     * @depends testGetContentId
     * @depends testGetBannerById
     */
    public function testUpdateBannerTitle()
    {
        $testBannerId = $this->getRandomPositiveNumber();
        $testTitle = $this->generateString();

        $this->assertFalse($this->block->updateBannerTitle(-$testBannerId, $testTitle));
        $this->assertFalse($this->block->updateBannerTitle(0, $testTitle));
        $this->assertFalse($this->block->updateBannerTitle($testBannerId, '<b onClick="alert(\'XSS\');"></b>'));

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'last_update_author_id' => $this->mockUser->user_id,
                'title' => trim(strip_tags((string)$testTitle)),
            ]);

        $this->assertTrue($this->block->updateBannerTitle($testBannerId, $testTitle));
    }

    public function provideRawBannerData()
    {
        return [
          [
              [
                  'bannerId' => $this->getRandomPositiveNumber(),
                  'bannerPageId' => $this->getRandomPositiveNumber(),
                  'bannerAttachmentId' => $this->getRandomPositiveNumber(),
                  'bannerIsPublished' => $this->getRandomNumber(0,1),
                  'bannerIsTitleVisible' => $this->getRandomNumber(0,1),
                  'bannerIsContentsVisible' => $this->getRandomNumber(0,1),
                  'bannerOrder' => $this->getRandomNumber(),
                  'bannerUrlAddressType' => $this->generateString(),
                  'bannerExternalUrl' => $this->generateString(),
                  'bannerTitle' => $this->generateString(),
                  'bannerContents' => $this->getGeneratedText(),
                  'bannerImages' => [
                      [
                        'uniqueHashForFileName' => $this->generateString(),
                      ]
                  ],
              ]
          ]
        ];
    }
}

<?php

namespace SiteTest\Block\Employees;

use Site\Block\Employees\Employees;

/**
 * @coversDefaultClass \Site\Block\Employees\Employees
 */
class EmployeesTest extends \SiteTest\Block\BlockTestCase
{
    /**
     * @var Employees
     */
    protected $block;

    protected $blockClassName = Employees::class;

    /**
     * @depends testHasDynamicAttribute
     * @depends testDeleteAllEmployees
     * @depends testDeleteAllDegrees
     * @depends testDeleteBlockRecordFromSearchIndex
     */
    public function testOnBeforeDeleted()
    {
        $this->assertTrue(true);
    }

    public function testHasDynamicAttribute()
    {
        $this->hasDynamicAttribute();
    }

    public function testDeleteAllEmployees()
    {
        $this->isPdoResult($this->block->deleteAllEmployees());
    }

    public function testDeleteAllDegrees()
    {
        $this->isPdoResult($this->block->deleteAllDegrees());
    }

    public function testGetAllEmployees()
    {
        $this->isHydratingResultSet($this->block->getAllEmployees());
    }

    /**
     * @depends testGetAllEmployees
     * @depends testGetContentId
     * @depends testGenerateCleanUrlFromEmployeeNameAndSurname
     * @depends testUpdateBlockRecordInSearchIndex
     * @covers Employees::updateSearchIndex
     */
    public function testUpdateSearchIndex()
    {
        $this->assertTrue(true);
    }

    /**
     * @depends testGenerateCleanUrlFromText
     * @covers Employees::generateCleanUrlFromEmployeeNameAndSurname
     */
    public function testGenerateCleanUrlFromEmployeeNameAndSurname()
    {
        $this->assertTrue(true);
    }

    /**
     * @depends testChangeOfRenderingMode
     * @covers Employees::hideEmployees
     */
    public function testHideEmployees()
    {
        $this->assertTrue(true);
    }

    /**
     * @depends testGetSettings
     * @depends testGetContentId
     */
    public function testRenderDegreesManagement()
    {
        $pageNumber = $this->getRandomPositiveNumber();

        $mockRequest = $this->getMock(\Zend\Http\Request::class);
        $mockRequest->expects($this->any())
            ->method('getPost')
            ->with('pageNumber')
            ->willReturn($pageNumber);
        $mockRequest->expects($this->any())
            ->method('getPost')
            ->with('itemCountPerPage')
            ->willReturn($this->getRandomNumber(1,30));
        $mockRequest->expects($this->any())
            ->method('getPost')
            ->with('includePagesFrom')
            ->willReturn($this->getRandomNumber(''));

        $this->phpRenderer->expects($this->any())
            ->method('render')
            ->willReturn($pageNumber);

        $this->assertEquals($pageNumber, $this->block->renderDegreesManagement($mockRequest, true));

        ob_start();
        $this->block->renderDegreesManagement($mockRequest, false);
        $this->assertEquals($pageNumber, ob_get_clean());
    }

    /**
     * @dataProvider provideRandomPositiveNumber
     */
    public function testGetDegreeById($degreeId)
    {
        $this->assertFalse($this->block->getDegreeById(0));
        $this->assertFalse($this->block->getDegreeById(-$degreeId));

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'degree_id' => $degreeId
            ]);

        $result = $this->block->getDegreeById($degreeId);

        $this->assertInternalType('object', $result);
        $this->assertObjectHasAttribute('degree_id', $result);
        $this->assertEquals($degreeId, $result->degree_id);
    }

    /**
     * @depends testGetSettings
     * @depends testGetDegreeById
     */
    public function testRenderAddEditDegree()
    {
        $degreeId = $this->getRandomPositiveNumber();
        $degreeTitle = $this->generateString();
        $pageNumber = $this->getRandomPositiveNumber();

        $mockRequest = $this->getMock(\Zend\Http\Request::class);
        $mockRequest->expects($this->any())
            ->method('getPost')
            ->with('pageNumber')
            ->willReturn($pageNumber);
        $mockRequest->expects($this->any())
            ->method('getPost')
            ->with('itemCountPerPage')
            ->willReturn($this->getRandomNumber(1,30));
        $mockRequest->expects($this->any())
            ->method('getPost')
            ->with('includePagesFrom')
            ->willReturn($this->getRandomNumber(''));
        $mockRequest->expects($this->any())
            ->method('getPost')
            ->with('degreeId')
            ->willReturn($degreeId);

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'degree_id' => $degreeId,
                'degreeTitle' => $degreeTitle,
                'degreeIsPublished' => $this->generateString(),
            ]);

        $this->phpRenderer->expects($this->any())
            ->method('render')
            ->willReturn($degreeTitle);

        $this->assertEquals($pageNumber, $this->block->renderAddEditDegree($mockRequest, true));

        ob_start();
        $this->block->renderAddEditDegree($mockRequest, false);
        $this->assertEquals($degreeTitle, ob_get_clean());
    }

    public function testConvertFoundEmployeesToArray()
    {
        $this->assertFalse($this->block->convertFoundEmployeesToArray([]));

        $testArray = [
            new \Site\Custom\FlexibleContainer(),
            new \Site\Custom\FlexibleContainer()
        ];

        $this->assertEquals([ [], [] ], $this->block->convertFoundEmployeesToArray($testArray));
    }

    public function testDivideFoundEmployeesByCriteria()
    {
        foreach ([ 'degree_title', 'position' ] as $criterion) {

            switch ($criterion) {
                case "degree_title":
                    $method = 'divideFoundEmployeesByDegree';
                    break;
                case "position":
                default:
                    $method = 'divideFoundEmployeesByEmployeePosition';
                    break;
            }

            $this->assertFalse($this->block->$method([]));

            $testArray = [
                new \Site\Custom\FlexibleContainer([ $criterion => 'z' ]),
                new \Site\Custom\FlexibleContainer([ $criterion => 'a' ])
            ];

            $this->assertEquals([
                'a' => [ [ $criterion => 'a' ] ],
                'z' => [ [ $criterion => 'z' ] ],
            ], $this->block->$method($testArray));

        }
    }

    public function testDivideFoundEmployeesByFirstLetter()
    {
        $this->assertFalse($this->block->divideFoundEmployeesByFirstLetter([]));

        $firstEmployee = 'Z'.$this->generateString();
        $lastEmployee = 'A'.$this->generateString();

        $testArray = [
            new \Site\Custom\FlexibleContainer([ 'name_and_surname' => $firstEmployee ]),
            new \Site\Custom\FlexibleContainer([ 'name_and_surname' => $lastEmployee ])
        ];

        $this->assertEquals([
            'A' => [ [ 'name_and_surname' => $lastEmployee ] ],
            'Z' => [ [ 'name_and_surname' => $firstEmployee ] ]
        ], $this->block->divideFoundEmployeesByFirstLetter($testArray));
    }

    /**
     * @depends testGetContentId
     * @depends testGetLastRecognizedEmployeeDetailsPageUrl
     */
    public function testFindEmployees()
    {
        $alphabet = 'AĄBCĆDEĘFGHIJKLŁMNŃOÓPQRSŚTUWXYZŹŻ';
        $letter = $this->getRandomNumber(0, mb_strlen($alphabet)-1);
        $degreeId = $this->getRandomPositiveNumber();

        $this->assertFalse($this->block->findEmployees('', ''));
        $this->assertFalse($this->block->findEmployees('Б', ''));
        $this->assertFalse($this->block->findEmployees($letter, '', 0));
        $this->assertFalse($this->block->findEmployees($letter, '', -$degreeId));

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'degree_id' => $degreeId
            ]);

        $this->isHydratingResultSet($this->block->findEmployees($letter, ''));
        $this->isHydratingResultSet($this->block->findEmployees($letter, '', $degreeId));
        $this->isHydratingResultSet($this->block->findEmployees($letter, $this->generateString(), $degreeId));
    }

    /**
     * @depends testGetSettings
     */
    public function testGetLastRecognizedEmployeeDetailsPageUrl()
    {

        $testCleanUrl = $this->generateString();

        $this->mockPageManager->expects($this->once())
            ->method('getFullPageCleanUrlByPageId')
            ->willReturn($testCleanUrl);

        $this->assertEquals($testCleanUrl, $this->block->getLastRecognizedEmployeeDetailsPageUrl());
    }

    public function testGetDegrees()
    {
        $this->isHydratingResultSet($this->block->getDegrees());
    }

    public function testGetDegreeCount()
    {
        $count = $this->getRandomPositiveNumber();

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'count' => $count
            ]);

        $this->assertEquals($count, $this->block->getDegreeCount());
    }

    public function testGetEmployeeByIdAndContentId()
    {
        $employeeId = $this->getRandomPositiveNumber();
        $contentId = $this->getRandomPositiveNumber();

        $this->assertFalse($this->block->getEmployeeByIdAndContentId(0, $contentId));
        $this->assertFalse($this->block->getEmployeeByIdAndContentId($employeeId, -$contentId));

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'employee_id' => $employeeId
            ]);

        $result = $this->block->getEmployeeByIdAndContentId($employeeId, $contentId);

        $this->assertInternalType('object', $result);
        $this->assertObjectHasAttribute('employee_id', $result);
        $this->assertEquals($employeeId, $result->employee_id);
    }

    public function testGetEmployeePartialDataFromEmployeePartialDataString()
    {
        $employeeId = $this->getRandomPositiveNumber();
        $contentId = $this->getRandomPositiveNumber();
        $randomString = $this->generateString();

        $input = $randomString.'-some-random-string-'.$contentId.'-'.$employeeId;

        $this->assertEquals(
            [ $randomString.'-some-random-string', $contentId, $employeeId ],
            $this->block->getEmployeePartialDataFromEmployeePartialDataString($input)
        );
    }

    /**
     * @depends testGetParametersForCurrentPageFromRequest
     * @depends testGetEmployeePartialDataFromEmployeePartialDataString
     */
    public function testGetEmployeePartialDataFromRequest()
    {
        $this->assertTrue(true);
    }

    /**
     * @depends testGetContentId
     */
    public function testGetEmployeeDegreesForDropdown()
    {
        $degreeId = $this->getRandomPositiveNumber();
        $title = $this->generateString();

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'degree_id' => $degreeId,
                'title' => $title,
            ]);

        $result = $this->block->getEmployeeDegreesForDropdown();

        $this->assertInternalType('array', $result);
        $this->assertCount(2, $result);
        $this->assertArrayHasKey($degreeId, $result);
        $this->assertEquals($result[$degreeId], $title);
    }

    /**
     * @depends testGetContentId
     */
    public function testGetEmployeesPositionsForDropdown()
    {
        $position = $this->getRandomPositiveNumber();

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'position' => $position,
            ]);

        $result = $this->block->getEmployeesPositionsForDropdown();

        $this->assertInternalType('array', $result);
        $this->assertCount(2, $result);
        $this->assertContains($position, $result);
    }

    /**
     * @depends testConvertToFlexibleContainer
     */
    public function testFormatDegreeDataAndReturnIfAreValid()
    {
        $data = [
            'degreeId' => $this->getRandomPositiveNumber(),
            'degreeIsPublished' => $this->getRandomNumber(0,1),
            'degreeTitle' => $this->generateString(),
            'degreeLastUpdateAuthorId' => $this->mockUser->user_id,
        ];

        $dataCopy = $data;
        $dataCopy['degreeTitle'] = '';
        $this->assertFalse($this->block->formatDegreeDataAndReturnIfAreValid($dataCopy));

        $dataCopy = $data;
        $dataCopy['degreeId'] = 0;
        $this->assertFalse($this->block->formatDegreeDataAndReturnIfAreValid($dataCopy));

        $result = $this->block->formatDegreeDataAndReturnIfAreValid($data);

        $this->assertInternalType('object', $result);
        $this->assertObjectHasAttribute('degreeId', $result);
        $this->assertObjectHasAttribute('degreeIsPublished', $result);
        $this->assertObjectHasAttribute('degreeTitle', $result);
        $this->assertObjectHasAttribute('degreeLastUpdateAuthorId', $result);

        $this->assertEquals($result->degreeId, $data['degreeId']);
        $this->assertEquals($result->degreeIsPublished, $data['degreeIsPublished']);
        $this->assertEquals($result->degreeTitle, $data['degreeTitle']);
        $this->assertEquals($result->degreeLastUpdateAuthorId, $data['degreeLastUpdateAuthorId']);
    }

    public function testFormatEmployeeDataAndReturnIfAreValid()
    {
        $data = [
            'employeeId' => $this->getRandomPositiveNumber(),
            'employeeDegreeId' => $this->getRandomPositiveNumber(),
            'employeeAttachmentId' => $this->getRandomPositiveNumber(),
            'employeeIsPublished' => $this->getRandomNumber(0,1),
            'employeeNameAndSurname' => $this->generateString(),
            'employeePosition' => $this->generateString(),
            'employeeAbout' => $this->generateString(),
            'employeeScientificCareer' => $this->generateString(),
            'employeeTheMostImportantPublications' => $this->generateString(),
            'employeeContactInfo' => $this->generateString(),
            'employeeImage' => [
                [
                    'uniqueHashForFileName' => sha1($this->generateString())
                ]
            ]
        ];

        $this->mockAttachmentManager->expects($this->any())
            ->method('getAttachmentByUniqueHashForFileName')
            ->willReturn((object)[ 'attachment_id' => $this->getRandomPositiveNumber() ]);

        $result = $this->block->formatEmployeeDataAndReturnIfAreValid($data);

        $this->assertInternalType('object', $result);
        $this->assertEquals($result->employeeId, $data['employeeId']);
    }

    /**
     * @depends testFormatDegreeDataAndReturnIfAreValid
     * @depends testUpdateExistingDegree
     * @depends testCreateNewDegree
     */
    public function testAddOrEditDegree()
    {
        $this->assertTrue(true);
    }

    /**
     * @depends testGetDegreeById
     * @depends testGetContentId
     */
    public function testUpdateExistingDegree()
    {
        $data = [
            'degreeId' => $this->getRandomPositiveNumber(),
            'degreeIsPublished' => $this->getRandomNumber(0,1),
            'degreeTitle' => $this->generateString(),
            'degreeLastUpdateAuthorId' => $this->mockUser->user_id,
            'degreeLastUpdateDate' => $this->generateString()
        ];

        $this->mockResult->expects($this->once())
            ->method('current')
            ->willReturn([
                'is_published' => $data['degreeIsPublished'],
                'last_update_author_id' => $data['degreeLastUpdateAuthorId'],
                'title' => $data['degreeTitle'],
            ]);

        $class = new \ReflectionObject($this->block);
        $method = $class->getMethod('updateExistingDegree');
        $method->setAccessible(true);

        $this->assertTrue($method->invokeArgs($this->block, [ $data ]));
    }

    /**
     * @depends testGetContentId
     */
    public function testCreateNewDegree()
    {
        $data = [
            'degreeIsPublished' => $this->getRandomNumber(0,1),
            'degreeTitle' => $this->generateString(),
            'degreeLastUpdateAuthorId' => $this->mockUser->user_id,
            'degreeLastUpdateDate' => $this->generateString()
        ];

        $newDegreeId = $this->getRandomPositiveNumber();

        $this->mockDatabaseManager->expects($this->once())
            ->method('executeInsertAndGetLastInsertId')
            ->willReturn($newDegreeId);

        $class = new \ReflectionObject($this->block);
        $method = $class->getMethod('createNewDegree');
        $method->setAccessible(true);

        $this->assertEquals($newDegreeId, $method->invokeArgs($this->block, [ $data ]));
    }
}

<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Site;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\ModuleManager;
use Site\Service\ErrorHandling as ErrorHandlingService;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream as LogWriterStream;
use Zend\Session\Config\SessionConfig;
use Zend\Session\SessionManager;
use Zend\Session\Container;
use Zend\Console\Adapter\AdapterInterface as Console;

class Module
{
    protected $defaultLanguageIsoCode = null;
    protected $language = null;
    protected $databaseManager = null;

    public function init(ModuleManager $moduleManager)
    {
        date_default_timezone_set('UTC');
        mb_internal_encoding('utf8');

        $eventManager = $moduleManager->getEventManager();
        $sharedEvents = $eventManager->getSharedManager();

        $obj = $this;
        $sharedEvents->attach(array('Auth', __NAMESPACE__), MvcEvent::EVENT_DISPATCH, function($e) use($obj)
        {
            $controller = $e->getTarget();
            $obj->setControllerBasicData($e, $controller);
        }, 100);
    }

    public function onBootstrap(MvcEvent $e)
    {
        $serviceManager = $e->getApplication()->getServiceManager();
        $config = $serviceManager->get('config');
        $this->applyPhpSettings($config);

        $sessionConfig = new SessionConfig();
        $sessionConfig->setOptions(isset($config['session']) ? $config['session'] : array());
        $sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();
        Container::setDefaultManager($sessionManager);

        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $this->initAcl($e);
        $eventManager->attach(MvcEvent::EVENT_ROUTE, array($this, 'checkAcl'));

        $obj = $this;
        $eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, function(MvcEvent $e) use ($obj, $eventManager)
        {
            $response = $e->getResponse();
            $serviceManager = $e->getApplication()->getServiceManager();
            $databaseManager = $serviceManager->get('DatabaseManager');
            $config = $serviceManager->get('config');
            $cmsConfig = new \Site\Custom\FlexibleContainer($config['cms']);
            $languageIsoCode = $cmsConfig->default_language_iso_code;
            $defaultLanguageIsoCode = $cmsConfig->default_language_iso_code;
            $languages = $obj->getLanguages($databaseManager);
            $language = $obj->getLanguageInfo($databaseManager, $languageIsoCode);
            $defaultLanguage = $this->getLanguageInfo($databaseManager, $defaultLanguageIsoCode);

            $page = new \Site\Custom\FlexibleContainer();
            $page->title = '';

            if ($response instanceof \Zend\Console\Response)
            {
                $page->title = $response->getErrorLevel();
            }
            else
            {
                $page->title = $response->getStatusCode();
            }

            $translator = $serviceManager->get('translator');
            $translator->setLocale($language->zend2_locale);

            $viewModel = $e->getViewModel();
            $viewModel->setVariables(
                array(
                    'language' => $language,
                    'languages' => $languages->buffer(),
                    'languageCount' => $languages->count(),
                    'defaultLanguage' => $defaultLanguage,
                    'page' => $page
                )
            );

            //$exception = $e->getResult()->exception;
            $exception = $e->getParam('exception');
            if ($exception)
            {
                $service = $serviceManager->get('Site\Service\ErrorHandling');
                $service->logException($exception);
            }
        });

        $eventManager->attach(MvcEvent::EVENT_FINISH, function(MvcEvent $e)
        {
            $response = $e->getResponse();
            if ($response instanceof \Zend\Http\AbstractMessage)
            {
                $header = new \Zend\Http\Header\GenericHeader();
                $header->name = 'X-UA-Compatible';
                $header->value = 'chrome=1';
                $response->getHeaders()->addHeader($header);
            }
        }, 500);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
                'dropdown' => function ($helperPluginManager){
                    $serviceLocator = $helperPluginManager->getServiceLocator();
                    $phpRenderer = $serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                    $viewHelper = new View\Helper\Dropdown();
                    $viewHelper->setPhpRenderer($phpRenderer);

                    return $viewHelper;
                },
                'dropdownMultiselect' => function ($helperPluginManager){
                    $serviceLocator = $helperPluginManager->getServiceLocator();
                    $phpRenderer = $serviceLocator->get('\Zend\View\Renderer\PhpRenderer');
                    $viewHelper = new View\Helper\DropdownMultiselect();
                    $viewHelper->setPhpRenderer($phpRenderer);

                    return $viewHelper;
                },
                'renderBlocks' => function ($helperPluginManager){
                        $serviceLocator = $helperPluginManager->getServiceLocator();
                        $request = $serviceLocator->get('Request');
                    return new \Site\View\Helper\RenderBlocks($request);
                }
            ),
            'invokables' => array(
                'arrayToJsObject' => 'Site\View\Helper\ArrayToJsObject',
                'checkbox' => 'Site\View\Helper\Checkbox',
                'cutText' => 'Site\View\Helper\CutText',
                'email' => 'Site\View\Helper\Email',
                'escapeForHtml' => 'Site\View\Helper\EscapeForHtml',
                'hidden' => 'Site\View\Helper\Hidden',
                'input' => 'Site\View\Helper\Input',
                'password' => 'Site\View\Helper\Password',
                'radio' => 'Site\View\Helper\Radio',
                'text' => 'Site\View\Helper\Text',
                'textarea' => 'Site\View\Helper\Textarea'
            )
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Site\Service\ErrorHandling' => function($sm)
                {
                    $logger = $sm->get('Zend\Log');
                    $service = new ErrorHandlingService($logger);

                    return $service;
                },
                'Zend\Log' => function ($sm)
                {

                    $filePath = './data/logs/exceptions_' . date('Y-m-d') . '.txt';
                    $writer = new LogWriterStream($filePath);
                    $log = new Logger();
                    $log->addWriter($writer);

                    return $log;
                },
            ),
        );
    }

    public function getConsoleUsage(Console $console)
    {
        return array(
            // Describe available commands:
            'update [info|start|stop] [--verbose|-v]' => 'Start, stop or get info about search indexing process',

            // Describe expected parameters:
            array('info', 'Prints information about current indexing process'),
            array('start', 'Starts indexing (only if not started yet)'),
            array('stop', 'Stops indexing'),
            array('--verbose|-v', '(optional) turn on verbose mode'),
        );
    }

    protected function initAcl(MvcEvent $e)
    {
        $acl = $e->getApplication()->getServiceManager()->get('Acl');
        $config = include __DIR__ . '/config/acl.php';

        foreach ($config['roles'] as $roleName => $parentName)
        {
            $role = new \Zend\Permissions\Acl\Role\GenericRole($roleName);
            $parent = ($parentName != '') ? new \Zend\Permissions\Acl\Role\GenericRole($parentName) : null;

            $acl->addRole($role, $parent);
        }

        $this->allowResources($acl, $config['resources']);
        $this->denyResources($acl, $config['resources']);

        $e->getViewModel()->acl = $acl;
    }

    protected function allowResources(&$acl, &$rules)
    {
        if (isset($rules['allow']) && $rules['allow'])
        {
            foreach ($rules['allow'] as $role => $resources)
            {
                foreach ($resources as $resource => $privileges)
                {
                    if (!$acl->hasResource($resource))
                    {
                        $acl->addResource(new \Zend\Permissions\Acl\Resource\GenericResource($resource));
                    }

                    $acl->allow($role, $resource, $privileges);
                }
            }
        }
    }

    protected function denyResources(&$acl, &$rules)
    {
        if (isset($rules['deny']) && $rules['deny'])
        {
            foreach ($rules['deny'] as $role => $resources)
            {
                foreach ($resources as $resource => $privileges)
                {
                    if (!$acl->hasResource($resource))
                    {
                        $acl->addResource(new \Zend\Permissions\Acl\Resource\GenericResource($resource));
                    }

                    $acl->deny($role, $resource, $privileges);
                }
            }
        }
    }

    public function checkAcl(MvcEvent $e)
    {
        $routeMatch = $e->getRouteMatch();
        $route = mb_strtolower($routeMatch->getMatchedRouteName());
        $module = mb_strtolower($routeMatch->getParam('module'));
        $controller = mb_strtolower($routeMatch->getParam('controller'));
        $action = $routeMatch->getParam('action');

        if (mb_strpos($controller, '\\') !== false)
        {
            $namespaceParts = explode('\\', $controller);
            $module = $namespaceParts[0];
            $controller = $namespaceParts[count($namespaceParts) - 1];
        }

        $serviceManager = $e->getApplication()->getServiceManager();
        $authService = $serviceManager->get('Zend\Authentication\AuthenticationService');
        $sessionData = new \Site\Component\BasicSessionData($authService->getIdentity());

        /**
         *  If user has no session then we can:
         *     redirect him to 404 page, but we lose functionality of dynamically showing jAlert with
         *     message "Session has expired". Of course this is more secure way.
         *  OR
         *     do nothing, then in every controller and action we SHOULD check if session exist. If does not - we
         *     SHOULD show jAlert with message "Session has expired" at every action that user tries to do.
         *  Currently the second approach id used.
         */
        if ($sessionData)
        {
            $userRole = $sessionData->account->role;
            $acl = $e->getViewModel()->acl;
            $filter = new \Zend\Filter\Word\CamelCaseToDash();
            $action = mb_strtolower($filter->filter($action));

            /*
              echo '<br><br><br>';
              //echo 'session data: ';
              //var_dump($sessionData);
              echo 'userRole: ';
              var_dump($userRole);
              echo 'route: ';
              var_dump($route);
              echo 'module: ';
              var_dump($module);
              echo 'controller: ';
              var_dump($controller);
              echo 'action: ';
              var_dump($action);
              echo 'params: ';
              var_dump($routeMatch->getParams());
              echo 'resource: ';
              var_dump($module . '/' . $controller);
              echo 'allowed: ';
              var_dump($acl->isAllowed($userRole, $module . '/' . $controller, $action));
            */

            if (!$acl->isAllowed($userRole, $module . '/' . $controller, $action))
            {
                $response = $e->getResponse();
                $response->getHeaders()->addHeaderLine('Location', $e->getRequest()->getBaseUrl() . '/404');
                $response->setStatusCode(404);

                exit; // DO NOT remove this line!
            }

            if (($module == 'site') && ($controller == 'page'))
            {
                $referer = $e->getRequest()->getHeader('referer');
                if ($referer)
                {
                    $session = new \Zend\Session\Container('referer');
                    $session->uri = $referer->getUri();
                }
            }
        }
    }

    public function setControllerBasicData(MvcEvent $e, &$controller)
    {
        $serviceManager = $e->getApplication()->getServiceManager();
        $databaseManager = $serviceManager->get('DatabaseManager');
        $viewModel = $e->getViewModel();
        $config = $serviceManager->get('config');
        $cmsConfig = new \Site\Custom\FlexibleContainer($config['cms']);

        $languages = $this->getLanguages($databaseManager);
        $language = $this->getCurrentLanguageInfo($databaseManager, $controller, $cmsConfig);

        $defaultLanguageIsoCode = $cmsConfig->default_language_iso_code;
        $defaultLanguage = $this->getLanguageInfo($databaseManager, $defaultLanguageIsoCode);

        $translator = $serviceManager->get('translator');
        $translator->setLocale($language->zend2_locale);
        $serviceManager->get('ViewHelperManager')->get('translate')->setTranslator($translator);

        $controller->cmsConfig = $cmsConfig;
        $controller->serviceLocator = $serviceManager;
        $controller->dbAdapter = $controller->getDbAdapter();
        $controller->sql = $serviceManager->get('Sql');
        $controller->language = $language;
        $controller->defaultLanguage = $defaultLanguage;

        $viewModel->languages = $languages->buffer();
        $viewModel->languageCount = $languages->count();
        $viewModel->language = $controller->language;
        $viewModel->defaultLanguage = $defaultLanguage;
        $viewModel->page = new \Site\Custom\FlexibleContainer();
        $viewModel->page->title = '';
    }

    public function getLanguages($databaseManager)
    {
        $prefixColumnsWithTable = true;

        $select = $databaseManager->getSql()->select();
        $select
            ->from('languages')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable);
        $resultSet = $databaseManager->getHydratedResultSet($select);

        return $resultSet;
    }

    protected function getLanguageFromDatabaseByIsoCode($databaseManager, $isoCode)
    {
        $prefixColumnsWithTable = true;

        $select = $databaseManager->getSql()->select();
        $select
            ->from('languages')
            ->columns(array($select::SQL_STAR), $prefixColumnsWithTable)
            ->where(array('iso_code' => (string)$isoCode));
        $resultSet = $databaseManager->getHydratedResultSet($select);
        if ($resultSet)
        {
            return $resultSet->current();
        }

        return false;
    }

    protected function getCurrentLanguageInfo($databaseManager, $controller, $cmsConfg)
    {
        $defaultLanguageIsoCode = strtolower($cmsConfg->default_language_iso_code);

        $languageIsoCode = $controller->params()->fromRoute('lang', null);
        if (!preg_match('/^[a-z]{3}$/i', $languageIsoCode))
        {
            $languageIsoCode = $defaultLanguageIsoCode;
        }

        // Get language information from database for current language ISO code:
        $language = $this->getLanguageFromDatabaseByIsoCode($databaseManager, $languageIsoCode);

        if (!$language && ($languageIsoCode != $defaultLanguageIsoCode))
        {
            // Get language information from database for default language ISO code:
            $languageIsoCode = $defaultLanguageIsoCode;
            $language = $this->getLanguageFromDatabaseByIsoCode($databaseManager, $languageIsoCode);
        }

        if (!$language)
        {
            $this->throwExceptionOnNotFoundLanguage($languageIsoCode);
        }

        return (object)$language;
    }

    public function getLanguageInfo($databaseManager, $languageIsoCode)
    {
        $language = $this->getLanguageFromDatabaseByIsoCode($databaseManager, $languageIsoCode);
        if (!$language)
        {
            $this->throwExceptionOnNotFoundLanguage($languageIsoCode);
        }

        return (object)$language;
    }

    protected function applyPhpSettings($config)
    {
        $phpSettings = isset($config->phpSettings) ? $config->phpSettings : array();
        if ($phpSettings)
        {
            foreach ($phpSettings as $key => $value)
            {
                ini_set($key, $value);
            }
        }
    }

    protected function throwExceptionOnNotFoundLanguage($languageIsoCode)
    {
        throw new \Exception('Default language "' . $languageIsoCode . '" not exists in database!');
    }
}
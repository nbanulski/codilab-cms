<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'module' => 'site',
                        'controller' => 'page',
                        'action' => 'index'
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '[:lang[/:controller[/:action[,:parameters]]]]',
                            'constraints' => array(
                                'lang' => '[a-zA-Z]{3}',
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                // Numbers are allowed in first bracket for URLs to pages:
                                'action' => '[a-zA-Z0-9][a-zA-Z0-9_-]*',
                                'parameters' => '[a-zA-Z0-9_!%\(\)\.,-]*'
                            ),
                            'defaults' => array(
                                'module' => 'site',
                                'lang' => 'eng',
                                'controller' => 'page',
                                'action' => 'index',
                                'parameters' => ''
                            )
                        )
                    ),
                    'subPageWithParams' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => ':lang/:controller/:action/:subPage[,:parameters]',
                            'constraints' => array(
                                'lang' => '[a-zA-Z]{3}',
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                // Numbers are allowed in first bracket for URLs to pages:
                                'action' => '[a-zA-Z0-9][a-zA-Z0-9_-]*',
                                'subPage' => '[a-zA-Z0-9_-]*',
                                'parameters' => '[a-zA-Z0-9_!%\(\)\.,-]*'
                            ),
                            'defaults' => array(
                                'module' => 'site',
                                'lang' => 'eng',
                                'controller' => 'page',
                                'action' => 'index',
                                'subPage' => '',
                                'parameters' => ''
                            )
                        )
                    )
                )
            ),
        /* 'new-password' => array(
          'type' => 'Segment',
          'options' => array(
          'route' => '/pol/user/new-password/code/:code',
          'constraints' => array(
          //'code' => '[abcdefghijkmnopqrstuvwxyz23456789ABCDEFGHIJKLMNPQRSTUVWXYZ]{8}',
          'code' => '[a-zA-Z0-9]*',
          ),
          'defaults' => array(
          'module' => 'site',
          'lang' => 'eng',
          'controller' => 'user',
          'action' => 'new-password',
          'code' => 'aaaaaaaa'
          )
          )
          ) */
        )
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
        'invokables' => array(
            // Keys are the service names
            // Values are valid class names to instantiate.
            'Acl' => 'Zend\Permissions\Acl\Acl',
        ),
        'factories' => array(
            'Sql' => function ($serviceManager)
            {
                $adapter = $serviceManager->get('\Zend\Db\Adapter\Adapter');

                return new \Zend\Db\Sql\Sql($adapter);
            },
            'DatabaseManager' => function ($serviceManager)
            {
                $sql = $serviceManager->get('Sql');

                return new Site\Controller\DatabaseManager($sql);
            },
            'GlobalConfig' => function($serviceManager)
            {
                $config = $serviceManager->get('config');

                return new Site\Custom\GlobalConfig($config);
            }
        )
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Page' => 'Site\Controller\PageController',
            'Admin' => 'Site\Controller\AdminController',
            'Block' => 'Site\Controller\BlockController',
            'CodeTest' => 'Site\Controller\CodeTestController',
            'GetFile' => 'Site\Controller\GetFileController',
            'Language' => 'Site\Controller\LanguageController',
            'Dialog' => 'Site\Controller\DialogController',
            'Upload' => 'Site\Controller\UploadController',
            'User' => 'Site\Controller\UserController',
            'SearchIndex' => 'Site\Controller\SearchIndexController',
            'Settings' => 'Site\Controller\SettingsController',
            'FeedbackMailController' => 'Site\Controller\FeedbackMailController',
            // Block specific controllers:
            'BlockBachelorStudies' => 'Site\Controller\BlockBachelorStudiesController',
            'BlockEmployees' => 'Site\Controller\BlockEmployeesController',
            'BlockEmployeeDetails' => 'Site\Controller\BlockEmployeeDetailsController',
            'BlockEventList' => 'Site\Controller\BlockEventListController',
            'BlockEventDetails' => 'Site\Controller\BlockEventDetailsController',
            'BlockImageGallery' => 'Site\Controller\BlockImageGalleryController',
            'BlockImageSlider' => 'Site\Controller\BlockImageSliderController',
            'BlockNewsAdd' => 'Site\Controller\BlockNewsAddController',
            'BlockNewsList' => 'Site\Controller\BlockNewsListController',
            'BlockNewsDetails' => 'Site\Controller\BlockNewsDetailsController',
            'BlockNavigationMenu' => 'Site\Controller\BlockNavigationMenuController',
            'BlockOffer' => 'Site\Controller\BlockOfferController',
            'BlockPostgraduateStudies' => 'Site\Controller\BlockPostgraduateStudiesController',
            'BlockImagesPile' => 'Site\Controller\BlockImagesPileController',
            'BlockPopup' => 'Site\Controller\BlockPopupController',
            'BlockTabsControl' => 'Site\Controller\BlockTabsControlController',
            'BigBanner' => 'Site\Controller\BlockBigBannerController',
            'BlockSmallBanner' => 'Site\Controller\BlockSmallBannerController',
            'KcFinder' => 'Site\Controller\KcFinderController',   
            'BlockAccordion' => 'Site\Controller\BlockAccordionController',
            'BlockMainMenu' => 'Site\Controller\BlockMainMenuController'
        ),
    ),
    'controller_plugins' => array(
        'factories' => array(
            'sessionData' => 'Site\Controller\Plugin\SessionData',
            'translate' => 'Site\Controller\Plugin\Translate'
        ),
        'invokables' => array(
            'jsonResponse' => 'Site\Controller\Plugin\JsonResponse',
            'escapeForHtml' => 'Site\Controller\Plugin\EscapeForHtml'
        )
    ),
    'view_manager' => array(
        'display_not_found_reason' => false,
        'display_exceptions' => false,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'site/index/index' => __DIR__ . '/../view/site/index/index.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view/',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
                'update-search-index' => array(
                    'options' => array(
                        'route'    => 'update [info|start|stop]:mode [--verbose|-v]',
                        'defaults' => array(
                            'controller' => 'SearchIndex',
                            'action'     => 'update'
                        )
                    )
                )
            )
        )
    ),
);

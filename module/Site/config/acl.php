<?php
return array(
    /* Roles with inheritance */
    'roles' => array(
        'guest' => null,
        'member' => 'guest',
        'moderator' => 'member',
        'admin' => 'moderator'
    ),
    /* Access to resources like controllers, actions etc. */
    'resources' => array(
        'allow' => array(
            /* role => array (resource => privileges) */
            'guest' => array(
                'auth/index' => null,
                'site/block' => array(
                    'get-dynamic-block-count',
                    'render-block-again'
                ),
                'site/dialog' => null,
                'site/get-file' => null,
                'site/language' => null,
                'site/page' => null,
                'site/user' => array(
                    'new-password',
                    'registration',
                    'reset-password'
                ),
                'site/feedback-mail' => array(
                    'send'
                ),
                'site/search-index' => null,
                '/searchindex' => null, // For console.
                // Employees
                'site/block-employees' => array(
                    'find-employees',
                    'get-employees-columns'
                ),
                // EventDetails
                'site/block-event-details' => array(
                    'get-sign-up-form-view',
                    'sign-up-for-event'
                ),
                // EventList
                'site/block-event-list' => array(
                    'add-event-as-guest',
                    'get-sign-up-form-view',
                    'sign-up-for-event',
                    'download-users-signed-up-for-event-as-csv',
                    'download-users-signed-up-for-event-as-xls'
                ),
                'site/upload' => null,
                // Popup
                'site/block-popup' => array(
                    'get-title-and-contents-view'
                )
            ),
            'member' => array(
            ),
            'moderator' => array(
                'site/admin' => array(
                    'blocks', 'toggle-design-mode', 'pages', 'toggle-page-publishing', 'add-edit-page', 'delete-page',
                    'get-page-deletion-confirmation-view'
                ),
                'site/block' => array(
                    'update',
                    'update-title',
                    'change-title-size',
                    'delete-dynamic-block',
                    'reorder',
                    'reorder-blocks-for-admin-tool-wnd',
                    'toggle-publishing',
                    'get-management-view',
                    'get-add-edit-view',
                    'get-block-list-view',
                    'get-dynamic-block-list-view',
                    'get-edit-block-properties-view',
                    'get-settings-view',
                    'save-settings',
                    'get-image-edit-view',
                    'render-block-again',
                    'save-image-changes',
                    'update-block-properties'
                ),
                'site/user' => array(
                    'list'
                ),
                /* Block specific controllers */
                // BachelorStudies
                'site/block-bachelor-studies' => array(
                    'reorder-tabs',
                    'update-basic-info',
                    'update-tab-contents'
                ),
                // Employees
                'site/block-employees' => array(
                    'toggle-employee-publishing',
                    'add-edit-employee',
                    'delete-employee',
                    'get-degrees-management-view',
                    'get-add-edit-degree-view',
                    'toggle-degree-publishing',
                    'add-edit-degree',
                    'delete-degree'
                ),
                // EmployeeDetails
                'site/block-employee-details' => array(
                    'set-employee-degree-id',
                    'update-employee-name-and-surname',
                    'update-employee-position',
                    'update-employee-about',
                    'update-employee-scientific-career',
                    'update-employee-the-most-important-publications',
                    'update-employee-contact-info'
                ),
                // EventList
                'site/block-event-list' => array(
                    'reorder-event-images',
                    'toggle-event-publishing',
                    'add-edit-event',
                    'delete-event',
                    'delete-event-image',
                    'get-categories-management-view',
                    'get-add-edit-category-view',
                    'toggle-category-publishing',
                    'add-edit-category',
                    'delete-category'
                ),
                // EventDetails
                'site/block-event-details' => array(
                    'reorder-event-images',
                    'update-event-title',
                    'update-event-contents',
                    'update-event-date-start',
                    'update-event-additional-data',
                    'update-event-gallery-image-title'
                ),
                // ImageGallery
                'site/block-image-gallery' => array(
                    'reorder-images',
                    'toggle-image-publishing',
                    'add-edit-image',
                    'delete-image'
                ),
                // ImageSlider
                'site/block-image-slider' => array(
                    'reorder-images',
                    'toggle-image-publishing',
                    'add-edit-image',
                    'delete-image'
                ),
                // NewsAdd
                'site/block-news-add' => array(
                    'add-news-as-guest'
                ),
                // NewsList
                'site/block-news-list' => array(
                    'toggle-news-publishing',
                    'add-edit-news',
                    'delete-news',
                    'get-categories-management-view',
                    'get-add-edit-category-view',
                    'toggle-category-publishing',
                    'add-edit-category',
                    'delete-category'
                ),
                // NewsDetails
                'site/block-news-details' => array(
                    'reorder-news-images',
                    'update-news-title',
                    'update-news-contents',
                    'update-news-gallery-image-title'
                ),
                // Offer
                'site/block-offer' => array(
                    'toggle-major-publishing',
                    'add-edit-major',
                    'delete-major',
                    'reorder-majors',
                    'get-degrees-management-view',
                    'get-add-edit-degree-view',
                    'toggle-degree-publishing',
                    'add-edit-degree',
                    'delete-degree',
                    'reorder-degrees'
                ),
                // PostgraduateStudies
                'site/block-postgraduate-studies' => array(
                    'reorder-tabs',
                    'update-basic-info',
                    'update-tab-contents'
                ),
                // ImagesPile
                'site/block-images-pile' => array(
                    'reorder-groups',
                    'reorder-items-in-group',
                    'toggle-item-publishing',
                    'add-edit-item',
                    'delete-item',
                    'get-groups-management-view',
                    'get-add-edit-group-view',
                    'toggle-group-publishing',
                    'add-edit-group',
                    'delete-group'
                ),
                // Popup
                'site/block-popup' => array(
                    'get-edit-view',
                    'save-contents'
                ),
                // TabsControl
                'site/block-tabs-control' => array(
                    'add-edit-tab',
                    'delete-tab',
                    'reorder-tabs',
                    'update-tab-name',
                    'update-tab-contents'
                ),
                // BigBanner
                'site/big-banner' => array(
                    'reorder-banners',
                    'create-banner',
                    'delete-banner',
                    'update-banner',
                    'update-banner-contents',
                    'update-banner-title',
                    'toggle-banner-publishing'
                ),
                // NavigationMenu
                'site/block-navigation-menu' => array(
                    'reorder-items',
                    'get-parent-items',
                    'toggle-item-publishing',
                    'add-edit-item',
                    'delete-item',
                    'get-categories-management-view',
                    'get-add-edit-category-view',
                    'toggle-category-publishing',
                    'add-edit-category',
                    'delete-category'
                ),
                // KcFinder
                'site/kc-finder' => array(
                    'compress',
                    'upload'
                ),
                // SmallBanner
                'site/block-small-banner' => array(
                    'reorder-banners',
                    'create-banner',
                    'delete-banner',
                    'update-banner',
                    'update-banner-text',
                    'toggle-banner-publishing'
                ),
                // Accordion                
                'site/block-accordion' => array(
                    'add-edit-panel',
                    'delete-panel',                    
                    'update-panel-name',
                    'update-panel-contents',
                    'reorder-panels'
                ),
                // Main Menu
                'site/block-main-menu' => array(
                    'add-edit-info',
                    'delete-info',                    
                    'update-info-title',
                    'update-info-contents'
                ),
            ),
            'admin' => array(
                'site/admin' => array(
                    'toggle-design-mode', 'pages', 'toggle-page-publishing', 'add-edit-page', 'delete-page',
                    'event-list', 'users-signed-up-for-event', 'delete-user-signed-up-for-event',
                    'get-user-signed-up-for-event-deletion-confirmation-view'
                ),
                'site/block' => array(
                    'insert', 'delete', 'get-save-as-dynamic-view', 'save-as-dynamic', 'move-to-another-slot', 'config'
                ),
                'site/user' => array(
                    'toggle-account-activity',
                    'delete',
                    'add-edit'
                ),
                'site/settings' => null,
                'site/code-test' => null
            )
        )
    )
);

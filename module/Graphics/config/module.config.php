<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Image' => 'Site\Controller\ImageController'
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => false,
        'display_exceptions' => false
    )
);

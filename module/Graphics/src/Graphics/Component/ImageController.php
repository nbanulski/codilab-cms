<?php
namespace Graphics\Component;

use FileSystem\Component\FileSystem;

class ImageController
{
    protected $fileSystem = null;
    protected $defaultFormatExtension = 'png';
    protected $jpegQuality = 100;
    protected $pngQuality = 9;
    protected $defaultBackgroundColorRed = 255;
    protected $defaultBackgroundColorGreen = 255;
    protected $defaultBackgroundColorBlue = 255;

    public function __construct(FileSystem $fileSystem)
    {
        $this->fileSystem = $fileSystem;
    }

    public function getDefaultBackgroundColorComponents()
    {
        return array(
            $this->defaultBackgroundColorRed,
            $this->defaultBackgroundColorGreen,
            $this->defaultBackgroundColorBlue
        );
    }

    public function setDefaultBackgroundColorComponents($componentRed, $componentGreen, $componentBlue)
    {
        // Accept only values from 0 to 255):
        $componentRed = max(0, min(255, (int)$componentRed));
        $componentGreen = max(0, min(255, (int)$componentGreen));
        $componentBlue = max(0, min(255, (int)$componentBlue));

        $this->defaultBackgroundColorRed = $componentRed;
        $this->defaultBackgroundColorGreen = $componentGreen;
        $this->defaultBackgroundColorBlue = $componentBlue;
    }

    public function getImageDimensions($imageFilePath)
    {
        $dimensions = array(0, 0);

        if (!is_file($imageFilePath))
        {
            return $dimensions;
        }

        $info = getimagesize($imageFilePath);
        if ($info)
        {
            list($width, $height) = $info;
            $dimensions = array($width, $height);
        }

        return $dimensions;
    }

    public function calculateAspectRatio($width, $height)
    {
        $aspectRatio = false;

        if ($height != 0)
        {
            $aspectRatio = $width / $height;
        }

        return $aspectRatio;
    }

    public function adjustDimensionsUsingAspectRatio(&$width, &$height, $aspectRatio)
    {
        if (($height != 0) && ($aspectRatio != 0.00))
        {
            if ($width / $height > $aspectRatio)
            {
                $width = $height * $aspectRatio;
            }
            else
            {
                $height = $width / $aspectRatio;
            }

            return true;
        }

        return false;
    }

    public function lowerDimensionsToMinWidthAndMinHeightMaintainingAspectRatio(
        &$width, &$height, $minWidth, &$minHeight
    )
    {
        if (($width <= 0) || ($height <= 0) || ($minWidth <= 0) || ($minHeight <= 0))
        {
            return false;
        }

        $aspectRatio = $this->calculateAspectRatio($width, $height);

        // Resize image to minWidth using original height:
        $width = $minWidth;
        $this->adjustDimensionsUsingAspectRatio($width, $height, $aspectRatio);

        // Then adjust dimensions:
        if ($width < $minWidth)
        {
            $difference = $minWidth - $width;
            $width += $difference;
            $height += $difference * $aspectRatio;
        }
        else if ($height < $minHeight)
        {
            $difference = $minHeight - $height;
            $height += $difference;
            $width += $difference * $aspectRatio;
        }

        return true;
    }

    public function convertRectCoordsStringToArray($rectCoordsString)
    {
        $rectCoordsString = trim((string)$rectCoordsString);
        $rectCoordsParts = explode(',', $rectCoordsString);

        if (count($rectCoordsParts) < 4)
        {
            return false;
        }

        $allCoordinatesAreValid = true;
        $rectCoords = array();
        for ($i = 0; $i < 4; $i++)
        {
            $number = (int)$rectCoordsParts[$i];

            if (
                (($i <= 1) && ($number < 0)) || // Invalid top-left positions.
                (($i >= 2) && ($number <= 0))  // Invalid bottom-right positions.
            )
            {
                $allCoordinatesAreValid = false;

                break;
            }

            $rectCoords[$i] = $number;
        }

        return $allCoordinatesAreValid ? $rectCoords : false;
    }

    public function filePathPointsToImage($filePath)
    {
        $fileExtension = mb_strtolower(pathinfo($filePath, PATHINFO_EXTENSION));

        return in_array(
            $fileExtension,
            array(
                'gif', 'jpeg', 'jpg', 'png'
            )
        );
    }

    public function isValidGdImageResource($imageResourceHandle)
    {
        return $imageResourceHandle && (@get_resource_type($imageResourceHandle) == 'gd');
    }

    public function fillImageWithColor($imageResourceHandle, $componentRed, $componentGreen, $componentBlue)
    {
        if (!$this->isValidGdImageResource($imageResourceHandle))
        {
            return false;
        }

        $color = imagecolorallocate(
            $imageResourceHandle,
            $componentRed,
            $componentGreen,
            $componentBlue
        );

        return imagefill($imageResourceHandle, 0, 0, $color);
    }

    /**
     * 	If $destinationFormat is empty then this function uses the format specified in the destination path.
     * 	If the destination path does not include the file extension then the function uses the format of the source path.
     * 	If the source path does not contain a file extension then the function uses the default "png".
     */
    public function resizeImageToSpecificWidth($sourceFilePath, $destinationFilePath, $width = null, $destinationFormat = null)
    {
        list($originalWidth, $originalHeight) = $this->getImageDimensions($sourceFilePath);
        $height = $originalHeight;

        return $this->resizeImage($sourceFilePath, $destinationFilePath, $width, $height, $destinationFormat);
    }

    public function resizeImageToSpecificHeight($sourceFilePath, $destinationFilePath, $height = null, $destinationFormat = null)
    {
        list($originalWidth, $originalHeight) = $this->getImageDimensions($sourceFilePath);
        $width = $originalWidth;

        return $this->resizeImage($sourceFilePath, $destinationFilePath, $width, $height, $destinationFormat);
    }

    // It maintains aspect ratio.
    public function resizeImageToSpecificDimensionsMaintainingAspectRatio(
        $sourceFilePath, $destinationFilePath, $width, $height, $destinationFormat = null
    )
    {
        list($originalWidth, $originalHeight) = $this->getImageDimensions($sourceFilePath);

        $outputWidth = $originalWidth;
        $outputHeight = $originalHeight;

        $this->lowerDimensionsToMinWidthAndMinHeightMaintainingAspectRatio(
            $outputWidth, $outputHeight, $width, $height
        );

        /*$data =
            '$originalWidth: ' . $originalWidth . "\n" .
            '$originalHeight: ' . $originalHeight . "\n" .
            'new $width: ' . $width . "\n" .
            'new $height: ' . $height . "\n" .
            '$outputWidth: ' . $outputWidth . "\n" .
            '$outputHeight: ' . $outputHeight . "\n"
        ;
        file_put_contents('/home/wojtek/Desktop/proimagine-cms.messages.log', $data);*/

        return $this->resizeImage($sourceFilePath, $destinationFilePath, $outputWidth, $outputHeight, $destinationFormat);
    }

    /**
     * 	If $destinationFormat is empty then this function uses the format specified in the destination path.
     * 	If the destination path does not include the file extension then the function uses the format of the source path.
     * 	If the source path does not contain a file extension then the function uses the default "png".
     */
    public function resizeImageToSpecificDimensions($sourceFilePath, $destinationFilePath, $width, $height, $destinationFormat = null)
    {
        return $this->resizeImage($sourceFilePath, $destinationFilePath, $width, $height, $destinationFormat);
    }

    public function cropImageToSpecificDimensions(
        $sourceFilePath, $destinationFilePath,
        $width, $height, $forceUsingGivenDimensions = true, $destinationFormat = null
    )
    {
        list($originalWidth, $originalHeight) = $this->getImageDimensions($sourceFilePath);

        if (!$originalWidth || !$originalHeight) // If incorrect dimensions.
        {
            return false;
        }

        $destinationFilePath = trim($destinationFilePath);
        $destinationFilePathExtension = pathinfo($destinationFilePath, PATHINFO_EXTENSION);
        $destinationFormat = $this->recognizeDestinationFormat(
            $sourceFilePath, $destinationFilePathExtension, $destinationFormat
        );

        $originalAspectRatio = $this->calculateAspectRatio($originalWidth, $originalHeight);
        $thumbAspectRatio = $this->calculateAspectRatio($width, $height);

        if ($originalAspectRatio < $thumbAspectRatio)
        {
            $newHeight = $height;
            $newWidth = $originalWidth / ($originalHeight / $height);
        }
        else if ($originalAspectRatio > $thumbAspectRatio)
        {
            $newWidth = $width;
            $newHeight = $originalHeight / ($originalWidth / $width);
        }
        else
        {
            $newWidth = $width;
            $newHeight = $height;
        }

        $sourceImage = imagecreatefromstring(file_get_contents($sourceFilePath));

        if ($forceUsingGivenDimensions)
        {
            $resampledImage = imagecreatetruecolor($width, $height);
        }
        else
        {
            $resampledImage = imagecreatetruecolor($newWidth, $newHeight);
        }

        imagealphablending($resampledImage, false);
        imagesavealpha($resampledImage, true);

        /*
        $this->fillImageWithColor(
            $resampledImage,
            $this->defaultBackgroundColorRed,
            $this->defaultBackgroundColorGreen,
            $this->defaultBackgroundColorBlue
        );
        */

        imagecopyresampled(
            $resampledImage,
            $sourceImage,
            $forceUsingGivenDimensions ? abs(($newWidth - $width) / 2) : 0, // Center the image horizontally.
            $forceUsingGivenDimensions ? abs(($newHeight - $height) / 2) : 0, // Center the image vertically.
            0, 0,
            $newWidth, $newHeight,
            $originalWidth, $originalHeight
        );

        return $this->saveImage($resampledImage, $destinationFilePath, $destinationFormat);
    }

    public function cropImageUsingRectCoordsStringAndSave($sourceFilePath, $rectCoordsString, $destinationFilePath, $destinationFormat = null)
    {
        $c = $this->convertRectCoordsStringToArray($rectCoordsString);

        return $this->cropImageUsingRectCoordsAndSave($sourceFilePath, $c[0], $c[1], $c[2], $c[3], $destinationFilePath, $destinationFormat);
    }

    public function cropImageUsingRectCoordsAndSave($sourceFilePath, $x, $y, $x2, $y2, $destinationFilePath, $destinationFormat = null)
    {
        $croppedImage = $this->cropImageUsingRectCoordsAndReturn($sourceFilePath, $x, $y, $x2, $y2);
        if (!$croppedImage)
        {
            return false;
        }

        $destinationFilePath = trim($destinationFilePath);
        $destinationFilePathExtension = pathinfo($destinationFilePath, PATHINFO_EXTENSION);
        $destinationFormat = $this->recognizeDestinationFormat($sourceFilePath, $destinationFilePathExtension, $destinationFormat);

        return $this->saveImage($croppedImage, $destinationFilePath, $destinationFormat);
    }

    public function cropImageUsingRectCoordsStringAndReturn($sourceFilePath, $rectCoordsString)
    {
        $c = $this->convertRectCoordsStringToArray($rectCoordsString);
        if (!$c)
        {
            return false;
        }

        return $this->cropImageUsingRectCoordsAndReturn($sourceFilePath, $c[0], $c[1], $c[2], $c[3]);
    }

    public function cropImageUsingRectCoordsAndReturn($sourceFilePath, $x, $y, $x2, $y2)
    {
        if (!is_file($sourceFilePath))
        {
            return false;
        }

        $sourceFilePath = trim($sourceFilePath);
        $sourceFilePathExtension = pathinfo($sourceFilePath, PATHINFO_EXTENSION);
        $destinationFormat = $this->recognizeDestinationFormat(
            $sourceFilePath, $sourceFilePathExtension
        );

        $croppedImageWidth = $x2 - $x;
        $croppedImageHeight = $y2 - $y;

        $sourceImage = imagecreatefromstring(file_get_contents($sourceFilePath));
        $croppedImage = imagecreatetruecolor($croppedImageWidth, $croppedImageHeight);
        imagealphablending($croppedImage, false);
        imagesavealpha($croppedImage, true);

        /**
         * For GIF files we must fill entire image with transparent color, otherwise it will not have a transparency.
         * For PNGs - it is enough to use imagealphablending() and imagesavealpha() functions.
         */

        if ($destinationFormat == 'gif')
        {
            imagealphablending($croppedImage, true);
            imagesavealpha($croppedImage, false);
            $transparentColor = imagecolortransparent($sourceImage);
            $transparentColorRGBValues = imagecolorsforindex($sourceImage, $transparentColor);
            $newlyAllocatedTransparentColor = imagecolorallocate(
                $croppedImage,
                $transparentColorRGBValues['red'],
                $transparentColorRGBValues['green'],
                $transparentColorRGBValues['blue']
            );
            imagefill($croppedImage, 0, 0, $newlyAllocatedTransparentColor);
            imagecolortransparent($croppedImage, $newlyAllocatedTransparentColor);
        }

        imagecopy($croppedImage, $sourceImage, 0, 0, $x, $y, $x2, $y2);

        return $croppedImage;
    }

    public function recognizeExtensionBasicOnSourceFilePathAndMimeType($sourceFilePath, $mimeType)
    {
        $fileExtension = $this->getFileExtensionFromMimeType($mimeType);
        if ($fileExtension == '')
        {
            $fileExtension = mb_strtolower(pathinfo($sourceFilePath, PATHINFO_EXTENSION));
        }

        return $fileExtension;
    }

    public function getFileExtensionFromMimeType($mimeType)
    {
        $mimeType = trim($mimeType);

        switch ($mimeType)
        {
            case 'image/gif': $fileExtension = 'gif';
                break;
            case 'image/jpeg': $fileExtension = 'jpg';
                break;
            case 'image/png': $fileExtension = 'png';
                break;
            case 'image/tiff': $fileExtension = 'tiff';
                break;
            case 'image/vnd.microsoft.icon': $fileExtension = 'ico';
                break;
            default: $fileExtension = '';
        }

        return $fileExtension;
    }

    protected function resizeImage($sourceFilePath, $destinationFilePath, $width, $height, $destinationFormat = null)
    {
        list($originalWidth, $originalHeight) = $this->getImageDimensions($sourceFilePath);

        if (!$originalWidth || !$originalHeight) // If incorrect dimensions.
        {
            return false;
        }

        $aspectRatio = $this->calculateAspectRatio($originalWidth, $originalHeight);
        if (!$this->adjustDimensionsUsingAspectRatio($width, $height, $aspectRatio))
        {
            return false;
        }

        $destinationFilePath = trim($destinationFilePath);
        $destinationFilePathExtension = pathinfo($destinationFilePath, PATHINFO_EXTENSION);
        $destinationFormat = $this->recognizeDestinationFormat(
            $sourceFilePath, $destinationFilePathExtension, $destinationFormat
        );

        $sourceImage = imagecreatefromstring(file_get_contents($sourceFilePath));
        $resampledImage = imagecreatetruecolor($width, $height);
        imagealphablending($resampledImage, false);
        imagesavealpha($resampledImage, true);

        /**
         * For GIF files we must fill entire image with transparent color, otherwise it will not have a transparency.
         * For PNGs - it is enough to use imagealphablending() and imagesavealpha() functions.
         */

        if ($destinationFormat == 'gif')
        {
            imagealphablending($resampledImage, true);
            imagesavealpha($resampledImage, false);
            $transparentColor = imagecolortransparent($sourceImage);
            $transparentColorRGBValues = imagecolorsforindex($sourceImage, $transparentColor);
            $newlyAllocatedTransparentColor = imagecolorallocate(
                $resampledImage,
                $transparentColorRGBValues['red'],
                $transparentColorRGBValues['green'],
                $transparentColorRGBValues['blue']
            );
            imagefill($resampledImage, 0, 0, $newlyAllocatedTransparentColor);
            imagecolortransparent($resampledImage, $newlyAllocatedTransparentColor);
        }

        imagecopyresampled($resampledImage, $sourceImage, 0, 0, 0, 0, $width, $height, $originalWidth, $originalHeight);

        /**
         * 	If destination file path does not contains file extension then we must add it.
         */
        if ($destinationFilePathExtension == '')
        {
            $lastChar = substr($destinationFilePath, -1);
            if ($lastChar != '.') // If the last character is not a dot.
            {
                $destinationFilePath .= '.';
            }
            $destinationFilePath .= $destinationFormat;
        }

        return $this->saveImage($resampledImage, $destinationFilePath, $destinationFormat);
    }

    protected function recognizeDestinationFormat($sourceFilePath, $destinationFilePathExtension, $destinationFormat = null)
    {
        if ($destinationFormat == '')
        {
            $destinationFormat = $destinationFilePathExtension;
        }
        if ($destinationFormat == '')
        {
            $destinationFormat = pathinfo($sourceFilePath, PATHINFO_EXTENSION);
        }
        if ($destinationFormat == '')
        {
            $destinationFormat = $this->defaultFormatExtension;
        }
        $destinationFormat = strtolower($destinationFormat);

        return $destinationFormat;
    }

    // Todo: Fixing problems with imagejpeg function on Windows. Read the comment inside for more info.
    protected function saveImage($resampledImage, $destinationFilePath, $destinationFormat)
    {
        /**
         * Problems on Windows 8 (and other version too maybe).
         * PHP reports that invalid argument was given to the imagejpeg() function
         * It applies to the file path, but surely it is correct. Note that this problems occurs only sometimes.
         * Using clearstatcache() function will not resolve this problem (whenever you use some arguments or not).
         * More info here:
         * http://stackoverflow.com/questions/20324302/imagejpeg-failed-to-open-stream
         * http://stackoverflow.com/questions/14820031/imagejpeg-unable-to-open-for-writing-sometimes-it-works-sometimes-not
         */

        $oldErrorHandler = set_error_handler(array($this, 'errorHandler'));

        switch ($destinationFormat)
        {
            case 'gif': $result = imagegif($resampledImage, $destinationFilePath);
                break;
            case 'jpeg':
            case 'jpg':
                $result = imagejpeg($resampledImage, $destinationFilePath, $this->jpegQuality); // 100% quality.
                break;
            default: $result = imagepng($resampledImage, $destinationFilePath, $this->pngQuality);
                break; // Maximum compression level.
        }

        restore_error_handler();

        return $result;
    }

    protected function errorHandler()
    {
        return true; // Don't execute PHP internal error handler.
    }
}
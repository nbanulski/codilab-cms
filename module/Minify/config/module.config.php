<?php
return array(
    'view_helpers' => array(
        'invokables'=> array(
            'minify' => 'Minify\View\Helper\Minify'
        )
    ),
    'view_manager' => array(
        'display_not_found_reason' => false,
        'display_exceptions' => false
    )
);

var jDialogsFetchingTemplatesErrorCount=0;
function jDialog(
    contents,title,customId,doneCallback,withSaveButton,onCloseCallback,doNotRequireSession,additionalCssClasses,
    useStylesForLoggedUser
    )
{
    if(jDialogsFetchingTemplatesErrorCount>=3)
    {
        var message='Przekroczono maksymalną ilość prób (3) wczytywania '+
            'szablonu dla okien dialogowych. Nie udało się wczytać '+
            'szablonu z powodu błędu. Okna dialogowe nie będą '+
            'wyświetlane do czasu ponownego odświeżenia strony.';
        $(message).dialog({
            modal:true,
            buttons:{
                Ok:function(){
                    $(this).dialog('close');
                }
            }
        });

        return false;
    }

    var id='j-dialog';
    if((typeof (customId)!='undefined')&&(customId!=null))
    {
        id=customId;
    }

    if((typeof useStylesForLoggedUser=='undefined')||(useStylesForLoggedUser=='auto'))
    {
        useStylesForLoggedUser=isUserLogged();
    }

    var postData={
        'templateType':'standard',
        'withSaveButton':(withSaveButton==true)?'1':'0',
        'id':id,
        'title':title,
        'contents':contents
    };
    makePostRequest(makeCmsUrl('dialog/get-template'),function(response){
        var success=false;
        var domInstance=null;

        if(
            (!doNotRequireSession&&requestOkAndSessionExist(response))||
                (doNotRequireSession&&requestOk(response))
            )
        {
            var modalLevel=$('body').find('> .modal').length;
            var html=response.data;
            var modal=$(html).hide();
            if(!useStylesForLoggedUser)
            {
                modal.addClass('admin');
            }
            if(additionalCssClasses!='')
            {
                modal.addClass(additionalCssClasses);
            }
            modal.find('> .modal-dialog').addClass('dialog-level-'+modalLevel);
            $('body').append(modal);
            domInstance=$('#'+id);
            domInstance.modal({'show':false});
            enableDisableNestedInputs('input[type="checkbox"]');
            success=true;
        }
        else
        {
            jDialogsFetchingTemplatesErrorCount++;
        }

        if(domInstance)
        {
            if(doneCallback)
            {
                domInstance.on('shown.bs.modal',function(e){
                    if(e.target===this)
                    {
                        doneCallback(id,success);
                    }
                });
            }

            domInstance.on('hidden.bs.modal',function(e){
                if(e.target===this)
                {
                    if($.isFunction(onCloseCallback))
                    {
                        onCloseCallback();
                    }

                    domInstance.remove();
                }
            });

            domInstance.modal('show');
        }
    },postData,doNotRequireSession);
}
function jConfirm(
    contents,title,customId,doneCallback,onCloseCallback,doNotRequireSession,additionalCssClasses,
    useStylesForLoggedUser
    )
{
    var id='j-confirm';
    if((typeof (customId)!='undefined')&&(customId!=null))
    {
        id=customId;
    }

    if((typeof useStylesForLoggedUser=='undefined')||(useStylesForLoggedUser=='auto'))
    {
        useStylesForLoggedUser=isUserLogged();
    }

    var postData={
        'templateType':'confirm',
        'id':id,
        'title':title,
        'contents':contents
    };
    makePostRequest(makeCmsUrl('dialog/get-template'),function(response){
        var domInstance=null;

        if(
            (!doNotRequireSession&&requestOkAndSessionExist(response))||
                (doNotRequireSession&&requestOk(response))
            )
        {
            var modalLevel=$('body').find('> .modal').length;
            var html=response.data;
            var modal=$(html).hide();
            if(!useStylesForLoggedUser)
            {
                modal.addClass('admin');
            }
            if(additionalCssClasses!='')
            {
                modal.addClass(additionalCssClasses);
            }
            modal.find('> .modal-dialog').addClass('dialog-level-'+modalLevel);
            modal.find('.btn-confirm').on('click',function(){
                if(doneCallback)
                {
                    doneCallback(id,true);
                }
                modal.closest('.modal').attr('confirmed',true);
                domInstance.modal('hide');
            });
            $('body').append(modal);
            domInstance=$('#'+id);
            domInstance.modal({'show':false});
            enableDisableNestedInputs('input[type="checkbox"]');
        }

        if(domInstance)
        {
            if(doneCallback)
            {
                domInstance.on('hidden.bs.modal',function(e){
                    if(e.target===this)
                    {
                        if(!domInstance.attr('confirmed'))
                        {
                            doneCallback(id,false);
                        }
                        if($.isFunction(onCloseCallback))
                        {
                            onCloseCallback();
                        }
                        domInstance.remove();
                    }
                });
            }

            domInstance.modal('show');
        }
    },postData,doNotRequireSession);
}
function jDialogForAdmin(contents,title,customId,doneCallback,withSaveButton)
{
    if(isUserLoggedAsAdmin())
    {
        jDialog(contents,title,customId,doneCallback,withSaveButton);
    }
}
+function($){"use strict";
    var bootstrap_originalPopoverFn=$.fn.popover;
    /*$.extend(bootstrap_originalPopoverFn.Constructor.DEFAULTS,{});*/
    var Popover=function(element,options){
        this.baseTipFunction=this.bootstrap_originalPopoverFn.tip;
        bootstrap_originalPopoverFn.Constructor.apply(this,arguments);
    }
    Popover.prototype=$.extend({},bootstrap_originalPopoverFn.Constructor.prototype,{
        constructor:Popover,
        bootstrap_originalPopoverFn:function(){
            var args=$.makeArray(arguments);
            bootstrap_originalPopoverFn.Constructor.prototype[args.shift()].apply(this,args);
        },
        getTitle:function(){
            var title;
            var $e=this.$element;
            var o=this.options;
            var $tip=this.tip();
            title=
                $e.attr('data-original-title')||
                $.trim($tip.find('.popover-title')[this.options.html ? 'html' : 'text']())||
                (typeof o.title=='function'?o.title.call($e[0]):o.title);
            return title
        },
        getContent:function(){
            var content;
            var $e=this.$element;
            var o=this.options;
            var $tip=this.tip();
            content=
                $e.attr('data-content')||
                $.trim($tip.find('.popover-content')[this.options.html ? 'html' : 'text']())||
                (typeof o.content=='function'?o.content.call($e[0]):o.content);
            return content;
        },
        tip:function(){
            var $e=$(this.$element);
            if($e&&$e.length)
            {
                var $tipContainer=$e.next('.popover');
                if($tipContainer&&$tipContainer.length)
                {
                    this.$tip=$tipContainer;
                    return this.$tip;
                }
            }
            if (!this.$tip)this.$tip=$(this.options.template);
            return this.$tip
        }
    });
    $.fn.popover=$.extend(function(option){
        var args=$.makeArray(arguments),option=args.shift();
        return this.each(function(){
            var $this=$(this);
            var data=$this.data('bs.popover'),
                options=$.extend({},bootstrap_originalPopoverFn.defaults,$this.data(),typeof option=='object'&&option);
            if(!data){
                $this.data('bs.popover',(data=new Popover(this,options)));
            }
            if(typeof option=='string'){
                data[option].apply(data,args);
            }
            else if(options.show){
                data.show.apply(data,args);
            }
        });
    },bootstrap_originalPopoverFn);
    $.fn.popover.noConflict=function(){
        $.fn.popover=bootstrap_originalPopoverFn;
        return this;
    }
}(jQuery);
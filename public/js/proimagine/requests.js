/**
 * Block management.
 */

function request_getBlocksView(doneCallback,data)
{
    makePostRequest(makeCmsUrl('admin/blocks'),doneCallback,data);
}
function request_renderBlockAgain(postData,doneCallback)
{
    var doNotRequireSession=true;
    makePostRequest(makeCmsUrl('block/render-block-again'),doneCallback,postData,doNotRequireSession);
}
function request_getBlockSaveAsDynamicView(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block/get-save-as-dynamic-view'),doneCallback,postData);
}
function request_getEditBlockPropertiesView(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block/get-edit-block-properties-view'),doneCallback,postData);
}
function request_saveBlockAsDynamic(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block/save-as-dynamic'),doneCallback,postData);
}
function request_toggleBlockPublishing(pageBlockId,doneCallback)
{
    makePostRequest(makeCmsUrl('block/toggle-publishing'),doneCallback,{
        'pageBlockId':pageBlockId
    });
}
function request_getBlockSettingsView(pageBlockId,doneCallback)
{
    makePostRequest(makeCmsUrl('block/get-settings-view'),doneCallback,{
        'pageBlockId':pageBlockId
    });
}
function request_getBlockManagementView(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block/get-management-view'),doneCallback,postData);
}
function request_getBlockAddEditView(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block/get-add-edit-view'),doneCallback,postData);
}
function request_saveBlockSettings(pageBlockId,settings,doneCallback)
{
    makePostRequest(makeCmsUrl('block/save-settings'),doneCallback,{
        'pageBlockId':pageBlockId,
        'settings':settings
    });
}
function request_changeBlockTitleSize(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block/change-title-size'),doneCallback,postData);
}
function request_reorderBlocksInAdminToolWnd(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block/reorder-blocks-for-admin-tool-wnd'),doneCallback,postData);
}
function request_deleteDynamicBlock(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block/delete-dynamic-block'),doneCallback,postData);
}
function request_getBlockListView(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block/get-block-list-view'),doneCallback,postData);
}
function request_getDynamicBlockCount(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block/get-dynamic-block-count'),doneCallback,postData);
}
function request_getDynamicBlockListView(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block/get-dynamic-block-list-view'),doneCallback,postData);
}
function request_updateBlockProperties(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block/update-block-properties'),doneCallback,postData);
}

/**
 * Image edit window. Allows image cropping.
 */

function request_getBlockImageEditView(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block/get-image-edit-view'),doneCallback,postData);
}
function request_saveImageChanges(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block/save-image-changes'),doneCallback,postData);
}

/**
 * Pages management.
 */

function request_getPagesView(doneCallback,data)
{
    makePostRequest(makeCmsUrl('admin/pages'),doneCallback,data);
}
function request_togglePagePublishing(pageId,published,doneCallback)
{
    var data={
        'pageId':pageId,
        'published':published?1:0
    };
    makePostRequest(makeCmsUrl('admin/toggle-page-publishing'),doneCallback,data);
}
function request_getAddEditPageView(doneCallback,data)
{
    makePostRequest(makeCmsUrl('admin/add-edit-page'),doneCallback,data);
}
function request_addEditPage(doneCallback,formData)
{
    makePostRequest(makeCmsUrl('admin/add-edit-page'),doneCallback,formData);
}
function request_getPageDeletionConfirmationView(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('admin/get-page-deletion-confirmation-view'),doneCallback,postData);
}
function request_deletePage(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('admin/delete-page'),doneCallback,postData);
}

/**
 * Event list management.
 */

function request_getEventListView(doneCallback,data)
{
    makePostRequest(makeCmsUrl('admin/event-list'),doneCallback,data);
}
function request_getListOfUsersSignedUpForEventView(doneCallback,data)
{
    makePostRequest(makeCmsUrl('admin/users-signed-up-for-event'),doneCallback,data);
}
function request_getUserSignedUpForEventDeletionConfirmationView(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('admin/get-user-signed-up-for-event-deletion-confirmation-view'),doneCallback,postData);
}
function request_deleteUserSignedUpForEvent(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('admin/delete-user-signed-up-for-event'),doneCallback,postData);
}

/**
 * Users management.
 */

function request_getUsersView(doneCallback,data)
{
    makePostRequest(makeCmsUrl('user/list'),doneCallback,data);
}
function request_toggleUserAccountActivity(userId,active,doneCallback)
{
    makePostRequest(makeCmsUrl('user/toggle-account-activity'),doneCallback,{
        'userId':userId,
        'active':active?1:0
    });
}
function request_deleteUser(userId,doneCallback)
{
    makePostRequest(makeCmsUrl('user/delete'),doneCallback,{'userId':userId});
}
function request_getUserAddEditView(doneCallback,data)
{
    makePostRequest(makeCmsUrl('user/add-edit'),doneCallback,data);
}
function request_addEditUser(doneCallback,formData)
{
    makePostRequest(makeCmsUrl('user/add-edit'),doneCallback,formData);
}

/**
 * Fast login form.
 */

function request_getFastLoginFormView(doneCallback)
{
    makePostRequest(makeCmsUrl('dialog/get-fast-login-form-view'),doneCallback);
}

/**
 * System settings management.
 */

function request_getSettingsView(doneCallback)
{
    makePostRequest(makeCmsUrl('settings/get-view'),doneCallback,null);
}
function request_saveSettings(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('settings/save'),doneCallback,postData);
}
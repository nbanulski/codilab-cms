function request_feedbackMail_send(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('feedback-mail/send'),doneCallback,postData);
}

function feedbackMail_attachEvents(){
    var feedbackForm=$('#subfooter .col-md-3 form[role="form"]');
    var submitButton = feedbackForm.find('button');
    submitButton.on('click',function(){
        var sender = feedbackForm.find('#feedback-from').val();
        var subject = feedbackForm.find('#feedback-subject').val();
        var message = feedbackForm.find('#feedback-message').val();

        var mailObject = {
            'from': sender,
            'subject': subject,
            'body': message
        };
        var postData = {
           'mailInfo' : mailObject
        };
        request_feedbackMail_send(postData,function(response){
            if(requestOkAndSessionExist(response))
            {
                console.log(response);
            }

        });
        return false;
    });

}
var inDesignMode=false;
var blockIsCurrentlyBeingSorted=false;
var blockIsCurrentlyBeingInserted=false;
var currentlyBeingSortedBlock=null;
var escKeyPressedWhileDragging=false;
// Disallow inserting more than one block in the same time:
var insertingBlockInProgress=false;
// Disallow multiple reorder processes in the same time:
var reorderingBlockInProgress=false;
var currentlyBeingSortedBlockSourceSlotId=null;
var lastInsertedBlockTemporaryDomTreeNode=null;
var isHidingAdminToolWndAllowedRightNow=true;

function switchBlockTabInAdminToolWnd(tabId)
{
    if(typeof tabId=='undefined'||!tabId)
    {
        return;
    }

    isHidingAdminToolWndAllowedRightNow=false;

    var adminToolWnd=$('#admin-toolwnd');
    var adminToolWndBottom=adminToolWnd.css('bottom').replace('px','');
    var bottom=0;

    var zendDeveloperToolbar=$('#zend-developer-toolbar');
    if(zendDeveloperToolbar.length>0)
    {
        bottom+=zendDeveloperToolbar.outerHeight();
    }

    var tabsContentsContainer=adminToolWnd.find('.tabs-contents-container');
    tabsContentsContainer.find('.inner[data-tab-id!="'+tabId+'"]').hide();
    tabsContentsContainer.find('.inner[data-tab-id="'+tabId+'"]').show(function(){
        if(adminToolWndBottom<0)
        {
            adminToolWnd.stop().animate({
                'bottom':bottom
            },200,'linear',function(){
                isHidingAdminToolWndAllowedRightNow=true;
            });
        }
        else
        {
            isHidingAdminToolWndAllowedRightNow=true;
        }
    });
}
function hideAdminToolWnd()
{
    var adminToolWnd=$('#admin-toolwnd');
    var bottom=-adminToolWnd.outerHeight();

    var zendDeveloperToolbar=$('#zend-developer-toolbar');
    if(zendDeveloperToolbar.length>0)
    {
        bottom+=zendDeveloperToolbar.outerHeight();
    }

    adminToolWnd.stop().animate({
        'bottom':bottom
    });
}
function getDroppedBlockOrderInSpecificSlot(slotId)
{
    var order=0;
    var slot=$('.admin-slot[data-slot-id="'+slotId+'"]');
    slot.find('.slot-item,.block-bar').each(function(){
        var element=$(this);
        order++;
        if(element.hasClass('block-bar'))
        {
            return false;
        }
    });
    return order;
}
function getPageBlocksOrderInSpecificSlot(slotId)
{
    var pageBlocksOrder=[];
    var order=0;
    var slot=$('.admin-slot[data-slot-id="'+slotId+'"]');
    slot.find('.slot-item').each(function(){
        var pageBlockId=$(this).attr('data-page-block-id');
        order++;
        pageBlocksOrder.push({'pageBlockId':pageBlockId,'order':order});
    });
    return pageBlocksOrder;
}
function request_insertBlockIntoPage(pageId,slotId,blockId,order)
{
    var postData={
        'pageId':pageId,
        'slotId':slotId,
        'blockId':blockId,
        'order':order
    };
    makePostRequest(makeCmsUrl('block/insert'),function(response){
        if(requestOkAndSessionExist(response))
        {
            if(response.meta.customStatus=='BLOCK_INSERTED')
            {
                var pageBlockId=response.data.pageBlockId;
                var phpClassName=lastInsertedBlockTemporaryDomTreeNode.attr('data-php-class-name');
                var phpClassNameInHyphenNotation=toHyphenNotation(phpClassName);
                createSlotForBlock(
                    lastInsertedBlockTemporaryDomTreeNode,pageBlockId,phpClassNameInHyphenNotation
                );
                reorderBlocksInSpecificSlot(slotId,function(response){
                    if(requestOkAndSessionExist(response))
                    {
                        var additionalData={
                            'firstDisplayAfterCreating':true
                        };
                        renderBlockAgain(pageBlockId,additionalData);
                    }
                    else
                    {
                        refreshPage();
                    }

                    var canRefreshIfNeeded=false;

                    return canRefreshIfNeeded;
                });
            }
            else
            {
                refreshPage();
            }
        }

        blockIsCurrentlyBeingInserted=false;
        insertingBlockInProgress=false;
        lastInsertedBlockTemporaryDomTreeNode=null;
    },postData);
}
function insertBlockIntoPage(pageId,slotId,blockId)
{
    if(insertingBlockInProgress)
    {
        return;
    }

    insertingBlockInProgress=true;

    var order=getDroppedBlockOrderInSpecificSlot(slotId);

    /*addCmsMessageToConsole('### Dropped new block:');
    addCmsMessageToConsole('* pageId: '+pageId);
    addCmsMessageToConsole('* blockId: '+blockId);
    addCmsMessageToConsole('* slotId: '+slotId);
    addCmsMessageToConsole('* block order in this slot: '+order);*/

    request_insertBlockIntoPage(pageId,slotId,blockId,order);
}
function request_reorderBlocksInSpecificSlot(pageId,slotId,pageBlocksOrder,specialCallback)
{
    var postData={
        'pageId':pageId,
        'slotId':slotId,
        'pageBlocksOrder':pageBlocksOrder
    };
    makePostRequest(makeCmsUrl('block/reorder'),function(response){
        var canRefreshIfNeeded=true;
        if($.isFunction(specialCallback))
        {
            canRefreshIfNeeded=specialCallback(response);
        }
        if(requestOkAndSessionExist(response))
        {
            if(response.meta.customStatus=='NOT_ALL_BLOCKS_WAS_REORDERED')
            {
                if(canRefreshIfNeeded)
                {
                    refreshPage();
                }
            }
        }

        reorderingBlockInProgress=false;
        currentlyBeingSortedBlockSourceSlotId=null;
    },postData);
}
function reorderBlocksInSpecificSlot(slotId,specialCallback)
{
    if(reorderingBlockInProgress)
    {
        return;
    }

    reorderingBlockInProgress=true;

    var pageId=getCurrentPageId();
    var pageBlocksOrder=getPageBlocksOrderInSpecificSlot(slotId);

    addCmsMessageToConsole('Reordering blocks in slot '+slotId+'.');

    request_reorderBlocksInSpecificSlot(pageId,slotId,pageBlocksOrder,specialCallback);
}
function moveBlockToAnotherSlot(pageBlockId,sourceSlotId,destinationSlotId)
{
    if(reorderingBlockInProgress||(pageBlockId<=0)||(sourceSlotId==destinationSlotId))
    {
        return;
    }

    reorderingBlockInProgress=true;

    var pageId=getCurrentPageId();
    var pageBlocksOrder=getPageBlocksOrderInSpecificSlot(destinationSlotId);

    addCmsMessageToConsole('Moving block '+pageBlockId+' from slot '+sourceSlotId+' to slot '+destinationSlotId+'.');

    makePostRequest(makeCmsUrl('block/move-to-another-slot'),function(response){
        if(requestOkAndSessionExist(response))
        {
            var status=response.meta.customStatus;
            if(status=='BLOCK_MOVED')
            {
                addCmsMessageToConsole('Block '+pageBlockId+' was successfully moved from slot '+sourceSlotId+' to slot '+destinationSlotId+'.');
            }
            else if(status=='NOT_ALL_BLOCKS_WAS_REORDERED')
            {
                addCmsMessageToConsole('Block '+pageBlockId+' was moved from slot '+sourceSlotId+' to slot '+destinationSlotId+'. '+
                    'Unfortunately blocks in slot '+destinationSlotId+' was probably only partially reordered.');
                refreshPage();
            }
        }

        reorderingBlockInProgress=false;
        currentlyBeingSortedBlockSourceSlotId=null;
    },{
        'pageId':pageId,
        'pageBlockId':pageBlockId,
        'sourceSlotId':sourceSlotId,
        'destinationSlotId':destinationSlotId,
        'pageBlocksOrder':pageBlocksOrder
    });
}
function showBlockSaveAsDynamicView(contents,title)
{
    var withSaveButton=true;
    jDialog(contents,title,'dialog-save-block-as-dynamic',function(id,success){
        if(success)
        {
            var dialog=$('#'+id);
            dialog.find('.modal-content .modal-footer').on('click','.btn-save',function(){
                var requestWillBeMade=saveBlockAsDynamic(dialog);
                if(requestWillBeMade)
                {
                    dialog.modal('hide');
                }
            });
        }
    },withSaveButton);
}
function saveBlockAsDynamic(dialogInstance)
{
    var dialog=$(dialogInstance);
    var form=dialog.find('.window-contents .form');
    var pageBlockId=form.attr('data-page-block-id');
    var formData=getFormData(form);

    var allNamesFilled=false;
    if(formData.names)
    {
        allNamesFilled=true;
        for(var prop in formData.names)
        {
            if(formData.names.hasOwnProperty(prop))
            {
                if(formData.names[prop]=='')
                {
                    allNamesFilled=false;
                    break;
                }
            }
        }
    }

    if(allNamesFilled)
    {
        var postData={
            'pageBlockId':pageBlockId,
            'formData':formData
        };
        request_saveBlockAsDynamic(postData,function(response){
            if(requestOkAndSessionExist(response))
            {
                if(response.meta.customStatus=='BLOCK_SAVED')
                {
                    renderBlockAgain(pageBlockId);
                    refreshDynamicBlockListInAdminToolWnd();
                    dialog.modal('hide');
                }
            }
        });

        return true;
    }

    return false;
}
function saveBlockSettings(dialogInstance)
{
    var dialog=$(dialogInstance);
    var form=dialog.find('.window-contents .form');
    var pageBlockId=form.attr('data-page-block-id');
    var settings=getFormData(form);

    request_saveBlockSettings(pageBlockId,settings,function(response){
        if(requestOkAndSessionExist(response))
        {
            if(response.meta.customStatus=='SETTINGS_SAVED')
            {
                renderBlockAgain(pageBlockId);
                dialog.modal('hide');
            }
        }
    });
}
function request_deleteBlock(pageBlockId)
{
    var block=$('.admin-slot .slot-item[data-page-block-id="'+pageBlockId+'"]');
    var blockName=toCamelCase(block.attr('data-block-name'));
    var onBeforeDeletedFunctionName=blockName+'_onBeforeDeleted';
    callFunctionIfExists(onBeforeDeletedFunctionName,[pageBlockId]);
    var postData={
        'pageBlockId':pageBlockId
    };
    makePostRequest(makeCmsUrl('block/delete'),function(response){
        if(requestOkAndSessionExist(response))
        {
            if(response.meta.customStatus=='BLOCK_DELETED')
            {
                block.animate({
                    opacity:0,
                    width:'0',
                    height:'0'
                },1000,function(){
                    block.remove();
                    var onAfterDeletedFunctionName=blockName+'_onAfterDeleted';
                    callFunctionIfExists(onAfterDeletedFunctionName,[pageBlockId]);
                });
            }
        }
    },postData);
}
function showAddEditWindow(type,title,btn_save_text,btn_cancel_text,additionalData)
{
    var expectedStatus=(type=='add')?'PAGE_ADDED':'PAGE_MODIFIED';
    var postData=null;
    if(type=='edit')
    {
        postData=additionalData;
    }

    request_getAddEditPageView(function(response){
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            var withSaveButton=true;
            jDialog(contents,title,'dialog-add-edit-page',function(id,success){
                if(success)
                {
                    var dialog=$('#'+id);
                    var unlockPageCleanUrlInput=dialog.find('#anp-unlock-page-clean-url-input');
                    var pageCleanUrlEditingLocked=false;
                    if(unlockPageCleanUrlInput.length>0)
                    {
                        pageCleanUrlEditingLocked=true;
                        dialog.find('#anp-clean-url').attr('disabled',true);
                    }

                    dialog.on('click keydown keyup paste blur','#anp-page-title',function(){
                        if(pageCleanUrlEditingLocked)
                        {
                            return true;
                        }
                        var cleanUrlInput=dialog.find('#anp-clean-url');
                        if((type=='add')||((type=='edit')&&(cleanUrlInput.val()!='')))
                        {
                            var title=$(this).val();
                            var cleanUrl=generateCleanUrlFromText(title);
                            cleanUrlInput.val(cleanUrl);
                            dialog.find('#anp-clean-url-path-span').text(cleanUrl);
                        }
                    });
                    dialog.on('click keydown keyup paste blur','#anp-clean-url',function(){
                        if(pageCleanUrlEditingLocked)
                        {
                            return false;
                        }
                        var cleanUrl=$.trim($(this).val());
                        dialog.find('#anp-clean-url-path-span').text(cleanUrl);
                    });
                    dialog.on('click','#anp-unlock-page-clean-url-input',function(){
                        var checked=$(this).prop('checked')?1:0;
                        var cleanUrlInput=dialog.find('#anp-clean-url');
                        pageCleanUrlEditingLocked=!checked;
                        if(checked)
                        {
                            cleanUrlInput.removeAttr('disabled');
                        }
                        else
                        {
                            cleanUrlInput.attr('disabled',true);
                        }
                    });
                    dialog.on('click change','#anp-page-language',function(){
                        var selectedOption=$(this).find('option:selected');
                        var languageId=$.trim(selectedOption.val());
                        var isoCode=selectedOption.attr('data-iso-code');
                        dialog.find('[id^="anp-language-equivalent-row-"]').show();
                        dialog.find('#anp-language-equivalent-row-'+languageId).hide();
                        dialog.find('#anp-clean-url-iso-code-span').text(isoCode);
                    });
                    var languageId=$.trim(dialog.find('#anp-page-language option:selected').val());
                    dialog.find('[id^="anp-language-equivalent-row-"]').show();
                    dialog.find('#anp-language-equivalent-row-'+languageId).hide();
                    dialog.on('click','.btn-save',function(){
                        var form=dialog.find('.form');
                        var formData=getFormData(form);
                        var postData=$.extend({
                            'dataPosted':true,
                            'formData':formData
                        },additionalData);

                        request_addEditPage(function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                if(response.meta.customStatus==expectedStatus)
                                {
                                    dialog.modal('hide');

                                    var pagesWindow=$('#dialog-pages');
                                    if(pagesWindow&&pagesWindow.length>0)
                                    {
                                        var itemCountPerPage=pagesWindow.find('.icpp-dropdown option:selected').val();
                                        request_getPagesView(function(response){
                                            if(requestOkAndSessionExist(response))
                                            {
                                                pagesWindow.find('.modal-content .modal-body').html(response.data);
                                            }
                                        },{
                                            'itemCountPerPage':itemCountPerPage
                                        });
                                    }
                                    else
                                    {
                                        refreshPage();
                                    }
                                }
                            }
                        },postData);
                    });
                }
            },withSaveButton);
        }
    },postData);
}
function showUserAddEditWindow(type,title,btn_save_text,btn_cancel_text,additionalData)
{
    var userId=additionalData?additionalData.userId:null;
    var expectedStatus=(type=='add')?'USER_ADDED':'USER_MODIFIED';
    var postData={};
    postData=$.extend(postData,additionalData);

    request_getUserAddEditView(function(response){
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            var withSaveButton=true;
            jDialog(contents,title,'dialog-add-edit-user',function(id,success){
                if(success)
                {
                    var dialog=$('#'+id);
                    dialog.on('click','#anu-btn-random-password',function(){
                        var randomPassword=generateRandomString(8);
                        dialog.find('#anu-user-password,#anu-user-password2').val(randomPassword);
                    });
                    dialog.on('click','.btn-save',function(){
                        var form=dialog.find('.form');
                        var formData=getFormData(form);
                        var postData=$.extend({
                            'dataPosted':true,
                            'formData':formData
                        },additionalData);

                        request_addEditUser(function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                if(response.meta.customStatus==expectedStatus)
                                {
                                    dialog.modal('hide');

                                    var listingsWindow=$('#dialog-users');
                                    if(listingsWindow&&listingsWindow.length>0)
                                    {
                                        var itemCountPerPage=listingsWindow.find('.icpp-dropdown option:selected').val();
                                        request_getUsersView(function(response){
                                            if(requestOkAndSessionExist(response))
                                            {
                                                listingsWindow.find('.modal-content .modal-body').html(response.data);
                                            }
                                        },{
                                            'itemCountPerPage':itemCountPerPage
                                        });
                                    }
                                    else
                                    {
                                        refreshPage();
                                    }
                                }
                            }
                        },postData);
                    });
                }
            },withSaveButton);
        }
    },postData);
}

function savePageBlockTitle(pageBlockId,titleContainerId)
{
    if((pageBlockId<=0)||!titleContainerId)
    {
        return;
    }

    var id=titleContainerId;
    var pageBlockTitle=null;

    if(getCkeditorInstanceById(id))
    {
        pageBlockTitle=getCkeditorData(id); // CKEditor.
    }
    else
    {
        pageBlockTitle=$('#'+id).html(); // HTML5 Content-Editable element.
    }

    makePostRequest(makeCmsUrl('block/update-title'),function(response){
        if(requestOkAndSessionExist(response))
        {
            if(response.meta.customStatus=='BLOCK_TITLE_SAVED')
            {
                //addCmsMessageToConsole('Block title successfully saved.');
            }
        }
    },{
        'pageBlockId':pageBlockId,
        'title':pageBlockTitle
    });
}
function attachManagementFunctionsToManagementWnd(
    wndSelectorOrInstance,viewRequestGetFunction,viewGetCallback,
    togglePublishingCallback,addNewCallback,actionCallback,onCloseCallback,onOpenCallback
)
{
    var dialog=$(wndSelectorOrInstance);
    dialog.on('change','.icpp-dropdown',function(){
        var pageBlockId=dialog.find('.window-contents').attr('data-page-block-id');
        var activeAnchor=dialog.find('.pagination li.active a');
        var currentPageNumber=toInt($.trim(activeAnchor.text()));
        var itemCountPerPage=$(this).val();
        var includePagesFrom=$('.ipf-dropdown option:selected').val();
        var form=dialog.find('.modal-content .modal-body');
        var additionalData=getFormData(form);
        viewRequestGetFunction(function(response){
            if(requestOkAndSessionExist(response))
            {
                dialog.find('.modal-content .modal-body').html(response.data);
                if(viewGetCallback)
                {
                    viewGetCallback(response);
                }
            }
        },{
            'pageBlockId':pageBlockId,
            'pageNumber':currentPageNumber,
            'itemCountPerPage':itemCountPerPage,
            'includePagesFrom':includePagesFrom,
            'additionalData':additionalData
        });
    });
    dialog.on('change','.ipf-dropdown',function(){
        var pageBlockId=dialog.find('.window-contents').attr('data-page-block-id');
        var activeAnchor=dialog.find('.pagination li.active a');
        var currentPageNumber=toInt($.trim(activeAnchor.text()));
        var itemCountPerPage=dialog.find('.icpp-dropdown option:selected').val();
        var includePagesFrom=$(this).val();
        var form=dialog.find('.modal-content .modal-body');
        var additionalData=getFormData(form);
        viewRequestGetFunction(function(response){
            if(requestOkAndSessionExist(response))
            {
                dialog.find('.modal-content .modal-body').html(response.data);
                if(viewGetCallback)
                {
                    viewGetCallback(response);
                }
            }
        },{
            'pageBlockId':pageBlockId,
            'pageNumber':currentPageNumber,
            'itemCountPerPage':itemCountPerPage,
            'includePagesFrom':includePagesFrom,
            'additionalData':additionalData
        });
    });
    dialog.on('click','.btn-refresh',function(){
        var pageBlockId=dialog.find('.window-contents').attr('data-page-block-id');
        var activeAnchor=dialog.find('.pagination li.active a');
        var currentPageNumber=toInt($.trim(activeAnchor.text()));
        var itemCountPerPage=dialog.find('.icpp-dropdown option:selected').val();
        var includePagesFrom=dialog.find('.ipf-dropdown option:selected').val();
        var form=dialog.find('.modal-content .modal-body');
        var additionalData=getFormData(form);
        viewRequestGetFunction(function(response){
            if(requestOkAndSessionExist(response))
            {
                dialog.find('.modal-content .modal-body').html(response.data);
                if(viewGetCallback)
                {
                    viewGetCallback(response);
                }
            }
        },{
            'pageBlockId':pageBlockId,
            'pageNumber':currentPageNumber,
            'itemCountPerPage':itemCountPerPage,
            'includePagesFrom':includePagesFrom,
            'additionalData':additionalData
        });
    });
    if(togglePublishingCallback)
    {
        dialog.on('click','.btn-toggle-publishing',function(){
            var button=$(this);
            var elementId=button.closest('tr').attr('data-element-id');
            var published=button.prop('checked')?1:0;
            togglePublishingCallback(published,elementId,button);
        });
    }
    if(actionCallback)
    {
        dialog.on('click','.actions [data-action]',function(){
            var button=$(this);
            var action=button.attr('data-action');
            var elementId=button.closest('tr').attr('data-element-id');
            actionCallback(elementId,action,button);
        });
    }
    dialog.on('click','.pagination li:not(.disabled):not(.active) a',function(){
        var pageBlockId=dialog.find('.window-contents').attr('data-page-block-id');
        var anchor=$(this);
        var text=$.trim(anchor.text());
        var pageNumber=null;
        if(text=='<<')
        {
            var activeAnchor=anchor.closest('ul').find('li.active a');
            var currentPageNumber=toInt($.trim(activeAnchor.text()));
            pageNumber=currentPageNumber-1;
        }
        else if(text=='>>')
        {
            var activeAnchor=anchor.closest('ul').find('li.active a');
            var currentPageNumber=toInt($.trim(activeAnchor.text()));
            pageNumber=currentPageNumber+1;
        }
        else
        {
            pageNumber=toInt(text);
        }
        var itemCountPerPage=dialog.find('.icpp-dropdown option:selected').val();
        var includePagesFrom=dialog.find('.ipf-dropdown option:selected').val();
        var form=dialog.find('.modal-content .modal-body');
        var additionalData=getFormData(form);
        viewRequestGetFunction(function(response){
            if(requestOkAndSessionExist(response))
            {
                dialog.find('.modal-content .modal-body').html(response.data);
                if(viewGetCallback)
                {
                    viewGetCallback(response);
                }
            }
        },{
            'pageBlockId':pageBlockId,
            'pageNumber':pageNumber,
            'itemCountPerPage':itemCountPerPage,
            'includePagesFrom':includePagesFrom,
            'additionalData':additionalData
        });

        return false;
    });
    if(addNewCallback)
    {
        dialog.on('click','.btn-add-new',function(){
            var button=$(this);
            addNewCallback(button);
        });
    }
    if(onCloseCallback)
    {
        dialog.on('click','.btn-close,button.close',function(){
            var button=$(this);
            onCloseCallback(button);
        });
    }
    if(onOpenCallback)
    {
        onOpenCallback(dialog);
    }
}
function refreshPageBlockManagementWindow(pageBlockId)
{
    request_getBlockManagementView(function(response){
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            if(contents!='')
            {
                $('#dialog-block-management .modal-body').html(contents);
            }
        }
    },{
        'pageBlockId':pageBlockId
    });
}
function handlePageBlockManagementWindow(
    pageBlockId,viewRequestGetFunction,viewGetCallback,togglePublishingCallback,addNewCallback,actionCallback,
    onCloseCallback,otherSelectorOrInstance,optionalCssClassPrefix,otherTitlePrefix,optionalSourcePageBlockId,
    onOpenCallback
)
{
    $('.window-contents.admin[data-page-block-id="'+pageBlockId+'"] a.history').popover({'placement':'bottom'});
    var sourcePageBlockId=pageBlockId;
    if((typeof optionalSourcePageBlockId!='undefined')&&(optionalSourcePageBlockId>0))
    {
        sourcePageBlockId=toInt(optionalSourcePageBlockId);
    }

    var domActivator=null;
    if(otherSelectorOrInstance)
    {
        domActivator=$(otherSelectorOrInstance);
    }
    else
    {
        domActivator=$('.admin-slot .slot-item[data-page-block-id="'+pageBlockId+'"] .block-frame .title-bar .buttons .management');
    }
    if(!optionalCssClassPrefix)
    {
        optionalCssClassPrefix='';
    }

    domActivator.click(function(){
        if(typeof otherTitlePrefix=='undefined'||!otherTitlePrefix)
        {
            var title=$.trim($(this).closest('.title-bar').find('.block-name .name').text());
        }
        else
        {
            var title=otherTitlePrefix;
        }
        title+=' - Zarządzanie';

        var postData={
            'pageBlockId':sourcePageBlockId
        };
        viewRequestGetFunction(function(response){
            if(requestOkAndSessionExist(response))
            {
                var contents=response.data;
                var withSaveButton=false;
                jDialog(contents,title,'dialog-block-'+optionalCssClassPrefix+'management',function(id,success){
                    if(success)
                    {
                        attachManagementFunctionsToManagementWnd(
                            '#'+id,
                            viewRequestGetFunction,
                            viewGetCallback,
                            togglePublishingCallback,
                            addNewCallback,
                            actionCallback,
                            onCloseCallback,
                            onOpenCallback
                        );
                    }
                },withSaveButton,onCloseCallback);
            }
        },postData);
    });
}
function updateTitleAdjustingButtons(titleSelectorOrInstance,titleSize)
{
    var title=$(titleSelectorOrInstance);
    var increaseAnchor=title.find('.increase-title-size').closest('.btn');
    var decreaseAnchor=title.find('.decrease-title-size').closest('.btn');
    if(titleSize<=1)
    {
        increaseAnchor.hide();
    }
    else
    {
        increaseAnchor.show();
    }
    if(titleSize>=6)
    {
        decreaseAnchor.hide();
    }
    else
    {
        decreaseAnchor.show();
    }
}

function attachSupportForBlockImageEditView(viewSelectorOrInstance,onUpdateCallback,otherAspectRatio)
{
    loadJsFile('/libs/images-loaded/imagesloaded.pkgd.min.js');

    var view=$(viewSelectorOrInstance);
    //var pageBlockId=view.find('input[name="pageBlockId"]').val();
    var originalImage=view.find('img.original-image');
    var imageCropRectCoordsInput=view.find('.image-crop-rect-coords');
    //var uniqueHashForFileName=view.find('.unique-hash-for-file-name').val();

    var aspectRatio=23/16;
    if((typeof otherAspectRatio!='undefined')&&(otherAspectRatio!=0.0))
    {
        aspectRatio=otherAspectRatio;
    }

    $('#ai-original-image-scrollable-container').imagesLoaded().done(function(){
        //var jCropApi=null;
        var jCropApi=$.Jcrop(originalImage,{
            'aspectRatio':aspectRatio,
            'minSize':[16,16],
            'bgOpacity':.6,
            'keySupport':false,
            'onChange':function(c){
                var coordsString=c.x+','+c.y+','+c.x2+','+c.y2;
                imageCropRectCoordsInput.val(coordsString);
            }
        }/*,function(){
            jCropApi=this;
        }*/);

        var c=convertRectCoordsStringToArray(imageCropRectCoordsInput.val());
        if(c)
        {
            jCropApi.setSelect([c[0],c[1],c[2],c[3]]);
        }

        view.on('click','.btn-select-entire-image',function(){
            var thumbW=originalImage.width();
            var thumbH=originalImage.height();
            jCropApi.setSelect([0,0,thumbW,thumbH]);
        });
    });

    view.on('click','.btn-save',function(){
        var form=view.find('.form');
        var formData=getFormData(form);
        var postData={
            'formData':formData
        };
        request_saveImageChanges(function(response){
            var success=false;

            if(requestOkAndSessionExist(response))
            {
                var expectedStatus='IMAGE_CHANGES_SAVED';
                if(response.meta.customStatus==expectedStatus)
                {
                    view.modal('hide');

                    success=true;
                }
            }

            if(onUpdateCallback)
            {
                var attachmentInfo=response.data;

                onUpdateCallback(success,attachmentInfo);
            }
        },postData);
    });
}

function attachEventToPageBlockSaveAsDynamicBlockButton(saveAsDynamicBlockButtonSelectorOrInstance)
{
    var saveAsDynamicBlockButton=$(saveAsDynamicBlockButtonSelectorOrInstance);
    saveAsDynamicBlockButton.off('click').on('click',function(){
        var block=$(this).closest('.slot-item');
        var pageId=getCurrentPageId();
        var pageBlockId=block.attr('data-page-block-id');
        var postData={
            'pageId':pageId,
            'pageBlockId':pageBlockId
        };
        request_getBlockSaveAsDynamicView(postData,function(response){
            if(requestOkAndSessionExist(response))
            {
                var contents=response.data;
                var title='Saving block (id: '+pageBlockId+') as dynamic block';
                showBlockSaveAsDynamicView(contents,title);
            }
        });
    });
}

function attachEventToPageBlockTogglePublishingButton(togglePublishingButtonSelectorOrInstance)
{
    var togglePublishingButton=$(togglePublishingButtonSelectorOrInstance);
    togglePublishingButton.off('click').on('click',function(){
        var block=$(this).closest('.slot-item');
        var pageBlockId=block.attr('data-page-block-id');
        var blockFrame=block.find('.block-frame');
        var blockContent=block.find('.block-content');

        request_toggleBlockPublishing(pageBlockId,function(response){
            if(requestOkAndSessionExist(response))
            {
                var status=response.meta.customStatus;
                if(status=='PUBLISHED')
                {
                    blockFrame.animate({
                        'opacity':1
                    },1000,function(){
                    });
                    blockContent.animate({
                        'opacity':1
                    },1000,function(){
                    });
                }
                else if(status=='UNPUBLISHED')
                {
                    blockFrame.animate({
                        'opacity':0.5
                    },1000,function(){
                    });
                    blockContent.animate({
                        'opacity':0.5
                    },1000,function(){
                    });
                }
            }
        });
    });
}

function attachEventToPageBlockSettingsButton(settingsButtonSelectorOrInstance)
{
    var settingsButton=$(settingsButtonSelectorOrInstance);
    settingsButton.off('click').on('click',function(){
        var pageBlockId=$(this).closest('.slot-item').attr('data-page-block-id');
        var title=$.trim($(this).closest('.title-bar').find('.block-name').text())+' - Settings';

        request_getBlockSettingsView(pageBlockId,function(response){
            if(requestOkAndSessionExist(response))
            {
                var contents=response.data;
                var withSaveButton=true;
                jDialog(contents,title,'dialog-block-settings',function(id,success){
                    if(success)
                    {
                        var dialog=$('#'+id);
                        dialog.on('click','.btn-save',function(){
                            saveBlockSettings(dialog);
                        });
                    }
                },withSaveButton);
            }
        });
    });
}

function attachEventToPageBlockDeleteButton(deleteButtonSelectorOrInstance)
{
    var deleteButton=$(deleteButtonSelectorOrInstance);
    deleteButton.off('click').on('click',function(){
        var pageBlockId=$(this).closest('.slot-item').attr('data-page-block-id');
        var title=$.trim($(this).closest('.title-bar').find('.block-name').html());

        jConfirm('Czy na pewno chcesz usunąć blok &quot;'+title+'&quot; ?','Usuwanie bloku','dialog-delete-block',function(id,confirmed){
            if(confirmed)
            {
                request_deleteBlock(pageBlockId);
            }
        });
    });
}

function attachEventsToPageBlockTitle(blockContentSelectorOrInstance)
{
    var blockContent=$(blockContentSelectorOrInstance);

    /**
     * Show / hide block title's buttons on hover.
     */
    blockContent.off('mouseenter','.title').on('mouseenter','.title',function(){
        $(this).find('.btn-group-vertical').show();
    }).on('mouseleave','.title',function(){
        $(this).find('.btn-group-vertical').hide();
    });

    /**
     * Increase / decrease title size by using "plus" and "minus" buttons.
     * TODO: These functions needs refactoring!
     */
    blockContent.off('click','> .title > .title-adjusting .increase-title-size')
        .on('click','> .title > .title-adjusting .increase-title-size',function(){
            var blockContent=$(this).closest('.block-content');
            var title=blockContent.find('> .title');
            var pageBlockId=title.closest('.slot-item').attr('data-page-block-id');

            var data={
                'pageBlockId':pageBlockId,
                'action':'increase'
            };

            request_changeBlockTitleSize(function(response){
                if(requestOkAndSessionExist(response))
                {
                    var titleText=response.data.title;
                    var titleSize=response.data.titleSize;
                    if(titleSize!==null)
                    {
                        titleSize=toInt(titleSize);
                        if((titleSize>=1)&&(titleSize<=6))
                        {
                            var contents=title.html();
                            var classes=title.attr('class');

                            title.replaceWith('<h'+titleSize+' class="'+classes+'"></h'+titleSize+'>');
                            title=blockContent.find('> .title');
                            title.html(contents);
                        }
                        title.find('.text').html(titleSize?titleText:'&nbsp;');

                        updateTitleAdjustingButtons(title,titleSize);
                    }
                    else
                    {
                        addCmsErrorToConsole('AJAX request returns title size equal to NULL!');
                        refreshPage();
                    }
                }
            },data);
            return false;
        });
    blockContent.off('click','> .title > .title-adjusting .decrease-title-size')
        .on('click','> .title > .title-adjusting .decrease-title-size',function(){
            var blockContent=$(this).closest('.block-content');
            var title=blockContent.find('> .title');
            var pageBlockId=title.closest('.slot-item').attr('data-page-block-id');

            var data={
                'pageBlockId':pageBlockId,
                'action':'decrease'
            };

            request_changeBlockTitleSize(function(response){
                if(requestOkAndSessionExist(response))
                {
                    var titleText=response.data.title;
                    var titleSize=response.data.titleSize;
                    if(titleSize!==null)
                    {
                        titleSize=toInt(titleSize);
                        if((titleSize>=1)&&(titleSize<=6))
                        {
                            var contents=title.html();
                            var classes=title.attr('class');

                            title.replaceWith('<h'+titleSize+' class="'+classes+'"></h'+titleSize+'>');
                            title=blockContent.find('> .title');
                            title.html(contents);
                        }
                        title.find('.text').html(titleSize?titleText:'&nbsp;');

                        updateTitleAdjustingButtons(title,titleSize);
                    }
                    else
                    {
                        addCmsErrorToConsole('AJAX request returns title size equal to NULL!');
                        refreshPage();
                    }
                }
            },data);
            return false;
        });

    /**
     * Enter into edit-mode on double click on block title.
     */
    blockContent.off('dblclick','> .title .text').on('dblclick','> .title .text',function(){
        loadCkEditorLibrary();

        var blockTitleContainer=$(this);

        var parentElement=blockTitleContainer.parent();
        var currentTitle=blockTitleContainer.text();
        blockTitleContainer.replaceWith('<div class="text">'+currentTitle+'</div>');
        blockTitleContainer=parentElement.find('> div.text');

        var pageBlockId=blockTitleContainer.closest('.slot-item').attr('data-page-block-id');
        var titleContainerId='block-title-editable-'+pageBlockId;
        blockTitleContainer.attr('id',titleContainerId);
        blockTitleContainer.attr('contenteditable','true');
        blockTitleContainer.ckeditor($.extend(defaultCkeditorConfig,{
            'removeButtons':'',
            'toolbar':[
                {
                    name:'document',
                    items:[
                    ]
                },
                {
                    name:'basicstyles',
                    items:[
                        'Bold','Italic','Underline','Strike','-','RemoveFormat'
                    ]
                },
                '/',
                {
                    name:'paragraph',
                    items:[
                        'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'
                    ]
                },
                {
                    name:'colors',
                    items:[
                        'TextColor'
                    ]
                }
            ]
        }));
        //blockTitleContainer.focus();

        var editor=getCkeditorInstanceById(titleContainerId);
        editor.on('blur',function(e){
            if($(document.activeElement).get(0).tagName.toLowerCase()!="iframe"){
                var editor=$(this)[0];
                if(editor)
                {
                    var id=editor.name;
                    if(id!='')
                    {
                        savePageBlockTitle(pageBlockId,titleContainerId);

                        destroyCkeditor(id);
                        $('#'+id).removeAttr('contenteditable');
                    }
                }
            }
        });
    });
}

function attachEventsToAdminToolWindowInnerContainer(tabId)
{
    var tabId=toInt(tabId);
    var adminToolWnd=$('#admin-toolwnd');
    adminToolWnd.find('.tabs-contents-container .inner[data-tab-id="'+tabId+'"] .block-bar')
        .draggable({
            handle:'.insert',
            helper:'clone',
            cursor:'move',
            connectToSortable:'.admin-slot',
            cursorAt:{top:14,right:20},
            start:function(){
                //addCmsMessageToConsole('jQuery UI draggable\'s start() event called');
                blockIsCurrentlyBeingInserted=true;
                hideAdminToolWnd();
                $('.admin-slot').addClass('presented');
            },
            drag:function(event,ui){
                //addCmsMessageToConsole('jQuery UI draggable\'s drag() event called');
                if(escKeyPressedWhileDragging)
                {
                    //addCmsMessageToConsole('Canceling drag by ESC key.');
                    $(this).closest('.block-bar').data('drag-canceled',true);
                    escKeyPressedWhileDragging=false;

                    return false;
                }

                return true;
            },
            stop:function(){
                //addCmsMessageToConsole('jQuery UI draggable\'s stop() event called');
                blockIsCurrentlyBeingInserted=false;
                $('.admin-slot').removeClass('presented');
            }
        })
        .disableSelection();
    adminToolWnd.find('.tabs-contents-container .inner[data-tab-id="'+tabId+'"]')
        .sortable({
            handle:'.move',
            items:'.block-bar',
            cursor:'move',
            forcePlaceholderSize:true,
            stop:function(event,ui){
                var draggedElement=$(ui.item[0]);
                var container=draggedElement.closest('.inner');
                var dynamic=container.attr('data-dynamic-blocks');
                var blocks=[];
                var order=0;
                container.find('.block-bar').each(function(){
                    var blockId=$(this).attr('data-block-id');
                    order++;
                    blocks.push({'blockId':blockId,'order':order});
                });

                var postData={
                    'blocks':blocks,
                    'dynamic':dynamic?1:0
                };
                request_reorderBlocksInAdminToolWnd(postData);
            }
        })
        .disableSelection();
    adminToolWnd
        .off('click','.tabs-contents-container .inner .block-bar .edit')
        .on('click','.tabs-contents-container .inner .block-bar .edit',function(){
            var blockBar=$(this).closest('.block-bar');
            var blockId=blockBar.attr('data-block-id');
            var postData={
                'blockId':blockId
            };
            request_getEditBlockPropertiesView(postData,function(response){
                if(requestOkAndSessionExist(response))
                {
                    var contents=response.data;
                    var title='Edycja właściwości bloku';
                    var customId='dialog-edit-block-properties';
                    var withSaveButton=true;
                    jDialog(contents,title,customId,function(id,success){
                        var dialog=$('#'+id);
                        dialog.on('click','.btn-save',function(){
                            var form=dialog.find('.form');
                            var blockId=form.attr('data-block-id');
                            var formData=getFormData(form);
                            var postData={
                                'blockId':blockId,
                                'formData':formData
                            };
                            request_updateBlockProperties(postData,function(response){
                                if(requestOkAndSessionExist(response))
                                {
                                    if(response.meta.customStatus=='BLOCK_PROPERTIES_UPDATED')
                                    {
                                        var isBlockDynamic=blockBar.closest('.inner').attr('data-dynamic-blocks');
                                        dialog.modal('hide');
                                        renderBlockAgainByBlockId(blockId);
                                        if(isBlockDynamic)
                                        {
                                            refreshDynamicBlockListInAdminToolWnd();
                                        }
                                        else
                                        {
                                            refreshBlockListInAdminToolWnd();
                                        }
                                    }
                                }
                            });
                        });
                    },withSaveButton);
                }
            });
        });
    if(adminToolWnd.find('.tabs-contents-container .inner[data-tab-id="2"] .block-bar.ui-draggable').length==0)
    {
        adminToolWnd
            .off('click','.tabs-contents-container .inner[data-tab-id="2"] .delete')
            .on('click','.tabs-contents-container .inner[data-tab-id="2"] .delete',function(){
                var blockBar=$(this).closest('.block-bar');
                var blockId=blockBar.attr('data-block-id');
                var postData={
                    'blockId':blockId
                };
                request_getDynamicBlockCount(postData,function(response){
                    if(requestOkAndSessionExist(response))
                    {
                        var blockCount=toInt(response.data);
                        var blockName=$.trim(blockBar.find('.block-name .text').text());
                        var contents=
                            'Próbujesz usunąć blok &quot;'+blockName+'&quot;. '+
                            'Blok zostanie usunięty ze wszystkich stron oraz z tej listy. Usunięte zostaną również powiązane '+
                            'z nim dane. Ta operacja jest nieodwracalna!';

                        if(blockCount>0)
                        {
                            contents+=' Obecna ilość wystąpień tego bloku: '+blockCount+'.';
                        }

                        contents+=
                            '<p>Czy na pewno chcesz CAŁKOWICIE usunąć ten blok?</p>';
                        var title='Potwierdź usunięcie bloku dynamicznego';
                        var customId='dialog-confirm-dynamic-block-deletion';
                        jConfirm(contents,title,customId,function(id,confirmed){
                            if(confirmed)
                            {
                                var postData={
                                    'blockId':blockId
                                };
                                request_deleteDynamicBlock(postData,function(response){
                                    if(requestOkAndSessionExist(response))
                                    {
                                        refreshDynamicBlockListInAdminToolWnd();
                                    }
                                });
                            }
                        });

                        return false;
                    }
                });
            });
    }
}

function createSlotForBlock(blockTemporaryDomTreeNode,pageBlockId,blockNameInHyphenNotation)
{
    if((typeof blockTemporaryDomTreeNode!='object')||!blockTemporaryDomTreeNode)
    {
        return false;
    }

    var blockName='block-'+blockNameInHyphenNotation;

    blockTemporaryDomTreeNode.replaceWith(
        '<div class="slot-item '+blockName+'" data-page-block-id="'+pageBlockId+'" data-block-name="'+blockName+'"></div>'
    );
}


function filterItemsInDialogWindowUsingFastSearch(dialogWndInstanceOrSelector,viewRequestGetFunction,viewGetCallback)
{
    var dialog=$(dialogWndInstanceOrSelector);

    if(!dialog||(dialog.length<1))
    {
        return;
    }

    var fastSearchInput=dialog.find('.fast-search');
    var searchText=$.trim(fastSearchInput.val());
    if(searchText.length>1)
    {
        var pageBlockId=dialog.find('.window-contents').attr('data-page-block-id');
        var activeAnchor=dialog.find('.pagination li.active a');
        var currentPageNumber=toInt($.trim(activeAnchor.text()));
        var itemCountPerPage=dialog.find('.icpp-dropdown option:selected').val();
        var includePagesFrom=dialog.find('.ipf-dropdown option:selected').val();
        var postData={
            'pageBlockId':pageBlockId,
            'pageNumber':currentPageNumber,
            'itemCountPerPage':itemCountPerPage,
            'includePagesFrom':includePagesFrom,
            'searchText':searchText
        }
        viewRequestGetFunction(function(response){
            if(requestOkAndSessionExist(response))
            {
                dialog.find('.modal-content .modal-body').html(response.data);
                if(viewGetCallback)
                {
                    viewGetCallback(response);
                }
            }
        },postData);
    }
}

function searchUsingTopSearchArea(destinationPageCleanUrl)
{
    var currentUrl=getCurrentUrl();
    var currentPageFullCleanUrl=getFullPageCleanUrlFromUrl(currentUrl);
    var topSearch=$('#top-search');
    var input=topSearch.find('input[type="text"]');
    var query=$.trim(input.val());
    var parameters={
        'query':query
    }

    if(currentPageFullCleanUrl!=destinationPageCleanUrl)
    {
        var urlParameters=$.param(parameters);
        var url=makeCmsUrl('page/'+destinationPageCleanUrl)+'?'+urlParameters;
        addCmsMessageToConsole('Searching started. Url: '+url);
        redirect(url);
    }
    else
    {
        addCmsMessageToConsole('Searching was repeated. Redirection will not occur.');
        addCmsMessageToConsole('Search parameters: '+parameters);
    }
}

function refreshBlockListInAdminToolWnd()
{
    var adminToolWnd=$('#admin-toolwnd');
    var postData={
        'languageCategoryId':adminToolWnd.find('#block-list-languages option:selected').val(),
        'phpClassName':adminToolWnd.find('#block-list-categories option:selected').val(),
        'blockNameQuery':adminToolWnd.find('#block-list-search-field').val()
    };
    request_getBlockListView(postData,function(response){
        if(requestOkAndSessionExist(response))
        {
            var viewHtml=$.trim(response.data);
            var tabId=1;
            adminToolWnd.find('.tabs-contents-container .inner[data-tab-id="'+tabId+'"]').html(viewHtml);
            attachEventsToAdminToolWindowInnerContainer(tabId);
        }
    });
}
function refreshDynamicBlockListInAdminToolWnd()
{
    var adminToolWnd=$('#admin-toolwnd');
    var postData={
        'languageCategoryId':adminToolWnd.find('#block-list-languages option:selected').val(),
        'phpClassName':adminToolWnd.find('#block-list-categories option:selected').val(),
        'blockNameQuery':adminToolWnd.find('#block-list-search-field').val()
    };
    request_getDynamicBlockListView(postData,function(response){
        if(requestOkAndSessionExist(response))
        {
            var viewHtml=$.trim(response.data);
            var tabId=2;
            adminToolWnd.find('.tabs-contents-container .inner[data-tab-id="'+tabId+'"]').html(viewHtml);
            attachEventsToAdminToolWindowInnerContainer(tabId);
        }
    });
}

function filterItemsInBlocksDialogWindow(dialogWndInstanceOrSelector,viewRequestGetFunction,viewGetCallback)
{
    var dialog=$(dialogWndInstanceOrSelector);

    if(!dialog||(dialog.length<1))
    {
        return;
    }

    var pageQueryInput=dialog.find('.page-query');
    var blockQueryInput=dialog.find('.block-query');
    var pageQuery=$.trim(pageQueryInput.val());
    var blockQuery=$.trim(blockQueryInput.val());
    if((pageQuery.length>1)||(blockQuery.length>1))
    {
        var pageBlockId=dialog.find('.window-contents').attr('data-page-block-id');
        var activeAnchor=dialog.find('.pagination li.active a');
        var currentPageNumber=toInt($.trim(activeAnchor.text()));
        var itemCountPerPage=dialog.find('.icpp-dropdown option:selected').val();
        var includePagesFrom=dialog.find('.ipf-dropdown option:selected').val();
        var postData={
            'pageBlockId':pageBlockId,
            'pageNumber':currentPageNumber,
            'itemCountPerPage':itemCountPerPage,
            'includePagesFrom':includePagesFrom,
            'pageQuery':pageQuery,
            'blockQuery':blockQuery
        }
        viewRequestGetFunction(function(response){
            if(requestOkAndSessionExist(response))
            {
                dialog.find('.modal-content .modal-body').html(response.data);
                if(viewGetCallback)
                {
                    viewGetCallback(response);
                }
            }
        },postData);
    }
}

function handleCustomFunctionalityInSettingsWindow()
{
    var dialog=$('#dialog-settings');
    var form=dialog.find('.form');

    attachCkEditorToDomElement(form.find('.message-about-cookies'),true);
    /*attachCkEditorToDomElement(form.find('.default-contents-for-mail-for-user-about-user-signed-up-for-event'));
    attachCkEditorToDomElement(form.find('.default-contents-for-mail-for-admin-about-user-signed-up-for-event'));*/
}

function showDialogWindowWithListOfUsersSignedUpForEvent(eventId,contentId)
{
    loadBlockCssFile('event-list', 'event-list-signing-up-for-event');
    loadBlockJsFile('event-list', 'event-list-signing-up-for-event');
    var postData={
        'additionalData':{
            'eventId':eventId,
            'contentId':contentId
        }
    };
    request_getListOfUsersSignedUpForEventView(function(response){
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            var title='Lista użytkowników zapisanych na wydarzenie';
            var withSaveButton=false;
            jDialog(contents,title,'dialog-users-signed-up-for-event',function(id,success){
                if(success)
                {
                    var viewRequestGetFunction=request_getListOfUsersSignedUpForEventView;
                    var viewGetCallback=null;

                    var dialog=$('#'+id);

                    dialog.on('keypress','#users-signed-up-for-event-fast-search',function(e){
                        if(e.which==13)
                        {
                            filterItemsInDialogWindowUsingFastSearch(
                                dialog,viewRequestGetFunction,viewGetCallback
                            );
                        }
                    });
                    dialog.on('click','#users-signed-up-for-event-btn-fast-search',function(){
                        filterItemsInDialogWindowUsingFastSearch(
                            dialog,viewRequestGetFunction,viewGetCallback
                        );
                    });

                    dialog.find('#users-signed-up-for-event-fast-search').focus();

                    var togglePublishingCallback=false;
                    var addNewCallback=false;
                    var actionCallback=function(elementId,action,button){
                        var subscriptionId=elementId;
                        if(action=='preview-or-edit')
                        {
                            var postData={
                                'contentId':contentId,
                                'eventId':eventId,
                                'subscriptionId':subscriptionId
                            };
                            var signUpBtn=button;
                            blockEventList_request_addAndGetSignUpFormViewForEventSignUpBtn(
                                signUpBtn,postData,function(signUpForm){
                                    if(signUpForm&&signUpForm.length)
                                    {
                                        signUpForm.slideDown();
                                    }
                                },false
                            );
                        }
                        else if(action=='delete')
                        {
                            var postData={
                                'subscriptionId':subscriptionId,
                                'eventId':eventId,
                                'contentId':contentId
                            };
                            request_getUserSignedUpForEventDeletionConfirmationView(postData,function(response){
                                if(requestOkAndSessionExist(response))
                                {
                                    var dialog=$('#'+id);
                                    var viewHtml=response.data;
                                    var contents=viewHtml;
                                    var title='Potwierdź usunięcie użytkownika';
                                    var customId='dialog-confirm-user-removal';
                                    jConfirm(contents,title,customId,function(id,confirmed){
                                        if(confirmed)
                                        {
                                            var confirmDialog=$('#'+id);
                                            var postData={
                                                'subscriptionId':subscriptionId,
                                                'eventId':eventId,
                                                'contentId':contentId
                                            };
                                            request_deleteUserSignedUpForEvent(postData,function(response){
                                                if(requestOkAndSessionExist(response))
                                                {
                                                    if(response.meta.customStatus=='USER_DELETED')
                                                    {
                                                        var activeAnchor=dialog.find('.pagination li.active a');
                                                        var currentPageNumber=toInt($.trim(activeAnchor.text()));
                                                        var itemCountPerPage=$('#pages-icpp-dropdown option:selected').val();
                                                        var postData={
                                                            'pageNumber':currentPageNumber,
                                                            'itemCountPerPage':itemCountPerPage,
                                                            'additionalData':{
                                                                'eventId':eventId,
                                                                'contentId':contentId
                                                            }
                                                        };
                                                        request_getListOfUsersSignedUpForEventView(function(response){
                                                            if(requestOkAndSessionExist(response))
                                                            {
                                                                dialog.find('.modal-content .modal-body').html(
                                                                    response.data
                                                                );
                                                            }
                                                        },postData);
                                                        request_getEventListView(function(response){
                                                            if(requestOkAndSessionExist(response))
                                                            {
                                                                var dialog=$('#dialog-event-list');
                                                                dialog.find('.modal-content .modal-body').html(
                                                                    response.data
                                                                );
                                                            }
                                                        });
                                                    }
                                                }
                                                confirmDialog.modal('hide');
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    };
                    attachManagementFunctionsToManagementWnd(
                        '#'+id,viewRequestGetFunction,viewGetCallback,togglePublishingCallback,addNewCallback,
                        actionCallback
                    );
                }
            },withSaveButton);
        }
    },postData);
}
$(document).ready(function(){
    var body=$('body');
    var adminSlots=$('.admin-slot');
    var adminTopSidePanel=$('#admin-top-side-panel');
    var adminToolWnd=$('#admin-toolwnd');
    inDesignMode=body.hasClass('design-mode');

    body.on('click','#admin-edit-this-page',function(){
        var pageId=getCurrentPageId();
        var data={
            'pageId':pageId
        };
        showAddEditWindow('edit','Edytuj bieżącą stronę','Zapisz','Anuluj',data);

        return false;
    });
    body.on('click','#admin-show-language-switcher',function(){
        body.find('#admin-language-switcher').toggleClass('displayed-as-block'); // TODO: CSS transition.

        return false;
    });
    body.on('click','#admin-language-switcher a[data-iso-code]',function(){
        var url=location.href;
        var controller=getControllerFromUrl(url);
        var isoCode=$(this).attr('data-iso-code');
        if(controller=='page')
        {
            var parentPageCleanUrl=getParentPageCleanUrlFromUrl(url);
            var subPageCleanUrl=getSubPageCleanUrlFromUrl(url);
            var parameters=getPageParametersStringFromUrl(url);
            var postData={
                'languageIsoCode':isoCode,
                'parentPageCleanUrl':parentPageCleanUrl,
                'subPageCleanUrl':subPageCleanUrl,
                'parameters':parameters
            }
            makePostRequest(makeCmsUrl('language/switch-page-language'),function(response){
                    if(requestOkAndSessionExist(response))
                    {
                        var newUrl=response.data;
                        if(newUrl!='')
                        {
                            redirect(newUrl);
                        }
                    }
                },postData
            );
        }
        else
        {
            var newUrl=changeLanguageInUrl(url,isoCode);
            if(newUrl!='')
            {
                redirect(newUrl);
            }
        }

        return false;
    });
    body.on('click','#admin-toggle-design-mode',function(){
        makePostRequest(makeCmsUrl('admin/toggle-design-mode'),function(response){
            if(requestOkAndSessionExist(response))
            {
                var status=response.meta.customStatus;
                if(status=='DESIGN_MODE_ENABLED')
                {
                    refreshPage();
                }
                else if(status=='DESIGN_MODE_DISABLED')
                {
                    refreshPage();
                }
            }
        });

        return false;
    });

    /**
     * User interface language switcher.
     */

    body.on('click','#btn-show-user-language-switcher',function(){
        body.find('#user-language-switcher').toggleClass('displayed-as-block'); // TODO: CSS transition.

        return false;
    });
    body.on('click','#user-language-switcher a[data-iso-code]',function(){
        var url=location.href;
        var isoCode=$(this).attr('data-iso-code');
        var postData={
            'languageIsoCode':isoCode
        }
        makePostRequest(makeCmsUrl('language/switch-user-language'),function(response){
                if(requestOkAndSessionExist(response))
                {
                    refreshPage();
                }
            },postData
        );

        return false;
    });

    var href=$('#admin-btn-logout').prop('href');
    $('#btn-logout').prop('href',href);

    if(inDesignMode)
    {
        adminSlots.droppable({
            //hoverClass:'slot-highlighted',
            //activeClass: "ui-state-active",
            //hoverClass: "ui-state-hover",
            accept:'.block-bar',
            tolerance:'pointer',
            // Do not use "drop" event, because it will be called twice, and even
            // before the user drops the element.
            drop:function(event,ui){
                //addCmsMessageToConsole('jQuery UI droppable\'s drop() event fired.');
                lastInsertedBlockTemporaryDomTreeNode=ui.draggable;
            }
        }).sortable({
            connectWith:'.admin-slot',
            forcePlaceholderSize:true,
            tolerance:'pointer',
            distance:0.5,
            items:'.slot-item',
            handle:'.block-frame .title-bar',
            cancel:'.block-frame .title-bar .buttons',
            start:function(event,ui){
                //addCmsMessageToConsole('jQuery UI sortable\'s start() event called.');
                var slotId=ui.item.closest('.admin-slot').attr('data-slot-id');
                blockIsCurrentlyBeingSorted=true;
                currentlyBeingSortedBlock=ui.item;
                currentlyBeingSortedBlockSourceSlotId=slotId;
                $('.admin-slot').addClass('presented');
            },
            sort:function(event,ui){
                //addCmsMessageToConsole('jQuery UI sortable\'s sort() event called.');
                // gets added unintentionally by droppable interacting with sortable
                // using connectWithSortable fixes this, but doesn't allow you to customize active/hoverClass options
                $(this).removeClass('ui-state-default');
            },
            stop:function(event,ui){
                //addCmsMessageToConsole('jQuery UI sortable\'s stop() event called.');

                var draggedElement=$(ui.item[0]);

                // If block is not currently being inserted then it indicates that
                // it is only being sorted.
                if(!draggedElement.data('drag-canceled')&&!blockIsCurrentlyBeingInserted)
                {
                    var pageBlockId=currentlyBeingSortedBlock.attr('data-page-block-id');
                    var sourceSlotId=currentlyBeingSortedBlockSourceSlotId;
                    var destinationSlotId=ui.item.closest('.admin-slot').attr('data-slot-id');

                    if(sourceSlotId==destinationSlotId)
                    {
                        reorderBlocksInSpecificSlot(sourceSlotId);
                    }
                    else
                    {
                        moveBlockToAnotherSlot(pageBlockId,sourceSlotId,destinationSlotId);
                    }
                }

                blockIsCurrentlyBeingSorted=false;
                currentlyBeingSortedBlock=null;
                $('.admin-slot').removeClass('presented');
            },
            receive:function(event,ui){
                //addCmsMessageToConsole('jQuery UI sortable\'s receive() event called.');

                var draggedElement=$(ui.item[0]);
                if(draggedElement.data('drag-canceled'))
                {
                    return false;
                }

                var isNewBlock=draggedElement.hasClass('ui-draggable');
                if(isNewBlock)
                {
                    var pageId=getCurrentPageId();
                    var slotId=$(this).attr('data-slot-id');
                    var blockId=draggedElement.attr('data-block-id');

                    insertBlockIntoPage(pageId,slotId,blockId);
                }
            }
        });

        /**
         * Adjust block frame width (100% + 20px) for better visual effect.
         * Additionally set block-content background to .admin-slot background color
         * and change block-content z-index from 1 to 2 because without it you
         * cannot see .block-content contents.
         */
        body
            .on('mouseover','.admin-slot .slot-item',function(){
                if(blockIsCurrentlyBeingSorted||blockIsCurrentlyBeingInserted)
                {
                    return false;
                }

                var element=$(this);
                var blockFrame=element.find('.block-frame');
                var blockContent=element.find('.block-content');
                var width=blockContent.width();
                //var parentBackground=element.inheritedBackground(); // TODO: Rewrite previous function.
                //console.log(parentBackground);
                blockFrame.width(width+20);
                //blockFrame.css('background',parentBackground);

                // If block top position is very small then place title bar at bottom of the block instead:
                var topPosition=blockFrame.offset().top-$(window).scrollTop();
                var titleBar=blockFrame.find('.title-bar');
                if (topPosition<=0)
                {
                    blockFrame.css('top',0);
                    titleBar.css({
                        'position':'absolute',
                        'bottom':0,
                        'width':'100%'
                    });
                }
                else
                {
                    blockFrame.css('top','-2.4em');
                    titleBar.css({
                        'position':'inherit',
                        'bottom':'auto',
                        'width':'inherit'
                    });
                }

                blockContent.css('z-index',2);
            })
            .on('mouseleave','.admin-slot .slot-item',function(){
                if(blockIsCurrentlyBeingSorted||blockIsCurrentlyBeingInserted)
                {
                    return false;
                }

                var blockContent=$(this).find('.block-content');
                blockContent.css('z-index','auto');
            });

        /**
         * Block title bar buttons actions.
         */
        attachEventToPageBlockSaveAsDynamicBlockButton($('.admin-slot .slot-item .block-frame .title-bar .buttons .save-as-dynamic-block'));
        attachEventToPageBlockTogglePublishingButton($('.admin-slot .slot-item .block-frame .title-bar .buttons .toggle-publishing'));
        attachEventToPageBlockSettingsButton($('.admin-slot .slot-item .block-frame .title-bar .buttons .settings'));
        attachEventToPageBlockDeleteButton($('.admin-slot .slot-item .block-frame .title-bar .buttons .delete'));

        /**
         * Block content actions.
         */
        attachEventsToPageBlockTitle($('.admin-slot .slot-item > .block-content'));


        /**
         * Admin tool window.
         */
        body.on('mouseenter','#admin-blocks-tab-activator',function(){
            switchBlockTabInAdminToolWnd(1);
        });
        body.on('mouseenter','#admin-saved-blocks-tab-activator',function(){
            switchBlockTabInAdminToolWnd(2);
        });
        adminToolWnd
            .on('mouseenter',function(){
                isHidingAdminToolWndAllowedRightNow=true;
            })
            .on('mouseleave',function(){
                if(isHidingAdminToolWndAllowedRightNow)
                {
                    hideAdminToolWnd();
                }
            })
            .on('mouseenter','#block-list-languages,#block-list-categories,#block-list-search-field',function(){
                isHidingAdminToolWndAllowedRightNow=false;
            })
            .on('click change','#block-list-languages,#block-list-categories',function(){
                var areDynamicBlocksDisplayed=
                    toInt($(this).closest('.tabs-contents-container').find('.inner:visible').attr('data-dynamic-blocks'));
                if(areDynamicBlocksDisplayed)
                {
                    refreshDynamicBlockListInAdminToolWnd();
                }
                else
                {
                    refreshBlockListInAdminToolWnd();
                }
            })
            .on('keyup','#block-list-search-field',function(e){
                if(e.keyCode==13)
                {
                    var areDynamicBlocksDisplayed=
                        toInt($(this).closest('.tabs-contents-container').find('.inner:visible').attr('data-dynamic-blocks'));
                    if(areDynamicBlocksDisplayed)
                    {
                        refreshDynamicBlockListInAdminToolWnd();
                    }
                    else
                    {
                        refreshBlockListInAdminToolWnd();
                    }
                }
            });
        attachEventsToAdminToolWindowInnerContainer(1);
        attachEventsToAdminToolWindowInnerContainer(2);
    }

    /**
     * Admin top side panel.
     */
    adminTopSidePanel.on('click','.btn-pages',function(){
        request_getPagesView(function(response){
            if(requestOkAndSessionExist(response))
            {
                var contents=response.data;
                var title='Strony';
                var withSaveButton=false;
                jDialog(contents,title,'dialog-pages',function(id,success){
                    if(success)
                    {
                        var viewRequestGetFunction=request_getPagesView;
                        var viewGetCallback=null;

                        var dialog=$('#'+id);

                        dialog.on('keypress','#pages-fast-search',function(e){
                            if(e.which==13)
                            {
                                filterItemsInDialogWindowUsingFastSearch(
                                    dialog,viewRequestGetFunction,viewGetCallback
                                );
                            }
                        });
                        dialog.on('click','#pages-btn-fast-search',function(){
                            filterItemsInDialogWindowUsingFastSearch(
                                dialog,viewRequestGetFunction,viewGetCallback
                            );
                        });

                        dialog.find('#pages-fast-search').focus();

                        var togglePublishingCallback=function(published,elementId,button)
                        {
                            var pageId=elementId;
                            request_togglePagePublishing(pageId,published,function(response){
                                if(requestOkAndSessionExist(response))
                                {
                                    button.prop('checked',published?true:false);
                                }
                            });
                        };
                        var addNewCallback=function(button){
                            showAddEditWindow('add','Dodaj nową stronę','Zapisz','Anuluj');
                        };
                        var actionCallback=function(elementId,action,button){
                            var pageId=elementId;
                            var data={
                                'pageId':pageId
                            };
                            if(action=='edit')
                            {
                                showAddEditWindow('edit','Edytuj stronę','Zapisz','Anuluj',data);
                            }
                            else if(action=='delete')
                            {
                                var postData={
                                    'pageId':pageId
                                };
                                request_getPageDeletionConfirmationView(postData,function(response){
                                    if(requestOkAndSessionExist(response))
                                    {
                                        var dialog=$('#'+id);
                                        var viewHtml=response.data;
                                        var contents=viewHtml;
                                        var title='Potwierdź usunięcie strony';
                                        var customId='dialog-confirm-page-removal';
                                        jConfirm(contents,title,customId,function(id,confirmed){
                                            if(confirmed)
                                            {
                                                var confirmDialog=$('#'+id);
                                                var actionToTakOnMenus=
                                                    confirmDialog
                                                        .find('#action-to-take-on-menus option:selected')
                                                        .val();
                                                var postData={
                                                    'pageId':pageId,
                                                    'actionToTakOnMenus':actionToTakOnMenus
                                                };
                                                request_deletePage(postData,function(response){
                                                    if(requestOkAndSessionExist(response))
                                                    {
                                                        if(response.meta.customStatus=='PAGE_DELETED')
                                                        {
                                                            var activeAnchor=dialog.find('.pagination li.active a');
                                                            var currentPageNumber=toInt($.trim(activeAnchor.text()));
                                                            var itemCountPerPage=$('#pages-icpp-dropdown option:selected').val();
                                                            var includePagesFrom=$('#pages-ipf-dropdown option:selected').val();
                                                            request_getPagesView(function(response){
                                                                if(requestOkAndSessionExist(response))
                                                                {
                                                                    dialog.find('.modal-content .modal-body').html(response.data);
                                                                    renderBlockAgainByBlockName('main-menu');
                                                                    renderBlockAgainByBlockName('navigation-menu');
                                                                    renderBlockAgainByBlockName('shortcut-menu');
                                                                }
                                                            },{
                                                                'pageNumber':currentPageNumber,
                                                                'itemCountPerPage':itemCountPerPage,
                                                                'includePagesFrom':includePagesFrom
                                                            });
                                                        }
                                                    }
                                                    confirmDialog.modal('hide');
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        };
                        attachManagementFunctionsToManagementWnd('#'+id,viewRequestGetFunction,viewGetCallback,togglePublishingCallback,addNewCallback,actionCallback);
                    }
                },withSaveButton);
            }
        });

        return false;
    });
    adminTopSidePanel.on('click','.btn-search-for-blocks',function(){
        request_getBlocksView(function(response){
            if(requestOkAndSessionExist(response))
            {
                var contents=response.data;
                var title='Wyszukiwanie bloków';
                var withSaveButton=false;
                jDialog(contents,title,'dialog-search-for-blocks',function(id,success){
                    if(success)
                    {
                        var viewRequestGetFunction=request_getBlocksView;
                        var viewGetCallback=null;

                        var dialog=$('#'+id);

                        dialog.on('keypress','#search-for-blocks-page-query',function(e){
                            if(e.which==13)
                            {
                                filterItemsInBlocksDialogWindow(
                                    dialog,viewRequestGetFunction,viewGetCallback
                                );
                            }
                        });
                        dialog.on('keypress','#search-for-blocks-block-query',function(e){
                            if(e.which==13)
                            {
                                filterItemsInBlocksDialogWindow(
                                    dialog,viewRequestGetFunction,viewGetCallback
                                );
                            }
                        });
                        dialog.on('click','#search-for-blocks-btn-search',function(){
                            filterItemsInBlocksDialogWindow(
                                dialog,viewRequestGetFunction,viewGetCallback
                            );
                        });
                        dialog.find('#search-for-blocks-block-query').focus();

                        dialog.on('click','.btn-show-hide-page-list',function(){
                            var listBlockId=$(this).attr('data-list-block-id');
                            dialog.find('.page-list[data-list-block-id="'+listBlockId+'"]').toggle();
                        });

                        var togglePublishingCallback=false;
                        var addNewCallback=false;
                        var actionCallback=false;
                        attachManagementFunctionsToManagementWnd(
                            '#'+id,viewRequestGetFunction,viewGetCallback,togglePublishingCallback,
                            addNewCallback,actionCallback
                        );
                    }
                },withSaveButton);
            }
        });

        return false;
    });
    adminTopSidePanel.on('click','.btn-show-event-list',function(){
        request_getEventListView(function(response){
            if(requestOkAndSessionExist(response))
            {
                var contents=response.data;
                var title='Lista wydarzeń z możliwością zapisu';
                var withSaveButton=false;
                jDialog(contents,title,'dialog-event-list',function(id,success){
                    if(success)
                    {
                        var viewRequestGetFunction=request_getEventListView;
                        var viewGetCallback=null;

                        var dialog=$('#'+id);

                        dialog.on('keypress','#event-list-fast-search',function(e){
                            if(e.which==13)
                            {
                                filterItemsInDialogWindowUsingFastSearch(
                                    dialog,viewRequestGetFunction,viewGetCallback
                                );
                            }
                        });
                        dialog.on('click','#event-list-btn-fast-search',function(){
                            filterItemsInDialogWindowUsingFastSearch(
                                dialog,viewRequestGetFunction,viewGetCallback
                            );
                        });

                        dialog.find('#event-list-fast-search').focus();

                        var togglePublishingCallback=false;
                        var addNewCallback=false;
                        var actionCallback=function(elementId,action,button){
                            var eventId=elementId;
                            var contentId=button.closest('tr').attr('data-content-id');
                            if(action=='show-user-list')
                            {
                                showDialogWindowWithListOfUsersSignedUpForEvent(eventId,contentId);
                            }
                        };
                        attachManagementFunctionsToManagementWnd('#'+id,viewRequestGetFunction,viewGetCallback,togglePublishingCallback,addNewCallback,actionCallback);
                    }
                },withSaveButton);
            }
        });

        return false;
    });
    adminTopSidePanel.on('click','.btn-users',function(){
        request_getUsersView(function(response){
            if(requestOkAndSessionExist(response))
            {
                var contents=response.data;
                var title='Użytkownicy';
                var withSaveButton=false;
                jDialog(contents,title,'dialog-users',function(id,success){
                    if(success)
                    {
                        var viewRequestGetFunction=request_getUsersView;
                        var viewGetCallback=null;

                        var dialog=$('#'+id);

                        dialog.on('keypress','#users-fast-search',function(e){
                            if(e.which==13)
                            {
                                filterItemsInDialogWindowUsingFastSearch(
                                    dialog,viewRequestGetFunction,viewGetCallback
                                );
                            }
                        });
                        dialog.on('click','#users-btn-fast-search',function(){
                            filterItemsInDialogWindowUsingFastSearch(
                                dialog,viewRequestGetFunction,viewGetCallback
                            );
                        });

                        dialog.find('#users-fast-search').focus();

                        var toggleActivityCallback=function(active,elementId,button)
                        {
                            var userId=elementId;
                            request_toggleUserAccountActivity(userId,active,function(response){
                                if(requestOkAndSessionExist(response))
                                {
                                    button.prop('checked',active?true:false);
                                }
                            });
                        };
                        var addNewCallback=function(button){
                            showUserAddEditWindow('add','Dodaj nowego użytkownika','Zapisz','Anuluj');
                        };
                        var actionCallback=function(elementId,action,button){
                            var userId=elementId;
                            var data={
                                'userId':userId
                            };
                            if(action=='edit')
                            {
                                showUserAddEditWindow('edit','Edytuj użytkownika','Zapisz','Anuluj',data);
                            }
                            else if(action=='delete')
                            {
                                jConfirm('Czy na pewno chcesz usunąć tego użytkownika?','Potwierdź usunięcie użytkownika','dialog-confirm-user-removal',function(id,confirmed){
                                    if(confirmed)
                                    {
                                        request_deleteUser(userId,function(response){
                                            if(requestOkAndSessionExist(response))
                                            {
                                                if(response.meta.customStatus=='USER_DELETED')
                                                {
                                                    var listingsWindow=$('#dialog-users');
                                                    var activeAnchor=listingsWindow.find('.pagination li.active a');
                                                    var currentPageNumber=toInt($.trim(activeAnchor.text()));
                                                    var itemCountPerPage=$('#users-icpp-dropdown option:selected').val();
                                                    request_getUsersView(function(response){
                                                        if(requestOkAndSessionExist(response))
                                                        {
                                                            listingsWindow.find('.modal-content .modal-body').html(response.data);
                                                        }
                                                    },{
                                                        'pageNumber':currentPageNumber,
                                                        'itemCountPerPage':itemCountPerPage
                                                    });
                                                }
                                            }
                                            var dialog=$('#'+id);
                                            dialog.modal('hide');
                                        });
                                    }
                                });
                            }
                        };
                        attachManagementFunctionsToManagementWnd('#'+id,viewRequestGetFunction,viewGetCallback,toggleActivityCallback,addNewCallback,actionCallback);
                    }
                },withSaveButton);
            }
        });

        return false;
    });
    adminTopSidePanel.on('click','.btn-settings',function(){
        request_getSettingsView(function(response){
            if(requestOkAndSessionExist(response))
            {
                var contents=response.data;
                var title='Ustawienia';
                var withSaveButton=true;
                jDialog(contents,title,'dialog-settings',function(id,success){
                    if(success)
                    {
                        var dialog=$('#'+id);

                        dialog.on('click','.btn-save',function(){
                            var form=dialog.find('.form');
                            var formData=getFormData(form);
                            var postData={
                                'dataPosted':true,
                                'formData':formData
                            };

                            request_saveSettings(postData,function(response){
                                if(requestOkAndSessionExist(response))
                                {
                                    if(response.meta.customStatus=='SETTINGS_SAVED')
                                    {
                                        dialog.modal('hide');
                                    }
                                }
                            });
                        });
                    }
                },withSaveButton);
            }
        });

        return false;
    });

    /**
     * Top-search area.
     */
    var searchDestinationFullPageCleanUrl='wyniki-wyszukiwania';
    body.on('keypress','#top-search input[type="text"]',function(e){
        if(e.which==13)
        {
            searchUsingTopSearchArea(searchDestinationFullPageCleanUrl);
        }
    });
    body.on('click','#top-search a',function(e){
        searchUsingTopSearchArea(searchDestinationFullPageCleanUrl);

        return false;
    });

    /**
     * CMS keyboard events support.
     */
    $(document).keyup(function(event){
        if(event.which==27) // On "Esc".
        {
            escKeyPressedWhileDragging=true;
            //$('.ui-draggable-dragging').remove();
        }
    });
});
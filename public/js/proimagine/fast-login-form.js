function fastLoginForm_request_login(postData,doneCallback)
{
    makePostRequest('/auth/index/fast-login',doneCallback,postData);
}
function fastLoginForm_login(fastLoginForm)
{
    var form=fastLoginForm.find('.form');
    var formData=getFormData(form);
    var postData={
        'formData':formData,
        'isDesignModeEnabled':isDesignModeEnabled()
    }
    fastLoginForm_request_login(postData,function(response){
        if(response.meta.requestOk)
        {
            var userInfo=response.data.userInfo;
            if (userInfo)
            {
                fastLoginForm.remove();
                $('#user-info').attr('data-content',userInfo.login);
                var title='Szybkie logowanie.';
                var contents=
                    '<p>Logowanie przebiegło pomyślnie.</p>'+
                    '<p>Twoja sesja została odtworzona.</p>'+
                    '<p>Możesz teraz kontynuować pracę bez konieczności odświeżania strony.</p>';
                var customId='dialog-fast-login-success-message';
                jDialog(contents,title,customId);
            }
            else
            {
                var message=response.data.message;
                fastLoginForm.find('.message').html(message);
            }
        }
    });
}
function fastLoginForm_handleCustomFunctionality()
{
    var fastLoginForm=$('#fast-login-form');
    fastLoginForm.on('keypress','input',function(e){
        if(e.which==13)// On ENTER key press.
        {
            fastLoginForm_login(fastLoginForm);
        }
    });
    fastLoginForm.on('click','button.login',function(){
        fastLoginForm_login(fastLoginForm);
    });
}
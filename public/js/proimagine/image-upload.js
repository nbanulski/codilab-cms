function imageUpload_handleSection(
    pageBlockId,blockElementId,imageArrayName,aspectRatio,parametersForPlUploadLibrary,onBeforeDeletedCallback,
    uniqueSectionIdentifier
)
{
    var selector='.image-upload[data-page-block-id="'+pageBlockId+'"]';
    if(blockElementId>0)
    {
        selector+='[data-block-element-id="'+blockElementId+'"]';
    }
    var section=$(selector);

    // Refresh PlUpload (required for some browsers) instance
    // when image upload section was made visible at the first time:
    var checkVisible=setInterval(function(){
        var isSectionVisible=section.is(':visible');
        var uploader=section.data('uploader-instance');
        if(isSectionVisible&&uploader)
        {
            try
            {
                uploader.refresh();
                section.removeAttr('data-uploader-instance');
                clearInterval(checkVisible);
            }
            catch(e)
            {

            }
        }
    },1000);

    var atLeastOneImageIsSetAsMain=false;
    section.find('[data-image-number]').each(function(){
        var div=$(this);
        var imageNumber=div.attr('data-image-number');
        var uniqueIdentifier=div.attr('data-unique-identifier');

        if(!atLeastOneImageIsSetAsMain)
        {
            var imageIsMainRadio=div.find('#iu-image-is-main-'+imageNumber);
            var imageIsMain=imageIsMainRadio.prop('checked')||imageIsMainRadio.attr('checked');
            if(imageIsMain)
            {
                atLeastOneImageIsSetAsMain=true;
            }
        }

        imageUpload_attachEventToImageEditButton(section,pageBlockId,aspectRatio,imageNumber,uniqueIdentifier);
        imageUpload_attachUploadLibraryToSection(
            section,pageBlockId,parametersForPlUploadLibrary,imageNumber,uniqueIdentifier
        );
        imageUpload_attachEventToImageDeleteButton(
            section,pageBlockId,blockElementId,imageArrayName,aspectRatio,parametersForPlUploadLibrary,
            onBeforeDeletedCallback,imageNumber,uniqueSectionIdentifier,uniqueIdentifier
        );
    });

    if(!atLeastOneImageIsSetAsMain)
    {
        section.find('#iu-image-is-main-0').prop('checked',true).attr('checked',true);
    }

    section.on('click','.iu-image-is-main',function(){
        var radio=$(this);
        var id=radio.attr('id');

        // Warning - both "prop()" and "attr()" methods must be used for jQuery Sortable!
        section.find('[id!='+id+']').prop('checked',false).removeAttr('checked');
        radio.prop('checked',true).attr('checked',true);

        var imageSectionContainer=radio.closest('[data-image-number]');
        var imageSectionTpl=section.find('#iu-image-section-tpl');
        imageSectionContainer.insertAfter(imageSectionTpl);
    });

    section.on('click','button.add-new-image',function(){
        imageUpload_addNewImage(
            section,pageBlockId,blockElementId,imageArrayName,aspectRatio,parametersForPlUploadLibrary,
            onBeforeDeletedCallback,uniqueSectionIdentifier
        );
    });

    imageUpload_attachSortableToSection(section);
}
function imageUpload_attachUploadLibraryToSection(section,pageBlockId,givenParameters,imageNumber,uniqueIdentifier)
{
    if(typeof imageNumber=='undefined')
    {
        return false;
    }
    var defaultParameters={
        'moduleName':'site',
        'blockName':'',
        'pageBlockId':pageBlockId,
        'resizeImageToMaxWidth':false,
        'resizeImageToMaxHeight':false,
        'crop':false,
        'cropMaskCoords':''
    }
    var parameters=$.extend({},defaultParameters,givenParameters);
    var url='/eng/upload';
    var uploader=new plupload.Uploader({
        'runtimes':'html5,flash,silverlight,html4',
        'browse_button':'iu-select-file-to-upload-btn-'+uniqueIdentifier,
        'container':document.getElementById('iu-files-to-upload-container-'+uniqueIdentifier),
        'url':url,
        'multipart_params':parameters,
        'filters':{
            max_file_size:'10mb',
            mime_types:[
                {title:"Image files",extensions:"jpeg,jpg,gif,png"}
            ]
        },
        'flash_swf_url':'http://rawgithub.com/moxiecode/moxie/master/bin/flash/Moxie.cdn.swf',
        'silverlight_xap_url':'http://rawgithub.com/moxiecode/moxie/master/bin/silverlight/Moxie.cdn.xap',
        'init':{
            PostInit:function(){
                document.getElementById('iu-uploaded-file-list-'+uniqueIdentifier).innerHTML='';
                document.getElementById('iu-upload-selected-file-'+uniqueIdentifier).onclick=function(){
                    uploader.start();

                    return false;
                };
            },
            FileFiltered:function(){
                // Remove previously added files because event can have only
                // one main image file at the same time:
                section.find('#iu-uploaded-file-list-'+uniqueIdentifier+' > div').remove();
            },
            FilesAdded:function(up,files){
                plupload.each(files,function(file){
                    document.getElementById('iu-uploaded-file-list-'+uniqueIdentifier).innerHTML+=
                        '<div id="'+file.id+'">'+plupload.formatSize(file.size)+'</div>';
                });
                section.find('#iu-upload-selected-file-'+uniqueIdentifier).show();
            },
            UploadProgress:function(up,file){
                var fileEntry=document.getElementById(file.id);
                if(fileEntry&&fileEntry.length>0)
                {
                    fileEntry.getElementsByTagName('b')[0].innerHTML='<span>'+file.percent+"%</span>";
                }
            },
            FileUploaded:function(up,file,responseObject){
                var doNotRequireSession=true;
                var responseString=responseObject.response;
                var response=processAjaxResponse(url,responseString,doNotRequireSession);
                if(requestOk(response))
                {
                    var uniqueHash=response.data[0];
                    var thumbnailUrl=makeCmsUrl('get-file/index/hash,'+uniqueHash+','+file.name);

                    var eventImageThumb=section.find('#iu-image-thumbnail-'+uniqueIdentifier);
                    eventImageThumb.prop('src',thumbnailUrl);
                    eventImageThumb.removeAttr('width');
                    eventImageThumb.removeAttr('height');
                    eventImageThumb.css({'maxWidth':'150px','maxHeight':'150px'});

                    section.find('#iu-unique-hash-for-file-name-'+uniqueIdentifier).val(uniqueHash);
                    section.find('#iu-image-btn-edit-'+uniqueIdentifier).prop('disabled',false);
                    section.find('#iu-upload-selected-file-'+uniqueIdentifier).hide();
                    section.find('#iu-delete-file-'+uniqueIdentifier).show();
                    section.find('#iu-uploaded-file-list-'+uniqueIdentifier+' > div').remove();
                }
            },
            Error:function(up,err){
                document.getElementById('iu-file-upload-console').innerHTML+="\nError #"+err.code+": "+err.message;
                $('#iu-upload-console-messages-section').show();
            }
        }
    });

    uploader.init();

    section.data('uploader-instance',uploader);
}
function imageUpload_attachEventToImageEditButton(section,pageBlockId,aspectRatio,imageNumber,uniqueIdentifier)
{
    var section=$(section);
    if((section.length<1)||(typeof imageNumber=='undefined'))
    {
        return false;
    }

    var n=uniqueIdentifier;

    var thisImageSection=section.find('[data-image-number="'+imageNumber+'"]');
    var uniqueHashForFileName=
        $.trim(thisImageSection.find('#iu-unique-hash-for-file-name-'+n).val());
    if(uniqueHashForFileName=='')
    {
        thisImageSection.find('#iu-image-btn-edit-'+n).prop('disabled',true);
    }

    section
        .off('click','#iu-image-btn-edit-'+n)
        .on('click','#iu-image-btn-edit-'+n,function(){
        var uniqueHashForFileNameInput=section.find('#iu-unique-hash-for-file-name-'+n);
        var uniqueHashForFileName=uniqueHashForFileNameInput.val();
        var postData={
            'pageBlockId':pageBlockId,
            'uniqueHashForFileName':uniqueHashForFileName
        };
        request_getBlockImageEditView(function(response){
            if(requestOkAndSessionExist(response))
            {
                var contents=response.data;
                var withSaveButton=true;
                jDialog(contents,'Edycja obrazka','dialog-image-edit',function(id,success){
                    if(success)
                    {
                        var imageEditDialog=$('#'+id);
                        attachSupportForBlockImageEditView(imageEditDialog,function(success,attachmentInfo){
                            if(success)
                            {
                                var eventImageThumb=section.find('#iu-image-thumbnail-'+n);
                                eventImageThumb.prop('src',attachmentInfo.imageUrl);
                                uniqueHashForFileNameInput.val(attachmentInfo.uniqueHashForFileName);
                            }
                        },aspectRatio);
                    }
                },withSaveButton);
            }
        },postData);
    });
}
function imageUpload_attachEventToImageDeleteButton(
    section,pageBlockId,blockElementId,imageArrayName,aspectRatio,parametersForPlUploadLibrary,onBeforeDeletedCallback,
    imageNumber,uniqueSectionIdentifier,uniqueIdentifier
)
{
    var section=$(section);
    if((section.length<1)||(typeof imageNumber=='undefined'))
    {
        return false;
    }

    var n=uniqueIdentifier;

    section
        .off('click','#iu-delete-file-'+n)
        .on('click','#iu-delete-file-'+n,function(){
        var imageSection=$(this).closest('[data-image-number]');
        var imageId=imageSection.find('#iu-image-id-'+n).val();
        var imageUrl=imageSection.find('#iu-image-thumbnail-'+n).attr('src');
        var newImageTag='<img style="max-width:96px;max-height:96px;margin: 0 auto" src="'+imageUrl+'" />';
        var contents='<p>Czy na pewno chcesz usunąć poniższy obrazek?</p>'+newImageTag;
        var title='Usuwanie obrazka';
        var customId=customId;
        jConfirm(contents,title,customId,function(id,confirmed){
            if(confirmed)
            {
                var canContinueRemoving=true;

                var info=
                    (onBeforeDeletedCallback!='')
                    ?
                        callFunctionIfExists(onBeforeDeletedCallback,{
                            'pageBlockId':pageBlockId,
                            'blockElementId':blockElementId,
                            'imageId':imageId
                        })
                    :
                        false
                ;

                if(info&&info.wasFunctionCalled)
                {
                    canContinueRemoving=info.valueReturnedByFunction;
                }

                if(canContinueRemoving)
                {
                    imageSection.slideUp(400,function(){
                        var imagesContainer=imageSection.parent();
                        imageSection.remove();

                        // Set the first image as main if no main image exists:
                        var radioIsMain=imagesContainer.find('input[type="radio"].iu-image-is-main:checked');
                        if(!radioIsMain||!radioIsMain.length)
                        {
                            imageSection=imagesContainer.find('[data-image-number]:first');
                            if(imageSection)
                            {
                                imageSection
                                    .find('input[type="radio"].iu-image-is-main')
                                    .prop('checked',true)
                                    .attr('checked','checked')
                                ;
                            }
                        }

                        var imageCount=section.find('[data-image-number]').length;
                        if(!imageCount)
                        {
                            imageUpload_addNewImage(
                                section,pageBlockId,blockElementId,imageArrayName,aspectRatio,parametersForPlUploadLibrary,
                                onBeforeDeletedCallback,uniqueSectionIdentifier
                            );
                        }
                    });

                    var attachmentId=imageSection.find('#iu-attachment-id-'+n).val();
                    var uniqueHashForFileName=
                        imageSection.find('#iu-unique-hash-for-file-name-'+n).val();
                    section
                        .find('.attachments-to-delete')
                        .append('<input type="hidden" name="attachmentsToDelete['+attachmentId+']" value="'+uniqueHashForFileName+'" />');


                }
            }
        });
    });

    return true;
}
function imageUpload_addNewImage(
    section,pageBlockId,blockElementId,imageArrayName,aspectRatio,parametersForPlUploadLibrary,onBeforeDeletedCallback,
    uniqueSectionIdentifier
)
{
    var section=$(section);

    // Get maximum image number:
    var maximumImageNumber=0;
    section.find('[data-image-number]').each(function(i,n){
        var number=toInt($(n).attr('data-image-number'));
        if (number>maximumImageNumber)
        {
            maximumImageNumber=number;
        }
    });

    var imageSection=section.find('#iu-image-section-tpl');

    // Create new image section:
    var newImageNumber=maximumImageNumber+1;
    var newImageSection=imageSection.clone(false).removeAttr('id');

    // Modify the section's number in every place:
    var uniqueIdentifier=uniqueSectionIdentifier+'-'+newImageNumber;
    var n=uniqueIdentifier;
    newImageSection
        .attr('data-image-number',newImageNumber)
        .attr('data-unique-identifier',n);
    var imageThumbnailTag=newImageSection.find('#iu-image-thumbnail-tpl');
    imageThumbnailTag.attr('id','iu-image-thumbnail-'+n);
    newImageSection.find('#iu-image-btn-edit-tpl').attr('id','iu-image-btn-edit-'+n);
    newImageSection.find('#iu-select-file-to-upload-btn-tpl').attr('id','iu-select-file-to-upload-btn-'+n);
    newImageSection.find('#iu-upload-selected-file-tpl').attr('id','iu-upload-selected-file-'+n).hide();
    newImageSection.find('#iu-delete-file-tpl').attr('id','iu-delete-file-'+n).hide();
    var imageIsMainInput=newImageSection.find('#iu-image-is-main-tpl');
    imageIsMainInput.attr('id','iu-image-is-main-'+n);
    imageIsMainInput.attr('name',imageArrayName+'['+newImageNumber+'][isMain]');
    newImageSection.find('#iu-files-to-upload-container-tpl').attr('id','iu-files-to-upload-container-'+n);
    newImageSection.find('#iu-uploaded-file-list-tpl').attr('id','iu-uploaded-file-list-'+n);
    newImageSection.find('#iu-upload-console-messages-section-tpl').attr('id','iu-upload-console-messages-section-'+n);
    newImageSection.find('#iu-file-upload-console-tpl').attr('id','iu-file-upload-console-'+n);
    var imageNumberInput=newImageSection.find('#iu-image-number-tpl');
    imageNumberInput.attr('id','iu-image-number-'+n);
    imageNumberInput.attr('name',imageArrayName+'['+newImageNumber+'][imageNumber]');
    imageNumberInput.val(newImageNumber);
    var imageIdInput=newImageSection.find('#iu-image-id-tpl');
    imageIdInput.attr('id','iu-image-id-'+n);
    imageIdInput.attr('name',imageArrayName+'['+newImageNumber+'][imageId]');
    var eventUniqueHashForFileNameInput=newImageSection.find('#iu-unique-hash-for-file-name-tpl');
    eventUniqueHashForFileNameInput.attr('id','iu-unique-hash-for-file-name-'+n);
    eventUniqueHashForFileNameInput.attr('name',imageArrayName+'['+newImageNumber+'][uniqueHashForFileName]');

    // Append it before add-button:
    newImageSection.show();
    section.find('#iu-image-section-tpl').after(newImageSection);

    // Attach events:
    imageUpload_attachEventToImageEditButton(section,pageBlockId,aspectRatio,newImageNumber,uniqueIdentifier);
    imageUpload_attachUploadLibraryToSection(
        section,pageBlockId,parametersForPlUploadLibrary,newImageNumber,uniqueIdentifier
    );
    imageUpload_attachEventToImageDeleteButton(
        section,pageBlockId,blockElementId,imageArrayName,aspectRatio,parametersForPlUploadLibrary,
        onBeforeDeletedCallback,newImageNumber,uniqueSectionIdentifier,uniqueIdentifier
    );
}
function imageUpload_attachSortableToSection(section)
{
    var section=$(section);

    if(section.length<1)
    {
        return false;
    }

    section.sortable({
        'items':'[data-image-number]',
        'handle':'> img',
        'cancel':'[data-image-number]:has(.ae-event-image-is-main:checked)',
        'sort':function(event,ui){
            var draggedElement=$(ui.item[0]);
            var draggedElementHeight=draggedElement.outerHeight();
            var placeholder=draggedElement.parent().find('.ui-sortable-placeholder');
            placeholder.removeClass('ui-sortable-placeholder');
            placeholder.css({
                'background':'#fcefa1',
                'border':'1px solid orange',
                'height':draggedElementHeight+'px',
                'visibility':'visible'
            });
        }
    }).disableSelection();
}
function blockOffer_handleCustomFunctionality(pageBlockId)
{
    var blockContent=$('.slot-major[data-page-block-id="'+pageBlockId+'"] .block-content');
}
function blockOffer_request_reorderMajors(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-offer/reorder-majors'),doneCallback,postData);
}
function blockOffer_request_toggleMajorPublishing(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-offer/toggle-major-publishing'),doneCallback,postData);
}
function blockOffer_request_addEditMajor(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-offer/add-edit-major'),doneCallback,postData);
}
function blockOffer_request_deleteMajor(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-offer/delete-major'),doneCallback,postData);
}
function blockOffer_showAddEditWindow(type,title,btn_save_text,btn_cancel_text,additionalData)
{
    var pageBlockId=additionalData.pageBlockId;
    var postData={};
    postData=$.extend(postData,additionalData);
    request_getBlockAddEditView(function(response){
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            var withSaveButton=true;
            jDialog(contents,title,'dialog-add-edit-major',function(id,success){
                if(success)
                {
                    var dialog=$('#'+id);
                    dialog.on('click change','#am-major-degree',function(){

                    });
                    dialog.find('#am-major-text-icon').selectpicker();
                    dialog.on('click','.btn-save',function(){
                        var majorTextIcon=dialog.find('#am-major-text-icon').selectpicker('val');
                        console.log(majorTextIcon);
                        var form=dialog.find('.form');
                        var formData=getFormData(form);
                        var formData= $.extend(formData,{
                            'majorTextIcon':majorTextIcon
                        });
                        var postData=$.extend({
                            'dataPosted':true,
                            'formData':formData
                        },additionalData);
                        blockOffer_request_addEditMajor(postData,function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                var expectedStatus=(type=='add')?'MAJOR_ADDED':'MAJOR_MODIFIED';
                                if(response.meta.customStatus==expectedStatus)
                                {
                                    dialog.modal('hide');
                                    refreshPageBlockManagementWindow(pageBlockId);
                                }
                            }
                        });
                    });
                }
            },withSaveButton);
        }
    },postData);
}
function blockOffer_handleManagementWindow(pageBlockId)
{
    var viewRequestGetFunction=request_getBlockManagementView;
    var viewGetCallback=null;
    var togglePublishingCallback=function(published,elementId,button){
        var postData={
            'pageBlockId':pageBlockId,
            'majorId':elementId,
            'published':published?1:0
        };
        blockOffer_request_toggleMajorPublishing(postData,function(response){
            if(requestOkAndSessionExist(response))
            {
                var status=response.meta.customStatus;
                if(status=='PUBLISHED')
                {

                }
                else if(status=='UNPUBLISHED')
                {

                }
            }
        });
    };
    var addNewCallback=function(button){
        var additionalData={
            'pageBlockId':pageBlockId
        };
        blockOffer_showAddEditWindow('add','Dodaj nowy kierunek','Zapisz','Anuluj',additionalData);
    };
    var actionCallback=function(elementId,action,button)
    {
        action=$.trim(action).toLowerCase();
        if(action=='edit')
        {
            blockOffer_showAddEditWindow('edit','Edytuj kierunek','Zapisz','Anuluj',{
                'pageBlockId':pageBlockId,
                'majorId':elementId
            });
        }
        else if(action=='delete')
        {
            jConfirm(
                'Czy na pewno chcesz usunąć ten kierunek?',
                'Potwierdź usunięcie kierunku',
                'dialog-confirm-major-removal',
                function(id,confirmed){
                    if(confirmed)
                    {
                        var postData={
                            'pageBlockId':pageBlockId,
                            'majorId':elementId
                        };
                        blockOffer_request_deleteMajor(postData,function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                if(response.meta.customStatus=='MAJOR_DELETED')
                                {
                                    refreshPageBlockManagementWindow(pageBlockId);
                                }
                            }
                        });
                    }
                }
            );
        }
    };
    var onCloseCallback=function(button){
        renderBlockAgain(pageBlockId);
    };
    handlePageBlockManagementWindow(
        pageBlockId,viewRequestGetFunction,viewGetCallback,togglePublishingCallback,addNewCallback,actionCallback,
        onCloseCallback
    );
}
function blockOffer_request_getDegreesManagementView(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-offer/get-degrees-management-view'),doneCallback,postData);
}
function blockOffer_request_toggleDegreePublishing(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-offer/toggle-degree-publishing'),doneCallback,postData);
}
function blockOffer_request_getAddEditDegreeView(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-offer/get-add-edit-degree-view'),doneCallback,postData);
}
function blockOffer_request_addEditDegree(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-offer/add-edit-degree'),doneCallback,postData);
}
function blockOffer_request_deleteDegree(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-offer/delete-degree'),doneCallback,postData);
}
function blockOffer_refreshDegreesManagementWindow(pageBlockId)
{
    blockOffer_request_getDegreesManagementView(function(response){
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            if(contents!='')
            {
                $('#dialog-block-degrees-management .modal-body').html(contents);
            }
        }
    },{
        'pageBlockId':pageBlockId
    });
}
function blockOffer_showAddEditDegreeWindow(type,title,btn_save_text,btn_cancel_text,additionalData)
{
    var pageBlockId=additionalData.pageBlockId;
    var expectedStatus=(type=='add')?'DEGREE_ADDED':'DEGREE_MODIFIED';
    var postData={};
    postData=$.extend(postData,additionalData);
    blockOffer_request_getAddEditDegreeView(function(response){
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            var withSaveButton=true;
            jDialog(contents,title,'dialog-add-edit-degree',function(id,success){
                if(success)
                {
                    var dialog=$('#'+id);
                    dialog.on('click','.btn-save',function(){
                        var form=dialog.find('.form');
                        var formData=getFormData(form);
                        var postData=$.extend({
                            'dataPosted':true,
                            'formData':formData
                        },additionalData);
                        blockOffer_request_addEditDegree(function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                if(response.meta.customStatus==expectedStatus)
                                {
                                    dialog.modal('hide');
                                    blockOffer_refreshDegreesManagementWindow(pageBlockId);
                                }
                            }
                        },postData);
                    });
                }
            },withSaveButton);
        }
    },postData);
}
function blockOffer_handleDegreesManagementWindow(pageBlockId)
{
    var viewRequestGetFunction=blockOffer_request_getDegreesManagementView;
    var viewGetCallback=null;
    var togglePublishingCallback=function(published,elementId,button){
        blockOffer_request_toggleDegreePublishing(function(response){
            if(requestOkAndSessionExist(response))
            {
                var status=response.meta.customStatus;
                if(status=='PUBLISHED')
                {

                }
                else if(status=='UNPUBLISHED')
                {

                }
            }
        },{
            'pageBlockId':pageBlockId,
            'degreeId':elementId,
            'published':published?1:0
        });
    };
    var addNewCallback=function(button){
        blockOffer_showAddEditDegreeWindow('add','Dodaj nowy stopień','Zapisz','Anuluj',{
            'pageBlockId':pageBlockId
        });
    };
    var actionCallback=function(elementId,action,button){
        action=$.trim(action).toLowerCase();
        if(action=='edit')
        {
            blockOffer_showAddEditDegreeWindow('edit','Edytuj stopień','Zapisz','Anuluj',{
                'pageBlockId':pageBlockId,
                'degreeId':elementId
            });
        }
        else if(action=='delete')
        {
            jConfirm(
                'Czy na pewno chcesz usunąć tn stopień?',
                'Potwierdź usunięcie stopnia',
                'dialog-confirm-degree-removal',
                function(id,confirmed){
                    if(confirmed)
                    {
                        blockOffer_request_deleteDegree(function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                if(response.meta.customStatus=='DEGREE_DELETED')
                                {
                                    blockOffer_refreshDegreesManagementWindow(pageBlockId);
                                    refreshPageBlockManagementWindow(pageBlockId);
                                }
                            }
                        },{
                            'pageBlockId':pageBlockId,
                            'degreeId':elementId
                        });
                    }
                }
            );
        }
    };
    var onCloseCallback=function(button){
        refreshPageBlockManagementWindow(pageBlockId);
    };
    var otherSelectorOrInstance=$('#dialog-block-management .btn-manage-degrees');
    var optionalCssClassPrefix='degrees-';
    var otherTitlePrefix='Stopnie';
    handlePageBlockManagementWindow(
        pageBlockId,
        viewRequestGetFunction,
        viewGetCallback,
        togglePublishingCallback,
        addNewCallback,
        actionCallback,
        onCloseCallback,
        otherSelectorOrInstance,
        optionalCssClassPrefix,
        otherTitlePrefix
    );
}
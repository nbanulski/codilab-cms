function blockNewsSlider_handleCustomFunctionality(pageBlockId)
{
    loadJsFile('/libs/jcarousel/jquery.jcarousel.min.js');

    var blockContent=$('.slot-item[data-page-block-id="'+pageBlockId+'"] .block-content');
    var newsSlider=blockContent.find('.news-slider');
    var jCarouselWrapper=newsSlider.find('.jcarousel-wrapper');
    var jCarousel=jCarouselWrapper.find('.jcarousel');
    var jCarouselControls = newsSlider.find('a.jcarousel-control-prev, a.jcarousel-control-next');

    if(!jCarousel.length)
    {
        return;
    }

    var slideIntervalInMs=toInt(newsSlider.attr('data-slide-interval-in-ms'));
    var animationsTimeInMs=toInt(newsSlider.attr('data-animations-time-in-ms'));
    var wrappingType=newsSlider.attr('data-wrapping-type');

    if(slideIntervalInMs<=0)
    {
        slideIntervalInMs=5000;
    }
    if(animationsTimeInMs<=0)
    {
        animationsTimeInMs=1500;
    }
    if($.inArray(wrappingType,['','first','last','both','circular'])==-1)
    {
        wrappingType='last';
    }

    /*jCarousel.off('mouseenter').on('mouseenter',function(){
        $(this).attr('data-sliding-disabled',1);
        jCarouselControls.show();
    });
    jCarousel.off('mouseleave').on('mouseleave',function(){
        $(this).attr('data-sliding-disabled',0);
        jCarouselControls.hide();
    });*/
    jCarouselControls.show();

    jCarousel.jcarousel({
        animation:animationsTimeInMs,
        wrap:wrappingType,
        transitions:'transforms3d'
    });

    jCarouselWrapper.find('.jcarousel-control-prev')
    .on('jcarouselcontrol:active',function(){
        $(this).removeClass('inactive');
    })
    .on('jcarouselcontrol:inactive',function(){
        $(this).addClass('inactive');
    })
    .jcarouselControl({
        'target':'-=1'
    });

    jCarouselWrapper.find('.jcarousel-control-next')
    .on('jcarouselcontrol:active','click touchstart',function(){
        $(this).removeClass('inactive');
    })
    .on('jcarouselcontrol:inactive','click touchstart',function(){
        $(this).addClass('inactive');
    })
    .jcarouselControl({
        'target':'+=1'
    });

    jCarouselWrapper.find('.jcarousel-pagination')
    .on('jcarouselpagination:active','a',function(){
        $(this).addClass('active');
    })
    .on('jcarouselpagination:inactive','a',function(){
        $(this).removeClass('active');
    })
    .jcarouselPagination();

    /**
     * Enable dynamic sliding.
     */
    var slidingTimerId = setInterval(blockNewsSlider_slideElementInCarousel,slideIntervalInMs,jCarousel);
    newsSlider.attr('data-sliding-timer-id',slidingTimerId);
}
function blockNewsSlider_onBeforeDeleted(pageBlockId){
    var blockContent=$('.slot-item[data-page-block-id="'+pageBlockId+'"] .block-content');
    var imageSlider=blockContent.find('.news-slider');
    var slidingTimerId=imageSlider.attr('data-sliding-timer-id');

    clearInterval(slidingTimerId);
}
function blockNewsSlider_slideElementInCarousel(jCarousel)
{
    var slidingDisabled=toInt(jCarousel.attr('data-sliding-disabled'));
    if(!slidingDisabled)
    {
        try
        {
            jCarousel.jcarousel('scroll','+=1');
        }
        catch(e)
        {

        }
    }
}
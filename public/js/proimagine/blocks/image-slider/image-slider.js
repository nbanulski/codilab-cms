loadPlUploadLibrary();
function blockImageSlider_request_reorderImages(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-image-slider/reorder-images'),doneCallback,postData);
}
function blockImageSlider_request_toggleImagePublishing(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-image-slider/toggle-image-publishing'),doneCallback,postData);
}
function blockImageSlider_request_addEditImage(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-image-slider/add-edit-image'),doneCallback,postData);
}
function blockImageSlider_request_deleteImage(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-image-slider/delete-image'),doneCallback,postData);
}
function blockImageSlider_handleCustomFunctionality(pageBlockId)
{
    var blockContent=$('.slot-item[data-page-block-id="'+pageBlockId+'"] .block-content');
    var prevNextControls=blockContent.find('.prev-next-controls');
    var cycleSlideshow=blockContent.find('.slideshow');

    cycleSlideshow.cycle({maxZ:50});
}
function blockImageSlider_showAddEditWindow(type,title,btn_save_text,btn_cancel_text,additionalData)
{
    var pageBlockId=additionalData.pageBlockId;

    var blockContent=$('.slot-item[data-page-block-id="'+pageBlockId+'"] .block-content');
    var slideshow=blockContent.find('.slideshow');
    var imageWidth=toInt(slideshow.attr('data-image-width'));
    var imageHeight=toInt(slideshow.attr('data-image-height'));

    var postData={};
    postData=$.extend(postData,additionalData);
    request_getBlockAddEditView(function(response){
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            var withSaveButton=true;
            jDialog(contents,title,'dialog-add-edit-image',function(id,success){
                if(success)
                {
                    var dialog=$('#'+id);
                    var urlAddressTypeDropdown=dialog.find('#ai-image-url-address-type');
                    var urlAddressType=urlAddressTypeDropdown.find('option:selected').val();
                    var isInternalUrl=(urlAddressType=='0');

                    dialog.on('click change',urlAddressTypeDropdown,function(){
                        var urlAddressType=$.trim($(this).find('option:selected').val());
                        if(urlAddressType=='0')
                        {
                            dialog.find('#ai-image-internal-page-id').show();
                            dialog.find('#ai-image-external-url').hide();
                            dialog.find('#ai-image-url-form-group').show();
                        }
                        else if(urlAddressType=='1')
                        {
                            dialog.find('#ai-image-internal-page-id').hide();
                            dialog.find('#ai-image-external-url').show();
                            dialog.find('#ai-image-url-form-group').show();
                        }
                        else
                        {
                            dialog.find('#ai-image-url-form-group').hide();
                            dialog.find('#ai-image-internal-page-id').hide();
                            dialog.find('#ai-image-external-url').hide();
                        }
                    });
                    dialog.on('click change','#ai-image-internal-page-id',function(){
                        var urlAddressType=urlAddressTypeDropdown.find('option:selected').val();
                        var isInternalUrl=(urlAddressType=='0');
                        if(isInternalUrl)
                        {
                            var internalPageIdDropdown=dialog.find('#ai-image-internal-page-id');
                            var selectedOption=internalPageIdDropdown.find('option:selected');

                            // Set image title the same as selected internal page title:
                            var selectedInternalPageTitle=$.trim(selectedOption.attr('data-title'));
                            dialog.find('#ai-image-title').val(selectedInternalPageTitle);
                        }
                    });
                    var imageTitleInput=dialog.find('#ai-image-title');
                    var imageTitle=$.trim(imageTitleInput.val());
                    if(isInternalUrl&&(imageTitle==''))
                    {
                        var internalPageIdDropdown=dialog.find('#ai-image-internal-page-id');
                        var selectedOption=internalPageIdDropdown.find('option:selected');

                        // Set image title the same as selected internal page title:
                        var selectedInternalPageTitle=$.trim(selectedOption.attr('data-title'));
                        imageTitleInput.val(selectedInternalPageTitle);
                    }
                    dialog.on('click','.btn-save',function(){
                        var form=dialog.find('.form');
                        var formData=getFormData(form);
                        var postData=$.extend({
                            'dataPosted':true,
                            'formData':formData
                        },additionalData);
                        blockImageSlider_request_addEditImage(function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                var expectedStatus=(type=='add')?'IMAGE_ADDED':'IMAGE_MODIFIED';
                                if(response.meta.customStatus==expectedStatus)
                                {
                                    dialog.modal('hide');
                                    refreshPageBlockManagementWindow(pageBlockId);
                                }
                            }
                        },postData);
                    });
                }
            },withSaveButton);
        }
    },postData);
}
function blockImageSlider_handleManagementWindow(pageBlockId)
{
    var viewRequestGetFunction=request_getBlockManagementView;
    var viewGetCallback=null;
    var togglePublishingCallback=function(published,elementId,button){
        blockImageSlider_request_toggleImagePublishing(function(response){
            if(requestOkAndSessionExist(response))
            {
                var status=response.meta.customStatus;
                if(status=='PUBLISHED')
                {

                }
                else if(status=='UNPUBLISHED')
                {

                }
            }
        },{
            'pageBlockId':pageBlockId,
            'imageId':elementId,
            'published':published?1:0
        });
    };
    var addNewCallback=function(button){
        blockImageSlider_showAddEditWindow('add','Dodaj nowy element','Zapisz','Anuluj',{
            'pageBlockId':pageBlockId
        });
    };
    var actionCallback=function(elementId,action,button)
    {
        action=$.trim(action).toLowerCase();
        if(action=='edit')
        {
            blockImageSlider_showAddEditWindow('edit','Edytuj element','Zapisz','Anuluj',{
                'pageBlockId':pageBlockId,
                'imageId':elementId
            });
        }
        else if(action=='delete')
        {
            var title='Potwierdź usunięcie elementu';
            var contents='Czy na pewno chcesz usunąć ten element?';
            var id='dialog-confirm-image-removal';
            jConfirm(contents,title,id,function(id,confirmed){
                if(confirmed)
                {
                    blockImageSlider_request_deleteImage(function(response){
                        if(requestOkAndSessionExist(response))
                        {
                            if(response.meta.customStatus=='IMAGE_DELETED')
                            {
                                refreshPageBlockManagementWindow(pageBlockId);
                            }
                        }
                    },{
                        'pageBlockId':pageBlockId,
                        'imageId':elementId
                    });
                }
            });
        }
    };
    var onCloseCallback=function(button){
        renderBlockAgain(pageBlockId);
    };

    handlePageBlockManagementWindow(
        pageBlockId,viewRequestGetFunction,viewGetCallback,togglePublishingCallback,addNewCallback,actionCallback,
        onCloseCallback
    );
}
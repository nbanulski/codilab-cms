function blockTabsControl_request_reorderTabs(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-tabs-control/reorder-tabs'),doneCallback,postData);
}
function blockTabsControl_request_addEditTab(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-tabs-control/add-edit-tab'),doneCallback,postData);
}
function blockTabsControl_request_deleteTab(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-tabs-control/delete-tab'),doneCallback,postData);
}
function blockTabsControl_request_updateTabName(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-tabs-control/update-tab-name'),doneCallback,postData);
}
function blockTabsControl_request_updateTabContents(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-tabs-control/update-tab-contents'),doneCallback,postData);
}
function blockTabsControl_deleteTab(pageBlockId,tabId,doneCallback)
{
    var postData={
        'pageBlockId':pageBlockId,
        'tabId':tabId
    };
    blockTabsControl_request_deleteTab(postData,doneCallback);
}
function blockTabsControl_updateTabName(pageBlockId,tabId,tabName,doneCallback)
{
    var postData={
        'pageBlockId':pageBlockId,
        'tabId':tabId,
        'tabName':tabName
    };
    blockTabsControl_request_updateTabName(postData,doneCallback);
}
function blockTabsControl_updateTabContents(pageBlockId,tabId,tabContents,doneCallback)
{
    var postData={
        'pageBlockId':pageBlockId,
        'tabId':tabId,
        'tabContents':tabContents
    };
    blockTabsControl_request_updateTabContents(postData,doneCallback);
}
function blockTabsControl_handleCustomFunctionality(pageBlockId)
{
    var blockContent=$('.slot-item[data-page-block-id="'+pageBlockId+'"] .block-content');
    var tabsControlList=blockContent.find('.nav.nav-tabs');
    var tabsContents=blockContent.find('.tabs-contents');

    tabsControlList.off('click','> li').on('click','> li',function(){
        var tabId=$(this).attr('data-tab-id');
        tabsContents.find('.tab-pane[data-tab-id!='+tabId+']').hide();
        tabsContents.find('.tab-pane[data-tab-id='+tabId+']').show();
        tabsControlList.find('> li[data-tab-id!='+tabId+']').removeClass('active');
        tabsControlList.find('> li[data-tab-id='+tabId+']').addClass('active');
    });

    if(isDesignModeEnabled())
    {
        tabsControlList.sortable({
            'items':'> li:not(.add-new-tab)',
            'sort':function(event,ui){
                var draggedElement=$(ui.item[0]);
                var draggedElementHeight=draggedElement.outerHeight();
                var placeholder=draggedElement.parent().find('.ui-sortable-placeholder');
                placeholder.removeClass('ui-sortable-placeholder');
                placeholder.css({
                    'background':'#fcefa1',
                    'border':'1px solid orange',
                    'height':draggedElementHeight+'px',
                    'visibility':'visible'
                });
            },
            'stop':function(event,ui){
                var parentUlTag=ui.item.parent();
                var tabs=[];
                parentUlTag.find('>li').each(function(){
                    var tab=$(this);
                    var tabId=tab.attr('data-tab-id');
                    tabs.push(tabId);
                });

                var postData={
                    'pageBlockId':pageBlockId,
                    'tabs':tabs
                };
                blockTabsControl_request_reorderTabs(postData,function(response){

                });
            }
        });

        tabsControlList.off('dblclick','> li:not(.add-new-tab) span')
                       .on('dblclick','> li:not(.add-new-tab) span',function(){
            var span=$(this);
            span.prop('contenteditable',true);
            span.focus();
            span.off('blur').on('blur',function(){
                var tabId=span.closest('li').attr('data-tab-id');
                var tabName=$.trim(span.text());
                blockTabsControl_updateTabName(pageBlockId,tabId,tabName);
                span.removeAttr('contenteditable');
            });
        });

        tabsControlList.off('click','> li.add-new-tab').on('click','> li.add-new-tab',function(){
            var postData={
                'pageBlockId':pageBlockId
            };
            blockTabsControl_request_addEditTab(postData,function(response){
                if(requestOkAndSessionExist(response))
                {
                    var expectedStatus='TAB_ADDED';
                    if(response.meta.customStatus==expectedStatus)
                    {
                        var additionalData={
                            'tabId':response.data
                        }
                        renderBlockAgain(pageBlockId,additionalData);
                    }
                }
            });

            return false;
        });

        tabsContents.off('dblclick','.tab-pane[data-tab-id] .tab-contents')
                    .on('dblclick','.tab-pane[data-tab-id] .tab-contents',function(){
            loadCkEditorLibrary();

            var textDiv=$(this);
            var ckEditorId='block-tabs-control-tab-contents-editable-'+pageBlockId;
            textDiv.attr('id',ckEditorId);
            textDiv.prop('contenteditable',true);
            textDiv.ckeditor($.extend(defaultCkeditorConfig,{
                filebrowserImageBrowseUrl:'/libs/kcfinder/kcfinder/browse.php?type=images&cms=proimagine-cms',
                filebrowserImageUploadUrl:'/libs/kcfinder/kcfinder/upload.php?type=images&cms=proimagine-cms',
                filebrowserBrowseUrl:'/libs/kcfinder/kcfinder/browse.php?type=other-files&cms=proimagine-cms',
                filebrowserUploadUrl:'/libs/kcfinder/kcfinder/upload.php?type=other-files&cms=proimagine-cms',
                'toolbar':false
            }));

            var ckEditorInstance=getCkeditorInstanceById(ckEditorId);
            ckEditorInstance.on('blur',function(e){
                if($(document.activeElement).get(0).tagName.toLowerCase()!="iframe"){
                    var editor=$(this)[0];
                    if(editor)
                    {
                        var id=editor.name;
                        if(id!='')
                        {
                            var tabId=textDiv.closest('.tab-pane').attr('data-tab-id');
                            var tabContents=null;
                            if(getCkeditorInstanceById(id))
                            {
                                tabContents=getCkeditorData(id); // CKEditor.
                            }
                            else
                            {
                                tabContents=$('#'+id).html(); // HTML5 Content-Editable element.
                            }

                            blockTabsControl_updateTabContents(pageBlockId,tabId,tabContents,function(response){
                                if(requestOkAndSessionExist(response))
                                {
                                    var expectedStatus='TAB_CONTENTS_UPDATED';
                                    if(response.meta.customStatus==expectedStatus)
                                    {
                                        var additionalData={
                                            'tabId':tabId
                                        }
                                        renderBlockAgain(pageBlockId,additionalData);
                                    }
                                }
                            });

                            destroyCkeditor(id);
                            $('#'+id).removeAttr('contenteditable');
                        }
                    }
                }
            });
        });

        tabsContents.off('click','.btn-delete-this-tab').on('click','.btn-delete-this-tab',function(){
            var tabId=$(this).closest('.tab-pane').attr('data-tab-id');
            var tabName=$.trim(tabsControlList.find('> li[data-tab-id="'+tabId+'"]').text());
            var title='Potwierdź usunięcie zakładki';
            var contents='Czy na pewno chcesz usunąć zakładkę &quot;'+tabName+'&quot; ?';
            var customId='dialog-confirm-tab-deletion';
            jConfirm(contents,title,customId,function(id,confirmed){
                if(confirmed)
                {
                    blockTabsControl_deleteTab(pageBlockId,tabId,function(response){
                        if(requestOkAndSessionExist(response))
                        {
                            var expectedStatus='TAB_DELETED';
                            if(response.meta.customStatus==expectedStatus)
                            {
                                renderBlockAgain(pageBlockId);
                            }
                        }
                    });
                }
            });

            return false;
        });
    }
}
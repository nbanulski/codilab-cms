function blockEventDetails_request_reorderEventImages(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-event-details/reorder-event-images'),doneCallback,postData);
}
function blockEventDetails_request_updateEventTitle(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-event-details/update-event-title'),doneCallback,postData);
}
function blockEventDetails_request_updateEventImageTitle(postData,doneCallback)
{
     makePostRequest(makeCmsUrl('block-event-details/update-event-gallery-image-title'),doneCallback,postData);
}
function blockEventDetails_request_updateEventContents(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-event-details/update-event-contents'),doneCallback,postData);
}
function blockEventDetails_request_updateEventDateStart(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-event-details/update-event-date-start'),doneCallback,postData);
}
function blockEventDetails_request_updateEventAdditionalData(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-event-details/update-event-additional-data'),doneCallback,postData);
}
function blockEventDetails_updateEventTitle(pageBlockId,eventId,eventTitle,doneCallback)
{
    var postData={
        'pageBlockId':pageBlockId,
        'eventId':eventId,
        'eventTitle':eventTitle
    };
    blockEventDetails_request_updateEventTitle(postData,doneCallback);
}
function blockEventDetails_updateEventImageTitle(contentId,eventId,imageId,imageTitle,doneCallback)
{
    var postData={
        'contentId':contentId,
        'eventId':eventId,
        'imageId':imageId,
        'imageTitle':imageTitle
    };
    blockEventDetails_request_updateEventImageTitle(postData,doneCallback);
}
function blockEventDetails_updateEventContents(pageBlockId,eventId,eventContents,doneCallback)
{
    var postData={
        'pageBlockId':pageBlockId,
        'eventId':eventId,
        'eventContents':eventContents
    };
    blockEventDetails_request_updateEventContents(postData,doneCallback);
}
function blockEventDetails_updateEventAdditionalData(pageBlockId,eventId,dataTypeId,data,doneCallback)
{
    var postData={
        'pageBlockId':pageBlockId,
        'eventId':eventId,
        'dataTypeId':dataTypeId,
        'data':data
    };
    blockEventDetails_request_updateEventAdditionalData(postData,doneCallback);
}
function blockEventDetails_refreshEventBlocks()
{
    renderBlockAgainByBlockName('event-list');
    renderBlockAgainByBlockName('future-event-list');
    renderBlockAgainByBlockName('old-event-list');
    renderBlockAgainByBlockName('event-details');
}
function blockEventDetails_request_signUpForEvent(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-event-details/sign-up-for-event'),doneCallback,postData);
}
function blockEventDetails_handleSignUpFormFunctionality(signUpFormSelectorOrInstance,eventListContentId,translations)
{
    var signUpForm=$(signUpFormSelectorOrInstance);

    signUpForm.on('click','.btn-cancel',function(){
        $(this).closest('.sign-up-form').hide();
    });

    signUpForm.on('click','[name="signUpType"]',function(){
        var input=$(this);
        var type=input.val();
        var institutionDataSection=input.closest('.sign-up-form').find('.institution-data');
        if(type=='institution')
        {
            institutionDataSection.find('> .form-group').addClass('required');
            institutionDataSection.find('[name="signUpInstitutionName"]').addClass('required');
            institutionDataSection.slideDown();
        }
        else
        {
            institutionDataSection.slideUp();
            institutionDataSection.find('[name="signUpInstitutionName"]').removeClass('required');
            institutionDataSection.find('> .form-group').removeClass('required');
        }
    });

    signUpForm.on('click','.btn-send',function(){
        var btn=$(this);
        var form=btn.closest('.sign-up-form');
        var formInvalid=markRequiredFormFieldsWhenTheyAreEmptyOrUnselectedAndReturnResult(form);
        console.log(form);
        console.log(formInvalid);
        if(formInvalid)
        {
            return false;
        }
        var formData=getFormData(form);
        var postData={
            'eventListContentId':eventListContentId,
            'formData':formData
        };
        blockEventDetails_request_signUpForEvent(postData,function(response){
            var message=translations.messageFailedToSignUpForEvent;
            var messageCssClass='text-danger';
            if(requestOk(response))
            {
                if (response.meta.customStatus=='SUBSCRIPTION_SAVED')
                {
                    message=translations.messageSuccessfullySignedUpForEvent;
                    messageCssClass='text-success';
                    form.slideUp();
                }
                else if (response.meta.customStatus=='USER_ALREADY_SIGNED_UP_FOR_EVENT')
                {
                    message=translations.messageAlreadySignedUpForEvent;
                    messageCssClass='text-info';
                }
            }
            var messageContainer=form.closest('.sign-up').find('.message');
            messageContainer.text(message);
            messageContainer.removeClass('text-success text-info text-danger').addClass(messageCssClass);
            btn.text(translations.sendAgain);
        });
    });
}
function blockEventDetails_request_getSignUpFormView(postData,doneCallback,translations)
{
    var body=$('body');
    var id='sign-up-form-view-template';
    var signUpFormViewTemplate=body.find('#'+id);
    if(!signUpFormViewTemplate.length)
    {
        var doNotRequireSession=true;
        makePostRequest(makeCmsUrl('block-event-details/get-sign-up-form-view'),function(response){
            if(requestOk(response))
            {
                signUpFormViewTemplate=$(response.data).attr('id',id);
                body.append(signUpFormViewTemplate);
            }

            if(signUpFormViewTemplate.length)
            {
                blockEventDetails_handleSignUpFormFunctionality(
                    signUpFormViewTemplate,postData.eventListContentId,translations
                );

                signUpFormViewTemplate=signUpFormViewTemplate.clone(true);
            }

            doneCallback(signUpFormViewTemplate);
        },postData,doNotRequireSession);
    }
    else
    {
        doneCallback(signUpFormViewTemplate.clone(true));
    }
}
function blockEventDetails_request_addAndGetSignUpFormViewForEventSignUpBtn(
    signUpBtnSelectorOrInstance,postData,doneCallback,translations
)
{
    var signUpBtn=$(signUpBtnSelectorOrInstance);
    var signUp=signUpBtn.closest('.sign-up');
    var signUpFormView=signUp.find('.sign-up-form');
    if(!signUpFormView.length)
    {
        blockEventDetails_request_getSignUpFormView(postData,function(signUpFormViewClone){
            if(signUpFormViewClone&&signUpFormViewClone.length)
            {
                var id=signUpFormViewClone.attr('id').replace('-template','');
                signUpFormViewClone.attr('id',id);
                signUpFormViewClone.find('[name="signUpEventId"]').val(postData.eventId);
                signUpFormViewClone.hide();

                signUp.append(signUpFormViewClone);

                signUp.find('#'+id).slideDown();
            }
            doneCallback(signUpFormViewClone); // Return new (empty) form.
        },translations);
    }
    else
    {
        doneCallback(signUpFormView); // Return current form, may be already filled with some contents.
    }
}
function blockEventDetails_handleCustomFunctionality(pageBlockId,translations)
{
    var block=$('.slot-item[data-page-block-id="'+pageBlockId+'"]');
    var blockContent=block.find('.block-content > .content');
    var eventDetailsDiv=blockContent.find('.event-details');
    var eventContentsDiv=eventDetailsDiv.find('.contents');
    var eventAdditionalDataDomElement=eventDetailsDiv.find('.additional-data');

    eventDetailsDiv.off('dblclick','> .title').on('dblclick','> .title',function(){
        var h3Tag=$(this);
        h3Tag.prop('contenteditable',true);
        h3Tag.off('blur').on('blur',function(){
            var eventId=eventDetailsDiv.attr('data-event-id');
            var eventTitle=$.trim(h3Tag.text());
            blockEventDetails_updateEventTitle(pageBlockId,eventId,eventTitle,function(response){
                if(requestOkAndSessionExist(response))
                {
                    var expectedStatus='UPDATED';
                    if(response.meta.customStatus==expectedStatus)
                    {
                        blockEventDetails_refreshEventBlocks();
                    }
                }
            });
            h3Tag.removeAttr('contenteditable');
        });
    });

    eventDetailsDiv.on('click','.btn-sign-up-for-event',function(){
        var eventListContentId=eventDetailsDiv.attr('data-event-list-content-id');
        var eventId=$(this).closest('[data-event-id]').attr('data-event-id');
        var postData={
            'eventListContentId':eventListContentId,
            'eventId':eventId
        };
        blockEventDetails_request_addAndGetSignUpFormViewForEventSignUpBtn(this,postData,function(signUpForm){
            if(signUpForm&&signUpForm.length)
            {
                signUpForm.show();
            }
        },translations);
    });

    eventDetailsDiv.off('dblclick','.event-details-image-text').on('dblclick','.event-details-image-text',function(){        
        var imageTitle=$(this);
        imageTitle.prop('contenteditable',true);
        imageTitle.off('blur').on('blur',function(){
            var imageTitle=$(this);
            var contentId=eventDetailsDiv.attr('data-event-list-content-id');
            var eventId=eventDetailsDiv.attr('data-event-id');
            var imageId=imageTitle.attr('data-image-id');
            var imageTitleText=$.trim(imageTitle.text());
            blockEventDetails_updateEventImageTitle(contentId,eventId,imageId,imageTitleText,function(response){
                if(requestOkAndSessionExist(response))
                {
                    var expectedStatus='UPDATED';
                    if(response.meta.customStatus==expectedStatus)
                    {
                        blockEventDetails_refreshEventBlocks();
                    }
                }
            });
            var lightBoxElement = imageTitle.parent().children('a');
            lightBoxElement.attr('title',imageTitleText);
            imageTitle.removeAttr('contenteditable');
        });
    });
    blockEventDetails_attachSortableToGallery(eventDetailsDiv);

    eventContentsDiv.off('dblclick','> .text').on('dblclick','> .text',function(){
        loadCkEditorLibrary();

        var textDiv=$(this);
        var ckEditorId='block-event-details-text-editable-'+pageBlockId;
        textDiv.attr('id',ckEditorId);
        textDiv.prop('contenteditable',true);
        textDiv.ckeditor($.extend(defaultCkeditorConfig,{
            filebrowserImageBrowseUrl:'/libs/kcfinder/kcfinder/browse.php?type=images&cms=proimagine-cms',
            filebrowserImageUploadUrl:'/libs/kcfinder/kcfinder/upload.php?type=images&cms=proimagine-cms',
            filebrowserBrowseUrl:'/libs/kcfinder/kcfinder/browse.php?type=other-files&cms=proimagine-cms',
            filebrowserUploadUrl:'/libs/kcfinder/kcfinder/upload.php?type=other-files&cms=proimagine-cms',
            'toolbar':false
        }));

        var ckEditorInstance=getCkeditorInstanceById(ckEditorId);
        ckEditorInstance.on('blur',function(e){
            if($(document.activeElement).get(0).tagName.toLowerCase()!="iframe"){
                var editor=$(this)[0];
                if(editor)
                {
                    var id=editor.name;
                    if(id!='')
                    {
                        var eventId=eventDetailsDiv.attr('data-event-id');
                        var eventContents=null;
                        if(getCkeditorInstanceById(id))
                        {
                            eventContents=getCkeditorData(id); // CKEditor.
                        }
                        else
                        {
                            eventContents=$('#'+id).html(); // HTML5 Content-Editable element.
                        }

                        blockEventDetails_updateEventContents(pageBlockId,eventId,eventContents,function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                var expectedStatus='UPDATED';
                                if(response.meta.customStatus==expectedStatus)
                                {
                                    blockEventDetails_refreshEventBlocks();
                                }
                            }
                        });

                        destroyCkeditor(id);
                        $('#'+id).removeAttr('contenteditable');
                    }
                }
            }
        });
    });

    eventAdditionalDataDomElement
        .off('dblclick','.start-hour .editable')
        .on('dblclick','.start-hour .editable',function(){
            var contentsDomElement=$(this);
            var currentHourText=$.trim(contentsDomElement.text());
            contentsDomElement
                .empty()
                .append('<input type="text" class="form-control timepicker" value="'+currentHourText+'" />');
            var timepicker=contentsDomElement.find('.timepicker');
            timepicker.timepicker({
                showMeridian:false
            }).on('hide.timepicker',function(e){
                var startHourWithMinutes=e.time.hours+':'+e.time.minutes;
                var postData={
                    'pageBlockId':pageBlockId,
                    'eventId':eventDetailsDiv.attr('data-event-id'),
                    'startHourWithMinutes':startHourWithMinutes
                }
                blockEventDetails_request_updateEventDateStart(postData,function(response){
                    if(requestOkAndSessionExist(response))
                    {
                        var expectedStatus='UPDATED';
                        if(response.meta.customStatus==expectedStatus)
                        {
                            blockEventDetails_refreshEventBlocks();
                        }
                    }
                });
                contentsDomElement.empty().text(startHourWithMinutes);
            });
        }
    );

    eventAdditionalDataDomElement.off('dblclick','[data-adata-name]').on('dblclick','[data-adata-name]',function(){
        loadCkEditorLibrary();

        var contentsDomElement=$(this);
        var dataTypeId=contentsDomElement.attr('data-adata-type-id');
        var ckEditorId=dataTypeId+'-editable'+pageBlockId;
        contentsDomElement.attr('id',ckEditorId);
        contentsDomElement.prop('contenteditable',true);
        contentsDomElement.ckeditor(defaultCkeditorConfig);

        var ckEditorInstance=getCkeditorInstanceById(ckEditorId);
        ckEditorInstance.on('blur',function(e){
            if($(document.activeElement).get(0).tagName.toLowerCase()!="iframe"){
                var editor=$(this)[0];
                if(editor)
                {
                    var id=editor.name;
                    if(id!='')
                    {
                        var eventId=eventDetailsDiv.attr('data-event-id');
                        var data=null;
                        if(getCkeditorInstanceById(id))
                        {
                            data=getCkeditorData(id); // CKEditor.
                        }
                        else
                        {
                            data=$('#'+id).html(); // HTML5 Content-Editable element.
                        }

                        blockEventDetails_updateEventAdditionalData(pageBlockId,eventId,dataTypeId,data,function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                var expectedStatus='UPDATED';
                                if(response.meta.customStatus==expectedStatus)
                                {
                                    blockEventDetails_refreshEventBlocks();
                                }
                            }
                        });

                        destroyCkeditor(id);
                        $('#'+id).removeAttr('contenteditable');
                    }
                }
            }
        });
    });
}
function blockEventDetails_attachSortableToGallery(eventDetailsDivInstanceOrSelector)
{
    var eventDetailsDiv=$(eventDetailsDivInstanceOrSelector);

    if(eventDetailsDiv.length<1)
    {
        return false;
    }

    var eventId=eventDetailsDiv.attr('data-event-id');
    var contentId=eventDetailsDiv.attr('data-event-list-content-id');
    var gallery=eventDetailsDiv.find('.contents .gallery');

    gallery.sortable({
        'sort':function(event,ui){
            var draggedElement=$(ui.item[0]);
            var draggedElementHeight=draggedElement.outerHeight();
            var placeholder=draggedElement.parent().find('.ui-sortable-placeholder');
            placeholder.removeClass('ui-sortable-placeholder');
            placeholder.css({
                'background':'#fcefa1',
                'border':'1px solid orange',
                'height':draggedElementHeight+'px',
                'visibility':'visible'
            });
        },
        'stop':function(event,ui){
            var parentElement=ui.item.parent();
            var images=[];
            parentElement.find('img').each(function(){
                var div=$(this);
                var imageId=div.attr('data-image-id');
                images.push(imageId);
            });

            var postData={
                'eventId':eventId,
                'contentId':contentId,
                'images':images
            };
            blockEventDetails_request_reorderEventImages(postData);
        }
    }).disableSelection();
}
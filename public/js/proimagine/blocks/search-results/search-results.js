function blockSearchResults_handleCustomFunctionality(pageBlockId)
{

}
function blockSearchResults_handlePagination(pageBlockId)
{
    var block=$('.slot-item[data-page-block-id="'+pageBlockId+'"]');
    block.find('.block-content .content .pagination')
        .off('click','li:not(.disabled):not(.active) a')
        .on('click','li:not(.disabled):not(.active) a',function(){
            var anchor=$(this);
            var text=$.trim(anchor.text());
            var pageNumber=null;
            if(text=='<<')
            {
                var activeAnchor=anchor.closest('ul').find('li.active a');
                var currentPageNumber=toInt($.trim(activeAnchor.text()));
                pageNumber=currentPageNumber-1;
            }
            else if(text=='>>')
            {
                var activeAnchor=anchor.closest('ul').find('li.active a');
                var currentPageNumber=toInt($.trim(activeAnchor.text()));
                pageNumber=currentPageNumber+1;
            }
            else
            {
                pageNumber=toInt(text);
            }

            var additionalData={
                'query':block.find('.search-results').attr('data-query'),
                'pageNumber':pageNumber
            }

            renderBlockAgain(pageBlockId,additionalData);

            return false;
        });
}
function blockText_getLeftAndTopPositionForBtnOpenCkEditor(
    blockTextContentSelectorOrInstance,btnOpenCkEditorSelectorOrInstance
)
{
    var blockTextContent=$(blockTextContentSelectorOrInstance);
    var btnOpenCkEditor=$(btnOpenCkEditorSelectorOrInstance);

    if(!btnOpenCkEditor||!btnOpenCkEditor.length)
    {
        return false;
    }

    var win=$(window);
    var viewPort={
        top:win.scrollTop(),
        left:win.scrollLeft()
    };
    viewPort.right=viewPort.left+win.width();
    viewPort.bottom=viewPort.top+win.height();

    var position=blockTextContent.position();
    var bounds=blockTextContent.offset();
    bounds.right=bounds.left+blockTextContent.outerWidth();
    bounds.bottom=bounds.top+blockTextContent.outerHeight();

    var clippedBoundsTop=null;
    var clippedBoundsBottom=null;
    var clippedBoundsLeft=null;
    var clippedBoundsRight=null;
    if(bounds.top>viewPort.top)
    {
        clippedBoundsTop=bounds.top;
    }
    else
    {
        clippedBoundsTop=viewPort.top;
    }
    if(bounds.bottom>viewPort.bottom)
    {
        clippedBoundsBottom=viewPort.bottom;
    }
    else
    {
        clippedBoundsBottom=bounds.bottom;
    }
    if(bounds.left>viewPort.left)
    {
        clippedBoundsLeft=bounds.left;
    }
    else
    {
        clippedBoundsLeft=viewPort.left;
    }
    if(bounds.right>viewPort.right)
    {
        clippedBoundsRight=viewPort.right;
    }
    else
    {
        clippedBoundsRight=bounds.right;
    }

    var visibleWidth=clippedBoundsRight-clippedBoundsLeft;
    var visibleHeight=clippedBoundsBottom-clippedBoundsTop;

    var scrollTopDifference=(viewPort.top-bounds.top);
    if(scrollTopDifference<0)
    {
        scrollTopDifference=0;
    }
    var scrollLeftDifference=(viewPort.left-bounds.left);
    if(scrollLeftDifference<0)
    {
        scrollLeftDifference=0;
    }

    var iconPositionTop=Math.ceil(position.top+((visibleHeight-btnOpenCkEditor.outerHeight())/2)+scrollTopDifference);
    var iconPositionLeft=Math.ceil(position.left+((visibleWidth-btnOpenCkEditor.outerWidth())/2)+scrollLeftDifference);

    return {'left':iconPositionLeft,'top':iconPositionTop};
}
function blockText_handleCustomFunctionality(pageBlockId)
{
    var block=$('.slot-item[data-page-block-id="'+pageBlockId+'"]');
    var blockContent=block.find('.block-content > .content');

    if(isDesignModeEnabled())
    {
        var contentContainerId='block-content-editable-'+pageBlockId;
        attachInlineCkEditorToDomElement(
            blockContent,
            '.block-text-content',
            contentContainerId,
            true,
            function(eventContainer,textContainer,oldTextContainerId,ckEditorId,kcFinderAttached,contents){
                var postData={
                    'pageBlockId':pageBlockId,
                    'blockContent':contents
                };
                makePostRequest('/eng/block/update',function(response){
                    if(requestOkAndSessionExist(response))
                    {
                        if(response.meta.customStatus=='BLOCK_CONTENT_SAVED')
                        {
                            renderBlockAgain(pageBlockId);
                        }
                    }
                },postData);
            }
        )

        blockContent.off('mouseenter').on('mouseenter',function(){
            var blockTextContent=blockContent.find('.block-text-content');
            if(!blockTextContent.hasClass('cke_editable'))
            {
                blockContent.append('<span class="btn-open-ckeditor fa fa-edit"></span>');
                var btnOpenCkEditor=blockContent.find('.btn-open-ckeditor');
                var newPosition=blockText_getLeftAndTopPositionForBtnOpenCkEditor(
                    blockTextContent,btnOpenCkEditor
                );

                btnOpenCkEditor.css('left',newPosition.left+'px');
                btnOpenCkEditor.css('top',newPosition.top+'px');

                blockContent.on('click','.btn-open-ckeditor',function(){
                    blockTextContent.trigger('dblclick');
                });
            }
        });
        blockContent.off('mouseleave').on('mouseleave',function(){
            blockContent.find('.btn-open-ckeditor').remove();
        });

        $(document).on('scroll',function(){
            var blockTextContent=blockContent.find('.block-text-content');
            var btnOpenCkEditor=blockContent.find('.btn-open-ckeditor');
            var newPosition=blockText_getLeftAndTopPositionForBtnOpenCkEditor(
                blockTextContent,btnOpenCkEditor
            );

            btnOpenCkEditor.css('left',newPosition.left+'px');
            btnOpenCkEditor.css('top',newPosition.top+'px');
        });
    }
}
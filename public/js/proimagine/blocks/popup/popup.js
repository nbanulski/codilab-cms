function blockPopup_request_getPopupEditView(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-popup/get-edit-view'),doneCallback,postData);
}
function blockPopup_request_getPopupTitleAndContentsView(postData,doneCallback)
{
    var doNotRequireSession=true;
    makePostRequest(makeCmsUrl('block-popup/get-title-and-contents-view'),doneCallback,postData,doNotRequireSession);
}
function blockPopup_request_savePopupContents(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-popup/save-contents'),doneCallback,postData);
}
function blockPopup_handleCustomFunctionality(pageBlockId)
{
    var block=$('.slot-item[data-page-block-id="'+pageBlockId+'"]');
    var blockContent=block.find('.block-content > .content');

    blockContent.on('click','.btn-edit-contents',function(){
        var postData={
            'pageBlockId':pageBlockId
        };
        blockPopup_request_getPopupEditView(postData,function(response){
            if(requestOkAndSessionExist(response))
            {
                var title=response.data.title;
                var contents=response.data.contents;
                var withSaveButton=true;
                jDialog(contents,title,'dialog-edit-popup-contents',function(id,success){
                    if(success)
                    {
                        var dialog=$('#'+id);

                        attachCkEditorToDomElement(
                            dialog.find('#popup-contents'),
                            true
                        );

                        dialog.on('click','.btn-save',function(){
                            var form=dialog.find('.form');
                            var formData=getFormData(form);

                            var popupTitleFormGroup=form.find('[name="popupTitle"]').closest('.form-group');
                            var popupContentsFormGroup=form.find('[name="popupContents"]').closest('.form-group');

                            popupTitleFormGroup.removeClass('has-error');
                            popupContentsFormGroup.removeClass('has-error');

                            if($.trim(formData.popupTitle)=='')
                            {
                                popupTitleFormGroup.addClass('has-error');
                            }
                            if($.trim(formData.popupContents)=='')
                            {
                                popupContentsFormGroup.addClass('has-error');
                            }

                            var postData={
                                'pageBlockId':pageBlockId,
                                'formData':formData
                            };
                            blockPopup_request_savePopupContents(postData,function(response){
                                if(requestOkAndSessionExist(response))
                                {
                                    if(response.meta.customStatus=='CONTENTS_UPDATED')
                                    {
                                        var additionalData={
                                            'doNotDisplayPopupNow':true
                                        };
                                        renderBlockAgain(pageBlockId,additionalData);
                                        dialog.modal('hide');
                                    }
                                }
                            });
                        });
                    }
                },withSaveButton);
            }
        });

        return false;
    });

    blockContent.on('click','.btn-display-popup',function(){
        blockPopup_displayPopup(pageBlockId);

        return false;
    });
}
function blockPopup_displayPopup(pageBlockId)
{
    var postData={
        'pageBlockId':pageBlockId
    };
    blockPopup_request_getPopupTitleAndContentsView(postData,function(response){
        if(requestOk(response)&&response.data)
        {
            var title=response.data.title;
            var contents=response.data.contents;
            var customId='dialog-custom-popup-'+pageBlockId;
            if((title!='')&&(contents!=''))
            {
                var withSaveButton=false;
                var onCloseCallback=false;
                var doNotRequireSession=true;
                var additionalCssClasses='block-popup';
                jDialog(contents,title,customId,function(id,success){
                    if(success)
                    {
                        var dialog=$('#'+id);

                        dialog.on('click','.btn-close',function(){
                            var checkbox=dialog.find('[id^="popup-do-not-show-again"]');
                            var contentId=checkbox.attr('id').replace('popup-do-not-show-again-','');
                            var doNotShowAgain=checkbox.prop('checked')?true:false;
                            if(doNotShowAgain)
                            {
                                loadJsFile('/libs/jquery-cookie/jquery.cookie.js');
                                $.cookie('popup_'+contentId,1,{
                                    'expires':10,// Expires in 10 days
                                    'path':'/'
                                });
                            }
                        });
                    }
                },withSaveButton,onCloseCallback,doNotRequireSession,additionalCssClasses);
            }
        }
    });
}
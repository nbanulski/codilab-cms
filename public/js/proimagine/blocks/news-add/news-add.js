function blockNewsAdd_request_addNewsAsGuest(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-news-add/add-news-as-guest'),doneCallback,postData);
}
function blockNewsAdd_handleCustomFunctionality(pageBlockId)
{
    var block=$('.slot-item[data-page-block-id="'+pageBlockId+'"]');
    block.off('click keydown keyup paste blur','.re-news-title')
        .on('click keydown keyup paste blur','.re-news-title',function(){
            var title=$(this).val();
            var cleanUrlInput=block.find('.re-clean-url');
            var cleanUrl=generateCleanUrlFromText(title);
            cleanUrlInput.val(cleanUrl);
            block.find('.re-clean-url-path-span').text(cleanUrl);
        });
    block.off('click keydown keyup paste blur','.re-clean-url')
        .on('click keydown keyup paste blur','.re-clean-url',function(){
            var cleanUrl=$.trim($(this).val());
            block.find('.re-clean-url-path-span').text(cleanUrl);
        });
    blockNewsAdd_attachCkeditorToContents(block.find('#re-news-contents-'+pageBlockId),'moono-light');
    block.find('.re-news-teaser').limiter(350,block.find('.re-news-teaser-length'));
    block.find('#re-news-date-'+pageBlockId).datepicker({
        format:'yyyy-mm-dd'
    });
    block.find('#re-news-start-hour-'+pageBlockId+',#re-news-end-hour-'+pageBlockId).timepicker({
        showMeridian:false
    });
    block.off('click','.btn-cancel').on('click','.btn-cancel',function(){
        var blockRootDiv=$(this).closest('.news-add');
        var btn=blockRootDiv.find('.btn-add-news');
        var form=blockRootDiv.find('.form-add-news');
        btn.removeClass('activated');
        btn.attr('data-pushed',0);
        form.hide();
    });
    block.off('click','.btn-report').on('click','.btn-report',function(){
        var blockRootDiv=$(this).closest('.news-add');
        var btn=blockRootDiv.find('.btn-add-news');
        var form=blockRootDiv.find('.form-add-news');

        /**
         * Validation.
         */
        var formInvalid=markRequiredFormFieldsWhenTheyAreEmptyOrUnselectedAndReturnResult(form);
        if(formInvalid)
        {
            return false;
        }

        /**
         * Saving.
         */
        var formData=getFormData(form);
        var postData={
            'dataPosted':true,
            'formData':formData
        };
        blockNewsAdd_request_addNewsAsGuest(function(response){
            if(requestOkAndSessionExist(response))
            {
                if(response.meta.customStatus=='NEWS_ADDED')
                {
                    renderBlockAgain(formData.pageBlockId);
                    return false;
                }
            }
        },postData);
    });
    block.off('mouseenter mouseleave','.block-content .content .news-add .item .news-edit-btn')
        .find('.block-content .content .news-add .item').each(function(){
            var item=$(this);
            var editBtn=item.find('.news-edit-btn');
            item.hover(function(){
                editBtn.show();
            },function(){
                editBtn.hide();
            });
        });
}
function blockNewsAdd_attachCkeditorToContents(selectorOrInstance,skin)
{
    loadCkEditorLibrary();

    $(selectorOrInstance).ckeditor($.extend(defaultCkeditorConfig,{
        'skin':skin,
        'removeButtons':'',
        'toolbar':[
            {
                name:'document',
                items:[
                    'Cut','Copy','PasteText','PasteFromWord',
                    'Undo','Redo','-',
                    'Find','Replace','SelectAll'
                ]
            },
            {
                name:'basicstyles',
                items:[
                    'Bold','Italic','Underline','Strike','-',
                    'Subscript','Superscript','-','RemoveFormat'
                ]
            },
            {
                name:'paragraph',
                items:[
                    'JustifyLeft','JustifyCenter',
                    'JustifyRight','JustifyBlock','-',
                    'NumberedList','BulletedList','-',
                    'Indent','Outdent','-',
                    'BidiLtr','BidiRtl','-',
                    'Link','Unlink','Anchor','-',
                    'Image','HorizontalRule','SpecialChar'
                ]
            },
            {
                name:'colors',
                items:[
                    'TextColor'
                ]
            }
        ]
    }));
}
loadJsFile('/js/admin/bootstrap-timepicker.min.js');
loadJsFile('/js/admin/moment.min.js');
loadJsFile('/libs/bootstrap-datepicker/js/bootstrap-datepicker.js');
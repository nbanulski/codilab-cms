function blockFutureEventList_attachCkeditorToContents(selectorOrInstance,skin)
{
    loadCkEditorLibrary();

    $(selectorOrInstance).ckeditor($.extend(defaultCkeditorConfig,{
        'skin':skin,
        'removeButtons':'',
        'toolbar':[
            {
                name:'document',
                items:[
                    'Cut','Copy','PasteText','PasteFromWord',
                    'Undo','Redo','-',
                    'Find','Replace','SelectAll'
                ]
            },
            {
                name:'basicstyles',
                items:[
                    'Bold','Italic','Underline','Strike','-',
                    'Subscript','Superscript','-','RemoveFormat'
                ]
            },
            {
                name:'paragraph',
                items:[
                    'JustifyLeft','JustifyCenter',
                    'JustifyRight','JustifyBlock','-',
                    'NumberedList','BulletedList','-',
                    'Indent','Outdent','-',
                    'BidiLtr','BidiRtl','-',
                    'Link','Unlink','Anchor','-',
                    'Image','HorizontalRule','SpecialChar'
                ]
            },
            {
                name:'colors',
                items:[
                    'TextColor'
                ]
            }
        ]
    }));
}
function blockFutureEventList_handlePagination(pageBlockId)
{
    var block=$('.slot-item[data-page-block-id="'+pageBlockId+'"]');
    block.find('.block-content .content .pagination')
         .off('click','li:not(.disabled):not(.active) a')
         .on('click','li:not(.disabled):not(.active) a',function(){
        var anchor=$(this);
        var text=$.trim(anchor.text());
        var pageNumber=null;
        if(text=='<<')
        {
            var activeAnchor=anchor.closest('ul').find('li.active a');
            var currentPageNumber=toInt($.trim(activeAnchor.text()));
            pageNumber=currentPageNumber-1;
        }
        else if(text=='>>')
        {
            var activeAnchor=anchor.closest('ul').find('li.active a');
            var currentPageNumber=toInt($.trim(activeAnchor.text()));
            pageNumber=currentPageNumber+1;
        }
        else
        {
            pageNumber=toInt(text);
        }

        renderBlockAgain(pageBlockId,{'pageNumber':pageNumber});

        return false;
    });
}

loadJsFile('/js/admin/bootstrap-timepicker.min.js');
loadJsFile('/js/admin/moment.min.js');
loadJsFile('/js/admin/daterangepicker.min.js');
function blockNewsDetails_request_reorderNewsImages(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-news-details/reorder-news-images'),doneCallback,postData);
}
function blockNewsDetails_request_updateNewsTitle(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-news-details/update-news-title'),doneCallback,postData);
}
function blockNewsDetails_request_updateNewsImageTitle(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-news-details/update-news-gallery-image-title'),doneCallback,postData);
}
function blockNewsDetails_request_updateNewsContents(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-news-details/update-news-contents'),doneCallback,postData);
}
function blockNewsDetails_updateNewsTitle(pageBlockId,newsId,newsTitle,doneCallback)
{
    var postData={
        'pageBlockId':pageBlockId,
        'newsId':newsId,
        'newsTitle':newsTitle,
        'currentUrl':getCurrentUrl()
    };
    blockNewsDetails_request_updateNewsTitle(postData,doneCallback);
}
function blockNewsDetails_updateNewsImageTitle(contentId,newsId,imageId,imageTitle,doneCallback)
{
    var postData={
        'contentId':contentId,
        'newsId':newsId,
        'imageId':imageId,
        'imageTitle':imageTitle
    };
    blockNewsDetails_request_updateNewsImageTitle(postData,doneCallback);
}
function blockNewsDetails_updateNewsContents(pageBlockId,newsId,newsContents,doneCallback)
{
    var postData={
        'pageBlockId':pageBlockId,
        'newsId':newsId,
        'newsContents':newsContents,
        'currentUrl':getCurrentUrl()
    };
    blockNewsDetails_request_updateNewsContents(postData,doneCallback);
}
function blockNewsDetails_refreshNewsBlocks()
{
    renderBlockAgainByBlockName('news-list');
    renderBlockAgainByBlockName('news-details');
}
function blockNewsDetails_handleCustomFunctionality(pageBlockId)
{
    var block=$('.slot-item[data-page-block-id="'+pageBlockId+'"]');
    var blockContent=block.find('.block-content > .content');
    var newsDetailsDiv=blockContent.find('.news-details');
    var newsContentsDiv=newsDetailsDiv.find('.contents');

    newsDetailsDiv.off('dblclick','> .title').on('dblclick','> .title',function(){
        var h3Tag=$(this);
        h3Tag.prop('contenteditable',true);
        h3Tag.off('blur').on('blur',function(){
            var newsId=newsDetailsDiv.attr('data-news-id');
            var newsTitle=$.trim(h3Tag.text());
            blockNewsDetails_updateNewsTitle(pageBlockId,newsId,newsTitle,function(response){
                if(requestOkAndSessionExist(response))
                {
                    var expectedStatus='UPDATED';
                    if(response.meta.customStatus==expectedStatus)
                    {
                        blockNewsDetails_refreshNewsBlocks();
                    }
                }
            });
            h3Tag.removeAttr('contenteditable');
        });
    });

    newsDetailsDiv.off('dblclick','.news-details-image-text').on('dblclick','.news-details-image-text',function(){
        var imageTitle=$(this);
        imageTitle.prop('contenteditable',true);
        imageTitle.off('blur').on('blur',function(){
            var imageTitle=$(this);
            var contentId=newsDetailsDiv.attr('data-news-list-content-id');
            var newsId=newsDetailsDiv.attr('data-news-id');
            var imageId=imageTitle.attr('data-image-id');
            var imageTitleText=$.trim(imageTitle.text());
            blockNewsDetails_updateNewsImageTitle(contentId,newsId,imageId,imageTitleText,function(response){
                if(requestOkAndSessionExist(response))
                {
                    var expectedStatus='UPDATED';
                    if(response.meta.customStatus==expectedStatus)
                    {
                        blockNewsDetails_refreshNewsBlocks();
                    }
                }
            });
            var lightBoxElement = imageTitle.parent().children('a');
            lightBoxElement.attr('title',imageTitleText);
            imageTitle.removeAttr('contenteditable');
        });
    });
    blockNewsDetails_attachSortableToGallery(newsDetailsDiv);

    newsContentsDiv.off('dblclick','> .text').on('dblclick','> .text',function(){
        loadCkEditorLibrary();

        var textDiv=$(this);
        var ckEditorId='block-news-details-text-editable-'+pageBlockId;
        textDiv.attr('id',ckEditorId);
        textDiv.prop('contenteditable',true);
        textDiv.ckeditor($.extend(defaultCkeditorConfig,{
            filebrowserImageBrowseUrl:'/libs/kcfinder/kcfinder/browse.php?type=images&cms=proimagine-cms',
            filebrowserImageUploadUrl:'/libs/kcfinder/kcfinder/upload.php?type=images&cms=proimagine-cms',
            filebrowserBrowseUrl:'/libs/kcfinder/kcfinder/browse.php?type=other-files&cms=proimagine-cms',
            filebrowserUploadUrl:'/libs/kcfinder/kcfinder/upload.php?type=other-files&cms=proimagine-cms',
            'toolbar':false
        }));

        var ckEditorInstance=getCkeditorInstanceById(ckEditorId);
        ckEditorInstance.on('blur',function(e){
            if($(document.activeElement).get(0).tagName.toLowerCase()!="iframe"){
                var editor=$(this)[0];
                if(editor)
                {
                    var id=editor.name;
                    if(id!='')
                    {
                        var newsId=newsDetailsDiv.attr('data-news-id');
                        var newsContents=null;
                        if(getCkeditorInstanceById(id))
                        {
                            newsContents=getCkeditorData(id); // CKEditor.
                        }
                        else
                        {
                            newsContents=$('#'+id).html(); // HTML5 Content-Editable element.
                        }

                        blockNewsDetails_updateNewsContents(pageBlockId,newsId,newsContents,function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                var expectedStatus='UPDATED';
                                if(response.meta.customStatus==expectedStatus)
                                {
                                    blockNewsDetails_refreshNewsBlocks();
                                }
                            }
                        });

                        destroyCkeditor(id);
                        $('#'+id).removeAttr('contenteditable');
                    }
                }
            }
        });
    });
}
function blockNewsDetails_attachSortableToGallery(newsDetailsDivInstanceOrSelector)
{
    var newsDetailsDiv=$(newsDetailsDivInstanceOrSelector);

    if(newsDetailsDiv.length<1)
    {
        return false;
    }

    var newsId=newsDetailsDiv.attr('data-news-id');
    var contentId=newsDetailsDiv.attr('data-news-list-content-id');
    var gallery=newsDetailsDiv.find('.contents .gallery');

    gallery.sortable({
        'sort':function(event,ui){
            var draggedElement=$(ui.item[0]);
            var draggedElementHeight=draggedElement.outerHeight();
            var placeholder=draggedElement.parent().find('.ui-sortable-placeholder');
            placeholder.removeClass('ui-sortable-placeholder');
            placeholder.css({
                'background':'#fcefa1',
                'border':'1px solid orange',
                'height':draggedElementHeight+'px',
                'visibility':'visible'
            });
        },
        'stop':function(event,ui){
            var parentElement=ui.item.parent();
            var images=[];
            parentElement.find('img').each(function(){
                var div=$(this);
                var imageId=div.attr('data-image-id');
                images.push(imageId);
            });

            var postData={
                'newsId':newsId,
                'contentId':contentId,
                'images':images
            };
            blockNewsDetails_request_reorderNewsImages(postData);
        }
    }).disableSelection();
}
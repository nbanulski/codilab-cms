loadPlUploadLibrary();
function blockBigBanner_request_reorderBanners(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('big-banner/reorder-banners'),doneCallback,postData);
}
function blockBigBanner_request_addBanner(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('big-banner/create-banner'),doneCallback,postData);
}
function blockBigBanner_request_editBanner(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('big-banner/update-banner'),doneCallback,postData);
}
function blockBigBanner_request_deleteBanner(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('big-banner/delete-banner'),doneCallback,postData);
}
function blockBigBanner_request_editBannerTitle(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('big-banner/update-banner-title'),doneCallback,postData);
}
function blockBigBanner_request_editBannerContents(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('big-banner/update-banner-contents'),doneCallback,postData);
}
function blockBigBanner_request_toggleBannerPublishing(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('big-banner/toggle-banner-publishing'),doneCallback,postData);
}
function blockBigBanner_handleCustomFunctionality(pageBlockId)
{
    var block=$('.slot-item[data-page-block-id="'+pageBlockId+'"]');
    var blockContent=block.find('.block-content > .content');
    var cycleSlideshow=blockContent.find('.slideshow');

    cycleSlideshow.on('cycle-initialized',function(e,o){
        blockContent.find('.big-banner-loading').hide();
    });
    cycleSlideshow.cycle({maxZ:50});

    if(isDesignModeEnabled())
    {
        cycleSlideshow.off('dblclick','.banner-title').on('dblclick','.banner-title',function(){
            cycleSlideshow.cycle('pause');
            var bannerTitle=$(this);
            var slide=bannerTitle.closest('.slide');
            var bannerId=slide.attr('data-banner-id');
            var bannerTitleEditorId='big-banner-'+bannerId;
            var bannerTitleCharacterLimit=110;
            var bannerTitleCharacterCounterValue=blockContent.find('#big-banner-title-counter-value');

            bannerTitle.attr('id',bannerTitleEditorId);
            bannerTitle.attr('contenteditable','true');
            bannerTitle.text($.trim(bannerTitle.text()));
            bannerTitle.before(
                '<div id="big-banner-title-counter">'+
                    'Możesz wprowadzić jeszcze: '+
                    '<span id="big-banner-title-counter-value">0</span> '+
                    '/ 110 znaków.'+
                '</div>'
            );
            bannerTitle.limiter(bannerTitleCharacterLimit,bannerTitleCharacterCounterValue);
            bannerTitle.off('blur').on('blur',function(){
                var postData={
                    'pageBlockId':pageBlockId,
                    'bannerId':bannerId,
                    'bannerTitle':$.trim($(this).text())
                };
                blockBigBanner_request_editBannerTitle(function()
                {
                    bannerTitle.removeAttr('contenteditable');

                    var numSlides=cycleSlideshow.find('>div').length;
                    if(numSlides>1)
                    {
                        cycleSlideshow.cycle('resume');
                    }
                    renderBlockAgain(pageBlockId);
                },postData);
            });
        });

        cycleSlideshow.off('dblclick','.banner-content').on('dblclick','.banner-content',function(){
            cycleSlideshow.cycle('pause');
            var bannerContent=$(this);
            var parentContainer=bannerContent.parent();
            var slide=bannerContent.closest('.slide');
            var bannerId=slide.attr('data-banner-id');
            var bannerContentEditorId='big-banner-'+bannerId;

            loadCkEditorLibrary();

            bannerContent.attr('id',bannerContentEditorId);
            bannerContent.attr('contenteditable','true');
            bannerContent.ckeditor($.extend(defaultCkeditorConfig,{
                'removeButtons':'',
                'toolbar':[
                    {
                        name:'document',
                        items:[
                        ]
                    },
                    {
                        name:'basicstyles',
                        items:[
                            'Bold','Italic','Underline','Strike','-','RemoveFormat'
                        ]
                    },
                    '/',
                    {
                        name:'paragraph',
                        items:[
                            'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'
                        ]
                    },
                    {
                        name:'colors',
                        items:[
                            'TextColor'
                        ]
                    },
                    {
                        name:'links',
                        items:[
                            'Link'
                        ]
                    }
                ]
            }));
            var editor=getCkeditorInstanceById(bannerContentEditorId);
            editor.on('blur',function()
            {
                if($(document.activeElement).get(0).tagName.toLowerCase()!="iframe"){
                    var editor=$(this)[0];
                    if(editor)
                    {
                        var id=editor.name;
                        if(id!='')
                        {
                            if(getCkeditorInstanceById(bannerContentEditorId))
                            {
                                var bannerContents=getCkeditorData(id); // CKEditor.
                            }
                            else
                            {
                                var bannerContents=$('#'+id).html(); // HTML5 Content-Editable element.
                            }

                            var postData={
                                'pageBlockId':pageBlockId,
                                'bannerId':bannerId,
                                'bannerContents':bannerContents
                            };
                            blockBigBanner_request_editBannerContents(function()
                            {
                                bannerContent.removeAttr('contenteditable');
                                destroyCkeditor(id);
                                $('#'+id).removeAttr('contenteditable');

                                var numSlides=cycleSlideshow.find('>div').length;
                                if(numSlides>1)
                                {
                                    cycleSlideshow.cycle('resume');
                                }
                                renderBlockAgain(pageBlockId);
                            },postData);
                        }
                    }
                }
            });
        });
    }
}
function blockBigBanner_handleManagementWindow(pageBlockId)
{
    var viewRequestGetFunction=request_getBlockManagementView;
    var viewGetCallback=null;
    var togglePublishingCallback=function(published,elementId,button)
    {
        blockBigBanner_request_toggleBannerPublishing(function(response)
        {
            if(requestOkAndSessionExist(response))
            {
                var status=response.meta.customStatus;
                if(status=='PUBLISHED')
                {

                }
                else if(status=='UNPUBLISHED')
                {

                }
            }
        },{
            'pageBlockId':pageBlockId,
            'bannerId':elementId,
            'published':published?1:0
        });
    };
    var addNewCallback=function(button){
        blockBigBanner_showBlockAddEditWindow('add','Dodaj nowy baner','Zapisz','Anuluj',{
            'pageBlockId':pageBlockId
        });
    };
    var actionCallback=function(elementId,action,button)
    {
        action=$.trim(action).toLowerCase();
        if(action=='edit')
        {
            blockBigBanner_showBlockAddEditWindow('edit','Edytuj baner','Zapisz','Anuluj',{
                'pageBlockId':pageBlockId,
                'bannerId':elementId
            });
        }
        else if(action=='delete')
        {
            jConfirm('Czy na pewno chcesz usunąć ten baner?','Potwierdź usunięcie banera','dialog-confirm-big-banner-removal',function(id,confirmed){
                if(confirmed)
                {
                    blockBigBanner_request_deleteBanner(function(response){
                        if(requestOkAndSessionExist(response))
                        {
                            if(response.meta.customStatus=='BANNER_DELETED')
                            {
                                refreshPageBlockManagementWindow(pageBlockId);
                            }
                        }
                    },{
                        'pageBlockId':pageBlockId,
                        'bannerId':elementId
                    });
                }
            });
        }
    };
    var onCloseCallback=function(button){
        renderBlockAgain(pageBlockId);
    };
    var otherSelectorOrInstance=null;
    var optionalCssClassPrefix=null;
    var otherTitlePrefix=null;
    var optionalSourcePageBlockId=null;
    var onOpenCallback=function(dialog){
        var sortable=blockBigBanner_attachSortableToManagementWindow(dialog,pageBlockId);

        dialog
            .off('mouseenter','td.actions [data-action="move"]')
            .on('mouseenter','td.actions [data-action="move"]',function(){
                if(!dialog.find('.table').hasClass('ui-sortable'))
                {
                    sortable=blockBigBanner_attachSortableToManagementWindow(dialog,pageBlockId);
                }

                sortable.sortable('option','items','tr[data-element-id]');

                return false;
            });
    };

    handlePageBlockManagementWindow(
        pageBlockId,viewRequestGetFunction,viewGetCallback,togglePublishingCallback,addNewCallback,actionCallback,
        onCloseCallback,otherSelectorOrInstance,optionalCssClassPrefix,otherTitlePrefix,optionalSourcePageBlockId,
        onOpenCallback
    );
}
function blockBigBanner_attachSortableToManagementWindow(dialog,pageBlockId)
{
    var table=dialog.find('.table');
    var sortable=table.sortable({
        'handle':'td.actions [data-action="move"]',
        'sort':function(event,ui){
            var draggedElement=$(ui.item[0]);
            var draggedElementHeight=draggedElement.outerHeight();
            var placeholder=draggedElement.parent().find('.ui-sortable-placeholder');
            placeholder.removeClass('ui-sortable-placeholder');
            placeholder.css({
                'background':'#fcefa1',
                'border':'1px solid orange',
                'height':draggedElementHeight+'px',
                'visibility':'visible'
            });
        },
        'stop':function(event,ui){
            var banners=[];
            table.find('tr[data-element-id]').each(function(){
                var bannerId=$(this).attr('data-element-id');
                banners.push(bannerId);
            });

            var postData={
                'pageBlockId':pageBlockId,
                'banners':banners
            };
            blockBigBanner_request_reorderBanners(postData,function(){

            });
        }
    });

    return sortable;
}
function blockBigBanner_showBlockAddEditWindow(type,title,btn_save_text,btn_cancel_text,additionalData)
{
    var pageBlockId=additionalData.pageBlockId;
    var expectedStatus=(type=='add')?'BANNER_SAVED':'BANNER_UPDATED';
    var operationType=(type=='add')?'new':'update';
    var postData={};
    postData=$.extend(postData,additionalData);
    request_getBlockAddEditView(function(response)
    {
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            var withSaveButton=true;
            jDialog(contents,title,'dialog-add-edit-banner',function(id,success){
                if(success)
                {
                    var dialog=$('#'+id);

                    dialog.find('#ae-big-banner-title').limiter(110,dialog.find('#ae-banner-title-count'));
                    dialog.on('click keydown keyup paste blur','#ae-big-banner-title',function()
                    {
                        var cleanUrlInput=dialog.find('#ae-clean-url');
                        if((type=='add')||((type=='edit')&&(cleanUrlInput.val()!='')))
                        {
                            var title=$(this).val();
                            var cleanUrl=generateCleanUrlFromText(title);
                            cleanUrlInput.val(cleanUrl);
                            dialog.find('#ae-clean-url-path-span').text(cleanUrl);
                        }
                    });
                    dialog.on('click change','#ae-banner-url-address-type',function(){
                        var urlAddressType=$.trim($(this).find('option:selected').val());
                        if(urlAddressType=='0')
                        {
                            dialog.find('#ae-banner-internal-page-id').show();
                            dialog.find('#ae-banner-external-url').hide();
                            dialog.find('#ae-banner-url-form-group').show();
                        }
                        else if(urlAddressType=='1')
                        {
                            dialog.find('#ae-banner-internal-page-id').hide();
                            dialog.find('#ae-banner-external-url').show();
                            dialog.find('#ae-banner-url-form-group').show();
                        }
                        else
                        {
                            dialog.find('#ae-banner-url-form-group').hide();
                            dialog.find('#ae-banner-internal-page-id').hide();
                            dialog.find('#ae-banner-external-url').hide();
                        }
                    });
                    dialog.on('click change','#ae-banner-internal-page-id',function(){
                        var urlAddressType=dialog.find('#ae-banner-url-address-type option:selected').val();
                        var isInternalUrl=(urlAddressType=='0');
                        if(isInternalUrl)
                        {
                            var internalPageIdDropdown=dialog.find('#ae-banner-internal-page-id');
                            var selectedOption=internalPageIdDropdown.find('option:selected');

                            // Set banner title the same as selected internal page title:
                            var selectedInternalPageTitle=selectedOption.attr('data-title');
                            dialog.find('#ae-banner-title').val($.trim(selectedInternalPageTitle));
                        }
                    });

                    attachCkEditorToDomElement(
                        dialog.find('#ae-big-banner-contents'),
                        true,
                        'moono-dark'
                    );

                    dialog.on('click','.btn-save',function()
                    {
                        var uniqueHashForFileNameInput=dialog.find('#ae-banner-unique-hash-for-file-name');
                        var uniqueHashForFileName=uniqueHashForFileNameInput.val();
                        if(uniqueHashForFileName!='')
                        {
                            var form=dialog.find('.form');
                            var formData=getFormData(form);
                            var postData=$.extend({
                                'formData':formData
                            },additionalData);
                            if(operationType=='update')
                            {
                                blockBigBanner_request_editBanner(function(response)
                                {
                                     if(requestOkAndSessionExist(response))
                                     {
                                         if(response.meta.customStatus==expectedStatus)
                                         {
                                             dialog.modal('hide');
                                             refreshPageBlockManagementWindow(pageBlockId);
                                         }
                                     }
                                 },postData);
                            }
                            else
                            {
                                blockBigBanner_request_addBanner(function(response)
                                {
                                    if(requestOkAndSessionExist(response))
                                    {
                                        if(response.meta.customStatus==expectedStatus)
                                        {
                                            dialog.modal('hide');
                                            refreshPageBlockManagementWindow(pageBlockId);
                                        }
                                    }
                                },postData);
                            }
                        }
                        else
                        {
                            jDialog("Prześlij zdjęcie","Błąd");
                            console.log("Attachment not found - Please upload a file");
                        }
                    });
                }
            },withSaveButton);
        }
    },postData);
}
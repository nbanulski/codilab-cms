function saveHeaderBlockContents(pageBlockId,contentContainerId)
{
    if((pageBlockId<=0)||!contentContainerId)
    {
        return;
    }

    var id=contentContainerId;
    var blockContent=null;

    if(getCkeditorInstanceById(id))
    {
        blockContent=getCkeditorData(id); // CKEditor.
    }
    else
    {
        blockContent=$('#'+id).html(); // HTML5 Content-Editable element.
    }

    makePostRequest('/pol/block/update',function(response){
        if(requestOkAndSessionExist(response))
        {
            if(response.meta.customStatus=='BLOCK_CONTENT_SAVED')
            {
                console.log('Block content successfully saved.');
            }
        }
    },{
        'pageBlockId':pageBlockId,
        'blockContent':blockContent
    });
}
function attachEventsToHeaderBlock(pageBlockId)
{
    $('.slot-item[data-page-block-id="'+pageBlockId+'"] .block-content .content').on('dblclick','.block-header-content',function(){
        loadCkEditorLibrary();

        var blockHeaderContent=$(this);
        var pageBlockId=blockHeaderContent.closest('.slot-item').attr('data-page-block-id');
        var contentContainerId='block-content-editable-'+pageBlockId;
        blockHeaderContent.attr('id',contentContainerId);
        blockHeaderContent.attr('contenteditable','true');
        blockHeaderContent.ckeditor(defaultCkeditorConfig);

        var editor=getCkeditorInstanceById(contentContainerId);
        editor.on('blur',function(e){
            if($(document.activeElement).get(0).tagName.toLowerCase()!="iframe"){
                var editor=$(this)[0];
                if(editor)
                {
                    var id=editor.name;
                    if(id!='')
                    {
                        saveHeaderBlockContents(pageBlockId,contentContainerId);

                        destroyCkeditor(id);
                        $('#'+id).removeAttr('contenteditable');
                    }
                }
            }
        });
    });
}
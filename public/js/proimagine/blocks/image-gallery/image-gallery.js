loadPlUploadLibrary();
function blockImageGallery_request_reorderImages(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-image-gallery/reorder-images'),doneCallback,postData);
}
function blockImageGallery_request_toggleItemPublishing(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-image-gallery/toggle-image-publishing'),doneCallback,postData);
}
function blockImageGallery_request_addEditItem(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-image-gallery/add-edit-image'),doneCallback,postData);
}
function blockImageGallery_request_deleteItem(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-image-gallery/delete-image'),doneCallback,postData);
}
function blockImageGallery_handleCustomFunctionality(pageBlockId)
{
    var block=$('.slot-item[data-page-block-id="'+pageBlockId+'"]');
    var blockContent=block.find('.block-content > .content');
    var imageGallery=blockContent.find('.image-gallery');

    imageGallery.sortable({
        'handle':'img',
        'sort':function(event,ui){
            var draggedElement=$(ui.item[0]);
            var draggedElementHeight=draggedElement.outerHeight();
            var placeholder=draggedElement.parent().find('.ui-sortable-placeholder');
            placeholder.removeClass('ui-sortable-placeholder');
            placeholder.css({
                'background':'#fcefa1',
                'border':'1px solid orange',
                'height':draggedElementHeight+'px',
                'visibility':'visible'
            });
        },
        'stop':function(event,ui){
            var images=[];
            imageGallery.find('.thumb').each(function(){
                var imageId=$(this).attr('data-image-id');
                images.push(imageId);
            });

            var postData={
                'pageBlockId':pageBlockId,
                'images':images
            };
            blockImageGallery_request_reorderImages(function(){

            },postData);
        }
    });

    imageGallery.on('click','.btn-show-hide-more-images',function(){
        imageGallery.find('.thumb.limited').toggle();
    });
}
function blockImageGallery_showAddEditWindow(type,title,btn_save_text,btn_cancel_text,additionalData)
{
    var pageBlockId=additionalData.pageBlockId;
    var postData={};
    postData=$.extend(postData,additionalData);
    request_getBlockAddEditView(function(response){
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            var withSaveButton=true;
            jDialog(contents,title,'dialog-add-edit-image',function(id,success){
                if(success)
                {
                    var dialog=$('#'+id);

                    dialog.on('click','.btn-save',function(){
                        var form=dialog.find('.form');
                        var formData=getFormData(form);
                        var postData=$.extend({
                            'dataPosted':true,
                            'formData':formData
                        },additionalData);
                        blockImageGallery_request_addEditItem(function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                var expectedStatus=(type=='add')?'IMAGE_ADDED':'IMAGE_MODIFIED';
                                if(response.meta.customStatus==expectedStatus)
                                {
                                    dialog.modal('hide');
                                    refreshPageBlockManagementWindow(pageBlockId);
                                }
                            }
                        },postData);
                    });
                }
            },withSaveButton);
        }
    },postData);
}
function blockImageGallery_handleManagementWindow(pageBlockId)
{
    var viewRequestGetFunction=request_getBlockManagementView;
    var viewGetCallback=null;
    var togglePublishingCallback=function(published,elementId,button){
        blockImageGallery_request_toggleItemPublishing(function(response){
            if(requestOkAndSessionExist(response))
            {
                var status=response.meta.customStatus;
                if(status=='PUBLISHED')
                {

                }
                else if(status=='UNPUBLISHED')
                {

                }
            }
        },{
            'pageBlockId':pageBlockId,
            'imageId':elementId,
            'published':published?1:0
        });
    };
    var addNewCallback=function(button){
        blockImageGallery_showAddEditWindow('add','Dodaj nowy obrazek','Zapisz','Anuluj',{
            'pageBlockId':pageBlockId
        });
    };
    var actionCallback=function(elementId,action,button)
    {
        action=$.trim(action).toLowerCase();
        if(action=='edit')
        {
            blockImageGallery_showAddEditWindow('edit','Edytuj obrazek','Zapisz','Anuluj',{
                'pageBlockId':pageBlockId,
                'imageId':elementId
            });
        }
        else if(action=='delete')
        {
            var title='Potwierdź usunięcie obrazka';
            var contents='Czy na pewno chcesz usunąć ten obrazek?';
            var id='dialog-confirm-image-removal';
            jConfirm(contents,title,id,function(id,confirmed){
                if(confirmed)
                {
                    var postData={
                        'pageBlockId':pageBlockId,
                        'imageId':elementId
                    };
                    blockImageGallery_request_deleteItem(function(response){
                        if(requestOkAndSessionExist(response))
                        {
                            if(response.meta.customStatus=='IMAGE_DELETED')
                            {
                                refreshPageBlockManagementWindow(pageBlockId);
                            }
                        }
                    },postData);
                }
            });
        }
    };
    var onCloseCallback=function(button){
        renderBlockAgain(pageBlockId);
    };
    var otherSelectorOrInstance=null;
    var optionalCssClassPrefix=null;
    var otherTitlePrefix=null;
    var optionalSourcePageBlockId=null;
    var onOpenCallback=function(dialog){
        var sortable=blockImageGallery_attachSortableToManagementWindow(dialog,pageBlockId);

        dialog
            .off('mouseenter','td.actions [data-action="move"]')
            .on('mouseenter','td.actions [data-action="move"]',function(){
                if(!dialog.find('.table').hasClass('ui-sortable'))
                {
                    sortable=blockImageGallery_attachSortableToManagementWindow(dialog,pageBlockId);
                }

                sortable.sortable('option','items','tr[data-element-id]');

                return false;
            });
    };
    handlePageBlockManagementWindow(
        pageBlockId,viewRequestGetFunction,viewGetCallback,togglePublishingCallback,addNewCallback,actionCallback,
        onCloseCallback,otherSelectorOrInstance,optionalCssClassPrefix,otherTitlePrefix,optionalSourcePageBlockId,
        onOpenCallback
    );
}
function blockImageGallery_attachSortableToManagementWindow(dialog,pageBlockId)
{
    var table=dialog.find('.table');
    var sortable=table.sortable({
        'handle':'td.actions [data-action="move"]',
        'sort':function(event,ui){
            var draggedElement=$(ui.item[0]);
            var draggedElementHeight=draggedElement.outerHeight();
            var placeholder=draggedElement.parent().find('.ui-sortable-placeholder');
            placeholder.removeClass('ui-sortable-placeholder');
            placeholder.css({
                'background':'#fcefa1',
                'border':'1px solid orange',
                'height':draggedElementHeight+'px',
                'visibility':'visible'
            });
        },
        'stop':function(event,ui){
            var images=[];
            table.find('tr[data-element-id]').each(function(){
                var imageId=$(this).attr('data-element-id');
                images.push(imageId);
            });

            var postData={
                'pageBlockId':pageBlockId,
                'images':images
            };
            blockImageGallery_request_reorderImages(function(){

            },postData);
        }
    });

    return sortable;
}
loadPlUploadLibrary();
function blockEventList_request_addEditEvent(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-event-list/add-edit-event'),doneCallback,postData);
}
function blockEventList_request_addEventAsGuest(postData,doneCallback)
{
    var doNotRequireSession=true;
    makePostRequest(makeCmsUrl('block-event-list/add-event-as-guest'),doneCallback,postData,doNotRequireSession);
}
function blockEventList_request_deleteEvent(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-event-list/delete-event'),doneCallback,postData);
}
function blockEventList_request_toggleEventPublishing(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-event-list/toggle-event-publishing'),doneCallback,postData);
}
function blockEventList_request_deleteEventImage(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-event-list/delete-event-image'),doneCallback,postData);
}
function blockEventList_handleCustomFunctionality(pageBlockId,optionalSourcePageBlockId,translations)
{
    var sourcePageBlockId=pageBlockId;
    if((typeof optionalSourcePageBlockId!='undefined')&&(optionalSourcePageBlockId>0))
    {
        sourcePageBlockId=toInt(optionalSourcePageBlockId);
    }

    var block=$('.slot-item[data-page-block-id="'+pageBlockId+'"]');
    var blockContent=block.find('.block-content > .content');
    var formAddEvent=blockContent.find('.form-add-event');

    if (isDesignModeEnabled())
    {
        blockContent.find('.item').hover(function(){
            $(this).find('.event-edit-btn').css('display','inline-block');
        },function(){
            $(this).find('.event-edit-btn').css('display','none');
        });

        blockContent.off('click','.item .event-edit-btn').on('click','.item .event-edit-btn',function(){
            var postData={
                'pageBlockId':sourcePageBlockId,
                'eventId':$(this).closest('.item').attr('data-event-id')
            };
            blockEventList_showAddEditWindow('edit','Edytuj wydarzenie','Zapisz','Anuluj',postData,true);
        });
    }

    blockContent.on('click','.btn-add-event',function(){
        var btn=$(this);
        var form=blockContent.find('.form-add-event');
        var pushed=btn.attr('data-pushed')==1?true:false;
        if(pushed)
        {
            btn.removeClass('activated');
            btn.attr('data-pushed',0);
            form.hide();
        }
        else
        {
            btn.addClass('activated');
            btn.attr('data-pushed',1);
            form.show();
        }

        return false;
    });

    var attachKcFinder=true;
    /*attachCkEditorToDomElement(
        formAddEvent.find('#re-event-contents'),
        attachKcFinder
    );*/

    formAddEvent.find('#re-event-day-range').daterangepicker({
        format:'YYYY-MM-DD'
    });
    formAddEvent.find('#re-event-start-hour,#re-event-end-hour').timepicker({
        showMeridian:false
    });
    formAddEvent.on('click','.btn-cancel',function(){
        var btn=blockContent.find('.btn-add-event');
        var form=blockContent.find('.form-add-event');
        btn.removeClass('activated');
        btn.attr('data-pushed',0);
        form.hide();
    });
    formAddEvent.on('click','.btn-report',function(){
        var btn=blockContent.find('.btn-add-event');
        var form=blockContent.find('.form-add-event');
        var formData=getFormData(form);
        var eventImagesOrder=[];
        blockContent.find('#re-event-image-form-group [data-image-number]').each(function(){
            var div=$(this);
            var uniqueIdentifier=div.attr('data-unique-identifier');
            var uniqueHashForFileName=
                div.find('#iu-unique-hash-for-file-name-'+uniqueIdentifier).val();
            eventImagesOrder.push(uniqueHashForFileName);
        });
        formData=$.extend(formData,{
            'eventImagesOrder':eventImagesOrder
        });
        var postData={
            'dataPosted':true,
            'formData':formData,
            'pageBlockId':pageBlockId
        };
        blockEventList_request_addEventAsGuest(postData,function(response){
            if(requestOk(response))
            {
                if(response.meta.customStatus=='EVENT_ADDED')
                {
                    btn.removeClass('activated');
                    btn.attr('data-pushed',0);
                    form.hide();
                }
            }
        });
    });

    blockContent.on('click','.btn-sign-up-for-event',function(){
        var eventListPageBlockId=pageBlockId;
        if(optionalSourcePageBlockId>0)
        {
            eventListPageBlockId=optionalSourcePageBlockId;
        }

        var eventId=$(this).closest('[data-event-id]').attr('data-event-id');
        var postData={
            'pageBlockId':eventListPageBlockId,
            'eventId':eventId
        };
        blockEventList_request_addAndGetSignUpFormViewForEventSignUpBtn(this,postData,function(signUpForm){
            if(signUpForm&&signUpForm.length)
            {
                signUpForm.slideDown();
            }
        },translations);
    });
}
function blockEventList_attachCkeditorToContents(selectorOrInstance,skin)
{
    loadCkEditorLibrary();

    $(selectorOrInstance).ckeditor($.extend(defaultCkeditorConfig,{
        'skin':skin,
        'removeButtons':'',
        'toolbar':[
            {
                name:'document',
                items:[
                    'Cut','Copy','PasteText','PasteFromWord',
                    'Undo','Redo','-',
                    'Find','Replace','SelectAll'
                ]
            },
            {
                name:'basicstyles',
                items:[
                    'Bold','Italic','Underline','Strike','-',
                    'Subscript','Superscript','-','RemoveFormat'
                ]
            },
            {
                name:'paragraph',
                items:[
                    'JustifyLeft','JustifyCenter',
                    'JustifyRight','JustifyBlock','-',
                    'NumberedList','BulletedList','-',
                    'Indent','Outdent','-',
                    'BidiLtr','BidiRtl','-',
                    'Link','Unlink','Anchor','-',
                    'Image','HorizontalRule','SpecialChar'
                ]
            },
            {
                name:'colors',
                items:[
                    'TextColor'
                ]
            }
        ]
    }));
}
function blockEventList_handlePagination(pageBlockId)
{
    var block=$('.slot-item[data-page-block-id="'+pageBlockId+'"]');
    block.find('.block-content .content .pagination')
         .off('click','li:not(.disabled):not(.active) a')
         .on('click','li:not(.disabled):not(.active) a',function(){
        var anchor=$(this);
        var text=$.trim(anchor.text());
        var pageNumber=null;
        if(text=='<<')
        {
            var activeAnchor=anchor.closest('ul').find('li.active a');
            var currentPageNumber=toInt($.trim(activeAnchor.text()));
            pageNumber=currentPageNumber-1;
        }
        else if(text=='>>')
        {
            var activeAnchor=anchor.closest('ul').find('li.active a');
            var currentPageNumber=toInt($.trim(activeAnchor.text()));
            pageNumber=currentPageNumber+1;
        }
        else
        {
            pageNumber=toInt(text);
        }

        renderBlockAgain(pageBlockId,{'pageNumber':pageNumber});

        return false;
    });
}
function blockEventList_showAddEditWindow(
    type,title,btn_save_text,btn_cancel_text,additionalData,windowCalledFromInlineEdit
)
{
    var pageBlockId=additionalData.pageBlockId;
    var postData={};
    postData=$.extend(postData,additionalData);
    request_getBlockAddEditView(function(response){
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            var withSaveButton=true;
            jDialog(contents,title,'dialog-add-edit-event',function(id,success){
                if(success)
                {
                    var dialog=$('#'+id);
                    dialog.on('click change','#ae-event-categories',function(){
                        var categoryId=$.trim($(this).find('option:selected').val());
                    });
                    dialog.on('click keydown keyup paste blur','#ae-event-title',function(){
                        var cleanUrlInput=dialog.find('#ae-event-clean-url');
                        var title=$(this).val();
                        var cleanUrl=generateCleanUrlFromText(title);
                        cleanUrlInput.val(cleanUrl);
                        dialog.find('#ae-event-clean-url-path-span').text(cleanUrl);
                    });
                    dialog.on('click keydown keyup paste blur','#ae-event-clean-url',function(){
                        var cleanUrl= $.trim($(this).val());
                        dialog.find('#ae-event-clean-url-path-span').text(cleanUrl);
                    });
                    dialog.find('#ae-event-day-range').daterangepicker({
                        format:'YYYY-MM-DD'
                    });
                    dialog.find('#ae-event-start-hour,#ae-event-end-hour').timepicker({
                        showMeridian:false
                    });
                    //blockEventList_attachCkeditorToContents(dialog.find('#ae-event-contents'),'bootstrapck');

                    attachCkEditorToDomElement(
                        dialog.find('#ae-event-contents'),
                        true
                    );

                    dialog.find('#ae-event-teaser').limiter(350,dialog.find('#ae-event-teaser-length'));

                    var attachKcFinder=true;
                    var additionalEventData=dialog.find('[id^="ae-event-adata-"]:not(:first)');
                    additionalEventData.each(function(){
                        attachCkEditorToDomElement(
                            $(this),
                            attachKcFinder
                        );
                    });

                    dialog.on('click','[name="eventIsSigningUpEnabled"]',function(){
                        var input=$(this);
                        var signingUpEnabled=(input.val()==1)?true:false;
                        var contactPersonDataSection=dialog.find('.contact-person-data');
                        if(signingUpEnabled)
                        {
                            contactPersonDataSection.slideDown();
                        }
                        else
                        {
                            contactPersonDataSection.slideUp();
                        }
                    });

                    dialog.on('click','#ae-approve',function(){
                        var checked=$(this).prop('checked');
                        var mailSection=dialog.find('#ae-approve-mail-contents');
                        if(checked)
                        {
                            mailSection.show();
                        }
                        else
                        {
                            mailSection.hide();
                        }
                    });

                    dialog.on('click','.btn-save',function(){
                        var form=dialog.find('.form');
                        var formData=getFormData(form);
                        var eventImagesOrder=[];
                        dialog.find('#ae-event-image-form-group [data-image-number]').each(function(){
                            var div=$(this);
                            var uniqueIdentifier=div.attr('data-unique-identifier');
                            var uniqueHashForFileName=
                                div.find('#iu-unique-hash-for-file-name-'+uniqueIdentifier).val();
                            eventImagesOrder.push(uniqueHashForFileName);
                        });
                        formData=$.extend(formData,{
                            'eventImagesOrder':eventImagesOrder
                        });
                        var postData=$.extend({
                            'dataPosted':true,
                            'formData':formData
                        },additionalData);
                        blockEventList_request_addEditEvent(postData,function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                var expectedStatus=(type=='add')?'EVENT_ADDED':'EVENT_MODIFIED';
                                if(response.meta.customStatus==expectedStatus)
                                {
                                    dialog.modal('hide');
                                    refreshPageBlockManagementWindow(pageBlockId);
                                    if (windowCalledFromInlineEdit)
                                    {
                                        renderBlockAgainByBlockName('event-list');
                                        renderBlockAgainByBlockName('future-event-list');
                                        renderBlockAgainByBlockName('old-event-list');
                                        renderBlockAgainByBlockName('event-details');
                                    }
                                }
                            }
                        });
                    });
                }
            },
            withSaveButton,
            function(){
                if(windowCalledFromInlineEdit)
                {
                    renderBlockAgainByBlockName('event-list');
                    renderBlockAgainByBlockName('future-event-list');
                    renderBlockAgainByBlockName('old-event-list');
                    renderBlockAgainByBlockName('event-details');
                }
            });
        }
    },postData);
}
function blockEventList_handleManagementWindow(pageBlockId,optionalSourcePageBlockId)
{
    var sourcePageBlockId=pageBlockId;
    if((typeof optionalSourcePageBlockId!='undefined')&&(optionalSourcePageBlockId>0))
    {
       sourcePageBlockId=toInt(optionalSourcePageBlockId);
    }

    var viewRequestGetFunction=request_getBlockManagementView;
    var viewGetCallback=null;
    var togglePublishingCallback=function(published,elementId,button){
        var postData={
            'pageBlockId':sourcePageBlockId,
            'eventId':elementId,
            'published':published?1:0
        };
        blockEventList_request_toggleEventPublishing(postData,function(response){
            if(requestOkAndSessionExist(response))
            {
                var status=response.meta.customStatus;
                if(status=='PUBLISHED')
                {

                }
                else if(status=='UNPUBLISHED')
                {

                }
            }
        });
    };
    var addNewCallback=function(button){
        blockEventList_showAddEditWindow('add','Dodaj nowe wydarzenie','Zapisz','Anuluj',{
            'pageBlockId':sourcePageBlockId
        });
    };
    var actionCallback=function(elementId,action,button)
    {
        action=$.trim(action).toLowerCase();
        if(action=='edit')
        {
            blockEventList_showAddEditWindow('edit','Edytuj wydarzenie','Zapisz','Anuluj',{
                'pageBlockId':sourcePageBlockId,
                'eventId':elementId
            });
        }
        else if(action=='delete')
        {
            jConfirm('Czy na pewno chcesz usunąć to wydarzenie?','Potwierdź usunięcie wydarzenia','dialog-confirm-event-removal',function(id,confirmed){
                if(confirmed)
                {
                    var postData={
                        'pageBlockId':sourcePageBlockId,
                        'eventId':elementId
                    };
                    blockEventList_request_deleteEvent(postData,function(response){
                        if(requestOkAndSessionExist(response))
                        {
                            if(response.meta.customStatus=='EVENT_DELETED')
                            {
                                refreshPageBlockManagementWindow(pageBlockId);
                            }
                        }
                    });
                }
            });
        }
    };
    var onCloseCallback=function(button){
        renderBlockAgainByBlockName('event-list');
        renderBlockAgainByBlockName('future-event-list');
        renderBlockAgainByBlockName('old-event-list');
        renderBlockAgainByBlockName('event-details');
    };

    var otherSelectorOrInstance=null;
    var optionalCssClassPrefix=null;
    var otherTitlePrefix=null;
    handlePageBlockManagementWindow(
        pageBlockId,viewRequestGetFunction,viewGetCallback,togglePublishingCallback,addNewCallback,
        actionCallback,onCloseCallback,otherSelectorOrInstance,optionalCssClassPrefix,otherTitlePrefix,
        sourcePageBlockId
    );
}
function blockEventList_handleFilterInManagementWindow(pageBlockId)
{
    var dialog=$('#dialog-block-management');
    dialog.on('change','#events-management-filter',function(){
        var activeAnchor=dialog.find('.pagination li.active a');
        var currentPageNumber=toInt($.trim(activeAnchor.text()));
        var itemCountPerPage=dialog.find('.icpp-dropdown option:selected').val();
        var filterEvents=$(this).val();
        var postData={
            'pageBlockId':pageBlockId,
            'pageNumber':currentPageNumber,
            'itemCountPerPage':itemCountPerPage,
            'additionalData':{
                'filterEvents':filterEvents
            }
        }
        request_getBlockManagementView(function(response){
            if(requestOkAndSessionExist(response))
            {
                dialog.find('.modal-content .modal-body').html(response.data);
            }
        },postData);
    });
}
/*function blockEventList_attachSortableToAddEditWindow(eventImageFormGroupInstanceOrSelector,eventId)
{
    var eventImageFormGroup=$(eventImageFormGroupInstanceOrSelector);

    if(eventImageFormGroup.length<1)
    {
        return false;
    }

    eventImageFormGroup.sortable({
        'items':'[data-image-number]',
        'handle':'> img',
        'cancel':'[data-image-number]:has(.ae-event-image-is-main:checked)',
        'sort':function(event,ui){
            var draggedElement=$(ui.item[0]);
            var draggedElementHeight=draggedElement.outerHeight();
            var placeholder=draggedElement.parent().find('.ui-sortable-placeholder');
            placeholder.removeClass('ui-sortable-placeholder');
            placeholder.css({
                'background':'#fcefa1',
                'border':'1px solid orange',
                'height':draggedElementHeight+'px',
                'visibility':'visible'
            });
        }
    }).disableSelection();
}*/
function blockEventList_handleSettingsFunctionality(pageBlockId)
{
    var dialogBlockSettings=$('.modal#dialog-block-settings');
    var settingsForm=dialogBlockSettings.find('.form[data-page-block-id="'+pageBlockId+'"]');

    attachCkEditorToDomElement(
        settingsForm.find('#block-event-list-user-signed-up-for-event-user-mail-contents'),
        false
    );
    attachCkEditorToDomElement(
        settingsForm.find('#block-event-list-user-signed-up-for-event-admin-mail-contents'),
        false
    );
}
loadJsFile('/js/admin/bootstrap-timepicker.min.js');
loadJsFile('/js/admin/moment.min.js');
loadJsFile('/js/admin/daterangepicker.min.js');
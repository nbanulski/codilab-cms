function blockEventList_request_getCategoriesManagementView(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-event-list/get-categories-management-view'),doneCallback,postData);
}
function blockEventList_request_toggleCategoryPublishing(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-event-list/toggle-category-publishing'),doneCallback,postData);
}
function blockEventList_request_getAddEditCategoryView(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-event-list/get-add-edit-category-view'),doneCallback,postData);
}
function blockEventList_request_addEditCategory(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-event-list/add-edit-category'),doneCallback,postData);
}
function blockEventList_request_deleteCategory(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-event-list/delete-category'),doneCallback,postData);
}
function blockEventList_refreshCategoriesManagementWindow(pageBlockId)
{
    blockEventList_request_getCategoriesManagementView(function(response){
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            if(contents!='')
            {
                $('#dialog-block-categories-management .modal-body').html(contents);
            }
        }
    },{
        'pageBlockId':pageBlockId
    });
}
function blockEventList_showAddEditCategoryWindow(type,title,btn_save_text,btn_cancel_text,additionalData)
{
    var pageBlockId=additionalData.pageBlockId;
    var expectedStatus=(type=='add')?'EVENT_CATEGORY_ADDED':'EVENT_CATEGORY_MODIFIED';
    var postData={};
    postData=$.extend(postData,additionalData);
    blockEventList_request_getAddEditCategoryView(function(response){
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            var withSaveButton=true;
            jDialog(contents,title,'dialog-add-edit-category',function(id,success){
                if(success)
                {
                    var dialog=$('#'+id);
                    dialog.on('click','.btn-save',function(){
                        var form=dialog.find('.form');
                        var formData=getFormData(form);
                        var postData=$.extend({
                            'dataPosted':true,
                            'formData':formData
                        },additionalData);
                        blockEventList_request_addEditCategory(function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                if(response.meta.customStatus==expectedStatus)
                                {
                                    dialog.modal('hide');
                                    blockEventList_refreshCategoriesManagementWindow(pageBlockId);
                                }
                            }
                        },postData);
                    });
                }
            },withSaveButton);
        }
    },postData);
}
function blockEventList_handleCategoriesManagementWindow(pageBlockId)
{
    var viewRequestGetFunction=blockEventList_request_getCategoriesManagementView;
    var viewGetCallback=null;
    var togglePublishingCallback=function(published,elementId,button){
        blockEventList_request_toggleCategoryPublishing(function(response){
            if(requestOkAndSessionExist(response))
            {
                var status=response.meta.customStatus;
                if(status=='PUBLISHED')
                {

                }
                else if(status=='UNPUBLISHED')
                {

                }
            }
        },{
            'pageBlockId':pageBlockId,
            'categoryId':elementId,
            'published':published?1:0
        });
    };
    var addNewCallback=function(button){
        blockEventList_showAddEditCategoryWindow('add','Dodaj nową kategorię','Zapisz','Anuluj',{
            'pageBlockId':pageBlockId
        });
    };
    var actionCallback=function(elementId,action,button){
        action=$.trim(action).toLowerCase();
        if(action=='edit')
        {
            blockEventList_showAddEditCategoryWindow('edit','Edytuj kategorię','Zapisz','Anuluj',{
                'pageBlockId':pageBlockId,
                'categoryId':elementId
            });
        }
        else if(action=='delete')
        {
            jConfirm('Czy na pewno chcesz usunąć tę kategorię?','Potwierdź usunięcie kategorii','dialog-confirm-category-removal',function(id,confirmed){
                if(confirmed)
                {
                    blockEventList_request_deleteCategory(function(response){
                        if(requestOkAndSessionExist(response))
                        {
                            if(response.meta.customStatus=='EVENT_CATEGORY_DELETED')
                            {
                                blockEventList_refreshCategoriesManagementWindow(pageBlockId);
                                refreshPageBlockManagementWindow(pageBlockId);
                            }
                        }
                    },{
                        'pageBlockId':pageBlockId,
                        'categoryId':elementId
                    });
                }
            });
        }
    };
    var onCloseCallback=function(button){
        refreshPageBlockManagementWindow(pageBlockId);
    };
    var otherSelectorOrInstance=$('#dialog-block-management .btn-manage-categories');
    var optionalCssClassPrefix='categories-';
    var otherTitlePrefix='Kategorie';
    handlePageBlockManagementWindow(
        pageBlockId,
        viewRequestGetFunction,
        viewGetCallback,
        togglePublishingCallback,
        addNewCallback,
        actionCallback,
        onCloseCallback,
        otherSelectorOrInstance,
        optionalCssClassPrefix,
        otherTitlePrefix
    );
}
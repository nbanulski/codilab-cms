function blockEventList_request_signUpForEvent(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-event-list/sign-up-for-event'),doneCallback,postData);
}
function blockEventList_handleSignUpFormFunctionality(signUpFormSelectorOrInstance,pageBlockId,postData,translations)
{
    var signUpForm=$(signUpFormSelectorOrInstance);

    signUpForm.on('click','.btn-cancel',function(){
        $(this).closest('.sign-up-form').slideUp();
    });

    signUpForm.on('click','[name="signUpType"]',function(){
        var input=$(this);
        var type=input.val();
        var institutionDataSection=input.closest('.sign-up-form').find('.institution-data');
        if(type=='institution')
        {
            institutionDataSection.find('> .form-group').addClass('required');
            institutionDataSection.find('[name="signUpInstitutionName"]').addClass('required');
            institutionDataSection.slideDown();
        }
        else
        {
            institutionDataSection.slideUp();
            institutionDataSection.find('[name="signUpInstitutionName"]').removeClass('required');
            institutionDataSection.find('> .form-group').removeClass('required');
        }
    });

    var previousPostData=postData;
    var postDataSerialized=toJson(previousPostData);
    signUpForm.find('.btn-send').data('postDataInJson',postDataSerialized);

    signUpForm.on('click','.btn-send',function(){
        var btn=$(this);
        var form=btn.closest('.sign-up-form');
        var formInvalid=markRequiredFormFieldsWhenTheyAreEmptyOrUnselectedAndReturnResult(form);
        if(formInvalid)
        {
            return false;
        }
        var formData=getFormData(form);
        var postData=fromJson($(this).data('postDataInJson'));
        var postData=$.extend({
            'pageBlockId':pageBlockId,
            'formData':formData
        },postData);
        blockEventList_request_signUpForEvent(postData,function(response){
            var message=translations.messageFailedToSignUpForEvent;
            var messageCssClass='text-danger';
            if(requestOk(response))
            {
                if(response.meta.customStatus=='SUBSCRIPTION_SAVED')
                {
                    message=translations.messageSuccessfullySignedUpForEvent;
                    messageCssClass='text-success';
                    form.slideUp();

                    var dialog=$('#dialog-users-signed-up-for-event');
                    if(dialog.length>0)
                    {
                        var postData={
                            'additionalData':{
                                'contentId':previousPostData.contentId,
                                'eventId':previousPostData.eventId
                            }
                        };
                        request_getListOfUsersSignedUpForEventView(function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                dialog.find('.modal-content .modal-body').html(
                                    response.data
                                );
                            }
                        },postData);
                        request_getEventListView(function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                var dialog=$('#dialog-event-list');
                                dialog.find('.modal-content .modal-body').html(
                                    response.data
                                );
                            }
                        });
                    }
                }
                else if (response.meta.customStatus=='USER_ALREADY_SIGNED_UP_FOR_EVENT')
                {
                    message=translations.messageAlreadySignedUpForEvent;
                    messageCssClass='text-info';
                }
            }
            var messageContainer=form.closest('.sign-up').find('.message');
            messageContainer.text(message);
            messageContainer.removeClass('text-success text-info text-danger').addClass(messageCssClass);
            btn.text(translations.sendAgain);
        });
    });
}
function blockEventList_request_getSignUpFormView(postData,doneCallback,translations)
{
    var body=$('body');
    var id='sign-up-form-view-template';
    var signUpFormViewTemplate=body.find('#'+id);
    if(!signUpFormViewTemplate.length||(postData.subscriptionId>0))
    {
        var doNotRequireSession=true;
        makePostRequest(makeCmsUrl('block-event-list/get-sign-up-form-view'),function(response){
            if(requestOk(response))
            {
                signUpFormViewTemplate=$(response.data).attr('id',id);
                body.append(signUpFormViewTemplate);
            }

            if(signUpFormViewTemplate.length)
            {
                blockEventList_handleSignUpFormFunctionality(
                    signUpFormViewTemplate,postData.pageBlockId,postData,translations
                );

                signUpFormViewTemplate=signUpFormViewTemplate.clone(true);
            }

            doneCallback(signUpFormViewTemplate);
        },postData,doNotRequireSession);
    }
    else
    {
        doneCallback(signUpFormViewTemplate.clone(true));
    }
}
function blockEventList_request_addAndGetSignUpFormViewForEventSignUpBtn(
    signUpBtnSelectorOrInstance,postData,doneCallback,translations
    )
{
    var signUpBtn=$(signUpBtnSelectorOrInstance);
    var signUp=signUpBtn.closest('.sign-up');
    var signUpFormView=signUp.find('.sign-up-form');
    if(!signUpFormView.length)
    {
        blockEventList_request_getSignUpFormView(postData,function(signUpFormViewClone){
            if(signUpFormViewClone&&signUpFormViewClone.length)
            {
                var id=signUpFormViewClone.attr('id').replace('-template','');
                signUpFormViewClone.attr('id',id);
                signUpFormViewClone.find('[name="signUpEventId"]').val(postData.eventId);
                signUpFormViewClone.hide();

                signUp.append(signUpFormViewClone);

                signUp.find('#'+id).slideDown();
            }
            doneCallback(signUpFormViewClone); // Return new (empty) form.
        },translations);
    }
    else
    {
        doneCallback(signUpFormView); // Return current form, may be already filled with some contents.
    }
}
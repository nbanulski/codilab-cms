function blockBachelorStudies_request_reorderTabs(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-bachelor-studies/reorder-tabs'),doneCallback,postData);
}
function blockBachelorStudies_request_updateBasicInfo(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-bachelor-studies/update-basic-info'),doneCallback,postData);
}
function blockBachelorStudies_request_updateTabContents(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-bachelor-studies/update-tab-contents'),doneCallback,postData);
}
function blockBachelorStudies_updateBasicInfo(pageBlockId,blockContent,infoType,selector)
{
    attachInlineCkEditorToDomElement(
        blockContent,
        selector,
        'bs-'+infoType,
        false,
        function(eventContainer,textContainer,oldTextContainerId,ckEditorId,kcFinderAttached,contents){
            var postData={
                'pageBlockId':pageBlockId,
                'infoType':infoType,
                'contents':contents
            };
            blockBachelorStudies_request_updateBasicInfo(postData);
            destroyCkeditor(ckEditorId);
        },
        false,
        [
            {
                name:'document',
                items:[
                ]
            },
            {
                name:'basicstyles',
                items:[
                    'Bold','Italic','Underline','Strike','-','RemoveFormat'
                ]
            },
            '/',
            {
                name:'paragraph',
                items:[
                    'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'
                ]
            },
            {
                name:'colors',
                items:[
                    'TextColor'
                ]
            },
            {
                name:'links',
                items:[
                    'Link'
                ]
            }
        ]
    );
}
function blockBachelorStudies_handleCustomFunctionality(pageBlockId)
{
    var blockContent=$('.slot-item[data-page-block-id="'+pageBlockId+'"] .block-content');
    var basicInfo=blockContent.find('.basic-info');
    var tabsControlList=blockContent.find('.nav.nav-tabs');
    var tabsContents=blockContent.find('.tabs-contents');

    tabsControlList.off('click','> li').on('click','> li',function(){
        var tabId=$(this).attr('data-tab-id');
        tabsContents.find('.tab-pane[data-tab-id!='+tabId+']').hide();
        tabsContents.find('.tab-pane[data-tab-id='+tabId+']').show();
        tabsControlList.find('> li[data-tab-id!='+tabId+']').removeClass('active');
        tabsControlList.find('> li[data-tab-id='+tabId+']').addClass('active');
    });

    if(isDesignModeEnabled())
    {
        blockBachelorStudies_updateBasicInfo(pageBlockId,blockContent,'title','.basic-info .title');
        //blockBachelorStudies_updateBasicInfo(pageBlockId,blockContent,'glyphicon','.basic-info .glyphicon');
        blockBachelorStudies_updateBasicInfo(pageBlockId,blockContent,'time','.basic-info .time');
        blockBachelorStudies_updateBasicInfo(pageBlockId,blockContent,'degree_type','.basic-info .degree-type');
        blockBachelorStudies_updateBasicInfo(pageBlockId,blockContent,'language','.basic-info .language');

        tabsControlList.sortable({
            'items':'> li:not(.add-new-tab)',
            'sort':function(event,ui){
                var draggedElement=$(ui.item[0]);
                var draggedElementHeight=draggedElement.outerHeight();
                var placeholder=draggedElement.parent().find('.ui-sortable-placeholder');
                placeholder.removeClass('ui-sortable-placeholder');
                placeholder.css({
                    'background':'#fcefa1',
                    'border':'1px solid orange',
                    'height':draggedElementHeight+'px',
                    'visibility':'visible'
                });
            },
            'stop':function(event,ui){
                var parentUlTag=ui.item.parent();
                var tabs=[];
                parentUlTag.find('>li').each(function(){
                    var tab=$(this);
                    var tabId=tab.attr('data-tab-id');
                    tabs.push(tabId);
                });

                var postData={
                    'pageBlockId':pageBlockId,
                    'tabs':tabs
                };
                blockBachelorStudies_request_reorderTabs(postData,function(response){

                });
            }
        });

        tabsContents.off('dblclick','.tab-pane[data-tab-id] .tab-contents')
                    .on('dblclick','.tab-pane[data-tab-id] .tab-contents',function(){
            loadCkEditorLibrary();

            var textDiv=$(this);
            var ckEditorId='block-bachelor-studies-tab-contents-editable-'+pageBlockId;
            textDiv.attr('id',ckEditorId);
            textDiv.prop('contenteditable',true);
            textDiv.ckeditor($.extend(defaultCkeditorConfig,{
                filebrowserImageBrowseUrl:'/libs/kcfinder/kcfinder/browse.php?type=images&cms=proimagine-cms',
                filebrowserImageUploadUrl:'/libs/kcfinder/kcfinder/upload.php?type=images&cms=proimagine-cms',
                filebrowserBrowseUrl:'/libs/kcfinder/kcfinder/browse.php?type=other-files&cms=proimagine-cms',
                filebrowserUploadUrl:'/libs/kcfinder/kcfinder/upload.php?type=other-files&cms=proimagine-cms',
                'toolbar':false
            }));

            var ckEditorInstance=getCkeditorInstanceById(ckEditorId);
            ckEditorInstance.on('blur',function(e){
                if($(document.activeElement).get(0).tagName.toLowerCase()!="iframe"){
                    var editor=$(this)[0];
                    if(editor)
                    {
                        var id=editor.name;
                        if(id!='')
                        {
                            var tabId=textDiv.closest('.tab-pane').attr('data-tab-id');
                            var tabContents=null;
                            if(getCkeditorInstanceById(id))
                            {
                                tabContents=getCkeditorData(id); // CKEditor.
                            }
                            else
                            {
                                tabContents=$('#'+id).html(); // HTML5 Content-Editable element.
                            }

                            var postData={
                                'pageBlockId':pageBlockId,
                                'tabId':tabId,
                                'tabContents':tabContents
                            };
                            blockBachelorStudies_request_updateTabContents(postData,function(response){
                                if(requestOkAndSessionExist(response))
                                {
                                    var expectedStatus='TAB_CONTENTS_UPDATED';
                                    if(response.meta.customStatus==expectedStatus)
                                    {
                                        var additionalData={
                                            'tabId':tabId
                                        }
                                        renderBlockAgain(pageBlockId,additionalData);
                                    }
                                }
                            });

                            destroyCkeditor(id);
                            $('#'+id).removeAttr('contenteditable');
                        }
                    }
                }
            });
        });
    }
}
function blockBachelorStudies_handleSettingsFunctionality(pageBlockId)
{
    var dialogBlockSettings=$('.modal#dialog-block-settings');
    var settingsForm=dialogBlockSettings.find('.form[data-page-block-id="'+pageBlockId+'"]');

    dialogBlockSettings.on('click','.btn-save',function(){
        var uniqueHashForFileName=settingsForm.find('[name*="[uniqueHashForFileName]"]').val();
        if(!uniqueHashForFileName)
        {
            uniqueHashForFileName='';
        }
        var uniqueHashForFileNameInput=
            $('<input type="hidden" name="uniqueHashForFileName" />').val(uniqueHashForFileName);
        settingsForm.find('[data-image-number]').append(uniqueHashForFileNameInput);
        settingsForm.find('[name^="bsImages"]').remove();
        settingsForm.find('[name^="attachmentsToDelete"]').remove();
    });
}
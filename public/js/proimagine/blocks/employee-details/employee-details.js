function blockEmployeeDetails_request_setEmployeeDegreeId(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-employee-details/set-employee-degree'),doneCallback,postData);
}
function blockEmployeeDetails_request_updateEmployeeNameAndSurname(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-employee-details/update-employee-name-and-surname'),doneCallback,postData);
}
function blockEmployeeDetails_request_updateEmployeePosition(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-employee-details/update-employee-position'),doneCallback,postData);
}
function blockEmployeeDetails_request_updateEmployeeAbout(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-employee-details/update-employee-about'),doneCallback,postData);
}
function blockEmployeeDetails_request_updateEmployeeScientificCareer(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-employee-details/update-employee-scientific-career'),doneCallback,postData);
}
function blockEmployeeDetails_request_updateEmployeeTheMostImportantPublications(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-employee-details/update-employee-the-most-important-publications'),doneCallback,postData);
}
function blockEmployeeDetails_request_updateEmployeeContactInfo(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-employee-details/update-employee-contact-info'),doneCallback,postData);
}
function blockEmployeeDetails_handleCustomFunctionality(pageBlockId)
{
    var block=$('.slot-item[data-page-block-id="'+pageBlockId+'"]');
    var blockContent=block.find('.block-content > .content');
    var employeeDetails=blockContent.find('.employee-details');

    var tabsControl=employeeDetails.find('.tabs-control');
    var tabsControlList=tabsControl.find('.nav.nav-tabs');
    var tabsContents=tabsControl.find('.tabs-contents');
    tabsControlList.off('click','> li').on('click','> li',function(){
        var tabId=$(this).attr('data-tab-id');
        tabsContents.find('.tab-pane[data-tab-id!='+tabId+']').hide();
        tabsContents.find('.tab-pane[data-tab-id='+tabId+']').show();
        tabsControlList.find('> li[data-tab-id!='+tabId+']').removeClass('active');
        tabsControlList.find('> li[data-tab-id='+tabId+']').addClass('active');
    });

    if(isDesignModeEnabled())
    {
        var employeeId=employeeDetails.attr('data-employee-id');

        employeeDetails.off('dblclick','.name-and-surname').on('dblclick','.name-and-surname',function(){
            var htmlTag=$(this);
            htmlTag.prop('contenteditable',true);
            htmlTag.off('blur').on('blur',function(){
                var nameAndSurname=$.trim(htmlTag.text());
                var postData={
                    'pageBlockId':pageBlockId,
                    'employeeId':employeeId,
                    'nameAndSurname':nameAndSurname
                };
                blockEmployeeDetails_request_updateEmployeeNameAndSurname(postData);

                htmlTag.removeAttr('contenteditable');
            });
            htmlTag.focus();
        });

        employeeDetails.off('dblclick','.position').on('dblclick','.position',function(){
            var htmlTag=$(this);
            htmlTag.prop('contenteditable',true);
            htmlTag.off('blur').on('blur',function(){
                var position=$.trim(htmlTag.text());
                var postData={
                    'pageBlockId':pageBlockId,
                    'employeeId':employeeId,
                    'position':position
                };
                blockEmployeeDetails_request_updateEmployeePosition(postData);

                htmlTag.removeAttr('contenteditable');
            });
            htmlTag.focus();
        });

        var prefixForCkEditors='employee-details-'+pageBlockId+'-';

        attachInlineCkEditorToDomElement(
            employeeDetails,
            '.about',
            prefixForCkEditors+'about',
            true,
            function(eventContainer,textContainer,oldTextContainerId,ckEditorId,kcFinderAttached,contents){
                var postData={
                    'pageBlockId':pageBlockId,
                    'employeeId':employeeId,
                    'about':contents
                }
                blockEmployeeDetails_request_updateEmployeeAbout(postData);
            }
        );
        attachInlineCkEditorToDomElement(
            employeeDetails,
            '.scientific-career',
            prefixForCkEditors+'scientific-career',
            true,
            function(eventContainer,textContainer,oldTextContainerId,ckEditorId,kcFinderAttached,contents){
                var postData={
                    'pageBlockId':pageBlockId,
                    'employeeId':employeeId,
                    'scientificCareer':contents
                }
                blockEmployeeDetails_request_updateEmployeeScientificCareer(postData);
            }
        );
        attachInlineCkEditorToDomElement(
            employeeDetails,
            '.the-most-important-publications',
            prefixForCkEditors+'the-most-important-publications',
            true,
            function(eventContainer,textContainer,oldTextContainerId,ckEditorId,kcFinderAttached,contents){
                var postData={
                    'pageBlockId':pageBlockId,
                    'employeeId':employeeId,
                    'theMostImportantPublications':contents
                }
                blockEmployeeDetails_request_updateEmployeeTheMostImportantPublications(postData);
            }
        );
        attachInlineCkEditorToDomElement(
            employeeDetails,
            '.contact-info',
            prefixForCkEditors+'contact-info',
            true,
            function(eventContainer,textContainer,oldTextContainerId,ckEditorId,kcFinderAttached,contents){
                var postData={
                    'pageBlockId':pageBlockId,
                    'employeeId':employeeId,
                    'contactInfo':contents
                }
                blockEmployeeDetails_request_updateEmployeeContactInfo(postData);
            }
        );
    }
}

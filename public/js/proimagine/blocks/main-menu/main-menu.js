loadJsFile('/js/jquery.hoverIntent.min.js');

function blockMainMenu_request_addEditPanel(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-main-menu/add-edit-info'),doneCallback,postData);
}
function blockMainMenu_request_deleteInfo(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-main-menu/delete-info'),doneCallback,postData);
}
function blockMainMenu_request_updateInfoTitle(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-main-menu/update-info-title'),doneCallback,postData);
}
function blockMainMenu_request_updateInfoContents(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-main-menu/update-info-contents'),doneCallback,postData);
}
function blockMainMenu_deleteInfo(pageBlockId,infoId,doneCallback)
{
    var postData={
        'pageBlockId':pageBlockId,
        'infoId':infoId
    };
    blockMainMenu_request_deleteInfo(postData,doneCallback);
}
function blockMainMenu_updateInfoTitle(pageBlockId,infoId,infoTitle,doneCallback)
{
    var postData={
        'pageBlockId':pageBlockId,
        'infoId':infoId,
        'infoTitle':infoTitle
    };
    blockMainMenu_request_updateInfoTitle(postData,doneCallback);
}
function blockMainMenu_updateInfoContents(pageBlockId,infoId,infoContents,doneCallback)
{
    var postData={
        'pageBlockId':pageBlockId,
        'infoId':infoId,
        'infoContents':infoContents
    };
    blockMainMenu_request_updateInfoContents(postData,doneCallback);
}

function blockMainMenu_handleCustomFunctionality(pageBlockId)
{
    var mainMenuList=$('.slot-item[data-page-block-id="'+pageBlockId+'"] .block-content .nav.nav-justified');
    mainMenuList.hoverIntent({
        'over':function(e){
            var rootElement=$(this).closest('li[data-item-id]').find('> a');
            var subMenu = $(this).find('.nav-submenu li');
            var panelBackground=$(this).find('.panel-bcg');
            var panel=$(this).find('.panel');
            var panelContainerLeft=mainMenuList.offset().left;
            var windowWidth=$(document).width();

            rootElement.addClass('active');
            if(subMenu.length >1)
            {
                panel.show();
                panelBackground.show();
            }
            var attr=panelBackground.attr('visible');
            if((typeof attr==='undefined')||(attr===false))
            {
                panelBackground.css({
                    'position':'absolute',
                    'left':'-'+panelContainerLeft+'px',
                    'width':windowWidth+'px',
                    'height':panel.height()+20+'px'
                });
                panelBackground.removeClass('inactive');
                panelBackground.addClass('active');
                panel.removeClass('inactive');
                panel.addClass('active');
            }
        },
        'out':function(e){
            var rootElement=$(this).closest('li[data-item-id]').find('> a');
            var panelBackground=$(this).find('.panel-bcg');
            var panel=$(this).find('.panel');
            var panelContainerLeft=mainMenuList.find('> li.child-0').first().offset().left;
            var windowWidth=$(document).width();

            panelBackground.css({
                'position':'static',
                'left':panelContainerLeft+'px',
                'width':'auto',
                'display':'none'
            });
            panelBackground.removeClass('active');
            panelBackground.addClass('inactive');
            panel.removeClass('active');
            panel.addClass('inactive');
            rootElement.removeClass('active');
        },
        'timeout':500,
        'selector':'> li.child-0'
    });
    
    var subMainMenuList=$('.slot-item[data-page-block-id="'+pageBlockId+'"] .block-content .nav.nav-justified .nav-submenu li');
    subMainMenuList.hoverIntent({
        'over':function(e){            
            var subItems = $(this).find('ul');            
            subItems.show();
        },
        'out':function(e){
            var subItems = $(this).find('ul');            
            subItems.hide();
        },
        'timeout':100
    });
    
    if(isDesignModeEnabled())
    {
        var navPanel=$('.slot-item[data-page-block-id="'+pageBlockId+'"] .panel .nav-panel');
        navPanel.off('dblclick','.main-menu-information-title')
            .on('dblclick','.main-menu-information-title',function()
            {
                var panelTitle=$(this);
                panelTitle.prop('contenteditable',true);
                panelTitle.focus();
                mainMenuList.sortable( "disable" );
                panelTitle.off('blur').on('blur',function(){
                    var infoId=panelTitle.closest('.nav-panel').attr('data-info-id');
                    var infoTitleData=$.trim(panelTitle.text());
                    blockMainMenu_updateInfoTitle(pageBlockId,infoId,infoTitleData);
                    panelTitle.removeAttr('contenteditable');
                    mainMenuList.sortable( "enable" );
                });
            });
       navPanel.find('.main-menu-information-description .description')
           .off('dblclick')
           .on('dblclick',function()
            {
                loadCkEditorLibrary();

                var textDiv=$(this);
                var ckEditorId='block-main-menu-information-contents-editable-'+pageBlockId;
                textDiv.attr('id',ckEditorId);                
                textDiv.prop('contenteditable',true);
                textDiv.focus();
                textDiv.ckeditor($.extend(defaultCkeditorConfig,{
                    filebrowserImageBrowseUrl:'/libs/kcfinder/kcfinder/browse.php?type=images&cms=proimagine-cms',
                    filebrowserImageUploadUrl:'/libs/kcfinder/kcfinder/upload.php?type=images&cms=proimagine-cms',
                    filebrowserBrowseUrl:'/libs/kcfinder/kcfinder/browse.php?type=other-files&cms=proimagine-cms',
                    filebrowserUploadUrl:'/libs/kcfinder/kcfinder/upload.php?type=other-files&cms=proimagine-cms',
                    'toolbar':false
                }));
                
                mainMenuList.unbind("mouseenter").unbind("mouseleave").sortable( "disable" );
                var ckEditorInstance=getCkeditorInstanceById(ckEditorId);                
               
                ckEditorInstance.on('blur',function(e){
                    if($(document.activeElement).get(0).tagName.toLowerCase()!="iframe"){
                        var editor= $(ckEditorInstance)[0];
                        if(editor)
                        {
                            var id=editor.name;
                            if(id!='')
                            {
                                var infoId=textDiv.closest('.nav-panel').attr('data-info-id');                                
                                var infoContents=null;
                                if(getCkeditorInstanceById(id))
                                {
                                    infoContents=getCkeditorData(id); // CKEditor.
                                }
                                else
                                {
                                    infoContents=$('#'+id).html(); // HTML5 Content-Editable element.
                                }

                                blockMainMenu_updateInfoContents(pageBlockId,infoId,infoContents,function(response){
                                    if(requestOkAndSessionExist(response))
                                    {
                                        var expectedStatus='INFO_CONTENTS_UPDATED';
                                        if(response.meta.customStatus==expectedStatus)
                                        {                                           
                                            renderBlockAgain(pageBlockId);
                                        }
                                    }
                                });

                                destroyCkeditor(id);
                                $('#'+id).removeAttr('contenteditable');
                            }
                        }
                    }
                });
            });
    }
}
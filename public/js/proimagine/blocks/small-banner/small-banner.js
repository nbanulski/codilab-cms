loadPlUploadLibrary();
function blockSmallBanner_request_reorderBanners(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-small-banner/reorder-banners'),doneCallback,postData);
}
function smallBanner_request_addBannerEvent(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-small-banner/create-banner'),doneCallback,postData);
}
function smallBanner_request_editBannerEvent(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-small-banner/update-banner'),doneCallback,postData);
}
function smallBanner_request_editBannerTextEvent(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-small-banner/update-banner-text'),doneCallback,postData);
}
function smallBanner_request_deleteBannerEvent(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-small-banner/delete-banner'),doneCallback,postData);
}
function smallBanner_request_toggleBannerPublishing(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-small-banner/toggle-banner-publishing'),doneCallback,postData);
}
function smallBanner_handleBlockManagementWindow(pageBlockId)
{
    var viewRequestGetFunction=request_getBlockManagementView;
    var viewGetCallback=null;
    var togglePublishingCallback=function(published,elementId,button)
    {
        smallBanner_request_toggleBannerPublishing(function(response)
        {
            if(requestOkAndSessionExist(response))
            {
                var status=response.meta.customStatus;
                if(status=='PUBLISHED')
                {

                }
                else if(status=='UNPUBLISHED')
                {

                }
            }
        },{
            'pageBlockId':pageBlockId,
            'bannerId':elementId,
            'published':published?1:0
        });
    };
    var addNewCallback=function(button){
        smallBanner_showBlockAddEditWindow('add','Dodaj nowy baner','Zapisz','Anuluj',{
            'pageBlockId':pageBlockId
        });
    };
    var actionCallback=function(elementId,action,button)
    {
        action=$.trim(action).toLowerCase();
        if(action=='edit')
        {
            smallBanner_showBlockAddEditWindow('edit','Edytuj baner','Zapisz','Anuluj',{
                'pageBlockId':pageBlockId,
                'bannerId':elementId
            });
        }
        else if(action=='delete')
        {
            jConfirm('Czy na pewno chcesz usunąć ten baner?','Potwierdź usunięcie baneru','dialog-confirm-banner-small-removal',function(id,confirmed){
                if(confirmed)
                {
                    smallBanner_request_deleteBannerEvent(function(response)
                    {
                        if(requestOkAndSessionExist(response))
                        {
                            if(response.meta.customStatus=='BANNER_DELETED')
                            {
                                refreshPageBlockManagementWindow(pageBlockId);
                            }
                        }
                    },{
                        'pageBlockId':pageBlockId,
                        'bannerId':elementId
                    });
                }
            });
        }
    };
    var onCloseCallback=function(button)
    {
        renderBlockAgain(pageBlockId);
    };
    var otherSelectorOrInstance=null;
    var optionalCssClassPrefix=null;
    var otherTitlePrefix=null;
    var optionalSourcePageBlockId=null;
    var onOpenCallback=function(dialog){
        var sortable=blockSmallBanner_attachSortableToManagementWindow(dialog,pageBlockId);

        dialog
            .off('mouseenter','td.actions [data-action="move"]')
            .on('mouseenter','td.actions [data-action="move"]',function(){
                if(!dialog.find('.table').hasClass('ui-sortable'))
                {
                    sortable=blockSmallBanner_attachSortableToManagementWindow(dialog,pageBlockId);
                }

                sortable.sortable('option','items','tr[data-element-id]');

                return false;
            });
    };

    handlePageBlockManagementWindow(
        pageBlockId,viewRequestGetFunction,viewGetCallback,togglePublishingCallback,addNewCallback,actionCallback,
        onCloseCallback,otherSelectorOrInstance,optionalCssClassPrefix,otherTitlePrefix,optionalSourcePageBlockId,
        onOpenCallback
    );
}
function blockSmallBanner_attachSortableToManagementWindow(dialog,pageBlockId)
{
    var table=dialog.find('.table');
    var sortable=table.sortable({
        'handle':'td.actions [data-action="move"]',
        'sort':function(event,ui){
            var draggedElement=$(ui.item[0]);
            var draggedElementHeight=draggedElement.outerHeight();
            var placeholder=draggedElement.parent().find('.ui-sortable-placeholder');
            placeholder.removeClass('ui-sortable-placeholder');
            placeholder.css({
                'background':'#fcefa1',
                'border':'1px solid orange',
                'height':draggedElementHeight+'px',
                'visibility':'visible'
            });
        },
        'stop':function(event,ui){
            var banners=[];
            table.find('tr[data-element-id]').each(function(){
                var bannerId=$(this).attr('data-element-id');
                banners.push(bannerId);
            });

            var postData={
                'pageBlockId':pageBlockId,
                'banners':banners
            };
            blockSmallBanner_request_reorderBanners(postData,function(){

            });
        }
    });

    return sortable;
}
function smallBanner_showBlockAddEditWindow(type,title,btn_save_text,btn_cancel_text,additionalData)
{
    var pageBlockId=additionalData.pageBlockId;
    var expectedStatus=(type=='add')?'BANNER_SAVED':'BANNER_UPDATED';
    var operationType=(type=='add')?'new':'update';
    var postData={};
    postData=$.extend(postData,additionalData);
    request_getBlockAddEditView(function(response)
    {
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            var withSaveButton=true;
            jDialog(contents,title,'dialog-add-edit-banner',function(id,success)
            {
                if(success)
                {
                    var dialog=$('#'+id);

                    //smallBanner_attachPlUploadScriptToAddEditWindow(pageBlockId);


                    attachCkEditorToDomElement(
                        dialog.find('#ae-small-banner-contents'),
                        true,
                        'moono-dark'
                    );
                   
                    dialog.on('click','.btn-save',function()
                    {                        
                        var uniqueHashForFileNameInput=dialog.find('#ae-banner-unique-hash-for-file-name');
                        var uniqueHashForFileName=uniqueHashForFileNameInput.val();
                        if(uniqueHashForFileName != '')
                        {
                            var form=dialog.find('.form');
                            var formData=getFormData(form);
                            var postData=$.extend({                            
                                'formData':formData
                            },additionalData);
                            if(operationType == 'update')
                            {
                                smallBanner_request_editBannerEvent(function(response)
                                {
                                     if(requestOkAndSessionExist(response))
                                     {
                                         if(response.meta.customStatus==expectedStatus)
                                         {
                                             dialog.modal('hide');
                                             refreshPageBlockManagementWindow(pageBlockId);
                                         }
                                     }
                                 },postData);
                            }
                            else
                            {
                                smallBanner_request_addBannerEvent(function(response)
                                {
                                    if(requestOkAndSessionExist(response))
                                    {
                                        if(response.meta.customStatus==expectedStatus)
                                        {
                                            dialog.modal('hide');
                                            refreshPageBlockManagementWindow(pageBlockId);
                                        }
                                    }
                                },postData);
                            }
                        }
                        else
                        {                                
                            jDialog("Prześlij zdjęcie","Błąd");
                            console.log("Attachment not found - Please upload a file");
                        }                        
                    });
                }
            },withSaveButton);
        }
    },postData);
}
function smallBanner_attachEventsToBlock(pageBlockId)
{
    $('.slot-item[data-page-block-id="'+pageBlockId+'"] .slider-wrapper').on('dblclick','.nivo-caption',function()
    {
        var bannerTextContent=$(this);        
        var sliderId = bannerTextContent.closest('.nivoSlider').attr('id');
        var contentContainerId='small-banner-text'+sliderId;

        loadCkEditorLibrary();

        $("#"+sliderId).data('nivoslider').stop();
        bannerTextContent.attr('id',contentContainerId);
        bannerTextContent.attr('contenteditable','true');
        bannerTextContent.ckeditor($.extend(defaultCkeditorConfig,{
            toolbar: 
            [
              ['Bold','Italic','Underline'],
              ['NumberedList','BulletedList'],
              ['JustifyLeft','JustifyCenter','JustifyRight'],
              ['Undo','Redo'],
              '/',
              ['TextColor','Font','FontSize'],
              [ 'Link','Unlink','Anchor' ]
            ]
        }));
        
        var editor=getCkeditorInstanceById(contentContainerId);
        
        editor.on('blur',function(e)
        {
            if($(document.activeElement).get(0).tagName.toLowerCase()!="iframe")
            {
                var editor=$(this)[0];
                if(editor)
                {
                    var id=editor.name;
                    if(id!='')
                    {      
                        var blockContent = null;
                        if(getCkeditorInstanceById(contentContainerId))
                        {
                            blockContent=getCkeditorData(id); // CKEditor.
                        }
                        else
                        {
                            blockContent=$('#'+id).html(); // HTML5 Content-Editable element.
                        }                 
                        var itemId = $("#"+sliderId).data('nivo:vars').currentImage.data('small-banner-id');
                        var banner = {
                            'smallBannerId':itemId,
                            'bannerContents':blockContent                         
                        };
                         var postData={
                             'pageBlockId':pageBlockId,
                             'banner':banner
                         };
                         smallBanner_request_editBannerTextEvent(function(){
                             destroyCkeditor(id);
                             $('#'+id).removeAttr('contenteditable');
                             $('.slot-item[data-page-block-id="'+pageBlockId+'"] .slider-wrapper .nivoSlider img').each(function(){
                                 var itemKey = $(this).attr('data-small-banner-id');
                                 if(itemKey == itemId){
                                     var textContainer = $(this).attr('title');
                                     $(textContainer).html(blockContent);
                                 }
                             });
                             $("#"+sliderId).data('nivoslider').start();
                        },postData);
                    }           
                }
            }        
        }); 
    });
}
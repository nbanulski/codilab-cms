function blockPostgraduateStudies_request_reorderTabs(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-postgraduate-studies/reorder-tabs'),doneCallback,postData);
}
function blockPostgraduateStudies_request_updateBasicInfo(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-postgraduate-studies/update-basic-info'),doneCallback,postData);
}
function blockPostgraduateStudies_request_updateTabContents(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-postgraduate-studies/update-tab-contents'),doneCallback,postData);
}
function blockPostgraduateStudies_updateBasicInfo(pageBlockId,blockContent,infoType,selector)
{
    attachInlineCkEditorToDomElement(
        blockContent,
        selector,
        'bs-'+infoType,
        false,
        function(eventContainer,textContainer,oldTextContainerId,ckEditorId,kcFinderAttached,contents){
            var postData={
                'pageBlockId':pageBlockId,
                'infoType':infoType,
                'contents':contents
            };
            blockPostgraduateStudies_request_updateBasicInfo(postData);
        },
        false,
        [
            {
                name:'document',
                items:[
                ]
            },
            {
                name:'basicstyles',
                items:[
                    'Bold','Italic','Underline','Strike','-','RemoveFormat'
                ]
            },
            '/',
            {
                name:'paragraph',
                items:[
                    'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'
                ]
            },
            {
                name:'colors',
                items:[
                    'TextColor'
                ]
            },
            {
                name:'links',
                items:[
                    'Link'
                ]
            }
        ]
    );
}
function blockPostgraduateStudies_handleCustomFunctionality(pageBlockId)
{
    var blockContent=$('.slot-item[data-page-block-id="'+pageBlockId+'"] .block-content');
    var basicInfo=blockContent.find('.basic-info');
    var tabsControlList=blockContent.find('.nav.nav-tabs');
    var tabsContents=blockContent.find('.tabs-contents');

    tabsControlList.off('click','> li').on('click','> li',function(){
        var tabId=$(this).attr('data-tab-id');
        tabsContents.find('.tab-pane[data-tab-id!='+tabId+']').hide();
        tabsContents.find('.tab-pane[data-tab-id='+tabId+']').show();
        tabsControlList.find('> li[data-tab-id!='+tabId+']').removeClass('active');
        tabsControlList.find('> li[data-tab-id='+tabId+']').addClass('active');
    });

    if(isDesignModeEnabled())
    {
        blockPostgraduateStudies_updateBasicInfo(pageBlockId,blockContent,'to_whom','.basic-info .to-whom');
        blockPostgraduateStudies_updateBasicInfo(pageBlockId,blockContent,'contact','.basic-info .contact');
        blockPostgraduateStudies_updateBasicInfo(pageBlockId,blockContent,'duration','.basic-info .duration');
        blockPostgraduateStudies_updateBasicInfo(pageBlockId,blockContent,'email','.basic-info .e-mail');
        blockPostgraduateStudies_updateBasicInfo(pageBlockId,blockContent,'language','.basic-info .language');

        tabsControlList.sortable({
            'items':'> li:not(.add-new-tab)',
            'sort':function(event,ui){
                var draggedElement=$(ui.item[0]);
                var draggedElementHeight=draggedElement.outerHeight();
                var placeholder=draggedElement.parent().find('.ui-sortable-placeholder');
                placeholder.removeClass('ui-sortable-placeholder');
                placeholder.css({
                    'background':'#fcefa1',
                    'border':'1px solid orange',
                    'height':draggedElementHeight+'px',
                    'visibility':'visible'
                });
            },
            'stop':function(event,ui){
                var parentUlTag=ui.item.parent();
                var tabs=[];
                parentUlTag.find('>li').each(function(){
                    var tab=$(this);
                    var tabId=tab.attr('data-tab-id');
                    tabs.push(tabId);
                });

                var postData={
                    'pageBlockId':pageBlockId,
                    'tabs':tabs
                };
                blockPostgraduateStudies_request_reorderTabs(postData,function(response){

                });
            }
        });

        attachInlineCkEditorToDomElement(
            tabsContents,
            '.tab-pane[data-tab-id] .tab-contents',
            'block-postgraduate-studies-tab-contents-editable-'+pageBlockId,
            true,
            function(eventContainer,textContainer,oldTextContainerId,ckEditorId,kcFinderAttached,contents){
                var tabId=textContainer.closest('.tab-pane').attr('data-tab-id');
                var postData={
                    'pageBlockId':pageBlockId,
                    'tabId':tabId,
                    'tabContents':contents
                };
                blockPostgraduateStudies_request_updateTabContents(postData,function(response){
                    if(requestOkAndSessionExist(response))
                    {
                        var expectedStatus='TAB_CONTENTS_UPDATED';
                        if(response.meta.customStatus==expectedStatus)
                        {
                            var additionalData={
                                'tabId':tabId
                            }
                            renderBlockAgain(pageBlockId,additionalData);
                        }
                    }
                });
            }
        );
    }
}
function blockPostgraduateStudies_handleSettingsFunctionality(pageBlockId)
{
    var dialogBlockSettings=$('.modal#dialog-block-settings');
    var settingsForm=dialogBlockSettings.find('.form[data-page-block-id="'+pageBlockId+'"]');

    dialogBlockSettings.on('click','.btn-save',function(){
        var uniqueHashForFileName=settingsForm.find('[name*="[uniqueHashForFileName]"]').val();
        if(!uniqueHashForFileName)
        {
            uniqueHashForFileName='';
        }
        var uniqueHashForFileNameInput=
            $('<input type="hidden" name="uniqueHashForFileName" />').val(uniqueHashForFileName);
        settingsForm.find('[data-image-number]').append(uniqueHashForFileNameInput);
        settingsForm.find('[name^="psImages"]').remove();
        settingsForm.find('[name^="attachmentsToDelete"]').remove();
    });
}
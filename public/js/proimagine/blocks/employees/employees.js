loadPlUploadLibrary();
function blockEmployees_request_toggleEmployeePublishing(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-employees/toggle-employee-publishing'),doneCallback,postData);
}
function blockEmployees_request_addEditEmployee(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-employees/add-edit-employee'),doneCallback,postData);
}
function blockEmployees_request_deleteEmployee(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-employees/delete-employee'),doneCallback,postData);
}
function blockEmployees_request_getEmployeesColumns(doneCallback,postData)
{
    var doNotRequireSession=true;
    makePostRequest(makeCmsUrl('block-employees/get-employees-columns'),doneCallback,postData,doNotRequireSession);
}
function blockEmployees_request_findEmployees(doneCallback,postData)
{
    var doNotRequireSession=true;
    makePostRequest(makeCmsUrl('block-employees/find-employees'),doneCallback,postData,doNotRequireSession);
}
function blockEmployees_handleCustomFunctionality(pageBlockId,userSettings,optionalSourcePageBlockId)
{
    var block=$('.slot-item[data-page-block-id="'+pageBlockId+'"]');
    var blockContent=block.find('.block-content > .content');
    var tabsControlList=blockContent.find('.nav.nav-tabs');
    var tabsContents=blockContent.find('.tabs-contents');

    tabsControlList.off('click','> li').on('click','> li',function(){
        var tabId=$(this).attr('data-tab-id');
        tabsContents.find('.tab-pane[data-tab-id!='+tabId+']').hide();
        tabsContents.find('.tab-pane[data-tab-id='+tabId+']').show();
        tabsControlList.find('> li[data-tab-id!='+tabId+']').removeClass('active');
        tabsControlList.find('> li[data-tab-id='+tabId+']').addClass('active');
    });

    var firstTabContents=tabsContents.find('.tab-pane[data-tab-id=1] .tab-contents');
    var secondTabContents=tabsContents.find('.tab-pane[data-tab-id=2] .tab-contents');
    var thirdTabContents=tabsContents.find('.tab-pane[data-tab-id=3] .tab-contents');

    firstTabContents
        .off('click','.alphabet span.enabled')
        .on('click','.alphabet span.enabled',function(e){
            var letter=$(this).attr('data-letter');
            blockEmployees_showEmployeesColumns(pageBlockId,letter);
        });

    thirdTabContents
        .off('keypress','.search-area [name="nameAndSurname"]')
        .on('keypress','.search-area [name="nameAndSurname"]',function(e){
            if(e.which==13)
            {
                blockEmployees_findEmployees(pageBlockId);
            }
    });

    thirdTabContents.off('click','.btn-search').on('click','.btn-search',function(){
        blockEmployees_findEmployees(pageBlockId);

        return false;
    });
}
function blockEmployees_showEmployeesColumns(pageBlockId,firstLetter)
{
    var block=$('.slot-item[data-page-block-id="'+pageBlockId+'"]');
    var blockContent=block.find('.block-content > .content');
    var tabsContents=blockContent.find('.tabs-contents');

    var postData={
        'pageBlockId':pageBlockId,
        'firstLetter':firstLetter
    };
    blockEmployees_request_getEmployeesColumns(function(response){
        if(requestOk(response))
        {
            var firstTabContents=tabsContents.find('.tab-pane[data-tab-id=1] .tab-contents');
            var employeesColumns=firstTabContents.find('.employees-columns');
            var employeesColumnTemplate=employeesColumns.find('.employees-column[data-is-template="1"]');

            var foundEmployeesColumnCount=toInt(response.data.foundEmployeesColumnCount);
            var foundEmployeesColumns=response.data.foundEmployeesColumns;

            employeesColumns.find('.employees-column:not([data-is-template])').remove();

            if(foundEmployeesColumnCount)
            {
                for(var i=0;i<foundEmployeesColumnCount;i++)
                {
                    var column=foundEmployeesColumns[i];
                    var employees=column.employees;
                    var employeeCount=employees.length;
                    var employeesColumn=employeesColumnTemplate.clone();
                    employeesColumn.removeAttr('data-is-template');
                    employeesColumn.find('>h3').text(column.firstLetter);

                    if(employeeCount>0)
                    {
                        var ul=employeesColumn.find('ul');
                        var liTemplate=ul.find('> li[data-is-template="1"]');

                        for(var j=0;j<employeeCount;j++)
                        {
                            var employee=employees[j];
                            var li=liTemplate.clone();
                            li.removeAttr('data-is-template');
                            li.find('img').attr('src',employee.thumbUrl);
                            var anchor=li.find('a');
                            anchor.attr('href',employee.detailsPageUrl);
                            anchor.text(employee.name_and_surname);
                            anchor.after(employee.degree_title);

                            ul.append(li);
                        }

                        liTemplate.remove();
                    }

                    employeesColumn.show();

                    employeesColumns.append(employeesColumn);
                }
            }
        }
    },postData);
}
function blockEmployees_findEmployees(pageBlockId)
{
    var block=$('.slot-item[data-page-block-id="'+pageBlockId+'"]');
    var blockContent=block.find('.block-content > .content');
    var tabsContents=blockContent.find('.tabs-contents');
    var thirdTabContents=tabsContents.find('.tab-pane[data-tab-id=3] .tab-contents');
    var searchArea=thirdTabContents.find('.search-area');
    var searchResults=thirdTabContents.find('.search-results');
    var searchInfo=searchResults.find('h3.search-info');
    var foundEmployeeCountTag=searchInfo.find('.found-employe-count');
    var employeeList=searchResults.find('.employee-list');

    var messages=searchArea.find('.messages');
    messages.empty();

    searchResults.hide();
    foundEmployeeCountTag.text(0);
    employeeList.find('.employee:not([data-is-template])').remove();

    var formData=getFormData(searchArea);
    var postData={
        'pageBlockId':pageBlockId,
        'formData':formData
    }
    blockEmployees_request_findEmployees(function(response){
        if(requestOk(response))
        {
            var employeeTemplate=employeeList.find('.employee[data-is-template="1"]');

            var foundEmployeeCount=toInt(response.data.foundEmployeeCount);
            var foundEmployees=response.data.foundEmployees;

            foundEmployeeCountTag.text(foundEmployeeCount);

            if(foundEmployeeCount)
            {
                for(var i=0;i<foundEmployeeCount;i++)
                {
                    var employee=foundEmployees[i];
                    var employeeListRow=employeeTemplate.clone();
                    employeeListRow.removeAttr('data-is-template');
                    employeeListRow.find('img').attr('src',employee.thumbUrl);
                    var information=employeeListRow.find('.information');
                    var anchor=information.find('a');
                    anchor.attr('href',employee.detailsPageUrl);
                    anchor.attr('title',employee.degree_title+' '+employee.name_and_surname);
                    information.find('.degree-title').text(employee.degree_title);
                    information.find('.name-and-surname').text(employee.name_and_surname);
                    information.find('.position').text(employee.position);
                    employeeListRow.show();

                    employeeList.append(employeeListRow);
                }
            }

            searchResults.show();
        }
    },postData);

    return true;
}
function blockEmployees_showAddEditWindow(type,title,btn_save_text,btn_cancel_text,additionalData)
{
    var pageBlockId=additionalData.pageBlockId;
    var postData={};
    postData=$.extend(postData,additionalData);
    request_getBlockAddEditView(function(response){
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            var withSaveButton=true;
            jDialog(contents,title,'dialog-add-edit-employee',function(id,success){
                if(success)
                {
                    var dialog=$('#'+id);

                    var attachKcFinder=true;

                    attachCkEditorToDomElement(
                        dialog.find('#ae-employee-about'),
                        attachKcFinder
                    );
                    attachCkEditorToDomElement(
                        dialog.find('#ae-employee-scientific-career'),
                        attachKcFinder
                    );
                    attachCkEditorToDomElement(
                        dialog.find('#ae-employee-the-most-important-publications'),
                        attachKcFinder
                    );
                    attachCkEditorToDomElement(
                        dialog.find('#ae-employee-contact-info'),
                        attachKcFinder
                    );

                    dialog.on('click','.btn-save',function(){
                        var form=dialog.find('.form');
                        var formData=getFormData(form);
                        var postData=$.extend({
                            'dataPosted':true,
                            'formData':formData
                        },additionalData);
                        blockEmployees_request_addEditEmployee(function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                var expectedStatus=(type=='add')?'EMPLOYEE_ADDED':'EMPLOYEE_MODIFIED';
                                if(response.meta.customStatus==expectedStatus)
                                {
                                    dialog.modal('hide');
                                    refreshPageBlockManagementWindow(pageBlockId);
                                }
                            }
                        },postData);
                    });
                }
            },withSaveButton);
        }
    },postData);
}
function blockEmployees_handleManagementWindow(pageBlockId)
{
    var viewRequestGetFunction=request_getBlockManagementView;
    var viewGetCallback=null;
    var togglePublishingCallback=function(published,elementId,button){
        blockEmployees_request_toggleEmployeePublishing(function(response){
            if(requestOkAndSessionExist(response))
            {
                var status=response.meta.customStatus;
                if(status=='PUBLISHED')
                {

                }
                else if(status=='UNPUBLISHED')
                {

                }
            }
        },{
            'pageBlockId':pageBlockId,
            'employeeId':elementId,
            'published':published?1:0
        });
    };
    var addNewCallback=function(button){
        blockEmployees_showAddEditWindow('add','Dodaj nowego pracownika','Zapisz','Anuluj',{
            'pageBlockId':pageBlockId
        });
    };
    var actionCallback=function(elementId,action,button)
    {
        action=$.trim(action).toLowerCase();
        if(action=='edit')
        {
            blockEmployees_showAddEditWindow('edit','Edytuj pracownika','Zapisz','Anuluj',{
                'pageBlockId':pageBlockId,
                'employeeId':elementId
            });
        }
        else if(action=='delete')
        {
            var title='Potwierdź usunięcie pracownika';
            var contents='Czy na pewno chcesz usunąć tego pracownika?';
            var id='dialog-confirm-employee-removal';
            jConfirm(contents,title,id,function(id,confirmed){
                if(confirmed)
                {
                    var postData={
                        'pageBlockId':pageBlockId,
                        'employeeId':elementId
                    };
                    blockEmployees_request_deleteEmployee(function(response){
                        if(requestOkAndSessionExist(response))
                        {
                            if(response.meta.customStatus=='EMPLOYEE_DELETED')
                            {
                                refreshPageBlockManagementWindow(pageBlockId);
                            }
                        }
                    },postData);
                }
            });
        }
    };
    var onCloseCallback=function(button){
        renderBlockAgain(pageBlockId);
    };
    var otherSelectorOrInstance=null;
    var optionalCssClassPrefix=null;
    var otherTitlePrefix=null;
    var optionalSourcePageBlockId=null;
    handlePageBlockManagementWindow(
        pageBlockId,viewRequestGetFunction,viewGetCallback,togglePublishingCallback,addNewCallback,actionCallback,
        onCloseCallback
    );
}

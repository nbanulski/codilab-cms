function blockEmployees_request_getDegreesManagementView(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-employees/get-degrees-management-view'),doneCallback,postData);
}
function blockEmployees_request_toggleDegreePublishing(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-employees/toggle-degree-publishing'),doneCallback,postData);
}
function blockEmployees_request_getAddEditDegreeView(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-employees/get-add-edit-degree-view'),doneCallback,postData);
}
function blockEmployees_request_addEditDegree(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-employees/add-edit-degree'),doneCallback,postData);
}
function blockEmployees_request_deleteDegree(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-employees/delete-degree'),doneCallback,postData);
}
function blockEmployees_refreshDegreesManagementWindow(pageBlockId)
{
    var postData={
        'pageBlockId':pageBlockId
    };
    blockEmployees_request_getDegreesManagementView(function(response){
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            if(contents!='')
            {
                $('#dialog-block-degrees-management .modal-body').html(contents);
            }
        }
    },postData);
}
function blockEmployees_showAddEditDegreeWindow(type,title,btn_save_text,btn_cancel_text,additionalData)
{
    var pageBlockId=additionalData.pageBlockId;
    var expectedStatus=(type=='add')?'DEGREE_ADDED':'DEGREE_MODIFIED';
    var postData={};
    postData=$.extend(postData,additionalData);
    blockEmployees_request_getAddEditDegreeView(function(response){
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            var withSaveButton=true;
            jDialog(contents,title,'dialog-add-edit-degree',function(id,success){
                if(success)
                {
                    var dialog=$('#'+id);
                    dialog.on('click','.btn-save',function(){
                        var form=dialog.find('.form');
                        var formData=getFormData(form);
                        var postData=$.extend({
                            'dataPosted':true,
                            'formData':formData
                        },additionalData);
                        blockEmployees_request_addEditDegree(function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                if(response.meta.customStatus==expectedStatus)
                                {
                                    dialog.modal('hide');
                                    blockEmployees_refreshDegreesManagementWindow(pageBlockId);
                                }
                            }
                        },postData);
                    });
                }
            },withSaveButton);
        }
    },postData);
}
function blockEmployees_handleDegreesManagementWindow(pageBlockId)
{
    var viewRequestGetFunction=blockEmployees_request_getDegreesManagementView;
    var viewGetCallback=null;
    var togglePublishingCallback=function(published,elementId,button){
        var postData={
            'pageBlockId':pageBlockId,
            'degreeId':elementId,
            'published':published?1:0
        };
        blockEmployees_request_toggleDegreePublishing(function(response){
            if(requestOkAndSessionExist(response))
            {
                var status=response.meta.customStatus;
                if(status=='PUBLISHED')
                {

                }
                else if(status=='UNPUBLISHED')
                {

                }
            }
        },postData);
    };
    var addNewCallback=function(button){
        blockEmployees_showAddEditDegreeWindow('add','Dodaj nowe stanowisko','Zapisz','Anuluj',{
            'pageBlockId':pageBlockId
        });
    };
    var actionCallback=function(elementId,action,button){
        action=$.trim(action).toLowerCase();
        if(action=='edit')
        {
            blockEmployees_showAddEditDegreeWindow('edit','Edytuj stanowisko','Zapisz','Anuluj',{
                'pageBlockId':pageBlockId,
                'degreeId':elementId
            });
        }
        else if(action=='delete')
        {
            jConfirm('Czy na pewno chcesz usunąć to stanowisko?','Potwierdź usunięcie stanowiska','dialog-confirm-degree-removal',function(id,confirmed){
                if(confirmed)
                {
                    var postData={
                        'pageBlockId':pageBlockId,
                        'degreeId':elementId
                    };
                    blockEmployees_request_deleteDegree(function(response){
                        if(requestOkAndSessionExist(response))
                        {
                            if(response.meta.customStatus=='DEGREE_DELETED')
                            {
                                blockEmployees_refreshDegreesManagementWindow(pageBlockId);
                                refreshPageBlockManagementWindow(pageBlockId);
                            }
                        }
                    },postData);
                }
            });
        }
    };
    var onCloseCallback=function(button){
        refreshPageBlockManagementWindow(pageBlockId);
    };
    var otherSelectorOrInstance=$('#dialog-block-management .btn-manage-degrees');
    var optionalCssClassPrefix='degrees-';
    var otherTitlePrefix='Stanowiska';
    handlePageBlockManagementWindow(
        pageBlockId,
        viewRequestGetFunction,
        viewGetCallback,
        togglePublishingCallback,
        addNewCallback,
        actionCallback,
        onCloseCallback,
        otherSelectorOrInstance,
        optionalCssClassPrefix,
        otherTitlePrefix
    );
}
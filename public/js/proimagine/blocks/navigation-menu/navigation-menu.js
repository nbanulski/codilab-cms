loadPlUploadLibrary();
function blockNavigationMenu_request_reorderItems(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-navigation-menu/reorder-items'),doneCallback,postData);
}
function blockNavigationMenu_request_getParentItems(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-navigation-menu/get-parent-items'),doneCallback,postData);
}
function blockNavigationMenu_request_toggleItemPublishing(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-navigation-menu/toggle-item-publishing'),doneCallback,postData);
}
function blockNavigationMenu_request_addEditItem(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-navigation-menu/add-edit-item'),doneCallback,postData);
}
function blockNavigationMenu_request_deleteItem(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-navigation-menu/delete-item'),doneCallback,postData);
}
function blockNavigationMenu_handleCustomFunctionality(pageBlockId)
{
    var blockContent=$('.slot-item[data-page-block-id="'+pageBlockId+'"] .block-content');

    if(isDesignModeEnabled())
    {
        var navigationMenuLists=blockContent.find('.nav,.nav-submenu');
        navigationMenuLists.sortable({
            'sort':function(event,ui){
                var draggedElement=$(ui.item[0]);
                var draggedElementHeight=draggedElement.outerHeight();
                var placeholder=draggedElement.parent().find('.ui-sortable-placeholder');
                placeholder.removeClass('ui-sortable-placeholder');
                placeholder.css({
                    'background':'#fcefa1',
                    'border':'1px solid orange',
                    'height':draggedElementHeight+'px',
                    'visibility':'visible'
                });
            },
            'stop':function(event,ui){
                var parentUlTag=ui.item.parent();
                var parentItemId=parentUlTag.attr('data-parent-item-id');
                var items=[];
                parentUlTag.find('>li').each(function(){
                    var item=$(this);
                    var itemId=item.attr('data-item-id');
                    items.push(itemId);
                });

                var postData={
                    'pageBlockId':pageBlockId,
                    'parentItemId':parentItemId?parentItemId:null,
                    'items':items
                };
                blockNavigationMenu_request_reorderItems(function(response){

                },postData);
            }
        });
    }

    var navigationMenuRootList=blockContent.find('> .content > .nav');
    navigationMenuRootList.find('> [data-item-id] > [data-folding-switch]').on('click',function(){
        var foldingSwitch=$(this);
        var ul=foldingSwitch.closest('[data-item-id]').find('> ul');
        ul.slideToggle(400,function(){
            foldingSwitch.toggleClass('uncollapse');
        });

        return false;
    });
    navigationMenuRootList.find('> [data-item-id].has-childs > a[href="#"]').on('click',function(){
        var li=$(this).closest('li');
        var foldingSwitch=li.find('> [data-folding-switch]');
        var ul=li.find('> ul');
        ul.slideToggle(400,function(){
            foldingSwitch.toggleClass('uncollapse');
        });

        return false;
    });
}
function blockNavigationMenu_showAddEditWindow(type,title,btn_save_text,btn_cancel_text,additionalData)
{
    var pageBlockId=additionalData.pageBlockId;
    var expectedStatus=(type=='add')?'MENU_ITEM_ADDED':'MENU_ITEM_MODIFIED';
    var postData={};
    postData=$.extend(postData,additionalData);
    request_getBlockAddEditView(function(response){
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            var withSaveButton=true;
            jDialog(contents,title,'dialog-add-edit-item',function(id,success){
                if(success)
                {
                    var dialog=$('#'+id);
                    var urlAddressTypeDropdown=dialog.find('#ai-item-url-address-type');
                    var urlAddressType=urlAddressTypeDropdown.find('option:selected').val();
                    var isInternalUrl=(urlAddressType=='0');

                    dialog.on('click change','#ai-item-category',function(){
                        var postData={
                            'pageBlockId':pageBlockId,
                            'categoryId':$(this).find('option:selected').val(),
                            'itemId':dialog.find('input[name="itemId"]').val()
                        };
                        blockNavigationMenu_request_getParentItems(function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                var itemParentInput=dialog.find('#ai-item-parent');
                                itemParentInput.find('option').remove();
                                itemParentInput.append(response.data);
                            }
                        },postData);
                    });
                    dialog.on('click change','#ai-item-url-address-type',function(){
                        var urlAddressType=$.trim($(this).find('option:selected').val());
                        if(urlAddressType=='0')
                        {
                            dialog.find('#ai-item-internal-page-id').show();
                            dialog.find('#ai-item-external-url').hide();
                            dialog.find('#ai-item-url-form-group').show();
                        }
                        else if(urlAddressType=='1')
                        {
                            dialog.find('#ai-item-internal-page-id').hide();
                            dialog.find('#ai-item-external-url').show();
                            dialog.find('#ai-item-url-form-group').show();
                        }
                        else
                        {
                            dialog.find('#ai-item-url-form-group').hide();
                            dialog.find('#ai-item-internal-page-id').hide();
                            dialog.find('#ai-item-external-url').hide();
                        }
                    });
                    dialog.on('click change','#ai-item-type',function(){
                        var itemTextIconFormGroup=dialog.find('#ai-item-text-icon-form-group');
                        var itemImageFormGroup=dialog.find('#ai-item-image-form-group');
                        var itemTitleFormGroup=dialog.find('#ai-item-title-form-group');
                        var plUploadButtons=dialog.find('#select-file-to-upload-btn,#upload-selected-file');
                        var itemType=toInt($.trim($(this).find('option:selected').val()));

                        if(
                            (itemType===1)||// Only icon.
                            (itemType===3)  // Icon + text.
                          )
                        {
                            itemTextIconFormGroup.show();
                        }
                        else
                        {
                            itemTextIconFormGroup.hide();
                        }

                        if(
                            (itemType===2)||// Only image.
                            (itemType===4)  // Image + text.
                          )
                        {
                            itemImageFormGroup.show();
                            plUploadButtons.prop('disabled',false);
                        }
                        else
                        {
                            itemImageFormGroup.hide();
                            plUploadButtons.prop('disabled',true);
                        }

                        if(
                            (itemType===0)||// Only text.
                            (itemType===3)||// Icon + text.
                            (itemType===4)  // Image + text.
                          )
                        {
                            itemTitleFormGroup.show();
                        }
                        else
                        {
                            itemTitleFormGroup.hide();
                        }
                    });
                    dialog.on('click change','#ai-item-internal-page-id',function(){
                        var urlAddressType=dialog.find('#ai-item-url-address-type option:selected').val();
                        var isInternalUrl=(urlAddressType=='0');
                        if(isInternalUrl)
                        {
                            var internalPageIdDropdown=dialog.find('#ai-item-internal-page-id');
                            var selectedOption=internalPageIdDropdown.find('option:selected[value!=""]');
                            var selectedInternalPageTitle='';
                            if(selectedOption.length>0)
                            {
                                // Set item title the same as selected internal page title:
                                selectedInternalPageTitle=selectedOption.attr('data-title');
                            }
                            dialog.find('#ai-item-title').val($.trim(selectedInternalPageTitle));
                        }
                    });
                    dialog.find('#ai-item-text-icon').selectpicker();
                    var itemTitleInput=dialog.find('#ai-item-title');
                    var itemTitle=$.trim(itemTitleInput.val());
                    if(isInternalUrl&&(itemTitle==''))
                    {
                        var internalPageIdDropdown=dialog.find('#ai-item-internal-page-id');
                        var selectedOption=internalPageIdDropdown.find('option:selected[value!=""]');
                        var selectedInternalPageTitle='';
                        if(selectedOption.length>0)
                        {
                            // Set item title the same as selected internal page title:
                            selectedInternalPageTitle=$.trim(selectedOption.attr('data-title'));
                        }
                        itemTitleInput.val(selectedInternalPageTitle);
                    }
                    dialog.on('click','.btn-save',function(){
                        var itemTextIcon=dialog.find('#ai-item-text-icon').selectpicker('val');
                        var form=dialog.find('.form');
                        var formData=getFormData(form);
                        var formData= $.extend(formData,{
                            'itemTextIcon':itemTextIcon
                        });
                        var postData=$.extend({
                            'dataPosted':true,
                            'formData':formData
                        },additionalData);
                        blockNavigationMenu_request_addEditItem(function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                if(response.meta.customStatus==expectedStatus)
                                {
                                    dialog.modal('hide');
                                    refreshPageBlockManagementWindow(pageBlockId);
                                }
                            }
                        },postData);
                    });
                }
            },withSaveButton);
        }
    },postData);
}
function blockNavigationMenu_handleManagementWindow(pageBlockId)
{
    var viewRequestGetFunction=request_getBlockManagementView;
    var viewGetCallback=null;
    var togglePublishingCallback=function(published,elementId,button){
        blockNavigationMenu_request_toggleItemPublishing(function(response){
            if(requestOkAndSessionExist(response))
            {
                var status=response.meta.customStatus;
                if(status=='PUBLISHED')
                {

                }
                else if(status=='UNPUBLISHED')
                {

                }
            }
        },{
            'pageBlockId':pageBlockId,
            'itemId':elementId,
            'published':published?1:0
        });
    };
    var addNewCallback=function(button){
        blockNavigationMenu_showAddEditWindow('add','Dodaj nowy element','Zapisz','Anuluj',{
            'pageBlockId':pageBlockId
        });
    };
    var actionCallback=function(elementId,action,button)
    {
        action=$.trim(action).toLowerCase();
        if(action=='edit')
        {
            blockNavigationMenu_showAddEditWindow('edit','Edytuj element','Zapisz','Anuluj',{
                'pageBlockId':pageBlockId,
                'itemId':elementId
            });
        }
        else if(action=='delete')
        {
            jConfirm('Czy na pewno chcesz usunąć ten element?','Potwierdź usunięcie elementu','dialog-confirm-item-removal',function(id,confirmed){
                if(confirmed)
                {
                    blockNavigationMenu_request_deleteItem(function(response){
                        if(requestOkAndSessionExist(response))
                        {
                            if(response.meta.customStatus=='MENU_ITEM_DELETED')
                            {
                                refreshPageBlockManagementWindow(pageBlockId);
                            }
                        }
                    },{
                        'pageBlockId':pageBlockId,
                        'itemId':elementId
                    });
                }
            });
        }
    };
    var onCloseCallback=function(button){
        renderBlockAgain(pageBlockId);
    };

    handlePageBlockManagementWindow(pageBlockId,viewRequestGetFunction,viewGetCallback,togglePublishingCallback,addNewCallback,actionCallback,onCloseCallback);
}
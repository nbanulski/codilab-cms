function request_getNavigationMenuBlockCategoriesManagementView(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-navigation-menu/get-categories-management-view'),doneCallback,postData);
}
function request_toggleNavigationMenuBlockCategoryPublishing(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-navigation-menu/toggle-category-publishing'),doneCallback,postData);
}
function request_getNavigationMenuBlockAddEditCategoryView(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-navigation-menu/get-add-edit-category-view'),doneCallback,postData);
}
function request_addEditNavigationMenuBlockCategory(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-navigation-menu/add-edit-category'),doneCallback,postData);
}
function request_deleteNavigationMenuBlockCategory(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-navigation-menu/delete-category'),doneCallback,postData);
}
function refreshNavigationMenuBlockCategoriesManagementWindow(pageBlockId)
{
    request_getNavigationMenuBlockCategoriesManagementView(function(response){
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            if(contents!='')
            {
                $('#dialog-block-categories-management .modal-body').html(contents);
            }
        }
    },{
        'pageBlockId':pageBlockId
    });
}
function showNavigationMenuBlockAddEditCategoryWindow(type,title,btn_save_text,btn_cancel_text,additionalData)
{
    var pageBlockId=additionalData.pageBlockId;
    var expectedStatus=(type=='add')?'MENU_CATEGORY_ADDED':'MENU_CATEGORY_MODIFIED';
    var postData={};
    postData=$.extend(postData,additionalData);
    request_getNavigationMenuBlockAddEditCategoryView(function(response){
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            var withSaveButton=true;
            jDialog(contents,title,'dialog-add-edit-category',function(id,success){
                if(success)
                {
                    var dialog=$('#'+id);
                    dialog.on('click','.btn-save',function(){
                        var form=dialog.find('.form');
                        var formData=getFormData(form);
                        var postData=$.extend({
                            'dataPosted':true,
                            'formData':formData
                        },additionalData);
                        request_addEditNavigationMenuBlockCategory(function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                if(response.meta.customStatus==expectedStatus)
                                {
                                    dialog.modal('hide');
                                    refreshNavigationMenuBlockCategoriesManagementWindow(pageBlockId);
                                }
                            }
                        },postData);
                    });
                }
            },withSaveButton);
        }
    },postData);
}
function handleNavigationMenuBlockCategoriesManagementWindow(pageBlockId)
{
    var viewRequestGetFunction=request_getNavigationMenuBlockCategoriesManagementView;
    var viewGetCallback=null;
    var togglePublishingCallback=function(published,elementId,button){
        request_toggleNavigationMenuBlockCategoryPublishing(function(response){
            if(requestOkAndSessionExist(response))
            {
                var status=response.meta.customStatus;
                if(status=='PUBLISHED')
                {

                }
                else if(status=='UNPUBLISHED')
                {

                }
            }
        },{
            'pageBlockId':pageBlockId,
            'categoryId':elementId,
            'published':published?1:0
        });
    };
    var addNewCallback=function(button){
        showNavigationMenuBlockAddEditCategoryWindow('add','Dodaj nową kategorię','Zapisz','Anuluj',{
            'pageBlockId':pageBlockId
        });
    };
    var actionCallback=function(elementId,action,button){
        action=$.trim(action).toLowerCase();
        if(action=='edit')
        {
            showNavigationMenuBlockAddEditCategoryWindow('edit','Edytuj kategorię','Zapisz','Anuluj',{
                'pageBlockId':pageBlockId,
                'categoryId':elementId
            });
        }
        else if(action=='delete')
        {
            jConfirm('Czy na pewno chcesz usunąć tę kategorię?','Potwierdź usunięcie kategorii','dialog-confirm-category-removal',function(id,confirmed){
                if(confirmed)
                {
                    request_deleteNavigationMenuBlockCategory(function(response){
                        if(requestOkAndSessionExist(response))
                        {
                            if(response.meta.customStatus=='MENU_CATEGORY_DELETED')
                            {
                                refreshNavigationMenuBlockCategoriesManagementWindow(pageBlockId);
                                refreshPageBlockManagementWindow(pageBlockId);
                            }
                        }
                    },{
                        'pageBlockId':pageBlockId,
                        'categoryId':elementId
                    });
                }
            });
        }
    };
    var onCloseCallback=function(button){
        refreshPageBlockManagementWindow(pageBlockId);
    };
    var otherSelectorOrInstance=$('#dialog-block-management .btn-manage-categories');
    var optionalCssClassPrefix='categories-';
    var otherTitlePrefix='Kategorie menu';
    handlePageBlockManagementWindow(
        pageBlockId,
        viewRequestGetFunction,
        viewGetCallback,
        togglePublishingCallback,
        addNewCallback,
        actionCallback,
        onCloseCallback,
        otherSelectorOrInstance,
        optionalCssClassPrefix,
        otherTitlePrefix
    );
}
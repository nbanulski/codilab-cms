function blockAccordion_request_reorderTabs(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-accordion/reorder-panels'),doneCallback,postData);
}
function blockAccordion_request_addEditPanel(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-accordion/add-edit-panel'),doneCallback,postData);
}
function blockAccordion_request_deletePanel(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-accordion/delete-panel'),doneCallback,postData);
}
function blockAccordion_request_updatePanelName(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-accordion/update-panel-name'),doneCallback,postData);
}
function blockAccordion_request_updatePanelContents(postData,doneCallback)
{
    makePostRequest(makeCmsUrl('block-accordion/update-panel-contents'),doneCallback,postData);
}
function blockAccordion_deletePanel(pageBlockId,panelId,doneCallback)
{
    var postData={
        'pageBlockId':pageBlockId,
        'panelId':panelId
    };
    blockAccordion_request_deletePanel(postData,doneCallback);
}
function blockAccordion_updatePanelName(pageBlockId,panelId,panelName,doneCallback)
{
    var postData={
        'pageBlockId':pageBlockId,
        'panelId':panelId,
        'panelName':panelName
    };
    blockAccordion_request_updatePanelName(postData,doneCallback);
}
function blockAccordion_updatePanelContents(pageBlockId,panelId,panelContents,doneCallback)
{
    var postData={
        'pageBlockId':pageBlockId,
        'panelId':panelId,
        'panelContents':panelContents
    };
    blockAccordion_request_updatePanelContents(postData,doneCallback);
}

function blockAccordion_handleCustomFunctionality(pageBlockId)
{
    var blockContent=$('.slot-item[data-page-block-id="'+pageBlockId+'"] .block-content');
    var panelsControlList=blockContent.find('.panel-group.accordion');
    blockContent.find('.panel-collapse').off('show.bs.collapse').on('show.bs.collapse',function(){
        var panel=$(this).closest('.panel');
        var panelIndicator=panel.find('.panel-status-indicator');
        var glyphiconTag=panelIndicator.find('.fa');
        glyphiconTag.removeClass('fa-sort-asc').addClass('fa-sort-desc');
    }).off('hide.bs.collapse').on('hide.bs.collapse',function(){
        var panel=$(this).closest('.panel');
        var panelIndicator=panel.find('.panel-status-indicator');
        var glyphiconTag=panelIndicator.find('.fa');
        glyphiconTag.removeClass('fa-sort-desc').addClass('fa-sort-asc');
    });
    
    if(isDesignModeEnabled())
    {
        panelsControlList.sortable({
            'items':">div:not(.non-sortable-panel)",
            'handle':'.panel-heading',
            'cancel':'.delete-this-panel, .panel-status-indicator',
            'stop':function(event,ui)
            {
                var panelWrapper=ui.item.parent();
                var panels=[];
                panelWrapper.children('div.panel:not(.non-sortable-panel)').each(function(){
                    var panel=$(this);
                    var panelId=panel.attr('data-panel-id');
                    panels.push(panelId);
                });
                var postData={
                    'pageBlockId':pageBlockId,
                    'panels':panels
                };
                blockAccordion_request_reorderTabs(postData);
            }
        });

        blockContent.find('.add-new-panel').off('click').on('click',function()
        {
            var postData={
                'pageBlockId':pageBlockId
            };
            blockAccordion_request_addEditPanel(postData,function(response){
                if(requestOkAndSessionExist(response))
                {
                    var expectedStatus='PANEL_ADDED';
                    if(response.meta.customStatus==expectedStatus)
                    {
                        var additionalData={
                            'panelId':response.data
                        }
                        renderBlockAgain(pageBlockId,additionalData);
                    }
                }
            });
            return false;
        });        
        blockContent.find('.panel-title')
            .off('dblclick','> a:not(.add-new-panel)')
            .on('dblclick','> a:not(.add-new-panel)',function()
            {
                var panelTitle=$(this);
                panelTitle.prop('contenteditable',true);
                panelTitle.focus();
                panelTitle.off('blur').on('blur',function(){
                    var panelId=panelTitle.attr('data-panel-id');
                    var panelName=$.trim(panelTitle.text());
                    blockAccordion_updatePanelName(pageBlockId,panelId,panelName);
                    panelTitle.removeAttr('contenteditable');
                });
                return false;
            });
           
       blockContent.find('.panel-body')
           .off('dblclick')
           .on('dblclick',function()
            {
                var textDiv=$(this);
                var panelId=textDiv.attr('data-panel-id');
                var prefixForCkEditors='accordion-'+pageBlockId+'-';

                attachInlineCkEditorToDomElement(
                    blockContent,
                    '.panel-body',
                    prefixForCkEditors+'about',
                    true,
                    function(eventContainer,textContainer,oldTextContainerId,ckEditorId,kcFinderAttached,contents){
                        blockAccordion_updatePanelContents(pageBlockId,panelId,contents,function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                var expectedStatus='PANEL_CONTENTS_UPDATED';
                                if(response.meta.customStatus==expectedStatus)
                                {
                                    var additionalData={
                                        'panelId':panelId
                                    }
                                    renderBlockAgain(pageBlockId,additionalData);
                                }
                            }
                        });
                    }
                );
            });
        blockContent.find('.delete-this-panel').off('click').on('click',function(){
            var panelId=$(this).closest('.panel').attr('data-panel-id');
            var panelName=$.trim($(this).siblings('a').text());
            var title='Potwierdź usunięcie panelu';
            var contents='Czy na pewno chcesz usunąć panel &quot;'+panelName+'&quot; ?';
            var customId='dialog-confirm-panel-deletion';
            jConfirm(contents,title,customId,function(id,confirmed){
                if(confirmed)
                {
                    blockAccordion_deletePanel(pageBlockId,panelId,function(response){
                        if(requestOkAndSessionExist(response))
                        {
                            var expectedStatus='PANEL_DELETED';
                            if(response.meta.customStatus==expectedStatus)
                            {
                                renderBlockAgain(pageBlockId);
                            }
                        }
                    });
                }
            });

            return false;
        });    
    }
}
loadPlUploadLibrary();
function blockNewsList_request_addEditNews(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-news-list/add-edit-news'),doneCallback,postData);
}
function blockNewsList_request_deleteNews(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-news-list/delete-news'),doneCallback,postData);
}
function blockNewsList_request_toggleNewsPublishing(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-news-list/toggle-news-publishing'),doneCallback,postData);
}
function blockNewsList_handleCustomFunctionality(pageBlockId,optionalSourcePageBlockId)
{
    var sourcePageBlockId=pageBlockId;
    if((typeof optionalSourcePageBlockId!='undefined')&&(optionalSourcePageBlockId>0))
    {
        sourcePageBlockId=toInt(optionalSourcePageBlockId);
    }

    var block=$('.slot-item[data-page-block-id="'+pageBlockId+'"]');
    var blockContent=block.find('.block-content > .content');

    if (isDesignModeEnabled())
    {
        blockContent.find('.item').hover(function(){
            $(this).find('.news-edit-btn').css('display','inline-block');
        },function(){
            $(this).find('.news-edit-btn').css('display','none');
        });

        blockContent.off('click','.item .news-edit-btn').on('click','.item .news-edit-btn',function(){
            var postData={
                'pageBlockId':sourcePageBlockId,
                'newsId':$(this).closest('.item').attr('data-news-id')
            };
            blockNewsList_showAddEditWindow('edit','Edytuj aktualność','Zapisz','Anuluj',postData,true);
        });
    }
}
function blockNewsList_handlePagination(pageBlockId)
{
    var block=$('.slot-item[data-page-block-id="'+pageBlockId+'"]');
    block.find('.block-content .content .pagination')
         .off('click','li:not(.disabled):not(.active) a')
         .on('click','li:not(.disabled):not(.active) a',function(){
        var anchor=$(this);
        var text=$.trim(anchor.text());
        var pageNumber=null;
        if(text=='<<')
        {
            var activeAnchor=anchor.closest('ul').find('li.active a');
            var currentPageNumber=toInt($.trim(activeAnchor.text()));
            pageNumber=currentPageNumber-1;
        }
        else if(text=='>>')
        {
            var activeAnchor=anchor.closest('ul').find('li.active a');
            var currentPageNumber=toInt($.trim(activeAnchor.text()));
            pageNumber=currentPageNumber+1;
        }
        else
        {
            pageNumber=toInt(text);
        }

        renderBlockAgain(pageBlockId,{'pageNumber':pageNumber});

        return false;
    });
}
function blockNewsList_showAddEditWindow(
    type,title,btn_save_text,btn_cancel_text,additionalData,windowCalledFromInlineEdit
)
{
    loadJsFile('/js/admin/bootstrap-datepicker.min.js');
    var pageBlockId=additionalData.pageBlockId;
    var postData={};
    postData=$.extend(postData,additionalData);
    request_getBlockAddEditView(function(response){
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            var withSaveButton=true;
            jDialog(contents,title,'dialog-add-edit-news',function(id,success){
                if(success)
                {
                    var dialog=$('#'+id);
                    dialog.on('click change','#ae-news-categories',function(){
                        var categoryId=$.trim($(this).find('option:selected').val());
                    });
                    dialog.on('click keydown keyup paste blur','#ae-news-title',function(){
                        var cleanUrlInput=dialog.find('#ae-clean-url');
                        var title=$(this).val();
                        var cleanUrl=generateCleanUrlFromText(title);
                        cleanUrlInput.val(cleanUrl);
                        dialog.find('#ae-clean-url-path-span').text(cleanUrl);
                    });
                    dialog.on('click keydown keyup paste blur','#ae-clean-url',function(){
                        var cleanUrl= $.trim($(this).val());
                        dialog.find('#ae-clean-url-path-span').text(cleanUrl);
                    });
                    dialog.find('#ae-news-date').datepicker({
                        format:'yyyy-mm-dd'
                    });
                    dialog.find('#ae-news-start-hour').timepicker({
                        showMeridian:false
                    });

                    attachCkEditorToDomElement(
                        dialog.find('#ae-news-contents'),
                        true
                    );

                    dialog.find('#ae-news-teaser').limiter(350,dialog.find('#ae-news-teaser-length'));
                    dialog.on('click','.btn-save',function(){
                        var form=dialog.find('.form');
                        var formData=getFormData(form);
                        var newsImagesOrder=[];
                        dialog.find('#ae-news-image-form-group [data-image-number]').each(function(){
                            var div=$(this);
                            var uniqueIdentifier=div.attr('data-unique-identifier');
                            var uniqueHashForFileName=
                                div.find('#iu-unique-hash-for-file-name-'+uniqueIdentifier).val();
                            newsImagesOrder.push(uniqueHashForFileName);
                        });
                        formData=$.extend(formData,{
                            'newsImagesOrder':newsImagesOrder
                        });
                        var postData=$.extend({
                            'dataPosted':true,
                            'formData':formData
                        },additionalData);
                        blockNewsList_request_addEditNews(function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                var expectedStatus=(type=='add')?'NEWS_ADDED':'NEWS_MODIFIED';
                                if(response.meta.customStatus==expectedStatus)
                                {
                                    dialog.modal('hide');
                                    refreshPageBlockManagementWindow(pageBlockId);
                                    if (windowCalledFromInlineEdit)
                                    {
                                        renderBlockAgainByBlockName('news-list');
                                        renderBlockAgainByBlockName('news-details');
                                    }
                                }
                            }
                        },postData);
                    });
                }
            },
            withSaveButton,
                function(){
                    if(windowCalledFromInlineEdit)
                    {
                        renderBlockAgainByBlockName('news-list');
                        renderBlockAgainByBlockName('news-details');
                    }
                }
            );
        }
    },postData);
}
function blockNewsList_handleManagementWindow(pageBlockId)
{
    var viewRequestGetFunction=request_getBlockManagementView;
    var viewGetCallback=null;
    var togglePublishingCallback=function(published,elementId,button){
        var postData={
            'pageBlockId':pageBlockId,
            'newsId':elementId,
            'published':published?1:0
        };
        blockNewsList_request_toggleNewsPublishing(function(response){
            if(requestOkAndSessionExist(response))
            {
                var status=response.meta.customStatus;
                if(status=='PUBLISHED')
                {

                }
                else if(status=='UNPUBLISHED')
                {

                }
            }
        },postData);
    };
    var addNewCallback=function(button){
        blockNewsList_showAddEditWindow('add','Dodaj nową aktualność','Zapisz','Anuluj',{
            'pageBlockId':pageBlockId
        });
    };
    var actionCallback=function(elementId,action,button)
    {
        action=$.trim(action).toLowerCase();
        if(action=='edit')
        {
            blockNewsList_showAddEditWindow('edit','Edytuj aktualność','Zapisz','Anuluj',{
                'pageBlockId':pageBlockId,
                'newsId':elementId
            });
        }
        else if(action=='delete')
        {
            jConfirm('Czy na pewno chcesz usunąć tę aktualność?','Potwierdź usunięcie aktualności','dialog-confirm-news-removal',function(id,confirmed){
                if(confirmed)
                {
                    var postData={
                        'pageBlockId':pageBlockId,
                        'newsId':elementId
                    };
                    blockNewsList_request_deleteNews(function(response){
                        if(requestOkAndSessionExist(response))
                        {
                            if(response.meta.customStatus=='NEWS_DELETED')
                            {
                                refreshPageBlockManagementWindow(pageBlockId);
                            }
                        }
                    },postData);
                }
            });
        }
    };
    var onCloseCallback=function(button){
        renderBlockAgainByBlockName('news-list');
        renderBlockAgainByBlockName('news-details');
    };

    handlePageBlockManagementWindow(pageBlockId,viewRequestGetFunction,viewGetCallback,togglePublishingCallback,addNewCallback,actionCallback,onCloseCallback);
}
loadJsFile('/js/admin/bootstrap-timepicker.min.js');
loadJsFile('/js/admin/moment.min.js');
//loadJsFile('/js/admin/daterangepicker.min.js');
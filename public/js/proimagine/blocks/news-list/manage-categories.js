function request_getNewsBlockCategoriesManagementView(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-news-list/get-categories-management-view'),doneCallback,postData);
}
function request_toggleNewsBlockCategoryPublishing(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-news-list/toggle-category-publishing'),doneCallback,postData);
}
function request_getNewsBlockAddEditCategoryView(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-news-list/get-add-edit-category-view'),doneCallback,postData);
}
function request_addEditNewsBlockCategory(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-news-list/add-edit-category'),doneCallback,postData);
}
function request_deleteNewsBlockCategory(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-news-list/delete-category'),doneCallback,postData);
}
function refreshNewsBlockCategoriesManagementWindow(pageBlockId)
{
    request_getNewsBlockCategoriesManagementView(function(response){
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            if(contents!='')
            {
                $('#dialog-block-categories-management .modal-body').html(contents);
            }
        }
    },{
        'pageBlockId':pageBlockId
    });
}
function showNewsBlockAddEditCategoryWindow(type,title,btn_save_text,btn_cancel_text,additionalData)
{
    var pageBlockId=additionalData.pageBlockId;
    var expectedStatus=(type=='add')?'NEWS_CATEGORY_ADDED':'NEWS_CATEGORY_MODIFIED';
    var postData={};
    postData=$.extend(postData,additionalData);
    request_getNewsBlockAddEditCategoryView(function(response){
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            var withSaveButton=true;
            jDialog(contents,title,'dialog-add-edit-category',function(id,success){
                if(success)
                {
                    var dialog=$('#'+id);
                    dialog.on('click','.btn-save',function(){
                        var form=dialog.find('.form');
                        var formData=getFormData(form);
                        var postData=$.extend({
                            'dataPosted':true,
                            'formData':formData
                        },additionalData);
                        request_addEditNewsBlockCategory(function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                if(response.meta.customStatus==expectedStatus)
                                {
                                    dialog.modal('hide');
                                    refreshNewsBlockCategoriesManagementWindow(pageBlockId);
                                }
                            }
                        },postData);
                    });
                }
            },withSaveButton);
        }
    },postData);
}
function handleNewsBlockCategoriesManagementWindow(pageBlockId)
{
    var viewRequestGetFunction=request_getNewsBlockCategoriesManagementView;
    var viewGetCallback=null;
    var togglePublishingCallback=function(published,elementId,button){
        request_toggleNewsBlockCategoryPublishing(function(response){
            if(requestOkAndSessionExist(response))
            {
                var status=response.meta.customStatus;
                if(status=='PUBLISHED')
                {

                }
                else if(status=='UNPUBLISHED')
                {

                }
            }
        },{
            'pageBlockId':pageBlockId,
            'categoryId':elementId,
            'published':published?1:0
        });
    };
    var addNewCallback=function(button){
        showNewsBlockAddEditCategoryWindow('add','Dodaj nową kategorię','Zapisz','Anuluj',{
            'pageBlockId':pageBlockId
        });
    };
    var actionCallback=function(elementId,action,button){
        action=$.trim(action).toLowerCase();
        if(action=='edit')
        {
            showNewsBlockAddEditCategoryWindow('edit','Edytuj kategorię','Zapisz','Anuluj',{
                'pageBlockId':pageBlockId,
                'categoryId':elementId
            });
        }
        else if(action=='delete')
        {
            jConfirm('Czy na pewno chcesz usunąć tę kategorię?','Potwierdź usunięcie kategorii','dialog-confirm-category-removal',function(id,confirmed){
                if(confirmed)
                {
                    request_deleteNewsBlockCategory(function(response){
                        if(requestOkAndSessionExist(response))
                        {
                            if(response.meta.customStatus=='NEWS_CATEGORY_DELETED')
                            {
                                refreshNewsBlockCategoriesManagementWindow(pageBlockId);
                                refreshPageBlockManagementWindow(pageBlockId);
                            }
                        }
                    },{
                        'pageBlockId':pageBlockId,
                        'categoryId':elementId
                    });
                }
            });
        }
    };
    var onCloseCallback=function(button){
        refreshPageBlockManagementWindow(pageBlockId);
    };
    var otherSelectorOrInstance=$('#dialog-block-management .btn-manage-categories');
    var optionalCssClassPrefix='categories-';
    var otherTitlePrefix='Kategorie';
    handlePageBlockManagementWindow(
        pageBlockId,
        viewRequestGetFunction,
        viewGetCallback,
        togglePublishingCallback,
        addNewCallback,
        actionCallback,
        onCloseCallback,
        otherSelectorOrInstance,
        optionalCssClassPrefix,
        otherTitlePrefix
    );
}
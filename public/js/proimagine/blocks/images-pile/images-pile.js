loadJsFile('/js/modernizr.js');
loadBlockJsFile('images-pile','jquery.stapel.min');
function blockImagesPile_request_reorderGroups(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-images-pile/reorder-groups'),doneCallback,postData);
}
function blockImagesPile_request_reorderItemsInGroup(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-images-pile/reorder-items-in-group'),doneCallback,postData);
}
function blockImagesPile_request_toggleItemPublishing(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-images-pile/toggle-item-publishing'),doneCallback,postData);
}
function blockImagesPile_request_addEditItem(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-images-pile/add-edit-item'),doneCallback,postData);
}
function blockImagesPile_request_deleteItem(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-images-pile/delete-item'),doneCallback,postData);
}
function blockImagesPile_handleSettingsFunctionality(pageBlockId)
{
    loadCssFile('/css/bootstrap-colorpicker.min.css');
    loadJsFile('/js/admin/bootstrap-colorpicker.min.js');
    var dialogBlockSettings=$('.modal#dialog-block-settings');
    var blocksSettings=dialogBlockSettings.find('.form[data-page-block-id="'+pageBlockId+'"]');
    var backgroundColorGroup=blocksSettings.find('.group-with-colorpicker');
    var hiddenInput=blocksSettings.find('input[type="hidden"][name="backgroundColor"]');
    var backgroundColor=hiddenInput.val();
    backgroundColorGroup.colorpicker().on('changeColor',function(e){
        hiddenInput.val(e.color.toHex());
    }).colorpicker('setValue',backgroundColor);
    blocksSettings.on('click','[name="backgroundColorEnabled"]',function(){
        var checked=$(this).prop('checked');
        backgroundColorGroup.find('input').prop('disabled',!checked);
    });
}
function blockImagesPile_handleCustomFunctionality(pageBlockId,userSettings,optionalSourcePageBlockId)
{
    var block=$('.slot-item[data-page-block-id="'+pageBlockId+'"]');
    var blockContent=block.find('.block-content > .content');
    var tpGrid=blockContent.find('.tp-grid');

    var settings=$.extend({
        onLoad:function(){
            blockContent.find('.loader-bg').remove();
            tpGrid.find('.tp-info').hide();
            tpGrid.show();
        },
        onBeforeOpen:function(pileName){
            blockContent.find('.pile-name').html(pileName);
        },
        onAfterOpen:function(pileName){
            blockContent.find('.close-pile-btn').show();
            tpGrid.find('li:visible').find('.tp-info').show();
        }
    },userSettings);

    var stapel=tpGrid.stapel(settings);

    blockContent.off('click','.close-pile-btn').on('click','.close-pile-btn',function() {
        $(this).hide();
        blockContent.find('.pile-name').empty();
        tpGrid.find('li:visible').find('.tp-info').hide();
        stapel.closePile();
    });
}
function blockImagesPile_openFirstGroup(pageBlockId)
{
    var block=$('.slot-item[data-page-block-id="'+pageBlockId+'"]');
    var blockContent=block.find('.block-content > .content');
    var tpGrid=blockContent.find('.tp-grid');
    var firstGroupName=tpGrid.find('>li[data-pile]:first').attr('data-pile');

    blockImagesPile_openGroup(pageBlockId,firstGroupName);
}
function blockImagesPile_openGroup(pageBlockId,groupName)
{
    var block=$('.slot-item[data-page-block-id="'+pageBlockId+'"]');
    var blockContent=block.find('.block-content > .content');
    var tpGrid=blockContent.find('.tp-grid');

    tpGrid.imagesLoaded(function(){
        blockContent.find('.close-pile-btn').show();
        blockContent.find('.pile-name').text(groupName);

        var li=tpGrid.find('>li[data-pile="'+groupName+'"]:last');
        if(li.length>0)
        {
            li.trigger('click');
        }
    });
}
function blockImagesPile_showAddEditWindow(type,title,btn_save_text,btn_cancel_text,additionalData)
{
    var pageBlockId=additionalData.pageBlockId;
    var postData={};
    postData=$.extend(postData,additionalData);
    request_getBlockAddEditView(function(response){
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            var withSaveButton=true;
            jDialog(contents,title,'dialog-add-edit-item',function(id,success){
                if(success)
                {
                    var dialog=$('#'+id);
                    var urlAddressTypeDropdown=dialog.find('#ai-item-url-address-type');
                    var urlAddressType=urlAddressTypeDropdown.find('option:selected').val();
                    var isInternalUrl=(urlAddressType=='0');

                    dialog.on('click change','#ai-item-url-address-type',function(){
                        var urlAddressType=$.trim(urlAddressTypeDropdown.find('option:selected').val());
                        if(urlAddressType=='0')
                        {
                            dialog.find('#ai-item-internal-page-id').show();
                            dialog.find('#ai-item-external-url').hide();
                            dialog.find('#ai-item-url-form-group').show();
                        }
                        else if(urlAddressType=='1')
                        {
                            dialog.find('#ai-item-internal-page-id').hide();
                            dialog.find('#ai-item-external-url').show();
                            dialog.find('#ai-item-url-form-group').show();
                        }
                        else
                        {
                            dialog.find('#ai-item-url-form-group').hide();
                            dialog.find('#ai-item-internal-page-id').hide();
                            dialog.find('#ai-item-external-url').hide();
                        }
                    });
                    dialog.on('click change','#ai-item-internal-page-id',function(){
                        var urlAddressType=urlAddressTypeDropdown.find('option:selected').val();
                        var isInternalUrl=(urlAddressType=='0');
                        if(isInternalUrl)
                        {
                            var internalPageIdDropdown=dialog.find('#ai-item-internal-page-id');
                            var selectedOption=internalPageIdDropdown.find('option:selected');

                            // Set item title the same as selected internal page title:
                            var selectedInternalPageTitle=$.trim(selectedOption.attr('data-title'));
                            dialog.find('#ai-item-title').val(selectedInternalPageTitle);
                        }
                    });
                    var itemTitleInput=dialog.find('#ai-item-title');
                    var itemTitle=$.trim(itemTitleInput.val());
                    if(isInternalUrl&&(itemTitle==''))
                    {
                        var internalPageIdDropdown=dialog.find('#ai-item-internal-page-id');
                        var selectedOption=internalPageIdDropdown.find('option:selected');

                        // Set item title the same as selected internal page title:
                        var selectedInternalPageTitle=$.trim(selectedOption.attr('data-title'));
                        itemTitleInput.val(selectedInternalPageTitle);
                    }
                    dialog.on('click','.btn-save',function(){
                        var form=dialog.find('.form');
                        var formData=getFormData(form);
                        var postData=$.extend({
                            'dataPosted':true,
                            'formData':formData
                        },additionalData);
                        blockImagesPile_request_addEditItem(function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                var expectedStatus=(type=='add')?'ITEM_ADDED':'ITEM_MODIFIED';
                                if(response.meta.customStatus==expectedStatus)
                                {
                                    dialog.modal('hide');
                                    refreshPageBlockManagementWindow(pageBlockId);
                                }
                            }
                        },postData);
                    });
                }
            },withSaveButton);
        }
    },postData);
}
function blockImagesPile_handleManagementWindow(pageBlockId)
{
    var viewRequestGetFunction=request_getBlockManagementView;
    var viewGetCallback=null;
    var togglePublishingCallback=function(published,elementId,button){
        blockImagesPile_request_toggleItemPublishing(function(response){
            if(requestOkAndSessionExist(response))
            {
                var status=response.meta.customStatus;
                if(status=='PUBLISHED')
                {

                }
                else if(status=='UNPUBLISHED')
                {

                }
            }
        },{
            'pageBlockId':pageBlockId,
            'itemId':elementId,
            'published':published?1:0
        });
    };
    var addNewCallback=function(button){
        blockImagesPile_showAddEditWindow('add','Dodaj nowy obrazek','Zapisz','Anuluj',{
            'pageBlockId':pageBlockId
        });
    };
    var actionCallback=function(elementId,action,button)
    {
        action=$.trim(action).toLowerCase();
        if(action=='edit')
        {
            blockImagesPile_showAddEditWindow('edit','Edytuj obrazek','Zapisz','Anuluj',{
                'pageBlockId':pageBlockId,
                'itemId':elementId
            });
        }
        else if(action=='delete')
        {
            var title='Potwierdź usunięcie obrazka';
            var contents='Czy na pewno chcesz usunąć ten obrazek?';
            var id='dialog-confirm-item-removal';
            jConfirm(contents,title,id,function(id,confirmed){
                if(confirmed)
                {
                    var postData={
                        'pageBlockId':pageBlockId,
                        'itemId':elementId
                    };
                    blockImagesPile_request_deleteItem(function(response){
                        if(requestOkAndSessionExist(response))
                        {
                            if(response.meta.customStatus=='ITEM_DELETED')
                            {
                                refreshPageBlockManagementWindow(pageBlockId);
                            }
                        }
                    },postData);
                }
            });
        }
    };
    var onCloseCallback=function(button){
        renderBlockAgain(pageBlockId);
    };
    var otherSelectorOrInstance=null;
    var optionalCssClassPrefix=null;
    var otherTitlePrefix=null;
    var optionalSourcePageBlockId=null;
    var onOpenCallback=function(dialog){
        var sortable=blockImagesPile_attachSortableToManagementWindow(dialog,pageBlockId);

        dialog
            .off('mouseenter','td.actions [data-action="move"]')
            .on('mouseenter','td.actions [data-action="move"]',function(){
            var groupId=$(this).closest('tr').attr('data-group-id');

            if(!dialog.find('.table').hasClass('ui-sortable'))
            {
                sortable=blockImagesPile_attachSortableToManagementWindow(dialog,pageBlockId);
            }

            // Limit the sortable to sort items from specific group only:
            sortable.sortable('option','items','tr[data-group-id="'+groupId+'"]');

            return false;
        });
    };
    handlePageBlockManagementWindow(
        pageBlockId,viewRequestGetFunction,viewGetCallback,togglePublishingCallback,addNewCallback,actionCallback,
        onCloseCallback,otherSelectorOrInstance,optionalCssClassPrefix,otherTitlePrefix,optionalSourcePageBlockId,
        onOpenCallback
    );
}
function blockImagesPile_attachSortableToManagementWindow(dialog,pageBlockId)
{
    var table=dialog.find('.table');
    var sortable=table.sortable({
        'handle':'td.actions [data-action="move"]',
        'sort':function(event,ui){
            var draggedElement=$(ui.item[0]);
            var draggedElementHeight=draggedElement.outerHeight();
            var placeholder=draggedElement.parent().find('.ui-sortable-placeholder');
            placeholder.removeClass('ui-sortable-placeholder');
            placeholder.css({
                'background':'#fcefa1',
                'border':'1px solid orange',
                'height':draggedElementHeight+'px',
                'visibility':'visible'
            });
        },
        'stop':function(event,ui){
            var groupId=ui.item.attr('data-group-id');
            var items=[];
            table.find('tr[data-group-id="'+groupId+'"]').each(function(){
                var itemId=$(this).attr('data-element-id');
                items.push(itemId);
            });

            var postData={
                'pageBlockId':pageBlockId,
                'groupId':groupId,
                'items':items
            };
            blockImagesPile_request_reorderItemsInGroup(function(){

            },postData);
        }
    });

    return sortable;
}
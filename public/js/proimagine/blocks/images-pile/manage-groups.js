function blockImagesPile_request_getGroupsManagementView(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-images-pile/get-groups-management-view'),doneCallback,postData);
}
function blockImagesPile_request_toggleGroupPublishing(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-images-pile/toggle-group-publishing'),doneCallback,postData);
}
function blockImagesPile_request_getAddEditGroupView(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-images-pile/get-add-edit-group-view'),doneCallback,postData);
}
function blockImagesPile_request_addEditGroup(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-images-pile/add-edit-group'),doneCallback,postData);
}
function blockImagesPile_request_deleteGroup(doneCallback,postData)
{
    makePostRequest(makeCmsUrl('block-images-pile/delete-group'),doneCallback,postData);
}
function blockImagesPile_refreshGroupsManagementWindow(pageBlockId)
{
    var postData={
        'pageBlockId':pageBlockId
    };
    blockImagesPile_request_getGroupsManagementView(function(response){
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            if(contents!='')
            {
                $('#dialog-block-groups-management .modal-body').html(contents);
            }
        }
    },postData);
}
function blockImagesPile_showAddEditGroupWindow(type,title,btn_save_text,btn_cancel_text,additionalData)
{
    var pageBlockId=additionalData.pageBlockId;
    var expectedStatus=(type=='add')?'GROUP_ADDED':'GROUP_MODIFIED';
    var postData={};
    postData=$.extend(postData,additionalData);
    blockImagesPile_request_getAddEditGroupView(function(response){
        if(requestOkAndSessionExist(response))
        {
            var contents=response.data;
            var withSaveButton=true;
            jDialog(contents,title,'dialog-add-edit-group',function(id,success){
                if(success)
                {
                    var dialog=$('#'+id);
                    dialog.on('click','.btn-save',function(){
                        var form=dialog.find('.form');
                        var formData=getFormData(form);
                        var postData=$.extend({
                            'dataPosted':true,
                            'formData':formData
                        },additionalData);
                        blockImagesPile_request_addEditGroup(function(response){
                            if(requestOkAndSessionExist(response))
                            {
                                if(response.meta.customStatus==expectedStatus)
                                {
                                    dialog.modal('hide');
                                    blockImagesPile_refreshGroupsManagementWindow(pageBlockId);
                                }
                            }
                        },postData);
                    });
                }
            },withSaveButton);
        }
    },postData);
}
function blockImagesPile_handleGroupsManagementWindow(pageBlockId)
{
    var viewRequestGetFunction=blockImagesPile_request_getGroupsManagementView;
    var viewGetCallback=null;
    var togglePublishingCallback=function(published,elementId,button){
        var postData={
            'pageBlockId':pageBlockId,
            'groupId':elementId,
            'published':published?1:0
        };
        blockImagesPile_request_toggleGroupPublishing(function(response){
            if(requestOkAndSessionExist(response))
            {
                var status=response.meta.customStatus;
                if(status=='PUBLISHED')
                {

                }
                else if(status=='UNPUBLISHED')
                {

                }
            }
        },postData);
    };
    var addNewCallback=function(button){
        blockImagesPile_showAddEditGroupWindow('add','Dodaj nową grupę','Zapisz','Anuluj',{
            'pageBlockId':pageBlockId
        });
    };
    var actionCallback=function(elementId,action,button){
        action=$.trim(action).toLowerCase();
        if(action=='edit')
        {
            blockImagesPile_showAddEditGroupWindow('edit','Edytuj grupę','Zapisz','Anuluj',{
                'pageBlockId':pageBlockId,
                'groupId':elementId
            });
        }
        else if(action=='delete')
        {
            jConfirm('Czy na pewno chcesz usunąć tę grupę?','Potwierdź usunięcie grupy','dialog-confirm-group-removal',function(id,confirmed){
                if(confirmed)
                {
                    var postData={
                        'pageBlockId':pageBlockId,
                        'groupId':elementId
                    };
                    blockImagesPile_request_deleteGroup(function(response){
                        if(requestOkAndSessionExist(response))
                        {
                            if(response.meta.customStatus=='GROUP_DELETED')
                            {
                                blockImagesPile_refreshGroupsManagementWindow(pageBlockId);
                                refreshPageBlockManagementWindow(pageBlockId);
                            }
                        }
                    },postData);
                }
            });
        }
    };
    var onCloseCallback=function(button){
        refreshPageBlockManagementWindow(pageBlockId);
    };
    var otherSelectorOrInstance=$('#dialog-block-management .btn-manage-groups');
    var optionalCssClassPrefix='groups-';
    var otherTitlePrefix='Grupy';
    var optionalSourcePageBlockId=null;
    var onOpenCallback=function(dialog){
        blockImagesPile_attachSortableToGroupsManagementWindow(dialog,pageBlockId);

        dialog
            .off('mouseenter','td.actions [data-action="move"]')
            .on('mouseenter','td.actions [data-action="move"]',function(){
                if(!dialog.find('.table').hasClass('ui-sortable'))
                {
                    blockImagesPile_attachSortableToGroupsManagementWindow(dialog,pageBlockId);
                }

                return false;
            });
    };
    handlePageBlockManagementWindow(
        pageBlockId,
        viewRequestGetFunction,
        viewGetCallback,
        togglePublishingCallback,
        addNewCallback,
        actionCallback,
        onCloseCallback,
        otherSelectorOrInstance,
        optionalCssClassPrefix,
        otherTitlePrefix,
        optionalSourcePageBlockId,
        onOpenCallback
    );
}
function blockImagesPile_attachSortableToGroupsManagementWindow(dialog,pageBlockId)
{
    var table=dialog.find('.table');
    table.sortable({
        'items':'tr[data-element-id]',
        'handle':'td.actions [data-action="move"]',
        'sort':function(event,ui){
            var draggedElement=$(ui.item[0]);
            var draggedElementHeight=draggedElement.outerHeight();
            var placeholder=draggedElement.parent().find('.ui-sortable-placeholder');
            placeholder.removeClass('ui-sortable-placeholder');
            placeholder.css({
                'background':'#fcefa1',
                'border':'1px solid orange',
                'height':draggedElementHeight+'px',
                'visibility':'visible'
            });
        },
        'stop':function(event,ui){
            var groups=[];
            table.find('tr[data-element-id]').each(function(){
                var groupId=$(this).attr('data-element-id');
                groups.push(groupId);
            });

            var postData={
                'pageBlockId':pageBlockId,
                'groups':groups
            };
            blockImagesPile_request_reorderGroups(function(){

            },postData);
        }
    });
}
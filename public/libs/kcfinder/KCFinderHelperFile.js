//Works when called in document-ready function.

CKEDITOR.on( 'dialogDefinition', function( evt )
{
    // Dialog definition  image https://github.com/ckeditor/ckeditor-dev/blob/dc8dccc5eb8c22b69a1061ea130cf090da4e3ebf/plugins/image/dialogs/image.js#L437
    var dialog = evt.data;

    if ( dialog.name == 'image' )
    {
        // Get dialog definition.
        var def = evt.data.definition;
        var infoTab = def.getContents( 'info' );
        var advTab = def.getContents( 'advanced' );

        infoTab.add(
        {
            type: 'button',
            id: 'compressCkImage',
            label: 'Compress Image',
            onClick: function()
            {
                var theDialog = CKEDITOR.dialog.getCurrent();
                var button = this;
                var url = theDialog.getValueOf('info','txtUrl');
                if(
                    (url == undefined) ||
                    (url == "") ||
                    (url == null)
                  )
                {
                        return;
                }

                var width = theDialog.getValueOf('info','txtWidth');
                var height = theDialog.getValueOf('info','txtHeight');
                var tempUrlArray = url.split("/");
                var fileName = decodeURI(tempUrlArray[tempUrlArray.length-1]);
                var newFileName = fileName.split(".")[0] + "_"+width+"_"+height+"."+fileName.split(".")[1];
                tempUrlArray.pop();
                var directoryPath = decodeURI(Array.prototype.join.call(tempUrlArray,'/'));

                $.ajax(
                    {
                        url: makeCmsUrl('kc-finder/compress'),
                        data: {
                                   name: fileName,
                                   path: directoryPath,
                                   width: width,
                                   height: height,
                                   output: newFileName
                               },
                        type: 'post',
                        dataType: 'json',
                        success: function(output)
                        {
                           if(output.data == 1)
                           {
                               var outputLink = "";
                               for(var i = 0;i<tempUrlArray.length-1;i++)
                               {
                                   outputLink = outputLink+tempUrlArray[i]+"/";
                               }
                               outputLink = directoryPath+'/'+newFileName;
                               theDialog.setValueOf('info','txtUrl',encodeURI(outputLink));
                               $('.image-compress-success').show();
                           }
                           else if(output.data == 0)
                           {
                                   console.log("Could Not Access File");
                           }
                           else
                           {
                                   console.log("Unknown Output");
                           }
                           button.disable();
                         }
                    });
                }
        });

        infoTab.add(
        {
            type: 'button',
            id: 'makeImageResponsive',
            label: 'Make Responsive',
            title: 'Make Responsive',
            onClick: function()
            {
                var theDialog = CKEDITOR.dialog.getCurrent();

                theDialog.setValueOf('advanced','txtGenClass',"img-responsive");
                theDialog.setValueOf('advanced','txtdlgGenStyle',"");
                theDialog.setValueOf('info','txtHeight',"");
                theDialog.setValueOf('info','txtWidth',"");

                $('.image-responsive-success').show();
            }
        });

        infoTab.add(
        {
            type: 'html',
            html: '<span style="color:forestgreen; display: none;" class="image-compress-success">Image successfully compressed to the new resolution. </span>'
        },'makeImageResponsive');

        infoTab.add(
        {
            type: 'html',
            html: '<span style="color:forestgreen; display: none;" class="image-responsive-success">Successfully removed width and height attributes to have a responsive image. </span>'
        });
    }
});


<?php


class ImageController
{
    //protected $fileSystem = null;
    protected $defaultFormatExtension = 'png';
    protected $jpegQuality = 100;
    protected $pngQuality = 9;

	public function __construct(){
		
		error_reporting(E_ALL);
		ini_set('display_errors', true);

	}

    public function getImageDimensions($imageFilePath)
    {
        if (is_file($imageFilePath))
        {
            list($width, $height) = getimagesize($imageFilePath);

            return array($width, $height);
        }

        return array(0, 0);
    }

    public function calculateAspectRatio($width, $height)
    {
        $aspectRatio = false;

        if ($height != 0)
        {
            $aspectRatio = $width / $height;
        }

        return $aspectRatio;
    }

    public function adjustDimensionsUsingAspectRatio(&$width, &$height, $aspectRatio)
    {
        if (($height != 0) && ($aspectRatio != 0.00))
        {
            if ($width / $height > $aspectRatio)
            {
                $width = $height * $aspectRatio;
            }
            else
            {
                $height = $width / $aspectRatio;
            }

            return true;
        }

        return false;
    }

    /**
     * 	If $destinationFormat is empty then this function uses the format specified in the destination path.
     * 	If the destination path does not include the file extension then the function uses the format of the source path.
     * 	If the source path does not contain a file extension then the function uses the default "png".
     */
    public function resizeImageToSpecificWidth($sourceFilePath, $destinationFilePath, $width = null, $destinationFormat = null)
    {
        list($originalWidth, $originalHeight) = $this->getImageDimensions($sourceFilePath);
        $height = $originalHeight;

        return $this->resizeImage($sourceFilePath, $destinationFilePath, $width, $height, $destinationFormat);

        return false;
    }

    /**
     * 	If $destinationFormat is empty then this function uses the format specified in the destination path.
     * 	If the destination path does not include the file extension then the function uses the format of the source path.
     * 	If the source path does not contain a file extension then the function uses the default "png".
     */
    public function resizeImageToSpecificDimensions($sourceFilePath, $destinationFilePath, $width, $height, $destinationFormat = null)
    {
		
        return $this->resizeImage($sourceFilePath, $destinationFilePath, $width, $height, $destinationFormat);
    }


   protected function resizeImage($sourceFilePath, $destinationFilePath, $width, $height, $destinationFormat = null)
    {
        if (is_file($sourceFilePath))
        {
            list($originalWidth, $originalHeight) = getimagesize($sourceFilePath);

            $aspectRatio = $this->calculateAspectRatio($originalWidth, $originalHeight);
            if (!$this->adjustDimensionsUsingAspectRatio($width, $height, $aspectRatio))
            {
                echo 'there is a problem with dimensions or aspect ratio';
                return false;
            }

            $sourceImage = imagecreatefromstring(file_get_contents($sourceFilePath));
            $resampledImage = imagecreatetruecolor($width, $height);
            imagealphablending($sourceImage, false);
            imagesavealpha($sourceImage, true);
            imagecopyresampled($resampledImage, $sourceImage, 0, 0, 0, 0, $width, $height, $originalWidth, $originalHeight);

            $destinationFilePath = trim($destinationFilePath);
            $destinationFilePathExtension = pathinfo($destinationFilePath, PATHINFO_EXTENSION);
            $destinationFormat = $this->recognizeDestinationFormat($sourceFilePath, $destinationFilePathExtension, $destinationFormat);

            /**
             * 	If destination file path does not containt file extension then we must add it.
             */
            if ($destinationFilePathExtension == '')
            {
                $lastChar = substr($destinationFilePath, -1);
                if ($lastChar != '.') // If the last character is not a dot.
                {
                    $destinationFilePath .= '.';
                }
                $destinationFilePath .= $destinationFormat;
            }

            return $this->saveImage($resampledImage, $destinationFilePath, $destinationFormat);
        }
        else
        {
            echo 'file does not exist';
        }

        return false;
    }

    protected function recognizeDestinationFormat($sourceFilePath, $destinationFilePathExtension, $destinationFormat = null)
    {
        if ($destinationFormat == '')
        {
            $destinationFormat = $destinationFilePathExtension;
        }
        if ($destinationFormat == '')
        {
            $destinationFormat = pathinfo($sourceFilePath, PATHINFO_EXTENSION);
        }
        if ($destinationFormat == '')
        {
            $destinationFormat = $this->defaultFormatExtension;
        }
        $destinationFormat = strtolower($destinationFormat);

        return $destinationFormat;
    }

    protected function saveImage($resampledImage, $destinationFilePath, $destinationFormat)
    {
        switch ($destinationFormat)
        {
            case 'gif': $result = imagegif($resampledImage, $destinationFilePath);
                break;
            case 'jpeg':
            case 'jpg':
                $result = imagejpeg($resampledImage, $destinationFilePath, $this->jpegQuality); // 100% quality.
                break;
            default: $result = imagepng($resampledImage, $destinationFilePath, $this->pngQuality);
                break; // Maximum compression level.
        }

        return $result;
    }
}

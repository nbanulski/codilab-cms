KFINDER MODIFICATION::::BETA 1 
================================

1. Copy KCFinder into CKEditor Directory or any directory in WWW Folder

2. Add the Following lines in your ckeditor configuration (config.js):

	   config.filebrowserBrowseUrl = 'js/ckeditor/kcfinder/browse.php?type=files';
	   config.filebrowserImageBrowseUrl = 'js/ckeditor/kcfinder/browse.php?type=images';
	   config.filebrowserFlashBrowseUrl = 'js/ckeditor/kcfinder/browse.php?type=flash';
	   config.filebrowserUploadUrl = 'js/ckeditor/kcfinder/upload.php?type=files';
	   config.filebrowserImageUploadUrl = 'js/ckeditor/kcfinder/upload.php?type=images';
	   config.filebrowserFlashUploadUrl = 'js/ckeditor/kcfinder/upload.php?type=flash';

3. Change value of baseURL in KCFinder/compress.php to where you want to upload files after they get compressed.


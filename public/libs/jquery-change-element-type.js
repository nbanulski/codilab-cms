(function($){
    $.fn.changeElementType=function(newType){
        var attributes={};
        $.each(this[0].attributes,function(idx,attribute){
            attributes[attribute.nodeName]=attribute.nodeValue;
        });
        this.replaceWith(function(){
            return $("<"+newType+"/>",attributes).append($(this).contents());
        });
    };
})(jQuery);
<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
return array(
    'cms' => array(
        'default_language_iso_code' => 'pol',
        'default_name_for_index_page' => 'start',
        'password_salt' => '&jU37K%tm__rB@SGq2h^5ll$0ny*EOA)',
        'publish_blocks_after_insertion' => '1',
        'remember_me_time_in_seconds' => '86400',
        'system_email_sender_address' => 'norbert.banulski@codilab.pl',
        'system_email_sender_host' => 'codilab.pl',
        'system_email_sender_server_name' => 'codilab.pl',
        'system_email_sender_login' => 'norbert.banulski@codilab.pl',
        'system_email_sender_password' => 'biedrona2742',
        'system_email_sender_port' => '993',
        'system_email_sender_ssl' => '1',
        'system_email_sender_connection_class' => 'login',
        'system_email_sender_title' => 'CodiLab',
        'domain_name' => 'codilab',
        'site_name' => 'CodiLab CMS'
    ),
    'phpSettings' => array(
        'date.timezone' => 'Europe/Warsaw'
    ),
    'session' => array(
        'remember_me_seconds' => '86400',
        'use_cookies' => '1',
        'cookie_httponly' => '1'
    )
);
<?php
$environment = getenv('APP_ENV') ? : 'production';

$dbParams = array(
    'driver' => 'ExtendedPdo',
    'database' => 'proimagine_cms_sandbox',
    'username' => 'root',
    'password' => '',
    'hostname' => '127.0.0.1',
    'charset' => 'utf8',
    'options' => array('buffer_results' => true)
);

// Use the $environment value to determine which modules to load:
if ($environment == 'development')
{
    $serviceManagerFactories = array(
        'Zend\Db\Adapter\Adapter' => function ($sm) use ($dbParams)
    {
        $adapter = new BjyProfiler\Db\Adapter\ProfilingAdapter(array(
            'driver' => $dbParams['driver'],
            'dsn' => 'mysql:dbname=' . $dbParams['database'] . ';host=' . $dbParams['hostname'] . ';charset=' . $dbParams['charset'],
            'database' => $dbParams['database'],
            'username' => $dbParams['username'],
            'password' => $dbParams['password'],
            'hostname' => $dbParams['hostname'],
        ));

        if (php_sapi_name() == 'cli')
        {
            $logger = new Zend\Log\Logger();

            // Write queries profiling info to stdout in CLI mode:
            $writer = new Zend\Log\Writer\Stream('php://output');
            $logger->addWriter($writer, Zend\Log\Logger::DEBUG);
            $adapter->setProfiler(new BjyProfiler\Db\Profiler\LoggingProfiler($logger));
        }
        else
        {
            $adapter->setProfiler(new BjyProfiler\Db\Profiler\Profiler());
        }
        if (isset($dbParams['options']) && is_array($dbParams['options']))
        {
            $options = $dbParams['options'];
        }
        else
        {
            $options = array();
        }

        $adapter->injectProfilingStatementPrototype($options);

\Zend\Db\TableGateway\Feature\GlobalAdapterFeature::setStaticAdapter($adapter);

        return $adapter;
    }
    );
}
else // "production" or environment other than "development".
{
    $serviceManagerFactories = array(
        'Zend\Db\Adapter\Adapter' => function ($serviceManager)
    {
        $adapterFactory = new Zend\Db\Adapter\AdapterServiceFactory();
        $adapter = $adapterFactory->createService($serviceManager);

\Zend\Db\TableGateway\Feature\GlobalAdapterFeature::setStaticAdapter($adapter);

        return $adapter;
    }
    );
}

return array(
    'db' => array(
        'driver' => $dbParams['driver'],
        'dsn' => 'mysql:dbname=' . $dbParams['database'] . ';host=' . $dbParams['hostname'] . ';charset=' . $dbParams['charset'],
        'username' => $dbParams['username'],
        'password' => $dbParams['password']
    ),
    'service_manager' => array(
        'factories' => $serviceManagerFactories,
    ),
);
